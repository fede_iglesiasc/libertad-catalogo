<?php

//Max execution time
set_time_limit(0);
ini_set('memory_limit','1024M');

ini_set ('display_errors', 'on');
ini_set ('log_errors', 'on');
ini_set ('display_startup_errors', 'on');
ini_set ('error_reporting', E_ALL);

  


require_once '../classes/class.imap.php';
require_once '../classes/PHPExcel.php';
require_once '../classes/simple_html_dom.php';
include_once '../mysql.php';
include_once "../classes/mpdf57/mpdf.php";
include_once '../classes/PrintNode/Bootstrap.php';



$stmt = $GLOBALS['conf']['pdo']->query("SELECT impresora_1, impresora_2, impresora_3, impresora_4, impresora_5 FROM impresoras LIMIT 1");
$lista_impresoras =  $stmt->fetch(PDO::FETCH_NUM);

//Parseamos a int
foreach($lista_impresoras as $k => $v) $lista_impresoras[$k] = (int)$v;


/////////////////////////////////////////////////////////////////////
// CONECTAMOS A CLOUDPRINT PARA VER SI LA IMPRESORA ESTA ACTIVA
/////////////////////////////////////////////////////////////////////

$GLOBALS['toolbox']->createPrintNodeClient();
$computers = $GLOBALS['printnode']->viewComputers();
$connectedPc = null;


//PRINTSERVER 
if(isset($computers[55393]) && ($computers[55393]->state == 'connected')){
    $connectedPc = $computers[55393];
}

//SSD
elseif(isset($computers[55137]) && ($computers[55137]->state == 'connected')){
	$connectedPc = $computers[55137];
}

//Mechanic
elseif(isset($computers[55360]) && ($computers[55360]->state == 'connected')){
	$connectedPc = $computers[55360];
}

//Gabriel
elseif(isset($computers[55294]) && ($computers[55294]->state == 'connected')){
	$connectedPc = $computers[55294];
}

if(!$connectedPc) return;

$GLOBALS['printer'] = $connectedPc;




//////////////////////////////////////////////////////////////////////////////////
// OBTENEMOS TODOS LOS IDS DE LOS CORREOS QUE ACTUALMENTE ESTAN EN LA CASILLA
//////////////////////////////////////////////////////////////////////////////////

$mailbox = 'mail.distribuidoralibertad.com';
$username = 'pedidos@distribuidoralibertad.com';
$password = '2019Chile';
$encryption = 'tls';

// open connection
$GLOBALS['imap'] = new Imap($mailbox, $username, $password, $encryption);

// stop on error
if($GLOBALS['imap']->isConnected()===false)
    die($GLOBALS['imap']->getError());

// select folder Inbox
$GLOBALS['imap']->selectFolder('INBOX');

//Lista de UIDs
$uids = $GLOBALS['imap']->getUids();



//////////////////////////////////////////////////////////////////////////////////
// AGREGAMOS A LA DB TODOS LOS MAILS NUEVOS
//////////////////////////////////////////////////////////////////////////////////
$sql = "INSERT IGNORE INTO mailprint (uid) VALUES (".implode("), (", $uids).")";
$GLOBALS['conf']['pdo']->query($sql);




//////////////////////////////////////////////////////////////////////////////////
// FILTRAMOS EMAILS QUE NO DEBEN IMPRIMIRSE
//////////////////////////////////////////////////////////////////////////////////

$stmt = $GLOBALS['conf']['pdo']->query("SELECT uid FROM mailprint WHERE (pdf_was_generated = 0) OR (fecha_impresion IS NULL)");
$uids_filtrar = $stmt->fetchAll(PDO::FETCH_COLUMN);
foreach($uids_filtrar as $k=>$uid) 
	if(in_array((int)$uid, $uids))
		filterMails((int)$uid);






//////////////////////////////////////////////////////////////////////////////////
// EXISTEN MAILS QUE HACE MAS DE 15 MINUTOS NO SE IMPRIMEN?
//////////////////////////////////////////////////////////////////////////////////
$sql = " 	SELECT 	uid 
			FROM 	mailprint
			WHERE 	fecha_creacion IS NOT NULL 
					AND (fecha_creacion < DATE_SUB(NOW(),INTERVAL 15 MINUTE)) 
					AND fecha_impresion IS NULL";

$stmt = $GLOBALS['conf']['pdo']->query($sql);
$uids_pendientes = $stmt->fetchAll(PDO::FETCH_COLUMN);



if(count($uids_pendientes)){

  $printer_active = null;
  //printserver
  if($connectedPc->id == 55393) $printer_active = '388963';
  //ssd
  if($connectedPc->id == 55137) $printer_active = '378619';
  //mecanico
  if($connectedPc->id == 55360) $printer_active = '336289';
  //gabrielrt
  if($connectedPc->id == 55294) $printer_active = '391255';


  /*$printJob = new \PrintNode\Entity\PrintJob($GLOBALS['printnode']);
  $printJob->title = "Aviso Mails Pendientes";
  $printJob->source = 'Aviso Mails pendientes';
  $printJob->contentType = 'pdf_base64';
  $printJob->addPdfFile('./aviso_mails.pdf');
  $printJob->printer = $printer_active;
  $printJobId = $GLOBALS['printnode']->createPrintJob($printJob);*/
}



//////////////////////////////////////////////////////////////////////////////////
// CREAMOS LOS ARCHIVOS CORRESPONDIENTES
//////////////////////////////////////////////////////////////////////////////////

$stmt = $GLOBALS['conf']['pdo']->query("SELECT uid FROM mailprint WHERE pdf_was_generated = 0");
$uids_creados = $stmt->fetchAll(PDO::FETCH_COLUMN);

foreach($uids_creados as $k=>$uid)
	//Chequeamos que existan en la casilla 
	if(in_array((int)$uid, $uids)){
		echo $uid;
		generateMailFiles((int)$uid);
	}




//////////////////////////////////////////////////////////////////////////////////
// MANDAMOS A IMPRIMIR SOLO LOS MAILS QUE NO ESTAN IMPRESOS
//////////////////////////////////////////////////////////////////////////////////

$sql = "SELECT uid FROM mailprint WHERE fecha_impresion IS NULL";
$stmt = $GLOBALS['conf']['pdo']->query($sql);
$uids_por_imprimir = $stmt->fetchAll(PDO::FETCH_COLUMN);


//Marcamos leidos los Mails de confirmacion
foreach($uids_por_imprimir as $x=>$uid)
	printMail((int)$uid);




function printMail($uid_actual){

  if(!file_exists('./tmp/'.$uid_actual.'/'.$uid_actual.'.pdf'))
	return;

  $printer_active = null;
  //printserver
  if($GLOBALS['printer']->id == 55393) $printer_active = '388963';
	//ssd
	if($GLOBALS['printer']->id == 55137) $printer_active = '378619';
	//mecanico
	if($GLOBALS['printer']->id == 55360) $printer_active = '336289';
	//gabriel
	if($GLOBALS['printer']->id == 55294) $printer_active = '391255';


	$printJob = new \PrintNode\Entity\PrintJob($GLOBALS['printnode']);
	$printJob->title = "Mail ".$uid_actual;
	$printJob->source = 'Mail';
	$printJob->contentType = 'pdf_base64';
	$printJob->addPdfFile('./tmp/'.$uid_actual.'/'.$uid_actual.'.pdf');
	$printJob->printer = $printer_active;
	$printJobId = $GLOBALS['printnode']->createPrintJob($printJob);

	$sql = "	UPDATE 	mailprint
				SET 	fecha_impresion = NOW(), 
						printjob_id=".$printJobId."
				WHERE 	uid = ".$uid_actual;
	
	$GLOBALS['conf']['pdo']->query($sql);


	$filesInFolder = glob('./tmp/'.$uid_actual.'/*.{pdf}', GLOB_BRACE);
	foreach($filesInFolder as $f){
		$path = pathinfo($f);
		if($path['basename'] != $uid_actual.'.pdf'){
			$printJob = new \PrintNode\Entity\PrintJob($GLOBALS['printnode']);
			$printJob->title = "Mail ".$uid_actual . " - Adjunto";
			$printJob->source = 'Mail';
			$printJob->contentType = 'pdf_base64';
			$printJob->addPdfFile($f);
			$printJob->printer = $printer_active;
			$printJobId = $GLOBALS['printnode']->createPrintJob($printJob);

		}
	}
}


function getBody($html){

	$html = mb_convert_encoding($html, 'utf8', 'auto');
	$html = str_get_html($html);
	//var_dump($html);
	if(!$html) return ""; 
	$body = $html->find('body');
	
	//Si no encontramos body retornamos false
	if(!is_bool($body))
		if($body && is_array($body) && isset($body[0])) $result = $body[0]->innertext;
	else $result = $html;

	//Replace !important
	$result = str_replace('!important', '', $result);

	return $result;
}


function toPortrait($file){
	//Get the width and height of our image in pixels.
	list($width, $height) = getimagesize($file);

	//If the height is larger than the width, assume
	//that it is a portrait photograph.
	if($height < $width){
		//Create an image from our original JPEG image.
		$sourceCopy = imagecreatefromjpeg($file);
		//Rotate this image by 90 degrees.
		$rotatedImg = imagerotate($sourceCopy, 90, 0);
		//Replace original image with rotated / landscape version.
		imagejpeg($rotatedImg, $file);
	}
}


function xlsToPdf($filename, $filenamePDF){
	$mPDFLibraryPath = '../classes/mpdf57';
	$objPHPExcel = PHPExcel_IOFactory::load($filename);
	$objPHPExcel->setActiveSheetIndex(0);

	PHPExcel_Settings::setPdfRenderer(PHPExcel_Settings::PDF_RENDERER_MPDF, $mPDFLibraryPath);
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'PDF');

	$objWriter->save($filenamePDF);
	return true;
}

function filterMails($uid_actual){
	$email = $GLOBALS['imap']->getMessage($uid_actual);

	$sql = " 	INSERT INTO 	mailprint 	(uid, fecha_impresion, fecha_creacion, printjob_id, pdf_was_generated)
				VALUES 			($uid_actual,NOW(), NOW(), NULL, 1)
				ON DUPLICATE KEY UPDATE 
  					fecha_impresion= NOW(), fecha_creacion=NOW(), printjob_id=NULL, pdf_was_generated=1";

	if(($email['from'] == 'federico.colombo@distribuidoralibertad.com') && ($email['subject'] == 'Recibimos el Pedido')){
		$GLOBALS['conf']['pdo']->query($sql);
	}
}

function generateMailFiles($uid_actual){
	
	//Encabezado
	$html = <<<EOD
		<!doctype html>
		<html>
		    <head>
		        <meta charset="utf-8">
		    </head>
		    <body style="font-family: 'opensans', sans-serif;">

				<div style="">
					<table>
						<tr><td width="150"><b>De:</b></td><td>{{DE}}</td></tr>
						{{CLIENTE}}
						{{DATOS_CONTACTO}}
						<tr><td width="150"><b>Fecha:</b></td><td>{{FECHA}}</td></tr>
						<tr><td width="150"><b>Para:</b></td><td>{{PARA}}</td></tr>
						{{CC}}
						<tr><td width="150"><b>Asunto:</b></td><td>{{ASUNTO}}</td></tr>
						{{ADJUNTOS_DETALLE}}
					</table>
				</div>     
			    
			    <div style="margin-top: 20px;">
			    {{CONTENIDO}}
			    </div>
			    <div class="adjuntos">{{ADJUNTOS}}</div>
		    </body>
		</html>
EOD;

	$css = <<<EOD
	        .adjuntos{
	        	display: flex;
	        	flex-direction: column;
	        }
EOD;

	
	$email = $GLOBALS['imap']->getMessage($uid_actual);



	$cliente = array();
	//Consultamos si el cliente existe en la base de datos con ese email

	$sql = "SELECT 	usuario,
					(SELECT razon_social FROM clientes WHERE usuario = t.usuario) as razon_social,
					(SELECT telefono FROM clientes WHERE usuario = t.usuario) as telefono,
					(CONCAT(
						(SELECT domicilio FROM clientes WHERE usuario = t.usuario),
						', ',
						(SELECT nombre FROM sys_localidades WHERE id = (SELECT localidad FROM clientes WHERE usuario = t.usuario)),
						', ',
						(SELECT nombre FROM sys_provincias WHERE id = (SELECT provincia_id FROM sys_localidades WHERE id = (SELECT localidad FROM clientes WHERE usuario = t.usuario)))
					)) as domicilio
			FROM 	usuarios t
			WHERE 	(email = '".$email['from']."') AND 
					(rol_id = 2)";

	$stmt = $GLOBALS['conf']['pdo']->query($sql);
	$cliente = $stmt->fetchAll(PDO::FETCH_ASSOC);

	if(count($cliente)){
		$cliente_contacto = $cliente[0]['domicilio'] . " - TEL: " . $cliente[0]['telefono'];
		$cliente_rsocial = $cliente[0]['usuario'] . ' - ' .$cliente[0]['razon_social'];
	}else{
		$cliente_contacto = $cliente_rsocial = '';
	}

	$html = str_replace("{{DE}}", $email['from'], $html);

	if(count($cliente)){
		$html = str_replace("{{CLIENTE}}", '<tr><td width="150"><b>Cliente:</b></td><td>' . $cliente_rsocial . '</td></tr>', $html);
		$html = str_replace("{{DATOS_CONTACTO}}", '<tr><td width="150"><b>Contacto:</b></td><td>' . $cliente_contacto . '</td></tr>', $html);
	}else{
		$html = str_replace("{{CLIENTE}}", '', $html);
		$html = str_replace("{{DATOS_CONTACTO}}", '', $html);
	}

	$html = str_replace("{{FECHA}}", $email['date'], $html);

	if($email['to'] != '') $html = str_replace("{{PARA}}", implode(',', $email['to']), $html);
	else $html = str_replace("{{PARA}}", '', $html);

	if(!isset($email['cc'])) $cc = '';
	else $cc = '<tr><td width="150"><b>CC:</b></td><td>' . implode(',', $email['cc']) . '</td></tr>';

	$html = str_replace("{{CC}}", $cc, $html);
	$html = str_replace("{{ASUNTO}}", $email['subject'], $html);
	

	if(is_string(getBody($email['body'])))
		$html = str_replace("{{CONTENIDO}}", getBody($email['body']), $html);
	else 
		$html = str_replace("{{CONTENIDO}}", 'contenido invalido', $html);


	//Array de impresiones
	$impresiones = [];

	//Creamos un directorio nuevo
	if(!file_exists('./tmp/'.$email['uid']))
		mkdir('./tmp/'.$email['uid']);


	$impresiones[] = array('archivo'=>'./tmp/'.$email['uid'].'/'.$email['uid'].'.pdf');


	//Grabamos en directorio
	if(isset($email['attachments']))
		foreach ($email['attachments'] as $k=>$a){
			$attachment = $GLOBALS['imap']->getAttachment($email['uid'],$k);
			$archivo = './tmp/'.$email['uid'].'/'.$a['name'];
			if($a['disposition'] == 'attachment')
				$impresiones[] = array('archivo'=>$archivo);
			$fp = fopen($archivo, "w+");
			fwrite($fp, $attachment['content']);
			fclose($fp);
		}



	//AGREGAMOS ADJUNTOS
	$adjuntos_detalle = Array();
	$adjuntos = Array();
	$imgs = Array();
	$imgs_str = '';

	//////////////////////////////////////////////////////////////////////////////////
	// XLS Y XLSX ADJUNTOS
	//////////////////////////////////////////////////////////////////////////////////
	$filesInFolder = glob('./tmp/'.$email['uid'].'/*.{xls,xlsx}', GLOB_BRACE);
	$xls_ext = ['xls','xlsx'];
	foreach($filesInFolder as $f) {
		$path = pathinfo($f);

		if(in_array(strtolower($path['extension']), $xls_ext)){	

			$pathPDF = $path['dirname'].'/'.$path['filename'].'.pdf';

			if(xlsToPdf($f, $pathPDF)){
				$adjuntos[] = $pathPDF;
				$adjuntos_detalle[] = $path['basename'];
			}
		}
	}

	//////////////////////////////////////////////////////////////////////////////////
	// IMAGENES ADJUNTAS
	//////////////////////////////////////////////////////////////////////////////////

	$filesInFolder = glob('./tmp/'.$email['uid'].'/*', GLOB_BRACE);
	$img_ext = ['jpg','png','gif','jpeg'];
	foreach($filesInFolder as $f) {
		$path = pathinfo($f);

		if(in_array(strtolower($path['extension']), $img_ext)){

			//Path del File
			$file = './tmp/'.$email['uid'].'/'.$path['basename'];
			
			//String HTML para agregar al template
			$imgs[] = '<img src="'.$file.'"/>';

			//Agregamos a la lista de adjuntos
			$adjuntos_detalle[] = $path['basename'];
			
			//A Portrait
			toPortrait($file);
		} 
	}

	//Agregamos detalles de adjuntos detalles de adjuntos
	if(!count($adjuntos_detalle)) $html =  str_replace("{{ADJUNTOS_DETALLE}}", '', $html);
	else $html = str_replace("{{ADJUNTOS_DETALLE}}", '<tr><td width="150"><b>ADJUNTOS:</b></td><td>' . implode(' - ', $adjuntos_detalle) . '</td></tr>', $html);


	//Agregamos los adjuntos al template
	if(count($imgs)) $imgs_str = implode('', $imgs);
	$html = str_replace("{{ADJUNTOS}}", $imgs_str, $html);

	$pdf_file = './tmp/'.$email['uid'].'/'.$email['uid'].'.pdf';

	//Borrar si existe uno anterior
	if(file_exists($pdf_file)) unlink($pdf_file);

	//Creamos el PDF Vacio
	$mpdf =new mPDF('','', 11, '', 11, 10, 10, 10, 0, 0, '');
	$mpdf->WriteHTML($css,1);
	$mpdf->WriteHTML($html);
	$mpdf->Output('./tmp/'.$email['uid'].'/'.$email['uid'].'.pdf','F');

    //Save all in DB
	//$sql = "UPDATE mailprint SET pdf_was_generated = 1 WHERE uid = ".$email['uid'];
	$GLOBALS['conf']['pdo']->query("UPDATE mailprint SET pdf_was_generated = 1 WHERE uid = ".$email['uid']);
}

?>
