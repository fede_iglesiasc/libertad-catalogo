<?php

	include_once('config.php');
	ini_set('memory_limit', '2048M');


	//Quitamos el limite de ejecucuin
	set_time_limit(0);

	//Incluimos clase para generar PDF
	include("./classes/mpdf57/mpdf.php");

	//Borramos todos los archivos anteriores
	$files = glob('./listas/*');
	foreach($files as $file)
	  if(is_file($file))
	    unlink($file);


	$css = "body{font-family: sans-serif; font-size: 10px} ";
	$css .= ".col1{width: 157px; height: 900px; position: fixed; left: 0px; top: 0px;} ";
	$css .= ".col2{width: 157px; height: 900px; position: fixed; left: 186px; top: 0px;} ";
	$css .= ".col3{width: 157px; height: 900px; position: fixed; right: 186px; top: 0px;} ";
	$css .= ".col4{width: 157px; height: 900px; position: fixed; right: 0px; top: 0px;} ";
	$css .= ".articulo{width: 157px; height: 17px; border-bottom: 1px dotted black;} ";

	//Columna 1
	$css .= ".pref-1{width: 24px; height: 17px; position: fixed; left: 0px; line-height: 17px; text-align: center; overflow: hidden;} ";
	$css .= ".codigo-1{width: 60px; height: 17px; position: fixed; left: 24px; line-height: 17px; text-align: center; overflow: hidden;} ";
	$css .= ".suf-1{width: 24px; height: 17px; position: fixed; left: 84px; line-height: 17px; text-align: center; overflow: hidden;} ";
	$css .= ".moneda-1{width: 8px; height: 17px; position: fixed; left: 108px; line-height: 17px; text-align: right; overflow: hidden;} ";
	$css .= ".precio-1{width: 50px; height: 17px; position: fixed; left: 116px; line-height: 17px; text-align: right; overflow: hidden;} ";
	$css .= ".guiones-1{width: 166px; height: 1px; position: fixed; left: 0px; line-height: 1px; overflow: hidden;} ";
	$css .= ".col1-cod{width: 108px; height: 17px; position: fixed; left: 0px; line-height: 17px; overflow: hidden; text-align: center; font-weight: bold;} ";
	$css .= ".col1-precio{width: 58px; height: 17px; position: fixed; left: 108px; line-height: 17px; overflow: hidden; text-align: center; font-weight: bold;} ";

	//Columna 2
	$css .= ".pref-2{width: 24px; height: 17px; position: fixed; left: 184px; line-height: 17px; text-align: center; overflow: hidden;} ";
	$css .= ".codigo-2{width: 60px; height: 17px; position: fixed; left: 208px; line-height: 17px; text-align: center; overflow: hidden;} ";
	$css .= ".suf-2{width: 24px; height: 17px; position: fixed; left: 268px; line-height: 17px; text-align: center; overflow: hidden;} ";
	$css .= ".moneda-2{width: 8px; height: 17px; position: fixed; left: 292px; line-height: 17px; text-align: right; overflow: hidden;} ";
	$css .= ".precio-2{width: 50px; height: 17px; position: fixed; left: 300px; line-height: 17px; text-align: right; overflow: hidden;} ";
	$css .= ".guiones-2{width: 166px; height: 1px; position: fixed; left: 184px; line-height: 1px; overflow: hidden;} ";
	$css .= ".col2-cod{width: 108px; height: 17px; position: fixed; left: 184px; line-height: 17px; overflow: hidden; text-align: center; font-weight: bold;} ";
	$css .= ".col2-precio{width: 58px; height: 17px; position: fixed; left: 292px; line-height: 17px; overflow: hidden; text-align: center; font-weight: bold;} ";

	//Columna 3
	$css .= ".pref-3{width: 24px; height: 17px; position: fixed; left: 369px; line-height: 17px; text-align: center; overflow: hidden;} ";
	$css .= ".codigo-3{width: 60px; height: 17px; position: fixed; left: 393px; line-height: 17px; text-align: center; overflow: hidden;} ";
	$css .= ".suf-3{width: 24px; height: 17px; position: fixed; left: 453px; line-height: 17px; text-align: center; overflow: hidden;} ";
	$css .= ".moneda-3{width: 8px; height: 17px; position: fixed; left: 477px; line-height: 17px; text-align: right; overflow: hidden;} ";
	$css .= ".precio-3{width: 50px; height: 17px; position: fixed; left: 485px; line-height: 17px; text-align: right; overflow: hidden;} ";
	$css .= ".guiones-3{width: 166px; height: 1px; position: fixed; left: 369px; line-height: 1px; overflow: hidden;} ";
	$css .= ".col3-cod{width: 108px; height: 17px; position: fixed; left: 368px; line-height: 17px; overflow: hidden; text-align: center; font-weight: bold;} ";
	$css .= ".col3-precio{width: 58px; height: 17px; position: fixed; left: 476px; line-height: 17px; overflow: hidden; text-align: center; font-weight: bold;} ";

	//Columna 4
	$css .= ".pref-4{width: 24px; height: 17px; position: fixed; right: 142px; line-height: 17px; text-align: center; overflow: hidden;} ";
	$css .= ".codigo-4{width: 60px; height: 17px; position: fixed; right: 82px; line-height: 17px; text-align: center; overflow: hidden;} ";
	$css .= ".suf-4{width: 24px; height: 17px; position: fixed; right: 58px; line-height: 17px; text-align: center; overflow: hidden;} ";
	$css .= ".moneda-4{width: 8px; height: 17px; position: fixed; right: 50px; line-height: 17px; text-align: right; overflow: hidden;} ";
	$css .= ".precio-4{width: 50px; height: 17px; position: fixed; right: 0px; line-height: 17px; text-align: right; overflow: hidden;} ";
	$css .= ".guiones-4{width: 166px; height: 1px; position: fixed; right: 0px; line-height: 1px; overflow: hidden;} ";
	$css .= ".col4-cod{width: 108px; height: 17px; position: fixed; right: 58px; line-height: 17px; overflow: hidden; text-align: center; font-weight: bold;} ";
	$css .= ".col4-precio{width: 58px; height: 17px; position: fixed; right: 0px; line-height: 17px; overflow: hidden; text-align: center; font-weight: bold;} ";


	//Consultamos toda la lista de aumentos
	$sql = "SELECT  DISTINCT 
	                id_marca, 
	                fabricante_nombre,
	                fecha,
	                0 as por_rubros,
	                precio_update
	        FROM (

	                SELECT              id_marca,
	                                    precio_update,
	                                    (DATE_FORMAT(precio_update,'%d-%m-%Y')) as fecha,
	                                    (SELECT nombre FROM catalogo_marcas WHERE id=id_marca) as fabricante_nombre,
	                                    (SELECT descarga_por_rubro FROM aumentos WHERE (fabricante = a.id_marca) AND (fecha = a.precio_update)) as descarga_por_rubro
	                FROM (
	                    
	                    SELECT DISTINCT     id_marca, 
	                                        precio_update
	                    FROM                catalogo_articulos 
	                    WHERE               (precio_update > '2015-12-04') AND 
	                                        (id_marca IS NOT NULL) AND 
	                                        (precio_anterior > 0.000)
	                    ORDER BY            precio_update DESC
	                ) as a
	        ) as b

	        WHERE   descarga_por_rubro IS NULL

	        UNION(
	           SELECT  DISTINCT 
	                    id_marca, 
	                    fabricante_nombre,
	                    (DATE_FORMAT(b.precio_update,'%d-%m-%Y')) as fecha,
	                    1 as por_rubros,
	                    precio_update
	            FROM (

	                    SELECT              id_marca,
	                                        precio_update,
	                                        (DATE_FORMAT(a.precio_update,'%d-%m-%Y')) as fecha,
	                                        (SELECT nombre FROM catalogo_marcas WHERE id=id_marca) as fabricante_nombre,
	                                        (SELECT descarga_por_rubro FROM aumentos WHERE (fabricante = a.id_marca) AND (fecha = a.precio_update)) as descarga_por_rubro
	                    FROM (
	                        
	                        SELECT DISTINCT     id_marca, 
	                                            precio_update
	                        FROM                catalogo_articulos 
	                        WHERE               (precio_update > '2015-12-04') AND 
	                                            (id_marca IS NOT NULL) AND 
	                                            (precio_anterior > 0.000)
	                        ORDER BY            precio_update DESC
	                    ) as a
	            ) as b

	            WHERE   descarga_por_rubro IS NOT NULL 
	        )
	        ";

	$stmt = $GLOBALS['conf']['pdo']->prepare($sql);
	$stmt->execute();
	$aumentos = $stmt->fetchAll(PDO::FETCH_ASSOC);


	//Generamos el pdf
	foreach ($aumentos as $k=>$aum){

	    //Traemos todos los Articulos
	    $sql = "    SELECT  prefijo, codigo, sufijo, precio
	                FROM    catalogo_articulos
	                WHERE   (habilitado = 1) AND 
	                        id_marca = ".$aum['id_marca'];

	    if( (int)$aum['por_rubros'] == 1 )
	        $sql = "    SELECT  prefijo, codigo, sufijo, precio
	                    FROM    catalogo_articulos 
	                    WHERE   id_marca = ".$aum['id_marca']."
	                            AND (habilitado = 1)
	                            AND id_rubro IN (
	                                    SELECT  id_rubro 
	                                    FROM    catalogo_articulos 
	                                    WHERE   id_marca = ".$aum['id_marca']." AND
	                                            precio_update = '".$aum['precio_update']."' AND 
	                                            id_rubro IS NOT NULL
	                            )";

	    $stmt = $GLOBALS['conf']['pdo']->prepare($sql);
	    $stmt->execute();
	    $articulos = $stmt->fetchAll(PDO::FETCH_ASSOC);

	    //Ordenamos naturalmente
	    usort($articulos, function($a1, $b1) {
	        if($a1['codigo'] == $b1['codigo']) 
	            return strnatcmp($a1['sufijo'], $b1['sufijo']);
	        return strnatcmp($a1['codigo'], $b1['codigo']);
	    });

	    
	    $html = '';
	    $art_col = 54;
	    $art_hoja = ($art_col*4);
	    $articulos_cant = count($articulos);
	    $paginas = ceil($articulos_cant/$art_hoja);
	    $fabricante = $aum['id_marca'];

	    for ($p=0; $p < $paginas; $p++){
	        
	        $icol1 = ($art_hoja*$p);
	        $icol2 = ($art_hoja*$p)+($art_col*1);
	        $icol3 = ($art_hoja*$p)+($art_col*2);
	        $icol4 = ($art_hoja*$p)+($art_col*3);

	        //Cabeceras
	        if(isset($articulos[$icol1]))
	            $html .= '<div class="col1-cod">CODIGO</div><div class="col1-precio">PRECIO</div>';
	        if(isset($articulos[$icol2]))
	            $html .= '<div class="col2-cod">CODIGO</div><div class="col2-precio">PRECIO</div>';
	        if(isset($articulos[$icol3]))
	            $html .= '<div class="col3-cod">CODIGO</div><div class="col3-precio">PRECIO</div>';
	        if(isset($articulos[$icol4]))
	            $html .= '<div class="col4-cod">CODIGO</div><div class="col4-precio">PRECIO</div>';
	    
	        for ($i=0; $i < $art_col; $i++){
	            
	            $icol1 = $i+($art_hoja*$p);
	            $icol2 = $i+($art_hoja*$p)+($art_col*1);
	            $icol3 = $i+($art_hoja*$p)+($art_col*2);
	            $icol4 = $i+($art_hoja*$p)+($art_col*3);

	            $y = ($i*17)+17;
	            $yg = $i*17+17;

	            if(isset($articulos[$icol1])){
	                $html .= '<div class="pref-1" style="top: '. $y .'px;">' . $articulos[$icol1]['prefijo'] . '</div><div class="codigo-1" style="top: '. $y .'px;">' . $articulos[$icol1]['codigo'] . '</div><div class="suf-1" style="top: '. $y .'px;">' . $articulos[$icol1]['sufijo'] . '</div><div class="moneda-1" style="top: '. $y .'px;">$</div><div class="precio-1" style="top: '. $y .'px;">'.number_format($articulos[$icol1]['precio'],2).'</div><div class="guiones-1" style="top: '. $yg .'px;"><img src="./img/guiones.svg"/></div>';
	            }
	            
	            if(isset($articulos[$icol2])){
	                $html .= '<div class="pref-2" style="top: '. $y .'px;">' . $articulos[$icol2]['prefijo'] . '</div><div class="codigo-2" style="top: '. $y .'px;">' . $articulos[$icol2]['codigo'] . '</div><div class="suf-2" style="top: '. $y .'px;">' . $articulos[$icol2]['sufijo'] . '</div><div class="moneda-2" style="top: '. $y .'px;">$</div><div class="precio-2" style="top: '. $y .'px;">'.number_format($articulos[$icol2]['precio'],2).'</div><div class="guiones-2" style="top: '. $yg .'px;"><img src="./img/guiones.svg"/></div>';
	            }

	            if(isset($articulos[$icol3])){
	                $html .= '<div class="pref-3" style="top: '. $y .'px;">' . $articulos[$icol3]['prefijo'] . '</div><div class="codigo-3" style="top: '. $y .'px;">' . $articulos[$icol3]['codigo'] . '</div><div class="suf-3" style="top: '. $y .'px;">' . $articulos[$icol3]['sufijo'] . '</div><div class="moneda-3" style="top: '. $y .'px;">$</div><div class="precio-3" style="top: '. $y .'px;">'.number_format($articulos[$icol3]['precio'],2).'</div><div class="guiones-3" style="top: '. $yg .'px;"><img src="./img/guiones.svg"/></div>';
	            }

	            if(isset($articulos[$icol4])){
	                $html .= '<div class="pref-4" style="top: '. $y .'px;">' . $articulos[$icol4]['prefijo'] . '</div><div class="codigo-4" style="top: '. $y .'px;">' . $articulos[$icol4]['codigo'] . '</div><div class="suf-4" style="top: '. $y .'px;">' . $articulos[$icol4]['sufijo'] . '</div><div class="moneda-4" style="top: '. $y .'px;">$</div><div class="precio-4" style="top: '. $y .'px;">'.number_format($articulos[$icol4]['precio'],2).'</div><div class="guiones-4" style="top: '. $yg .'px;"><img src="./img/guiones.svg"/></div>';
	            }
	        }


	        if( !($paginas == ($p+1)) ){
	            $html .= '<pagebreak />';
	        }
	    }


	    //Generamos el PDF
	    $mpdf=new mPDF('c','A4','','',12,10,25,20,10,10);
	    $mpdf->debug = true;
	    $mpdf->SetDisplayMode('fullpage');
	    $mpdf->SetHTMLHeader('<table style="width: 800px;"><tr><td width="268"><img src="fabricantes/logo.png" style="max-height: 50px;"/></td><td width="266" align="center" style="font-size: 18px;"><b>LISTA DE PRECIOS</b></td><td align="right" width="266"><img src="fabricantes/'.$fabricante.'.svg" style="max-height: 40px; max-width: 130px; margin-top: 5px;"/></td></tr></table>');
	    $mpdf->SetHTMLFooter('<table style="width: 800px;"><tr><td width="268" style="font-size: 13px;">Los precios no incluyen IVA</td><td width="266" align="center" style="font-size: 17px; font-weight: bold;">{PAGENO}</td><td align="right" style="font-size: 13px;" width="266">'.$aum['fecha'].'</td></tr></table>');
	    $mpdf->WriteHTML($css,1);
	    $mpdf->WriteHTML($html,2);
	    if( (int)$aum['por_rubros'] == 1)
	        $mpdf->Output('./listas/'.$aum['fabricante_nombre'].'('.$aum['fecha'].').pdf', 'F');
	    else
	        $mpdf->Output('./listas/'.$aum['fabricante_nombre'].'.pdf', 'F');
	    
	}

	
	$GLOBALS['conf']['pdo'] 		= null;
	
	$GLOBALS['conf']['pdo'] 		= $GLOBALS['toolbox'] ->mysql_conn(	$GLOBALS['conf']['mysql_host'], 
																	$GLOBALS['conf']['mysql_user'], 
																	$GLOBALS['conf']['mysql_pass'],
																	$GLOBALS['conf']['mysql_db']);


	//Ahora generamos la lista completa
	$sql = "    SELECT  prefijo, codigo, sufijo, precio
	            FROM    catalogo_articulos
	            WHERE   habilitado = 1
	            ORDER BY prefijo ASC, codigo ASC, sufijo ASC";

	$stmt = $GLOBALS['conf']['pdo']->prepare($sql);
	$stmt->execute();
	$articulos = $stmt->fetchAll(PDO::FETCH_ASSOC);

	//Ordenamos naturalmente
	usort($articulos, function($a, $b) {
	    //Primero ordenamos por porefijo
	    if($a['prefijo'] != $b['prefijo']) 
	        return strnatcmp($a['prefijo'], $b['prefijo']);

	    //Si el prefijo es igual ordenamos por codigo
	    if($a['prefijo'] == $b['prefijo'])
	        return strnatcmp($a['codigo'], $b['codigo']);

	    //Si el codigo es igual desempata por sufijo
	    if(($a['codigo'] == $b['codigo']) && ($a['prefijo'] == $b['prefijo']))
	        return strnatcmp($a['sufijo'], $b['sufijo']);
	});

	$html = '';
	$art_col = 54;
	$art_hoja = ($art_col*4);
	$articulos_cant = count($articulos);
	$paginas = ceil($articulos_cant/$art_hoja);


	for ($p=0; $p < $paginas; $p++){
	    
	    $icol1 = ($art_hoja*$p);
	    $icol2 = ($art_hoja*$p)+($art_col*1);
	    $icol3 = ($art_hoja*$p)+($art_col*2);
	    $icol4 = ($art_hoja*$p)+($art_col*3);

	    //Cabeceras
	    if(isset($articulos[$icol1]))
	        $html .= '<div class="col1-cod">CODIGO</div><div class="col1-precio">PRECIO</div>';
	    if(isset($articulos[$icol2]))
	        $html .= '<div class="col2-cod">CODIGO</div><div class="col2-precio">PRECIO</div>';
	    if(isset($articulos[$icol3]))
	        $html .= '<div class="col3-cod">CODIGO</div><div class="col3-precio">PRECIO</div>';
	    if(isset($articulos[$icol4]))
	        $html .= '<div class="col4-cod">CODIGO</div><div class="col4-precio">PRECIO</div>';

	    for ($i=0; $i < $art_col; $i++){
	        
	        $icol1 = $i+($art_hoja*$p);
	        $icol2 = $i+($art_hoja*$p)+($art_col*1);
	        $icol3 = $i+($art_hoja*$p)+($art_col*2);
	        $icol4 = $i+($art_hoja*$p)+($art_col*3);

	        $y = ($i*17)+17;
	        $yg = $i*17+17;

	        if(isset($articulos[$icol1]))
	            $html .= '<div class="pref-1" style="top: '. $y .'px;">' . $articulos[$icol1]['prefijo'] . '</div><div class="codigo-1" style="top: '. $y .'px;">' . $articulos[$icol1]['codigo'] . '</div><div class="suf-1" style="top: '. $y .'px;">' . $articulos[$icol1]['sufijo'] . '</div><div class="moneda-1" style="top: '. $y .'px;">$</div><div class="precio-1" style="top: '. $y .'px;">'.number_format($articulos[$icol1]['precio'],2).'</div><div class="guiones-1" style="top: '. $yg .'px;"><img src="./img/guiones.svg"/></div>';
	        
	        if(isset($articulos[$icol2]))
	            $html .= '<div class="pref-2" style="top: '. $y .'px;">' . $articulos[$icol2]['prefijo'] . '</div><div class="codigo-2" style="top: '. $y .'px;">' . $articulos[$icol2]['codigo'] . '</div><div class="suf-2" style="top: '. $y .'px;">' . $articulos[$icol2]['sufijo'] . '</div><div class="moneda-2" style="top: '. $y .'px;">$</div><div class="precio-2" style="top: '. $y .'px;">'.number_format($articulos[$icol2]['precio'],2).'</div><div class="guiones-2" style="top: '. $yg .'px;"><img src="./img/guiones.svg"/></div>';

	        if(isset($articulos[$icol3]))
	            $html .= '<div class="pref-3" style="top: '. $y .'px;">' . $articulos[$icol3]['prefijo'] . '</div><div class="codigo-3" style="top: '. $y .'px;">' . $articulos[$icol3]['codigo'] . '</div><div class="suf-3" style="top: '. $y .'px;">' . $articulos[$icol3]['sufijo'] . '</div><div class="moneda-3" style="top: '. $y .'px;">$</div><div class="precio-3" style="top: '. $y .'px;">'.number_format($articulos[$icol3]['precio'],2).'</div><div class="guiones-3" style="top: '. $yg .'px;"><img src="./img/guiones.svg"/></div>';

	        if(isset($articulos[$icol4]))
	            $html .= '<div class="pref-4" style="top: '. $y .'px;">' . $articulos[$icol4]['prefijo'] . '</div><div class="codigo-4" style="top: '. $y .'px;">' . $articulos[$icol4]['codigo'] . '</div><div class="suf-4" style="top: '. $y .'px;">' . $articulos[$icol4]['sufijo'] . '</div><div class="moneda-4" style="top: '. $y .'px;">$</div><div class="precio-4" style="top: '. $y .'px;">'.number_format($articulos[$icol4]['precio'],2).'</div><div class="guiones-4" style="top: '. $yg .'px;"><img src="./img/guiones.svg"/></div>';
	    }


	    if( !($paginas == ($p+1)) )
	        $html .= '<pagebreak />';
	}

	//Generamos el PDF
	$mpdf=new mPDF('c','A4','','',12,10,25,20,10,10);
	$mpdf->SetDisplayMode('fullpage');
	$mpdf->SetHTMLHeader('<table style="width: 800px;"><tr><td width="268"><img src="fabricantes/logo.png" style="max-height: 50px;"/></td><td width="266" align="center" style="font-size: 18px;"><b>LISTA DE PRECIOS</b></td><td align="right" width="266"></td></tr></table>');
	$mpdf->SetHTMLFooter('<table style="width: 800px;"><tr><td width="268" style="font-size: 13px;">Los precios no incluyen IVA</td><td width="266" align="center" style="font-size: 17px; font-weight: bold;">{PAGENO}</td><td align="right" style="font-size: 13px;" width="266">'.$aum['fecha'].'</td></tr></table>');
	$mpdf->WriteHTML($css,1);
	$mpdf->WriteHTML($html,2);
	$mpdf->Output('./listas/completa.pdf', 'F');

?>