<?php
include_once('toolbox.php');
include_once('classes/class.session.php');

//Creamos la caja de herramientas
$GLOBALS['toolbox'] = new Toolbox();


// Configuracion para el sitio
$GLOBALS['conf']['mysql_host'] 	= 'localhost';
$GLOBALS['conf']['mysql_pass'] 	= '2019Chile';
$GLOBALS['conf']['mysql_db'] 	= 'libertad_sistema';
$GLOBALS['conf']['mysql_user'] 	= 'libertad_usuario';




//Cremos una PDO
$GLOBALS['conf']['pdo'] 		= $GLOBALS['toolbox'] ->mysql_conn(	$GLOBALS['conf']['mysql_host'], 
																	$GLOBALS['conf']['mysql_user'], 
																	$GLOBALS['conf']['mysql_pass'],
																	$GLOBALS['conf']['mysql_db']);


// Email Settings
$GLOBALS['conf']['from_name'] 		= 'Federico Iglesias Colombo'; 						// From email name
$GLOBALS['conf']['from_email'] 		= 'federico.colombo@distribuidoralibertad.com'; 	// From email address
$GLOBALS['conf']['smtp_mode'] 		= 'enabled'; 										// enabled or disabled
$GLOBALS['conf']['smtp_host'] 		= 'mail.distribuidoralibertad.com';
$GLOBALS['conf']['smtp_port'] 		= '587';
$GLOBALS['conf']['smtp_username'] 	= 'federico.colombo@distribuidoralibertad.com';
$GLOBALS['conf']['smtp_password'] 	= 'Dd907435';

//Recuperar contraseña
$GLOBALS['conf']['rem_pass_limit'] 	= 0; 		/*Tiempo minimo entre un envio y el otro. Sirve para proteger contra 
												bombardeo de multiples envios de correos. Deje seteado en 0 para deshabilitar.*/


//Sesiones
$GLOBALS['conf']['session_cookie_name']		= 'session_cookie';	//Nombre del cookie donde se guarda el session_id
$GLOBALS['conf']['session_table_name']		= 'sessions';		//Nombre de la tabla donde se guardan las sesiones			
$GLOBALS['conf']['session_expiration']		= 604800;			//Segundos que dura la session hasta que expira
$GLOBALS['conf']['session_renew_id'] 		= 300;				//Cada cuantos segundo se se renueva la id de session
$GLOBALS['conf']['session_exp_on_close'] 	= FALSE;			//La session se termina cuando el navegador se cierra?
$GLOBALS['conf']['session_sec_cookie'] 		= FALSE;			//Cookie HTTPS
$GLOBALS['conf']['session_check_ip'] 		= null;				/*Seguridad, chequea que se este accediendo 
																desde la misma ip desde donde se creo la session, FALSE para deshabilitar. $_SERVER['REMOTE_ADDR']*/

$GLOBALS['conf']['session_check_agent']		= null;				/*Seguridad, chequea que se este accediendo 
																desde el mismo navegador desde donde se creo la session, FALSE para deshabilitar. $_SERVER['HTTP_USER_AGENT']*/



//Image upload and protection
$GLOBALS['conf']['img_upload_watermark_path'] = "./img/watermark.png";
$GLOBALS['conf']['img_upload_overlay_opacity'] = 50;
$GLOBALS['conf']['img_upload_output_quality'] = 100;
?>