<!doctype html>
<html lang="es">
<head>

  <meta charset="UTF-8"/>
  <meta name="description" content="Catalogo de autopartes en Mar del Plata">
  <meta name="author" content="Federico Iglesias">


  <!-- Titulo -->
  <title>Distribuidora Libertad - Catalogo de productos</title>

  <!-- CSS -->
  <link rel='stylesheet' href='css/index.php'/>
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400,600%7CRoboto:300,400,500" media="all">

  <!--Scripts-->
  <script src="./js/index.php"></script>
 

</head>

<body >

<!-- Header -->
<header id="header">
  <img class="logo_solo" src="./img/logo-solo.png"/>
  <img class="logo" src="./img/logo.png"/>
  <div id="menu_container">

  {$GLOBALS['menuHTML']}

  {$quickSearch}

  </div>
</header>

<div class="contenedor-publicidad">
  <div id="publicidad">
    <div> 
      <img src="./img/publicidad/54.jpg" onclick="openFlyer(0);" />
      <img src="./img/publicidad/52.jpg" onclick="openFlyer(2);" />
      <img src="./img/publicidad/39.jpg" onclick="openFlyer(3);" />
      <img src="./img/publicidad/43.jpg" onclick="openFlyer(4);" />
      <img src="./img/publicidad/49.jpg" onclick="openFlyer(5);" />
      <img src="./img/publicidad/12.jpg" />
    </div>
  </div>


  <div id="boton-cabecera-carro" class="boton-cabecera" onclick="cambiar_modo('carro')" style="margin-right: 15px !important;"><i class="fa fa-shopping-cart" style="font-size: 35px; margin-top: 4px; display: block; margin-bottom: 3px;"></i>PEDIDO<br></div>

  <div id="boton-cabecera-busqueda" class="boton-cabecera" onclick="cambiar_modo('busqueda')"><span class="glyphicon glyphicon-search" aria-hidden="true" style="font-size: 31px; margin-top: 4px; display: block; margin-bottom: 8px;"></span>BUSCAR<br></div>
  
  <div id="boton-cabecera-listas" class="boton-cabecera" onclick="cambiar_modo('listas')"><span class="glyphicon glyphicon-usd" aria-hidden="true" style="font-size: 31px; margin-top: 4px; display: block; margin-bottom: 8px;"></span>PRECIOS<br><span class="badge badge-notify" style="position: relative; top: -70px; right: -29px; background: red; display: none;">3</span></div>
  
  {if="$GLOBALS['permisos']['mensajes_chat']"}
  <div id="boton-cabecera-mensajes" class="boton-cabecera" onclick="cambiar_modo('mensajes')"><span class="glyphicon glyphicon-comment" aria-hidden="true" style="font-size: 31px; margin-top: 4px; display: block; margin-bottom: 8px;"></span>MENSAJES<br><span class="badge badge-notify" style="position: relative; top: -70px; right: -29px; background: red; display: none;">3</span></div>
  {/if}

  <div id="boton-cabecera-configuracion" class="boton-cabecera" onclick="cambiar_modo('configuracion_precios')"><span class="glyphicon glyphicon-cog" aria-hidden="true" style="font-size: 31px; margin-top: 4px; display: block; margin-bottom: 8px;"></span>CONFIG.<br></div>

</div>
  <div class="tags">
    <ul>
      {$breadcrumb}
    </ul>
  </div>
<div id="detalles"></div>
<div id="configuracion"></div>
<div id="mensajes"></div>

<div id="footer">TEL: (0223) 474-1222 (ROTATIVAS) <b>|</b> FAX: (0223) 475-7170 / 0800-333-6590 <b>|</b> PEDIDOS: pedidos@distribuidoralibertad.com  <b>|</b> HORARIO: LUN a VIE de 8 a 19:30 SAB de 8 a 12:30</div>

{$watable}
{$articuloEditar}
{$importadorPrecios}
{$backup}
{$notificaciones}
{$usuarios}
{$cuenta}
{$roles}
{$funciones}
{$clientes}
{$menu}
{$parametrosAPI}
{$aplicaciones}
{$equivalencias}
{$dimensiones}
{$descuentos}
{$vehiculos}
{$busqueda_avanzada}
{$faltantes}
{$imagenes}
{$fabricantes}
{$rubros}
{$pedidos}
{$configuracion}
{$mensajes}
{$sugerencias}
{$horarios}
{$printnode}

<script>

  /* Load User Permissions */
  var perm = <?php echo json_encode($GLOBALS['permisos']) ?>;

</script>

</body>
</html>

