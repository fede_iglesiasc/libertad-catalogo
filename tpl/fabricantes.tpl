<script type="text/javascript">

{if="$GLOBALS['permisos']['fabricantes_editar'] || $GLOBALS['permisos']['fabricantes_crear'] || $GLOBALS['permisos']['fabricantes_listar'] || $GLOBALS['permisos']['fabricantes_eliminar']"}
//Config
var fabricantes_config = {
	modName: 					'fabricantes',
	modLabelSingular: 			'Fabricante',
	modLabelPlural: 			'Fabricantes',
	creaLabel: 					'Creado',
	editaLabel: 				'Editado',
	eliminaLabel: 				'Eliminado',
	nuevoLabel: 				'Nuevo',
	artLabel: 					'El',
	max_height_edita_agrega: 	'300',
	max_height_lista: 			'240',
	campos:[
		{	
			id: 			"nombre",
			label: 			"Nombre",
			tipo: 			"input",
			mostrarEditar: 	true,
			mostrarCrear: 	true
		},
		{	
			id: 			"promocion_prioridad",
			label: 			"Prioridad al Mostrar en Promoción",
			tipo: 			"select2",
			mostrarEditar: 	true,
			mostrarCrear: 	true,
			config: 		{
								minimumInputLength: 0,
								placeholder: $(this).attr('placeholder'),
								allowClear: false,
								width: 'resolve',
								id: function(e) { return e.id; },
								formatNoMatches: function() { return 'Sin resultados'; },
								formatSearching: function(){ return "Buscando..."; },
								data: [	{id:'11',nombre:'Aleatorio'}, 
										{id:'0',nombre:'No mostrar'}, 
										{id:'1',nombre:'1'}, 
										{id:'2',nombre:'2'}, 
										{id:'3',nombre:'3'}, 
										{id:'4',nombre:'4'}, 
										{id:'5',nombre:'5'}, 
										{id:'6',nombre:'6'}, 
										{id:'7',nombre:'7'}, 
										{id:'8',nombre:'8'}, 
										{id:'9',nombre:'9'}, 
										{id:'10',nombre:'10'}],
								formatInputTooShort: function () {
									return "";
								},
								formatResult: function(item) {
									return item.nombre;
								},
								formatSelection: function(item) { 
									return item.nombre;
								},
								initSelection : function (element, callback) {
									var elementText = $(element).attr('data-init-text');
								}
							}
		},
		{	
			id: 			"descripcion",
			label: 			"Descripción",
			tipo: 			"textarea",
			mostrarEditar: 	true,
			mostrarCrear: 	true
		},
	]
};
{/if}

{if="$GLOBALS['permisos']['fabricantes_listar']"}
//Lista
function fabricantes_listar(){

	//Creamos el popup
	abrir_modal(fabricantes_config.modName, 550, false);

	//Mostramos la ventana
	$('#'+fabricantes_config.modName+'Modal').modal({show:true});

	//Titulo...
	$('#'+fabricantes_config.modName+'Modal_titulo').html(fabricantes_config.modLabelPlural);

	//Cargando...
	$('#'+fabricantes_config.modName+'Modal_container').html( cargando() );

	//Footer...
	$('#'+fabricantes_config.modName+'Modal_footer').html('');

	//Traemos los datos de la API
	apix(fabricantes_config.modName,'listar',{}, {
		ok: function(json){


			var tabla = generaTabla({
							id_tabla: fabricantes_config.modName,
							items: json.result,
							keys_busqueda: ['nombre'],
							key_nombre: null,
							max_height: fabricantes_config.max_height_lista,
							funcion_activado: function(el){
								//Seleccionamos el activo anterior
								var elActivo = $('#fabricantes_lista').find('.active_');
								var id = $(el).data('id');

								{if="$GLOBALS['permisos']['fabricantes_eliminar']"}
								//Borrar
								var boton_borrar = '';
								{/if}

								{if="$GLOBALS['permisos']['fabricantes_editar']"}
								//Editar
								var boton_editar = '';
								{/if}

								{if="$GLOBALS['permisos']['fabricantes_logo']"}
								//Editar
								var boton_logo = '';
								{/if}

								{if="$GLOBALS['permisos']['fabricantes_eliminar']"}
								//Boton Borrar
								boton_borrar = '<button class="btn btn-xs btn-warning" style="margin: 0px 0px 8px 5px; width: 24px; height: 24px;" data-toggle="tooltip" title="Borrar" onclick=" confirma(\'Eliminar '+fabricantes_config.modLabelSingular+'\',\'Si usted elimina el '+fabricantes_config.modLabelSingular+' se perderán sus relaciones con los artículos de forma permanente.\',\'exclamacion\',\'danger\',\''+fabricantes_config.modLabelPlural.toLowerCase()+'_elimina(\\\''+$(el).data('id')+'\\\')\');"><i class="fa fa-remove"></i></button>';
								{/if}

								{if="$GLOBALS['permisos']['fabricantes_editar']"}
								//Boton editar
								boton_editar = '<button class="btn btn-xs btn-primary" style="margin: 0px 0px 8px 5px; width: 24px; height: 24px;" data-toggle="tooltip" title="Editar" onclick="'+fabricantes_config.modLabelPlural.toLowerCase()+'_edita_agrega(\''+$(el).data('id')+'\');"><i class="fa fa-pencil"></i></button>';
								{/if}

								{if="$GLOBALS['permisos']['fabricantes_logo']"}
								//Boton editar
								boton_logo = '<button class="btn btn-xs btn-primary" style="margin: 0px 0px 8px 5px; width: 24px; height: 24px;" data-toggle="tooltip" data-html="true" title="Cambiar Logo <br>(Archivo SVG)" onclick="$(\'#fabricantes_logo_input\').data(\'id\','+$(el).data('id')+').click();"><i class="fa fa-picture-o"></i></button>';
								{/if}

								var html = '<img src="./fabricantes/'+$(el).data('logo')+'" class="fabricante_logo" style="width: 40%; margin: 15px auto;"> <br/><small><p>'+$(el).data('descripcion')+'</p></small> <div class="descripcion"></div><div class="pull-right descripcion" style="width: 120px; text-align: right; position: absolute; top: 8px; right: 10px;">';

								{if="$GLOBALS['permisos']['fabricantes_logo']"}
								//Boton borrrar
								html += boton_logo;
								{/if}

								{if="$GLOBALS['permisos']['fabricantes_editar']"}
								//Boton borrrar
								html += boton_editar;
								{/if}

								{if="$GLOBALS['permisos']['fabricantes_eliminar']"}
								//Boton borrrar
								html += boton_borrar;
								{/if}

								//Cerramos el div
								html += '</div>';

								//Agregamos los datos
								$(el).html(html);

								
								$('img.fabricante_logo',el).load(function() {
									if( $('img.fabricante_logo',el).height() > 100 )
										$('img.fabricante_logo',el).css({height: '80px', width: 'auto'});
								});


							},
							funcion_desactivado: function(el){
								//HTML
								$(el).html('<div class="thumbnail80_lista"><img src="./fabricantes/'+$(el).data('logo')+'"></div> <b>'+ $(el).data('nombre') +'</b>');
							}
						});

			//Agregamos los tooltips
			tabla.tooltip({selector: '[data-toggle="tooltip"]', placement: 'left', animation: false});

			//Generamos el listado
			$('#'+fabricantes_config.modName+'Modal_container').html('');
			$('#'+fabricantes_config.modName+'Modal_container').append(tabla);

			//Agregamos el campo input
			$('#'+fabricantes_config.modName+'Modal_container').append('<input type="file" id="fabricantes_logo_input" name="archivo" style="display: none;" onchange="fabricantes_logo();">');

			//Agregamos scrollbars
			$('#'+fabricantes_config.modName+'Modal_container .scrollable').mCustomScrollbar({theme: 'minimal-dark',scrollInertia: 100, mouseWheel:{ scrollAmount: 70 }});

			{if="$GLOBALS['permisos']['fabricantes_crear']"}
			//Boton Agregar
			$('#'+fabricantes_config.modName+'Modal_footer').append('<button type="button" class="btn btn-primary" style="display: block; width: 100%; margin: 10px auto;" onclick="'+fabricantes_config.modName+'_edita_agrega();">'+fabricantes_config.nuevoLabel+' '+fabricantes_config.modLabelSingular+'</button>');
			{/if}

			//Boton Cancelar
			$('#'+fabricantes_config.modName+'Modal_footer').append('<button type="button" data-dismiss="modal" class="btn btn-warning" style="display: block; width: 100%; margin: 10px auto;">Cerrar</button>');
		}
	});
}
{/if}

{if="$GLOBALS['permisos']['fabricantes_editar'] || $GLOBALS['permisos']['fabricantes_crear']"}
//Edita/Agrega
function fabricantes_edita_agrega(id){

	//Si estamos agregando...
	if(typeof id === 'undefined') var agrega = true;
	else var agrega = false;

	//Titulo...
	if(agrega) $('#'+fabricantes_config.modName+'Modal_titulo').html(fabricantes_config.nuevoLabel+' '+fabricantes_config.modLabelSingular);
	else $('#'+fabricantes_config.modName+'Modal_titulo').html('Editar '+fabricantes_config.modLabelSingular);

	//Cargando...
	$('#'+fabricantes_config.modName+'Modal_container').html(cargando());

	//Footer...
	$('#'+fabricantes_config.modName+'Modal_footer').html('');

	var contenido = $('<div class="scrollable" style="max-height: '+fabricantes_config.max_height_edita_agrega+'px; overflow-y: hidden;"></div>');

	//Agregamos todos los campos
	for(var k in fabricantes_config.campos){
		var campo = fabricantes_config.campos[k];
		if( (campo.mostrarEditar && !agrega) || (campo.mostrarCrear && agrega) ){
			contenido.append( genera_campo(fabricantes_config.campos[k], fabricantes_config.modName ,true) );
		}
	}

	//Agregamos el contenido y los botones
	//si estamos creando un registro nuevo
	if(agrega){
		//Agregamos el contenido
		$('#'+fabricantes_config.modName+'Modal_container').html(contenido);

		//Agregamos scrollbars
		$('#'+fabricantes_config.modName+'Modal_container .scrollable').mCustomScrollbar({theme: 'minimal-dark',scrollInertia: 100, mouseWheel:{ scrollAmount: 70 }});

		//Agregamos los botones al footer
		$('#'+fabricantes_config.modName+'Modal_footer').append('<button type="button" class="btn btn-primary" style="display: block; width: 100%; margin: 10px auto;" onclick="'+fabricantes_config.modLabelPlural.toLowerCase()+'_agrega_confirm();">Crear '+fabricantes_config.modLabelSingular+'</button>');
		$('#'+fabricantes_config.modName+'Modal_footer').append('<button type="button" class="btn btn-warning" style="display: block; width: 100%; margin: 10px auto;" onclick="'+fabricantes_config.modLabelPlural.toLowerCase()+'_listar();">Volver</button>');
	}


	//Si editamos
	if(!agrega){
		var data = {};
		data['fabricante'] = id;

		apix(fabricantes_config.modName,'info',data, {
			ok: function(json){

				//Titulo
				$('#'+fabricantes_config.modName+'Modal_titulo').html('Editar '+fabricantes_config.modLabelSingular+ ' <b>'+json.result.nombre+'</b>');

		    	//Llenamos los datos
		    	for(var k in fabricantes_config.campos){
		    		var campo = fabricantes_config.campos[k];
		    		if( !(typeof(json.result[ campo.id ]) == "undefined") && !(json.result[ campo.id ] === null) ){
		    			console.log('Campo: ' + fabricantes_config.modName+'_'+campo.id + ' - Valor: '+json.result[ campo.id ] );
		    			$('#'+fabricantes_config.modName+'_'+campo.id,contenido).val_(json.result[ campo.id ]);
		    		}
		    	}

		    	//Asignamos el contenido al popup
				$('#'+fabricantes_config.modName+'Modal_container').html(contenido);

				//Agregamos scrollbars
				$('#'+fabricantes_config.modName+'Modal_container .scrollable').mCustomScrollbar({theme: 'minimal-dark',scrollInertia: 100, mouseWheel:{ scrollAmount: 70 }});

				//Agregamos los botones al footer
				$('#'+fabricantes_config.modName+'Modal_footer').append('<button type="button" class="btn btn-primary" style="display: block; width: 100%; margin: 10px auto;" onclick="'+fabricantes_config.modLabelPlural.toLowerCase()+'_edita_confirm(\''+id+'\');">Editar '+fabricantes_config.modLabelSingular+'</button>');
				$('#'+fabricantes_config.modName+'Modal_footer').append('<button type="button" class="btn btn-warning" style="display: block; width: 100%; margin: 10px auto;" onclick="'+fabricantes_config.modLabelPlural.toLowerCase()+'_listar();">Volver</button>');
			}
		});
	}
}
{/if}

{if="$GLOBALS['permisos']['fabricantes_editar']"}
//Edita
function fabricantes_edita_confirm(id){

	//Data
	var data = {};
	data['fabricante'] = id;

	//Agregamos todos los campos
	for(var k in fabricantes_config.campos){
		var campo = fabricantes_config.campos[k];
		if(campo.mostrarEditar)
			data[campo.id] = $('#'+fabricantes_config.modName+'_'+campo.id).val();
	}

	apix(fabricantes_config.modName,'editar',data, {
		ok: function(json){
			notifica(	fabricantes_config.modLabelSingular+' '+fabricantes_config.editaLabel.toLowerCase(),
						fabricantes_config.artLabel+' '+fabricantes_config.modLabelSingular+' se editó con éxito',
						'confirmacion','success');
			fabricantes_listar();
		}
	});
}
{/if}

{if="$GLOBALS['permisos']['fabricantes_crear']"}
//Agrega
function fabricantes_agrega_confirm(){

	//Data
	var data = {};

	//Agregamos todos los campos
	for(var k in fabricantes_config.campos){
		var campo = fabricantes_config.campos[k];
		if(campo.mostrarEditar)
			data[campo.id] = $('#'+fabricantes_config.modName+'_'+campo.id).val();
	}

	apix(fabricantes_config.modName,'agregar',data, {
		ok: function(json){
			//Notifica
			notifica(	fabricantes_config.modLabelSingular+' '+fabricantes_config.creaLabel.toLowerCase(),
						fabricantes_config.artLabel+' '+fabricantes_config.modLabelSingular+' se creó con éxito',
						'confirmacion',
						'success'
					);
			//Update
			fabricantes_listar();
		}
	});
}
{/if}

{if="$GLOBALS['permisos']['fabricantes_eliminar']"}
//Elimina
function fabricantes_elimina(id){

	//Data array
	var data = {};
	data['fabricante'] = id;
	
	apix(fabricantes_config.modName,'eliminar',data, {
		ok: function(json){
	    	//Notifica
	    	notifica(	fabricantes_config.modLabelSingular+' '+fabricantes_config.eliminaLabel,
	    				fabricantes_config.artLabel+' '+fabricantes_config.modLabelSingular+' se eliminó con éxito',
	    				'informacion',
	    				'info'
	    			);
	    	//Update
	    	fabricantes_listar();
		}
	});
}
{/if}

{if="$GLOBALS['permisos']['fabricantes_logo']"}
//Elimina
function fabricantes_logo(){

    if($('#fabricantes_logo_input').val() != '' ){

      //Recolectamos las variables
      var file = $('#fabricantes_logo_input').prop('files')[0];

      //Datos del form
      var data = new FormData();
      data.append('archivo', file);
      data.append('fabricante', $('#fabricantes_logo_input').data('id'));

      //API
      apix('fabricantes','logo',data, {
        file: true,
        ok: function(json){
			fabricantes_listar();
        }
      });
    }

    //Reseteamos
    $('#fabricantes_logo_input').removeData('id');
    $('#fabricantes_logo_input').val('');

}
{/if}

</script>