{if="$GLOBALS['permisos']['backup']"}

<script type="text/javascript">

// Abre cuadro de dialogo para generar
// Backup de datos en el servidor
function backup(){

  //Creamos el popup
  abrir_modal('backup', 400, false);

  //Mostramos la ventana
  $('#backupModal').modal({show:true});

  //Titulo...
  $('#backupModal_titulo').html('Crear Backup');

  //Cargando...
  $('#backupModal_container').html('Esta por crear un respaldo de todos los datos del sistema. </br></br>Tenga en cuenta que: </br></br><ul><li>Para poder ejecutar esta herramienta, el servidor debe tener habilitada la función "system" de PHP.</li><li>El proceso puede demorar varios minutos.</li></ul>');

  //Boton Confirmar cambios
  $('#backupModal_footer').append('<button type="button" class="btn btn-primary" style="display: block; width: 100%; margin: 10px auto;" onclick="backup_confirm();">Crear Backup</button>');

  //Append Cancel Button
  $('#backupModal_footer').append('<button type="button" data-dismiss="modal" class="btn btn-warning" style="display: block; width: 100%; margin: 10px auto;">No crear Backup</button>');
}

// Da la orden al servidor para crear el Backup
// En caso de error informa
function backup_confirm(){
  
  //Indicamos al usuario que el sistema ahora va a estar ocupado haciendo el backup
  $('#backupModal_titulo').html('Creando Backup...');
  $('#backupModal_container').html('<div style="display: block; text-align: center;"><br/><img src="./img/procesando.gif" width="30" height="30"></div>');
  $('#backupModal_footer').html('');


  //Llamada a la API
  apix('backup','backup_confirm',{}, {
    ok: function(json){

      //Titulo...
      $('#backupModal_titulo').html('El Backup se creó exitosamente');

      //Cargando...
      $('#backupModal_container').html('Se creo el backup con éxito, podra encontrarlo en la carpeta de respaldos.');

      //Boton Confirmar cambios
      $('#backupModal_footer').append('<button type="button" data-dismiss="modal" class="btn btn-primary" style="display: block; width: 100%; margin: 10px auto;">OK</button>');
    }
  });
}

</script>

{/if}