<script type="text/javascript">

{if="$GLOBALS['permisos']['articulos_edicion_aplicaciones']"}

var moduloarticuloEditarAplicaciones_lista;
var articuloEditarAplicacion_anioDesde;
var articuloEditarAplicacion_anioHasta;

//Abrir Aplicaciones
function aplicaciones_listar(id){

  //Creamos el popup
  abrir_modal('articuloEditar', 630, false);

  //Mostramos la ventana
  $('#articuloEditarModal').modal({show:true});

  //Titulo...
  $('#articuloEditarModal_titulo').html('Cargando');

  //Contenedor aplicaciones...
  $('#articuloEditarModal_container').html('<div style="display: block; text-align: center;"><br/><img src="./img/procesando.gif" width="30" height="30"></div>');  


  //Llamada a la API
  var data = {};
  data['articulo'] = id;
  apix('aplicaciones','listar',data, {
    ok: function(json){
    
      //Cargamos lista de aplicaciones
      moduloarticuloEditarAplicaciones_lista = json;

      //Titulo...
      $('#articuloEditarModal_titulo').html('Aplicaciones del Artículo');

      //Contenedor aplicaciones...
      $('#articuloEditarModal_container').html('<div style="height: 60px;"><div style="width: 300px; float: left;"><input type="text" id="articuloEditarAplicaciones_vehiculo" style="width: 100%;"></div><div style="width: 85px; float: left; margin-left: 10px;"><input type="text" id="articuloEditarAplicaciones_vehiculo_anio_inicio" style="width: 100%;"></div><div style="width: 85px; float: left; margin-left: 10px;"><input type="text" id="articuloEditarAplicaciones_vehiculo_anio_final" style="width: 100%;"></div><button type="button" id="articuloEditarAplicaciones_vehiculo_agregar" class="btn btn-success" style="margin-left: 10px;" onclick="aplicaciones_agregar();">Agregar</button></div><ul class="list-group" id="articuloEditarAplicaciones_lista" style="overflow-y: auto; padding: 0px; box-shadow: none; margin-bottom: 0px;"></ul>');



      //PREFIJO SELECT BOX
      $('#articuloEditarAplicaciones_vehiculo').select2({
          minimumInputLength: 0,
          placeholder: 'Ingrese el Vehículo',
          allowClear: true,
          width: 'resolve',
          id: function(e) { return e.nombre; },
          formatNoMatches: function() { return 'Sin resultados'; },
          formatSearching: function(){ return "Buscando..."; },
          ajax: {
              url: "api.php",
              dataType: 'json',
              quietMillis: 200,
              data: function(term, page) {
                  return {
                      module: 'aplicaciones',
                      run: 'listar_vehiculos',
                      q: term,
                      p: page
                  };
              },
              results: function(data, page ) {
                  //If session expire reload page...
                  if(!data.logued) location.reload();

                  var more = (page * 40) < data.result.total; // whether or not there are more results available
     
                  // notice we return the value of more so Select2 knows if more results can be loaded
                  return {results: data.result.items, more: more};
              }
          },
          formatInputTooShort: function () {
                    return "";
                },  
          formatResult: function(item) {
              
              var label = '<i class="badge" style="padding: 4px 6px; float: none; border-radius: 2px; background-color: #e6e6e6; margin-left: 7px; color: #444444; font-size: 10px; border: solid 1px #dadada;">';
              
              if (item.nombre.indexOf("[Cualquier vehículo]") >= 0)
                item.nombre = item.nombre.replace(" [Cualquier vehículo]", "") + label + 'Cualquier vehículo</i>';

              if (item.nombre.indexOf("[Todos los modelos]") >= 0)
                item.nombre = item.nombre.replace(" [Todos los modelos]", "") + label + 'Todos los modelos</i>';

              if (item.nombre.indexOf("[Todas las versiones]") >= 0)
                item.nombre = item.nombre.replace(" [Todas las versiones]", "") + label + 'Todas las versiones</i>';

              return "<div class='select2-user-result'>" + item.nombre + "</div>"; 
          },
          formatSelection: function(item) { 
              return item.nombre; 
          },
          initSelection : function (element, callback) {
              var elementText = $(element).attr('data-init-text');
          }
      }).on("change", function(e) { 

        //Hacemos foco en el siguiente campo
        $('#articuloEditarAplicaciones_vehiculo_anio_inicio').select2('focus');

      }).on("select2-removed", function(e) {
        //advSearchsetSessionVar('advSearch_'+field, '-1'); 
      });

      //Boton Agregar
      $('#s2id_articuloEditarAplicaciones_vehiculo .select2-drop').append('<div style="text-align: center; padding: 10px; background: #F5F5F5;"><button type="button" class="btn btn-info btn-sm" style="width: 100%;" onclick="alert(\'En desarrollo\');">Haga click aquí para agregar un Vehículo</button></div>');

      //Focus en auto
      setTimeout(function(){ $('#articuloEditarAplicaciones_vehiculo').select2('focus'); }, 500);
      




      //PREFIJO SELECT BOX
      $('#articuloEditarAplicaciones_vehiculo_anio_inicio').select2({
          minimumInputLength: 0,
          placeholder: 'Desde',
          allowClear: true,
          width: 'resolve',
          id: function(e) { return e.id; },
          formatNoMatches: function() { return 'Sin resultados'; },
          formatSearching: function(){ return "Buscando..."; },
          data: function(){ return {results: articuloEditarAplicacion_anioDesde}; },
          formatInputTooShort: function () {
                    return "";
                },  
          formatResult: function(item) {
              return "<div class='select2-user-result'>" + item.text + "</div>"; 
          },
          formatSelection: function(item) { 
              return item.text; 
          },
          initSelection : function (element, callback) {
              var elementText = $(element).attr('data-init-text');
          },
          matcher: function(term, text, opt) {
             if(term.length == 2) return opt.search.toUpperCase().indexOf(term.toUpperCase())>=0;

             else return text.toUpperCase().indexOf(term.toUpperCase())>=0;
          }
      }).on("change", function(e) { 

        //calculamos nuevos valores para año final
        aplicaciones_recalcAnios();
        
        //Hacemos foco en el siguiente campo
        $('#articuloEditarAplicaciones_vehiculo_anio_final').select2('focus');

      }).on("select2-removed", function(e) {
        //advSearchsetSessionVar('advSearch_'+field, '-1'); 
      });


      //PREFIJO SELECT BOX
      $('#articuloEditarAplicaciones_vehiculo_anio_final').select2({
          minimumInputLength: 0,
          placeholder: 'Hasta',
          allowClear: true,
          width: 'resolve',
          id: function(e) { return e.id; },
          formatNoMatches: function() { return 'Sin resultados'; },
          formatSearching: function(){ return "Buscando..."; },
          initSelection: function(element, callback){
            callback({id: '-1', text: '--', search: '--'});
          },
          data: function(){ return {results: articuloEditarAplicacion_anioHasta}; },
          formatInputTooShort: function () {
                    return "";
                },  
          formatResult: function(item) {
              return "<div class='select2-user-result'>" + item.text + "</div>"; 
          },
          formatSelection: function(item) { 
              return item.text; 
          },
          initSelection : function (element, callback) {
              var elementText = $(element).attr('data-init-text');
          },
          matcher: function(term, text, opt) {
             if(term.length == 2) return opt.search.toUpperCase().indexOf(term.toUpperCase())>=0;

             else return text.toUpperCase().indexOf(term.toUpperCase())>=0;
          }
      }).on("change", function(e) {
        
        //calculamos nuevos valores para año final
        aplicaciones_recalcAnios();
        
        //Hacemos foco en el siguiente campo
        $('#articuloEditarAplicaciones_vehiculo_agregar').focus();

      }).on("select2-removed", function(e) {
      });

      

      //Boton Insertar imagen
      $('#articuloEditarModal_footer').append('<button type="button" id="button_confirm_upload_pic" class="btn btn-primary" style="display: block; width: 100%; margin: 10px auto;" onclick="aplicaciones_confirmar('+id+');">Guardar cambios</button>');

      //Append Cancel Button
      $('#articuloEditarModal_footer').append('<button type="button" data-dismiss="modal" class="btn btn-warning" style="display: block; width: 100%; margin: 10px auto;">Descartar cambios</button>');

      //Creamos grupo
      if(json.result.length == 0)
        $('#articuloEditarAplicaciones_lista').append('<li class="list-group-item" data-tipo="no-app" style="padding: 8px 12px; text-align: center; color: darkgrey; border: none;">Este artículo no contiene aplicaciones</li>');
      
      //Auto nombre
      var auto;

      //Agregamos las aplicaciones existentes
      for(var k in json.result){
        aplicaciones_addAppList( json.result[k].vehiculo_marca_id, json.result[k].vehiculo_marca_label, json.result[k].vehiculo_modelo_id, json.result[k].vehiculo_modelo_label, json.result[k].vehiculo_version_id, json.result[k].vehiculo_version_label, json.result[k].anio_inicio, json.result[k].anio_final);
      }

      //Creamos opciones de años
      aplicaciones_recalcAnios();
    }
  });
}

//Borrar Aplicacion
function aplicaciones_borrar(e){
  var r = confirm("¿Esta seguro que desea quitar esta Aplicación? \n\nAunque la elimine, siempre puede hacer click en 'Descartar cambios' para mantener la lista de aplicaciones original.");
  if (r == true) {
      //Eliminamos el item
      $(e).parent().parent().remove();
      
      //Creamos texto 'Este artículo no contiene aplicaciones'
      if( $('#articuloEditarAplicaciones_lista li').length == 0 )
        $('#articuloEditarAplicaciones_lista').html('<li class="list-group-item" data-tipo="no-app" style="padding: 8px 12px; text-align: center; color: darkgrey; border: none;">Este artículo no contiene aplicaciones</li>');

      //Recalculamos alto de la lista
      articuloEditarAplicacion_recalcAlt();
  }
}

//Actualiza años
function aplicaciones_recalcAnios(){

  //selected item
  var desdeSelected = $('#articuloEditarAplicaciones_vehiculo_anio_inicio').select2('data');
  var hastaSelected = $('#articuloEditarAplicaciones_vehiculo_anio_final').select2('data');

  articuloEditarAplicacion_anioDesde = [{id: '-1', text: '--', search: '--'}];
  articuloEditarAplicacion_anioHasta = [{id: '-1', text: '--', search: '--'}];

  var anio;
  if( (desdeSelected == null) || (desdeSelected.id == -1) ) anio = 1920;
  else anio = desdeSelected.id;

  //hasta no puede ser menor que desde
  for(var i = anio+1; i<2017; i++)
    articuloEditarAplicacion_anioHasta.push({id: i, text: i, search: i.toString().substring(2)});

  if((hastaSelected == null) || (hastaSelected.id == -1)) anio = 2018;
  else anio = hastaSelected.id;

  //desde no puede ser mayor que hasta
  for(var i = 1920; i<anio; i++)
    articuloEditarAplicacion_anioDesde.push({id: i, text: i, search: i.toString().substring(2)});
}

//Click al boton agregar
function aplicaciones_agregar(){
  //Creamos el elemento LI dentro del UL aplicaciones
  //Ordenamos alfabeticamente
  //reseteamos los valores de los campos
  //hacemos foco en el vehiculo nuevamente

  //Vehiculo seleccionado
  var vehicSel = $('#articuloEditarAplicaciones_vehiculo').select2('data');
  var anioDesde = $('#articuloEditarAplicaciones_vehiculo_anio_inicio').select2('data');
  var anioHasta = $('#articuloEditarAplicaciones_vehiculo_anio_final').select2('data');

  //Si dejaron los años en blanco...
  if(anioDesde == null) anioDesde = {id: -1};
  if(anioHasta == null) anioHasta = {id: -1};

  //Nos null en string los pasamos a valor
  if(vehicSel.marca_id == 'null') vehicSel.marca_id = null;
  if(vehicSel.modelo_id == 'null') vehicSel.modelo_id = null;
  if(vehicSel.version_id == 'null') vehicSel.version_id = null;


  //Agregar
  aplicaciones_addAppList( vehicSel.marca_id, vehicSel.marca_label, vehicSel.modelo_id, vehicSel.modelo_label, vehicSel.version_id, vehicSel.version_label, anioDesde.id, anioHasta.id);
}

//Agrega una aplicacion a la lista de aplicaciones del articulo
function aplicaciones_addAppList( marca_id, marca_label, modelo_id, modelo_label, version_id, version_label, anio_desde, anio_hasta){

  //Bandera para saber si agregamos o no
  var agregarBandera = true;

  //Recorremos las aplicaciones existentes para chequear...
  $('#articuloEditarAplicaciones_lista li[data-tipo=app]').each(function(){

    
    //Recuperamos datos de la app
    var liMarca = (($(this).data("marca") != null) ? parseInt($(this).data("marca")) : null);
    var liModelo = (($(this).data("modelo") != null) ? parseInt($(this).data("modelo")) : null);
    var liVersion = (($(this).data("version") != null) ? parseInt($(this).data("version")) : null);
    var liDesde = $(this).data("anio_desde");
    var liHasta = $(this).data("anio_hasta");

 
    //Si se esta tratando de agregar una aplicacion Universal
    if((marca_id == null) && (modelo_id == null) && (version_id == null))
        if( ( $('#articuloEditarAplicaciones_lista li[data-tipo=app]').length > 1) || ( $('#articuloEditarAplicaciones_lista li[data-tipo=app][data-marca!=null]').length == 1)){

        var r = confirm("Esta tratando de indicar que este artículo es Universal (aplica a cualquier vehículo), pero existen aplicaciones ya definidas. \n\nSi hace click en Aceptar, todos las aplicaciones anteriores se eliminarán y en lugar de estas se agregará 'Universal (Todos los modelos)'.");
        
        //Si confirma
        if(r == true){
          //Eliminamos los items de la marca
          $('#articuloEditarAplicaciones_lista li').each(function(){
            $(this).remove();
          });

        //Agregamos la aplicacion
        agregarBandera = true;

        }else{
          //No agregamos la aplicacion
          agregarBandera = false;
        }

        //Cortamos loop
        return false;
    }

    
    //Si se esta tratando de agregar una Marca (Todos los modelos) y ya existen modelos de esta marca
    if((marca_id != null) && (modelo_id == null) && (version_id == null) && (liMarca == marca_id) && (liModelo != null)){
      //
      var r = confirm("Esta tratando de agregar la marca "+marca_label+" (y todos sus modelos), pero entre las aplicaciones ya existen modelos de "+marca_label+".\n\nSi hace click en Aceptar, todos los modelos de "+marca_label+" se eliminarán y en lugar de estos se agregará '"+marca_label+" (Todos los modelos)'.");
      
      //Si confirma
      if(r == true){
        //Eliminamos los items de la marca
        $('#articuloEditarAplicaciones_lista li').each(function(){
          if( $(this).data('marca') == liMarca) 
            $(this).remove();
        });
        
        //Agregamos la aplicacion
        agregarBandera = true;

        }else{
          //No agregamos la aplicacion
          agregarBandera = false;
        }

        //Cortamos loop
        return false;
    }

    //che me mando msg tu cuñada, alterada porque te hice llegar lo que ella puso. la tuve que terminar bloqueando porque me mandaba mensajes kilometricos, 

    //Si se esta tratando de agregar un Modelo (Todos las versiones) y ya existen versiones de este mismo modelo.
    if((marca_id != null) && (modelo_id != null) && (version_id == null) && (liMarca == marca_id) && (liModelo == modelo_id) && (liVersion != null)){
      
      //
      var r = confirm("Esta tratando de agregar el modelo "+marca_label+" "+modelo_label+" (y todas sus versiones), pero entre las aplicaciones ya existen versiones del modelo "+marca_label+" "+modelo_label+".\n\nSi hace click en Aceptar, todas las versiones de "+marca_label+" "+modelo_label+" se eliminarán y en lugar de estas se agregará '"+marca_label+" "+modelo_label+" (Todas las versiones)'.");
      
      //Si confirma
      if(r == true){
        //Eliminamos los items de la marca
        $('#articuloEditarAplicaciones_lista li').each(function(){
          if( ($(this).data('marca') == liMarca) && ($(this).data('modelo') == liModelo)) 
            $(this).remove();
        });

        //Agregamos la aplicacion
        agregarBandera = true;

        }else{
          //No agregamos la aplicacion
          agregarBandera = false;
        }

        //Cortamos loop
        return false;
    }


    //Si la aplicacion ya existe 
    if((marca_id == liMarca) && (modelo_id == liModelo) && (version_id == liVersion)){
      //No agregamos la aplicacion
      agregarBandera = false;
    }
  });


  if(agregarBandera){

    // Si el articulo es universal
    if((marca_id == null) && (modelo_id == null) && (version_id == null))
        auto = 'Universal (Cualquier vehículo)';

    // Si el articulo es para una marca completa de autóviles
    if( (marca_id != null) && (modelo_id == null) && (version_id == null))
        auto = marca_label + ' (Todos los modelos)';

    // Si el articulo es para una marca completa de autóviles
    if( (marca_id != null) && (modelo_id != null)){

        // Nombre del modelo
        auto = marca_label + ' ' + modelo_label;

        //Agregamos la version
        if(version_id != null) auto +=  ' ' +  version_label;
    }

    //Creamos el año
    anio_desde = parseInt(anio_desde);
    anio_hasta = parseInt(anio_hasta);

    var anio = '';
    if((anio_desde == -1) && (anio_hasta == -1)) anio = 'TODOS';
    if((anio_desde == -1) && (anio_hasta != -1)) anio = '--/' + anio_hasta.toString().substring(2);
    if((anio_desde != -1) && (anio_hasta == -1)) anio = anio_desde.toString().substring(2) + '/--';
    if((anio_desde != -1) && (anio_hasta != -1)) anio = anio_desde.toString().substring(2) + '/' + anio_hasta.toString().substring(2);

    //Creamos el item
    var li = '<li class="list-group-item" style="padding: 8px 12px;" data-tipo="app" data-marca="'+marca_id+'" data-modelo="'+modelo_id+'" data-version="'+version_id+'" data-anio_inicio="'+anio_desde+'" data-anio_final="'+anio_hasta+'">'+auto;

    //Si es universal no usamos años
    if(marca_id != null)

    //Agregamos el año si no es universal..
    li += '<i class="badge" style="padding: 7px; float: none; border-radius: 3px; background-color: #e6e6e6; margin-left: 7px; color: black;">' + anio;

    //Agregamos boton borrar
    li += '</i><span class="pull-right"><button class="btn btn-xs btn-warning" onclick="aplicaciones_borrar(this);"><span class="glyphicon glyphicon-trash"></span></button></span></li>';
    
    //Aseguremonos de quitar el aviso 'Este artículo no contiene aplicaciones'
    $('#articuloEditarAplicaciones_lista li[data-tipo=no-app]').remove();

    //Si la aplicacion no es la universal y esta existe anteriormente, la quitamos...
    if((marca_id != null))
      $('#articuloEditarAplicaciones_lista li[data-tipo=app][data-marca=null][data-modelo=null][data-version=null]').remove();

    //Agregamos el Item
    $('#articuloEditarAplicaciones_lista').append(li);

    //Ordenamos la lista
    articuloEditar_ordenarLista('articuloEditarAplicaciones_lista')

    //Recalculamos alto de la lista
    articuloEditar_recalcAlt('articuloEditarAplicaciones_lista');

    //Vaciamos los campos
    $('#articuloEditarAplicaciones_vehiculo').select2('focus');
    $('#articuloEditarAplicaciones_vehiculo').select2('data',null);
    $('#articuloEditarAplicaciones_vehiculo_anio_inicio').select2('data',null);
    $('#articuloEditarAplicaciones_vehiculo_anio_final').select2('data',null);
  }
}

//Confirmamos los cambios en el servidor
function aplicaciones_confirmar(id){
  
  //Array de aplicaciones
  var aplicaciones = [];

  $('#articuloEditarAplicaciones_lista li[data-tipo=app]').each(function(){

    //Recuperamos datos de la app
    var liMarca = (($(this).data("marca") != null) ? parseInt($(this).data("marca")) : null);
    var liModelo = (($(this).data("modelo") != null) ? parseInt($(this).data("modelo")) : null);
    var liVersion = (($(this).data("version") != null) ? parseInt($(this).data("version")) : null);
    var liDesde = $(this).data("anio_inicio");
    var liHasta = $(this).data("anio_final");

    //Agregamos la aplicacion
    aplicaciones.push({marca_id: liMarca, modelo_id: liModelo, version_id: liVersion,  desde: liDesde, hasta: liHasta});
  });

  //Llamada a la API
  var data = {};
  data['articulo'] = id;
  data['aplicaciones'] = JSON.stringify(aplicaciones);
  apix('aplicaciones','editar',data, {
    ok: function(json){
      //Actualizamos la lista de articulos
      watable_volver = true;
      articulosUpdate();
      //Cerramos el popUp
      $('#articuloEditarModal').modal('hide');
    }
  });
}

//Ordenar lista de aplicaciones
function articuloEditar_ordenarLista(elId){
  var items = $('#'+elId+' li').get();
  items.sort(function(a,b){
    var keyA = $(a).text();
    var keyB = $(b).text();

    if (keyA < keyB) return -1;
    if (keyA > keyB) return 1;
    return 0;
  });
  var ul = $('#'+elId);
  $.each(items, function(i, li){
    ul.append(li);
  });
}

{/if}

</script>