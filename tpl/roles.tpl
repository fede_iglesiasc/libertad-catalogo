<script type="text/javascript">

{if="$GLOBALS['permisos']['roles_editar'] || $GLOBALS['permisos']['roles_crear'] || $GLOBALS['permisos']['roles_listar'] || $GLOBALS['permisos']['roles_eliminar'] || $GLOBALS['permisos']['roles_permisos']"}
//Config
var roles_config = {
	modName: 					'roles',
	modLabelSingular: 			'Rol',
	modLabelPlural: 			'Roles',
	creaLabel: 					'Creado',
	editaLabel: 				'Editado',
	eliminaLabel: 				'Eliminado',
	nuevoLabel: 				'Nuevo',
	artLabel: 					'El',
	max_height_edita_agrega: 	'300',
	max_height_lista: 			'240',
	campos:[
		{	
			id: 			"nombre",
			label: 			"Nombre",
			tipo: 			"input",
			mostrarEditar: 	true,
			mostrarCrear: 	true
		}
	]
};
{/if}

{if="$GLOBALS['permisos']['roles_listar']"}
//Lista
function roles_listar(){

	//Creamos el popup
	abrir_modal(roles_config.modName, 550, false);

	//Mostramos la ventana
	$('#'+roles_config.modName+'Modal').modal({show:true});

	//Titulo...
	$('#'+roles_config.modName+'Modal_titulo').html(roles_config.modLabelPlural);

	//Cargando...
	$('#'+roles_config.modName+'Modal_container').html( cargando() );

	//Footer...
	$('#'+roles_config.modName+'Modal_footer').html('');

	//Traemos los datos de la API
	apix(roles_config.modName,'listar',{}, {
		ok: function(json){


			var tabla = generaTabla({
							id_tabla: roles_config.modName,
							items: json.result,
							keys_busqueda: ['nombre'],
							key_nombre: null,
							max_height: roles_config.max_height_lista,
							funcion_activado: function(el){
								//Seleccionamos el activo anterior
								var elActivo = $('#roles_lista').find('.active_');
								var id = $(el).data('id');

								{if="$GLOBALS['permisos']['roles_permisos']"}
								//Permisos
								var boton_permisos = '<button class="btn btn-xs btn-primary" style="margin: 0px 0px 8px 5px; width: 24px; height: 24px;" data-toggle="tooltip" title="Permisos" onclick="'+roles_config.modLabelPlural.toLowerCase()+'_permisos_listar(\''+$(el).data('id')+'\');"><i class="fa fa-unlock-alt"></i></button>';
								{/if}

								{if="$GLOBALS['permisos']['roles_eliminar']"}
								//Borrar
								var boton_borrar = '';
								{/if}

								{if="$GLOBALS['permisos']['roles_editar']"}
								//Editar
								var boton_editar = '';
								{/if}

								//Si no son Roles fijos...
								if((id != 0) && (id != 1) && (id != 2)){

									{if="$GLOBALS['permisos']['roles_eliminar']"}
									//Boton Borrar
									boton_borrar = '<button class="btn btn-xs btn-warning" style="margin: 0px 0px 8px 5px; width: 24px; height: 24px;" data-toggle="tooltip" title="Borrar" onclick=" confirma(\'Eliminar '+roles_config.modLabelSingular+'\',\'Si usted elimina el '+roles_config.modLabelSingular+', todos los datos y usuarios se perderán de forma permanente.\',\'exclamacion\',\'danger\',\''+roles_config.modLabelPlural.toLowerCase()+'_elimina(\\\''+$(el).data('id')+'\\\')\');"><i class="fa fa-remove"></i></button>';
									{/if}

									{if="$GLOBALS['permisos']['roles_editar']"}
									//Boton editar
									boton_editar = '<button class="btn btn-xs btn-primary" style="margin: 0px 0px 8px 5px; width: 24px; height: 24px;" data-toggle="tooltip" title="Editar" onclick="'+roles_config.modLabelPlural.toLowerCase()+'_edita_agrega(\''+$(el).data('id')+'\');"><i class="fa fa-pencil"></i></button>';
									{/if}
								}

								var html = '<b>'+ $(el).data('nombre') + '</b><div class="descripcion"></div><div class="pull-right descripcion" style="width: 120px; text-align: right; position: absolute; top: 8px; right: 10px;">';

								{if="$GLOBALS['permisos']['roles_permisos']"}
								//Boton borrrar
								html += boton_permisos;
								{/if}

								{if="$GLOBALS['permisos']['roles_editar']"}
								//Boton borrrar
								html += boton_editar;
								{/if}

								{if="$GLOBALS['permisos']['roles_eliminar']"}
								//Boton borrrar
								html += boton_borrar;
								{/if}

								//Cerramos el div
								html += '</div>';

								//Agregamos los datos
								$(el).html(html);

							},
							funcion_desactivado: function(el){
								//cambiamos HTML
								$(el).html('<b>'+ $(el).data('nombre') +'</b>');
							}
						});

			//Agregamos los tooltips
			tabla.tooltip({selector: '[data-toggle="tooltip"]', placement: 'left', animation: false});

			//Generamos el listado
			$('#'+roles_config.modName+'Modal_container').html('');
			$('#'+roles_config.modName+'Modal_container').append(tabla);


			//Agregamos scrollbars
			$('#'+roles_config.modName+'Modal_container .scrollable').mCustomScrollbar({theme: 'minimal-dark',scrollInertia: 100, mouseWheel:{ scrollAmount: 70 }});

			{if="$GLOBALS['permisos']['roles_crear']"}
			//Boton Agregar Rol
			$('#'+roles_config.modName+'Modal_footer').append('<button type="button" class="btn btn-primary" style="display: block; width: 100%; margin: 10px auto;" onclick="'+roles_config.modName+'_edita_agrega();">'+roles_config.nuevoLabel+' '+roles_config.modLabelSingular+'</button>');
			{/if}

			//Append Cancel Button
			$('#'+roles_config.modName+'Modal_footer').append('<button type="button" data-dismiss="modal" class="btn btn-warning" style="display: block; width: 100%; margin: 10px auto;">Cerrar</button>');
		}
	});
}
{/if}

{if="$GLOBALS['permisos']['roles_editar'] || $GLOBALS['permisos']['roles_crear']"}
//Edita/Agrega
function roles_edita_agrega(id){

	//Si estamos agregando...
	if(typeof id === 'undefined') var agrega = true;
	else var agrega = false;

	//Titulo...
	if(agrega) $('#'+roles_config.modName+'Modal_titulo').html(roles_config.nuevoLabel+' '+roles_config.modLabelSingular);
	else $('#'+roles_config.modName+'Modal_titulo').html('Editar '+roles_config.modLabelSingular);

	//Cargando...
	$('#'+roles_config.modName+'Modal_container').html(cargando());

	//Footer...
	$('#'+roles_config.modName+'Modal_footer').html('');

	var contenido = $('<div class="scrollable" style="max-height: '+roles_config.max_height_edita_agrega+'px; overflow-y: hidden;"></div>');

	//Agregamos todos los campos
	for(var k in roles_config.campos){
		var campo = roles_config.campos[k];
		if( (campo.mostrarEditar && !agrega) || (campo.mostrarCrear && agrega) ){
			contenido.append( genera_campo(roles_config.campos[k], roles_config.modName ,true) );
		}
	}

	//Agregamos el contenido y los botones
	//si estamos creando un registro nuevo
	if(agrega){
		//Agregamos el contenido
		$('#'+roles_config.modName+'Modal_container').html(contenido);

		//Agregamos scrollbars
		$('#'+roles_config.modName+'Modal_container .scrollable').mCustomScrollbar({theme: 'minimal-dark',scrollInertia: 100, mouseWheel:{ scrollAmount: 70 }});

		//Agregamos los botones al footer
		$('#'+roles_config.modName+'Modal_footer').append('<button type="button" class="btn btn-primary" style="display: block; width: 100%; margin: 10px auto;" onclick="'+roles_config.modLabelPlural.toLowerCase()+'_agrega_confirm();">Crear '+roles_config.modLabelSingular+'</button>');
		$('#'+roles_config.modName+'Modal_footer').append('<button type="button" class="btn btn-warning" style="display: block; width: 100%; margin: 10px auto;" onclick="'+roles_config.modLabelPlural.toLowerCase()+'_listar();">Volver</button>');
	}


	//Si editamos
	if(!agrega){
		var data = {};
		data['rol'] = id;

		apix(roles_config.modName,'info',data, {
			ok: function(json){

				//Titulo
				$('#'+roles_config.modName+'Modal_titulo').html('Editar '+roles_config.modLabelSingular+ ' <b>'+json.result.nombre+'</b>');

		    	//Llenamos los datos
		    	for(var k in roles_config.campos){
		    		var campo = roles_config.campos[k];
		    		if( !(typeof(json.result[ campo.id ]) == "undefined") && !(json.result[ campo.id ] === null) ){
		    			$('#'+roles_config.modName+'_'+campo.id,contenido).val_(json.result[ campo.id ]);
		    		}
		    	}

		    	//Asignamos el contenido al popup
				$('#'+roles_config.modName+'Modal_container').html(contenido);

				//Agregamos scrollbars
				$('#'+roles_config.modName+'Modal_container .scrollable').mCustomScrollbar({theme: 'minimal-dark',scrollInertia: 100, mouseWheel:{ scrollAmount: 70 }});

				//Agregamos los botones al footer
				$('#'+roles_config.modName+'Modal_footer').append('<button type="button" class="btn btn-primary" style="display: block; width: 100%; margin: 10px auto;" onclick="'+roles_config.modLabelPlural.toLowerCase()+'_edita_confirm(\''+id+'\');">Editar '+roles_config.modLabelSingular+'</button>');
				$('#'+roles_config.modName+'Modal_footer').append('<button type="button" class="btn btn-warning" style="display: block; width: 100%; margin: 10px auto;" onclick="'+roles_config.modLabelPlural.toLowerCase()+'_listar();">Volver</button>');
			}
		});
	}
}
{/if}

{if="$GLOBALS['permisos']['roles_editar']"}
//Edita
function roles_edita_confirm(id){

	//Data
	var data = {};
	data['rol'] = id;

	//Agregamos todos los campos
	for(var k in roles_config.campos){
		var campo = roles_config.campos[k];
		if(campo.mostrarEditar)
			data[campo.id] = $('#'+roles_config.modName+'_'+campo.id).val();
	}

	apix(roles_config.modName,'editar',data, {
		ok: function(json){
			notifica(	roles_config.modLabelSingular+' '+roles_config.editaLabel.toLowerCase(),
						roles_config.artLabel+' '+roles_config.modLabelSingular+' se editó con éxito',
						'confirmacion','success');
			roles_listar();
		}
	});
}
{/if}

{if="$GLOBALS['permisos']['roles_crear']"}
//Agrega
function roles_agrega_confirm(){

	//Data
	var data = {};

	//Agregamos todos los campos
	for(var k in roles_config.campos){
		var campo = roles_config.campos[k];
		if(campo.mostrarEditar)
			data[campo.id] = $('#'+roles_config.modName+'_'+campo.id).val();
	}

	apix(roles_config.modName,'agregar',data, {
		ok: function(json){
			//Notifica
			notifica(	roles_config.modLabelSingular+' '+roles_config.creaLabel.toLowerCase(),
						roles_config.artLabel+' '+roles_config.modLabelSingular+' se creó con éxito',
						'confirmacion',
						'success'
					);
			//Update
			roles_listar();
		}
	});
}
{/if}

{if="$GLOBALS['permisos']['roles_eliminar']"}
//Elimina
function roles_elimina(id){

	//Data array
	var data = {};
	data['rol'] = id;
	
	apix(roles_config.modName,'eliminar',data, {
		ok: function(json){
	    	//Notifica
	    	notifica(	roles_config.modLabelSingular+' '+roles_config.eliminaLabel,
	    				roles_config.artLabel+' '+roles_config.modLabelSingular+' se eliminó con éxito',
	    				'informacion',
	    				'info'
	    			);
	    	//Update
	    	roles_listar();
		}
	});
}
{/if}

{if="$GLOBALS['permisos']['roles_permisos']"}
//Lista
function roles_permisos_listar(id){

	//Titulo...
	$('#'+roles_config.modName+'Modal_titulo').html('Permisos a la API');

	//Cargando...
	$('#'+roles_config.modName+'Modal_container').html( cargando() );

	//Footer...
	$('#'+roles_config.modName+'Modal_footer').html('');

	//Traemos los datos de la API
	var data = {};
	data['rol'] = id;
	apix(roles_config.modName,'permisos_listar',data, {
		ok: function(json){

			//Generamos tabla
			var tabla = generaTabla({
					id_tabla: roles_config.modName+'_permisos',
					items: json.result,
					keys_busqueda: ['grupo','accion'],
					key_nombre: null,
					max_height: roles_config.max_height_lista,
					funcion_activado: function(el){
						
					},
					funcion_desactivado: function(el){
						//Checkbox
						var checkbox = $('<button class="btn btn-xs permiso_val" style="position: absolute; top: 10px; right: 10px;" data-type="checkbox"></button>');

						$(checkbox).boton({
							claseActivo: 'btn-success',
							claseInactivo: 'btn-danger',
							valueActivo: '1',
							valueInactivo: '0',
							valueDefault: $(el).data('acceso'),
							labelActivo: function(el){
								$(el).html('<i class="fa fa-check-square-o"></i> Habilitado');
							},
							labelInactivo: function(el){
								$(el).html('<i class="fa fa-ban"></i> Deshabilitado');
							}
						});

						var html = $('<div><b>' + $(el).data('grupo') + '</b><br/><small>' + $(el).data('accion') + '</div>');
						html.append(checkbox);
						//cambiamos HTML
						$(el).html(html);
					}
				});

			//Agregamos los tooltips
			tabla.tooltip({selector: '[data-toggle="tooltip"]', placement: 'left', animation: false});

			//Generamos el listado
			$('#'+roles_config.modName+'Modal_container').html('');
			$('#'+roles_config.modName+'Modal_container').append(tabla);

			//Agregamos scrollbars
			$('#'+roles_config.modName+'Modal_container .scrollable').mCustomScrollbar({theme: 'minimal-dark',scrollInertia: 100, mouseWheel:{ scrollAmount: 70 }});

			//Boton Confirmar cambios
			$('#'+roles_config.modName+'Modal_footer').append('<button type="button" class="btn btn-primary" style="display: block; width: 100%; margin: 10px auto;" onclick="'+roles_config.modName+'_permisos_editar('+id+');">Guardar cambios</button>');

			//Append Cancel Button
			$('#'+roles_config.modName+'Modal_footer').append('<button type="button" class="btn btn-warning" style="display: block; width: 100%; margin: 10px auto;" onclick="'+roles_config.modName+'_listar();">Volver</button>');
		}
	});
}
{/if}

{if="$GLOBALS['permisos']['roles_permisos']"}
//Guarda Permisos
function roles_permisos_editar(id){
	//Array parametro permisos
	var permisos = Array();

	//Creamos array
	$('#'+roles_config.modName+'_permisos_lista .list-group-item').each(function(i, e){
		//Value
		var value = parseInt($('button[data-type="checkbox"]',this).val());
		
		//Agregamosel valor solo si esta habilitado
		if(value != 0) 
			permisos.push( parseInt($(this).data('id')) );
	});

	//Data
	var data = {};
	data['rol'] = id;
	data['permisos'] = JSON.stringify(permisos);

	//DB confirm
	apix(roles_config.modName,'permisos_editar',data, {
		ok: function(json){
			notifica('Permisos editados','Los permisos a la API se editaron con éxito','confirmacion','success');
			roles_listar();
		}
	});
}
{/if}
</script>
