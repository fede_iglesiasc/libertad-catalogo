<script type="text/javascript">

//Configuracion para el cliente
{if="$GLOBALS['session']->getData('rol') == 2"}
var cuenta_config = {
	modName: 					'cuenta',
	modLabelSingular: 			'Cuenta',
	modLabelPlural: 			'',
	creaLabel: 					'Agregado',
	editaLabel: 				'Editada',
	nuevoLabel: 				'Nuevo',
	artLabel: 					'El',
	max_height_edita_agrega: 	'400',
	campos:[
		{	
			id: 			"cuit",
			label: 			"CUIT",
			tipo: 			"input",
			mostrarEditar: 	true,
			mostrarCrear: 	true
		},
		{	
			id: 			"nombre_del_comercio",
			label: 			"Nombre del Comercio",
			tipo: 			"input",
			mostrarEditar: 	true,
			mostrarCrear: 	true
		},
		{	
			id: 			"razon_social",
			label: 			"Razón Social",
			tipo: 			"input",
			mostrarEditar: 	true,
			mostrarCrear: 	true
		},
		{	
			id: 			"email",
			label: 			"Email",
			tipo: 			"input",
			mostrarEditar: 	true,
			mostrarCrear: 	true
		},
		{	
			id: 			"persona_de_contacto",
			label: 			"Persona de contacto",
			tipo: 			"input",
			mostrarEditar: 	true,
			mostrarCrear: 	true
		},
		{	
			id: 			"telefono",
			label: 			"Teléfono",
			tipo: 			"input",
			mostrarEditar: 	true,
			mostrarCrear: 	true
		},
		{	
			id: 			"domicilio",
			label: 			"Domicilio",
			tipo: 			"input",
			mostrarEditar: 	true,
			mostrarCrear: 	true
		},
		{	
			id: 			"localidad",
			label: 			"Localidad",
			tipo: 			"select2",
			mostrarEditar: 	true,
			mostrarCrear: 	true,
			config: 		{
								minimumInputLength: 0,
								placeholder: $(this).attr('placeholder'),
								allowClear: false,
								width: 'resolve',
								id: function(e) { return e.id; },
								formatNoMatches: function() { return 'Sin resultados'; },
								formatSearching: function(){ return "Buscando..."; },
								ajax: {
								    url: "api.php",
								    dataType: 'json',
								    quietMillis: 200,
								    data: function(term, page) {
								        return {
								            module: 'localidades',
								            run: 'listar_select',
								            q: term,
								            p: page
								        };
								    },
								    results: function(data, page ) {
								        //Si el ususario no esta logueado refresh
								        if(!data.logued) location.reload();

								        //Calcula si existen mas resultados a mostrar
								        var more = (page * 40) < data.result.total;

								        //Devolvemos el valor de more para que selec2
								        //sepa que debemos cargar mas resultados.
								        return {results: data.result.items, more: more};
								    }
								},
								formatInputTooShort: function () {
									return "";
								},
								formatResult: function(item) {
									return item.nombre;
								},
								formatSelection: function(item) { 
									return item.nombre;
								},
								initSelection : function (element, callback) {
									var elementText = $(element).attr('data-init-text');
								}
							}
		}
	]
};
{/if}


//Configuracion para el cliente
{if="$GLOBALS['session']->getData('rol') != 2"}
var cuenta_config = {
	modName: 					'cuenta',
	modLabelSingular: 			'Cuenta',
	modLabelPlural: 			'',
	creaLabel: 					'Agregado',
	editaLabel: 				'Editada',
	nuevoLabel: 				'Nuevo',
	artLabel: 					'El',
	max_height_edita_agrega: 	'500',
	max_height_lista: 			'400',
	campos:[
		{	
			id: 			"nombre",
			label: 			"Nombre",
			tipo: 			"input",
			mostrarEditar: 	true,
			mostrarCrear: 	true
		},
		{	
			id: 			"email",
			label: 			"Email",
			tipo: 			"input",
			mostrarEditar: 	true,
			mostrarCrear: 	true
		}
	]
};
{/if}

var cuenta_usuario_config = {
	modName: 					'faltantes',
	modLabelSingular: 			'Faltante',
	modLabelPlural: 			'Faltantes',
	creaLabel: 					'Agregado',
	editaLabel: 				'Editado',
	eliminaLabel: 				'Eliminado',
	nuevoLabel: 				'Nuevo',
	artLabel: 					'El',
	max_height_edita_agrega: 	'300',
	max_height_lista: 			'300',
	campos:[
		{	
			id: 			"codigo",
			label: 			"Fabricante y Código",
			tipo: 			"select2",
			mostrarEditar: 	true,
			mostrarCrear: 	true,
			config: 		{
							  minimumInputLength: 0,
							  placeholder: 'Fabricante y Código',
							  allowClear: true,
							  width: 'resolve',
							  id: function(e) { return e.articulo_id; },
							  formatNoMatches: function() { return 'Sin resultados'; },
							  formatSearching: function(){ return "Buscando..."; },
							  ajax: {
							      url: "api.php",
							      dataType: 'json',
							      quietMillis: 200,
							      data: function(term, page) {
							          return {
							              module: 'faltantes',
							              run: 'fabricantes_codigo_listar',
							              q: term,
							              p: page
							          };
							      },
							      results: function(data, page ) {
							          //If session expire reload page...
							          //if(!data.logued) location.reload();

							          var more = (page * 40) < data.result.total; // whether or not there are more results available

							          // notice we return the value of more so Select2 knows if more results can be loaded
							          return {results: data.result.items, more: more};
							      }
							  },
							  formatInputTooShort: function () {
							            return "";
							        },  
							  formatResult: function(item) {
							      if((item.codigo == null) && (item.articulo_id == null))
							      	return '<b>' + item.marca_label + '</b> <i class="badge" style="padding: 5px 6px 6px 7px; float: none; border-radius: 4px; background-color: #e6e6e6; margin-rigth: 7px; margin-top: 2px; color: #444444; font-size: 10px; border: solid 1px #dadada">Agregar nuevo Código</i>';

							      return '<div class="select2-user-result"><i class="badge" style="padding: 5px 6px 6px 7px; float: none; border-radius: 4px; background-color: #e6e6e6; margin-right: 7px; margin-top: 2px; color: #444444; font-size: 12px; border: solid 1px #dadada">'+item.marca_label+'</i><b>'+item.codigo+'</b></div>'; 
							  },
							  formatSelection: function(item) {
							  	var codigo = item.codigo;
							  	
							  	//Creamos el textp para el codigo
							  	if(codigo == null) codigo = 'Ingrese el código...';
							  	else codigo = '<b>' + codigo + '</b>';

							    var label = '<i class="badge" style="padding: 5px 6px 6px 7px; float: none; border-radius: 4px; background-color: #e6e6e6; margin-right: 7px; margin-top: 2px; color: #444444; font-size: 12px; border: solid 1px #dadada;">'+item.marca_label+'</i>' + codigo;

							      return label; 
							  },
							  initSelection : function (element, callback) {
							      var elementText = $(element).attr('data-init-text');
							  }
							}
		},
		{	
			id: 			"comentario",
			label: 			"Comentario",
			tipo: 			"input",
			mostrarEditar: 	true,
			mostrarCrear: 	true
		}
	]
};

//Edita/Agrega
function cuenta_edita(){

	//Creamos el popup
	abrir_modal(cuenta_config.modName, 550, false);

	//Mostramos la ventana
	$('#'+cuenta_config.modName+'Modal').modal({show:true});

	//Titulo...
	$('#'+cuenta_config.modName+'Modal_titulo').html('Editar datos de la Cuenta');

	//Cargando...
	$('#'+cuenta_config.modName+'Modal_container').html(cargando());

	//Footer...
	$('#'+cuenta_config.modName+'Modal_footer').html('');

	//Contenido
	var contenido = $('<div class="scrollable" style="max-height: '+cuenta_config.max_height_edita_agrega+'px; overflow-y: hidden;"></div>');

	//Agregamos todos los campos
	for(var k in cuenta_config.campos){
		var campo = cuenta_config.campos[k];
		contenido.append( genera_campo(cuenta_config.campos[k], cuenta_config.modName ,true) );
		
	}

	//Obtenemos info de la cuenta
	apix(cuenta_config.modName,'info',{}, {
		ok: function(json){

	    	//Llenamos los datos
	    	for(var k in cuenta_config.campos){
	    		var campo = cuenta_config.campos[k];
	    		if((campo.tipo == 'input') || (campo.tipo == 'checkbox'))
		    		if( !(typeof(json.result[ campo.id ]) == "undefined") && !(json.result[ campo.id ] === null) ){
		    			$('#'+cuenta_config.modName+'_'+campo.id,contenido).val_(json.result[ campo.id ]);
		    		}
	    	}

	    	//Creamos el campo especial para popular el S2 de Fabricantes / Codigo
	    	$('#cuenta_localidad',contenido).val_({ id: json.result.localidad, nombre: json.result.localidad_label });

	    	//Asignamos el contenido al popup
			$('#'+cuenta_config.modName+'Modal_container').html(contenido);

			//Agregamos scrollbars
			$('#'+cuenta_config.modName+'Modal_container .scrollable').mCustomScrollbar({theme: 'minimal-dark',scrollInertia: 100, mouseWheel:{ scrollAmount: 70 }});

			//Agregamos los botones al footer
			$('#'+cuenta_config.modName+'Modal_footer').append('<button type="button" class="btn btn-primary" style="display: block; width: 100%; margin: 10px auto;" onclick="'+cuenta_config.modName+'_edita_confirm();">Editar '+cuenta_config.modLabelSingular+'</button>');
			//Append Cancel Button
			$('#'+cuenta_config.modName+'Modal_footer').append('<button type="button" data-dismiss="modal" class="btn btn-warning" style="display: block; width: 100%; margin: 10px auto;">Cerrar</button>');
		}
	});
}

//Confirma Edicion
function cuenta_edita_confirm(){

	//Data
	var data = {};

	//Agregamos todos los campos
	for(var k in cuenta_config.campos){
		var campo = cuenta_config.campos[k];
		if(campo.mostrarEditar && (campo.tipo == 'input'))
			data[campo.id] = $('#'+cuenta_config.modName+'_'+campo.id).val_();
	}

	{if="$GLOBALS['session']->getData('rol') == 2"}
	//Campo especial Codigo / Fabricante
	var localidad = $('#cuenta_localidad').select2('data');
	if(localidad != null) {
		data['localidad'] = localidad.id;
	}
	{/if}

	//Llamada a la API
	{if="$GLOBALS['session']->getData('rol') != 2"}
	var modulo = 'cuenta';
	var accion = 'editar';
	{/if}
	{if="$GLOBALS['session']->getData('rol') == 2"}
	var modulo = 'clientes';
	var accion = 'editar_cuenta';
	{/if}
	apix(modulo,accion,data,{
		ok: function(json){
			notifica(	cuenta_config.modLabelSingular+' '+cuenta_config.editaLabel.toLowerCase(),
						cuenta_config.artLabel+' '+cuenta_config.modLabelSingular+' se editó con éxito',
						'confirmacion','success');
			$('#'+cuenta_config.modName+'Modal').modal('hide');
		}
	});
}

//Cambiar Password
function cambiar_password(){

	//Creamos el popup
	abrir_modal(cuenta_config.modName, 550, false);

	//Mostramos la ventana
	$('#'+cuenta_config.modName+'Modal').modal({show:true});

	//Titulo...
	$('#'+cuenta_config.modName+'Modal_titulo').html('Cambiar la Contraseña');

	//Contenido
	var contenido = $('<div class="scrollable" style="max-height: '+cuenta_config.max_height_edita_agrega+'px; overflow-y: hidden;"></div>');

	contenido.append( genera_campo({	
		id: 			"password",
		label: 			"Contraseña actual",
		tipo: 			"input",
		mostrarEditar: 	true,
		mostrarCrear: 	true
	}, 'cuenta' ,true));

	contenido.append( genera_campo({	
		id: 			"password_nuevo",
		label: 			"Contraseña nueva",
		tipo: 			"input",
		mostrarEditar: 	true,
		mostrarCrear: 	true
	}, 'cuenta' ,true));

	contenido.append( genera_campo({	
		id: 			"password_nuevo_repetido",
		label: 			"Repetir contraseña nueva",
		tipo: 			"input",
		mostrarEditar: 	true,
		mostrarCrear: 	true
	}, 'cuenta' ,true));

	//Asignamos el contenido al popup
	$('#'+cuenta_config.modName+'Modal_container').html(contenido);

	//Agregamos scrollbars
	$('#'+cuenta_config.modName+'Modal_container .scrollable').mCustomScrollbar({theme: 'minimal-dark',scrollInertia: 100, mouseWheel:{ scrollAmount: 70 }});

	//Agregamos los botones al footer
	$('#'+cuenta_config.modName+'Modal_footer').append('<button type="button" class="btn btn-primary" style="display: block; width: 100%; margin: 10px auto;" onclick="cambiar_password_confirm();">Cambiar la Contraseña</button>');
	//Append Cancel Button
	$('#'+cuenta_config.modName+'Modal_footer').append('<button type="button" data-dismiss="modal" class="btn btn-warning" style="display: block; width: 100%; margin: 10px auto;">Cerrar</button>');
}

//Cambiar Password confirm
function cambiar_password_confirm(){
	//Llamada a la API
	var data = {};
	data['password'] = $('#cuenta_password').val_();
	data['password_nuevo'] = $('#cuenta_password_nuevo').val_();
	data['password_nuevo_repetido'] = $('#cuenta_password_nuevo_repetido').val_();

	apix('cuenta','chngPass',data,{
		ok: function(json){
	    	//Notifica
	    	notifica(	'Contraseña cambiada!',
	    				'La próxima vez que entre al sistema, recuerde que deberá usar la nueva Clave.',
	    				'informacion',
	    				'info'
	    			);
	    	//Update
	    	$('#'+cuenta_config.modName+'Modal').modal('hide');
		}
	});
}

//Logout
function logout(){

	//API
	apix('auth','logout',{}, {
		ok: function(json){
			location.reload();
		}
	});
}



{if="$GLOBALS['permisos']['cuenta_editar_vacaciones']"}
//Lista
function cuenta_vacaciones_listar(){

	//Creamos el popup
	abrir_modal('cuenta_vacaciones', 550, false);

	//Mostramos la ventana
	$('#cuenta_vacacionesModal').modal({show:true});

	//Titulo...
	$('#cuenta_vacacionesModal_titulo').html('Vacaciones');

	//Cargando...
	$('#cuenta_vacacionesModal_container').html( cargando() );

	//Footer...
	$('#cuenta_vacacionesModal_footer').html('');

	var data = {};

	//Traemos los datos de la API
	apix('cuenta','vacaciones_listar',data, {
		ok: function(json){

			//Titulo...
			$('#cuenta_vacacionesModal_titulo').html('Vacaciones de '+json.result.nombre);

			//Tabla
			var tabla = generaTabla({
							id_tabla: 'cuenta_vacaciones',
							items: json.result.vacaciones,
							keys_busqueda: ['nombre'],
							key_nombre: null,
							max_height: 400,
							campobusqueda: false,
							funcion_activado: function(el){
							},
							funcion_desactivado: function(el){
								var html = '<span style="display: inline-block; width: 200px;"><b>Inician: </b>'+$(el).data('inicio')+'</span><span style="display: inline-block; width: 200px;"><b>Finalizan: </b>'+$(el).data('fin')+'</span><div class="pull-right descripcion" style="width: 120px; text-align: right; position: absolute; top: 8px; right: 10px;"><button class="btn btn-xs btn-primary" style="margin: 0px 0px 8px 5px; width: 24px; height: 24px;" data-toggle="tooltip" title="" onclick="cuenta_vacaciones_edita_agrega('+$(el).data('id')+');" data-original-title="Editar"><i class="fa fa-pencil"></i></button><button class="btn btn-xs btn-warning" style="margin: 0px 0px 8px 5px; width: 24px; height: 24px;" data-toggle="tooltip" title="Borrar" onclick=" confirma(\'Eliminar Vacaciones\',\'Las Vacaciones se eliminarán de forma permanente junto con todos los datos relacionados.\',\'exclamacion\',\'danger\',\'cuenta_vacaciones_eliminar('+$(el).data('id')+')\');"><i class="fa fa-remove"></i></button><div></div></div>';
								
								$(el).html(html);
							}
						});

			//Agregamos los tooltips
			tabla.tooltip({selector: '[data-toggle="tooltip"]', placement: 'left', animation: false});

			//Generamos el listado
			$('#cuenta_vacacionesModal_container').html('');
			$('#cuenta_vacacionesModal_container').append(tabla);


			//Agregamos scrollbars
			$('#cuenta_vacacionesModal_container .scrollable').mCustomScrollbar({theme: 'minimal-dark',scrollInertia: 100, mouseWheel:{ scrollAmount: 70 }});

			//Agregar
			$('#cuenta_vacacionesModal_footer').append('<button type="button" class="btn btn-primary" style="display: block; width: 100%; margin: 10px auto;" onclick="cuenta_vacaciones_edita_agrega(0);">Agregar Vacaciones</button>');

			//Volver
			$('#cuenta_vacacionesModal_footer').append('<button type="button" data-dismiss="modal" class="btn btn-warning" style="display: block; width: 100%; margin: 10px auto;">Cerrar</button>');
		}
	});
}

//Eliminar Horario
function cuenta_vacaciones_eliminar(id){
	//Data array
	var data = {};
	data['vacaciones'] = id;
	
	apix('cuenta','vacaciones_eliminar',data, {
        ok: function(json){
			//Notifica
			notifica(	'Fecha de Vacaciones Eliminadas',
						'La fecha de vacaciones se eliminó con éxito',
						'informacion',
						'info'
					);
			//Update
			cuenta_vacaciones_listar();
		}
	});
}

//Edita/Agrega
function cuenta_vacaciones_edita_agrega(id){

	//Si estamos agregando...
	if(!id) var agrega = true;
	else var agrega = false;

	//Titulo...
	if(agrega) $('#cuenta_vacacionesModal_titulo').html('Agregar fecha de Vacaciones');
	else $('#cuenta_vacacionesModal_titulo').html('Editar fechas de Vacaciones');

	//Cargando...
	$('#cuenta_vacacionesModal_container').html(cargando());

	//Footer...
	$('#cuenta_vacacionesModal_footer').html('');

	//Contenido
	var contenido = $('<div class="scrollable" style="max-height: 500px; overflow-y: hidden;"></div>'); 

	//Agregamos los campos
	contenido.append( genera_campo(	{	
		id: 			"fecha",
		label: 			"Fecha de Vacaciones",
		labelDesde: 	'Desde',
		labelHasta: 	'Hasta',
		tipo: 			"daterange",
		mostrarEditar: 	true,
		mostrarCrear: 	true,
		config: 		{
						format: 'dd/mm/yyyy',
						autoclose: true,
						language: 'es'
		}
	}, 'cuenta_vacaciones' ,true) );

	//Agregamos el contenido y los botones
	//si estamos creando un registro nuevo
	if(agrega){
		//Agregamos el contenido
		$('#cuenta_vacacionesModal_container').html(contenido);

		//Agregamos scrollbars
		$('#cuenta_vacacionesModal_container .scrollable').mCustomScrollbar({theme: 'minimal-dark',scrollInertia: 100, mouseWheel:{ scrollAmount: 70 }});

		//Agregamos los botones al footer
		$('#cuenta_vacacionesModal_footer').append('<button type="button" class="btn btn-primary" style="display: block; width: 100%; margin: 10px auto;" onclick="cuenta_vacaciones_agrega_confirm();">Agregar Vacaciones</button>');
		$('#cuenta_vacacionesModal_footer').append('<button type="button" class="btn btn-warning" style="display: block; width: 100%; margin: 10px auto;" onclick="cuenta_vacaciones_listar();">Volver</button>');
	}


	//Si editamos
	if(!agrega){
		var data = {};
		data['vacaciones'] = id;

		apix('cuenta','vacaciones_info',data, {
            ok: function(json){

				//Titulo
				$('#cuenta_vacacionesModal_titulo').html('Editar Fecha de Vacaciones');

		    	//Asignamos el contenido al popup
				$('#cuenta_vacacionesModal_container').html(contenido);

				//Fechas
				$('#cuenta_vacaciones_fecha_hasta').datepicker('update', json.result.fin);
				$('#cuenta_vacaciones_fecha_desde').datepicker('update', json.result.inicio);

				//Agregamos scrollbars
				$('#cuenta_vacacionesModal_container .scrollable').mCustomScrollbar({theme: 'minimal-dark',scrollInertia: 100, mouseWheel:{ scrollAmount: 70 }});

				//Agregamos los botones al footer
				$('#cuenta_vacacionesModal_footer').append('<button type="button" class="btn btn-primary" style="display: block; width: 100%; margin: 10px auto;" onclick="cuenta_vacaciones_edita_confirm('+id+');">Editar Horario</button>');
				$('#cuenta_vacacionesModal_footer').append('<button type="button" class="btn btn-warning" style="display: block; width: 100%; margin: 10px auto;" onclick="cuenta_vacaciones_listar();">Volver</button>');
			}
		});
	}
}

//Edita
function cuenta_vacaciones_edita_confirm(id){
	//Data
	var data = {};
	data['vacaciones'] = id;
	data['inicio'] = $('#cuenta_vacaciones_fecha_desde').val();
	data['fin'] = $('#cuenta_vacaciones_fecha_hasta').val();

	apix('cuenta','vacaciones_editar',data, {
		ok: function(json){
			notifica(	'Vacaciones Editadas',
						'Las fechas de las Vacaciones se editaron con éxito',
						'confirmacion','success');
			cuenta_vacaciones_listar();
		}
	});
}

//Agrega
function cuenta_vacaciones_agrega_confirm(){
	//Data
	var data = {};
	data['inicio'] = $('#cuenta_vacaciones_fecha_desde').val();
	data['fin'] = $('#cuenta_vacaciones_fecha_hasta').val();

	apix('cuenta','vacaciones_agregar',data, {
		ok: function(json){
			notifica(	'Vacaciones Editadas',
						'Las fechas de las Vacaciones se editaron con éxito',
						'confirmacion','success');
			cuenta_vacaciones_listar();
		}
	});
}
{/if}

</script>