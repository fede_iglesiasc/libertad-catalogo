<script type="text/javascript">

{if="$GLOBALS['permisos']['faltantes_editar'] || $GLOBALS['permisos']['faltantes_crear'] || $GLOBALS['permisos']['faltantes_listar'] || $GLOBALS['permisos']['faltantes_eliminar']"}
//Config Faltantes
var faltantes_config = {
	modName: 					'faltantes',
	modLabelSingular: 			'Faltante',
	modLabelPlural: 			'Faltantes',
	creaLabel: 					'Agregado',
	editaLabel: 				'Editado',
	eliminaLabel: 				'Eliminado',
	nuevoLabel: 				'Nuevo',
	artLabel: 					'El',
	max_height_edita_agrega: 	'300',
	max_height_lista: 			'300',
	campos:[
		{	
			id: 			"codigo",
			label: 			"Fabricante y Código",
			tipo: 			"select2",
			mostrarEditar: 	true,
			mostrarCrear: 	true,
			config: 		{
							  minimumInputLength: 0,
							  placeholder: 'Fabricante y Código',
							  allowClear: true,
							  width: 'resolve',
							  id: function(e) { return e.articulo_id; },
							  formatNoMatches: function() { return 'Sin resultados'; },
							  formatSearching: function(){ return "Buscando..."; },
							  ajax: {
							      url: "api.php",
							      dataType: 'json',
							      quietMillis: 200,
							      data: function(term, page) {
							          return {
							              module: 'faltantes',
							              run: 'fabricantes_codigo_listar',
							              q: term,
							              p: page
							          };
							      },
							      results: function(data, page ) {
							          //If session expire reload page...
							          //if(!data.logued) location.reload();

							          var more = (page * 40) < data.result.total; // whether or not there are more results available

							          // notice we return the value of more so Select2 knows if more results can be loaded
							          return {results: data.result.items, more: more};
							      }
							  },
							  formatInputTooShort: function () {
							            return "";
							        },  
							  formatResult: function(item) {
							      if((item.codigo == null) && (item.articulo_id == null))
							      	return '<b>' + item.marca_label + '</b> <i class="badge" style="padding: 5px 6px 6px 7px; float: none; border-radius: 4px; background-color: #e6e6e6; margin-rigth: 7px; margin-top: 2px; color: #444444; font-size: 10px; border: solid 1px #dadada">Agregar nuevo Código</i>';

							      return '<div class="select2-user-result"><i class="badge" style="padding: 5px 6px 6px 7px; float: none; border-radius: 4px; background-color: #e6e6e6; margin-right: 7px; margin-top: 2px; color: #444444; font-size: 12px; border: solid 1px #dadada">'+item.marca_label+'</i><b>'+item.codigo+'</b></div>'; 
							  },
							  formatSelection: function(item) {
							  	var codigo = item.codigo;
							  	
							  	//Creamos el textp para el codigo
							  	if(codigo == null) codigo = 'Ingrese el código...';
							  	else codigo = '<b>' + codigo + '</b>';

							    var label = '<i class="badge" style="padding: 5px 6px 6px 7px; float: none; border-radius: 4px; background-color: #e6e6e6; margin-right: 7px; margin-top: 2px; color: #444444; font-size: 12px; border: solid 1px #dadada;">'+item.marca_label+'</i>' + codigo;

							      return label; 
							  },
							  initSelection : function (element, callback) {
							      var elementText = $(element).attr('data-init-text');
							  }
							}
		},
		{	
			id: 			"comentario",
			label: 			"Comentario",
			tipo: 			"input",
			mostrarEditar: 	true,
			mostrarCrear: 	true
		}
	]
};
{/if}

{if="$GLOBALS['permisos']['faltantes_listar']"}
//Lista
function faltantes_listar(){

	//Creamos el popup
	abrir_modal(faltantes_config.modName, 550, false);

	//Mostramos la ventana
	$('#'+faltantes_config.modName+'Modal').modal({show:true});

	//Titulo...
	$('#'+faltantes_config.modName+'Modal_titulo').html(faltantes_config.modLabelPlural);

	//Cargando...
	$('#'+faltantes_config.modName+'Modal_container').html( cargando() );

	//Footer...
	$('#'+faltantes_config.modName+'Modal_footer').html('');

	//Traemos los datos de la API
	apix(faltantes_config.modName,'listar',{}, {
		ok: function(json){
			//Tabla
			var tabla = generaTabla({
							id_tabla: faltantes_config.modName,
							items: json.result,
							ajaxModulo: 'faltantes',
							ajaxAccion: 'listar',
							keys_busqueda: ['id','grupo','accion'],
							key_nombre: null,
							max_height: faltantes_config.max_height_lista,
							funcion_activado: function(el){
								//Seleccionamos el activo anterior
								var id = $(el).data('id');

								//Boton Borrar
								var boton_borrar = '<button class="btn btn-xs btn-warning" style="margin: 0px 0px 8px 5px; width: 24px; height: 24px;" data-toggle="tooltip" title="Borrar" onclick=" confirma(\'Eliminar '+faltantes_config.modLabelSingular+'\',\'El '+faltantes_config.modLabelSingular+' se eliminará de forma permanente.\',\'exclamacion\',\'danger\',\''+faltantes_config.modLabelPlural.toLowerCase()+'_elimina(\\\''+$(el).data('id')+'\\\')\');"><i class="fa fa-remove"></i></button>';

								//Boton editar
								var boton_editar = '<button class="btn btn-xs btn-primary" style="margin: 0px 0px 8px 5px; width: 24px; height: 24px;" data-toggle="tooltip" title="Editar" onclick="'+faltantes_config.modLabelPlural.toLowerCase()+'_edita_agrega(\''+$(el).data('id')+'\');"><i class="fa fa-pencil"></i></button>';
								
								//Agregamos los datos
								$(el).html('<i class="badge" style="padding: 5px 10px; float: none; border-radius: 3px; background-color: #e6e6e6; margin-right: 7px; color: #444444; font-size: 12px; border: solid 1px #dadada; font-weight: 600;">' + $(el).data('marca_label') + '</i> <b>' + $(el).data('codigo') + '</b><small style="display: block; margin-top: 10px;">' + $(el).data('comentario') + '</small><div class="descripcion"></div><div class="pull-right descripcion" style="width: 120px; text-align: right; position: absolute; top: 8px; right: 10px;">'+boton_editar+boton_borrar+'</div>');


							},
							funcion_desactivado: function(el){
								var html = '<i class="badge" style="padding: 5px 10px; float: none; border-radius: 3px; background-color: #e6e6e6; margin-right: 7px; color: #444444; font-size: 12px; border: solid 1px #dadada; font-weight: 600;">' + $(el).data('marca_label') + '</i> <b>' + $(el).data('codigo') + '</b>';
								//cambiamos HTML
								$(el).html(html);
							}
						});

			//Agregamos los tooltips
			tabla.tooltip({selector: '[data-toggle="tooltip"]', placement: 'left', animation: false});

			//Generamos el listado
			$('#'+faltantes_config.modName+'Modal_container').html('');
			$('#'+faltantes_config.modName+'Modal_container').append(tabla);


			//Agregamos scrollbars
			$('#'+faltantes_config.modName+'Modal_container .scrollable').mCustomScrollbar({theme: 'minimal-dark',scrollInertia: 100, mouseWheel:{ scrollAmount: 70 }});

			//Boton Confirmar cambios
			$('#'+faltantes_config.modName+'Modal_footer').append('<button type="button" class="btn btn-primary" style="display: block; width: 100%; margin: 10px auto;" onclick="'+faltantes_config.modName+'_edita_agrega();">'+faltantes_config.nuevoLabel+' '+faltantes_config.modLabelSingular+'</button>');

			//Append Cancel Button
			$('#'+faltantes_config.modName+'Modal_footer').append('<button type="button" data-dismiss="modal" class="btn btn-warning" style="display: block; width: 100%; margin: 10px auto;">Cerrar</button>');
		}
	});
}
{/if}

{if="$GLOBALS['permisos']['faltantes_editar'] || $GLOBALS['permisos']['faltantes_crear']"}
//Edita/Agrega
function faltantes_edita_agrega(id){

	//Si estamos agregando...
	if(typeof id === 'undefined') var agrega = true;
	else var agrega = false;

	//Titulo...
	if(agrega) $('#'+faltantes_config.modName+'Modal_titulo').html(faltantes_config.nuevoLabel+' '+faltantes_config.modLabelSingular);
	else $('#'+faltantes_config.modName+'Modal_titulo').html('Editar '+faltantes_config.modLabelSingular);

	//Cargando...
	$('#'+faltantes_config.modName+'Modal_container').html(cargando());

	//Footer...
	$('#'+faltantes_config.modName+'Modal_footer').html('');

	var contenido = $('<div class="scrollable" style="max-height: '+faltantes_config.max_height_edita_agrega+'px; overflow-y: hidden;"></div>');

	//Agregamos todos los campos
	for(var k in faltantes_config.campos){
		var campo = faltantes_config.campos[k];
		if( (campo.mostrarEditar && !agrega) || (campo.mostrarCrear && agrega) ){
			//Agregamos el campo
			contenido.append( genera_campo(faltantes_config.campos[k], faltantes_config.modName ,true) );
		}
	}

	//Le pedimos el codigo en caso que este agregando un
	//faltante de un codigo que no exista anteriormente en la DB
	$('#faltantes_codigo',contenido).on("change", function(e) {
		//Agregamos Codigo
		var fabricante = $(this).select2('data');
		if(fabricante != null)
			if( (fabricante.articulo_id == null) && (fabricante.marca_id != null) )
				dialogo('Ingrese el Código','Ingrese el código del faltante.','interrogacion','info','Código',function(el){
					$('button.btn-primary',el).on('click',function(){
						fabricante.codigo = $('#dialogo_input',el).val();
						$('#faltantes_codigo').select2('data',fabricante);
						dialogo_.close();
                    });
					$('button.btn-warning',el).on('click',function(){
						$('#faltantes_codigo').val_(null);
                    });
				});
	});

	//Agregamos el contenido y los botones
	//si estamos creando un registro nuevo
	if(agrega){
		//Agregamos el contenido
		$('#'+faltantes_config.modName+'Modal_container').html(contenido);

		//Agregamos scrollbars
		$('#'+faltantes_config.modName+'Modal_container .scrollable').mCustomScrollbar({theme: 'minimal-dark',scrollInertia: 100, mouseWheel:{ scrollAmount: 70 }});

		//Agregamos los botones al footer
		$('#'+faltantes_config.modName+'Modal_footer').append('<button type="button" class="btn btn-primary" style="display: block; width: 100%; margin: 10px auto;" onclick="'+faltantes_config.modLabelPlural.toLowerCase()+'_agrega_confirm();">Agregar '+faltantes_config.modLabelSingular+'</button>');
		$('#'+faltantes_config.modName+'Modal_footer').append('<button type="button" class="btn btn-warning" style="display: block; width: 100%; margin: 10px auto;" onclick="'+faltantes_config.modLabelPlural.toLowerCase()+'_listar();">Volver</button>');
	}


	//Si editamos
	if(!agrega){
		var data = {};
		data['faltante'] = id;

		apix(faltantes_config.modName,'info',data, {
            ok: function(json){

				//Titulo
				$('#'+faltantes_config.modName+'Modal_titulo').html('Editar '+faltantes_config.modLabelSingular);

		    	//Llenamos los datos
		    	for(var k in faltantes_config.campos){
		    		var campo = faltantes_config.campos[k];
		    		if(campo.tipo == 'input')
			    		if( !(typeof(json.result[ campo.id ]) == "undefined") && !(json.result[ campo.id ] === null) ){
			    			$('#'+faltantes_config.modName+'_'+campo.id,contenido).val_(json.result[ campo.id ]);
			    		}
		    	}

		    	//Creamos el campo especial para popular el S2 de Fabricantes / Codigo
		    	$('#faltantes_codigo',contenido).val_({	codigo: json.result.codigo,
		    											articulo_id: json.result.articulo_id,
		    											marca_label: json.result.marca_label,
		    											marca_id: json.result.marca_id});

		    	//Asignamos el contenido al popup
				$('#'+faltantes_config.modName+'Modal_container').html(contenido);

				//Agregamos scrollbars
				$('#'+faltantes_config.modName+'Modal_container .scrollable').mCustomScrollbar({theme: 'minimal-dark',scrollInertia: 100, mouseWheel:{ scrollAmount: 70 }});

				//Agregamos los botones al footer
				$('#'+faltantes_config.modName+'Modal_footer').append('<button type="button" class="btn btn-primary" style="display: block; width: 100%; margin: 10px auto;" onclick="'+faltantes_config.modLabelPlural.toLowerCase()+'_edita_confirm(\''+id+'\');">Editar '+faltantes_config.modLabelSingular+'</button>');
				$('#'+faltantes_config.modName+'Modal_footer').append('<button type="button" class="btn btn-warning" style="display: block; width: 100%; margin: 10px auto;" onclick="'+faltantes_config.modLabelPlural.toLowerCase()+'_listar();">Volver</button>');
			}
		});
	}
}
{/if}

{if="$GLOBALS['permisos']['faltantes_editar']"}
//Edita
function faltantes_edita_confirm(id){

	//Data
	var data = {};
	data['faltante'] = id;

	//Comentario
	data['comentario'] = $('#faltantes_comentario').val_();
	
	//Agregamos Codigo
	var fabricante = $('#faltantes_codigo').select2('data');
	if(fabricante != null){
		//Si estamos agregando un código inexistente
		if(fabricante.articulo_id == null){
			data['fabricante'] = fabricante.marca_id;
			data['codigo'] = fabricante.codigo;
		//si existe el art.
		}else{
			data['articulo'] = fabricante.articulo_id;
		}
	}

	apix(faltantes_config.modName,'editar',data, {
		ok: function(json){
			notifica(	faltantes_config.modLabelSingular+' '+faltantes_config.editaLabel.toLowerCase(),
						faltantes_config.artLabel+' '+faltantes_config.modLabelSingular+' se editó con éxito',
						'confirmacion','success');
			faltantes_listar();
		}
	});
}
{/if}

{if="$GLOBALS['permisos']['faltantes_crear']"}
//Agrega
function faltantes_agrega_confirm(){

	//Data
	var data = {};
	
	//Comentario
	data['comentario'] = $('#faltantes_comentario').val_();
	
	//Agregamos Codigo
	var fabricante = $('#faltantes_codigo').select2('data');
	if(fabricante != null){
		//Si estamos agregando un código inexistente
		if(fabricante.articulo_id == null){
			data['fabricante'] = fabricante.marca_id;
			data['codigo'] = fabricante.codigo;
		//si existe el art.
		}else{
			data['articulo'] = fabricante.articulo_id;
		}
	}

	apix(faltantes_config.modName,'agregar',data, {
		ok: function(json){
			//Notifica
			notifica(	faltantes_config.modLabelSingular+' '+faltantes_config.creaLabel.toLowerCase(),
						faltantes_config.artLabel+' '+faltantes_config.modLabelSingular+' se creó con éxito',
						'confirmacion',
						'success'
					);
			//Update
			faltantes_listar();
		}
	});
}
{/if}

{if="$GLOBALS['permisos']['faltantes_eliminar']"}
//Elimina
function faltantes_elimina(id){

	//Data array
	var data = {};
	data['faltante'] = id;
	
	apix(faltantes_config.modName,'eliminar',data, {
		ok: function(json){
	    	//Notifica
	    	notifica(	faltantes_config.modLabelSingular+' '+faltantes_config.eliminaLabel,
	    				faltantes_config.artLabel+' '+faltantes_config.modLabelSingular+' se eliminó con éxito',
	    				'informacion',
	    				'info'
	    			);
	    	//Update
	    	faltantes_listar();
		}
	});
}
{/if}

{if="$GLOBALS['permisos']['faltantes_crear']"}
//Agrega
function faltantes_agrega_rapido(){

	//Data
	var data = {};
	data['comentario'] = '';
	data['articulo'] = resultados.data.rows[$('#resultados_table tbody tr.seleccionado').data('index')].id;

	apix(faltantes_config.modName,'agregar',data, {
		ok: function(json){
			//Notifica
			notifica(	faltantes_config.modLabelSingular+' '+faltantes_config.creaLabel.toLowerCase(),
						faltantes_config.artLabel+' '+faltantes_config.modLabelSingular+' se creó con éxito',
						'confirmacion',
						'success'
					);
		}
	});
}
{/if}













{if="$GLOBALS['permisos']['faltantes_reporte_generar'] || $GLOBALS['permisos']['faltantes_reporte_eliminar']"}
//Config
var reporte_faltantes_config = {
	modName: 					'reporte_faltantes',
	modLabelSingular: 			'Faltante',
	modLabelPlural: 			'Faltantes',
	creaLabel: 					'Agregado',
	editaLabel: 				'Editado',
	eliminaLabel: 				'Eliminado',
	nuevoLabel: 				'Nuevo',
	artLabel: 					'El',
	max_height_edita_agrega: 	'300',
	max_height_lista: 			'300',
	max_height_reporte: 		'300',
	campos:[
		{	
			id: 			"fabricante",
			label: 			"Fabricante",
			tipo: 			"select2",
			mostrarEditar: 	true,
			mostrarCrear: 	true,
			config: 		{
							  minimumInputLength: 0,
							  placeholder: 'Fabricante',
							  allowClear: false,
							  width: 'resolve',
							  id: function(e) { return e.id; },
							  formatNoMatches: function() { return 'Sin resultados'; },
							  formatSearching: function(){ return "Buscando..."; },
							  ajax: {
							      url: "api.php",
							      dataType: 'json',
							      quietMillis: 200,
							      data: function(term, page) {
							          return {
							              module: 'faltantes',
							              run: 'fabricantes_listar',
							              q: term,
							              p: page
							          };
							      },
							      results: function(data, page ) {
							          //If session expire reload page...
							          if(!data.logued) location.reload();

							          var more = (page * 40) < data.result.total; // whether or not there are more results available

							          // notice we return the value of more so Select2 knows if more results can be loaded
							          return {results: data.result.items, more: more};
							      }
							  },
							  formatInputTooShort: function () {
							            return "";
							        },  
							  formatResult: function(item) {
							      return item.nombre; 
							  },
							  formatSelection: function(item) {
							  	return item.nombre; 
							  },
							  initSelection : function (element, callback) {
							      var elementText = $(element).attr('data-init-text');
							  }
							}
		},
		{
			id: 			"orden",
			label: 			"Orden",
			tipo: 			"select2",
			mostrarEditar: 	true,
			mostrarCrear: 	true,
			config: 		{	data: 	[	{id:'por_fecha',text:'Ordenar por Fecha'},
											{id:'por_codigo',text:'Ordenar por Código'}
										],
								minimumResultsForSearch: -1,
								placeholder: 'Orden (por Fecha por defecto)'
							}
		},
		{	
			id: 			"rango",
			label: 			"Rango de Fechas",
			labelDesde: 	'Desde',
			labelHasta: 	'Hasta',
			tipo: 			"daterange",
			mostrarEditar: 	true,
			mostrarCrear: 	true,
			config: 		{
							format: 'dd/mm/yyyy',
							autoclose: true,
							language: 'es'
			}
		}
	]
};
{/if}

{if="$GLOBALS['permisos']['faltantes_reporte_generar'] || $GLOBALS['permisos']['faltantes_reporte_eliminar']"}
//Lista
function reporte_faltantes(){

	//Creamos el popup
	abrir_modal(reporte_faltantes_config.modName, 550, false);

	//Mostramos la ventana
	$('#'+reporte_faltantes_config.modName+'Modal').modal({show:true});

	//Titulo...
	$('#'+reporte_faltantes_config.modName+'Modal_titulo').html('Reporte de '+reporte_faltantes_config.modLabelPlural);

	//Contenido
	var contenido = $('<div class="scrollable" style="max-height: '+reporte_faltantes_config.max_height_edita_agrega+'px; overflow-y: hidden;"></div>');


	//Agregamos los campos
	for(var k in reporte_faltantes_config.campos)
		contenido.append( genera_campo(reporte_faltantes_config.campos[k], reporte_faltantes_config.modName ,true) );


	//Agregamos los tooltips
	//contenido.tooltip({selector: '[data-toggle="tooltip"]', placement: 'left', animation: false});

	//Generamos el listado
	$('#'+reporte_faltantes_config.modName+'Modal_container').empty();
	$('#'+reporte_faltantes_config.modName+'Modal_footer').empty();
	$('#'+reporte_faltantes_config.modName+'Modal_container').append(contenido);


	//Agregamos scrollbars
	$('#'+reporte_faltantes_config.modName+'Modal_container .scrollable').mCustomScrollbar({theme: 'minimal-dark',scrollInertia: 100, mouseWheel:{ scrollAmount: 70 }});

	//Boton Imprimir
	$('#'+reporte_faltantes_config.modName+'Modal_footer').append('<button type="button" class="btn btn-primary" style="display: block; width: 100%; margin: 10px auto;" onclick="'+reporte_faltantes_config.modName+'_generar();">Generar Reporte</button>');

	{if="$GLOBALS['permisos']['faltantes_reporte_eliminar']"}
	//Boton Borrar
	$('#'+reporte_faltantes_config.modName+'Modal_footer').append('<button type="button" class="btn btn-danger" style="display: block; width: 100%; margin: 10px auto;" onclick=" confirma(\'Eliminar '+reporte_faltantes_config.modLabelPlural+'\',\'Los '+reporte_faltantes_config.modLabelPlural+' se eliminarán de forma permanente.\',\'exclamacion\',\'danger\',\''+reporte_faltantes_config.modName.toLowerCase()+'_eliminar()\');">Eliminar Faltantes</button>');
	{/if}

	//Boton Cerrar
	$('#'+reporte_faltantes_config.modName+'Modal_footer').append('<button type="button" data-dismiss="modal" class="btn btn-warning" style="display: block; width: 100%; margin: 10px auto;">Cerrar</button>');
}
{/if}

{if="$GLOBALS['permisos']['faltantes_reporte_eliminar']"}
//Eliminar
function reporte_faltantes_eliminar(){
	//Data array
	var data = {};

	//Fabricante
	var fabricante = $('#reporte_faltantes_fabricante').select2('data');
	if(fabricante != null)
		data['fabricante'] = fabricante.id;

	data['fecha_desde'] = $('#reporte_faltantes_rango_desde').val();
	data['fecha_hasta'] = $('#reporte_faltantes_rango_hasta').val();
	
	apix('faltantes','reporte_eliminar',data, {
		ok: function(json){
			//Notifica
			notifica(	reporte_faltantes_config.modLabelSingular+' '+reporte_faltantes_config.eliminaLabel,
						reporte_faltantes_config.artLabel+' '+reporte_faltantes_config.modLabelSingular+' se eliminó con éxito',
						'informacion',
						'info'
					);
		}
	});
}
{/if}

{if="$GLOBALS['permisos']['faltantes_reporte_generar']"}
//Generar
function reporte_faltantes_generar(){
	//Data array
	var data = {};

	//Fabricante
	data['fabricante'] = '0';
	var fabricante = $('#reporte_faltantes_fabricante').select2('data');
	if(fabricante != null) data['fabricante'] = fabricante.id;

	//Fecha
	data['fecha_desde'] = $('#reporte_faltantes_rango_desde').val();
	data['fecha_hasta'] = $('#reporte_faltantes_rango_hasta').val();

	//Orden
	data['orden'] = 'por_fecha';
	var orden = $('#reporte_faltantes_orden').select2('data');
	if(orden != null) data['orden'] = orden.id;
	
	//DB
	apix('faltantes','reporte_imprimir',data, {
		ok: function(json){
			//Cambiamos el ancho de la ventana
			abrir_modal(reporte_faltantes_config.modName, 700, false);

			//Contenido
			var contenido = $('<div class="scrollable" style="max-height: '+reporte_faltantes_config.max_height_reporte+'px; overflow-y: hidden;"></div>');

			//Agregamos el reporte al contenido
			if(json.result.items.length) contenido.append( reporte(json.result) );
			else contenido.append( $('<div style="text-align: center; font-color: silver; height: 150px;"><br><br>No se registran Faltantes para este Fabricante.</div>') );

			//Limpiamos contenidos anteriores
			$('#'+reporte_faltantes_config.modName+'Modal_container').empty();
			$('#'+reporte_faltantes_config.modName+'Modal_footer').empty();

			//Agregamos el reporte a la ventana
			$('#'+reporte_faltantes_config.modName+'Modal_container').html(contenido);

			//Agregamos scrollbars
			$('#'+reporte_faltantes_config.modName+'Modal_container .scrollable').mCustomScrollbar({theme: 'minimal-dark',scrollInertia: 100, mouseWheel:{ scrollAmount: 70 }});



			//Boton Imprimir
			$('#'+reporte_faltantes_config.modName+'Modal_footer').append('<button type="button" class="btn btn-primary" style="display: block; width: 100%; margin: 10px auto;" onclick="'+reporte_faltantes_config.modName+'_imprimir();">Imprimir Reporte</button>');
			
			//Boton Exportar a Excel
			$('#'+reporte_faltantes_config.modName+'Modal_footer').append('<button type="button" class="btn btn-primary" style="display: block; width: 100%; margin: 10px auto;" onclick="'+reporte_faltantes_config.modName+'_exp_excel();">Exportar a Excel</button>');

			//Boton eliminar Seleccionados
			$('#'+reporte_faltantes_config.modName+'Modal_footer').append('<button type="button" class="btn btn-danger" style="display: block; width: 100%; margin: 10px auto;" onclick="reportes_faltantes_eliminar_seleccionados();">Eliminar Faltantes Seleccionados</button>');

			//Boton Volver
			$('#'+reporte_faltantes_config.modName+'Modal_footer').append('<button type="button" class="btn btn-warning" style="display: block; width: 100%; margin: 10px auto;" onclick="'+reporte_faltantes_config.modName+'();">Volver</button>');

		}
	});
}
{/if}

{if="$GLOBALS['permisos']['faltantes_reporte_generar']"}
//Imprimir
function reporte_faltantes_imprimir(){
	$('#'+reporte_faltantes_config.modName+'Modal_container .reporte').print({
	    addGlobalStyles : true,
	    stylesheet : './css/reporte.css',
	    rejectWindow : true,
	    noPrintSelector : ".no-print",
	    iframe : true,
	    append : null,
	    prepend : null
	});
}
{/if}

{if="$GLOBALS['permisos']['faltantes_reporte_generar']"}
//Exportar a excel
function reporte_faltantes_exp_excel(){

	//vars
	var contenido = $('#'+reporte_faltantes_config.modName+'Modal_container table.reporte');
	var excel = [];

	//Generamos el array
	$('tr', contenido ).each(function() {
		var arrayOfThisRow = [];
		var tableData = $(this).find('td');
		if (tableData.length > 0) {
			tableData.each(function() { arrayOfThisRow.push($(this).text()); });
			excel.push(arrayOfThisRow);
		}
	});

	//API
	var data = {};
	data['datos'] = JSON.stringify(excel);
	apix('faltantes','reporte_exportar_xls',data, {
		ok: function(json){
			$("body").append("<iframe src='./tmp/reporte.xls' style='display: none;'></iframe>");
		}
	});
}
{/if}

{if="$GLOBALS['permisos']['faltantes_reporte_generar']"}
function reportes_faltantes_eliminar_seleccionados(){
	
	var json = [];
	$('.reporte').find('input:checked').each( function(){
		json.push( parseInt($(this).val()) );
	});

	//Si no seleccionamos ninguno
	if(!json.length){
		notifica(	'Eliminar faltantes',
					'Debe seleccionar al menos un faltante para eliminar',
					'informacion','info');
		return;
	}

	var data = {};
	data['datos'] = JSON.stringify(json);
	apix('faltantes','reporte_eliminar_seleccionados',data, {
		ok: function(json){
			var jquery_selector = [];
			for(var i in json.result.faltantes_eliminados)
				jquery_selector.push(".reporte input[value='" + json.result.faltantes_eliminados[i] + "']");

			//Removemos
			var filas = $(jquery_selector.join(', ')).parent().parent();
			filas.remove();

			//Cantidades
			var cantidades = 'No se elimino ningun faltante';
			if(jquery_selector.length == 1)  cantidades = 'Se eliminó un faltante con éxito.';
			if(jquery_selector.length > 1)  cantidades = 'Se eliminaron '+ jquery_selector.length +' faltantes con éxito.';

			//informamos
			notifica('Eliminar faltantes' , cantidades, 'confirmacion','success');
		}
	});
}

{/if}

</script>