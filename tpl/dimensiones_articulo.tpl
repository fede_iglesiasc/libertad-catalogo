<script type="text/javascript">

{if="$GLOBALS['permisos']['articulos_edicion_dimensiones']"}

function dimensiones_articulo_listar(id,cod){

  //Creamos el popup
  abrir_modal('dimensiones_articulo', 600, false);

  //Mostramos la ventana
  $('#dimensiones_articuloModal').modal({show:true});

  //Titulo...
  $('#dimensiones_articuloModal_titulo').html('Dimensiones del artículo <b>' + cod.replace(/ /g, '') + '</b>');

  //Cargando...
  $('#dimensiones_articuloModal_container').html( cargando() );

  //Footer...
  $('#dimensiones_articuloModal_footer').html('');

  //Traemos los datos de la API
  var data = {};
  data['articulo'] = id;

  apix('dimensiones','listar_x_articulo',data, {
    ok: function(json){
      //Estructura del 
      var contenido = $('<div class="scrollable" id="usuarios_lista" style="padding: 3px 0px 10px 0px; max-height: 300px; overflow-y: hidden; margin-bottom: 0px;"></div>');

      //Agregamos items
      for(var k in json.result){

        //Funcion que genera los campos
        if(!json.result[k].opciones){
          contenido.append( genera_campo({ 
                                  id:         json.result[k].id,
                                  label:      json.result[k].nombre,
                                  tipo:       "input",
                                  mostrarEditar:  true,
                                  mostrarCrear:   true
          },'dimensiones',true));

          //Damos valor
          $('#dimensiones_'+json.result[k].id, contenido).val_(json.result[k].val);
        }
      }


      //Creamos los selects
      for(var k in json.result)
        if(json.result[k].opciones){

          contenido.append( genera_campo({
                                  id:         json.result[k].id,
                                  label:      json.result[k].nombre,
                                  tipo:       "select2",
                                  mostrarEditar:  true,
                                  mostrarCrear:   true,
                                  config:     {
                                      minimumInputLength: 0,
                                      placeholder: $(this).attr('placeholder'),
                                      width: 'resolve',
                                      data: json.result[k].opciones,
                                      id: function(e) { return e.id; },
                                      formatNoMatches: function() { return 'Sin resultados'; },
                                      formatSearching: function(){ return "Buscando..."; },
                                      formatInputTooShort: function () {
                                        return "";
                                      },
                                      formatResult: function(item) {
                                        return item.nombre;
                                      },
                                      formatSelection: function(item) { 
                                        return item.nombre;
                                      },
                                      initSelection : function (element, callback) {
                                        var elementText = $(element).attr('data-init-text');
                                      }
                                  }
          },'dimensiones',true));

          //Asignamos valor
          if(json.result[k].val != '')
            $('#dimensiones_'+json.result[k].id, contenido).val_(json.result[k].val);
        }


      //Generamos el listado de usuarios
      $('#dimensiones_articuloModal_container').empty();
      $('#dimensiones_articuloModal_container').append(contenido);

      //Agregamos scrollbars
      $('.scrollable').mCustomScrollbar({theme: 'minimal-dark',scrollInertia: 100});

      //Boton Confirmar cambios
      $('#dimensiones_articuloModal_footer').append('<button type="button" class="btn btn-primary" style="display: block; width: 100%; margin: 10px auto;" onclick="dimensiones_articulo_editar('+id+');">Guardar Cambios</button>');

      //Append Cancel Button
      $('#dimensiones_articuloModal_footer').append('<button type="button" data-dismiss="modal" class="btn btn-warning" style="display: block; width: 100%; margin: 10px auto;">Descartar Cambios</button>');
    }
  });
}

function dimensiones_articulo_editar(id){
  var dimensiones = {};
  $('#dimensiones_articuloModal_container [id^="dimensiones_"]').each(function(){
    var id = $(this).attr('id').replace('dimensiones_','');
    var value = $(this).val_();
    if(typeof value === 'undefined') value = '';
    if(typeof value === 'object'){
      if(value != null) value = value.id;
      else value = '';
    }

    //Guardamos la dimension
    dimensiones[id] = value;
  });

  //Llamada a la API
  var data = {};
  data['articulo'] = id;
  data['dimensiones'] = JSON.stringify(dimensiones);
  apix('dimensiones','editar_x_articulo',data, {
    ok: function(json){
      //Si todo salio ok mostramos el cartel y cerramos la ventana
      notifica('Dimensiones guardadas','Las dimensiones se modificaron con éxito','confirmacion','success');
      //cerramos la ventana...
      $('#dimensiones_articuloModal').modal('hide');
      //Actualizamos la lista de articulos
      watable_volver = true;
      articulosUpdate();
    }
  });
}


{/if}

</script>