<!doctype html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="en-US">
<!--<![endif]-->

<head>
    <meta charset="utf-8">
    <!-- scaling not possible (for smartphones, ipad, etc.) -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Distribuidora Libertad</title>
    <link rel='stylesheet' id='tp_twitter_plugin_css-css' href='./institucional/css/tp_twitter_plugin.css?ver=1.0' type='text/css' media='screen' />
    <link rel='stylesheet' href='./institucional/css/jquery.fancybox.css' type='text/css' media='screen' />
    <link rel='stylesheet' id='rs-plugin-settings-css' href='./institucional/css/settings.css?rev=4.5.6&#038;ver=4.1.11' type='text/css' media='all' />

    <link rel='stylesheet' id='default-style-css' href='./institucional/css/style.css?ver=1.4.1' type='text/css' media='all' />
    <link rel='stylesheet' id='flexslider-style-css' href='./institucional/css/flexslider.css?ver=1.0' type='text/css' media='all' />
    <link rel='stylesheet' id='fontawesome-style-css' href='./institucional/css/font-awesome.min.css?ver=3.2.1' type='text/css' media='all' />
    <link rel='stylesheet' id='scrollbar-style-css' href='./institucional/css/perfect.scrollbar.css?ver=3.2.1' type='text/css' media='all' />
    <link rel='stylesheet' id='retina-style-css' href='./institucional/css/retina.css?ver=1.0' type='text/css' media='all' />
    <link rel='stylesheet' id='isotope-style-css' href='./institucional/css/isotope.css?ver=1.0' type='text/css' media='all' />
    <link rel='stylesheet' id='mqueries-style-css' href='./institucional/css/mqueries.css?ver=1.0' type='text/css' media='all' />
    <link rel='stylesheet' id='custom-style-css' href='./institucional/css/custom-style.css?ver=1.0' type='text/css' media='all' />
    <link rel='stylesheet' id='sr_fonts-css' href='https://fonts.googleapis.com/css?family=Open+Sans%3A300%2C600%2C400%2C800%7CRaleway%3A300%2C900%2C600&#038;subset=latin%2Ccyrillic-ext&#038;ver=4.1.11' type='text/css' media='all' />
    <script type='text/javascript' src='./institucional/js/jquery.js?ver=1.11.1'></script>
    <script type='text/javascript' src='./institucional/js/jquery-migrate.min.js?ver=1.2.1'></script>
    <script type='text/javascript' src='./institucional/js/jquery.modernizr.min.js?ver=2.0.6'></script>
    
    <link rel="icon" href="./institucional/img/favicons.png" />
</head>

<body class="home page page-id-23 page-template page-template-template-onepage page-template-template-onepage-php">
    
    <!-- PAGELOADER -->
    <div id="page-loader">
        <div class="page-loader-inner">
            <div class="loader-logo"><img src="./institucional/img/logo.png" alt="Logo" /></div>
            <div class="loader-icon"><span class="spinner"></span><span></span></div>
        </div>
    </div>
    <!-- PAGELOADER --> 
    
    <!-- PAGE CONTENT -->
    <div id="page-content" class="fixed-header">
        
        <!-- HEADER -->
        <header class="light-header logo-left ">

            <div class="header-inner wrapper clearfix">
                
                <div id="logo" > <a id="defaut-logo" class="logotype" href="#"><img src="./institucional/img/logo.png" alt="Logo"></a> </div>

                <!-- MENU -->
                <div class="menu menu-light clearfix">
                    <nav id="main-nav" class="menu-main-menu-container">
                        <ul id="primary" class="">
                            <li class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-23 current_page_item"><a href="#section-home" class="scroll-to">Inicio</a></li>
                            <li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="#about" class="scroll-to">Nosotros
                            </a></li>
                            <li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="#section-productos" class="scroll-to">Productos
                            </a></li>
                            <li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="#section-contactook" class="scroll-to">Contacto
                            </a></li>
                            <li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="#as" class="scroll-to"><input type="submit" name="submit_form" class="submit" value="Acceso a Clientes" onclick="login();" style="font-size: 12px !important; background: #4da5f5; padding: 4px 10px; margin: 15px 0px;" />
                            </a></li>
                        </ul>
                    </nav>
                </div>
                <!-- MENU --> 
            </div>
        </header>
        <!-- HEADER -->

        <!-- PAGEBODY -->
        <div class="page-body">


            <!-- HOME -->
            <section id="section-home" style="margin-top: -60px;">

                <video id="video0" preload="auto" autoplay="autoplay" loop="loop" muted="muted" poster="./institucional/img/mdp2x.jpg" style="width: 100%"><source src="./institucional/video/final1.mp4" type="video/mp4">bgvideo</video>
                <div><a href="https://es-la.facebook.com/DunaFilms/" style="color: #9a9898; position: relative; top: -70px; font-size: 12px; margin-left: 10px;">Video cortesia de DunaFilms</a></div>
            </section>


           <!-- NUESTRA HISTORIA --> 
            <section id="about">
                <div class="section-inner">
                
                <div class="wrapper">
                    
                    <div class="section-title">
                        <h2>Bienvenidos a <span style="color: #4da5f5;">Distribuidora Libertad</span></h2>
                        <div class="seperator size-small"><span></span></div>
                        <h4 class="subtitle">UNA EMPRESA DE MAR DEL PLATA</h4>
                    </div>
                    
                    <div class="column-section clearfix">
                        <div class="column one-half sr-animation sr-animation-fromleft" data-delay="200">
                            <img src="./institucional/img/entrada.jpg" alt="IMAGENAME"/>
                        </div> 
                        <div class="column one-half last-col">
                        <h4><strong>Algo sobre nosotros...</strong></h4>
                            <p align="justify" style="text-indent: 5em;">Distribuidora Libertad es una empresa de origen familiar. Desde 1983 nos encontramos abocados a la comercialización y distribución de autopartes de tren delantero, suspensión y freno. Prestamos atención personalizada a cada uno de nuestros clientes, donde el personal que lo atiende se encuentra capacitado para interpretar y adaptarse a sus necesidades, despachando los pedidos en el día con altos niveles de cumplimiento. </p> <p align="justify" style="text-indent: 5em;">Además, dentro de nuestros recursos humanos contamos con profesionales que lo ayudarán a responder sus inquietudes. De esta manera hemos logrado llevar el dinamismo, capacidad y eficiencia a su máxima expresión en el compromiso de satisfacer las necesidades del cliente. Un profundo conocimiento del sector, capacitación permanente, reinversión de capitales y el aval de las prestigiosas marcas que representamos, hacen de Distribuidora Libertad una empresa en contínua expansión, protagonista del presente, trabajando para el futuro.</p>
                            <p><!--<a href="#" class="sr-button sr-button2 small-button">Conozca nuestro Equipo</a></p>-->
                        </div>
                    </div>
                    

                    <div class="spacer spacer-big"></div>
                    <div class="spacer spacer-big"></div>
                </div>

                <div class="wrapper">
                    <div class="horizontalsection text-light clearfix section-title" style="padding-top:100px;padding-bottom:100px;">
                        <h2 style="text-align: center; text-shadow: 0px 3px 3px rgba(0, 0, 0, 0.80);">Protagonista del presente, trabajando para el futuro</h2>
                        <div class="horizontalinner parallax-section" style="background:url(./institucional/img/bg12.jpg) center center repeat; background-size: cover; heigh: 300px;"></div>
                    </div>
                </div>

                </div>
            </section>
            <!-- NUESTRA HISTORIA -->


            <!-- FABRICANTES -->
            <section id="section-productos" class="">
                <div class="section-inner">
                    <div class="wrapper">
                        <div class="section-title">
                            <h2>Nuestros Proveedores</h2>
                            <div class="seperator size-small"><span></span></div>
                        </div>
                    </div>
                    <!-- AJAX AREA -->
                    <div id="ajax-portfolio-29" class="ajax-section">
                        <div id="ajax-loader">
                            <div class="loader-icon"><span class="spinner"></span><span></span></div>
                        </div>
                        <div class="ajax-content clearfix">
                            <!-- THE LOADED CONTENT WILL BE ADDED HERE -->
                        </div>
                    </div>
                    <!-- AJAX AREA -->
                    <ul id="menu-portfolio-filter" class="filter clearfix" data-related-grid="portfolio-grid-29">
                        <li><a class="active" data-option-value="*">Todos</a></li>
                        <li><a data-option-value=".encendido" href="#" title="Encendido">Encendido</a></li>
                        <li><a data-option-value=".fluidosyquimicos" href="#" title="Fluidos y Quimicos">Fluidos y Quimicos</a></li>
                        <li><a data-option-value=".frenos" href="#" title="Frenos">Frenos y Embragues</a></li>
                        <li><a data-option-value=".suspensionydireccion" href="#" title="Suspensión y dirección">Suspensión y dirección</a></li>
                        <li><a data-option-value=".transmision" href="#" title="Transmisión">Transmisión</a></li>
                    </ul>
                    <div id="portfolio-grid-29" class="masonry portfolio-entries clearfix grid-works" data-maxitemwidth="250" data-ajax="ajax-portfolio-29">
                        
                        <!-- Mattioli -->
                        <div class="portfolio-entry masonry-item suspensionydireccion">
                            <div class="entry-thumb portfolio-thumb">
                                <div class="imgoverlay text-light">
                                    <a href="javascript:void(0);" data-type="portfolio">
                                        <img width="540" height="320" src="./institucional/img/mattioli.png" class="attachment-portfolio-crop-thumb wp-post-image" alt="g-dark" />
                                        <div class="overlay"><span class="overlaycolor"></span>
                                            <div class="overlayinfo">
                                                <h6>BRAZOS Y RÓTULAS DE SUSPENSIÓN, ACOPLES, BARRAS, BRAZOS Y EXTREMOS DE DIRECCIÓN</h6> 
                                                <a href="http://mattioli.com.ar/" target="_blank">www.mattioli.com.ar</a>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>

                        <!-- Griffo -->
                        <div class="portfolio-entry masonry-item suspensionydireccion transmision">
                            <div class="entry-thumb portfolio-thumb">
                                <div class="imgoverlay text-light">
                                    <a href="javascript:void(0);" data-type="portfolio">
                                        <img width="540" height="320" src="./institucional/img/griffo.png" class="attachment-portfolio-crop-thumb wp-post-image" alt="g-dark" />
                                        <div class="overlay"><span class="overlaycolor"></span>
                                            <div class="overlayinfo">
                                                <h6>TOPES Y FUELLES DE DIRECCIÓN, TRANSMISIÓN Y SUSPENSIÓN.</h6> 
                                                <a href="http://www.griffo.com.ar/" target="_blank">www.griffo.com.ar</a>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>

                        <!-- Solmi -->
                        <div class="portfolio-entry masonry-item branding ">
                            <div class="entry-thumb portfolio-thumb">
                                <div class="imgoverlay text-light">
                                    <a href="javascript:void(0);" data-type="portfolio">
                                        <img width="540" height="320" src="./institucional/img/solmi.png" class="attachment-portfolio-crop-thumb wp-post-image" alt="g-dark" />
                                        <div class="overlay"><span class="overlaycolor"></span>
                                            <div class="overlayinfo">
                                                <h6>Contrapesos para balanceo.</h6> 
                                                <a href="http://www.contrapesossolmi.com.ar/" target="_blank">www.contrapesossolmi.com.ar</a>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>

                        <!-- Fabila -->
                        <div class="portfolio-entry masonry-item suspensionydireccion">
                            <div class="entry-thumb portfolio-thumb">
                                <div class="imgoverlay text-light">
                                    <a href="javascript:void(0);" data-type="portfolio">
                                        <img width="540" height="320" src="./institucional/img/fabila.png" class="attachment-portfolio-crop-thumb wp-post-image" alt="g-dark" />
                                        <div class="overlay"><span class="overlaycolor"></span>
                                            <div class="overlayinfo">
                                                <h6>ACOPLES, BARRAS, EXTREMOS Y PRECAPS DE DIRECCIÓN. BIELETAS Y RÓTULAS DE SUSPENSIÓN</h6>
                                                <a href="http://www.fabila.com.ar/" target="_blank">www.fabila.com.ar</a>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>

                        <!-- Fremax -->
                        <div class="portfolio-entry masonry-item frenos">
                            <div class="entry-thumb portfolio-thumb">
                                <div class="imgoverlay text-light">
                                    <a href="javascript:void(0);" data-type="portfolio">
                                        <img width="540" height="320" src="./institucional/img/fremax.png" class="attachment-portfolio-crop-thumb wp-post-image" alt="g-dark" />
                                        <div class="overlay"><span class="overlaycolor"></span>
                                            <div class="overlayinfo">
                                                <h6>DISCOS, CAMPANAS Y PASTILLAS DE FRENO</h6>
                                                <a href="http://www.fremax.com.ar/" target="_blank">www.fremax.com.ar</a>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>

                        <!-- Gacri -->
                        <div class="portfolio-entry masonry-item soportes suspensionydireccion">
                            <div class="entry-thumb portfolio-thumb">
                                <div class="imgoverlay text-light">
                                    <a href="javascript:void(0);" data-type="portfolio">
                                        <img width="540" height="320" src="./institucional/img/gacri.png" class="attachment-portfolio-crop-thumb wp-post-image" alt="g-dark" />
                                        <div class="overlay"><span class="overlaycolor"></span>
                                            <div class="overlayinfo">
                                                <h6>SOPORTE DE MOTOR</h6>
                                                <a href="http://www.gacri.com.ar/" target="_blank">www.gacri.com.ar</a>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>

                        <!-- Garma -->
                        <div class="portfolio-entry masonry-item transmision">
                            <div class="entry-thumb portfolio-thumb">
                                <div class="imgoverlay text-light">
                                    <a href="javascript:void(0);" data-type="portfolio">
                                        <img width="540" height="320" src="./institucional/img/garma.png" class="attachment-portfolio-crop-thumb wp-post-image" alt="g-dark" />
                                        <div class="overlay"><span class="overlaycolor"></span>
                                            <div class="overlayinfo">
                                                <h6>JUNTAS HOMOCINÉTICAS, SEMIEJES Y FUELLES DE TRANSMISIÓN</h6>
                                                <a href="http://www.cvjoint.com.ar/" target="_blank">www.cvjoint.com.ar</a>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>

                        <!-- Leo -->
                        <div class="portfolio-entry masonry-item suspensionydireccion">
                            <div class="entry-thumb portfolio-thumb">
                                <div class="imgoverlay text-light">
                                    <a href="javascript:void(0);" data-type="portfolio">
                                        <img width="540" height="320" src="./institucional/img/leo.png" class="attachment-portfolio-crop-thumb wp-post-image" alt="g-dark" />
                                        <div class="overlay"><span class="overlaycolor"></span>
                                            <div class="overlayinfo">
                                                <h6>COLUMNAS Y MANCHONES DE DIRECCIÓN, BIELETAS DE SUSPENSIÓN</h6>
                                                <a href="http://www.autopartesleo.com.ar/" target="_blank">www.autopartesleo.com.ar</a>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>

                        <!-- Difrani -->
                        <div class="portfolio-entry masonry-item branding ">
                            <div class="entry-thumb portfolio-thumb">
                                <div class="imgoverlay text-light">
                                    <a href="javascript:void(0);" data-type="portfolio">
                                        <img width="540" height="320" src="./institucional/img/difrani.png" class="attachment-portfolio-crop-thumb wp-post-image" alt="g-dark" />
                                        <div class="overlay"><span class="overlaycolor"></span>
                                            <div class="overlayinfo">
                                                <h6>BARRAS, BRAZOS Y EXTREMOS DE DIRECCIÓN, BRAZOS Y RÓTULAS DE SUSPENSIÓN</h6>
                                                <a href="http://www.difrani.com.ar/" target="_blank">www.difrani.com.ar</a>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>

 
                        <!-- Aston -->
                        <div class="portfolio-entry masonry-item">
                            <div class="entry-thumb portfolio-thumb">
                                <div class="imgoverlay text-light">
                                    <a href="javascript:void(0);" data-type="portfolio">
                                        <img width="540" height="320" src="./institucional/img/aston.png" class="attachment-portfolio-crop-thumb wp-post-image" alt="g-dark" />
                                        <div class="overlay"><span class="overlaycolor"></span>
                                            <div class="overlayinfo">
                                                <h6>Chapones cubrecarters</h6>
                                                <a href="http://www.aston.com.ar/" target="_blank">www.aston.com.ar</a>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>

 

                        <!-- Bator -->
                        <div class="portfolio-entry masonry-item suspensionydireccion">
                            <div class="entry-thumb portfolio-thumb">
                                <div class="imgoverlay text-light">
                                    <a href="javascript:void(0);" data-type="portfolio">
                                        <img width="540" height="320" src="./institucional/img/bator.png" class="attachment-portfolio-crop-thumb wp-post-image" alt="g-dark" />
                                        <div class="overlay"><span class="overlaycolor"></span>
                                            <div class="overlayinfo">
                                                <h6>Barras de torsión</h6>
                                                <a href="http://www.batorsrl.com.ar/" target="_blank">www.batorsrl.com.ar</a>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>

 
                        <!-- Tribuno -->
                        <div class="portfolio-entry masonry-item fluidosyquimicos">
                            <div class="entry-thumb portfolio-thumb">
                                <div class="imgoverlay text-light">
                                    <a href="javascript:void(0);" data-type="portfolio">
                                        <img width="540" height="320" src="./institucional/img/tribuno.png" class="attachment-portfolio-crop-thumb wp-post-image" alt="g-dark" />
                                        <div class="overlay"><span class="overlaycolor"></span>
                                            <div class="overlayinfo">
                                                <h6>LUBRICANTES, GRASAS, DESENGRASANTES, ADITIVOS Y COSMÉTICA AUTOMOTIVA</h6>
                                                <a href="http://www.tribunohome.com/" target="_blank">www.tribunohome.com</a>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>

 
                        <!-- Porpora -->
                        <div class="portfolio-entry masonry-item suspensionydireccion">
                            <div class="entry-thumb portfolio-thumb">
                                <div class="imgoverlay text-light">
                                    <a href="javascript:void(0);" data-type="portfolio">
                                        <img width="540" height="320" src="./institucional/img/porpora.png" class="attachment-portfolio-crop-thumb wp-post-image" alt="g-dark" />
                                        <div class="overlay"><span class="overlaycolor"></span>
                                            <div class="overlayinfo">
                                                <h6>BARRAS, BRAZOS Y EXTREMOS DE DIRECCIÓN</h6>
                                                <a href="http://www.porpora.com.ar/" target="_blank">www.porpora.com.ar</a>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>

 
                        <!-- VTH -->
                        <div class="portfolio-entry masonry-item soportes suspensionydireccion">
                            <div class="entry-thumb portfolio-thumb">
                                <div class="imgoverlay text-light">
                                    <a href="javascript:void(0);" data-type="portfolio">
                                        <img width="540" height="320" src="./institucional/img/vth.png" class="attachment-portfolio-crop-thumb wp-post-image" alt="g-dark" />
                                        <div class="overlay"><span class="overlaycolor"></span>
                                            <div class="overlayinfo">
                                                <h6>BUJES SILENTBLOCK, SOPORTES Y CAZOLETAS  DE SUSPENSIÓN</h6>
                                                <a href="http://www.tamarit.com.ar/" target="_blank">www.tamarit.com.ar</a>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>

 
                        <!-- RSF -->
                        <div class="portfolio-entry masonry-item suspensionydireccion">
                            <div class="entry-thumb portfolio-thumb">
                                <div class="imgoverlay text-light">
                                    <a href="javascript:void(0);" data-type="portfolio">
                                        <img width="540" height="320" src="./institucional/img/rsf.png" class="attachment-portfolio-crop-thumb wp-post-image" alt="g-dark" />
                                        <div class="overlay"><span class="overlaycolor"></span>
                                            <div class="overlayinfo">
                                                <h6>BUJES SILENTBLOCK Y CAZOLETAS DE SUSPENSIÓN</h6>
                                                <a href="http://www.ritossa.com.ar/" target="_blank">www.ritossa.com.ar</a>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>

 
                        <!-- Corven -->
                        <div class="portfolio-entry masonry-item embragues frenos suspensionydireccion transmision">
                            <div class="entry-thumb portfolio-thumb">
                                <div class="imgoverlay text-light">
                                    <a href="javascript:void(0);" data-type="portfolio">
                                        <img width="540" height="320" src="./institucional/img/corven.png" class="attachment-portfolio-crop-thumb wp-post-image" alt="g-dark" />
                                        <div class="overlay"><span class="overlaycolor"></span>
                                            <div class="overlayinfo">
                                                <h6>AMORTIGUADORES. CAMPANAS, DISCOS Y PASTILLAS DE FRENO. EMBRAGUES, SEMIEJES Y HOMOCINÉTICAS.</h6>
                                                <a href="http://www.corven.com.ar/" target="_blank">www.corven.com.ar</a>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>

 
                        <!-- DRL -->
                        <div class="portfolio-entry masonry-item frenos">
                            <div class="entry-thumb portfolio-thumb">
                                <div class="imgoverlay text-light">
                                    <a href="javascript:void(0);" data-type="portfolio">
                                        <img width="540" height="320" src="./institucional/img/drl.png" class="attachment-portfolio-crop-thumb wp-post-image" alt="g-dark" />
                                        <div class="overlay"><span class="overlaycolor"></span>
                                            <div class="overlayinfo">
                                                <h6>CUBETAS, FLEXIBLES y PISTONES DE CALIPER DE FRENO</h6>
                                                <a href="http://www.difrani.com.ar/" target="_blank"></a>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>

                        <!-- Bendix -->
                        <div class="portfolio-entry masonry-item frenos">
                            <div class="entry-thumb portfolio-thumb">
                                <div class="imgoverlay text-light">
                                    <a href="javascript:void(0);" data-type="portfolio">
                                        <img width="540" height="320" src="./institucional/img/bendix.png" class="attachment-portfolio-crop-thumb wp-post-image" alt="g-dark" />
                                        <div class="overlay"><span class="overlaycolor"></span>
                                            <div class="overlayinfo">
                                                <h6>Pastillas de Freno</h6>
                                                <a href="http://www.bendix.com/es/" target="_blank">www.bendix.com</a>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>

 
                        <!-- CTR -->
                        <div class="portfolio-entry masonry-item suspensionydireccion">
                            <div class="entry-thumb portfolio-thumb">
                                <div class="imgoverlay text-light">
                                    <a href="javascript:void(0);" data-type="portfolio">
                                        <img width="540" height="320" src="./institucional/img/ctr.png" class="attachment-portfolio-crop-thumb wp-post-image" alt="g-dark" />
                                        <div class="overlay"><span class="overlaycolor"></span>
                                            <div class="overlayinfo">
                                                <h6>BARRAS, BRAZOS Y EXTREMOS DE DIRECCIÓN, BRAZOS Y RÓTULAS DE SUSPENSIÓN</h6>
                                                <a href="http://www.ctr.co.kr/eng/main/main.php" target="_blank">www.ctr.co.kr</a>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>

 
                        <!-- Rey Goma -->
                        <div class="portfolio-entry masonry-item soportes suspensionydireccion">
                            <div class="entry-thumb portfolio-thumb">
                                <div class="imgoverlay text-light">
                                    <a href="javascript:void(0);" data-type="portfolio">
                                        <img width="540" height="320" src="./institucional/img/reygoma.png" class="attachment-portfolio-crop-thumb wp-post-image" alt="g-dark" />
                                        <div class="overlay"><span class="overlaycolor"></span>
                                            <div class="overlayinfo">
                                                <h6>SOPORTES DE MOTOR</h6>
                                                <a href="http://www.reygoma.com.ar/" target="_blank">www.reygoma.com.ar</a>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>

 
                        <!-- SDA -->
                        <div class="portfolio-entry masonry-item suspensionydireccion">
                            <div class="entry-thumb portfolio-thumb">
                                <div class="imgoverlay text-light">
                                    <a href="javascript:void(0);" data-type="portfolio">
                                        <img width="540" height="320" src="./institucional/img/sda.png" class="attachment-portfolio-crop-thumb wp-post-image" alt="g-dark" />
                                        <div class="overlay"><span class="overlaycolor"></span>
                                            <div class="overlayinfo">
                                                <h6>CAJAS DE DIRECCIÓN</h6>
                                                <a href="http://www.difrani.com.ar/" target="_blank"></a>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>

 
                        <!-- SR33 -->
                        <div class="portfolio-entry masonry-item fluidosyquimicos frenos embragues">
                            <div class="entry-thumb portfolio-thumb">
                                <div class="imgoverlay text-light">
                                    <a href="javascript:void(0);" data-type="portfolio">
                                        <img width="540" height="320" src="./institucional/img/sr33.png" class="attachment-portfolio-crop-thumb wp-post-image" alt="g-dark" />
                                        <div class="overlay"><span class="overlaycolor"></span>
                                            <div class="overlayinfo">
                                                <h6>BOMBAS, Y CILINDROS AUX. DE EMBRAGUE. BOMBAS, DEPOSITOS Y CILINDROS DE FRENO</h6>
                                                <a href="http://www.vdrfrenos.com.ar/" target="_blank">www.vdrfrenos.com.ar</a>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>

 
                        <!-- Trinter -->
                        <div class="portfolio-entry masonry-item suspensionydireccion">
                            <div class="entry-thumb portfolio-thumb">
                                <div class="imgoverlay text-light">
                                    <a href="javascript:void(0);" data-type="portfolio">
                                        <img width="540" height="320" src="./institucional/img/trinter.png" class="attachment-portfolio-crop-thumb wp-post-image" alt="g-dark" />
                                        <div class="overlay"><span class="overlaycolor"></span>
                                            <div class="overlayinfo">
                                                <h6>Cajas, sectores y sinfines de dirección</h6>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>

 
                        <!-- Viemar -->
                        <div class="portfolio-entry masonry-item suspensionydireccion">
                            <div class="entry-thumb portfolio-thumb">
                                <div class="imgoverlay text-light">
                                    <a href="javascript:void(0);" data-type="portfolio">
                                        <img width="540" height="320" src="./institucional/img/viemar.png" class="attachment-portfolio-crop-thumb wp-post-image" alt="g-dark" />
                                        <div class="overlay"><span class="overlaycolor"></span>
                                            <div class="overlayinfo">
                                                <h6>EXTREMOS Y PRECAPS DE DIRECCIóN. RÓTULAS DE SUSPENSIóN</h6>
                                                <a href="http://www.viemar.com.br/" target="_blank">www.viemar.com.br</a>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>

 
                        <!-- 500 Millas -->
                        <div class="portfolio-entry masonry-item transmision">
                            <div class="entry-thumb portfolio-thumb">
                                <div class="imgoverlay text-light">
                                    <a href="javascript:void(0);" data-type="portfolio">
                                        <img width="540" height="320" src="./institucional/img/500millas.png" class="attachment-portfolio-crop-thumb wp-post-image" alt="g-dark" />
                                        <div class="overlay"><span class="overlaycolor"></span>
                                            <div class="overlayinfo">
                                                <h6>CRUCETAS, TRICETAS Y MOVIMIENTOS UNIVERSALES</h6>
                                                <a href="http://www.etma.com.ar/" target="_blank">www.etma.com.ar</a>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>

 
                        <!-- Indaltmec -->
                        <div class="portfolio-entry masonry-item suspensionydireccion">
                            <div class="entry-thumb portfolio-thumb">
                                <div class="imgoverlay text-light">
                                    <a href="javascript:void(0);" data-type="portfolio">
                                        <img width="540" height="320" src="./institucional/img/indaltmec.png" class="attachment-portfolio-crop-thumb wp-post-image" alt="g-dark" />
                                        <div class="overlay"><span class="overlaycolor"></span>
                                            <div class="overlayinfo">
                                                <h6>Cajas de dirección</h6>
                                                <a href="http://www.difrani.com.ar/" target="_blank"></a>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>

 
                        <!-- FV -->
                        <div class="portfolio-entry masonry-item branding ">
                            <div class="entry-thumb portfolio-thumb">
                                <div class="imgoverlay text-light">
                                    <a href="javascript:void(0);" data-type="portfolio">
                                        <img width="540" height="320" src="./institucional/img/fv.png" class="attachment-portfolio-crop-thumb wp-post-image" alt="g-dark" />
                                        <div class="overlay"><span class="overlaycolor"></span>
                                            <div class="overlayinfo">
                                                <h6>Cajas de dirección</h6>
                                                <a href="http://www.fyv.com.ar/home.html" target="_blank">www.fyv.com.ar</a>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>

 
                        <!-- AG -->
                        <div class="portfolio-entry masonry-item suspensionydireccion">
                            <div class="entry-thumb portfolio-thumb">
                                <div class="imgoverlay text-light">
                                    <a href="javascript:void(0);" data-type="portfolio">
                                        <img width="540" height="320" src="./institucional/img/ag.png" class="attachment-portfolio-crop-thumb wp-post-image" alt="g-dark" />
                                        <div class="overlay"><span class="overlaycolor"></span>
                                            <div class="overlayinfo">
                                                <h6>Resortes de suspensión</h6>
                                                <a href="http://www.ag.com.ar/" target="_blank">www.ag.com.ar</a>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>

 
                        <!-- Hescher -->
                        <div class="portfolio-entry masonry-item encendido ">
                            <div class="entry-thumb portfolio-thumb">
                                <div class="imgoverlay text-light">
                                    <a href="javascript:void(0);" data-type="portfolio">
                                        <img width="540" height="320" src="./institucional/img/hescher.png" class="attachment-portfolio-crop-thumb wp-post-image" alt="g-dark" />
                                        <div class="overlay"><span class="overlaycolor"></span>
                                            <div class="overlayinfo">
                                                <h6>BUJIAS DE PRE Y POST CALENTAMIENTO</h6>
                                                <a href="http://www.hescher.com.ar/" target="_blank">www.hescher.com.ar</a>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>

 
                        <!-- Toledo -->
                        <div class="portfolio-entry masonry-item frenos suspensionydireccion">
                            <div class="entry-thumb portfolio-thumb">
                                <div class="imgoverlay text-light">
                                    <a href="javascript:void(0);" data-type="portfolio">
                                        <img width="540" height="320" src="./institucional/img/toledo.png" class="attachment-portfolio-crop-thumb wp-post-image" alt="g-dark" />
                                        <div class="overlay"><span class="overlaycolor"></span>
                                            <div class="overlayinfo">
                                                <h6>MAZAS DE RUEDA Y PUNTAS DE EJE</h6>
                                                <a href="http://www.mettoledo.com.ar/" target="_blank">www.mettoledo.com.ar</a>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>

 
                        <!-- Capemi -->
                        <div class="portfolio-entry masonry-item soportes suspensionydireccion">
                            <div class="entry-thumb portfolio-thumb">
                                <div class="imgoverlay text-light">
                                    <a href="javascript:void(0);" data-type="portfolio">
                                        <img width="540" height="320" src="./institucional/img/capemi.png" class="attachment-portfolio-crop-thumb wp-post-image" alt="g-dark" />
                                        <div class="overlay"><span class="overlaycolor"></span>
                                            <div class="overlayinfo">
                                                <h6>BUJES SILENTBLOCK, SOPORTES Y CAZOLETAS  DE SUSPENSIÓN</h6>
                                                <a href="http://www.capemi.com.ar/" target="_blank">www.capemi.com.ar</a>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>

 
                        <!-- OJS -->
                        <div class="portfolio-entry masonry-item suspensionydireccion">
                            <div class="entry-thumb portfolio-thumb">
                                <div class="imgoverlay text-light">
                                    <a href="javascript:void(0);" data-type="portfolio">
                                        <img width="540" height="320" src="./institucional/img/ojs.png" class="attachment-portfolio-crop-thumb wp-post-image" alt="g-dark" />
                                        <div class="overlay"><span class="overlaycolor"></span>
                                            <div class="overlayinfo">
                                                <h6>Barras estabilizadoras</h6>
                                                <a href="http://www.santiagobarras.com.ar/" target="_blank">www.santiagobarras.com.ar</a>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>

 
                        <!-- RG Frenos -->
                        <div class="portfolio-entry masonry-item embragues frenos">
                            <div class="entry-thumb portfolio-thumb">
                                <div class="imgoverlay text-light">
                                    <a href="javascript:void(0);" data-type="portfolio">
                                        <img width="540" height="320" src="./institucional/img/rg.png" class="attachment-portfolio-crop-thumb wp-post-image" alt="g-dark" />
                                        <div class="overlay"><span class="overlaycolor"></span>
                                            <div class="overlayinfo">
                                                <h6>BOMBAS Y CILINDROS AUXILIARES DE EMBRAGUE, BOMBAS Y CILINDROS DE FRENO</h6>
                                                <a href="http://www.rgfrenos.com.ar/" target="_blank">www.rgfrenos.com.ar</a>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>

 
                        <!-- Nakata -->
                        <div class="portfolio-entry masonry-item suspensionydireccion">
                            <div class="entry-thumb portfolio-thumb">
                                <div class="imgoverlay text-light">
                                    <a href="javascript:void(0);">
                                        <img width="540" height="320" src="./institucional/img/nakata.png" class="attachment-portfolio-crop-thumb wp-post-image" alt="g-dark" />
                                        <div class="overlay"><span class="overlaycolor"></span>
                                            <div class="overlayinfo">
                                                <h6>RESORTES NEUMÁTICOS. BARRAS, BRAZOS, EXTREMOS, PRECAPS, BIELETAS Y RÓTULAS</h6>
                                                <a href="http://www.affinia.com.ar/?s=nakata.php" target="_blank">www.affinia.com.ar</a>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>

 
                        <!-- Tensa -->
                        <div class="portfolio-entry masonry-item fluidosyquimicos">
                            <div class="entry-thumb portfolio-thumb">
                                <div class="imgoverlay text-light">
                                    <a href="javascript:void(0);" data-type="portfolio">
                                        <img width="540" height="320" src="./institucional/img/tensa.png" class="attachment-portfolio-crop-thumb wp-post-image" alt="g-dark" />
                                        <div class="overlay"><span class="overlaycolor"></span>
                                            <div class="overlayinfo">
                                                <h6>LIQUIDOS DE FRENO</h6>
                                                <a href="http://www.affinia.com.ar/?s=tensa.php" target="_blank">www.affinia.com.ar</a>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>


 
                        <!-- Mazfren -->
                        <div class="portfolio-entry masonry-item frenos">
                            <div class="entry-thumb portfolio-thumb">
                                <div class="imgoverlay text-light">
                                    <a href="javascript:void(0);" data-type="portfolio">
                                        <img width="540" height="320" src="./institucional/img/mazfren.png" class="attachment-portfolio-crop-thumb wp-post-image" alt="g-dark" />
                                        <div class="overlay"><span class="overlaycolor"></span>
                                            <div class="overlayinfo">
                                                <h6>Pastillas de freno</h6>
                                                <a href="http://www.mazfren.com/" target="_blank">www.mazfren.com</a>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>


                        <!-- AYD -->
                        <div class="portfolio-entry masonry-item suspensionydireccion">
                            <div class="entry-thumb portfolio-thumb">
                                <div class="imgoverlay text-light">
                                    <a href="javascript:void(0);" data-type="portfolio">
                                        <img width="540" height="320" src="./institucional/img/ayd.png" class="attachment-portfolio-crop-thumb wp-post-image" alt="g-dark" />
                                        <div class="overlay"><span class="overlaycolor"></span>
                                            <div class="overlayinfo">
                                                <h6>EXTREMOS Y PRECAPS DE DIRECCIóN. RÓTULAS DE SUSPENSIóN, BIELETAS DE DIRECCIóN</h6>
                                                <a href="http://www.viemar.com.br/" target="_blank">www.aydtr.com.ar</a>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>

                   </div>

                    <div class="spacer spacer-big"></div>
                </div>
            </section>
            <!-- FABRICANTES -->


            <!-- CONTACTO -->
            <section id="section-contacto">
                <div class="section-inner" style="padding-top:0px;">

                <div class="wrapper">
                    <div class="horizontalsection text-light clearfix section-title" style="padding-top:100px;padding-bottom:100px;">
                        <h2 style="text-align: center; text-shadow: 0px 3px 3px rgba(0, 0, 0, 0.80);">Una amplia trayectoria en el mercado de reposición automotríz nos avala</h2>
                        <div class="horizontalinner parallax-section" style="background:url(./institucional/img/mdp2x.jpg) center center repeat; background-size: cover; heigh: 300px;"></div>
                    </div>
                </div>

                    <div class="wrapper" id="section-contactook">
                        <div class="horizontalsection text-light clearfix section-title" style="padding-top:100px;padding-bottom:100px;">
                            <h2 style="text-align: center; color: black;">Estamos en contacto</h2>
                            <h4 class="subtitle" style="text-align: center; color: black; text-transform: uppercase; ">Ingresá tus datos para realizar una consulta o solicitar información. Te responderemos a la brevedad.</h4>
                        </div>
                    </div>

                    <div class="wrapper">
                        <div class="column-section ">

                            <!-- GOOGLE MAP -->
                            <div class="column one-third first-col" id="map" style="height: 455px;"></div>
                            <script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=false"></script>
                            <script type="text/javascript">
                            function mapinitialize() {
                                var latlng = new google.maps.LatLng(-37.9876046, -57.5771548);
                                var myOptions = {
                                    zoom: 14,
                                    center: latlng,
                                    scrollwheel: false,
                                    scaleControl: false,
                                    disableDefaultUI: false,
                                    mapTypeId: google.maps.MapTypeId.ROADMAP
                                };
                                var map = new google.maps.Map(document.getElementById("map"), myOptions);

                                var image = "files/uploads/map-pin.png";
                                var marker = new google.maps.Marker({
                                    map: map,
                                    position: map.getCenter()
                                });

                                var contentString = '<b>Office</b></br>Streetname 13</br>50000 Sydney';
                                var infowindow = new google.maps.InfoWindow({
                                    content: contentString
                                });

                                google.maps.event.addListener(marker, 'click', function() {
                                    infowindow.open(map, marker);
                                });


                            }
                            mapinitialize();
                            </script>
                            <!-- GOOGLE MAP -->

                            <div class="column one-third ">
                                <form id="contact-form" class="checkform" action="" target="./mail.php" method="post">
                                    <div class="form-row clearfix">
                                        <label for="name" class="req">Nombre *</label>
                                        <div class="form-value">
                                            <input type="text" name="name" class="name" id="name" value="" />
                                        </div>
                                    </div>
                                    <div class="form-row clearfix">
                                        <label for="email" class="req">Email *</label>
                                        <div class="form-value">
                                            <input type="text" name="email" class="email" id="email" value="" />
                                        </div>
                                    </div>
                                    <div class="form-row clearfix textbox">
                                        <label for="message" class="req">Mensaje *</label>
                                        <div class="form-value">
                                            <textarea name="message" class="message" id="message" rows="15" cols="50"></textarea>
                                        </div>
                                    </div>
                                    <div id="form-note">
                                        <div class="alert alert-error">
                                            <h6><strong>Error</strong>: Por favor revise sus datos!</h6>
                                        </div>
                                    </div>
                                    <div class="form-row form-submit">
                                        <input type="submit" name="submit_form" class="submit" value="Enviar" />
                                    </div>
                                </form>
                            </div>



                            
                            <div class="column one-third last-col ">
                                <h5 style="margin-top: 20px;"><strong>Personalmente</strong></h5>
                                <p style="margin-top: 0px;">
                                    Chile 2019, Mar del Plata
                                    </br> Buenos Aires, Argentina.
                                </p>
                                <h5><strong>Telefónicamente</strong></h5>
                                <p style="margin-top: 0px;">
                                    TEL: (0223) 474-1222 (ROTATIVAS)
                                    </br> FAX: (0223) 475-7170 / 0800-333-6590
                                </p>
                                <h5><strong>Email</strong></h5>
                                <p style="margin-top: 0px;">
                                    <a href="mailto:info@distribuidoralibertad.com">info@distribuidoralibertad.com</a></br>
                                    <a href="mailto:pedidos@distribuidoralibertad.com">pedidos@distribuidoralibertad.com</a></br>
                                </p>
                                <h5 style="margin-top: 20px;"><strong>Horarios</strong></h5>
                                <p style="margin-top: 0px;">
                                    LUN a VIE de 8 a 19:30 
                                    </br>SAB de 8 a 12:30
                                </p>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                    

                </div>
            </section>
            <!-- CONTACTO -->

        </div>
        <!-- PAGEBODY -->

        <!-- FOOTER -->
        <footer>
            <div class="footerinner wrapper align-center text-light">
                <a id="backtotop" href="#" class="sr-button sr-button4 mini-button sr-buttonicon"><i class="fa fa-chevron-up"></i></a>
            </div>
        </footer>
        <!-- FOOTER -->
    </div>
    
    <!-- PAGE CONTENT -->
    <script type='text/javascript' src='./institucional/js/jquery.easing.1.3.js?ver=1.0'></script>
    <script type='text/javascript' src='./institucional/js/jquery.easing.compatibility.js?ver=1.0'></script>
    <script type='text/javascript' src='./institucional/js/jquery.visible.min.js?ver=1.0'></script>
    <script type='text/javascript' src='./institucional/js/jquery.flexslider.min.js?ver=2.1'></script>
    <script type='text/javascript' src='./institucional/js/jquery.parallax.min.js?ver=1.0'></script>
    <script type='text/javascript' src='./institucional/js/jquery.counter.min.js?ver=1.0'></script>
    <script type='text/javascript' src='./institucional/js/jquery.mousewheel.js?ver=1.0'></script>
    <script type='text/javascript' src='./institucional/js/jquery.perfect.scrollbar.js?ver=1.0'></script>
    <script type='text/javascript' src='./institucional/js/xone-header.js?ver=1.0'></script>
    <script type='text/javascript' src='./institucional/js/jquery.scroll.min.js?ver=1.0'></script>
    <script type='text/javascript' src='./institucional/js/jquery.isotope.min.js?ver=1.5.25'></script>
    <script type='text/javascript' src='./institucional/js/xone-form.js?ver=1.0'></script>
    <script type='text/javascript' src='./institucional/js/script.js?ver=1.0'></script>
    <script type='text/javascript' src='./institucional/js/jquery.fancybox.pack.js'></script>
    
</body>

</html>
