<script type="text/javascript">

var importadorPrecios_opened = 0;

//ABRE ALERTA EXPLICANDO EL PROCESO, AL ACEPTAR
//SELECCIONAMOSEL ARCHIVO CSV
function importadorPrecios_open(){

  //Creamos el popup
  abrir_modal('importadorPrecios', 450, false);

  //Mostramos la ventana
  $('#importadorPreciosModal').modal({show:true});

  //Titulo...
  $('#importadorPreciosModal_titulo').html('Importar Precios');
  $('#importadorPreciosModal_footer').html('');

  //Cargando...
  $('#importadorPreciosModal_container').html(cargando());

  //API
  apix('precios','check_lista',{}, {
    file: true,
    ok: function(json){

      //Si existe importacion pendiente
      if(json.result.pendiente > 0){

        //Contenido
        $('#importadorPreciosModal_titulo').html('Importacion pendiente');
        $('#importadorPreciosModal_container').html('');

        //Boton Altas
        $('#importadorPreciosModal_footer').append('<button type="button" class="btn btn-primary" ' + (json.result.altas ? ''  : 'disabled="true"') + ' style="display: block; width: 100%; margin: 0px 0px 10px 0px; float: left;" onclick="importadorPrecios_listar_altas();">Paso 1: Altas ['+json.result.altas+']</button>');

        //Boton Bajas
        $('#importadorPreciosModal_footer').append('<button type="button" class="btn btn-primary" ' + (json.result.bajas ? ''  : 'disabled="true"') + ' style="display: block; width: 100%; margin: 0px 0px 10px 0px; float: right;" onclick="importadorPrecios_listar_bajas();">Paso 2: Bajas ['+json.result.bajas+']</button>');

        //Boton Reincorporaciones
        $('#importadorPreciosModal_footer').append('<button type="button" class="btn btn-primary" ' + (json.result.reincorporaciones ? ''  : 'disabled="true"') + ' style="display: block; width: 100%; margin: 0px 0px 10px 0px; float: right;" onclick="importadorPrecios_listar_reincorporaciones();">Paso 3: Reincorporaciones ['+json.result.reincorporaciones+']</button>');


        //Boton Actualizaciones
        $('#importadorPreciosModal_footer').append('<button type="button" class="btn btn-primary" ' + (json.result.actualizaciones ? ''  : 'disabled="true"') + ' style="display: block; width: 100%; margin: 0px 0px 10px 0px; float: left;" onclick="confirma(\'Actualizar Precios\',\'Los precios se modificaran de forma permanente.\',\'exclamacion\',\'success\',\'importadorPrecios_confirmar_actualizaciones()\');">Paso 4: Actualizaciones ['+json.result.actualizaciones+']</button>');



        //Boton Agregar
        $('#importadorPreciosModal_footer').append('<button type="button" class="btn btn-danger" style="display: block; width: 100%; margin: 10px auto;" onclick="importadorPrecios_descartar_lista();">Descartar lista completa</button>');

        //Append Cancel Button
        $('#importadorPreciosModal_footer').append('<button type="button" data-dismiss="modal" class="btn btn-warning" style="display: block; width: 100%; margin: 10px auto;">Cerrar</button>');
      
      //Subir archivo
      }else{
        //Cargando...
        $('#importadorPreciosModal_container').html('Para importar precios en Batch seleccione un archivo XLS. </br></br>Deberá tener el siguiente formato: </br></br><ul><li>Columna A: <b>Codigo</b></li><li>Columna B: <b>Precio</b></li></ul><input type="file" id="importadorPrecios_input" name="archivo" style="display: none;">');

        //Boton Confirmar cambios
        $('#importadorPreciosModal_footer').append('<button type="button" class="btn btn-primary" style="display: block; width: 100%; margin: 10px auto;" onclick="$(\'#importadorPrecios_input\').click();">Seleccionar Archivo</button>');

        //Append Cancel Button
        $('#importadorPreciosModal_footer').append('<button type="button" data-dismiss="modal" class="btn btn-warning" style="display: block; width: 100%; margin: 10px auto;">Cancelar Importación</button>');

        //Al seleccionar...
        $('#importadorPrecios_input').on('change', function(){

          if(!importadorPrecios_opened && ($('#importadorPrecios_input').val() != '')){
            
            //Abrimos el popup
            importadorPrecios_opened = 1;

            //Recolectamos las variables
            var file = $('#importadorPrecios_input').prop('files')[0];

            //Obtenemos la extension del archivo
            ext = '';
            //Si no es csv: ERROR
            if(ext != 'xls' && 0){
              alert('Solo se aceptan archivos XLS.');
              $('#importadorPrecios_input').val(''); 
              importadorPrecios_opened = 0;
            }else{

              //Reseteamos el campo
              $('#importadorPrecios_input').val(''); 
              importadorPrecios_opened = 0;

              //Datos del form
              var data = new FormData();
              data.append('archivo', file);

              //Indicamos al usuario que el sistema ahora va a estar ocupado haciendo el backup
              $('#importadorPreciosModal_titulo').html('Subiendo Archivo...');
              $('#importadorPreciosModal_container').html(cargando());
              $('#importadorPreciosModal_footer').html('');

              //API
              apix('precios','subir_lista',data, {
                file: true,
                ok: function(json){
                    //cargamos las opciones
                    importadorPrecios_open();
                }
              });
            }
          }
        });
      }
    }
  });
}

//Descartar la lista de precios anterior
function importadorPrecios_descartar_lista(){
  //API
  apix('precios','descartar_lista',{}, {
  file: false,
    ok: function(json){
      importadorPrecios_open();
    }
  });
}

//Confirmar los cambios de precios
function importadorPrecios_confirmar_actualizaciones(){
  //API
  apix('precios','confirmar_actualizaciones',{}, {
  file: false,
    ok: function(json){
      importadorPrecios_open();
    }
  });
}

//Baja de Articulos
function importadorPrecios_listar_bajas(){
  //API
  apix('precios','listar_bajas',{}, {
  file: false,
    ok: function(json){
        //Vaciamos popup
        $('#importadorPreciosModal_titulo').html('Articulos por Deshabilitar');
        $('#importadorPreciosModal_container').html('');
        $('#importadorPreciosModal_footer').html('');

        //Agregamos contenido
        $('#importadorPreciosModal_container').html('');

        //Contenido
        var contenido = $('<div class="scrollable" style="max-height: 400px; overflow-y: hidden;"></div>');

        //Descripcion
        contenido.append('<strong><p>La siguiente lista de Artículos se deshabilitará. Podra borrarlos definitivamente desde la heramienta de depuracion de Artículos.</p></strong>');

        //Recorremos los resultados
        for(var i in json.result) contenido.append(json.result[i] + '<br>');

        //Agregamos el contenido
        $('#importadorPreciosModal_container').append(contenido);

        //Agregamos los scrollbars
        $('#importadorPreciosModal_container .scrollable').mCustomScrollbar({theme: 'minimal-dark',scrollInertia: 100, mouseWheel:{ scrollAmount: 70 }});

        //Boton Confirmar
        $('#importadorPreciosModal_footer').append('<button type="button" class="btn btn-success" style="display: block; width: 100%; margin: 10px auto;" onclick="importadorPrecios_confirmar_bajas();">Confirmar baja de Artículos</button>');

        //Boton Volver
        $('#importadorPreciosModal_footer').append('<button type="button" class="btn btn-warning" style="display: block; width: 100%; margin: 10px auto;" onclick="importadorPrecios_open();">Volver</button>');

    }
  });
}

//Baja de Articulos
function importadorPrecios_listar_altas(){
  //API
  apix('precios','listar_altas',{}, {
  file: false,
    ok: function(json){
        //Vaciamos popup
        $('#importadorPreciosModal_titulo').html('Articulos nuevos por Agregar');
        $('#importadorPreciosModal_container').html('');
        $('#importadorPreciosModal_footer').html('');

        //Agregamos contenido
        $('#importadorPreciosModal_container').html('');

        //Contenido
        var contenido = $('<div class="scrollable" style="max-height: 400px; overflow-y: hidden;"></div>');

        //Descripcion
        contenido.append('<strong><p>La siguiente lista de Artículos se creará. Podra editarlos posteriormente desde la lista de últimas incorporaciones.</p></strong>');

        //Recorremos los resultados
        for(var i in json.result) contenido.append(json.result[i] + '<br>');

        //Agregamos el contenido
        $('#importadorPreciosModal_container').append(contenido);

        //Agregamos los scrollbars
        $('#importadorPreciosModal_container .scrollable').mCustomScrollbar({theme: 'minimal-dark',scrollInertia: 100, mouseWheel:{ scrollAmount: 70 }});

        //Boton Confirmar
        $('#importadorPreciosModal_footer').append('<button type="button" class="btn btn-success" style="display: block; width: 100%; margin: 10px auto;" onclick="importadorPrecios_confirmar_altas();">Confirmar alta de Artículos</button>');

        //Boton Volver
        $('#importadorPreciosModal_footer').append('<button type="button" class="btn btn-warning" style="display: block; width: 100%; margin: 10px auto;" onclick="importadorPrecios_open();">Volver</button>');

    }
  });
}

//Baja de Articulos
function importadorPrecios_listar_reincorporaciones(){
  //API
  apix('precios','listar_reincorporaciones',{}, {
  file: false,
    ok: function(json){
        //Vaciamos popup
        $('#importadorPreciosModal_titulo').html('Articulos por reincorporar');
        $('#importadorPreciosModal_container').html('');
        $('#importadorPreciosModal_footer').html('');

        //Agregamos contenido
        $('#importadorPreciosModal_container').html('');

        //Contenido
        var contenido = $('<div class="scrollable" style="max-height: 400px; overflow-y: hidden;"></div>');

        //Descripcion
        contenido.append('<strong><p>La siguiente lista de Artículos existia anteriormente y se reincorporarán. Podra editarlos posteriormente buscando cada uno de ellos.</p></strong>');

        //Recorremos los resultados
        for(var i in json.result) contenido.append(json.result[i] + '<br>');

        //Agregamos el contenido
        $('#importadorPreciosModal_container').append(contenido);

        //Agregamos los scrollbars
        $('#importadorPreciosModal_container .scrollable').mCustomScrollbar({theme: 'minimal-dark',scrollInertia: 100, mouseWheel:{ scrollAmount: 70 }});

        //Boton Confirmar
        $('#importadorPreciosModal_footer').append('<button type="button" class="btn btn-success" style="display: block; width: 100%; margin: 10px auto;" onclick="importadorPrecios_confirmar_reincorporaciones();">Confirmar reincorporaciones</button>');

        //Boton Volver
        $('#importadorPreciosModal_footer').append('<button type="button" class="btn btn-warning" style="display: block; width: 100%; margin: 10px auto;" onclick="importadorPrecios_open();">Volver</button>');

    }
  });
}

//Confirmar los cambios de precios
function importadorPrecios_confirmar_reincorporaciones(){
  //API
  apix('precios','confirmar_reincorporaciones',{}, {
  file: false,
    ok: function(json){
      importadorPrecios_open();
    }
  });
}

//Confirmar los cambios de precios
function importadorPrecios_confirmar_bajas(){
  //API
  apix('precios','confirmar_bajas',{}, {
  file: false,
    ok: function(json){
      importadorPrecios_open();
    }
  });
}

//Confirmar los cambios de precios
function importadorPrecios_confirmar_altas(){
  //API
  apix('precios','confirmar_altas',{}, {
  file: false,
    ok: function(json){
      importadorPrecios_open();
    }
  });
}

//Imprimimos el reporte...
function importadorPrecios_imprimir(){
  //Create dynamic popup...
  myWindow=window.open('','','scrollbars=yes,resizable=yes,width=0,height=0');
  myWindow.innerWidth = 0;
  myWindow.innerHeight = 0;
  myWindow.screenX = 0;
  myWindow.screenY = 0;
  myWindow.document.write( $('#importadorPreciosModal_container').clone().find('div').attr('style','').html() );
  myWindow.print();
  myWindow.close();
}


function importadorPrecios_generar_pdf(){
    //Llamada a la API

    //Agregamos el icono de loading
    $('#listas_generar_pdf_icono').html('<i class="fa fa-cog fa-spin" style="font-size: 17px; margin: 0px 3px 0px 0px;"></i> Generar Archivos PDF');

    
    apix('listas','generar_pdf',{}, {
      ok: function(json){
        $('#listas_generar_pdf_icono').html('Generar Archivos PDF');
      }
    });
}


</script>

