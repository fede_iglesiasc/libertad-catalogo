<script type="text/javascript">

{if="$GLOBALS['permisos']['printnode_apikey']"}

function printnode_apikey(){

	//Creamos el popup
	abrir_modal('printnodeApikey', 400, false);

	//Mostramos la ventana
	$('#printnodeApikeyModal').modal({show:true});

	//Titulo...
	$('#printnodeApikeyModal_titulo').html('API Key de Printnode');

	//Cargando...
	$('#printnodeApikeyModal_container').html( cargando() );

	//Footer...
	$('#printnodeApikeyModal_footer').html('');

	//Traemos los datos de la API
	apix('printnode','apikey_info',{}, {
		ok: function(json){

			//Generamos el listado
			$('#printnodeApikeyModal_container').html('');

			var contenido = $('<div class="scrollable" style="max-height: 300px; overflow-y: hidden;"></div>'); 
			contenido.append(genera_campo({ 
		      id:       "apikey",
		      label:      "API Key",
		      tipo:       "input",
		      mostrarEditar:  true,
		      mostrarCrear:   true
		    }, 'printnodeApikey' ,true));


			//Valor
			$('#printnodeApikey_apikey',contenido).val(json.result.apikey);

			//Creamos contenido
			$('#printnodeApikeyModal_container').html(contenido);

			//Agregamos scrollbars
			$('#printnodeApikeyModal_container .scrollable').mCustomScrollbar({theme: 'minimal-dark',scrollInertia: 100, mouseWheel:{ scrollAmount: 70 }});

			$('#printnodeApikeyModal_footer').append('<button type="button" class="btn btn-primary" style="display: block; width: 100%; margin: 10px auto;" onclick="printnode_apikey_edita_agrega();">Guardar</button>');

			//Append Cancel Button
			$('#printnodeApikeyModal_footer').append('<button type="button" data-dismiss="modal" class="btn btn-warning" style="display: block; width: 100%; margin: 10px auto;">Cerrar</button>');
		}
	});
}

function printnode_apikey_edita_agrega(){
	var data = {apikey: $('#printnodeApikey_apikey').val()};
	apix('printnode','apikey_editar',data, {
		ok: function(json){
			notifica(	'API Key Editada',
						'La API Key se editó con éxito',
						'confirmacion','success');

			$('#printnodeApikeyModal').modal('hide');
		}
	});
}

{/if}

{if="$GLOBALS['permisos']['printnode_impresoras']"}

function printnode_impresoras(){

	//Creamos el popup
	abrir_modal('printnode_impresoras', 600, false);

	//Mostramos la ventana
	$('#printnode_impresorasModal').modal({show:true});

	//Titulo...
	$('#printnode_impresorasModal_titulo').html('Impresoras');

	//Cargando...
	$('#printnode_impresorasModal_container').html( cargando() );

	//Footer...
	$('#printnode_impresorasModal_footer').html('');

	//Traemos los datos de la API
	apix('printnode','impresoras_dropdown',{}, {
		ok: function(json){

			//Generamos el listado
			$('#printnode_impresorasModal_container').html('');

			var contenido = $('<div class="scrollable" style="max-height: 400px; overflow-y: hidden;"></div>'); 
			
			//Agregamos N impresoras
			for(var i = 0; i <= 4; i++)
				contenido.append(genera_campo({	
					id: 			''+i,
					label: 			"Impresora "+i,
					tipo: 			"select2",
					mostrarEditar: 	true,
					mostrarCrear: 	true,
					config: 		{
						minimumInputLength: 0,
						placeholder: "Impresora "+i,
						allowClear: true,
						width: 'resolve',
						id: function(e) { return e.id; },
						formatNoMatches: function() { return 'Sin resultados'; },
						formatSearching: function(){ return "Buscando..."; },
						data: json.result.impresoras,
						formatInputTooShort: function () {
							return "";
						},
						formatResult: function(item) {
							return item.name;
						},
						formatSelection: function(item) { 
							var html = '';

							//ONline
							if(item.pc_state == 'connected') html += '<i class="badge" style="padding: 5px 10px;float: none;border-radius: 3px; color: #444444;font-size: 12px; font-weight: 600; background-color: #5cb85c;">ONLINE</i>';

							//OFFLINE
							else html += '<i class="badge" style="padding: 5px 10px;float: none;border-radius: 3px; color: #444444;font-size: 12px; font-weight: 600; background-color: #e04343;">OFFLINE</i>';


							html += '<b style="margin-left: 10px;">'+item.pc_name+' > </b> '+item.name;
							
							return html;
						}
					}
				}, 'printnode_impresoras' ,true));


			for(var i = 0; i <= 4; i++){
				$('#printnode_impresoras_'+i, contenido).val_(json.result.seleccionadas[i]);
			}


			//Creamos contenido
			$('#printnode_impresorasModal_container').html(contenido);

			//Agregamos scrollbars
			$('#printnode_impresorasModal_container .scrollable').mCustomScrollbar({theme: 'minimal-dark',scrollInertia: 100, mouseWheel:{ scrollAmount: 70 }});

			$('#printnode_impresorasModal_footer').append('<button type="button" class="btn btn-primary" style="display: block; width: 100%; margin: 10px auto;" onclick="printnode_impresoras_guardar();">Guardar</button>');

			//Append Cancel Button
			$('#printnode_impresorasModal_footer').append('<button type="button" data-dismiss="modal" class="btn btn-warning" style="display: block; width: 100%; margin: 10px auto;">Cerrar</button>');
		}
	});
}

function printnode_impresoras_guardar(){
	console.log('enmtro');
	var data = {};

	for (var i = 0; i <= 4; i++){
		data['impresora_' + (i+1)] = null;
		var tmp = $('#printnode_impresoras_'+i).select2('data');
		if(tmp != null) data['impresora_' + (i+1)] = tmp.id;
	}

	apix('printnode','impresoras_editar',data, {
		ok: function(json){
			notifica(	'Impresoras editadas',
						'Impresoras editadas con éxito',
						'confirmacion','success');

			$('#printnode_impresorasModal').modal('hide');
		}
	});
}

{/if}

</script>