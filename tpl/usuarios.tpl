<script type="text/javascript">

{if="$GLOBALS['permisos']['usuarios_listar'] || $GLOBALS['permisos']['usuarios_editar'] || $GLOBALS['permisos']['usuarios_crear'] || $GLOBALS['permisos']['usuarios_eliminar'] || $GLOBALS['permisos']['usuarios_permisos']"}
//Config Usuarios
var usuarios_config = {
	modName: 					'usuarios',
	modLabelSingular: 			'Usuario',
	modLabelPlural: 			'Usuarios',
	creaLabel: 					'Agregado',
	editaLabel: 				'Editado',
	eliminaLabel: 				'Eliminado',
	nuevoLabel: 				'Nuevo',
	artLabel: 					'El',
	max_height_edita_agrega: 	'300',
	max_height_lista: 			'300',
	campos:[
		{	
			id: 			"usuario",
			label: 			"Usuario",
			tipo: 			"input",
			mostrarEditar: 	false,
			mostrarCrear: 	true
		},
		{	
			id: 			"nombre",
			label: 			"Nombre",
			tipo: 			"input",
			mostrarEditar: 	true,
			mostrarCrear: 	true
		},
		{	
			id: 			"rol",
			label: 			"Rol",
			tipo: 			"select2",
			mostrarEditar: 	true,
			mostrarCrear: 	true,
			config: 		{
								minimumInputLength: 0,
								placeholder: $(this).attr('placeholder'),
								allowClear: false,
								width: 'resolve',
								id: function(e) { return e.id; },
								formatNoMatches: function() { return 'Sin resultados'; },
								formatSearching: function(){ return "Buscando..."; },
								ajax: {
								    url: "api.php",
								    dataType: 'json',
								    quietMillis: 200,
								    data: function(term, page) {
								        return {
								            module: 'roles',
								            run: 'roles_lista_select',
								            q: term,
								            p: page
								        };
								    },
								    results: function(data, page ) {
								        //Si el ususario no esta logueado refresh
								        if(!data.logued) location.reload();

								        //Calcula si existen mas resultados a mostrar
								        var more = (page * 40) < data.result.total;

								        //Devolvemos el valor de more para que selec2
								        //sepa que debemos cargar mas resultados.
								        return {results: data.result.items, more: more};
								    }
								},
								formatInputTooShort: function () {
									return "";
								},
								formatResult: function(item) {
									return item.nombre;
								},
								formatSelection: function(item) { 
									return item.nombre;
								},
								initSelection : function (element, callback) {
									var elementText = $(element).attr('data-init-text');
								}
							}
		},
		{	
			id: 			"horario",
			label: 			"Horario",
			tipo: 			"select2",
			mostrarEditar: 	true,
			mostrarCrear: 	true,
			config: 		{
								minimumInputLength: 0,
								placeholder: $(this).attr('placeholder'),
								allowClear: false,
								width: 'resolve',
								id: function(e) { return e.id; },
								formatNoMatches: function() { return 'Sin resultados'; },
								formatSearching: function(){ return "Buscando..."; },
								ajax: {
								    url: "api.php",
								    dataType: 'json',
								    quietMillis: 200,
								    data: function(term, page) {
								        return {
								            module: 'horarios',
								            run: 'horarios_lista_select',
								            q: term,
								            p: page
								        };
								    },
								    results: function(data, page ) {
								        //Si el ususario no esta logueado refresh
								        if(!data.logued) location.reload();

								        //Calcula si existen mas resultados a mostrar
								        var more = (page * 40) < data.result.total;

								        //Devolvemos el valor de more para que selec2
								        //sepa que debemos cargar mas resultados.
								        return {results: data.result.items, more: more};
								    }
								},
								formatInputTooShort: function () {
									return "";
								},
								formatResult: function(item) {
									return item.nombre;
								},
								formatSelection: function(item) { 
									return item.nombre;
								},
								initSelection : function (element, callback) {
									var elementText = $(element).attr('data-init-text');
								}
							}
		},
		{	
			id: 			"email",
			label: 			"Email",
			tipo: 			"input",
			mostrarEditar: 	true,
			mostrarCrear: 	true
		},
		{	
			id: 			"password",
			label: 			"Contraseña",
			tipo: 			"input",
			mostrarEditar: 	true,
			mostrarCrear: 	true
		},
		{	
			id: 			"activo",
			label: 			"Activo",
			tipo: 			"checkbox",
			mostrarEditar: 	true,
			mostrarCrear: 	true,
			config: 		{
							claseActivo: 'btn-success',
							claseInactivo: 'btn-danger',
							valueActivo: '1',
							valueInactivo: '0',
							valueDefault: '1',
							labelActivo: function(el){
								el.html('El Usuario está activo, haga click para desactivarlo');
							},
							labelInactivo: function(el){
								el.html('El Usuario está inactivo, haga click para activarlo');
							}
			}
		}
	]
};
{/if}

{if="$GLOBALS['permisos']['usuarios_listar']"}
//Lista
function usuarios_listar(){

	//Creamos el popup
	abrir_modal(usuarios_config.modName, 550, false);

	//Mostramos la ventana
	$('#'+usuarios_config.modName+'Modal').modal({show:true});

	//Titulo...
	$('#'+usuarios_config.modName+'Modal_titulo').html(usuarios_config.modLabelPlural);

	//Cargando...
	$('#'+usuarios_config.modName+'Modal_container').html( cargando() );

	//Footer...
	$('#'+usuarios_config.modName+'Modal_footer').html('');

	//Traemos los datos de la API
	apix(usuarios_config.modName,'listar',{}, {
		ok: function(json){

			var tabla = generaTabla({
							id_tabla: usuarios_config.modName,
							items: json.result,
							keys_busqueda: ['usuario','nombre','email'],
							key_nombre: null,
							max_height: usuarios_config.max_height_lista,
							funcion_activado: function(el){

								//Seleccionamos el activo anterior
								var elActivo = $('#usuarios_lista').find('.active_');

								//Boton Borrar
								var boton_eliminar = '<button class="btn btn-xs btn-warning" style="margin: 0px 0px 8px 5px; width: 24px; height: 24px;" data-toggle="tooltip" title="Borrar" onclick=" confirma(\'Eliminar '+usuarios_config.modLabelSingular+'\',\'El '+usuarios_config.modLabelSingular+' se eliminará de forma permanente.\',\'exclamacion\',\'danger\',\''+usuarios_config.modLabelPlural.toLowerCase()+'_elimina(\\\''+$(el).data('usuario')+'\\\')\');"><i class="fa fa-remove"></i></button>';
								
								//Boton editar
								var boton_editar = '<button class="btn btn-xs btn-primary" style="margin: 0px 0px 8px 5px; width: 24px; height: 24px;" data-toggle="tooltip" title="Editar" onclick="'+usuarios_config.modLabelPlural.toLowerCase()+'_edita_agrega(\''+$(el).data('usuario')+'\');"><i class="fa fa-pencil"></i></button>';

								var boton_permisos = '<button class="btn btn-xs btn-primary" style="margin: 0px 0px 8px 5px; width: 24px; height: 24px;" data-toggle="tooltip" title="Permisos" onclick="'+usuarios_config.modLabelPlural.toLowerCase()+'_permisos_listar(\''+$(el).data('usuario')+'\',\''+$(el).data('rol')+'\');"><i class="fa fa-unlock-alt"></i></button>';


								var boton_vacaciones = '<button class="btn btn-xs btn-primary" style="margin: 0px 0px 8px 5px; width: 24px; height: 24px;" data-toggle="tooltip" title="Vacaciones" onclick="usuarios_vacaciones_listar(\''+$(el).data('usuario')+'\');"><i class="fa fa fa-calendar"></i></button>';

								//Activo o no?
								var activo = parseInt($(el).data('activo'));
								var boton_activo = '<button class="btn btn-xs btn-' + ( activo==1 ? 'success' : 'danger' ) + '" style="height: 24px; padding: 0 10px;"><b>' + ( activo==1 ? 'Activo' : 'Inactivo' ) + '</b></button>';
 
								var html = '<b>' + $(el).data('nombre') + '</b><div class="descripcion"><h5>'+$(el).data('rol')+'</h5><small>Usuario: '+$(el).data('usuario')+'</br>Horario: '+$(el).data('horario_label')+'</br><a href="mailto:'+$(el).data('email')+'">'+$(el).data('email')+'</a></small></div><div class="pull-right descripcion" style="width: 120px; text-align: right; position: absolute; top: 8px; right: 10px;">';

								{if="$GLOBALS['permisos']['usuarios_vacaciones']"}
									//Boton Permisos
									html += boton_vacaciones;
								{/if}

								{if="$GLOBALS['permisos']['usuarios_permisos']"}
									//Boton Permisos
									html += boton_permisos;
								{/if}

								{if="$GLOBALS['permisos']['usuarios_editar']"}
									//Boton Permisos
									html += boton_editar;
								{/if}
								
								{if="$GLOBALS['permisos']['usuarios_eliminar']"}
									//Boton Permisos
									html += boton_eliminar;
								{/if}

								html += '<div>'+boton_activo+'</div></div>';

								//Agregamos los datos
								$(el).html(html);

							},
							funcion_desactivado: function(el){
								var html = '<b>'+ $(el).data('nombre') + '</b>';
								if( $(el).data('estado') == '1')
									html += '<span class="label label-success" style="padding: 6px 7px; font-size: 10px; display: inline-block; float: right;">ONLINE</span>';

								$(el).html(html);
							}
						});

			//Agregamos los tooltips
			tabla.tooltip({selector: '[data-toggle="tooltip"]', placement: 'left', animation: false});

			//Generamos el listado
			$('#'+usuarios_config.modName+'Modal_container').html('');
			$('#'+usuarios_config.modName+'Modal_container').append(tabla);


			//Agregamos scrollbars
			$('#'+usuarios_config.modName+'Modal_container .scrollable').mCustomScrollbar({theme: 'minimal-dark',scrollInertia: 100, mouseWheel:{ scrollAmount: 70 }});

			{if="$GLOBALS['permisos']['usuarios_crear']"}
			//Boton Agregar
			$('#'+usuarios_config.modName+'Modal_footer').append('<button type="button" class="btn btn-primary" style="display: block; width: 100%; margin: 10px auto;" onclick="'+usuarios_config.modName+'_edita_agrega();">'+usuarios_config.nuevoLabel+' '+usuarios_config.modLabelSingular+'</button>');
			{/if}

			//Append Cancel Button
			$('#'+usuarios_config.modName+'Modal_footer').append('<button type="button" data-dismiss="modal" class="btn btn-warning" style="display: block; width: 100%; margin: 10px auto;">Cerrar</button>');
		}
	});
}
{/if}

{if="$GLOBALS['permisos']['usuarios_editar'] || $GLOBALS['permisos']['usuarios_crear']"}
//Edita/Agrega
function usuarios_edita_agrega(id){

	//Si estamos agregando...
	if(typeof id === 'undefined') var agrega = true;
	else var agrega = false;

	//Titulo...
	if(agrega) $('#'+usuarios_config.modName+'Modal_titulo').html(usuarios_config.nuevoLabel+' '+usuarios_config.modLabelSingular);
	else $('#'+usuarios_config.modName+'Modal_titulo').html('Editar '+usuarios_config.modLabelSingular);

	//Cargando...
	$('#'+usuarios_config.modName+'Modal_container').html(cargando());

	//Footer...
	$('#'+usuarios_config.modName+'Modal_footer').html('');

	//Contenido
	var contenido = $('<div class="scrollable" style="max-height: '+usuarios_config.max_height_edita_agrega+'px; overflow-y: hidden;"></div>');

	//Agregamos todos los campos
	for(var k in usuarios_config.campos){
		var campo = usuarios_config.campos[k];
		if( (campo.mostrarEditar && !agrega) || (campo.mostrarCrear && agrega)){
			//Agregamos el campo
			contenido.append( genera_campo(usuarios_config.campos[k], usuarios_config.modName ,true) );
		}
	}


	//Agregamos el contenido y los botones
	//si estamos creando un registro nuevo
	if(agrega){
		//Agregamos el contenido
		$('#'+usuarios_config.modName+'Modal_container').html(contenido);

		//Agregamos scrollbars
		$('#'+usuarios_config.modName+'Modal_container .scrollable').mCustomScrollbar({theme: 'minimal-dark',scrollInertia: 100, mouseWheel:{ scrollAmount: 70 }});

		//Agregamos los botones al footer
		$('#'+usuarios_config.modName+'Modal_footer').append('<button type="button" class="btn btn-primary" style="display: block; width: 100%; margin: 10px auto;" onclick="'+usuarios_config.modLabelPlural.toLowerCase()+'_agrega_confirm();">Agregar '+usuarios_config.modLabelSingular+'</button>');
		$('#'+usuarios_config.modName+'Modal_footer').append('<button type="button" class="btn btn-warning" style="display: block; width: 100%; margin: 10px auto;" onclick="'+usuarios_config.modLabelPlural.toLowerCase()+'_listar();">Volver</button>');
	}


	//Si editamos
	if(!agrega){
		var data = {};
		data['usuario'] = id;

		apix(usuarios_config.modName,'info',data, {
            ok: function(json){

				//Titulo
				$('#'+usuarios_config.modName+'Modal_titulo').html('Editar '+usuarios_config.modLabelSingular);

		    	//Llenamos los datos
		    	for(var k in usuarios_config.campos){
		    		var campo = usuarios_config.campos[k];
		    		if((campo.tipo == 'input') || (campo.tipo == 'checkbox'))
			    		if( !(typeof(json.result[ campo.id ]) == "undefined") && !(json.result[ campo.id ] === null) ){
			    			$('#'+usuarios_config.modName+'_'+campo.id,contenido).val_(json.result[ campo.id ]);
			    		}
		    	}

		    	//Rol
		    	$('#usuarios_rol',contenido).val_({ id: json.result.rol_id, nombre: json.result.rol_label });

		    	//Horario
		    	$('#usuarios_horario',contenido).val_({ id: json.result.horario, nombre: json.result.horario_label });

		    	//Asignamos el contenido al popup
				$('#'+usuarios_config.modName+'Modal_container').html(contenido);

				//Agregamos scrollbars
				$('#'+usuarios_config.modName+'Modal_container .scrollable').mCustomScrollbar({theme: 'minimal-dark',scrollInertia: 100, mouseWheel:{ scrollAmount: 70 }});

				//Agregamos los botones al footer
				$('#'+usuarios_config.modName+'Modal_footer').append('<button type="button" class="btn btn-primary" style="display: block; width: 100%; margin: 10px auto;" onclick="'+usuarios_config.modLabelPlural.toLowerCase()+'_edita_confirm(\''+id+'\');">Editar '+usuarios_config.modLabelSingular+'</button>');
				$('#'+usuarios_config.modName+'Modal_footer').append('<button type="button" class="btn btn-warning" style="display: block; width: 100%; margin: 10px auto;" onclick="'+usuarios_config.modLabelPlural.toLowerCase()+'_listar();">Volver</button>');
			}
		});
	}
}
{/if}

{if="$GLOBALS['permisos']['usuarios_editar']"}
//Edita
function usuarios_edita_confirm(id){

	//Data
	var data = {};
	data['usuario'] = id;

	//Agregamos todos los campos
	for(var k in usuarios_config.campos){
		var campo = usuarios_config.campos[k];
		if(campo.mostrarEditar){
			if((campo.tipo == 'input') || (campo.tipo == 'checkbox'))
				data[campo.id] = $('#'+usuarios_config.modName+'_'+campo.id).val();
		}
	}

	//Rol
	var rol = $('#usuarios_rol').val_();
	if(rol != null) data['rol'] = rol.id;

	//Horario
	var horario = $('#usuarios_horario').val_();
	data['horario'] = horario.id;

	apix(usuarios_config.modName,'editar',data, {
		ok: function(json){
			notifica(	usuarios_config.modLabelSingular+' '+usuarios_config.editaLabel.toLowerCase(),
						usuarios_config.artLabel+' '+usuarios_config.modLabelSingular+' se editó con éxito',
						'confirmacion','success');
			usuarios_listar();
		}
	});
}
{/if}

{if="$GLOBALS['permisos']['usuarios_crear']"}
//Agrega
function usuarios_agrega_confirm(){

	//Data
	var data = {};

	//Agregamos todos los campos
	for(var k in usuarios_config.campos){
		var campo = usuarios_config.campos[k];
		if(campo.mostrarCrear)
			data[campo.id] = $('#'+usuarios_config.modName+'_'+campo.id).val_();
	}

	//Tomamos el id del rol
	var rol = $('#usuarios_rol').val_();
	if(rol != null) data['rol'] = rol.id;

	apix(usuarios_config.modName,'agregar',data, {
		ok: function(json){
			//Notifica
			notifica(	usuarios_config.modLabelSingular+' '+usuarios_config.creaLabel.toLowerCase(),
						usuarios_config.artLabel+' '+usuarios_config.modLabelSingular+' se creó con éxito',
						'confirmacion',
						'success'
					);
			//Update
			usuarios_listar();
		}
	});
}
{/if}

{if="$GLOBALS['permisos']['usuarios_eliminar']"}
//Elimina
function usuarios_elimina(id){

	//Data array
	var data = {};
	data['usuario'] = id;
	
	apix(usuarios_config.modName,'eliminar',data, {
		ok: function(json){
			//Notifica
			notifica(	usuarios_config.modLabelSingular+' '+usuarios_config.eliminaLabel,
						usuarios_config.artLabel+' '+usuarios_config.modLabelSingular+' se eliminó con éxito',
						'informacion',
						'info'
					);
			//Update
			usuarios_listar();
		}
	});
}
{/if}

{if="$GLOBALS['permisos']['usuarios_permisos']"}
//Lista
function usuarios_permisos_listar(id,rol){

	//Titulo...
	$('#'+usuarios_config.modName+'Modal_titulo').html('Permisos a las Funciones de <b>'+id+'</b> <i>('+rol+')</i>');

	//Cargando...
	$('#'+usuarios_config.modName+'Modal_container').html( cargando() );

	//Footer...
	$('#'+usuarios_config.modName+'Modal_footer').html('');

	//Traemos los datos de la API
	var data = {};
	data['usuario'] = id;
	apix(usuarios_config.modName,'permisos_listar',data, {
		ok: function(json){

			//Generamos tabla
			var tabla = generaTabla({
					id_tabla: usuarios_config.modName+'_permisos',
					items: json.result,
					keys_busqueda: ['grupo','accion'],
					key_nombre: null,
					max_height: usuarios_config.max_height_lista,
					funcion_activado: function(el){
					},
					funcion_desactivado: function(el){

						//Heredado o no?
						if($(el).data('acceso_manual') == '-1') var hereda = '1';
						else var hereda = '0';

						//Valor por defecto
						if(hereda == '1') var default_val = $(el).data('acceso');
						else var default_val = $(el).data('acceso_manual');

						//Creamos todos los objetos si es la primera vez
						//Que estamos creando el item...
						if(typeof $(el).data('item_render') === 'undefined'){

							//Boton que indica si esta Habilitado o no y ademas funciona como switch en el caso de ser Manual.
							var indicador = $('<button ' + (parseInt(hereda) ? 'disabled' : '') + ' id="usuarios_permiso_id'+$(el).data('id')+'" class="btn btn-xs usuarios_permisos_acciones_acceso"></button>');
							$(indicador).boton({
								claseActivo: 'btn-success',
								claseInactivo: 'btn-danger',
								valueActivo: '1',
								valueInactivo: '0',
								valueDefault: default_val,
								labelActivo: function(el){
									$(el).html('<i class="fa fa-check-square-o"></i> Habilitado');

									//Item
									var a = $(el).parent().parent();

									//Si tenemos el item
									if(a.length) a.data('acceso_manual','1');
								},
								labelInactivo: function(el){
									$(el).html('<i class="fa fa-ban"></i> Deshabilitado');

									//Item
									var a = $(el).parent().parent();

									//Si tenemos el item
									if(a.length) 
										a.data('acceso_manual','0');
								}
							});


							//Checkbox
							var checkbox = $('<button class="btn btn-xs btn-primary usuarios_permisos_acciones_hereda" style="margin-top: 10px;" data-id="'+$(el).data('id')+'"></button>');
							
							$(checkbox).boton({
								claseActivo: '',
								claseInactivo: '',
								valueActivo: '1',
								valueInactivo: '0',
								valueDefault: hereda,
								labelActivo: function(el){
									$(el).html('Permisos heredados del Rol');

									//Switch acceso
									var switchh = $('#usuarios_permiso_id'+ $(el).data('id') );
									//Item data
									var a = switchh.parent().parent();

									//Si estamos el modo automatico
									//Los permisos se heredan del rol
									//El boton esta deshabilitado
									switchh.attr("disabled", "disabled");
									switchh.val_(a.data('acceso'));

								},
								labelInactivo: function(el){
									$(el).html('Permisos definidos Manualmente');

									//Switch acceso
									var switchh = $('#usuarios_permiso_id'+ $(el).data('id') );
									//Item data
									var a = switchh.parent().parent();

										//Si estamos el modo automatico
										//Los permisos se heredan del rol
										//El boton esta deshabilitado
										switchh.removeAttr("disabled");
								}
							});

						
							//Cuadro de acciones sobre el permiso del usuario.
							var acciones = $('<div style="text-align: right; float: right;" class="usuarios_permisos_acciones"></div>');
							acciones.append(indicador);
							acciones.append('<br/>');
							acciones.append(checkbox);


							//Cuadro de información de la función
							var info = $('<div style="float: left; width: 250px;" class="usuarios_permisos_info"><b>' + $(el).data('grupo') + '</b><br/><small>' + $(el).data('accion') + '</div>');


							//Indicamos que el item ya fue creado
							$(el).data('item_render', true);

							//cambiamos HTML
							$(el).html(info);
							$(el).append(acciones);
							$(el).append('<div class="clearfix"></div>');
						
						//Creamos
						}else{


							
						}
					}
				});

			//Agregamos los tooltips
			tabla.tooltip({selector: '[data-toggle="tooltip"]', placement: 'left', animation: false});

			//Generamos el listado
			$('#'+usuarios_config.modName+'Modal_container').html('');
			$('#'+usuarios_config.modName+'Modal_container').append(tabla);

			//Agregamos scrollbars
			$('#'+usuarios_config.modName+'Modal_container .scrollable').mCustomScrollbar({theme: 'minimal-dark',scrollInertia: 100, mouseWheel:{ scrollAmount: 50 }});

			//Boton Confirmar cambios
			$('#'+usuarios_config.modName+'Modal_footer').append('<button type="button" class="btn btn-primary" style="display: block; width: 100%; margin: 10px auto;" onclick="'+usuarios_config.modName+'_permisos_editar(\''+id+'\',\''+rol+'\');">Guardar cambios</button>');

			//Append Cancel Button
			$('#'+usuarios_config.modName+'Modal_footer').append('<button type="button" class="btn btn-warning" style="display: block; width: 100%; margin: 10px auto;" onclick="'+usuarios_config.modName+'_listar();">Volver</button>');
		}
	});
}
{/if}

{if="$GLOBALS['permisos']['usuarios_permisos']"}
//Guarda Permisos
function usuarios_permisos_editar(id,rol){
	//Array parametro permisos
	var permisos = Array();

	//Creamos array
	$('#'+usuarios_config.modName+'_permisos_lista').find('.list-group-item').each(function(i, e){

		if( $('.usuarios_permisos_acciones_hereda',e).val() == '0' )
			permisos.push( { id: $(e).data('id'), acceso: $('.usuarios_permisos_acciones_acceso',e).val() } );

	});

	//Data
	var data = {};
	data['usuario'] = id;
	data['permisos'] = JSON.stringify(permisos);

	//DB confirm
	apix(usuarios_config.modName,'permisos_editar',data, {
		ok: function(json){
			notifica('Permisos editados','Los permisos a las Funciones del sistema se editaron con éxito','confirmacion','success');
			usuarios_permisos_listar(id,rol);
		}
	});
}
{/if}


{if="$GLOBALS['permisos']['usuarios_vacaciones']"}
//Lista
function usuarios_vacaciones_listar(usuario){

	//Titulo...
	$('#usuariosModal_titulo').html('Vacaciones');

	//Cargando...
	$('#usuariosModal_container').html( cargando() );

	//Footer...
	$('#usuariosModal_footer').html('');

	var data = {};
	data['usuario'] = usuario;

	//Traemos los datos de la API
	apix('horarios','vacaciones_listar',data, {
		ok: function(json){

			//Titulo...
			$('#usuariosModal_titulo').html('Vacaciones de '+json.result.nombre);

			//Tabla
			var tabla = generaTabla({
							id_tabla: 'usuarios_vacaciones',
							items: json.result.vacaciones,
							keys_busqueda: ['nombre'],
							key_nombre: null,
							max_height: 400,
							campobusqueda: false,
							funcion_activado: function(el){
							},
							funcion_desactivado: function(el){
								var html = '<span style="display: inline-block; width: 200px;"><b>Inician: </b>'+$(el).data('inicio')+'</span><span style="display: inline-block; width: 200px;"><b>Finalizan: </b>'+$(el).data('fin')+'</span><div class="pull-right descripcion" style="width: 120px; text-align: right; position: absolute; top: 8px; right: 10px;"><button class="btn btn-xs btn-primary" style="margin: 0px 0px 8px 5px; width: 24px; height: 24px;" data-toggle="tooltip" title="" onclick="usuarios_vacaciones_edita_agrega('+$(el).data('id')+',\''+usuario+'\');" data-original-title="Editar"><i class="fa fa-pencil"></i></button><button class="btn btn-xs btn-warning" style="margin: 0px 0px 8px 5px; width: 24px; height: 24px;" data-toggle="tooltip" title="Borrar" onclick=" confirma(\'Eliminar Vacaciones\',\'Las Vacaciones se eliminarán de forma permanente junto con todos los datos relacionados.\',\'exclamacion\',\'danger\',\'usuarios_vacaciones_eliminar('+$(el).data('id')+',\\\''+usuario+'\\\')\');"><i class="fa fa-remove"></i></button><div></div></div>';
								
								$(el).html(html);
							}
						});

			//Agregamos los tooltips
			tabla.tooltip({selector: '[data-toggle="tooltip"]', placement: 'left', animation: false});

			//Generamos el listado
			$('#usuariosModal_container').html('');
			$('#usuariosModal_container').append(tabla);


			//Agregamos scrollbars
			$('#usuariosModal_container .scrollable').mCustomScrollbar({theme: 'minimal-dark',scrollInertia: 100, mouseWheel:{ scrollAmount: 70 }});

			//Agregar
			$('#usuariosModal_footer').append('<button type="button" class="btn btn-primary" style="display: block; width: 100%; margin: 10px auto;" onclick="usuarios_vacaciones_edita_agrega(0,\''+usuario+'\');">Agregar Vacaciones</button>');

			//Volver
			$('#usuariosModal_footer').append('<button type="button" class="btn btn-warning" style="display: block; width: 100%; margin: 10px auto;" onclick="usuarios_listar();">Volver</button>');
		}
	});
}

//Eliminar Horario
function usuarios_vacaciones_eliminar(id,usuario){
	//Data array
	var data = {};
	data['vacaciones'] = id;
	
	apix('horarios','vacaciones_eliminar',data, {
        ok: function(json){
			//Notifica
			notifica(	'Fecha de Vacaciones Eliminadas',
						'La fecha de vacaciones se eliminó con éxito',
						'informacion',
						'info'
					);
			//Update
			usuarios_vacaciones_listar(usuario);
		}
	});
}

//Edita/Agrega
function usuarios_vacaciones_edita_agrega(id,usuario){

	//Si estamos agregando...
	if(!id) var agrega = true;
	else var agrega = false;

	//Titulo...
	if(agrega) $('#usuariosModal_titulo').html('Agregar fecha de Vacaciones');
	else $('#usuariosModal_titulo').html('Editar fechas de Vacaciones');

	//Cargando...
	$('#usuariosModal_container').html(cargando());

	//Footer...
	$('#usuariosModal_footer').html('');

	//Contenido
	var contenido = $('<div class="scrollable" style="max-height: 500px; overflow-y: hidden;"></div>'); 

	//Agregamos los campos
	contenido.append( genera_campo(	{	
		id: 			"fecha",
		label: 			"Fecha de Vacaciones",
		labelDesde: 	'Desde',
		labelHasta: 	'Hasta',
		tipo: 			"daterange",
		mostrarEditar: 	true,
		mostrarCrear: 	true,
		config: 		{
						format: 'dd/mm/yyyy',
						autoclose: true,
						language: 'es'
		}
	}, 'usuarios_vacaciones' ,true) );

	//Agregamos el contenido y los botones
	//si estamos creando un registro nuevo
	if(agrega){
		//Agregamos el contenido
		$('#usuariosModal_container').html(contenido);

		//Agregamos scrollbars
		$('#usuariosModal_container .scrollable').mCustomScrollbar({theme: 'minimal-dark',scrollInertia: 100, mouseWheel:{ scrollAmount: 70 }});

		//Agregamos los botones al footer
		$('#usuariosModal_footer').append('<button type="button" class="btn btn-primary" style="display: block; width: 100%; margin: 10px auto;" onclick="usuarios_vacaciones_agrega_confirm(\''+usuario+'\');">Agregar Vacaciones</button>');
		$('#usuariosModal_footer').append('<button type="button" class="btn btn-warning" style="display: block; width: 100%; margin: 10px auto;" onclick="usuarios_vacaciones_listar(\''+usuario+'\');">Volver</button>');
	}


	//Si editamos
	if(!agrega){
		var data = {};
		data['vacaciones'] = id;

		apix('horarios','vacaciones_info',data, {
            ok: function(json){

				//Titulo
				$('#usuariosModal_titulo').html('Editar Fecha de Vacaciones');

		    	//Asignamos el contenido al popup
				$('#usuariosModal_container').html(contenido);

				//Fechas
				$('#usuarios_vacaciones_fecha_hasta').datepicker('update', json.result.fin);
				$('#usuarios_vacaciones_fecha_desde').datepicker('update', json.result.inicio);

				//Agregamos scrollbars
				$('#usuariosModal_container .scrollable').mCustomScrollbar({theme: 'minimal-dark',scrollInertia: 100, mouseWheel:{ scrollAmount: 70 }});

				//Agregamos los botones al footer
				$('#usuariosModal_footer').append('<button type="button" class="btn btn-primary" style="display: block; width: 100%; margin: 10px auto;" onclick="usuarios_vacaciones_edita_confirm('+id+',\''+usuario+'\');">Editar Horario</button>');
				$('#usuariosModal_footer').append('<button type="button" class="btn btn-warning" style="display: block; width: 100%; margin: 10px auto;" onclick="usuarios_vacaciones_listar(\''+usuario+'\');">Volver</button>');
			}
		});
	}
}

//Edita
function usuarios_vacaciones_edita_confirm(id,usuario){
	//Data
	var data = {};
	data['vacaciones'] = id;
	data['inicio'] = $('#usuarios_vacaciones_fecha_desde').val();
	data['fin'] = $('#usuarios_vacaciones_fecha_hasta').val();

	apix('horarios','vacaciones_editar',data, {
		ok: function(json){
			notifica(	'Vacaciones Editadas',
						'Las fechas de las Vacaciones se editaron con éxito',
						'confirmacion','success');
			usuarios_vacaciones_listar(usuario);
		}
	});
}

//Agrega
function usuarios_vacaciones_agrega_confirm(usuario){
	//Data
	var data = {};
	data['usuario'] = usuario;
	data['inicio'] = $('#usuarios_vacaciones_fecha_desde').val();
	data['fin'] = $('#usuarios_vacaciones_fecha_hasta').val();

	apix('horarios','vacaciones_agregar',data, {
		ok: function(json){
			notifica(	'Vacaciones Editadas',
						'Las fechas de las Vacaciones se editaron con éxito',
						'confirmacion','success');
			usuarios_vacaciones_listar(usuario);
		}
	});
}
{/if}


</script>