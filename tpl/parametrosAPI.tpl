<script type="text/javascript">

//Config Usuarios
var parametrosAPI_config = {
	modName: 					'parametrosAPI',
	modLabelSingular: 			'Parametro',
	modLabelPlural: 			'Parametros',
	creaLabel: 					'Agregado',
	editaLabel: 				'Editado',
	eliminaLabel: 				'Eliminado',
	nuevoLabel: 				'Nuevo',
	artLabel: 					'El',
	max_height_edita_agrega: 	'300',
	max_height_lista: 			'300',
	campos:[
		{	
			id: 			"identificador",
			label: 			"Identificador",
			tipo: 			"input",
			mostrarEditar: 	true,
			mostrarCrear: 	true
		},
		{	
			id: 			"label_articulo",
			label: 			"Articulo del Label",
			tipo: 			"input",
			mostrarEditar: 	true,
			mostrarCrear: 	true
		},
		{	
			id: 			"label",
			label: 			"Label",
			tipo: 			"input",
			mostrarEditar: 	true,
			mostrarCrear: 	true
		},
		{	
			id: 			"descripcion",
			label: 			"Descripción",
			tipo: 			"textarea",
			mostrarEditar: 	true,
			mostrarCrear: 	true
		},
		{	
			id: 			"obligatorio",
			tipo: 			"checkbox",
			mostrarEditar: 	true,
			mostrarCrear: 	true,
			config: 		{
							claseActivo: 'btn-danger',
							claseInactivo: 'btn-success',
							valueActivo: '1',
							valueInactivo: '0',
							valueDefault: '1',
							labelActivo: function(el){
								el.html('El Parametro es obligatorio');
							},
							labelInactivo: function(el){
								el.html('El Parametro es opcional');
							}
			}
		}
	],
	campos_caracteres:[
		{	
			id: 			"caracteres_mayusculas",
			tipo: 			"checkbox",
			mostrarEditar: 	true,
			mostrarCrear: 	true,
			config: 		{
							claseActivo: 'btn-success',
							claseInactivo: 'btn-default',
							valueActivo: '1',
							valueInactivo: '0',
							valueDefault: '1',
							labelActivo: function(el){
								el.html('Permitir las mayusculas');
							},
							labelInactivo: function(el){
								el.html('No permitir las mayusculas');
							}
			}
		},		
		{	
			id: 			"caracteres_minusculas",
			tipo: 			"checkbox",
			mostrarEditar: 	true,
			mostrarCrear: 	true,
			config: 		{
							claseActivo: 'btn-success',
							claseInactivo: 'btn-default',
							valueActivo: '1',
							valueInactivo: '0',
							valueDefault: '1',
							labelActivo: function(el){
								el.html('Permitir las minusculas');
							},
							labelInactivo: function(el){
								el.html('No permitir las minusculas');
							}
			}
		},		
		{	
			id: 			"caracteres_acentos",
			tipo: 			"checkbox",
			mostrarEditar: 	true,
			mostrarCrear: 	true,
			config: 		{
							claseActivo: 'btn-success',
							claseInactivo: 'btn-default',
							valueActivo: '1',
							valueInactivo: '0',
							valueDefault: '1',
							labelActivo: function(el){
								el.html('Permitir los acentos');
							},
							labelInactivo: function(el){
								el.html('No permitir los acentos');
							}
			}
		},		
		{	
			id: 			"caracteres_numeros",
			tipo: 			"checkbox",
			mostrarEditar: 	true,
			mostrarCrear: 	true,
			config: 		{
							claseActivo: 'btn-success',
							claseInactivo: 'btn-default',
							valueActivo: '1',
							valueInactivo: '0',
							valueDefault: '1',
							labelActivo: function(el){
								el.html('Permitir los números');
							},
							labelInactivo: function(el){
								el.html('No permitir los números');
							}
			}
		},		
		{	
			id: 			"caracteres_corchetes",
			tipo: 			"checkbox",
			mostrarEditar: 	true,
			mostrarCrear: 	true,
			config: 		{
							claseActivo: 'btn-success',
							claseInactivo: 'btn-default',
							valueActivo: '1',
							valueInactivo: '0',
							valueDefault: '1',
							labelActivo: function(el){
								el.html('Permitir los corchetes');
							},
							labelInactivo: function(el){
								el.html('No permitir los corchetes');
							}
			}
		},		
		{	
			id: 			"caracteres_parentesis",
			tipo: 			"checkbox",
			mostrarEditar: 	true,
			mostrarCrear: 	true,
			config: 		{
							claseActivo: 'btn-success',
							claseInactivo: 'btn-default',
							valueActivo: '1',
							valueInactivo: '0',
							valueDefault: '1',
							labelActivo: function(el){
								el.html('Permitir los parentesis');
							},
							labelInactivo: function(el){
								el.html('No permitir los parentesis');
							}
			}
		},		
		{	
			id: 			"caracteres_llaves",
			tipo: 			"checkbox",
			mostrarEditar: 	true,
			mostrarCrear: 	true,
			config: 		{
							claseActivo: 'btn-success',
							claseInactivo: 'btn-default',
							valueActivo: '1',
							valueInactivo: '0',
							valueDefault: '1',
							labelActivo: function(el){
								el.html('Permitir las llaves');
							},
							labelInactivo: function(el){
								el.html('No permitir las llaves');
							}
			}
		},		
		{	
			id: 			"caracteres_espacios",
			tipo: 			"checkbox",
			mostrarEditar: 	true,
			mostrarCrear: 	true,
			config: 		{
							claseActivo: 'btn-success',
							claseInactivo: 'btn-default',
							valueActivo: '1',
							valueInactivo: '0',
							valueDefault: '1',
							labelActivo: function(el){
								el.html('Permitir los espacios');
							},
							labelInactivo: function(el){
								el.html('No permitir los espacios');
							}
			}
		},		
		{	
			id: 			"caracteres_puntos",
			tipo: 			"checkbox",
			mostrarEditar: 	true,
			mostrarCrear: 	true,
			config: 		{
							claseActivo: 'btn-success',
							claseInactivo: 'btn-default',
							valueActivo: '1',
							valueInactivo: '0',
							valueDefault: '1',
							labelActivo: function(el){
								el.html('Permitir los puntos');
							},
							labelInactivo: function(el){
								el.html('No permitir los puntos');
							}
			}
		},		
		{	
			id: 			"caracteres_comas",
			tipo: 			"checkbox",
			mostrarEditar: 	true,
			mostrarCrear: 	true,
			config: 		{
							claseActivo: 'btn-success',
							claseInactivo: 'btn-default',
							valueActivo: '1',
							valueInactivo: '0',
							valueDefault: '1',
							labelActivo: function(el){
								el.html('Permitir las comas');
							},
							labelInactivo: function(el){
								el.html('No permitir las comas');
							}
			}
		},		
		{	
			id: 			"caracteres_puntosycomas",
			tipo: 			"checkbox",
			mostrarEditar: 	true,
			mostrarCrear: 	true,
			config: 		{
							claseActivo: 'btn-success',
							claseInactivo: 'btn-default',
							valueActivo: '1',
							valueInactivo: '0',
							valueDefault: '1',
							labelActivo: function(el){
								el.html('Permitir los punto y comas');
							},
							labelInactivo: function(el){
								el.html('No permitir los punto y comas');
							}
			}
		},		
		{	
			id: 			"caracteres_dospuntos",
			tipo: 			"checkbox",
			mostrarEditar: 	true,
			mostrarCrear: 	true,
			config: 		{
							claseActivo: 'btn-success',
							claseInactivo: 'btn-default',
							valueActivo: '1',
							valueInactivo: '0',
							valueDefault: '1',
							labelActivo: function(el){
								el.html('Permitir los dos puntos');
							},
							labelInactivo: function(el){
								el.html('No permitir los dos puntos');
							}
			}
		},		
		{	
			id: 			"caracteres_guion",
			tipo: 			"checkbox",
			mostrarEditar: 	true,
			mostrarCrear: 	true,
			config: 		{
							claseActivo: 'btn-success',
							claseInactivo: 'btn-default',
							valueActivo: '1',
							valueInactivo: '0',
							valueDefault: '1',
							labelActivo: function(el){
								el.html('Permitir los guiones');
							},
							labelInactivo: function(el){
								el.html('No permitir los guiones');
							}
			}
		},		
		{	
			id: 			"caracteres_guionbajo",
			tipo: 			"checkbox",
			mostrarEditar: 	true,
			mostrarCrear: 	true,
			config: 		{
							claseActivo: 'btn-success',
							claseInactivo: 'btn-default',
							valueActivo: '1',
							valueInactivo: '0',
							valueDefault: '1',
							labelActivo: function(el){
								el.html('Permitir los guiones bajos');
							},
							labelInactivo: function(el){
								el.html('No permitir los guiones bajos');
							}
			}
		},		
		{	
			id: 			"caracteres_mas",
			tipo: 			"checkbox",
			mostrarEditar: 	true,
			mostrarCrear: 	true,
			config: 		{
							claseActivo: 'btn-success',
							claseInactivo: 'btn-default',
							valueActivo: '1',
							valueInactivo: '0',
							valueDefault: '1',
							labelActivo: function(el){
								el.html('Permitir el símbolo más');
							},
							labelInactivo: function(el){
								el.html('No permitir el símbolo más');
							}
			}
		},		
		{	
			id: 			"caracteres_asterisco",
			tipo: 			"checkbox",
			mostrarEditar: 	true,
			mostrarCrear: 	true,
			config: 		{
							claseActivo: 'btn-success',
							claseInactivo: 'btn-default',
							valueActivo: '1',
							valueInactivo: '0',
							valueDefault: '1',
							labelActivo: function(el){
								el.html('Permitir el asterisco');
							},
							labelInactivo: function(el){
								el.html('No permitir el asterisco');
							}
			}
		},		
		{	
			id: 			"caracteres_igual",
			tipo: 			"checkbox",
			mostrarEditar: 	true,
			mostrarCrear: 	true,
			config: 		{
							claseActivo: 'btn-success',
							claseInactivo: 'btn-default',
							valueActivo: '1',
							valueInactivo: '0',
							valueDefault: '1',
							labelActivo: function(el){
								el.html('Permitir el símbolo igual');
							},
							labelInactivo: function(el){
								el.html('No permitir el símbolo igual');
							}
			}
		},		
		{	
			id: 			"caracteres_porcentual",
			tipo: 			"checkbox",
			mostrarEditar: 	true,
			mostrarCrear: 	true,
			config: 		{
							claseActivo: 'btn-success',
							claseInactivo: 'btn-default',
							valueActivo: '1',
							valueInactivo: '0',
							valueDefault: '1',
							labelActivo: function(el){
								el.html('Permitir el símbolo porcentual');
							},
							labelInactivo: function(el){
								el.html('No permitir el símbolo porcentual');
							}
			}
		},		
		{	
			id: 			"caracteres_barra",
			tipo: 			"checkbox",
			mostrarEditar: 	true,
			mostrarCrear: 	true,
			config: 		{
							claseActivo: 'btn-success',
							claseInactivo: 'btn-default',
							valueActivo: '1',
							valueInactivo: '0',
							valueDefault: '1',
							labelActivo: function(el){
								el.html('Permitir la barra (/)');
							},
							labelInactivo: function(el){
								el.html('No permitir la barra (/)');
							}
			}
		},		
		{	
			id: 			"caracteres_barrainvertida",
			tipo: 			"checkbox",
			mostrarEditar: 	true,
			mostrarCrear: 	true,
			config: 		{
							claseActivo: 'btn-success',
							claseInactivo: 'btn-default',
							valueActivo: '1',
							valueInactivo: '0',
							valueDefault: '1',
							labelActivo: function(el){
								el.html('Permitir la barra invertida (\)');
							},
							labelInactivo: function(el){
								el.html('No permitir la barra invertida (\)');
							}
			}
		},		
		{	
			id: 			"caracteres_mas",
			tipo: 			"checkbox",
			mostrarEditar: 	true,
			mostrarCrear: 	true,
			config: 		{
							claseActivo: 'btn-success',
							claseInactivo: 'btn-default',
							valueActivo: '1',
							valueInactivo: '0',
							valueDefault: '1',
							labelActivo: function(el){
								el.html('Permitir el símbolo más');
							},
							labelInactivo: function(el){
								el.html('No permitir el símbolo más');
							}
			}
		},		
		{	
			id: 			"caracteres_moneda",
			tipo: 			"checkbox",
			mostrarEditar: 	true,
			mostrarCrear: 	true,
			config: 		{
							claseActivo: 'btn-success',
							claseInactivo: 'btn-default',
							valueActivo: '1',
							valueInactivo: '0',
							valueDefault: '1',
							labelActivo: function(el){
								el.html('Permitir el símbolo moneda');
							},
							labelInactivo: function(el){
								el.html('No permitir el símbolo moneda');
							}
			}
		},		
		{	
			id: 			"caracteres_interrogacion",
			tipo: 			"checkbox",
			mostrarEditar: 	true,
			mostrarCrear: 	true,
			config: 		{
							claseActivo: 'btn-success',
							claseInactivo: 'btn-default',
							valueActivo: '1',
							valueInactivo: '0',
							valueDefault: '1',
							labelActivo: function(el){
								el.html('Permitir signos de interrogación');
							},
							labelInactivo: function(el){
								el.html('No permitir signos de interrogación');
							}
			}
		},		
		{	
			id: 			"caracteres_admiracion",
			tipo: 			"checkbox",
			mostrarEditar: 	true,
			mostrarCrear: 	true,
			config: 		{
							claseActivo: 'btn-success',
							claseInactivo: 'btn-default',
							valueActivo: '1',
							valueInactivo: '0',
							valueDefault: '1',
							labelActivo: function(el){
								el.html('Permitir signos de admiración');
							},
							labelInactivo: function(el){
								el.html('No permitir signos de admiración');
							}
			}
		},		
		{	
			id: 			"caracteres_arroba",
			tipo: 			"checkbox",
			mostrarEditar: 	true,
			mostrarCrear: 	true,
			config: 		{
							claseActivo: 'btn-success',
							claseInactivo: 'btn-default',
							valueActivo: '1',
							valueInactivo: '0',
							valueDefault: '1',
							labelActivo: function(el){
								el.html('Permitir el símbolo arroba');
							},
							labelInactivo: function(el){
								el.html('No permitir el símbolo arroba');
							}
			}
		},		
		{	
			id: 			"caracteres_mayorymenor",
			tipo: 			"checkbox",
			mostrarEditar: 	true,
			mostrarCrear: 	true,
			config: 		{
							claseActivo: 'btn-success',
							claseInactivo: 'btn-default',
							valueActivo: '1',
							valueInactivo: '0',
							valueDefault: '1',
							labelActivo: function(el){
								el.html('Permitir signos Mayor y Menor');
							},
							labelInactivo: function(el){
								el.html('No permitir signos Mayor y Menor');
							}
			}
		},		
		{	
			id: 			"caracteres_ampersand",
			tipo: 			"checkbox",
			mostrarEditar: 	true,
			mostrarCrear: 	true,
			config: 		{
							claseActivo: 'btn-success',
							claseInactivo: 'btn-default',
							valueActivo: '1',
							valueInactivo: '0',
							valueDefault: '1',
							labelActivo: function(el){
								el.html('Permitir el símbolo Ampersand');
							},
							labelInactivo: function(el){
								el.html('No permitir el símbolo Ampersand');
							}
			}
		},		
		{	
			id: 			"caracteres_numeral",
			tipo: 			"checkbox",
			mostrarEditar: 	true,
			mostrarCrear: 	true,
			config: 		{
							claseActivo: 'btn-success',
							claseInactivo: 'btn-default',
							valueActivo: '1',
							valueInactivo: '0',
							valueDefault: '1',
							labelActivo: function(el){
								el.html('Permitir numeral');
							},
							labelInactivo: function(el){
								el.html('No permitir numeral');
							}
			}
		},		
		{	
			id: 			"caracteres_comillassimples",
			tipo: 			"checkbox",
			mostrarEditar: 	true,
			mostrarCrear: 	true,
			config: 		{
							claseActivo: 'btn-success',
							claseInactivo: 'btn-default',
							valueActivo: '1',
							valueInactivo: '0',
							valueDefault: '1',
							labelActivo: function(el){
								el.html('Permitir comillas simples');
							},
							labelInactivo: function(el){
								el.html('No permitir comillas simples');
							}
			}
		},		
		{	
			id: 			"caracteres_comillasdobles",
			tipo: 			"checkbox",
			mostrarEditar: 	true,
			mostrarCrear: 	true,
			config: 		{
							claseActivo: 'btn-success',
							claseInactivo: 'btn-default',
							valueActivo: '1',
							valueInactivo: '0',
							valueDefault: '1',
							labelActivo: function(el){
								el.html('Permitir comillas dobles');
							},
							labelInactivo: function(el){
								el.html('No permitir comillas dobles');
							}
			}
		},
		{	
			id: 			"caracteres_saltosdelinea",
			tipo: 			"checkbox",
			mostrarEditar: 	true,
			mostrarCrear: 	true,
			config: 		{
							claseActivo: 'btn-success',
							claseInactivo: 'btn-default',
							valueActivo: '1',
							valueInactivo: '0',
							valueDefault: '1',
							labelActivo: function(el){
								el.html('Permitir Saltos de Linea');
							},
							labelInactivo: function(el){
								el.html('No permitir Saltos de Linea');
							}
			}
		},
		{	
			id: 			"caracteres_vacio",
			tipo: 			"checkbox",
			mostrarEditar: 	true,
			mostrarCrear: 	true,
			config: 		{
							claseActivo: 'btn-success',
							claseInactivo: 'btn-default',
							valueActivo: '1',
							valueInactivo: '0',
							valueDefault: '1',
							labelActivo: function(el){
								el.html('Vacio');
							},
							labelInactivo: function(el){
								el.html('Vacio');
							}
			}
		},
		{	
			id: 			"caracteres_personalizado",
			label: 			"Filtro personalizado",
			tipo: 			"input",
			mostrarEditar: 	true,
			mostrarCrear: 	true
		},
		{	
			id: 			"caracteres_quitarcaracteres",
			label: 			"Quita los siguientes Caracteres",
			tipo: 			"input",
			mostrarEditar: 	true,
			mostrarCrear: 	true
		}
	],	
	campos_procesos:[
		{	
			id: 			"validador_errorenfiltro",
			tipo: 			"checkbox",
			mostrarEditar: 	true,
			mostrarCrear: 	true,
			config: 		{
							claseActivo: 'btn-success',
							claseInactivo: 'btn-default',
							valueActivo: '1',
							valueInactivo: '0',
							valueDefault: '1',
							labelActivo: function(el){
								el.html('Mostrar error si hay caracteres fuera del filtro');
							},
							labelInactivo: function(el){
								el.html('No mostrar error si hay caracteres fuera del filtro');
							}
			}
		},
		{	
			id: 			"filtro_convertirtildes",
			tipo: 			"checkbox",
			mostrarEditar: 	true,
			mostrarCrear: 	true,
			config: 		{
							claseActivo: 'btn-success',
							claseInactivo: 'btn-default',
							valueActivo: '1',
							valueInactivo: '0',
							valueDefault: '1',
							labelActivo: function(el){
								el.html('Convertir tildes en caracteres comunes');
							},
							labelInactivo: function(el){
								el.html('No convertir tildes en caracteres comunes');
							}
			}
		},
		{	
			id: 			"filtro_convertiraminusculas",
			tipo: 			"checkbox",
			mostrarEditar: 	true,
			mostrarCrear: 	true,
			config: 		{
							claseActivo: 'btn-success',
							claseInactivo: 'btn-default',
							valueActivo: '1',
							valueInactivo: '0',
							valueDefault: '1',
							labelActivo: function(el){
								el.html('Convertir todo a minusculas');
							},
							labelInactivo: function(el){
								el.html('No convertir todo a minusculas');
							}
			}
		},
		{	
			id: 			"filtro_convertiramayusculas",
			tipo: 			"checkbox",
			mostrarEditar: 	true,
			mostrarCrear: 	true,
			config: 		{
							claseActivo: 'btn-success',
							claseInactivo: 'btn-default',
							valueActivo: '1',
							valueInactivo: '0',
							valueDefault: '1',
							labelActivo: function(el){
								el.html('Convertir todo a mayusculas');
							},
							labelInactivo: function(el){
								el.html('No convertir todo a mayusculas');
							}
			}
		},
		{	
			id: 			"filtro_castearainteger",
			tipo: 			"checkbox",
			mostrarEditar: 	true,
			mostrarCrear: 	true,
			config: 		{
							claseActivo: 'btn-success',
							claseInactivo: 'btn-default',
							valueActivo: '1',
							valueInactivo: '0',
							valueDefault: '1',
							labelActivo: function(el){
								el.html('Convertir el valor a tipo entero');
							},
							labelInactivo: function(el){
								el.html('No convertir el valor a tipo entero');
							}
			}
		},
		{	
			id: 			"filtro_castearadecimal",
			tipo: 			"checkbox",
			mostrarEditar: 	true,
			mostrarCrear: 	true,
			config: 		{
							claseActivo: 'btn-success',
							claseInactivo: 'btn-default',
							valueActivo: '1',
							valueInactivo: '0',
							valueDefault: '1',
							labelActivo: function(el){
								el.html('Convertir el valor a tipo decimal');
							},
							labelInactivo: function(el){
								el.html('No convertir el valor a tipo decimal');
							}
			}
		},
		{	
			id: 			"filtro_castearajson",
			tipo: 			"checkbox",
			mostrarEditar: 	true,
			mostrarCrear: 	true,
			config: 		{
							claseActivo: 'btn-success',
							claseInactivo: 'btn-default',
							valueActivo: '1',
							valueInactivo: '0',
							valueDefault: '1',
							labelActivo: function(el){
								el.html('Convertir el valor a tipo JSON (array)');
							},
							labelInactivo: function(el){
								el.html('No convertir el valor a tipo JSON (array)');
							}
			}
		},
		{	
			id: 			"filtro_castearastring",
			tipo: 			"checkbox",
			mostrarEditar: 	true,
			mostrarCrear: 	true,
			config: 		{
							claseActivo: 'btn-success',
							claseInactivo: 'btn-default',
							valueActivo: '1',
							valueInactivo: '0',
							valueDefault: '1',
							labelActivo: function(el){
								el.html('Convertir el valor a tipo string');
							},
							labelInactivo: function(el){
								el.html('No convertir el valor a tipo string');
							}
			}
		},
		{	
			id: 			"filtro_castearaarchivo",
			tipo: 			"checkbox",
			mostrarEditar: 	true,
			mostrarCrear: 	true,
			config: 		{
							claseActivo: 'btn-success',
							claseInactivo: 'btn-default',
							valueActivo: '1',
							valueInactivo: '0',
							valueDefault: '1',
							labelActivo: function(el){
								el.html('El parámetro contiene un archivo');
							},
							labelInactivo: function(el){
								el.html('El parámetro NO contiene un archivo');
							}
			}
		},
		{	
			id: 			"filtro_email",
			tipo: 			"checkbox",
			mostrarEditar: 	true,
			mostrarCrear: 	true,
			config: 		{
							claseActivo: 'btn-success',
							claseInactivo: 'btn-default',
							valueActivo: '1',
							valueInactivo: '0',
							valueDefault: '1',
							labelActivo: function(el){
								el.html('El valor esperado es un Email');
							},
							labelInactivo: function(el){
								el.html('El valor esperado no es un Email');
							}
			}
		},
		{	
			id: 			"filtro_valorespermitidos",
			label: 			"Filtrar valores PERMITIDOS (Arreglo JSON)",
			tipo: 			"input",
			style: 			"width: 67%; margin-bottom: 10px; margin-right: 10px; float: left;",
			mostrarEditar: 	true,
			mostrarCrear: 	true
		},	
		{	
			id: 			"validador_valorespermitidos",
			tipo: 			"checkbox",
			style: 			"float: right; margin-bottom: 10px; width: 150px;",
			mostrarEditar: 	true,
			mostrarCrear: 	true,
			config: 		{
							claseActivo: 'btn-success',
							claseInactivo: 'btn-default',
							valueActivo: '1',
							valueInactivo: '0',
							valueDefault: '1',
							labelActivo: function(el){
								el.html('Mostrar error');
							},
							labelInactivo: function(el){
								el.html('No mostrar error');
							}
			}
		},
		{	
			id: 			"filtro_valoresnopermitidos",
			label: 			"Filtrar valores NO PERMITIDOS (Arreglo JSON)",
			tipo: 			"input",
			style: 			"width: 67%; margin-bottom: 10px; margin-right: 10px; float: left;",
			mostrarEditar: 	true,
			mostrarCrear: 	true
		},		
		{	
			id: 			"validador_valoresnopermitidos",
			tipo: 			"checkbox",
			style: 			"width: 30%; margin-bottom: 10px; float: right;",
			mostrarEditar: 	true,
			mostrarCrear: 	true,
			config: 		{
							claseActivo: 'btn-success',
							claseInactivo: 'btn-default',
							valueActivo: '1',
							valueInactivo: '0',
							valueDefault: '1',
							labelActivo: function(el){
								el.html('Mostrar error');
							},
							labelInactivo: function(el){
								el.html('No mostrar error');
							}
			}
		},
		{	
			id: 			"filtro_regexp",
			label: 			"Filtrar usando expresión regular",
			tipo: 			"input",
			style: 			"width: 67%; margin-bottom: 10px; margin-right: 10px; float: left;",
			mostrarEditar: 	true,
			mostrarCrear: 	true
		},		
		{	
			id: 			"validador_regexp",
			tipo: 			"checkbox",
			style: 			"width: 30%; margin-bottom: 10px; float: right;",
			mostrarEditar: 	true,
			mostrarCrear: 	true,
			config: 		{
							claseActivo: 'btn-success',
							claseInactivo: 'btn-default',
							valueActivo: '1',
							valueInactivo: '0',
							valueDefault: '1',
							labelActivo: function(el){
								el.html('Mostrar error');
							},
							labelInactivo: function(el){
								el.html('No mostrar error');
							}
			}
		},
		{	
			id: 			"filtro_decimales",
			label: 			"Cantidad de Decimales",
			tipo: 			"input",
			style: 			"width: 67%; margin-bottom: 10px; margin-right: 10px; float: left;",
			mostrarEditar: 	true,
			mostrarCrear: 	true
		},		
		{	
			id: 			"validador_decimales",
			tipo: 			"checkbox",
			style: 			"width: 30%; margin-bottom: 10px; float: right;",
			mostrarEditar: 	true,
			mostrarCrear: 	true,
			config: 		{
							claseActivo: 'btn-success',
							claseInactivo: 'btn-default',
							valueActivo: '1',
							valueInactivo: '0',
							valueDefault: '1',
							labelActivo: function(el){
								el.html('Mostrar error');
							},
							labelInactivo: function(el){
								el.html('No mostrar error');
							}
			}
		},
		{	
			id: 			"filtro_minimo",
			label: 			"Valor Mínimo (entero o decimal)",
			tipo: 			"input",
			style: 			"width: 67%; margin-bottom: 10px; margin-right: 10px; float: left;",
			mostrarEditar: 	true,
			mostrarCrear: 	true
		},		
		{	
			id: 			"validador_minimo",
			tipo: 			"checkbox",
			style: 			"width: 30%; margin-bottom: 10px; float: right;",
			mostrarEditar: 	true,
			mostrarCrear: 	true,
			config: 		{
							claseActivo: 'btn-success',
							claseInactivo: 'btn-default',
							valueActivo: '1',
							valueInactivo: '0',
							valueDefault: '1',
							labelActivo: function(el){
								el.html('Mostrar error');
							},
							labelInactivo: function(el){
								el.html('No mostrar error');
							}
			}
		},
		{	
			id: 			"filtro_maximo",
			label: 			"Valor Máximo (entero o decimal)",
			tipo: 			"input",
			style: 			"width: 67%; margin-bottom: 10px; margin-right: 10px; float: left;",
			mostrarEditar: 	true,
			mostrarCrear: 	true
		},		
		{	
			id: 			"validador_maximo",
			tipo: 			"checkbox",
			style: 			"width: 30%; margin-bottom: 10px; float: right;",
			mostrarEditar: 	true,
			mostrarCrear: 	true,
			config: 		{
							claseActivo: 'btn-success',
							claseInactivo: 'btn-default',
							valueActivo: '1',
							valueInactivo: '0',
							valueDefault: '1',
							labelActivo: function(el){
								el.html('Mostrar error');
							},
							labelInactivo: function(el){
								el.html('No mostrar error');
							}
			}
		},
		{	
			id: 			"validador_minimodecaracteres",
			label: 			"Mínima cantidad de caracteres",
			tipo: 			"input",
			mostrarEditar: 	true,
			mostrarCrear: 	true
		},
		{	
			id: 			"filtro_maximodecaracteres",
			label: 			"Máxima cantidad de caracteres",
			tipo: 			"input",
			style: 			"width: 67%; margin-bottom: 10px; margin-right: 10px; float: left;",
			mostrarEditar: 	true,
			mostrarCrear: 	true
		},		
		{	
			id: 			"validador_maximodecaracteres",
			tipo: 			"checkbox",
			style: 			"width: 30%; margin-bottom: 10px; float: right;",
			mostrarEditar: 	true,
			mostrarCrear: 	true,
			config: 		{
							claseActivo: 'btn-success',
							claseInactivo: 'btn-default',
							valueActivo: '1',
							valueInactivo: '0',
							valueDefault: '0',
							labelActivo: function(el){
								el.html('Mostrar error');
							},
							labelInactivo: function(el){
								el.html('Truncar y continuar');
							}
			}
		},
		{	
			id: 			"valorxdefecto",
			label: 			"Valor por defecto si luego de las validaciones sigue vacio",
			tipo: 			"input",
			mostrarEditar: 	true,
			mostrarCrear: 	true
		},
		{	
			id: 			"filtro_archivo_extensiones",
			label: 			"Archivo: Extensiones permitidas (separadas por ; )",
			tipo: 			"input",
			mostrarEditar: 	true,
			mostrarCrear: 	true
		},
		{	
			id: 			"filtro_archivo_peso",
			label: 			"Archivo: peso máximo en MB (MAX PHP <? echo ini_get('post_max_size') ?>)",
			tipo: 			"input",
			mostrarEditar: 	true,
			mostrarCrear: 	true
		},
		{	
			id: 			"filtro_archivo_directorio",
			label: 			"Archivo: mover archivo a éste directorio (puede incluir el nombre del archivo)",
			tipo: 			"input",
			mostrarEditar: 	true,
			mostrarCrear: 	true
		}
	]
};


//Lista
function parametrosAPI_listar(id,modulo,accion){

	//Creamos el popup
	abrir_modal(parametrosAPI_config.modName, 600, false);

	//Mostramos la ventana
	$('#'+parametrosAPI_config.modName+'Modal').modal({show:true});

	//Titulo...
	$('#'+parametrosAPI_config.modName+'Modal_titulo').html(parametrosAPI_config.modLabelPlural);

	//Cargando...
	$('#'+parametrosAPI_config.modName+'Modal_container').html( cargando() );

	//Footer...
	$('#'+parametrosAPI_config.modName+'Modal_footer').html('');

	//Traemos los datos de la API

	//Contenido
	var contenido = $('<div class="scrollable" style="max-height: '+parametrosAPI_config.max_height_edita_agrega+'px; overflow-y: hidden;"></div>');

	//Generamos Seleccionador de Acciones de la API
	var apiselect2  = genera_campo({	
			id: 			"listar_API",
			label: 			"Acción de la API",
			tipo: 			"select2",
			mostrarEditar: 	true,
			mostrarCrear: 	true,
			config: 		{
								minimumInputLength: 0,
								placeholder: $(this).attr('placeholder'),
								allowClear: false,
								width: 'resolve',
								id: function(e) { return e.id; },
								formatNoMatches: function() { return 'Sin resultados'; },
								formatSearching: function(){ return "Buscando..."; },
								ajax: {
								    url: "api.php",
								    dataType: 'json',
								    quietMillis: 200,
								    data: function(term, page) {
								        return {
								            module: 'parametrosAPI',
								            run: 'listar_accionesAPI',
								            q: term,
								            p: page
								        };
								    },
								    results: function(data, page ) {
								        //Si el ususario no esta logueado refresh
								        if(!data.logued) location.reload();

								        //Calcula si existen mas resultados a mostrar
								        var more = (page * 40) < data.result.total;

								        //Devolvemos el valor de more para que selec2
								        //sepa que debemos cargar mas resultados.
								        return {results: data.result.items, more: more};
								    }
								},
								formatInputTooShort: function () {
									return "";
								},
								formatResult: function(item) {
									return '<i class="badge" style="padding: 4px 6px; float: none; border-radius: 2px; background-color: #e6e6e6; margin-right: 7px; color: #444444; font-size: 11px; border: solid 1px #dadada;">'+item.modulo_nombre+'</i> '+item.accion_nombre;
								},
								formatSelection: function(item) { 
									return '<i class="badge" style="padding: 4px 6px; float: none; border-radius: 2px; background-color: #e6e6e6; margin-right: 7px; color: #444444; font-size: 11px; border: solid 1px #dadada;">'+item.modulo_nombre+'</i> '+item.accion_nombre;
								},
								initSelection : function (element, callback) {
									var elementText = $(element).attr('data-init-text');
								}
							}
			}, parametrosAPI_config.modName ,true);

	//Agregamos el select al contenido
	contenido.append(apiselect2);

	var tabla = generaTabla({
					id_tabla: parametrosAPI_config.modName,
					items: {},
					keys_busqueda: ['id','grupo','accion'],
					key_nombre: null,
					campobusqueda: false,
					max_height: parametrosAPI_config.max_height_lista,
					funcion_activado: function(el){

						//Boton Borrar
						var boton_eliminar = '<button class="btn btn-xs btn-danger" style="margin: 0px 0px 8px 5px; width: 24px; height: 24px;" data-toggle="tooltip" title="Borrar" onclick=" confirma(\'Eliminar '+parametrosAPI_config.modLabelSingular+'\',\'El '+parametrosAPI_config.modLabelSingular+' se eliminará de forma permanente.\',\'exclamacion\',\'danger\',\''+parametrosAPI_config.modName+'_elimina(\\\''+$(el).data('id')+'\\\')\');"><i class="fa fa-remove"></i></button>';

						//Boton Desvincular
						var boton_desvincular = '<button class="btn btn-xs btn-warning" style="margin: 0px 0px 8px 5px; width: 24px; height: 24px;" data-toggle="tooltip" title="Desvincular" onclick=" confirma(\'Desvincular '+parametrosAPI_config.modLabelSingular+'\',\'Realmente desea desvincular el '+parametrosAPI_config.modLabelSingular+' de la acción?\',\'exclamacion\',\'danger\',\''+parametrosAPI_config.modName+'_desvincular_confirm(\\\''+$(el).data('id')+'\\\')\');"><i class="fa fa-chain-broken"></i></button>';

						//Activo o no?
						var obligatorio = parseInt($(el).data('obligatorio'));
						var boton_obligatorio = '<button class="btn btn-xs btn-' + ( obligatorio==1 ? 'danger' : 'success' ) + '" style="height: 24px; padding: 0 10px;"><b>' + ( obligatorio==1 ? 'Obligatorio' : 'Opcional' ) + '</b></button>';

						//Boton editar
						var boton_editar = '<button class="btn btn-xs btn-primary" style="margin: 0px 0px 8px 5px; width: 24px; height: 24px;" data-toggle="tooltip" title="Editar" onclick="var accion = $(\'#parametrosAPI_listar_API\').val_(); if(accion != null) '+parametrosAPI_config.modName+'_edita_agrega('+$(el).data('id')+',accion);"><i class="fa fa-pencil"></i></button>';

						//Boton filtros
						var boton_caracteres = '<button class="btn btn-xs btn-primary" style="margin: 0px 0px 8px 5px; width: 24px; height: 24px;" data-toggle="tooltip" title="Carateres" onclick="var accion = $(\'#parametrosAPI_listar_API\').val_(); if(accion != null) '+parametrosAPI_config.modName+'_filtros('+$(el).data('id')+',\''+$(el).data('label')+'\',accion);"><i class="fa fa-keyboard-o"></i></button>';

						//Boton filtros
						var boton_procesos = '<button class="btn btn-xs btn-primary" style="margin: 0px 0px 8px 5px; width: 24px; height: 24px;" data-toggle="tooltip" title="Procesos" onclick="'+parametrosAPI_config.modName+'_procesos('+$(el).data('id')+');"><i class="fa fa-cog"></i></button>';

						var html = '<b>' + capitalise($(el).data('label_articulo')) +' '+ $(el).data('label') + '</b><div class="descripcion"><small>Identificador: <b>'+$(el).data('identificador')+'</b></br>Articulo del label: <b>'+$(el).data('label_articulo')+'</b></br>Filtro: <b style="word-wrap: break-word;">'+$(el).data('filtro')+'</b>'

						if($(el).data('descripcion') != '')
							html += '<br/>Descripción: <b>'+$(el).data('descripcion')+'</b>';

						html += '</small></div><div class="pull-right descripcion" style="width: 180px; text-align: right; position: absolute; top: 8px; right: 10px;">';


						html += '<div>'+boton_procesos+boton_caracteres+boton_editar+boton_desvincular+boton_eliminar+boton_obligatorio+'</div></div>';

						//Agregamos los datos
						$(el).html(html);

					},
					funcion_desactivado: function(el){
						$(el).html('<b>' + capitalise($(el).data('label_articulo')) +' '+ $(el).data('label') + '</b>');
					}
				});
	
	//Evento onchange de select2
	apiselect2.on("change", function(e) {
		//Select2 val..
		var accion = $('#parametrosAPI_listar_API').val_();

		//Rescatamos el ID
		if(accion != null) accion = accion.id;

		//Llamada a la API
		var data = {};
		data['accion'] = accion;
		apix('parametrosAPI','listar',data, {
            ok: function(json){
				$('#parametrosAPI_lista').data('plugin_update')( $('#parametrosAPI_lista'), json.result);
			}
		});
	});
	
	//Populamos
	if(typeof id !== 'undefined') {
		$('#parametrosAPI_listar_API',contenido).val_({id: id, modulo_nombre: modulo, accion_nombre: accion});
		//Llamada a la API
		var data = {};
		data['accion'] = id;
		apix('parametrosAPI','listar',data, {
            ok: function(json){
				$('#parametrosAPI_lista').data('plugin_update')( $('#parametrosAPI_lista'), json.result);
			}
		});
	}

	//Agregamos los tooltips
	tabla.tooltip({selector: '[data-toggle="tooltip"]', placement: 'left', animation: false});

	//Generamos el listado
	$('#'+parametrosAPI_config.modName+'Modal_container').html('');
	//$('#'+parametrosAPI_config.modName+'Modal_container').append(tabla);
	$('#'+parametrosAPI_config.modName+'Modal_container').append(contenido, tabla);

	//Agregamos scrollbars
	$('#'+parametrosAPI_config.modName+'Modal_container .scrollable').mCustomScrollbar({theme: 'minimal-dark',scrollInertia: 100, mouseWheel:{ scrollAmount: 70 }});

	//Boton Agregar
	$('#'+parametrosAPI_config.modName+'Modal_footer').append('<button type="button" class="btn btn-success" style="display: block; width: 49%; margin: 0px 0px 10px 0px; float: left;" onclick="var accion = $(\'#parametrosAPI_listar_API\').val_(); console.log(accion); if(accion != null) '+parametrosAPI_config.modName+'_edita_agrega(undefined,accion);">'+parametrosAPI_config.nuevoLabel+' '+parametrosAPI_config.modLabelSingular+'</button>');

	//Boton Agregar
	$('#'+parametrosAPI_config.modName+'Modal_footer').append('<button type="button" class="btn btn-primary" style="display: block; width: 49%; margin: 0px 0px 10px 0px; float: right;" onclick="var accion = $(\'#parametrosAPI_listar_API\').val_(); console.log(accion); if(accion != null) '+parametrosAPI_config.modName+'_vincular(accion);">Vincular Parámetro</button>');

	//Append Cancel Button
	$('#'+parametrosAPI_config.modName+'Modal_footer').append('<button type="button" data-dismiss="modal" class="btn btn-warning" style="display: block; width: 100%; margin: 10px auto;">Cerrar</button>');
}

//Edita/Agrega
function parametrosAPI_edita_agrega(id,accion){

	//Si estamos agregando...
	if(typeof id === 'undefined') var agrega = true;
	else var agrega = false;

	//Titulo...
	if(agrega) $('#'+parametrosAPI_config.modName+'Modal_titulo').html('Nuevo Parametro ('+accion.modulo_nombre+' - '+accion.accion_nombre+')');
	else $('#'+parametrosAPI_config.modName+'Modal_titulo').html('Editar Parametro');

	//Cargando...
	$('#'+parametrosAPI_config.modName+'Modal_container').html(cargando());

	//Footer...
	$('#'+parametrosAPI_config.modName+'Modal_footer').html('');

	//Contenido
	var contenido = $('<div class="scrollable" style="max-height: '+parametrosAPI_config.max_height_edita_agrega+'px; overflow-y: hidden;"></div>');

	//Agregamos todos los campos
	for(var k in parametrosAPI_config.campos){
		var campo = parametrosAPI_config.campos[k];
		if( (campo.mostrarEditar && !agrega) || (campo.mostrarCrear && agrega)){
			//Agregamos el campo
			contenido.append( genera_campo(parametrosAPI_config.campos[k], parametrosAPI_config.modName ,true) );
		}
	}


	//Agregamos el contenido y los botones
	//si estamos creando un registro nuevo
	if(agrega){
		//Agregamos el contenido
		$('#'+parametrosAPI_config.modName+'Modal_container').html(contenido);

		//Agregamos scrollbars
		$('#'+parametrosAPI_config.modName+'Modal_container .scrollable').mCustomScrollbar({theme: 'minimal-dark',scrollInertia: 100, mouseWheel:{ scrollAmount: 70 }});

		//Agregamos los botones al footer
		$('#'+parametrosAPI_config.modName+'Modal_footer').append('<button type="button" class="btn btn-primary" style="display: block; width: 100%; margin: 10px auto;" onclick="'+parametrosAPI_config.modName+'_agrega_confirm('+accion.id+',\''+accion.modulo_nombre+'\',\''+accion.accion_nombre+'\');">Agregar '+parametrosAPI_config.modLabelSingular+'</button>');
		$('#'+parametrosAPI_config.modName+'Modal_footer').append('<button type="button" class="btn btn-warning" style="display: block; width: 100%; margin: 10px auto;" onclick="'+parametrosAPI_config.modName+'_listar('+accion.id+',\''+accion.modulo_nombre+'\',\''+accion.accion_nombre+'\');">Volver</button>');
	}


	//Si editamos
	if(!agrega){
		var data = {};
		data['parametro'] = id;

		apix(parametrosAPI_config.modName,'info',data, {
			ok: function(json){

				//Titulo
				$('#'+parametrosAPI_config.modName+'Modal_titulo').html('Editar Parametro ('+json.result.modulo_nombre+' - '+json.result.accion_nombre+')');

		    	//Llenamos los datos
		    	for(var k in parametrosAPI_config.campos){
		    		var campo = parametrosAPI_config.campos[k];
			    	$('#'+parametrosAPI_config.modName+'_'+campo.id,contenido).val_(json.result[ campo.id ]);
		    	}

		    	//Asignamos el contenido al popup
				$('#'+parametrosAPI_config.modName+'Modal_container').html(contenido);

				//Agregamos scrollbars
				$('#'+parametrosAPI_config.modName+'Modal_container .scrollable').mCustomScrollbar({theme: 'minimal-dark',scrollInertia: 100, mouseWheel:{ scrollAmount: 70 }});

				//Agregamos los botones al footer
				$('#'+parametrosAPI_config.modName+'Modal_footer').append('<button type="button" class="btn btn-primary" style="display: block; width: 100%; margin: 10px auto;" onclick="'+parametrosAPI_config.modName+'_edita_confirm('+id+','+accion.id+',\''+accion.modulo_nombre+'\',\''+accion.accion_nombre+'\');">Editar '+parametrosAPI_config.modLabelSingular+'</button>');
					$('#'+parametrosAPI_config.modName+'Modal_footer').append('<button type="button" class="btn btn-warning" style="display: block; width: 100%; margin: 10px auto;" onclick="'+parametrosAPI_config.modName+'_listar('+accion.id+',\''+accion.modulo_nombre+'\',\''+accion.accion_nombre+'\');">Volver</button>');
			}
		});
	}
}

//Edita
function parametrosAPI_edita_confirm(id,accion,modulo_nombre,accion_nombre){

	//Data
	var data = {};
	data['parametro'] = id;

	//Agregamos todos los campos
	for(var k in parametrosAPI_config.campos){
		var campo = parametrosAPI_config.campos[k];
		if(campo.mostrarEditar)
			data[campo.id] = $('#'+parametrosAPI_config.modName+'_'+campo.id).val();
	}

	apix(parametrosAPI_config.modName,'editar',data, {
		ok: function(json){
			notifica(	parametrosAPI_config.modLabelSingular+' '+parametrosAPI_config.editaLabel.toLowerCase(),
						parametrosAPI_config.artLabel+' '+parametrosAPI_config.modLabelSingular+' se editó con éxito',
						'confirmacion','success');
			parametrosAPI_listar(accion,modulo_nombre,accion_nombre);
		}
	});
}

//Agrega
function parametrosAPI_agrega_confirm(accion,modulo_nombre,accion_nombre){

	//Data
	var data = {};
	data['accion'] = accion;

	//Agregamos todos los campos
	for(var k in parametrosAPI_config.campos){
		var campo = parametrosAPI_config.campos[k];
		if(campo.mostrarCrear)
			data[campo.id] = $('#'+parametrosAPI_config.modName+'_'+campo.id).val();
	}

	apix(parametrosAPI_config.modName,'agregar',data, {
		ok: function(json){
			//Notifica
			notifica(	parametrosAPI_config.modLabelSingular+' '+parametrosAPI_config.creaLabel.toLowerCase(),
						parametrosAPI_config.artLabel+' '+parametrosAPI_config.modLabelSingular+' se creó con éxito',
						'confirmacion',
						'success'
					);
			//Update
			parametrosAPI_listar(accion,modulo_nombre,accion_nombre);
		}
	});
}

//Elimina
function parametrosAPI_elimina(id){
	
	//Tomamos el objeto accion del titulo
	var accion = $('#parametrosAPI_listar_API').val_();

	//Data array
	var data = {};
	data['parametro'] = id;
	
	apix(parametrosAPI_config.modName,'eliminar',data, {
		ok: function(json){
	    	//Notifica
	    	notifica(	parametrosAPI_config.modLabelSingular+' '+parametrosAPI_config.eliminaLabel,
	    				parametrosAPI_config.artLabel+' '+parametrosAPI_config.modLabelSingular+' se eliminó con éxito',
	    				'informacion',
	    				'info'
	    			);
	    	//Update
	    	parametrosAPI_listar(accion.id,accion.modulo_nombre,accion.accion_nombre);
		}
	});
}

//Edita/Agrega Filtros
function parametrosAPI_filtros(id, label, accion){

	//Titulo...
	$('#'+parametrosAPI_config.modName+'Modal_titulo').html('Cagando...');

	//Cargando...
	$('#'+parametrosAPI_config.modName+'Modal_container').html(cargando());

	//Footer...
	$('#'+parametrosAPI_config.modName+'Modal_footer').html('');

	//Contenido
	var contenido = $('<div class="scrollable" style="max-height: '+parametrosAPI_config.max_height_edita_agrega+'px; overflow-y: hidden;"></div>');

	//Agregamos todos los campos
	for(var k in parametrosAPI_config.campos_caracteres){
		var campo = parametrosAPI_config.campos_caracteres[k];
		//Generamos campo
		var campoGenerado = genera_campo(parametrosAPI_config.campos_caracteres[k], parametrosAPI_config.modName ,true);
		
		//Editamos el estilo
		if( campo.tipo == 'checkbox')
			if(k % 2) campoGenerado.attr('style','margin: 0px 0px 10px 10px; width: 47% !important; float: right;');
			else campoGenerado.attr('style','margin: 0px 10px 10px 0px; width: 47% !important; float: left;');
		

		//Agregamos el campo
		contenido.append( campoGenerado );
	}

	var data = {};
	data['parametro'] = id;

	apix(parametrosAPI_config.modName,'info',data, {
		ok: function(json){

			//Titulo...
			$('#'+parametrosAPI_config.modName+'Modal_titulo').html('Editar caracteres permitidos de '+label);

	    	//Llenamos los datos
	    	for(var k in parametrosAPI_config.campos_caracteres){
	    		var campo = parametrosAPI_config.campos_caracteres[k];
		    	$('#'+parametrosAPI_config.modName+'_'+campo.id,contenido).val_(json.result[ campo.id ]);
	    	}

			//Agregamos el contenido
			$('#'+parametrosAPI_config.modName+'Modal_container').html(contenido);

			//Agregamos scrollbars
			$('#'+parametrosAPI_config.modName+'Modal_container .scrollable').mCustomScrollbar({theme: 'minimal-dark',scrollInertia: 100, mouseWheel:{ scrollAmount: 70 }});

			//Agregamos los botones al footer
			$('#'+parametrosAPI_config.modName+'Modal_footer').append('<button type="button" class="btn btn-primary" style="display: block; width: 100%; margin: 10px auto;" onclick="'+parametrosAPI_config.modName+'_filtros_commit('+id+','+accion.id+',\''+accion.modulo_nombre+'\',\''+accion.accion_nombre+'\');">Guardar Filtros</button>');
			$('#'+parametrosAPI_config.modName+'Modal_footer').append('<button type="button" class="btn btn-warning" style="display: block; width: 100%; margin: 10px auto;" onclick="'+parametrosAPI_config.modName+'_listar('+accion.id+',\''+accion.modulo_nombre+'\',\''+accion.accion_nombre+'\');">Volver</button>');
		}
	});
}

//Edita/Agrega Filtros
function parametrosAPI_filtros_commit(id,accion,modulo_nombre,accion_nombre){

	//Data
	var data = {};
	data['parametro'] = id;

	//Agregamos todos los campos
	for(var k in parametrosAPI_config.campos_caracteres){
		var campo = parametrosAPI_config.campos_caracteres[k];
		data[campo.id] = $('#'+parametrosAPI_config.modName+'_'+campo.id).val();
	}

	apix(parametrosAPI_config.modName,'editar_caracteres',data, {
		ok: function(json){
			//Notifica
			notifica(	'Caracteres permitidos Editados',
						'Los caracteres permitidos fueron editados con éxito',
						'confirmacion',
						'success'
					);
			//Update
			parametrosAPI_listar(accion,modulo_nombre,accion_nombre);
		}
	});
}

//Edita/Agrega Filtros
function parametrosAPI_vincular(accion){

	//Titulo...
	$('#'+parametrosAPI_config.modName+'Modal_titulo').html('Vincular Parámetro existente a <br/> <small> Modulo: <b>'+accion.modulo_nombre+'</b> | Acción: <b>'+accion.accion_nombre+'</b></small>');

	//Parche
	$('#parametrosAPIModal_titulo').data('accion',accion);

	//Cargando...
	$('#'+parametrosAPI_config.modName+'Modal_container').html( cargando() );

	//Footer...
	$('#'+parametrosAPI_config.modName+'Modal_footer').html('');

	//Traemos los datos de la API
	apix(parametrosAPI_config.modName,'listar_parametros',{}, {
		ok: function(json){

			var tabla = generaTabla({
							id_tabla: parametrosAPI_config.modName,
							items: json.result,
							keys_busqueda: ['label','descripcion'],
							key_nombre: null,
							max_height: parametrosAPI_config.max_height_lista,
							funcion_activado: function(el){

								//Seleccionamos el activo anterior
								var elActivo = $('#'+parametrosAPI_config.modName+'_lista').find('.active_');
								var id = $(el).data('id');

								//Obtenemos los permisos
								var acciones = $(el).data('api');

								var boton_vincular = '<button class="btn btn-xs btn-success" style="height: 24px; padding: 0 10px;" onclick="parametrosAPI_vincular_confirm($(this).closest(\'a\').data(\'id\'));"><b>Vincular</b></button>';
								
								//Mostramos los acciones o 'ningun acceso'
								if(objectLength(acciones)) var api = $('<ul class="fa-ul" style="margin-left: 50px;"/>');
								else var api = $('<span style="display: block;">(No esta vinculado con Acciones de la API)</span>');

								
								for(var k in acciones)
									api.append('<li><i class="fa-li fa fa-cog"></i>'+acciones[k]+'</li>');

								var html = '<b>'+ $(el).data('label') + '</b>';
								
								//Descripcion
								if($(el).data('descripcion') != '') html += '<br/><h5 style="width: 500px;">'+ $(el).data('descripcion') + '</h5>';

								//Identificador
								html += '<br/><small><i class="fa fa-key" style="width: 20px; text-align: center;"></i><strong>Identificador: </strong>' + $(el).data('identificador');

								//Filtro
								if($(el).data('filtro') != '') html += '<br/><i class="fa fa-filter" style="width: 20px; text-align: center;"></i><strong>Filtro: </strong>' + $(el).data('filtro');

								//Acciones vinculadas
								if(objectLength(acciones)) html += '<div style="margin-top: 10px;"><i class="fa fa-link" style="width: 20px; text-align: center;"></i><strong>ACCIONES QUE HACEN USO: </strong>'+api.outerHTML()+'</div>'

								//Boton vincular
								html += '<div class="pull-right descripcion" style="width: 120px; text-align: right; position: absolute; top: 8px; right: 10px;">'+boton_vincular+'</div>';

								//Agregamos los datos
								$(el).html(html);

							},
							funcion_desactivado: function(el){
								$(el).html('<b>'+ $(el).data('label') + '</b>');
							}
						});

			//Agregamos los tooltips
			tabla.tooltip({selector: '[data-toggle="tooltip"]', placement: 'left', animation: false});

			//Generamos el listado
			$('#'+parametrosAPI_config.modName+'Modal_container').html('');
			$('#'+parametrosAPI_config.modName+'Modal_container').append(tabla);

			//Agregamos scrollbars
			$('#'+parametrosAPI_config.modName+'Modal_container .scrollable').mCustomScrollbar({theme: 'minimal-dark',scrollInertia: 100, mouseWheel:{ scrollAmount: 70 }});

			$('#'+parametrosAPI_config.modName+'Modal_footer').append('<button type="button" class="btn btn-warning" style="display: block; width: 100%; margin: 10px auto;" onclick="'+parametrosAPI_config.modName+'_listar('+accion.id+',\''+accion.modulo_nombre+'\',\''+accion.accion_nombre+'\');">Volver</button>');
		}
	});
}

//Confirma vinculación
function parametrosAPI_vincular_confirm(id){

	//Tomamos el objeto accion del titulo
	var accion = $('#parametrosAPIModal_titulo').data('accion');
	$('#parametrosAPIModal_titulo').data('accion',undefined);

	//Data
	var data = {};
	data['accion'] = accion.id;
	data['parametro'] = id;

	apix(parametrosAPI_config.modName,'vincular_parametro',data, {
		ok: function(json){
			//Notifica
			notifica(	'Parametro Vinculado',
						'El Parámetro se vinculó de la acción <br/><small><b>'+accion.modulo_nombre+'<br/>'+accion.accion_nombre+'</b></small>',
						'confirmacion',
						'success'
					);
			//Update
			parametrosAPI_listar(accion.id,accion.modulo_nombre,accion.accion_nombre);
		}
	});
}

//Confirma desvinculación
function parametrosAPI_desvincular_confirm(id){

	//Tomamos el objeto accion del titulo
	var accion = $('#parametrosAPI_listar_API').val_();

	//Data
	var data = {};
	data['accion'] = accion.id;
	data['parametro'] = id;

	apix(parametrosAPI_config.modName,'desvincular_parametro',data, {
		ok: function(json){
			//Notifica
			notifica(	'Parametro Desvinculado',
						'El Parámetro se desvinculó de la acción <br/><small><b>'+accion.modulo_nombre+' <br/> '+accion.accion_nombre+'</b></small>',
						'confirmacion',
						'success'
					);
			//Update
			parametrosAPI_listar(accion.id,accion.modulo_nombre,accion.accion_nombre);
		}
	});
}

//Procesos que aplican
function parametrosAPI_procesos(id){

	//Creamos el popup
	abrir_modal(parametrosAPI_config.modName, 550, false);

	//Tomamos el objeto accion del titulo
	var accion = $('#parametrosAPI_listar_API').val_();

	//Titulo...
	$('#'+parametrosAPI_config.modName+'Modal_titulo').html('Cargando...');

	//Cargando...
	$('#'+parametrosAPI_config.modName+'Modal_container').html(cargando());

	//Footer...
	$('#'+parametrosAPI_config.modName+'Modal_footer').html('');

	//Contenido
	var contenido = $('<div class="scrollable" style="max-height: '+parametrosAPI_config.max_height_edita_agrega+'px; overflow-y: hidden;"></div>');

	//Agregamos todos los campos
	for(var k in parametrosAPI_config.campos_procesos){

		var campo = parametrosAPI_config.campos_procesos[k];
		//Generamos campo
		var campoGenerado = genera_campo(parametrosAPI_config.campos_procesos[k], parametrosAPI_config.modName ,true);

		//Editamos el estilo
		/*if( (campo.tipo == 'checkbox') && 
			(campo.id != 'validadores_errorcaracteresespecificosnopermitidos') && 
			(campo.id != 'validadores_mostrarerrorregexp') )
			if(k % 2) campoGenerado.attr('style','margin: 0px 0px 10px 5px; width: 49% !important; float: right;');
			else campoGenerado.attr('style','margin: 0px 5px 10px 0px; width: 49% !important; float: left;');*/

		if(typeof campo.style !== 'undefined')
			campoGenerado.attr('style',campo.style);

		var campoEnContenedor = $('<div></div>').append(campoGenerado);

		//Si tiene un estilo especial
		if( (typeof campo.style !== 'undefined') && (campo.tipo == 'input'))
			$('span',campoEnContenedor).attr("style","width: 100%;float: left;");
				

		//Agregamos el campo
		contenido.append( campoEnContenedor );
	}

	var data = {};
	data['parametro'] = id;

	apix(parametrosAPI_config.modName,'info',data, {
		ok: function(json){

			//Titulo
			$('#'+parametrosAPI_config.modName+'Modal_titulo').html('Editar '+parametrosAPI_config.modLabelSingular);

	    	//Llenamos los datos
	    	for(var k in parametrosAPI_config.campos_procesos){
	    		var campo = parametrosAPI_config.campos_procesos[k];
		    	$('#'+parametrosAPI_config.modName+'_'+campo.id,contenido).val_(json.result[ campo.id ]);
	    	}

			//Agregamos el contenido
			$('#'+parametrosAPI_config.modName+'Modal_container').html(contenido);

			//Agregamos scrollbars
			$('#'+parametrosAPI_config.modName+'Modal_container .scrollable').mCustomScrollbar({theme: 'minimal-dark',scrollInertia: 100, mouseWheel:{ scrollAmount: 70 }});

			//Agregamos los botones al footer
			$('#'+parametrosAPI_config.modName+'Modal_footer').append('<button type="button" class="btn btn-primary" style="display: block; width: 100%; margin: 10px auto;" onclick="'+parametrosAPI_config.modName+'_procesos_commit('+id+','+accion.id+',\''+accion.modulo_nombre+'\',\''+accion.accion_nombre+'\');">Guardar Procesos</button>');
			$('#'+parametrosAPI_config.modName+'Modal_footer').append('<button type="button" class="btn btn-warning" style="display: block; width: 100%; margin: 10px auto;" onclick="'+parametrosAPI_config.modName+'_listar('+accion.id+',\''+accion.modulo_nombre+'\',\''+accion.accion_nombre+'\');">Volver</button>');
		}
	});
}

function parametrosAPI_procesos_commit(id,accion,modulo_nombre,accion_nombre){

	//Data
	var data = {};
	data['parametro'] = id;

	//Agregamos todos los campos
	for(var k in parametrosAPI_config.campos_procesos){
		var campo = parametrosAPI_config.campos_procesos[k];
			data[campo.id] = $('#'+parametrosAPI_config.modName+'_'+campo.id).val();
	}

	apix(parametrosAPI_config.modName,'editar_procesos',data, {
		ok: function(json){
			//Notifica
			notifica(	parametrosAPI_config.modLabelSingular+' '+parametrosAPI_config.creaLabel.toLowerCase(),
						parametrosAPI_config.artLabel+' '+parametrosAPI_config.modLabelSingular+' se creó con éxito',
						'confirmacion',
						'success'
					);
			//Update
			parametrosAPI_listar(accion,modulo_nombre,accion_nombre);
		}
	});
}
</script>