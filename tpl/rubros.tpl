<script type="text/javascript">

{if="$GLOBALS['permisos']['rubros_administrar']"}

//Config Usuarios
var rubros_config = {
	modName: 					'rubros',
	modLabelSingular: 			'Rubro',
	modLabelPlural: 			'Rubros',
	creaLabel: 					'Agregado',
	editaLabel: 				'Editado',
	eliminaLabel: 				'Eliminado',
	nuevoLabel: 				'Nuevo',
	artLabel: 					'El',
	max_height_edita_agrega: 	'300',
	max_height_lista: 			'300',
	data: 						{}, 
	campos:[
		{	
			id: 			"click",
			label: 			"OnCLick JS (accion al hacer click al item)",
			tipo: 			"textarea",
			mostrarEditar: 	true,
			mostrarCrear: 	true
		},
		{	
			id: 			"permiso",
			label: 			"Permiso PHP (retorna boolean para saber permiso)",
			tipo: 			"textarea",
			mostrarEditar: 	true,
			mostrarCrear: 	true
		}
	]
};

//Lista
var rubros_listar_busqueda = false;
function rubros_listar(){

	//Creamos el popup
	abrir_modal(rubros_config.modName, 650, false);

	//Mostramos la ventana
	$('#'+rubros_config.modName+'Modal').modal({show:true});

	//Titulo...
	$('#'+rubros_config.modName+'Modal_titulo').html(rubros_config.modLabelPlural);

	//Cargando...
	$('#'+rubros_config.modName+'Modal_container').html( cargando() );

	//Footer...
	$('#'+rubros_config.modName+'Modal_footer').html('');

	//Traemos los datos de la API
	apix(rubros_config.modName,'listar',{}, {
		ok: function(json){

			var html = '<div style="margin: 10px;"> <button type="button" class="btn btn-success btn-sm" onclick="'+rubros_config.modName+'_listar_crear();"><i class="glyphicon glyphicon-asterisk"></i> Crear</button> <button type="button" class="btn btn-warning btn-sm" onclick="'+rubros_config.modName+'_listar_renombrar();"><i class="glyphicon glyphicon-pencil"></i> Renombrar</button> <button type="button" class="btn btn-danger btn-sm" onclick=" confirma(\'Eliminar Rubro\',\'Si usted elimina el Rubro, todas las dimensiones y rubros dependientes se perderán de forma permanente.\',\'exclamacion\',\'danger\',\'rubros_listar_eliminar();\')" ><i class="glyphicon glyphicon-remove"></i> Eliminar</button> <input type="text" value="" style="float: right; width: 200px; padding: 6px; border-radius: 3px; border: 1px solid silver; font-size: 12px;" id="'+rubros_config.modName+'_listar_buscar" placeholder="Buscar" /> </div> <div id="rubros_tree" class="scrollable demo" style="max-width: 100%; overflow: auto; font: 12px Verdana, sans-serif; box-shadow: 0 0 5px #ccc; margin: 10px; border-radius: 3px; min-height: 300px; max-height: 300px;"></div>';


			//Resultados
			rubros_config.data = json.result;
			

			//Generamos el listado
			$('#'+rubros_config.modName+'Modal_container').html('');
			$('#'+rubros_config.modName+'Modal_container').append(html);


			$('#rubros_tree').jstree({
			  "core" : {
			    "animation" : 1,
			    "multiple" : false,
			    "check_callback" : true,
				"themes" : {
				      "dots" : false // no connecting dots between dots
				 },
		        "data" : function (obj, cb) {
		            cb.call(this, rubros_config.data);
		        }
			  },
			  "ui": {
                "select_multiple_modifier": "on"
			  },
			  "state" : { "key" : "rubros" },
			  "types" : {
				"0" : {
					"icon":"glyphicon glyphicon-flash"
				},
				"1" : {
				}
			  },
			  "sort": function (a, b){ return this.get_text(a).toLowerCase() > this.get_text(b).toLowerCase() ? 1 : -1;},
			  "plugins" : [ "types", "state", "wholerow", "sort", "search", "json_data","ui" ],
			  "search": { "show_only_matches" : true}
			  

			}).on('rename_node.jstree', function (e,obj) {

				if(obj.node.original.type == 'new'){
					//Llamada a la API
					var data = {};
					data['parent'] = obj.node.parent;
					data['nombre'] = obj.text;
					apix('rubros','crear',data, {
						ok: function(json){
					    	//Notifica
					    	notifica(	'Item Creado',
					    				'El Item se creó con éxito',
					    				'informacion',
					    				'info'
					    			);
				    		//Update
							rubros_listar_update();
						}
					});
				}else{
					//Llamada a la API
					var data = {};
					data['rubro'] = obj.node.id;
					data['nombre'] = obj.text;
					apix('rubros','renombrar',data, {
            			ok: function(json){
					    	//Notifica
					    	notifica(	'Rubro editado',
					    				'El Rubro se editó con éxito',
					    				'informacion',
					    				'info'
					    			);
					    	//Update
							rubros_listar_update();
						}
					});
				}
			}).on('delete_node.jstree', function(e,obj){
				//Llamada a la API
				var data = {};
				data['rubro'] = obj.node.id;
				apix('rubros','eliminar',data, {
            		ok: function(json){
				    	//Notifica
				    	notifica(	'Item eliminado',
				    				'El Item se eliminó con éxito',
				    				'informacion',
				    				'info'
				    			);
				    	//Update
						rubros_listar_update();
					}
				});
			}).on('select_node.jstree', function(e, selected){

				//Quitamos seleccionados anteriores
				$('#rubros_tree .jstree-wholerow').not('#'+selected.node.a_attr.id).html('');

				//Seleccionamos el contenedor
				var contenedor = $('#'+selected.node.a_attr.id).siblings('div');

				var item = selected.node.original;

				//Mostramos nombre singular y propiedades
				var html = '<div class="jstree_node_selected"><a href="#" rel="tooltip" data-html="true" data-placement="left" title="<small><b>Nombre singular</b><br>'+item.nombre_singular+'</small>"><i class="fa fa-tag"></i></a>';

				if(typeof item.dimensiones != 'undefined'){
					html += '<a href="#" rel="tooltip" data-html="true" data-placement="left" title="<ul style=&#34;padding: 0px 0px 0px 5px; text-align: left; list-style: none;&#34;>';
					//Agregamos dimensiones
					for(var i in item.dimensiones){
						if(item.dimensiones[i].propia)
							html += '<li>'+item.dimensiones[i].nombre+'</li>';
						else
							html += '<li><i class=&#34;fa fa-sitemap&#34;></i>&nbsp;&nbsp;'+item.dimensiones[i].nombre+'</li>';
					} 
					html += '</ul>"><i class="fa fa-bars"></i></a></div>';
				}

				//Agregamos fin
				html += '</div>';

				//Agregamos contenido
				contenedor.html('').append(html);

				//tooltip
				contenedor.tooltip({selector: 'a[rel=tooltip]'});

				//Guardamos el seleccionado en el Data
				$('#rubros_tree').data('selected', selected.node.original );

			});

			//Buscador
	


			$('#rubros_listar_buscar').keyup(function () {
				if(rubros_listar_busqueda) { clearTimeout(rubros_listar_busqueda); }
				rubros_listar_busqueda = setTimeout(function () {
				  var v = $('#rubros_listar_buscar').val();
				  $('#rubros_tree').jstree(true).search(v);
				}, 250);
			});

			//Boton Nombre silgular
			$('#'+rubros_config.modName+'Modal_footer').append('<button type="button" class="btn btn-primary" style="display: block; width: 30%; margin: 0px 0px 10px 0px; float: left;" onclick="rubro_edita_nombre_singular();">Nombre Singular</button>');

			//Boton Nombres Alternativos
			$('#'+rubros_config.modName+'Modal_footer').append('<button type="button" class="btn btn-primary" style="display: block; width: 30%; margin: 0px 0px 0px 30px; float: left;" onclick="rubro_edita_nombres_alternativos();">Nombres Alternativos</button>');

			//Boton Agregar
			$('#'+rubros_config.modName+'Modal_footer').append('<button type="button" class="btn btn-primary" style="display: block; width: 30%; margin: 0px 0px 10px 0px; float: right;" onclick="rubro_dimensiones_listar($(\'#rubros_tree\').data(\'jstree\').get_selected()[0])">Dimensiones</button>');

			//Append Cancel Button
			$('#'+rubros_config.modName+'Modal_footer').append('<button type="button" data-dismiss="modal" class="btn btn-warning" style="display: block; width: 100%; margin: 10px auto;">Cerrar</button>');
		}
	});
}

//Actualiza Tree
function rubros_listar_update(){
	apix('rubros','listar',{},{
		ok: function(json){
			rubros_config.data = json.result;
			$('#rubros_tree').jstree("refresh");
		}
	});
}

//Crea Item
function rubros_listar_crear() {
	var ref = $('#'+rubros_config.modName+'_tree').jstree(true),
		sel = ref.get_selected();
	if(!sel.length) { return false; }
	sel = sel[0];
	sel = ref.create_node(sel, {"type":"new"});
	if(sel) {
		ref.edit(sel);
	}
}

//Renombra Item
function rubros_listar_renombrar() {
	var ref = $('#'+rubros_config.modName+'_tree').jstree(true),
		sel = ref.get_selected();
	var data = ref.settings.core.data;

	//Si no hay seleccionado ningun elemento...
	if(!sel.length) { return false; }
	//Si hay elemento seleccionado traemos el primero
	sel = sel[0];
	ref.edit(sel);
}

//Elimina un Item
function rubros_listar_eliminar(){
	var ref = $('#'+rubros_config.modName+'_tree').jstree(true),
		sel = ref.get_selected();
	if(!sel.length) { return false; }
	ref.delete_node(sel,'Nuevo item');
}

//Configuración del item
function rubro_dimensiones_listar(id){

	//Obtenemos la info del rubro seleccionado
	if(typeof id == 'undefined'){
		var rubro = $('#rubros_tree').data('selected');
		id = rubro.id;
	}

	//Titulo...
	$('#'+rubros_config.modName+'Modal_titulo').html('Editar Propiedades</br><small></small>');

	//Cargando...
	$('#'+rubros_config.modName+'Modal_container').html(cargando());

	//Footer...
	$('#'+rubros_config.modName+'Modal_footer').html('');

	var contenido = $('<div class="scrollable" style="max-height: '+rubros_config.max_height_edita_agrega+'px; overflow-y: hidden;"></div>');

	var data = {};
	data['rubro'] = id;

	apix('dimensiones','listar_x_rubro',data, {
		ok: function(json){

	    	var cant = json.result.length;
	    	var heredados = 0;

	    	//Calculamos Heredados
	    	for(var i = 0 in cant)
	    		if(json.result[i].heredado == 1)
	    			heredados++;



			var tabla = generaTabla({
							id_tabla: 'rubros_propiedades',
							items: json.result,
							keys_busqueda: ['id','label'],
							key_nombre: null,
							max_height: 300,
							funcion_activado: function(el){

								//Seleccionamos el activo anterior
								//var elActivo = $('#'+clientes_config.modName+'_lista').find('.active_');
								//var id = $(el).data('id');
								var list = $(el).parent();
								var index = $(el).index();


								//Si es Heredada la marcamos como heredada
								var html = $(el).data('label');
								
								//Agregamos tag Heredado
								if($(el).data('heredado') == '1'){
									html += '<span class="badge"><small>Heredado</small></span>';
								
								//Si no es heredado
								}else{

									html = '<b>' + html + '</b><div class="pull-right descripcion" style="width: 150px; text-align: right; position: absolute; top: 8px; right: 10px;">';

									//Boton Editar
									html += '<button class="btn btn-xs btn-success" style="margin: 0px 0px 8px 5px; width: 24px; height: 24px;" data-toggle="tooltip" title="Editar" onclick="rubro_dimensiones_editar_agregar(\''+$(el).data('id')+'\','+id+');"><i class="fa fa-pencil"></i></button>';

									//Boton Opciones
									html += '<button class="btn btn-xs btn-primary" style="margin: 0px 0px 8px 5px; width: 24px; height: 24px;" data-toggle="tooltip" title="Opciones" onclick="_edita_agrega(\''+$(el).data('id')+'\');"><i class="fa fa-bars"></i></button>';

									//Boton Subir
									if(heredados+1 < index)
									html += '<button class="btn btn-xs btn-primary" style="margin: 0px 0px 8px 5px; width: 24px; height: 24px;" data-toggle="tooltip" title="Subir un lugar" onclick="rubro_dimensiones_subir_posicion(\''+$(el).data('id')+'\','+id+');"><i class="fa fa-arrow-up"></i></button>';

									//Boton Bajar
									if(cant > index)
									html += '<button class="btn btn-xs btn-primary" style="margin: 0px 0px 8px 5px; width: 24px; height: 24px;" data-toggle="tooltip" title="Bajar un lugar" onclick="rubro_dimensiones_bajar_posicion(\''+$(el).data('id')+'\','+id+');"><i class="fa fa-arrow-down"></i></button>';

									//Boton Borrar
									html += '<button class="btn btn-xs btn-warning" style="margin: 0px 0px 8px 5px; width: 24px; height: 24px;" data-toggle="tooltip" title="Borrar" onclick=" confirma(\'Eliminar Dimensión\',\'La dimensión se eliminará de forma permanente.\',\'exclamacion\',\'danger\',\'rubro_dimensiones_eliminar('+$(el).data('id')+',' + id + ')\');"><i class="fa fa-remove"></i></button>';

									
								}

								//Agergamos el html
								$(el).html(html);
							},
							funcion_desactivado: function(el){
								var html = $(el).data('label');
								
								//Label
								if($(el).data('heredado') == '1')
									html += '<span class="badge"><small>Heredado</small></span>';
								
								//Agergamos el html
								$(el).html(html);
							}
						});


			//Agregamos los tooltips
			tabla.tooltip({selector: '[data-toggle="tooltip"]', placement: 'left', animation: false});

			//Generamos el listado
			$('#'+rubros_config.modName+'Modal_container').html('');
			$('#'+rubros_config.modName+'Modal_container').append(tabla);

			//Agregamos scrollbars
			$('#'+rubros_config.modName+'Modal_container .scrollable').mCustomScrollbar({theme: 'minimal-dark',scrollInertia: 100, mouseWheel:{ scrollAmount: 70 }});

			//Agregamos los botones al footer
			$('#'+rubros_config.modName+'Modal_footer').append('<button type="button" class="btn btn-primary" style="display: block; width: 100%; margin: 10px auto;" onclick="rubro_dimensiones_editar_agregar(undefined,'+id+');">Agregar Propiedad</button>');
			$('#'+rubros_config.modName+'Modal_footer').append('<button type="button" class="btn btn-warning" style="display: block; width: 100%; margin: 10px auto;" onclick="'+rubros_config.modLabelPlural.toLowerCase()+'_listar();">Volver</button>');
		}
	});
}

//Subir posicion de dimension
function rubro_dimensiones_subir_posicion(dimension,rubro){
	//Data
	var data = {};
	data['dimension'] = dimension;

	//DB confirm
	apix('dimensiones','subir_posicion',data, {
		ok: function(json){
			rubro_dimensiones_listar(rubro);
		}
	});
}

//Bajar posicion de dimension
function rubro_dimensiones_bajar_posicion(dimension,rubro){
	//Data
	var data = {};
	data['dimension'] = dimension;

	//DB confirm
	apix('dimensiones','bajar_posicion',data, {
		ok: function(json){
			rubro_dimensiones_listar(rubro);
		}
	});
}

//Eliminar dimension
function rubro_dimensiones_eliminar(dimension,rubro){
	//Data
	var data = {};
	data['dimension'] = dimension;

	//DB confirm
	apix('dimensiones','eliminar_dimension',data, {
		ok: function(json){
			notifica('Orden Editado','La dimensión se eliminó con éxito','confirmacion','success');
			rubro_dimensiones_listar(rubro);
		}
	});
}

//Editar dimension
function rubro_dimensiones_editar_agregar(dimension,rubro){

	//Si estamos agregando...
	if(typeof dimension === 'undefined') var agrega = true;
	else var agrega = false;

	//Titulo...
	if(agrega) $('#'+rubros_config.modName+'Modal_titulo').html('Agregar Dimensión');
	else $('#'+rubros_config.modName+'Modal_titulo').html('Editar Dimensión');

	//Cargando...
	$('#'+rubros_config.modName+'Modal_container').html(cargando());

	//Footer...
	$('#'+rubros_config.modName+'Modal_footer').html('');

	var contenido = $('<div class="scrollable" style="max-height: '+rubros_config.max_height_edita_agrega+'px; overflow-y: hidden;"></div>');

	//Agregamos campo nombre
	contenido.append( genera_campo({	
			id: 			"nombre",
			label: 			"Nombre",
			tipo: 			"input",
			mostrarEditar: 	true,
			mostrarCrear: 	true
	}, 'rubros_dimensiones' ,true) );

	//Agregamos campo nombre
	contenido.append( genera_campo({	
			id: 			"hereda",
			label: 			"Activo",
			tipo: 			"checkbox",
			mostrarEditar: 	true,
			mostrarCrear: 	true,
			config: 		{
							claseActivo: 'btn-success',
							claseInactivo: 'btn-danger',
							valueActivo: '1',
							valueInactivo: '0',
							valueDefault: '1',
							labelActivo: function(el){
								el.html('Los Rubros Hijos heredan esta Propiedad');
							},
							labelInactivo: function(el){
								el.html('Los Rubros Hijos NO heredan esta Propiedad');
							}
			}
	}, 'rubros_dimensiones' ,true) );

	//Agregamos el contenido y los botones
	//si estamos creando un registro nuevo
	if(agrega){
		//Agregamos el contenido
		$('#'+rubros_config.modName+'Modal_container').html(contenido);

		//Agregamos scrollbars
		$('#'+rubros_config.modName+'Modal_container .scrollable').mCustomScrollbar({theme: 'minimal-dark',scrollInertia: 100, mouseWheel:{ scrollAmount: 70 }});

		//Agregamos los botones al footer
		$('#'+rubros_config.modName+'Modal_footer').append('<button type="button" class="btn btn-primary" style="display: block; width: 100%; margin: 10px auto;" onclick="rubro_dimensiones_agregar_confirm('+rubro+');">Crear Propiedad</button>');
		$('#'+rubros_config.modName+'Modal_footer').append('<button type="button" class="btn btn-warning" style="display: block; width: 100%; margin: 10px auto;" onclick="'+rubros_config.modLabelPlural.toLowerCase()+'_listar();">Volver</button>');
	}

	//Si editamos
	if(!agrega){
		var data = {};
		data['dimension'] = dimension;

		apix('dimensiones','info',data, {
			ok: function(json){

				//Titulo
				$('#'+rubros_config.modName+'Modal_titulo').html('Editar Propiedad <b>'+json.result.nombre+'</b></br><small>Rubro: <b>'+json.result.rubro_nombre+'</b></small>');

		    	//Llenamos los datos
		    	$('#rubros_dimensiones_nombre',contenido).val_(json.result.nombre);
		    	
		    	var cede_decendencia = 0;
		    	if(json.result.cede_decendencia == 'Y')
		    		cede_decendencia = 1;
		    	$('#rubros_dimensiones_activo',contenido).val_(cede_decendencia);

		    	//Asignamos el contenido al popup
				$('#'+rubros_config.modName+'Modal_container').html(contenido);

				//Agregamos scrollbars
				$('#'+rubros_config.modName+'Modal_container .scrollable').mCustomScrollbar({theme: 'minimal-dark',scrollInertia: 100, mouseWheel:{ scrollAmount: 70 }});

				//Agregamos los botones al footer
				$('#'+rubros_config.modName+'Modal_footer').append('<button type="button" class="btn btn-primary" style="display: block; width: 100%; margin: 10px auto;" onclick="rubro_dimensiones_editar_confirm('+dimension+','+rubro+');">Editar Propiedad</button>');
				$('#'+rubros_config.modName+'Modal_footer').append('<button type="button" class="btn btn-warning" style="display: block; width: 100%; margin: 10px auto;" onclick="rubro_dimensiones_listar('+json.result.rubro_id+','+rubro+');">Volver</button>');
			}
		});
	}
}

//Editar Confirmacion
function rubro_dimensiones_editar_confirm(dimension,rubro){
		var data = {};
		data['dimension'] = dimension;
		data['nombre'] = $('#rubros_dimensiones_nombre').val_();
		data['hereda'] = $('#rubros_dimensiones_hereda').val_();

		apix('dimensiones','editar_dimension',data, {
			ok: function(json){
				notifica('Dimension editada','La dimensión se editó con éxito','confirmacion','success');
				rubro_dimensiones_listar(rubro);
			}
		});
}

//Agregar Confirmacion
function rubro_dimensiones_agregar_confirm(rubro){
		var data = {};
		data['rubro'] = rubro;
		data['nombre'] = $('#rubros_dimensiones_nombre').val_();
		data['hereda'] = $('#rubros_dimensiones_hereda').val_();

		apix('dimensiones','crear_dimension',data, {
			ok: function(json){
				notifica('Dimension Agregada','La dimensión se agregó con éxito','confirmacion','success');
				rubro_dimensiones_listar(rubro);
			}
		});
}

//Edita/Agrega
function rubro_edita_nombre_singular(){

	var ref = $('#'+rubros_config.modName+'_tree').jstree(true),
	sel = ref.get_selected();
	if(!sel.length) { 
		alert("SEleccionamos");
		return false; 
	}


	//Titulo...
	$('#'+rubros_config.modName+'Modal_titulo').html('Editar nombre singular');

	//Cargando...
	$('#'+rubros_config.modName+'Modal_container').html(cargando());

	//Footer...
	$('#'+rubros_config.modName+'Modal_footer').html('');

	//Contenido
	var contenido = $('<div class="scrollable" style="max-height: '+rubros_config.max_height_edita_agrega+'px; overflow-y: hidden;"></div>');


	var data = {};
	data['rubro'] = sel[0];

	apix(rubros_config.modName,'info_nombre_singular',data, {
        ok: function(json){

			//Titulo
			$('#'+rubros_config.modName+'Modal_titulo').html('Editar nombre singular del Rubro');

	    	//Agregamos el campo Nombre singular
	    	contenido.append( genera_campo({	
				id: 			"nombre_singular",
				label: 			"Nombre singular",
				tipo: 			"input",
				mostrarEditar: 	true,
				mostrarCrear: 	true
			}, rubros_config.modName ,true) );
	    	
	    	//Agregamos el valor
	    	$('#rubros_nombre_singular', contenido).val_(json.result);

	    	//Asignamos el contenido al popup
			$('#'+rubros_config.modName+'Modal_container').html(contenido);

			//Agregamos scrollbars
			$('#'+rubros_config.modName+'Modal_container .scrollable').mCustomScrollbar({theme: 'minimal-dark',scrollInertia: 100, mouseWheel:{ scrollAmount: 70 }});

			//Agregamos los botones al footer
			$('#'+rubros_config.modName+'Modal_footer').append('<button type="button" class="btn btn-primary" style="display: block; width: 100%; margin: 10px auto;" onclick="rubro_edita_nombre_singular_confirm('+sel[0]+');">Editar nombre singular</button>');
			$('#'+rubros_config.modName+'Modal_footer').append('<button type="button" class="btn btn-warning" style="display: block; width: 100%; margin: 10px auto;" onclick="'+rubros_config.modLabelPlural.toLowerCase()+'_listar();">Volver</button>');
		}
	});
}


//Confirma edicion de nombre Singular
function rubro_edita_nombre_singular_confirm(rubro){
		var data = {};
		data['rubro'] = rubro;
		data['rubro_nombre_singular'] = $('#rubros_nombre_singular').val_();

		apix('rubros','edita_nombre_singular',data, {
			ok: function(json){
				notifica('Nombre singular actualizado','El nombre se editó con éxito','confirmacion','success');
				rubros_listar();
			}
		});
}


//Edita/Agrega
function rubro_edita_nombres_alternativos(){

	//Si seleccionamos el rubro contiuamos sino salimos...
	var ref = $('#'+rubros_config.modName+'_tree').jstree(true),
	sel = ref.get_selected();
	if(!sel.length) {
		return false; 
	}

	//Titulo...
	$('#'+rubros_config.modName+'Modal_titulo').html('Editar nombres alternativos');

	//Cargando...
	$('#'+rubros_config.modName+'Modal_container').html(cargando());

	//Footer...
	$('#'+rubros_config.modName+'Modal_footer').html('');

	//Contenido
	var contenido = $('<div class="scrollable" style="max-height: '+rubros_config.max_height_edita_agrega+'px; overflow-y: hidden;"></div>');


	//Data para la API
	var data = {};
	data['rubro'] = sel[0];

	apix(rubros_config.modName,'info_nombres_alternativos',data, {
        ok: function(json){

			//Titulo
			$('#'+rubros_config.modName+'Modal_titulo').html('Editar nombres alternativos del Rubro');

	    	//Agregamos el campo Nombres Alternativos
	    	contenido.append( genera_campo({	
				id: 			"nombres_alternativos",
				label: 			"Nombres alternativos (separar con comas)",
				tipo: 			"textarea",
				mostrarEditar: 	true,
				mostrarCrear: 	true
			}, rubros_config.modName ,true) );
	    	
	    	//Agregamos el valor
	    	$('#rubros_nombres_alternativos', contenido).val_(json.result);

	    	//Asignamos el contenido al popup
			$('#'+rubros_config.modName+'Modal_container').html(contenido);

			//Agregamos los botones al footer
			$('#'+rubros_config.modName+'Modal_footer').append('<button type="button" class="btn btn-primary" style="display: block; width: 100%; margin: 10px auto;" onclick="rubro_edita_nombres_alternativos_confirm('+sel[0]+');">Editar Nombres Alternativos</button>');
			$('#'+rubros_config.modName+'Modal_footer').append('<button type="button" class="btn btn-warning" style="display: block; width: 100%; margin: 10px auto;" onclick="'+rubros_config.modLabelPlural.toLowerCase()+'_listar();">Volver</button>');
		}
	});
}


//Confirma edicion de nombres Alternativos
function rubro_edita_nombres_alternativos_confirm(rubro){
		var data = {};
		data['rubro'] = rubro;
		data['rubro_nombres_alternativos'] = $('#rubros_nombres_alternativos').val_();

		//
		apix('rubros','edita_nombres_alternativos',data, {
			ok: function(json){
				notifica('Nombres alternativos actualizados','Los nombres alternativos se editaron con éxito','confirmacion','success');
				rubros_listar();
			}
		});
}

{/if}

</script>