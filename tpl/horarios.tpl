{if="$GLOBALS['permisos']['horarios_administrar']"}

<script type="text/javascript">

/**********************************/
/* 			HORARIOS 			  */
/**********************************/

//Listar Horarios
function horarios_listar(){

	//Creamos el popup
	abrir_modal('horarios', 500, false);

	//Mostramos la ventana
	$('#horariosModal').modal({show:true});

	//Titulo...
	$('#horariosModal_titulo').html('Horarios');

	//Cargando...
	$('#horariosModal_container').html( cargando() );

	//Footer...
	$('#horariosModal_footer').html('');

	//Traemos los datos de la API
	apix('horarios','listar',{}, {
		ok: function(json){

			var tabla = generaTabla({
							id_tabla: 'horarios',
							items: json.result,
							keys_busqueda: ['nombre'],
							key_nombre: null,
							max_height: 500,
							campobusqueda: false,
							funcion_activado: function(el){

								//Seleccionamos el activo anterior
								var elActivo = $('#horarios_lista').find('.active_');
								var id = $(el).data('id');

								var html = '<b>'+$(el).data('nombre')+'</b><div class="pull-right descripcion" style="width: 120px; text-align: right; position: absolute; top: 8px; right: 10px;"><button class="btn btn-xs btn-primary" style="margin: 0px 0px 8px 5px; width: 24px; height: 24px;" data-toggle="tooltip" title="" onclick="semanario_listar(\''+$(el).data('id')+'\');" data-original-title="Semanario"><i class="fa fa-calendar"></i></button><button class="btn btn-xs btn-primary" style="margin: 0px 0px 8px 5px; width: 24px; height: 24px;" data-toggle="tooltip" title="" onclick="horarios_edita_agrega(\''+$(el).data('id')+'\');" data-original-title="Editar"><i class="fa fa-pencil"></i></button><button class="btn btn-xs btn-warning" style="margin: 0px 0px 8px 5px; width: 24px; height: 24px;" data-toggle="tooltip" title="Borrar" onclick=" confirma(\'Eliminar Horario\',\'El Horario se eliminará de forma permanente junto con todos los datos relacionados.\',\'exclamacion\',\'danger\',\'horarios_eliminar('+$(el).data('id')+')\');"><i class="fa fa-remove"></i></button><div></div></div>';

								//Agergamos el html
								$(el).html(html);
							},
							funcion_desactivado: function(el){
								var html = '<b>'+$(el).data('nombre')+'</b><div class="pull-right descripcion" style="width: 120px; text-align: right; position: absolute; top: 8px; right: 10px;"><button class="btn btn-xs btn-primary" style="margin: 0px 0px 8px 5px; width: 24px; height: 24px;" data-toggle="tooltip" title="" onclick="semanario_listar(\''+$(el).data('id')+'\');" data-original-title="Semanario"><i class="fa fa-calendar"></i></button><button class="btn btn-xs btn-primary" style="margin: 0px 0px 8px 5px; width: 24px; height: 24px;" data-toggle="tooltip" title="" onclick="horarios_edita_agrega(\''+$(el).data('id')+'\');" data-original-title="Editar"><i class="fa fa-pencil"></i></button><button class="btn btn-xs btn-warning" style="margin: 0px 0px 8px 5px; width: 24px; height: 24px;" data-toggle="tooltip" title="Borrar" onclick=" confirma(\'Eliminar Horario\',\'El Horario se eliminará de forma permanente junto con todos los datos relacionados.\',\'exclamacion\',\'danger\',\'horarios_eliminar('+$(el).data('id')+')\');"><i class="fa fa-remove"></i></button><div></div></div>';
								
								$(el).html(html);
							}
						});

			//Agregamos los tooltips
			tabla.tooltip({selector: '[data-toggle="tooltip"]', placement: 'left', animation: false});

			//Generamos el listado
			$('#horariosModal_container').html('');
			$('#horariosModal_container').append(tabla);


			//Agregamos scrollbars
			$('#horariosModal_container .scrollable').mCustomScrollbar({theme: 'minimal-dark',scrollInertia: 100, mouseWheel:{ scrollAmount: 70 }});

			//Agregar
			$('#horariosModal_footer').append('<button type="button" class="btn btn-primary" style="display: block; width: 100%; margin: 10px auto;" onclick="horarios_edita_agrega();">Nuevo Horario</button>');

			//Cerrar
			$('#horariosModal_footer').append('<button type="button" data-dismiss="modal" class="btn btn-warning" style="display: block; width: 100%; margin: 10px auto;">Cerrar</button>');
		}
	});
}

//Eliminar Horario
function horarios_eliminar(id){
	//Data array
	var data = {};
	data['horario'] = id;
	
	apix('horarios','eliminar',data, {
        ok: function(json){
			//Notifica
			notifica(	'Horario Eliminado',
						'El Horario se eliminó con éxito',
						'informacion',
						'info'
					);
			//Update
			horarios_listar();
		}
	});
}

//Edita/Agrega
function horarios_edita_agrega(id){

	//Si estamos agregando...
	if(typeof id === 'undefined') var agrega = true;
	else var agrega = false;

	//Titulo...
	if(agrega) $('#horariosModal_titulo').html('Agregar Horario');
	else $('#horariosModal_titulo').html('Editar Horario');

	//Cargando...
	$('#horariosModal_container').html(cargando());

	//Footer...
	$('#horariosModal_footer').html('');

	//Contenido
	var contenido = $('<div class="scrollable" style="max-height: 500px; overflow-y: hidden;"></div>'); 

	//Agregamos campo Nombre
	contenido.append( genera_campo({	
		id: 			"nombre",
		label: 			"Nombre",
		tipo: 			"input",
		mostrarEditar: 	true,
		mostrarCrear: 	true
	}, 'horarios' ,true) );


	//Agregamos el contenido y los botones
	//si estamos creando un registro nuevo
	if(agrega){
		//Agregamos el contenido
		$('#horariosModal_container').html(contenido);

		//Agregamos scrollbars
		$('#horariosModal_container .scrollable').mCustomScrollbar({theme: 'minimal-dark',scrollInertia: 100, mouseWheel:{ scrollAmount: 70 }});

		//Agregamos los botones al footer
		$('#horariosModal_footer').append('<button type="button" class="btn btn-primary" style="display: block; width: 100%; margin: 10px auto;" onclick="horarios_agrega_confirm();">Agregar Horario</button>');
		$('#horariosModal_footer').append('<button type="button" class="btn btn-warning" style="display: block; width: 100%; margin: 10px auto;" onclick="horarios_listar();">Volver</button>');
	}


	//Si editamos
	if(!agrega){
		var data = {};
		data['horario'] = id;

		apix('horarios','info',data, {
            ok: function(json){

				//Titulo
				$('#horariosModal_titulo').html('Editar Horario');

		    	//Llenamos los datos
		    	$('#horarios_nombre',contenido).val_(json.result.nombre);

		    	//Asignamos el contenido al popup
				$('#horariosModal_container').html(contenido);

				//Agregamos scrollbars
				$('#horariosModal_container .scrollable').mCustomScrollbar({theme: 'minimal-dark',scrollInertia: 100, mouseWheel:{ scrollAmount: 70 }});

				//Agregamos los botones al footer
				$('#horariosModal_footer').append('<button type="button" class="btn btn-primary" style="display: block; width: 100%; margin: 10px auto;" onclick="horarios_edita_confirm(\''+id+'\');">Editar Horario</button>');
				$('#horariosModal_footer').append('<button type="button" class="btn btn-warning" style="display: block; width: 100%; margin: 10px auto;" onclick="horarios_listar();">Volver</button>');
			}
		});
	}
}

//Edita
function horarios_edita_confirm(id){

	//Data
	var data = {};
	data['horario'] = id;
	data['nombre'] = $('#horarios_nombre').val_();

	apix('horarios','editar',data, {
		ok: function(json){
			notifica(	'Horario Editado',
						'El horario se editó con éxito',
						'confirmacion','success');
			horarios_listar();
		}
	});
}

//Agrega
function horarios_agrega_confirm(){
	//Data
	var data = {};
	data['nombre'] = $('#horarios_nombre').val_();

	apix('horarios','agregar',data, {
		ok: function(json){
			//Notifica
			notifica(	'Horario Creado',
						'El Horario se creó con éxito',
						'confirmacion',
						'success'
					);
			//Update
			horarios_listar();
		}
	});
}


/**********************************/
/* 			SEMANARIO 			  */
/**********************************/


//Listar Horarios
function semanario_listar(horario){

	//Creamos el popup
	abrir_modal('horarios', 500, false);

	//Mostramos la ventana
	$('#horariosModal').modal({show:true});

	//Titulo...
	$('#horariosModal_titulo').html('Semanario');

	//Cargando...
	$('#horariosModal_container').html( cargando() );

	//Footer...
	$('#horariosModal_footer').html('');

	var data = {};
	data['horario'] = horario;

	//Traemos los datos de la API
	apix('horarios','semanario_listar',data, {
		ok: function(json){

			var tabla = generaTabla({
							id_tabla: 'horarios',
							items: json.result,
							keys_busqueda: ['nombre'],
							key_nombre: null,
							max_height: 500,
							campobusqueda: false,
							funcion_activado: function(el){

								//Seleccionamos el activo anterior
								var elActivo = $('#semanario_lista').find('.active_');
								var id = $(el).data('id');

								var html = '<b style="width: 120px; display: inline-block; font-weight: bold;">'+$(el).data('dia_label')+'</b><span style="display: inline-block; width: 100px;">'+$(el).data('hora_inicio_label')+'</span><span style="display: inline-block; width: 100px;">'+$(el).data('hora_fin_label')+'</span><div class="pull-right descripcion" style="width: 120px; text-align: right; position: absolute; top: 8px; right: 10px;"><button class="btn btn-xs btn-primary" style="margin: 0px 0px 8px 5px; width: 24px; height: 24px;" data-toggle="tooltip" title="" onclick="semanario_edita_agrega('+$(el).data('id')+','+$(el).data('horario')+');" data-original-title="Editar"><i class="fa fa-pencil"></i></button><button class="btn btn-xs btn-warning" style="margin: 0px 0px 8px 5px; width: 24px; height: 24px;" data-toggle="tooltip" title="Borrar" onclick=" confirma(\'Eliminar Horario\',\'El Horario se eliminará de forma permanente junto con todos los datos relacionados.\',\'exclamacion\',\'danger\',\'semanario_eliminar('+$(el).data('id')+','+$(el).data('horario')+')\');"><i class="fa fa-remove"></i></button><div></div></div>';

								//Agergamos el html
								$(el).html(html);
							},
							funcion_desactivado: function(el){
								var html = '<b style="width: 120px; display: inline-block; font-weight: bold;">'+$(el).data('dia_label')+'</b><span style="display: inline-block; width: 100px;">'+$(el).data('hora_inicio_label')+'</span><span style="display: inline-block; width: 100px;">'+$(el).data('hora_fin_label')+'</span><div class="pull-right descripcion" style="width: 120px; text-align: right; position: absolute; top: 8px; right: 10px;"><button class="btn btn-xs btn-primary" style="margin: 0px 0px 8px 5px; width: 24px; height: 24px;" data-toggle="tooltip" title="" onclick="semanario_edita_agrega('+$(el).data('id')+','+$(el).data('horario')+');" data-original-title="Editar"><i class="fa fa-pencil"></i></button><button class="btn btn-xs btn-warning" style="margin: 0px 0px 8px 5px; width: 24px; height: 24px;" data-toggle="tooltip" title="Borrar" onclick=" confirma(\'Eliminar Horario\',\'El Horario se eliminará de forma permanente junto con todos los datos relacionados.\',\'exclamacion\',\'danger\',\'semanario_eliminar('+$(el).data('id')+','+$(el).data('horario')+')\');"><i class="fa fa-remove"></i></button><div></div></div>';
								
								$(el).html(html);
							}
						});

			//Agregamos los tooltips
			tabla.tooltip({selector: '[data-toggle="tooltip"]', placement: 'left', animation: false});

			//Generamos el listado
			$('#horariosModal_container').html('');
			$('#horariosModal_container').append(tabla);


			//Agregamos scrollbars
			$('#horariosModal_container .scrollable').mCustomScrollbar({theme: 'minimal-dark',scrollInertia: 100, mouseWheel:{ scrollAmount: 70 }});

			//Agregar
			$('#horariosModal_footer').append('<button type="button" class="btn btn-primary" style="display: block; width: 100%; margin: 10px auto;" onclick="semanario_edita_agrega(0,'+horario+');">Agregar</button>');

			//Cerrar
			$('#horariosModal_footer').append('<button type="button" onclick="horarios_listar();" class="btn btn-warning" style="display: block; width: 100%; margin: 10px auto;">Volver</button>');
		}
	});
}

//Eliminar Horario
function semanario_eliminar(id,horario){
	//Data array
	var data = {};
	data['semanario'] = id;
	
	apix('horarios','semanario_eliminar',data, {
        ok: function(json){
			//Notifica
			notifica(	'Horario del Semanario Eliminado',
						'El Horario del Semanario se eliminó con éxito',
						'informacion',
						'info'
					);
			//Update
			semanario_listar(horario);
		}
	});
}

//Edita/Agrega
function semanario_edita_agrega(id,horario){

	//Si estamos agregando...
	if(!id) var agrega = true;
	else var agrega = false;

	//Titulo...
	if(agrega) $('#horariosModal_titulo').html('Agregar Dia y Horario al Semanario');
	else $('#horariosModal_titulo').html('Editar Dia y Horario al Semanario');

	//Cargando...
	$('#horariosModal_container').html(cargando());

	//Footer...
	$('#horariosModal_footer').html('');

	//Contenido
	var contenido = $('<div class="scrollable" style="max-height: 500px; overflow-y: hidden;"></div>'); 

	//Agregamos campo Nombre
	var horas = [];
	for (var i = 0; i <= 23; i++)
		for (var x = 0; x <= 55; x=x+5)
			horas.push({id: pad(i,2)+":"+pad(x,2)+":00" , nombre: pad(i,2)+":"+pad(x,2)});

	//Agregamos los campos
	contenido.append( genera_campo({	
		id: 			"dia",
		label: 			"Día de la Semana",
		tipo: 			"select2",
		mostrarEditar: 	true,
		mostrarCrear: 	true,
		config: 		{
			minimumInputLength: 0,
			placeholder: $(this).attr('placeholder'),
			allowClear: false,
			width: 'resolve',
			id: function(e) { return e.id; },
			formatNoMatches: function() { return 'Sin resultados'; },
			formatSearching: function(){ return "Buscando..."; },
			data: [{id:1, nombre:'Lunes'},{id:2, nombre:'Martes'},{id:3, nombre:'Miercoles'},{id:4, nombre:'Jueves'},{id:5, nombre:'Viernes'},{id:6, nombre:'Sábado'},{id:7, nombre:'Domingo'}],
			formatInputTooShort: function () {
				return "";
			},
			formatResult: function(item) {
				return item.nombre;
			},
			formatSelection: function(item) { 
				return item.nombre;
			},
			initSelection : function (element, callback) {
				var elementText = $(element).attr('data-init-text');
			}
		}
	}, 'horarios' ,true) );
	contenido.append( genera_campo({	
		id: 			"hora_inicio",
		label: 			"Hora de Inicio de Actividad",
		tipo: 			"select2",
		mostrarEditar: 	true,
		mostrarCrear: 	true,
		config: 		{
			minimumInputLength: 0,
			placeholder: $(this).attr('placeholder'),
			allowClear: false,
			width: 'resolve',
			id: function(e) { return e.id; },
			formatNoMatches: function() { return 'Sin resultados'; },
			formatSearching: function(){ return "Buscando..."; },
			data: horas,
			formatInputTooShort: function () {
				return "";
			},
			formatResult: function(item) {
				return item.nombre;
			},
			formatSelection: function(item) { 
				return item.nombre;
			},
			initSelection : function (element, callback) {
				var elementText = $(element).attr('data-init-text');
			}
		}
	}, 'horarios' ,true) );
	contenido.append( genera_campo({	
		id: 			"hora_fin",
		label: 			"Hora de Finalización de Actividad",
		tipo: 			"select2",
		mostrarEditar: 	true,
		mostrarCrear: 	true,
		config: 		{
			minimumInputLength: 0,
			placeholder: $(this).attr('placeholder'),
			allowClear: false,
			width: 'resolve',
			id: function(e) { return e.id; },
			formatNoMatches: function() { return 'Sin resultados'; },
			formatSearching: function(){ return "Buscando..."; },
			data: horas,
			formatInputTooShort: function () {
				return "";
			},
			formatResult: function(item) {
				return item.nombre;
			},
			formatSelection: function(item) { 
				return item.nombre;
			},
			initSelection : function (element, callback) {
				var elementText = $(element).attr('data-init-text');
			}
		}
	}, 'horarios' ,true) );

	//Agregamos el contenido y los botones
	//si estamos creando un registro nuevo
	if(agrega){
		//Agregamos el contenido
		$('#horariosModal_container').html(contenido);

		//Agregamos scrollbars
		$('#horariosModal_container .scrollable').mCustomScrollbar({theme: 'minimal-dark',scrollInertia: 100, mouseWheel:{ scrollAmount: 70 }});

		//Agregamos los botones al footer
		$('#horariosModal_footer').append('<button type="button" class="btn btn-primary" style="display: block; width: 100%; margin: 10px auto;" onclick="semanario_agrega_confirm('+horario+');">Agregar Horario</button>');
		$('#horariosModal_footer').append('<button type="button" class="btn btn-warning" style="display: block; width: 100%; margin: 10px auto;" onclick="semanario_listar('+horario+');">Volver</button>');
	}


	//Si editamos
	if(!agrega){
		var data = {};
		data['semanario'] = id;

		apix('horarios','semanario_info',data, {
            ok: function(json){

				//Titulo
				$('#horariosModal_titulo').html('Editar Día de Semanario');

		    	//Llenamos los datos
		    	$('#horarios_dia',contenido).val_({id: json.result.dia, nombre: json.result.dia_label});
		    	$('#horarios_hora_inicio',contenido).val_({id: json.result.hora_inicio, nombre: json.result.hora_inicio_label});
		    	$('#horarios_hora_fin',contenido).val_({id: json.result.hora_fin, nombre: json.result.hora_fin_label});

		    	//Asignamos el contenido al popup
				$('#horariosModal_container').html(contenido);

				//Agregamos scrollbars
				$('#horariosModal_container .scrollable').mCustomScrollbar({theme: 'minimal-dark',scrollInertia: 100, mouseWheel:{ scrollAmount: 70 }});

				//Agregamos los botones al footer
				$('#horariosModal_footer').append('<button type="button" class="btn btn-primary" style="display: block; width: 100%; margin: 10px auto;" onclick="semanario_edita_confirm('+id+','+horario+');">Editar Horario</button>');
				$('#horariosModal_footer').append('<button type="button" class="btn btn-warning" style="display: block; width: 100%; margin: 10px auto;" onclick="semanario_listar('+horario+');">Volver</button>');
			}
		});
	}
}

//Edita
function semanario_edita_confirm(id,horario){
	var dia = $('#horarios_dia').val_();
	var hora_inicio = $('#horarios_hora_inicio').val_();
	var hora_fin = $('#horarios_hora_fin').val_();

	//Data
	var data = {};
	data['semanario'] = id;
	data['dia'] = dia.id;
	data['hora_inicio'] = hora_inicio.id;
	data['hora_fin'] = hora_fin.id;

	apix('horarios','semanario_editar',data, {
		ok: function(json){
			notifica(	'Horario Editado',
						'El Día del semanario se editó con éxito',
						'confirmacion','success');
			semanario_listar(horario);
		}
	});
}

//Agrega
function semanario_agrega_confirm(horario){
	var dia = $('#horarios_dia').val_();
	var hora_inicio = $('#horarios_hora_inicio').val_();
	var hora_fin = $('#horarios_hora_fin').val_();

	//Data
	var data = {};
	data['horario'] = horario;
	data['dia'] = dia.id;
	data['hora_inicio'] = hora_inicio.id;
	data['hora_fin'] = hora_fin.id;


	apix('horarios','semanario_agregar',data, {
		ok: function(json){
			//Notifica
			notifica(	'Horario Creado',
						'El Horario se Agrego al Semanario con éxito',
						'confirmacion',
						'success'
					);
			//Update
			semanario_listar(horario);
		}
	});
}


/**********************************/
/* 			FERIADOS 			  */
/**********************************/


//Listar Horarios
function feriados_listar(){

	//Creamos el popup
	abrir_modal('feriados', 500, false);

	//Mostramos la ventana
	$('#feriadosModal').modal({show:true});

	//Titulo...
	$('#feriadosModal_titulo').html('Feriados');

	//Cargando...
	$('#feriadosModal_container').html( cargando() );

	//Footer...
	$('#feriadosModal_footer').html('');

	var data = {};

	//Traemos los datos de la API
	apix('horarios','feriados_listar',data, {
		ok: function(json){

			var tabla = generaTabla({
							id_tabla: 'feriados',
							items: json.result,
							keys_busqueda: ['nombre'],
							key_nombre: null,
							max_height: 400,
							campobusqueda: false,
							funcion_activado: function(el){
							},
							funcion_desactivado: function(el){
								var html = '<b style="width: 120px; display: inline-block; font-weight: bold;">'+$(el).data('dia')+'</b>';

								if($(el).data('hora_inicio') && $(el).data('hora_fin')) html+='<span style="display: inline-block; width: 100px;">'+$(el).data('hora_inicio')+'</span><span style="display: inline-block; width: 100px;">'+$(el).data('hora_fin')+'</span>';

								if($(el).data('nombre') != '') html+='<div style="font-size:11px;">'+$(el).data('nombre')+'</div>';

								html += '<div class="pull-right descripcion" style="width: 120px; text-align: right; position: absolute; top: 8px; right: 10px;"><button class="btn btn-xs btn-primary" style="margin: 0px 0px 8px 5px; width: 24px; height: 24px;" data-toggle="tooltip" title="" onclick="semanario_edita_agrega(\''+$(el).data('dia_label')+'\');" data-original-title="Editar"><i class="fa fa-pencil"></i></button><button class="btn btn-xs btn-warning" style="margin: 0px 0px 8px 5px; width: 24px; height: 24px;" data-toggle="tooltip" title="Borrar" onclick=" confirma(\'Eliminar Horario\',\'El Horario se eliminará de forma permanente junto con todos los datos relacionados.\',\'exclamacion\',\'danger\',\'feriados_eliminar(\\\''+$(el).data('dia_label')+'\\\')\');"><i class="fa fa-remove"></i></button><div></div></div>';
								
								$(el).html(html);
							}
						});

			//Agregamos los tooltips
			tabla.tooltip({selector: '[data-toggle="tooltip"]', placement: 'left', animation: false});

			//Generamos el listado
			$('#feriadosModal_container').html('');
			$('#feriadosModal_container').append(tabla);


			//Agregamos scrollbars
			$('#feriadosModal_container .scrollable').mCustomScrollbar({theme: 'minimal-dark',scrollInertia: 100, mouseWheel:{ scrollAmount: 70 }});

			//Agregar
			$('#feriadosModal_footer').append('<button type="button" class="btn btn-primary" style="display: block; width: 100%; margin: 10px auto;" onclick="feriados_edita_agrega();">Agregar</button>');

			//Cerrar
			$('#feriadosModal_footer').append('<button type="button" data-dismiss="modal" class="btn btn-warning" style="display: block; width: 100%; margin: 10px auto;">Cerrar</button>');
		}
	});
}

//Eliminar Horario
function feriados_eliminar(fecha){
	//Data array
	var data = {};
	data['dia'] = fecha.replace(/-/g , "/");
	
	apix('horarios','feriados_eliminar',data, {
        ok: function(json){
			//Notifica
			notifica(	'Feriado Eliminado',
						'El Feriado se eliminó con éxito',
						'informacion',
						'info'
					);
			//Update
			feriados_listar();
		}
	});
}

//Edita/Agrega
function feriados_edita_agrega(fecha){

	//Si estamos agregando...
	if(!fecha) var agrega = true;
	else var agrega = false;

	//Titulo...
	if(agrega) $('#feriadosModal_titulo').html('Agregar Feriado');
	else $('#feriadosModal_titulo').html('Editar Feriado');

	//Cargando...
	$('#feriadosModal_container').html(cargando());

	//Footer...
	$('#feriadosModal_footer').html('');

	//Contenido
	var contenido = $('<div class="scrollable" style="max-height: 500px; overflow-y: hidden;"></div>'); 

	//Agregamos campo Nombre
	var horas = [{nombre:'Sin actividad', id: '0'}];
	for (var i = 0; i <= 23; i++)
		for (var x = 0; x <= 55; x=x+5)
			horas.push({id: pad(i,2)+":"+pad(x,2)+":00" , nombre: pad(i,2)+":"+pad(x,2)});

	//Agregamos los campos
	contenido.append( genera_campo({	
			id: 			"nombre",
			label: 			"Nombre del Feriado",
			tipo: 			"input",
			mostrarEditar: 	true,
			mostrarCrear: 	true
	}, 'feriados' ,true) );
	contenido.append( genera_campo({	
		id: 			"dia",
		label: 			"Fecha",
		tipo: 			"date",
		mostrarEditar: 	true,
		mostrarCrear: 	true,
		config: 		{
			format: 'dd/mm/yyyy',
			autoclose: true,
			language: 'es'
		}
	}, 'feriados' ,true));
	contenido.append( genera_campo({	
		id: 			"hora_inicio",
		label: 			"Hora de Inicio de Actividad",
		tipo: 			"select2",
		mostrarEditar: 	true,
		mostrarCrear: 	true,
		config: 		{
			minimumInputLength: 0,
			placeholder: $(this).attr('placeholder'),
			allowClear: false,
			width: 'resolve',
			id: function(e) { return e.id; },
			formatNoMatches: function() { return 'Sin resultados'; },
			formatSearching: function(){ return "Buscando..."; },
			data: horas,
			formatInputTooShort: function () {
				return "";
			},
			formatResult: function(item) {
				return item.nombre;
			},
			formatSelection: function(item) { 
				return item.nombre;
			},
			initSelection : function (element, callback) {
				var elementText = $(element).attr('data-init-text');
			}
		}
	}, 'feriados' ,true) );
	contenido.append( genera_campo({	
		id: 			"hora_fin",
		label: 			"Hora de Finalización de Actividad",
		tipo: 			"select2",
		mostrarEditar: 	true,
		mostrarCrear: 	true,
		config: 		{
			minimumInputLength: 0,
			placeholder: $(this).attr('placeholder'),
			allowClear: false,
			width: 'resolve',
			id: function(e) { return e.id; },
			formatNoMatches: function() { return 'Sin resultados'; },
			formatSearching: function(){ return "Buscando..."; },
			data: horas,
			formatInputTooShort: function () {
				return "";
			},
			formatResult: function(item) {
				return item.nombre;
			},
			formatSelection: function(item) { 
				return item.nombre;
			},
			initSelection : function (element, callback) {
				var elementText = $(element).attr('data-init-text');
			}
		}
	}, 'feriados' ,true) );

	//Agregamos el contenido y los botones
	//si estamos creando un registro nuevo
	if(agrega){
		//Agregamos el contenido
		$('#feriadosModal_container').html(contenido);

		//Agregamos scrollbars
		$('#feriadosModal_container .scrollable').mCustomScrollbar({theme: 'minimal-dark',scrollInertia: 100, mouseWheel:{ scrollAmount: 70 }});

		//Agregamos los botones al footer
		$('#feriadosModal_footer').append('<button type="button" class="btn btn-primary" style="display: block; width: 100%; margin: 10px auto;" onclick="feriados_agrega_confirm();">Agregar Feriado</button>');
		$('#feriadosModal_footer').append('<button type="button" class="btn btn-warning" style="display: block; width: 100%; margin: 10px auto;" onclick="feriados_listar();">Volver</button>');
	}


	//Si editamos
	if(!agrega){
		var data = {};
		data['feriado'] = fecha;

		apix('horarios','feriados_info',data, {
            ok: function(json){

				//Titulo
				$('#feriadosModal_titulo').html('Editar Día de Semanario');

		    	//Llenamos los datos
		    	$('#horarios_dia',contenido).val_({id: json.result.dia, nombre: json.result.dia_label});
		    	$('#horarios_hora_inicio',contenido).val_({id: json.result.hora_inicio, nombre: json.result.hora_inicio_label});
		    	$('#horarios_hora_fin',contenido).val_({id: json.result.hora_fin, nombre: json.result.hora_fin_label});

		    	//Asignamos el contenido al popup
				$('#feriadosModal_container').html(contenido);

				//Agregamos scrollbars
				$('#feriadosModal_container .scrollable').mCustomScrollbar({theme: 'minimal-dark',scrollInertia: 100, mouseWheel:{ scrollAmount: 70 }});

				//Agregamos los botones al footer
				$('#feriadosModal_footer').append('<button type="button" class="btn btn-primary" style="display: block; width: 100%; margin: 10px auto;" onclick="feriados_edita_confirm(\''+fecha+'\');">Editar Feriado</button>');
				$('#horariosModal_footer').append('<button type="button" class="btn btn-warning" style="display: block; width: 100%; margin: 10px auto;" onclick="feriados_listar();">Volver</button>');
			}
		});
	}
}

//Edita
function feriados_edita_confirm(fecha){
	var dia = $('#horarios_dia').val_();
	var hora_inicio = $('#horarios_hora_inicio').val_();
	var hora_fin = $('#horarios_hora_fin').val_();

	//Data
	var data = {};
	data['semanario'] = id;
	data['dia'] = dia.id;
	data['hora_inicio'] = hora_inicio.id;
	data['hora_fin'] = hora_fin.id;

	apix('horarios','semanario_editar',data, {
		ok: function(json){
			notifica(	'Horario Editado',
						'El Día del semanario se editó con éxito',
						'confirmacion','success');
			semanario_listar(horario);
		}
	});
}

//Agrega
function feriados_agrega_confirm(){
	var hora_inicio = $('#feriados_hora_inicio').val_();
	var hora_fin = $('#feriados_hora_fin').val_();


	//Data
	var data = {};
	data['nombre'] = $('#feriados_nombre').val();
	data['dia'] = $('#feriados_dia').val();
	
	if(hora_inicio) data['hora_inicio'] = hora_inicio.id;
	else data['hora_inicio'] = '0';

	if(hora_fin) data['hora_fin'] = hora_fin.id;
	else data['hora_fin'] = '0';


	apix('horarios','feriados_agregar',data, {
		ok: function(json){
			//Notifica
			notifica(	'Feriado Creado',
						'El Feriado se Agrego con éxito',
						'confirmacion',
						'success'
					);
			//Update
			feriados_listar();
		}
	});
}

</script>

{/if}