<script type="text/javascript">

{if="$GLOBALS['permisos']['articulos_edicion_dimensiones']"}

function dimensiones_articulo_listar(id,cod){

  //Creamos el popup
  abrir_modal('dimensiones_articulo', 600, false);

  //Mostramos la ventana
  $('#dimensiones_articuloModal').modal({show:true});

  //Titulo...
  $('#dimensiones_articuloModal_titulo').html('Dimensiones del artículo <b>' + cod.replace(/ /g, '') + '</b>');

  //Cargando...
  $('#dimensiones_articuloModal_container').html( cargando() );

  //Footer...
  $('#dimensiones_articuloModal_footer').html('');

  //Traemos los datos de la API
  var data = {};
  data['articulo'] = id;

  apix('dimensiones','listar_x_articulo',data, {
    ok: function(json){
      //Estructura del 
      var contenido = $('<div class="scrollable" id="usuarios_lista" style="padding: 3px 0px 10px 0px; max-height: 300px; overflow-y: hidden; margin-bottom: 0px;"></div>');

      //Agregamos items
      for(var k in json.result){

        //Funcion que genera los campos
        if(!json.result[k].opciones){
          contenido.append( genera_campo({ 
                                  id:         json.result[k].id,
                                  label:      json.result[k].nombre,
                                  tipo:       "input",
                                  mostrarEditar:  true,
                                  mostrarCrear:   true
          },'dimensiones',true));

          //Damos valor
          $('#dimensiones_'+json.result[k].id, contenido).val_(json.result[k].val);
        }
      }


      //Creamos los selects
      for(var k in json.result)
        if(json.result[k].opciones){

          contenido.append( genera_campo({
                                  id:         json.result[k].id,
                                  label:      json.result[k].nombre,
                                  tipo:       "select2",
                                  mostrarEditar:  true,
                                  mostrarCrear:   true,
                                  config:     {
                                      minimumInputLength: 0,
                                      placeholder: $(this).attr('placeholder'),
                                      width: 'resolve',
                                      data: json.result[k].opciones,
                                      id: function(e) { return e.id; },
                                      formatNoMatches: function() { return 'Sin resultados'; },
                                      formatSearching: function(){ return "Buscando..."; },
                                      formatInputTooShort: function () {
                                        return "";
                                      },
                                      formatResult: function(item) {
                                        return item.nombre;
                                      },
                                      formatSelection: function(item) { 
                                        return item.nombre;
                                      },
                                      initSelection : function (element, callback) {
                                        var elementText = $(element).attr('data-init-text');
                                      }
                                  }
          },'dimensiones',true));

          //Asignamos valor
          if(json.result[k].val != '')
            $('#dimensiones_'+json.result[k].id, contenido).val_(json.result[k].val);
        }


      //Generamos el listado de usuarios
      $('#dimensiones_articuloModal_container').empty();
      $('#dimensiones_articuloModal_container').append(contenido);

      //Agregamos scrollbars
      $('.scrollable').mCustomScrollbar({theme: 'minimal-dark',scrollInertia: 100});

      //Boton Confirmar cambios
      $('#dimensiones_articuloModal_footer').append('<button type="button" class="btn btn-primary" style="display: block; width: 100%; margin: 10px auto;" onclick="dimensiones_articulo_editar('+id+');">Guardar Cambios</button>');

      //Append Cancel Button
      $('#dimensiones_articuloModal_footer').append('<button type="button" data-dismiss="modal" class="btn btn-warning" style="display: block; width: 100%; margin: 10px auto;">Descartar Cambios</button>');
    }
  });
}

function dimensiones_articulo_editar(id){
  var dimensiones = {};
  $('#dimensiones_articuloModal_container [id^="dimensiones_"]').each(function(){
    var id = $(this).attr('id').replace('dimensiones_','');
    var value = $(this).val_();
    if(typeof value === 'undefined') value = '';
    if(typeof value === 'object'){
      if(value != null) value = value.id;
      else value = '';
    }

    //Guardamos la dimension
    dimensiones[id] = value;
  });

  //Llamada a la API
  var data = {};
  data['articulo'] = id;
  data['dimensiones'] = JSON.stringify(dimensiones);
  apix('dimensiones','editar_x_articulo',data, {
    ok: function(json){
      //Si todo salio ok mostramos el cartel y cerramos la ventana
      notifica('Dimensiones guardadas','Las dimensiones se modificaron con éxito','confirmacion','success');
      //cerramos la ventana...
      $('#dimensiones_articuloModal').modal('hide');
      //Actualizamos la lista de articulos
      watable_volver = true;
      articulosUpdate();
    }
  });
}

{/if}










{if="$GLOBALS['permisos']['importar_dimensiones']"}

function importador_dimensiones(){

  //Creamos el popup
  abrir_modal('importador_dimensiones', 500, false);

  //Mostramos la ventana
  $('#importador_dimensionesModal').modal({show:true});

  //Titulo...
  $('#importador_dimensionesModal_titulo').html('Importador de Dimensiones');

  //Contenedor
  var contenido = $('<div class="scrollable" style="max-height: 400px; overflow-y: hidden;"></div>');

  //Archivo de importacion
  contenido.append('<input type="file" id="importador_dimensiones_input" name="archivo" style="display: none;">');

  //Evento al seleccionar archivo
  $('#importador_dimensiones_input',contenido).on('change', function(){

    //Obtenemos las dimensiones del rubro
    var dimensiones_tmp = $('#importador_dimensiones_dimensiones').data('dimensiones');

    if($('#importador_dimensiones_columna_codigo').val_() == '')
      notifica('Error','La columna Código es obligatoria','exclamacion','danger');

    if( ($('#importador_dimensiones_input').val() != '') &&
        ($('#importador_dimensiones_columna_codigo').val_() != '')){

      //Recolectamos las variables
      var file = $('#importador_dimensiones_input').prop('files')[0];

      //Obtenemos las dimensiones
      var dimensiones = {};
      for(var i in dimensiones_tmp)
        dimensiones[ dimensiones_tmp[i].id ] = $('#importador_dimensiones_dimension_'+dimensiones_tmp[i].id).val_();
      
      //Rubro
      var rubro = $('#importador_dimensiones_rubro').val_();
      if(rubro != null) rubro = rubro.id;
      else rubro = '';

      //Datos del form
      var data = new FormData();
      data.append('archivo', file);
      data.append('rubro', rubro);
      data.append('modificar_rubro', $('#importador_dimensiones_modificar_rubro').val_());
      data.append('actualizar_o_sobreescribir_descripcion', $('#importador_dimensiones_actualizar_o_sobreescribir_descripcion').val_());
      data.append('actualizar_o_sobreescribir', $('#importador_dimensiones_actualizar_o_sobreescribir').val_());
      data.append('columna_codigo', $('#importador_dimensiones_columna_codigo').val_());
      data.append('columna_descripcion', $('#importador_dimensiones_columna_descripcion').val_());
      data.append('dimensiones', JSON.stringify(dimensiones));


      //Indicamos al usuario que el sistema ahora va a estar ocupado haciendo el backup
      $('#importador_dimensionesModal_titulo').html('Actualizando Dimensiones...');
      $('#importador_dimensionesModal_container').html(cargando());
      $('#importador_dimensionesModal_footer').html('');

      //API
      apix('dimensiones','importar',data, {
        file: true,
        ok: function(json){

            $('#importador_dimensionesModal_container').html(json.result.html);
            $('#importador_dimensionesModal_titulo').html('Importación finalizada');

            //Boton Insertar imagen
            $('#importador_dimensionesModal_footer').append('<button type="button" class="btn btn-primary" style="display: block; width: 100%; margin: 10px auto;" onclick="importador_dimensiones_imprimir();">Imprimir Reporte</button>');

            //Append Cancel Button
            $('#importador_dimensionesModal_footer').append('<button type="button" data-dismiss="modal" class="btn btn-warning" style="display: block; width: 100%; margin: 10px auto;">Cerrar</button>');
        },
        error: function(){
          importador_dimensiones();
        }
      });
    }

    //Reseteamos el campo
    $('#importador_dimensiones_input').val('');

  });

  //Rubro
  contenido.append(genera_campo({ 
      id:       "rubro",
      label:      "Rubro",
      tipo:       "select2",
      mostrarEditar:  true,
      mostrarCrear:   true,
      config:     {
        minimumInputLength: 0,
        placeholder: $(this).attr('placeholder'),
        allowClear: true,
        width: 'resolve',
        id: function(e) { return e.id; },
        formatNoMatches: function() { return 'Sin resultados'; },
        formatSearching: function(){ return "Buscando..."; },
        ajax: {
            url: "api.php",
            dataType: 'json',
            quietMillis: 200,
            data: function(term, page) {
                return {
                    module: 'rubros',
                    run: 'listar_rubros_s2',
                    q: term,
                    p: page
                };
            },
            results: function(data, page ) {
                //Si el ususario no esta logueado refresh
                if(!data.logued) location.reload();

                //Calcula si existen mas resultados a mostrar
                var more = (page * 40) < data.result.total;

                //Devolvemos el valor de more para que selec2
                //sepa que debemos cargar mas resultados.
                return {results: data.result.items, more: more};
            }
        },
        formatInputTooShort: function () {
          return "";
        },
        formatResult: function(item) {
          if(parseInt(item.nivel) == 2)
            return '<i class="badge" style="padding: 4px 6px; float: none; border-radius: 2px; background-color: #e6e6e6; margin-left: 0px; color: #444444; font-size: 11px; border: solid 1px #dadada;">'+item.bread+'</i> ';

          if(parseInt(item.nivel) == 3)
            return '<i class="badge" style="padding: 4px 6px; float: none; border-radius: 2px; background-color: #e6e6e6; margin-left: 25px; color: #444444; font-size: 11px; border: solid 1px #dadada;">'+item.bread+'</i> ';

          if(parseInt(item.nivel) == 4)
            return '<i class="badge" style="padding: 4px 6px; float: none; border-radius: 2px; background-color: #e6e6e6; margin-left: 50px; color: #444444; font-size: 11px; border: solid 1px #dadada;">'+item.bread+'</i> ';

          return item.bread;
        },
        formatSelection: function(item) { 
          return item.bread;
        },
        initSelection : function (element, callback) {
        }
      }
      },
      'importador_dimensiones',
      true)
  );
  
  //Eventos Rubro
  $('#importador_dimensiones_rubro',contenido).on("change", function(e){

    //Quitar dimensiones anteriores
    $('[id^="importador_dimensiones_dimension_"]').remove();

    //Guardamos el rubro
    var rubro = $('#importador_dimensiones_rubro').val_();

    //Si no seleccionamos Rubro vaciamos los mismos
    if(rubro == null) $('#importador_dimensiones_dimensiones').data('dimensiones',undefined);
    
    //Si seleccionamos Rubro
    if(rubro != null){
      //API
      var data = {};
      data['rubro'] = rubro.id;
      apix('dimensiones','listar_x_rubro',data, {
        ok: function(json){
          
          //Si no tiene dimensiones
          if(!json.result.length){
            $('#importador_dimensiones_dimensiones').data('dimensiones',undefined);
            notifica('Rubro sin dimensiones','Éste Rubro no posee dimensiones asignadas','informacion','info');
          }

          //Si tiene dimensiones
          if(json.result.length){
            //Guardamos las dimensiones
            $('#importador_dimensiones_dimensiones').data('dimensiones',json.result);

            for(var i in json.result)

              //Di no tiene opciones
              if(!json.result[i].opciones)
                $('#importador_dimensiones_dimensiones').append(genera_campo({ 
                  id:         'dimension_'+json.result[i].id,
                  label:      json.result[i].label,
                  tipo:       "input",
                  mostrarEditar:  true,
                  mostrarCrear:   true
                },'importador_dimensiones',true));
          }
        }
      });
    }
  });

  //Info
  contenido.append('<p style="margin: 10px 5px;"><small>Cambia los rubros de todos los articulos al seleccionado arriba. A exepcion de aquellos rubros que tengan equivalencias que sincronicen dimensiones y estas ya tengan asignado otro Rubro distinto.</small></p>');

  //Modificar Rubro
  contenido.append( genera_campo(   { 
      id:       "modificar_rubro",
      label:      "Activo",
      tipo:       "checkbox",
      mostrarEditar:  true,
      mostrarCrear:   true,
      config:     {
              claseActivo: 'btn-success',
              claseInactivo: 'btn-danger',
              valueActivo: '1',
              valueInactivo: '0',
              valueDefault: '1',
              labelActivo: function(el){
                el.html('Realizar cambios si el Artículo no pertenece a este Rubro');
              },
              labelInactivo: function(el){
                el.html('No realizar cambios si el Artículo no pertenece a este Rubro');
              }
      }
  },'importador_dimensiones',true));


  //Info
  contenido.append('<p style="margin: 10px 5px;"><small>En el caso que existan dimensiones en la DB puede mantenerlas, actualizando sólo los datos nuevos. O bien, puede sobreescribir todo. Es decir, si la celda que representa la dimensión en el archivo de importación esta vacia borra el dato existente en la DB.</small></p>');

  //Actualizar o Sobreescribir
  contenido.append( genera_campo(   { 
      id:       "actualizar_o_sobreescribir",
      label:      "Activo",
      tipo:       "checkbox",
      mostrarEditar:  true,
      mostrarCrear:   true,
      config:     {
              claseActivo: 'btn-success',
              claseInactivo: 'btn-danger',
              valueActivo: '1',
              valueInactivo: '0',
              valueDefault: '1',
              labelActivo: function(el){
                el.html('Actualizar sólo las dimensiones que contengan datos');
              },
              labelInactivo: function(el){
                el.html('Sobreescribir todo (si la celda esta vacia borra el dato)');
              }
      }
  },'importador_dimensiones',true));

  //Info
  contenido.append('<p style="margin: 10px 5px;"><small>Indique en que columna del archivo XLS de importación se encuentra cada uno de los datos. Por ejemplo, si el Código del articulo se encuentra en la columna A, complete dicho campo con la letra \'A\'</small></p>');

  //Codigo
  contenido.append( genera_campo({ 
                          id:         'columna_codigo',
                          label:      'Código',
                          tipo:       "input",
                          mostrarEditar:  true,
                          mostrarCrear:   true
  },'importador_dimensiones',true));

  //Descripcion
  contenido.append( genera_campo({ 
                          id:         'columna_descripcion',
                          label:      'Descripción',
                          tipo:       "input",
                          mostrarEditar:  true,
                          mostrarCrear:   true
  },'importador_dimensiones',true));

  //Modificar Equivalencias
  contenido.append( genera_campo(   { 
      id:       "actualizar_o_sobreescribir_descripcion",
      label:      "Activo",
      tipo:       "checkbox",
      mostrarEditar:  true,
      mostrarCrear:   true,
      config:     {
              claseActivo: 'btn-success',
              claseInactivo: 'btn-danger',
              valueActivo: '1',
              valueInactivo: '0',
              valueDefault: '1',
              labelActivo: function(el){
                el.html('Actualiza sólo las descripciones con texto');
              },
              labelInactivo: function(el){
                el.html('Actualiza todas las descripciones, incluso las vacias');
              }
      }
  },'importador_dimensiones',true));

  //Contenedor
  contenido.append('<div id="importador_dimensiones_dimensiones"></div>');

  //Cargando...
  $('#importador_dimensionesModal_container').html('');
  $('#importador_dimensionesModal_container').append(contenido);

  //Agregamos scrollbars
  $('#importador_dimensionesModal_container .scrollable').mCustomScrollbar({theme: 'minimal-dark',scrollInertia: 100, mouseWheel:{ scrollAmount: 70 }});

  //Boton Confirmar cambios
  $('#importador_dimensionesModal_footer').append('<button type="button" class="btn btn-primary" style="display: block; width: 100%; margin: 10px auto;" onclick="$(\'#importador_dimensiones_input\').click();">Seleccionar Archivo de Importación</button>');

  //Append Cancel Button
  $('#importador_dimensionesModal_footer').append('<button type="button" data-dismiss="modal" class="btn btn-warning" style="display: block; width: 100%; margin: 10px auto;">Salir</button>');
}

{/if}

</script>