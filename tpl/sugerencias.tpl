<script type="text/javascript">

{if="$GLOBALS['permisos']['sugerencias_agregar']"}

//Agregar una Sugerencia
function sugerencias_agregar(){

  //Creamos el popup
  abrir_modal('sugerencias_agregar', 600, false);

  //Mostramos la ventana
  $('#sugerencias_agregarModal').modal({show:true});

  //Titulo...
  $('#sugerencias_agregarModal_titulo').html('Ayudanos a Mejorar');

  //Info...
  $('#sugerencias_agregarModal_container').html('¿Encontraste un error en los datos de alguna pieza? ¿Sabes que hay datos omitidos y que deberían estar? Ayudanos a mejorar esta herramienta de trabajo! <br/>Desde acá podés enviarnos tus comentarios o sugerencias sobre alguna pieza. En cuanto analicemos y corrijamos el inconveniente nos pondremos en contacto para informarte como resulto el proceso.<br/><br/>');

  //Buscador de piezas
  $('#sugerencias_agregarModal_container').append( genera_campo( { 
      id:       "codigo",
      label:      "Fabricante y Código",
      tipo:       "select2",
      mostrarEditar:  true,
      mostrarCrear:   true,
      config:     {
                minimumInputLength: 0,
                placeholder: 'Fabricante y Código',
                allowClear: true,
                width: 'resolve',
                id: function(e) { return e.articulo_id; },
                formatNoMatches: function() { return 'Sin resultados'; },
                formatSearching: function(){ return "Buscando..."; },
                ajax: {
                    url: "api.php",
                    dataType: 'json',
                    quietMillis: 200,
                    data: function(term, page) {
                        return {
                            module: 'sugerencias',
                            run: 'fabricantes_codigo_listar',
                            q: term,
                            p: page
                        };
                    },
                    results: function(data, page ) {
                        //If session expire reload page...
                        //if(!data.logued) location.reload();

                        var more = (page * 40) < data.result.total; // whether or not there are more results available

                        // notice we return the value of more so Select2 knows if more results can be loaded
                        return {results: data.result.items, more: more};
                    }
                },
                formatInputTooShort: function () {
                          return "";
                      },  
                formatResult: function(item) {
                    return '<div class="select2-user-result"><i class="badge" style="padding: 5px 6px 6px 7px; float: none; border-radius: 4px; background-color: #e6e6e6; margin-right: 7px; margin-top: 2px; color: #444444; font-size: 12px; border: solid 1px #dadada">'+item.marca_label+'</i><b>'+item.codigo+'</b></div>'; 
                },
                formatSelection: function(item) {
                  var codigo = item.codigo;
                  
                  //Creamos el textp para el codigo
                  if(codigo == null) codigo = 'Ingrese el código...';
                  else codigo = '<b>' + codigo + '</b>';

                  var label = '<i class="badge" style="padding: 5px 6px 6px 7px; float: none; border-radius: 4px; background-color: #e6e6e6; margin-right: 7px; margin-top: 2px; color: #444444; font-size: 12px; border: solid 1px #dadada;">'+item.marca_label+'</i>' + codigo;

                    return label; 
                },
                initSelection : function (element, callback) {
                    var elementText = $(element).attr('data-init-text');
                }
              }
    }, 'sugerencias' ,true) );

  $('#sugerencias_agregarModal_container').append('<div class="form-group"><textarea class="form-control" rows="5" id="sugerencias_comentarios" placeholder="Comentarios"></textarea></div>');

  //Boton Confirmar cambios
  $('#sugerencias_agregarModal_footer').append('<button type="button" class="btn btn-primary" style="display: block; width: 100%; margin: 10px auto;" onclick="sugerencias_agregar_confirm();">Enviar</button>');

  //Append Cancel Button
  $('#sugerencias_agregarModal_footer').append('<button type="button" data-dismiss="modal" class="btn btn-warning" style="display: block; width: 100%; margin: 10px auto;">Cerrar</button>');
}

//Confirmamos agregar Sugerencia
function sugerencias_agregar_confirm(){
  
  var data = {};

  //ID del artículo
  data['articulo'] = $('#s2id_sugerencias_codigo').select2('data');
  if(data['articulo'] != null) data['articulo'] = data['articulo'].articulo_id;

  data['comentarios'] = $('#sugerencias_comentarios').val();

  
  //Llamada a la API
  apix('sugerencias','agregar',data, {
    ok: function(json){
      //Actualizamos la lista de articulos
      watable_volver = true;

      //Cerramos el popUp
      $('#sugerencias_agregarModal').modal('hide');
    }
  });
}
{/if}


//Lista
{if="$GLOBALS['permisos']['sugerencias_listar']"}
function sugerencias_listar(){

  //Creamos el popup
  abrir_modal('sugerencias_listar', 800, false);

  //Mostramos la ventana
  $('#sugerencias_listarModal').modal({show:true});

  //Titulo...
  $('#sugerencias_listarModal_titulo').html('Sugerencias');

  //Cargando...
  $('#sugerencias_listarModal_container').html( cargando() );

  //Footer...
  $('#sugerencias_listarModal_footer').html('');

  //Traemos los datos de la API
  apix('sugerencias','listar',{}, {
    ok: function(json){

      var tabla = generaTabla({
              id_tabla: 'sugerencias',
              items: json.result,
              keys_busqueda: ['codigo','comentario','usuario'],
              key_nombre: null,
              max_height: 400,
              funcion_activado: function(el){

                //Seleccionamos el activo anterior
                var elActivo = $('#sugerencias_listar_lista').find('.active_');
                var id = $(el).data('id');

                //Articulo
                var html = '<p style="width: 130px;display: inline-block;margin: 0px;"><span style="font-weight: bold;font-size: 10px;">CÓDIGO</span><br>'+$(el).data('codigo')+'</p>';

                //Usuario
                html += '<p style="width: 300px; display: inline-block; margin: 0px;"><span style="font-weight: bold; font-size: 10px;">USUARIO</span><br>'+$(el).data('usuario');

                //Razon Social
                if($(el).data('r') != null) html+= ' - ' + $(el).data('r');
                html+='</p>'

                //Fecha
                html += '<p style="width: 130px;display: inline-block; margin: 0px;"><span style="font-weight: bold; font-size: 10px;">FECHA</span><br>'+$(el).data('fecha')+'</p>';

                var accion_click = ''; 
                {if="$GLOBALS['permisos']['sugerencias_administrar']"}
                accion_click = "confirma('Resolver Sugerencia','Desea marcar la sugerencia como resuelta y enviar un email al usuario para informarlo?','interrogacion','info','sugerencias_resolver("+$(el).data('id')+")'); event.stopPropagation();";
                {/if}

                html += '<p style="width: 160px; display: inline-block;text-align: right;margin: 0px;">';

                //Pendiente o procesado?
                if($(el).data('completado') == '0')
                  html += '<button class="btn btn-xs btn-warning" style="margin: 0px 0px 8px 5px;height: 24px;width: 90px;" data-toggle="tooltip" title="" onclick="'+accion_click+'" data-original-title="Click para marcar como Procesado">PENDIENTE</button>';
                else
                  html += '<button class="btn btn-xs btn-success" style="margin: 0px 0px 8px 5px;height: 24px;width: 90px;">RESUELTO</button>';

                //Agregams el boton editar
                if($(el).data('completado') == '0'){
                  html += '<button class="btn btn-xs btn-primary" style="margin: 0px 0px 8px 5px; width: 24px; height: 24px;" data-toggle="tooltip" title="Editar" onclick="sugerencias_edita(\''+$(el).data('id')+'\'); event.stopPropagation();"><i class="fa fa-pencil"></i></button>';

                  html += '<button class="btn btn-xs btn-warning" style="margin: 0px 0px 8px 5px; width: 24px; height: 24px;" data-toggle="tooltip" title="Borrar" onclick=" confirma(\'Eliminar sugerencia\',\'La Sugerencia se eliminará de forma permanente.\',\'exclamacion\',\'danger\',\'sugerencias_elimina(\\\''+$(el).data('id')+'\\\')\'); event.stopPropagation();"><i class="fa fa-remove"></i></button>';
                }

                html += '</p>';

                //Comentarios
                html += '<p style="margin: 0px;"><span style="font-weight: bold; font-size: 10px;">COMENTARIOS</span><br>'+$(el).data('comentario')+'</p>';

                //Agergamos el html
                $(el).html(html);
              },
              funcion_desactivado: function(el){

                //Articulo
                var html = '<p style="width: 130px;display: inline-block;margin: 0px;"><span style="font-weight: bold;font-size: 10px; padding: 0px;">CÓDIGO</span><br>'+$(el).data('codigo')+'</p>';

                //Usuario
                html += '<p style="width: 300px; display: inline-block; margin: 0px;"><span style="font-weight: bold; font-size: 10px;">USUARIO</span><br>'+$(el).data('usuario');

                //Razon Social
                if($(el).data('r') != null) html+= ' - ' + $(el).data('r');
                html+='</p>'

                //Fecha
                html += '<p style="width: 130px;display: inline-block;margin: 0px;"><span style="font-weight: bold; font-size: 10px; padding: 0px;">FECHA</span><br>'+$(el).data('fecha')+'</p>';

                //Pendiente o procesado?
                if($(el).data('completado') == '0')
                  html += '<p style="width: 160px; display: inline-block;text-align: right;margin: 0px;"><button class="btn btn-xs btn-warning" style="margin: 0px 0px 8px 5px;height: 24px;width: 90px;" data-toggle="tooltip" title="" onclick=" confirma(\'Resolver Sugerencia\',\'Desea marcar la sugerencia como resuelta y enviar un email al usuario para informarlo?\',\'interrogacion\',\'info\',\'sugerencias_resolver('+$(el).data('id')+')\'); event.stopPropagation();" data-original-title="Click para marcar como Procesado">PENDIENTE</button></p>';
                else
                  html += '<p style="width: 160px; display: inline-block;text-align: right;margin: 0px;"><button class="btn btn-xs btn-success" style="margin: 0px 0px 8px 5px;height: 24px;width: 90px;">RESUELTO</button></p>';

               
                $(el).html(html);
              }
            });

      //Agregamos los tooltips
      tabla.tooltip({selector: '[data-toggle="tooltip"]', placement: 'left', animation: false});

      //Generamos el listado
      $('#sugerencias_listarModal_container').html('');
      $('#sugerencias_listarModal_container').append(tabla);


      //Agregamos scrollbars
      $('#sugerencias_listarModal_container .scrollable').mCustomScrollbar({theme: 'minimal-dark',scrollInertia: 100, mouseWheel:{ scrollAmount: 70 }});

      //Cerrar
      $('#sugerencias_listarModal_footer').append('<button type="button" data-dismiss="modal" class="btn btn-warning" style="display: block; width: 100%; margin: 10px auto;">Cerrar</button>');
    }
  });
}
{/if}

{if="$GLOBALS['permisos']['sugerencias_administrar']"}
function sugerencias_resolver(id){
  var data = {};
  data['sugerencia'] = id;
  
  //Llamada a la API
  apix('sugerencias','resolver',data, {
    ok: function(json){
      sugerencias_listar();
    }
  });
}
{/if}

{if="$GLOBALS['permisos']['sugerencias_listar']"}
function sugerencias_edita(id){
  var data = {};
  data['sugerencia'] = id;

  //Titulo...
  $('#sugerencias_listarModal_titulo').html('Editar Sugerencia');

  //Cargando...
  $('#sugerencias_listarModal_container').html( cargando() );

  //Footer...
  $('#sugerencias_listarModal_footer').html('');

  //Llamada a la API
  apix('sugerencias','info',data, {
    ok: function(json){
      //Titulo...
      $('#sugerencias_listarModal_titulo').html('Editar Sugerencia');

      $('#sugerencias_listarModal_container').html('');

      //Agregamos campo
      $('#sugerencias_listarModal_container').append( genera_campo({ 
        id:       "comentarios",
        label:      "Comentarios",
        tipo:       "textarea",
        mostrarEditar:  true,
        mostrarCrear:   true
      }, 'sugerencias' ,true) );

      //Valor
      $('#sugerencias_comentarios').val_(json.result['comentario']);

      //Agregamos los botones al footer
      $('#sugerencias_listarModal_footer').append('<button type="button" class="btn btn-primary" style="display: block; width: 100%; margin: 10px auto;" onclick="sugerencias_edita_confirm(\''+data['sugerencia']+'\');">Editar Sugerencia</button>');
      $('#sugerencias_listarModal_footer').append('<button type="button" class="btn btn-warning" style="display: block; width: 100%; margin: 10px auto;" onclick="sugerencias_listar();">Volver</button>');

    }
  });
}
{/if}

{if="$GLOBALS['permisos']['sugerencias_listar']"}
function sugerencias_edita_confirm(id){
  var data = {};
  data['sugerencia'] = id;
  data['comentarios'] = $('#sugerencias_comentarios').val_();
  
  //Llamada a la API
  apix('sugerencias','editar',data, {
    ok: function(json){
      sugerencias_listar();
    }
  });
}
{/if}

{if="$GLOBALS['permisos']['sugerencias_listar']"}
function sugerencias_elimina(id){
  var data = {};
  data['sugerencia'] = id;
  
  //Llamada a la API
  apix('sugerencias','eliminar',data, {
    ok: function(json){
      sugerencias_listar();
    }
  });
}
{/if}

</script>

