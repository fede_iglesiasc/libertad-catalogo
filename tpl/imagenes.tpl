<script type="text/javascript">

{if="$GLOBALS['permisos']['articulos_edicion_imagenes']"}

//Lista imágenes
function imagenes_listar(id){

	//Creamos el popup
	abrir_modal('imagenes', 600, false);

	//Mostramos la ventana
	$('#imagenesModal').modal({show:true});

	//Titulo...
	$('#imagenesModal_titulo').html('Imágenes');

	//Cargando...
	$('#imagenesModal_container').html( cargando() );

	//Footer...
	$('#imagenesModal_footer').html('');

	//API
	apix('imagenes','listar',{articulo: id}, {
		ok: function(json){

			//Guardamos id y codigo
			$('#imagenesModal').data('articulo',id);
			$('#imagenesModal').data('codigo', json.result.codigo);
			$('#imagenesModal').data('pendiente_w', json.result.pendiente_w);
			$('#imagenesModal').data('pendiente_h', json.result.pendiente_h);

			//Contenido
			var contenido = $('<div class="scrollable" style="max-height: 370px; min-height: 100px; overflow-y: hidden;"></div>');

			var ul = $('<div class="row galeria_grid"></div>');
			for(var i = 0; i < json.result.cantidad; i++){
				var tile = ('<div class="col-md-6 well span2 galeria_tile" onmouseover="$(this).find(\'button\').show();" onmouseout="$(this).find(\'button\').hide();"><span class="helper"></span><img src="./articulos/thumbs/'+json.result.codigo+'-'+i+'.jpg?'+Math.random()+'"/><button type="button" class="btn btn-danger btn-xs" onclick="imagenes_eliminar('+i+');">Eliminar</button></div>');

				ul.append(tile);
			}

			//Si no tiene Imágenes
			if(!json.result.cantidad)
				ul.append('<p style="text-align: center; color: silver;">Éste Artículo aún no tiene Imágenes</p>');

			//Galería
			contenido.append(ul);

			//Input para subir imágen
			contenido.append('<input type="file" id="imagenes_upload_input" name="archivo" style="display: none;"/>');

			//Al seleccionar...
			$('#imagenes_upload_input',contenido).on('change', function(){

				//Si seleccionamos archivo
				if($(this).val() != ''){
					var file = $(this).prop('files')[0];
				    var articulo = $('#imagenesModal').data('articulo');

				    //API
				    var data = new FormData();
				    data.append('articulo', articulo);
				    data.append('imagen', file);
					apix('imagenes','upload',data, {
					  file: true,
					  ok: function(json){
					  	//cargamos el layout para editar imagen
						$('#imagenesModal').data('pendiente_w', json.result.pendiente_w);
						$('#imagenesModal').data('pendiente_h', json.result.pendiente_h);
					  	imagenes_pendiente(json.result.codigo);
					  }
					});

					//Volvemos a vaciarlo
					$(this).val('');
			   	}
			});

			//Agregamos los tooltips
			//tabla.tooltip({selector: '[data-toggle="tooltip"]', placement: 'left', animation: false});

			//Generamos el listado
			$('#imagenesModal_container').html('');
			$('#imagenesModal_container').append(contenido);

			if(json.result.cantidad)
			$(".galeria_grid").sortable({
		        tolerance: 'pointer',
		        revert: 'invalid',
		        placeholder: 'span2 well placeholder galeria_tile',
		        forceHelperSize: true,
				start: function(e, ui) {
					//Creamos un atributo en el elemento con el indice
					$(this).attr('data-previndex', ui.item.index());
					},
				update: function(e, ui) {
					//Obtenemos el nuevo indice
					//y Removemos el atributo temporal
					var newIndex = ui.item.index();
					var oldIndex = $(this).attr('data-previndex');
					$(this).removeAttr('data-previndex');

					//API
					var data = {};
					data['articulo'] = $('#imagenesModal').data('articulo');
					data['posicion_anterior'] = oldIndex;
					data['posicion_nueva'] = newIndex;
					apix('imagenes','posicion',data, {
						ok: function(json){
							imagenes_listar(json.result['articulo']);
						}
					});	
				}
		    });

			//Agregamos scrollbars
			$('#imagenesModal_container .scrollable').mCustomScrollbar({theme: 'minimal-dark',scrollInertia: 100, mouseWheel:{ scrollAmount: 70 }});

			//Subir Imágen
			if(!json.result.pendiente)
				$('#imagenesModal_footer').append('<button type="button" class="btn btn-primary" style="display: block; width: 100%; margin: 10px auto;" onclick="$(\'#imagenes_upload_input\').click();">Agregar nueva imágen</button>');

			//Confirmar Imágen Pendiente
			if(json.result.pendiente)
				$('#imagenesModal_footer').append('<button type="button" class="btn btn-success" style="display: block; width: 100%; margin: 10px auto;" onclick="imagenes_pendiente('+json.result.codigo+');">Revisar Imágen pendiente</button>');

			//Append Cancel Button
			$('#imagenesModal_footer').append('<button type="button" data-dismiss="modal" class="btn btn-warning" style="display: block; width: 100%; margin: 10px auto;">Cerrar</button>');
		}
	});
}

//Imagen pendiente
function imagenes_pendiente(codigo){

	console.log('abrimos pendiente');

	//Creamos el popup
	abrir_modal('imagenes', 600, false);

	//Titulo...
	$('#imagenesModal_titulo').html('Editar Imágen');

	var contenido = $('<img id="imagenes_crop_preview" class="imagenes_crop_preview" src="./upload_tmp/'+$('#imagenesModal').data('codigo')+'.jpg?'+Math.random()+'" />');

	//Cargando...
	$('#imagenesModal_container').html(contenido);

	//Por defecto la imagen va completa
	$('#imagenes_crop_preview').data('x',0);
	$('#imagenes_crop_preview').data('y',0);
	$('#imagenes_crop_preview').data('h',$('#imagenesModal').data('pendiente_h'));
	$('#imagenes_crop_preview').data('w',$('#imagenesModal').data('pendiente_w'));

	//Inicializamos el Jcrop
	$('#imagenes_crop_preview').Jcrop({
		trueSize: 	[	$('#imagenesModal').data('pendiente_w'),
						$('#imagenesModal').data('pendiente_h')
					],
		onChange: 	function(c){
			$('#imagenes_crop_preview').data('x',parseInt(c.x));
			$('#imagenes_crop_preview').data('y',parseInt(c.y));
			$('#imagenes_crop_preview').data('h',parseInt(c.h));
			$('#imagenes_crop_preview').data('w',parseInt(c.w));
		},
		onSelect: 	function(c){
			$('#imagenes_crop_preview').data('x',parseInt(c.x));
			$('#imagenes_crop_preview').data('y',parseInt(c.y));
			$('#imagenes_crop_preview').data('h',parseInt(c.h));
			$('#imagenes_crop_preview').data('w',parseInt(c.w));
		}
	});

	//Footer...
	$('#imagenesModal_footer').html('');

	//Append Cancel Button
	$('#imagenesModal_footer').append('<button type="button" class="btn btn-warning" style="display: inline-block; width: 170px; margin: 0px;" onclick="imagenes_listar('+ $('#imagenesModal').data('articulo') +');">Volver</button>');

	//Descartar Imágen Pendiente
	$('#imagenesModal_footer').append('<button type="button" class="btn btn-danger" style="display: inline-block; width: 170px; margin: 10px;" onclick="imagenes_pendiente_descartar();">Descartar Imágen</button>');

	//Confirmar Imágen Pendiente
	$('#imagenesModal_footer').append('<button type="button" class="btn btn-primary" style="display: inline-block; width: 170px; margin: 0px;" onclick="imagenes_pendiente_confirmar();">Confirmar Imágen</button>');
}

//Descartar Imagen pendiente
function imagenes_pendiente_descartar(){
	//API
	var data = {};
	data['articulo'] = $('#imagenesModal').data('articulo');
	apix('imagenes','pendiente_eliminar',data, {
		ok: function(json){
			imagenes_listar($('#imagenesModal').data('articulo'));
		}
	});
}

//Confirmar Imagen pendiente
function imagenes_pendiente_confirmar(){
	//API
	var data = {};
	data['articulo'] = $('#imagenesModal').data('articulo');
	data['x'] = $('#imagenes_crop_preview').data('x');
	data['y'] = $('#imagenes_crop_preview').data('y');
	data['h'] = $('#imagenes_crop_preview').data('h');
	data['w'] = $('#imagenes_crop_preview').data('w');

	apix('imagenes','pendiente_confirmar',data, {
		ok: function(json){
			imagenes_listar($('#imagenesModal').data('articulo'));
		}
	});
}

//Confirmar Imagen pendiente
function imagenes_eliminar(id){
	//API
	var data = {};
	data['articulo'] = $('#imagenesModal').data('articulo');
	data['imagen'] = id;
	apix('imagenes','eliminar',data, {
		ok: function(json){
			imagenes_listar($('#imagenesModal').data('articulo'));
		}
	});
}

//Al cerrar la ventana actualizamos los articulos
function imagenesModal_onClose(){
	watable_volver = true;
	articulosUpdate();
}

{/if}

{if="$GLOBALS['permisos']['imagenes_importar']"}

//Abrir Importador de imagenes
function imagenes_importar(){

  //Creamos el popup
  abrir_modal('imagenes_importar', 450, false);

  //Mostramos la ventana
  $('#imagenes_importarModal').modal({show:true});

  //Titulo...
  $('#imagenes_importarModal_titulo').html('Importar Imagenes de Artículos');
  $('#imagenes_importarModal_footer').html('');

  //Cargando...
  $('#imagenes_importarModal_container').html(cargando());

  //Cargando...
  $('#imagenes_importarModal_container').html('Para importar las imágenes en Batch seleccione un ZIP. </br></br>El nombre de cada imágen deberá tener el siguiente formato: </br></br> <b>prefijo</b>-<b>codigo</b>-<b>sufijo</b>-<b>cantidad</b></br></br>Las extensiones pueden ser: <b>jpg png gif</b><input type="file" id="imagenes_importar_input" name="archivo" style="display: none;">');

  //Boton Confirmar cambios
  $('#imagenes_importarModal_footer').append('<button type="button" class="btn btn-primary" style="display: block; width: 100%; margin: 10px auto;" onclick="$(\'#imagenes_importar_input\').click();">Seleccionar Archivo</button>');

  //Append Cancel Button
  $('#imagenes_importarModal_footer').append('<button type="button" data-dismiss="modal" class="btn btn-warning" style="display: block; width: 100%; margin: 10px auto;">Cancelar Importación</button>');

  //Al seleccionar...
  $('#imagenes_importar_input').on('change', function(){

    if($('#imagenes_importar_input').val() != ''){

      //Recolectamos las variables
      var file = $('#imagenes_importar_input').prop('files')[0];

      //Reseteamos el campo
      $('#imagenes_importar_input').val('');

      //Datos del form
      var data = new FormData();
      data.append('archivo', file);

      //Indicamos al usuario que el sistema ahora va a estar ocupado..
      $('#imagenes_importarModal_titulo').html('Subiendo Archivo...');
      $('#imagenes_importarModal_container').html(cargando());
      $('#imagenes_importarModal_footer').html('');

      //API
      apix('imagenes','importar',data, {
        file: true,
        debug: true,
        ok: function(json){

          //Titulo...
          $('#imagenes_importarModal_titulo').html('Imagenes Importadas');

          //Mensaje
          $('#imagenes_importarModal_container').html('Las imagenes para los articulos fueron importadas y actualizadas con exito!');

          //Si existstieron corruptas
          if(json.result.corruptas.length){
          	$('#imagenes_importarModal_container').append('</br> Las siguientes imagenes estaban defectuosas o corruptas <ul>');
          	for(var i in json.result.corruptas) 
          		$('#imagenes_importarModal_container').append('<li>'+json.result.corruptas[i]+'</li>');
          	$('#imagenes_importarModal_container').append('</ul>');
          }

          //Revisamos todas las operaciones
          var count = 0;
          for(var i in json.result.operaciones)
          	if(!json.result.operaciones[i].thumb || !json.result.operaciones[i].original || !json.result.operaciones[i].watermark){
          		if(!count) $('#imagenes_importarModal_container').append('<br><br>Algunas imagenes tuvieron errores al ser procesadas<br><ul>');
          		$('#imagenes_importarModal_container').append('<li>'+i+'</li>');
          		count++;
          	}

          //Finalizamos
          if(count) $('#imagenes_importarModal_container').append('</ul>');

          //Append Cancel Button
          $('#imagenes_importarModal_footer').append('<button type="button" data-dismiss="modal" class="btn btn-warning" style="display: block; width: 100%; margin: 10px auto;">Cerrar</button>');
          	

        },
        error: function(){
          //Volvemos a mostrar el popup
          imagenes_importar();
        }
      });
    }
  });
}

{/if}


{if="$GLOBALS['permisos']['articulos_listar']"}

//Cambia las miniaturas en la galeria del listado de arts.
function imagenes_articulo_cambiaImg(el,id){
	//Referencias a elementos que utilizaremos
	var ul = el.parent();
	var contenedor = el.parent().parent();
	var index = ul.children().index(el);
	var tooltip = ul.parent().parent().parent().siblings('a[rel=tooltip]');

	//Ocultamos imagenes mostramos segun index   
	$('.galeria_archivo_tooltip_imgs img',contenedor).removeClass('active');
	$('.galeria_archivo_tooltip_imgs img:eq('+index+')',contenedor).addClass('active');

	//Updateamos el tooltip
	tooltip.tooltip('updatePlacement');
	//Desactivamos li's
	el.siblings().removeClass('active');
	//Activamos este
	el.addClass('active');
}

//Abre la Galeria de imagenes en el listado de arts.
function imagenes_articulo_abreGaleria(index,id){

	//Ocultamos tooltip
	$('#icono_fotos_'+id).tooltip('hide');

	//Creamos el arreglo de imagenes
	var fotosDB = resultados.data.rows[id].fotos;
	var imgs = Array();
	for(var i in fotosDB)
	  for (var z = 0; z < fotosDB[i].cant; z++)
	    imgs.push({ "href": './articulos/'+fotosDB[i].cod + '-' + z + '.jpg' });


	//Mostramos la galeria
	$.fancybox.open(imgs, {
	    padding : 0,
	    index: index,
	    openSpeed: 'fast', 
	    closeSpeed: 'fast',
	    nextSpeed: 'fast',
	    prevSpeed: 'fast',
	    wrapCSS    : 'fancybox-custom',
	    closeClick : true,
	    openEffect : 'none',
	    padding: 20,
	    helpers : {
	                title : {
	                  type : 'inside'
	                }
	    }
	});
}

{/if}


</script>