<script type="text/javascript">

{if="$GLOBALS['permisos']['clientes_editar'] || $GLOBALS['permisos']['clientes_crear'] || $GLOBALS['permisos']['clientes_listar'] || $GLOBALS['permisos']['clientes_eliminar']"}
//Config Clientes
var clientes_config = {
	modName: 					'clientes',
	modLabelSingular: 			'Cliente',
	modLabelPlural: 			'Clientes',
	creaLabel: 					'Agregado',
	editaLabel: 				'Editado',
	eliminaLabel: 				'Eliminado',
	nuevoLabel: 				'Nuevo',
	artLabel: 					'El',
	max_height_edita_agrega: 	'500',
	max_height_lista: 			'400',
	campos:[
		{	
			id: 			"cliente",
			label: 			"Cliente",
			tipo: 			"input",
			mostrarEditar: 	false,
			mostrarCrear: 	true
		},
		{	
			id: 			"cuit",
			label: 			"CUIT",
			tipo: 			"input",
			mostrarEditar: 	true,
			mostrarCrear: 	true
		},
		{	
			id: 			"nombre_del_comercio",
			label: 			"Nombre del Comercio",
			tipo: 			"input",
			mostrarEditar: 	true,
			mostrarCrear: 	true
		},
		{	
			id: 			"razon_social",
			label: 			"Razón Social",
			tipo: 			"input",
			mostrarEditar: 	true,
			mostrarCrear: 	true
		},
		{	
			id: 			"email",
			label: 			"Email",
			tipo: 			"input",
			mostrarEditar: 	true,
			mostrarCrear: 	true
		},
		{	
			id: 			"persona_de_contacto",
			label: 			"Persona de contacto",
			tipo: 			"input",
			mostrarEditar: 	true,
			mostrarCrear: 	true
		},
		{	
			id: 			"telefono",
			label: 			"Teléfono",
			tipo: 			"input",
			mostrarEditar: 	true,
			mostrarCrear: 	true
		},
		{	
			id: 			"domicilio",
			label: 			"Domicilio",
			tipo: 			"input",
			mostrarEditar: 	true,
			mostrarCrear: 	true
		},
		{	
			id: 			"localidad",
			label: 			"Localidad",
			tipo: 			"select2",
			mostrarEditar: 	true,
			mostrarCrear: 	true,
			config: 		{
								minimumInputLength: 0,
								placeholder: $(this).attr('placeholder'),
								allowClear: false,
								width: 'resolve',
								id: function(e) { return e.id; },
								formatNoMatches: function() { return 'Sin resultados'; },
								formatSearching: function(){ return "Buscando..."; },
								ajax: {
								    url: "api.php",
								    dataType: 'json',
								    quietMillis: 200,
								    data: function(term, page) {
								        return {
								            module: 'localidades',
								            run: 'listar_select',
								            q: term,
								            p: page
								        };
								    },
								    results: function(data, page ) {
								        //Si el ususario no esta logueado refresh
								        if(!data.logued) location.reload();

								        //Calcula si existen mas resultados a mostrar
								        var more = (page * 40) < data.result.total;

								        //Devolvemos el valor de more para que selec2
								        //sepa que debemos cargar mas resultados.
								        return {results: data.result.items, more: more};
								    }
								},
								formatInputTooShort: function () {
									return "";
								},
								formatResult: function(item) {
									return item.nombre;
								},
								formatSelection: function(item) { 
									return item.nombre;
								},
								initSelection : function (element, callback) {
									var elementText = $(element).attr('data-init-text');
								}
							}
		},
		{	
			id: 			"vendedor",
			label: 			"Vendedor",
			tipo: 			"select2",
			mostrarEditar: 	true,
			mostrarCrear: 	true,
			config: 		{
								minimumInputLength: 0,
								placeholder: $(this).attr('placeholder'),
								allowClear: false,
								width: 'resolve',
								id: function(e) { return e.id; },
								formatNoMatches: function() { return 'Sin resultados'; },
								formatSearching: function(){ return "Buscando..."; },
								ajax: {
								    url: "api.php",
								    dataType: 'json',
								    quietMillis: 200,
								    data: function(term, page) {
								        return {
								            module: 'clientes',
								            run: 'listar_vendedores',
								            q: term,
								            p: page
								        };
								    },
								    results: function(data, page ) {
								        //Si el ususario no esta logueado refresh
								        if(!data.logued) location.reload();

								        //Calcula si existen mas resultados a mostrar
								        var more = (page * 40) < data.result.total;

								        //Devolvemos el valor de more para que selec2
								        //sepa que debemos cargar mas resultados.
								        return {results: data.result.items, more: more};
								    }
								},
								formatInputTooShort: function () {
									return "";
								},
								formatResult: function(item) {
									return item.nombre;
								},
								formatSelection: function(item) { 
									return item.nombre;
								},
								initSelection : function (element, callback) {
									var elementText = $(element).attr('data-init-text');
								}
							}
		},
		{	
			id: 			"password",
			label: 			"Contraseña",
			tipo: 			"input",
			mostrarEditar: 	true,
			mostrarCrear: 	true
		},
		{	
			id: 			"activo",
			label: 			"Activo",
			tipo: 			"checkbox",
			mostrarEditar: 	true,
			mostrarCrear: 	true,
			config: 		{
							claseActivo: 'btn-success',
							claseInactivo: 'btn-danger',
							valueActivo: '1',
							valueInactivo: '0',
							valueDefault: '1',
							labelActivo: function(el){
								el.html('El Usuario está activo, haga click para desactivarlo');
							},
							labelInactivo: function(el){
								el.html('El Usuario está inactivo, haga click para activarlo');
							}
			}
		}
	]
};
{/if}

//Lista
{if="$GLOBALS['permisos']['clientes_listar']"}
function clientes_listar(){

	//Creamos el popup
	abrir_modal(clientes_config.modName, 650, false);

	//Mostramos la ventana
	$('#'+clientes_config.modName+'Modal').modal({show:true});

	//Titulo...
	$('#'+clientes_config.modName+'Modal_titulo').html(clientes_config.modLabelPlural);

	//Cargando...
	$('#'+clientes_config.modName+'Modal_container').html( cargando() );

	//Footer...
	$('#'+clientes_config.modName+'Modal_footer').html('');

	//Traemos los datos de la API
	apix(clientes_config.modName,'listar',{}, {
		ok: function(json){

			var tabla = generaTabla({
							id_tabla: clientes_config.modName,
							items: json.result,
							ajaxModulo: 'clientes',
							ajaxAccion: 'listar',
							keys_busqueda: ['id','grupo','accion'],
							key_nombre: null,
							max_height: clientes_config.max_height_lista,
							funcion_activado: function(el){

								//Seleccionamos el activo anterior
								var elActivo = $('#'+clientes_config.modName+'_lista').find('.active_');
								var id = $(el).data('id');

								
								//Boton Descuentos
								var boton_descuentos = '<button class="btn btn-xs btn-primary" style="margin: 0px 0px 8px 5px; width: 24px; height: 24px;" data-toggle="tooltip" title="Descuentos" onclick="clientes_descuentos_lista(\''+$(el).data('usuario')+'\');"><i class="fa fa-usd"></i></button>';

								//Boton Borrar
								var boton_borrar = '<button class="btn btn-xs btn-warning" style="margin: 0px 0px 8px 5px; width: 24px; height: 24px;" data-toggle="tooltip" title="Borrar" onclick=" confirma(\'Eliminar '+clientes_config.modLabelSingular+'\',\'El '+clientes_config.modLabelSingular+' se eliminará de forma permanente.\',\'exclamacion\',\'danger\',\''+clientes_config.modLabelPlural.toLowerCase()+'_elimina(\\\''+$(el).data('usuario')+'\\\')\');"><i class="fa fa-remove"></i></button>';

								//Boton editar
								var boton_editar = '<button class="btn btn-xs btn-primary" style="margin: 0px 0px 8px 5px; width: 24px; height: 24px;" data-toggle="tooltip" title="Editar" onclick="'+clientes_config.modLabelPlural.toLowerCase()+'_edita_agrega(\''+$(el).data('usuario')+'\');"><i class="fa fa-pencil"></i></button>';

								//Activo o no?
								var activo = parseInt($(el).data('activo'));
								var boton_activo = '<button class="btn btn-xs btn-' + ( activo==1 ? 'success' : 'danger' ) + '" style="height: 24px; padding: 0 10px;"><b>' + ( activo==1 ? 'Activo' : 'Inactivo' ) + '</b></button>';

								
								//Agregamos los datos
								var html = '<b>'+ $(el).data('usuario') + '</b> - ' + $(el).data('nombre_del_comercio') + '<div class="descripcion"><h5>'+$(el).data('razon_social')+'</h5><strong><small>CUIT</small></strong> &nbsp;'+$(el).data('cuit')+'<br/><br/><i class="fa fa-phone-square"></i> &nbsp;'+$(el).data('telefono')+'</br><i class="fa fa-home"></i> &nbsp;'+$(el).data('domicilio')+'<br/>&nbsp;<i class="fa fa-map-marker"></i> &nbsp;'+$(el).data('localidad_label')+'</br></br>&nbsp;<i class="fa fa-user"></i> &nbsp;'+$(el).data('persona_de_contacto')+'</br><i class="fa fa-envelope"></i> &nbsp;<a href="mailto:'+$(el).data('email')+'">'+$(el).data('email')+'</a></div><div class="pull-right descripcion" style="width: 120px; text-align: right; position: absolute; top: 8px; right: 10px;">';


								{if="$GLOBALS['permisos']['descuentos_clientes_administrar']"}
									//Agregams el boton descuentos
									html += boton_descuentos;
								{/if}

								

								{if="$GLOBALS['permisos']['clientes_editar']"}
									//Agregams el boton editar
									html += boton_editar;
								{/if}

								{if="$GLOBALS['permisos']['clientes_eliminar']"}
									//Agregams el boton borrar
									html += boton_borrar;
								{/if}

								//Final del item activo
								html += '<div>'+boton_activo+'</div></div>';

								//Agergamos el html
								$(el).html(html);
							},
							funcion_desactivado: function(el){
								var html = '<b>'+ $(el).data('usuario') + '</b> - ' + $(el).data('nombre_del_comercio');
								if( $(el).data('activo') == '-1')
									html += '<span class="label label-warning" style="padding: 6px 7px; font-size: 10px; display: inline-block; float: right;">Pendiente de Aprobación</span>';

								if( $(el).data('estado') == '1')
									html += '<span class="label label-success" style="padding: 6px 7px; font-size: 10px; display: inline-block; float: right;">ONLINE</span>';
								
								$(el).html(html);
							}
						});

			//Agregamos los tooltips
			tabla.tooltip({selector: '[data-toggle="tooltip"]', placement: 'left', animation: false});

			//Generamos el listado
			$('#'+clientes_config.modName+'Modal_container').html('');
			$('#'+clientes_config.modName+'Modal_container').append(tabla);


			//Agregamos scrollbars
			$('#'+clientes_config.modName+'Modal_container .scrollable').mCustomScrollbar({theme: 'minimal-dark',scrollInertia: 100, mouseWheel:{ scrollAmount: 70 }});

			{if="$GLOBALS['permisos']['clientes_crear']"}
			//Agregar
			$('#'+clientes_config.modName+'Modal_footer').append('<button type="button" class="btn btn-primary" style="display: block; width: 100%; margin: 10px auto;" onclick="'+clientes_config.modName+'_edita_agrega();">'+clientes_config.nuevoLabel+' '+clientes_config.modLabelSingular+'</button>');
			{/if}

			//Append Cancel Button
			$('#'+clientes_config.modName+'Modal_footer').append('<button type="button" data-dismiss="modal" class="btn btn-warning" style="display: block; width: 100%; margin: 10px auto;">Cerrar</button>');
		}
	});
}
{/if}

{if="$GLOBALS['permisos']['clientes_editar'] || $GLOBALS['permisos']['clientes_crear']"}
//Edita/Agrega
function clientes_edita_agrega(id){

	//Si estamos agregando...
	if(typeof id === 'undefined') var agrega = true;
	else var agrega = false;

	//Titulo...
	if(agrega) $('#'+clientes_config.modName+'Modal_titulo').html(clientes_config.nuevoLabel+' '+clientes_config.modLabelSingular);
	else $('#'+clientes_config.modName+'Modal_titulo').html('Editar '+clientes_config.modLabelSingular);

	//Cargando...
	$('#'+clientes_config.modName+'Modal_container').html(cargando());

	//Footer...
	$('#'+clientes_config.modName+'Modal_footer').html('');

	//Contenido
	var contenido = $('<div class="scrollable" style="max-height: '+clientes_config.max_height_edita_agrega+'px; overflow-y: hidden;"></div>'); 

	//Agregamos todos los campos
	for(var k in clientes_config.campos){
		var campo = clientes_config.campos[k];
		if( (campo.mostrarEditar && !agrega) || (campo.mostrarCrear && agrega)){
			//Agregamos el campo
			contenido.append( genera_campo(clientes_config.campos[k], clientes_config.modName ,true) );
		}
	}

	//Agregamos evento Blur en CUIT
	$('#clientes_cuit',contenido).on('blur', function(){
		$('#clientes_razon_social').val('cargando...').attr('disabled','disabled');
		//Llamada a la API
		var data = {};
		data['cuit'] = $(this).val();
		apix('clientes','info_razon_social',data, {
            ok: function(json){
				$('#clientes_razon_social').val_(json.result);
				$('#clientes_razon_social').removeAttr('disabled');
			}
		});
	});

	//Agregamos el contenido y los botones
	//si estamos creando un registro nuevo
	if(agrega){
		//Agregamos el contenido
		$('#'+clientes_config.modName+'Modal_container').html(contenido);

		//Agregamos scrollbars
		$('#'+clientes_config.modName+'Modal_container .scrollable').mCustomScrollbar({theme: 'minimal-dark',scrollInertia: 100, mouseWheel:{ scrollAmount: 70 }});

		//Agregamos los botones al footer
		$('#'+clientes_config.modName+'Modal_footer').append('<button type="button" class="btn btn-primary" style="display: block; width: 100%; margin: 10px auto;" onclick="'+clientes_config.modLabelPlural.toLowerCase()+'_agrega_confirm();">Agregar '+clientes_config.modLabelSingular+'</button>');
		$('#'+clientes_config.modName+'Modal_footer').append('<button type="button" class="btn btn-warning" style="display: block; width: 100%; margin: 10px auto;" onclick="'+clientes_config.modLabelPlural.toLowerCase()+'_listar();">Volver</button>');
	}


	//Si editamos
	if(!agrega){
		var data = {};
		data['cliente'] = id;

		apix(clientes_config.modName,'info',data, {
            ok: function(json){

				//Titulo
				$('#'+clientes_config.modName+'Modal_titulo').html('Editar '+clientes_config.modLabelSingular);

		    	//Llenamos los datos
		    	for(var k in clientes_config.campos){
		    		var campo = clientes_config.campos[k];
		    		if((campo.tipo == 'input') || (campo.tipo == 'checkbox'))
			    		if( !(typeof(json.result[ campo.id ]) == "undefined") && !(json.result[ campo.id ] === null) ){
			    			$('#'+clientes_config.modName+'_'+campo.id,contenido).val_(json.result[ campo.id ]);
			    		}
		    	}

		    	if(json.result.localidad != null)
		    		$('#clientes_vendedor',contenido).val_({ id: json.result.vendedor, nombre: json.result.vendedor_label });

		    	//Creamos el campo especial para popular el S2 de Fabricantes / Codigo
		    	if(json.result.localidad != null)
		    		$('#clientes_localidad',contenido).val_({ id: json.result.localidad, nombre: json.result.localidad_label });

		    	//Asignamos el contenido al popup
				$('#'+clientes_config.modName+'Modal_container').html(contenido);

				//Agregamos scrollbars
				$('#'+clientes_config.modName+'Modal_container .scrollable').mCustomScrollbar({theme: 'minimal-dark',scrollInertia: 100, mouseWheel:{ scrollAmount: 70 }});

				//Agregamos los botones al footer
				$('#'+clientes_config.modName+'Modal_footer').append('<button type="button" class="btn btn-primary" style="display: block; width: 100%; margin: 10px auto;" onclick="'+clientes_config.modLabelPlural.toLowerCase()+'_edita_confirm(\''+id+'\');">Editar '+clientes_config.modLabelSingular+'</button>');
				$('#'+clientes_config.modName+'Modal_footer').append('<button type="button" class="btn btn-warning" style="display: block; width: 100%; margin: 10px auto;" onclick="'+clientes_config.modLabelPlural.toLowerCase()+'_listar();">Volver</button>');
			}
		});
	}
}
{/if}

{if="$GLOBALS['permisos']['clientes_editar']"}
//Edita
function clientes_edita_confirm(id){

	//Data
	var data = {};
	data['cliente'] = id;

	//Agregamos todos los campos
	for(var k in clientes_config.campos){
		var campo = clientes_config.campos[k];
		if(campo.type = 'input')
			if(campo.mostrarEditar)
				data[campo.id] = $('#'+clientes_config.modName+'_'+campo.id).val();
	}

	apix(clientes_config.modName,'editar',data, {
		ok: function(json){
			notifica(	clientes_config.modLabelSingular+' '+clientes_config.editaLabel.toLowerCase(),
						clientes_config.artLabel+' '+clientes_config.modLabelSingular+' se editó con éxito',
						'confirmacion','success');
			clientes_listar();
		}
	});
}
{/if}
// (0223) 473-0458
{if="$GLOBALS['permisos']['clientes_crear']"}
//Agrega
function clientes_agrega_confirm(){

	//Data
	var data = {};

	//Agregamos todos los campos
	for(var k in clientes_config.campos){
		var campo = clientes_config.campos[k];
		if(campo.mostrarCrear){
			if((campo.tipo == 'input') || (campo.tipo == 'checkbox'))
				data[campo.id] = $('#'+clientes_config.modName+'_'+campo.id).val();
		}
	}

	//Agregamos Codigo
	var localidad = $('#clientes_localidad').select2('data');
	if(localidad != null){ 
		data['localidad'] = localidad.id;
	}

	apix(clientes_config.modName,'agregar',data, {
		ok: function(json){
			//Notifica
			notifica(	clientes_config.modLabelSingular+' '+clientes_config.creaLabel.toLowerCase(),
						clientes_config.artLabel+' '+clientes_config.modLabelSingular+' se creó con éxito',
						'confirmacion',
						'success'
					);
			//Update
			clientes_listar();
		}
	});
}
{/if}

{if="$GLOBALS['permisos']['clientes_eliminar']"}
//Elimina
function clientes_elimina(id){

	//Data array
	var data = {};
	data['cliente'] = id;
	
	apix(clientes_config.modName,'eliminar',data, {
            ok: function(json){
				//Notifica
				notifica(	clientes_config.modLabelSingular+' '+clientes_config.eliminaLabel,
							clientes_config.artLabel+' '+clientes_config.modLabelSingular+' se eliminó con éxito',
							'informacion',
							'info'
						);
				//Update
				clientes_listar();
		}
	});
}
{/if}


</script>