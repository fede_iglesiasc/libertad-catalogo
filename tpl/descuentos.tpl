<script type="text/javascript">

{if="$GLOBALS['permisos']['descuentos_articulos_administrar']"}
//Lista x Articulo
function descuentos_articulo_listar(id,cod){

	//Creamos el popup
	abrir_modal('descuentosArticulo', 450, false);

	//Mostramos la ventana
	$('#descuentosArticuloModal').modal({show:true});

	//Titulo
	$('#descuentosArticuloModal_titulo').html('Descuentos del Articulo '+cod);

	//Cargando...
	$('#descuentosArticuloModal_container').html(cargando());

	//Footer...
	$('#descuentosArticuloModal_footer').html('');

	//Contenido
	var contenido = $('<div class="scrollable" style="max-height: '+clientes_config.max_height_edita_agrega+'px; overflow-y: hidden;"></div>');

	//Data array
	var data = {};
	data['articulo'] = id;
	
	apix('descuentos','listar_x_articulo',data, {
        ok: function(json){

			var tabla = generaTabla({
							id_tabla: 'descuentos_lista',
							items: json.result,
							ajaxModulo: 'clientes',
							ajaxAccion: 'listar',
							campobusqueda: false,
							keys_busqueda: ['id','grupo','accion'],
							key_nombre: null,
							sinResultados_label: 'No existen descuentos para este Articulo',
							max_height: 300,
							funcion_activado: function(el){

								//Seleccionamos el activo anterior
								var elActivo = $('#descuentos_lista').find('.active_');

								//Boton Borrar
								var boton_borrar = '<button class="btn btn-xs btn-warning" style="margin: 0px 0px 8px 5px; width: 24px; height: 24px;" data-toggle="tooltip" title="Borrar" onclick=" confirma(\'Eliminar Descuento\',\'El descuento se eliminará de forma permanente.\',\'exclamacion\',\'danger\',\'descuentos_articulo_elimina(\\\''+$(el).data('id')+'\\\',\\\''+$(el).data('codigo')+'\\\',\\\''+$(el).data('letra')+'\\\')\');"><i class="fa fa-remove"></i></button>';

								//Boton editar
								//descuentos_articulo_edita_agrega(id, cod, letra, descuento)
								var boton_editar = '<button class="btn btn-xs btn-primary" style="margin: 0px 0px 8px 5px; width: 24px; height: 24px;" data-toggle="tooltip" title="Editar" onclick="descuentos_articulo_edita_agrega(\''+$(el).data('id')+'\', \''+$(el).data('codigo')+'\', \''+$(el).data('letra')+'\', \''+$(el).data('descuento')+'\');"><i class="fa fa-pencil"></i></button>';

								
								//Agregamos los datos
								var html = '<b>Letra ' + $(el).data('letra') + ': </b>  ' + $(el).data('descuento') + '% de descuento <div class="pull-right descripcion" style="width: 120px; text-align: right; position: absolute; top: 8px; right: 10px;">' + boton_editar + boton_borrar +'</div>';


								//Agergamos el html
								$(el).html(html);
							},
							funcion_desactivado: function(el){
								var html = '<b>Letra ' + $(el).data('letra') + ': </b>  ' + $(el).data('descuento') + '% de descuento';								
								$(el).html(html);
							}
						});

			//Agregamos los tooltips
			tabla.tooltip({selector: '[data-toggle="tooltip"]', placement: 'left', animation: false});

			//Generamos el listado
			$('#descuentosArticuloModal_container').html('');

	    	//Asignamos el contenido al popup
			$('#descuentosArticuloModal_container').html(contenido);
			$('#descuentosArticuloModal_container').append(tabla);

			//Agregamos scrollbars
			$('#descuentosArticuloModal_container .scrollable').mCustomScrollbar({theme: 'minimal-dark',scrollInertia: 100, mouseWheel:{ scrollAmount: 70 }});

			//Agregamos los botones al footer
			$('#descuentosArticuloModal_footer').append('<button type="button" class="btn btn-primary" style="display: block; width: 100%; margin: 10px auto;" onclick="descuentos_articulo_edita_agrega(\''+id+'\');">Agregar Descuento</button>');

			//Cerrar
			$('#descuentosArticuloModal_footer').append('<button type="button" data-dismiss="modal" class="btn btn-warning" style="display: block; width: 100%; margin: 10px auto;">Cerrar</button>');
		}
	});
}

//Edita / Agrega x Articulo
function descuentos_articulo_edita_agrega(articulo, codigo, letra, descuento){

	var modo = 'edita';
	if((typeof letra === 'undefined') && (typeof descuento === 'undefined'))
		modo = 'agrega';

	//Titulo
	if(modo == 'edita') $('#descuentosArticuloModal_titulo').html('Editando descuentos del Art. '+codigo);
	else $('#descuentosArticuloModal_titulo').html('Agregar descuento al Art. '+codigo);

	//Cargando...
	$('#descuentosArticuloModal_container').html(cargando());

	//Footer...
	$('#descuentosArticuloModal_footer').html('');

	//Contenido
	var contenido = $('<div class="scrollable" style="max-height: '+clientes_config.max_height_edita_agrega+'px; overflow-y: hidden;"></div>');

	//Datos del articulo
	contenido.append('<input type="hidden" id="descuentos_articulo_articulo" value="'+articulo+'">');
	contenido.append('<input type="hidden" id="descuentos_articulo_codigo" value="'+codigo+'">');

	//input Letra
	if(modo == 'agrega')
		contenido.append( genera_campo({	
				id: 			"letra",
				label: 			"Letra",
				tipo: 			"input",
				mostrarEditar: 	true,
				mostrarCrear: 	true
			}, 'descuentos_articulo' ,true) );
	if(modo == 'edita') contenido.append('<input type="hidden" id="descuentos_articulo_letra" value="'+letra+'">');

	//Input Descuento
	var label_input = "Descuento";
	if(modo == 'edita') label_input = label_input + " " + letra;
	contenido.append( genera_campo({	
			id: 			"descuento",
			label: 			label_input,
			tipo: 			"input",
			mostrarEditar: 	true,
			mostrarCrear: 	true
		}, 'descuentos_articulo' ,true) );

	//Agregamos el valor del descuento
	if(modo == 'edita') $('#descuentos_articulo_descuento',contenido).val_(descuento);

	//Contenido
	$('#descuentosArticuloModal_container').html(contenido);

	//Scrollbars
	$('#descuentosArticuloModal_container .scrollable').mCustomScrollbar({theme: 'minimal-dark',scrollInertia: 100, mouseWheel:{ scrollAmount: 70 }});

	//Agregar
	if(modo == 'agrega') $('#descuentosArticuloModal_footer').append('<button type="button" class="btn btn-primary" style="display: block; width: 100%; margin: 10px auto;" onclick="descuentos_articulo_agrega_confirm();">Agregar</button>');

	//Editar
	if(modo == 'edita') $('#descuentosArticuloModal_footer').append('<button type="button" class="btn btn-primary" style="display: block; width: 100%; margin: 10px auto;" onclick="descuentos_articulo_edita_confirm(\''+articulo+'\',\''+codigo+'\');">Editar</button>');

	//Volver
	$('#descuentosArticuloModal_footer').append('<button type="button" class="btn btn-warning" style="display: block; width: 100%; margin: 10px auto;" onclick="descuentos_articulo_listar(\''+articulo+'\',\''+codigo+'\');">Volver</button>');
}

//Edita x Articulo
function descuentos_articulo_edita_confirm(){
	
	//Articulo
	var articulo = $('#descuentos_articulo_articulo').val();
	var codigo = $('#descuentos_articulo_codigo').val();

	//Data
	var data = {};
	data['articulo'] = articulo;
	data['letra'] = $('#descuentos_articulo_letra').val();
	data['descuento'] = parseFloat($('#descuentos_articulo_descuento').val()).toFixed(2);
	
	apix('descuentos','editar_x_articulo',data, {
		ok: function(json){
				//Notifica
				notifica(	'Descuento Editar',
							'El descuento se editó con éxito',
							'informacion',
							'info'
						);
				//Update
				descuentos_articulo_listar(articulo,codigo);
		}
	});
}

//Agrega x Articulo
function descuentos_articulo_agrega_confirm(){
	
	//Articulo
	var articulo = $('#descuentos_articulo_articulo').val();
	var codigo = $('#descuentos_articulo_codigo').val();

	//Data
	var data = {};
	data['articulo'] = articulo;
	data['letra'] = $('#descuentos_articulo_letra').val();
	data['descuento'] = parseFloat($('#descuentos_articulo_descuento').val()).toFixed(2);
	
	apix('descuentos','agregar_x_articulo',data, {
		ok: function(json){
				//Notifica
				notifica(	'Agregar Descuento',
							'El descuento se agregó con éxito',
							'informacion',
							'info'
						);
				//Update
				descuentos_articulo_listar(articulo,codigo);
		}
	});
}

//Elimina x Articulo
function descuentos_articulo_elimina(articulo, codigo, letra){
	//Data
	var data = {};
	data['articulo'] = articulo;
	data['letra'] = letra;
	
	apix('descuentos','elimina_x_articulo',data, {
		ok: function(json){
				//Notifica
				notifica(	'Descuento Eliminado',
							'El descuento se eliminó con éxito',
							'informacion',
							'info'
						);
				//Update
				descuentos_articulo_listar(articulo,codigo);
		}
	});
}

{/if}

{if="$GLOBALS['permisos']['descuentos_clientes_administrar']"}
//Lista
function clientes_descuentos_lista(id){

	$('#clientesModal').width(500);

	$('#'+clientes_config.modName+'Modal_titulo').html('Descuentos del Cliente '+id);

	//Cargando...
	$('#'+clientes_config.modName+'Modal_container').html(cargando());

	//Footer...
	$('#'+clientes_config.modName+'Modal_footer').html('');

	//Contenido
	var contenido = $('<div class="scrollable" style="max-height: '+clientes_config.max_height_edita_agrega+'px; overflow-y: hidden;"></div>');


	//Data array
	var data = {};
	data['cliente'] = id;
	
	apix('descuentos','listar_x_cliente',data, {
        ok: function(json){

        	//Condicion de venta
        	contenido.append(
				genera_campo({	
					id: 			"condicion_venta",
					label: 			"Condicion de Venta",
					tipo: 			"select2",
					mostrarEditar: 	true,
					mostrarCrear: 	true,
					config: 		{
										minimumInputLength: 0,
										placeholder: 'Condicion de Venta',
										allowClear: false,
										width: 'resolve',
										id: function(e) { return e.id; },
										formatNoMatches: function() { return 'Sin resultados'; },
										formatSearching: function(){ return "Buscando..."; },
										ajax: {
										    url: "api.php",
										    dataType: 'json',
										    quietMillis: 200,
										    data: function(term, page) {
										        return {
										            module: 'descuentos',
										            run: 'listar_condiciones_venta',
										            q: term,
										            p: page
										        };
										    },
										    results: function(data, page ) {
										        //Si el ususario no esta logueado refresh
										        if(!data.logued) location.reload();

										        //Calcula si existen mas resultados a mostrar
										        var more = (page * 40) < data.result.total;

										        //Devolvemos el valor de more para que selec2
										        //sepa que debemos cargar mas resultados.
										        return {results: data.result.items, more: more};
										    }
										},
										formatInputTooShort: function () {
											return "";
										},
										formatResult: function(item) {
											return item.nombre;
										},
										formatSelection: function(item) { 
											return item.nombre;
										},
										initSelection : function (element, callback) {
											var elementText = $(element).attr('data-init-text');
										}
									}
				}, 'clientes_descuentos' ,true) );
        	$('#clientes_descuentos_condicion_venta',contenido).val_({id:json.result.condicion_venta.id, nombre:json.result.condicion_venta.nombre});


        	//Letra descuentos
        	contenido.append(
				genera_campo({	
					id: 			"descuento_letra",
					label: 			"Letra de Descuento del Articulo",
					tipo: 			"select2",
					mostrarEditar: 	true,
					mostrarCrear: 	true,
					config: 		{
										minimumInputLength: 0,
										placeholder: 'Letra de Descuento del Articulo',
										allowClear: true,
										width: 'resolve',
										id: function(e) { return e.id; },
										formatNoMatches: function() { return 'Sin resultados'; },
										formatSearching: function(){ return "Buscando..."; },
										data: [	{id:'A',nombre:'A'}, 
												{id:'B',nombre:'B'}, 
												{id:'C',nombre:'C'}, 
												{id:'D',nombre:'D'}, 
												{id:'E',nombre:'E'}, 
												{id:'F',nombre:'F'}, 
												{id:'G',nombre:'G'}, 
												{id:'H',nombre:'H'}, 
												{id:'I',nombre:'I'}, 
												{id:'J',nombre:'J'}, 
												{id:'K',nombre:'K'}, 
												{id:'L',nombre:'L'}],
										formatInputTooShort: function () {
											return "";
										},
										formatResult: function(item) {
											return item.nombre;
										},
										formatSelection: function(item) { 
											return item.nombre;
										},
										initSelection : function (element, callback) {
											var elementText = $(element).attr('data-init-text');
										}
									}
				}, 'clientes_descuentos' ,true) );

			//Cuando cambia...
			$('#clientes_descuentos_descuento_letra',contenido).on("change", function(e) {
			    console.log($('#clientes_descuentos_descuento_letra').val_());
			});

			if(json.result.descuento_letra != null)
        		$('#clientes_descuentos_descuento_letra',contenido).val_({id:json.result.descuento_letra, nombre:json.result.descuento_letra});

			contenido.append("<br>");

			var tabla = generaTabla({
							id_tabla: 'descuentos_lista',
							items: json.result.descuentos,
							ajaxModulo: 'clientes',
							ajaxAccion: 'listar',
							campobusqueda: false,
							keys_busqueda: ['id','grupo','accion'],
							key_nombre: null,
							sinResultados_label: 'No existen descuentos para este Cliente',
							max_height: 300,
							funcion_activado: function(el){

								//Seleccionamos el activo anterior
								var elActivo = $('#descuentos_lista').find('.active_');

								//Boton Borrar
								var boton_borrar = '<button class="btn btn-xs btn-warning" style="margin: 0px 0px 8px 5px; width: 24px; height: 24px;" data-toggle="tooltip" title="Borrar" onclick=" confirma(\'Eliminar Descuento\',\'El descuento se eliminará de forma permanente.\',\'exclamacion\',\'danger\',\'clientes_descuentos_elimina(\\\''+$(el).data('cliente')+'\\\',\\\''+$(el).data('prefijo')+'\\\')\');"><i class="fa fa-remove"></i></button>';

								//Boton editar
								var boton_editar = '<button class="btn btn-xs btn-primary" style="margin: 0px 0px 8px 5px; width: 24px; height: 24px;" data-toggle="tooltip" title="Editar" onclick="clientes_descuentos_edita_agrega(\''+$(el).data('cliente')+'\', \''+$(el).data('prefijo')+'\', \''+$(el).data('descuento')+'\');"><i class="fa fa-pencil"></i></button>';

								
								//Agregamos los datos
								var html = '<b>Prefijo ' + $(el).data('prefijo') + ': </b>  ' + $(el).data('descuento') + '% de descuento <div class="pull-right descripcion" style="width: 120px; text-align: right; position: absolute; top: 8px; right: 10px;">' + boton_editar + boton_borrar +'</div>';


								//Agergamos el html
								$(el).html(html);
							},
							funcion_desactivado: function(el){
								var html = '<b>Prefijo ' + $(el).data('prefijo') + ': </b>  ' + $(el).data('descuento') + '% de descuento';								
								$(el).html(html);
							}
						});

			//Agregamos los tooltips
			tabla.tooltip({selector: '[data-toggle="tooltip"]', placement: 'left', animation: false});

			//Generamos el listado
			$('#'+clientes_config.modName+'Modal_container').html('');
			

        	//$('#clientes_descuentos_fabricante',contenido).val_(json.result.fabricante);


	    	//Asignamos el contenido al popup
			$('#'+clientes_config.modName+'Modal_container').html(contenido);
			$('#'+clientes_config.modName+'Modal_container').append(tabla);

			//Agregamos scrollbars
			$('#'+clientes_config.modName+'Modal_container .scrollable').mCustomScrollbar({theme: 'minimal-dark',scrollInertia: 100, mouseWheel:{ scrollAmount: 70 }});

			//Agregamos los botones al footer
			$('#'+clientes_config.modName+'Modal_footer').append('<button type="button" class="btn btn-primary" style="display: block; width: 100%; margin: 10px auto;" onclick="clientes_descuentos_edita_agrega(\''+id+'\');">Agregar Descuento</button>');

			$('#'+clientes_config.modName+'Modal_footer').append('<button type="button" class="btn btn-warning" style="display: block; width: 100%; margin: 10px auto;" onclick="clientes_listar();">Volver</button>');
		}
	});
}

//Lista
function clientes_descuentos_edita_agrega(id, prefijo, descuento){

	var modo = 'edita';
	if((typeof prefijo === 'undefined') && (typeof descuento === 'undefined'))
		modo = 'agrega';

	//Titulo
	if(modo == 'edita') $('#'+clientes_config.modName+'Modal_titulo').html('Editando descuentos del Cliente '+id);
	else $('#'+clientes_config.modName+'Modal_titulo').html('Agregar descuento del Cliente '+id);

	//Cargando...
	$('#'+clientes_config.modName+'Modal_container').html(cargando());

	//Footer...
	$('#'+clientes_config.modName+'Modal_footer').html('');

	//Contenido
	var contenido = $('<div class="scrollable" style="max-height: '+clientes_config.max_height_edita_agrega+'px; overflow-y: hidden;"></div>');

	if(modo == 'agrega'){
		contenido.append( genera_campo({	
				id: 			"prefijo",
				label: 			"Prefijo",
				tipo: 			"input",
				mostrarEditar: 	true,
				mostrarCrear: 	true
			}, 'clientes_descuentos' ,true) );

		if(modo == 'edita') $('#clientes_descuentos_prefijo',contenido).val_(prefijo);
	}

	contenido.append( genera_campo({	
			id: 			"descuento",
			label: 			"Descuento",
			tipo: 			"input",
			mostrarEditar: 	true,
			mostrarCrear: 	true
		}, 'clientes_descuentos' ,true) );

	if(modo == 'edita') $('#clientes_descuentos_descuento',contenido).val_(descuento);

	//Asignamos el contenido al popup
	$('#'+clientes_config.modName+'Modal_container').html(contenido);

	//Agregamos scrollbars
	$('#'+clientes_config.modName+'Modal_container .scrollable').mCustomScrollbar({theme: 'minimal-dark',scrollInertia: 100, mouseWheel:{ scrollAmount: 70 }});

	//Agregamos los botones al footer
	if(modo == 'agrega') $('#'+clientes_config.modName+'Modal_footer').append('<button type="button" class="btn btn-primary" style="display: block; width: 100%; margin: 10px auto;" onclick="clientes_descuentos_agrega_confirm(\''+id+'\');">Agregar</button>');

	if(modo == 'edita') $('#'+clientes_config.modName+'Modal_footer').append('<button type="button" class="btn btn-primary" style="display: block; width: 100%; margin: 10px auto;" onclick="clientes_descuentos_edita_confirm(\''+id+'\',\''+prefijo+'\');">Editar</button>');

	$('#'+clientes_config.modName+'Modal_footer').append('<button type="button" class="btn btn-warning" style="display: block; width: 100%; margin: 10px auto;" onclick="clientes_descuentos_lista(\''+id+'\');">Volver</button>');
}

//Elimina el descuento
function clientes_descuentos_elimina(id, prefijo){
	//Data array
	var data = {};
	data['cliente'] = id;
	data['prefijo'] = prefijo;
	
	apix('descuentos','eliminar_x_cliente',data, {
		ok: function(json){
				//Notifica
				notifica(	'Descuento Eliminado',
							'El descuento se eliminó con éxito',
							'informacion',
							'info'
						);
				//Update
				clientes_descuentos_lista(id);
		}
	});
}

//Edita el descuento
function clientes_descuentos_agrega_confirm(id){
	//Data array
	var data = {};
	data['cliente'] = id;
	data['prefijo'] = $('#clientes_descuentos_prefijo').val();
	data['descuento'] = $('#clientes_descuentos_descuento').val();
	
	apix('descuentos','agregar_x_cliente',data, {
		ok: function(json){
				//Notifica
				notifica(	'Descuento Agregado',
							'El descuento se agregó con éxito',
							'informacion',
							'info'
						);
				//Update
				clientes_descuentos_lista(id);
		}
	});
}

//Edita el descuento
function clientes_descuentos_edita_confirm(id,prefijo){
	//Data array
	var data = {};
	data['cliente'] = id;
	data['prefijo'] = prefijo;
	data['descuento'] = $('#clientes_descuentos_descuento').val();
	
	apix('descuentos','editar_x_cliente',data, {
		ok: function(json){
				//Notifica
				notifica(	'Descuento Editar',
							'El descuento se editó con éxito',
							'informacion',
							'info'
						);
				//Update
				clientes_descuentos_lista(id);
		}
	});
}
{/if}


{if="$GLOBALS['permisos']['descuentos_articulos_importar']"}
//Abrir Importador de descuentos por articulo
function descuentos_articulos_importar(){

  //Creamos el popup
  abrir_modal('importador_descuentos_articulo', 450, false);

  //Mostramos la ventana
  $('#importador_descuentos_articuloModal').modal({show:true});

  //Titulo...
  $('#importador_descuentos_articuloModal_titulo').html('Importar Descuentos por Articulo');
  $('#importador_descuentos_articuloModal_footer').html('');

  //Cargando...
  $('#importador_descuentos_articuloModal_container').html(cargando());

  //Cargando...
  $('#importador_descuentos_articuloModal_container').html('Para importar descuentos sobre los Articulos en Batch seleccione un archivo XLS. </br></br>Deberá tener el siguiente formato: </br></br><ul><li>Columna A: <b>Prefijo</b></li><li>Columna B: <b>Código</b></li><li>Columna C: <b>Sufijo</b></li><li>Columna D: <b>Descripción</b></li><li>Columna E: <b>Letra</b></li><li>Columna F: <b>Descuento</b></li></ul><input type="file" id="importador_descuentos_articulo_input" name="archivo" style="display: none;">');

  //Boton Confirmar cambios
  $('#importador_descuentos_articuloModal_footer').append('<button type="button" class="btn btn-primary" style="display: block; width: 100%; margin: 10px auto;" onclick="$(\'#importador_descuentos_articulo_input\').click();">Seleccionar Archivo</button>');

  //Append Cancel Button
  $('#importador_descuentos_articuloModal_footer').append('<button type="button" data-dismiss="modal" class="btn btn-warning" style="display: block; width: 100%; margin: 10px auto;">Cancelar Importación</button>');

  //Al seleccionar...
  $('#importador_descuentos_articulo_input').on('change', function(){

    if($('#importador_descuentos_articulo_input').val() != ''){

      //Recolectamos las variables
      var file = $('#importador_descuentos_articulo_input').prop('files')[0];

      //Reseteamos el campo
      $('#importador_descuentos_articulo_input').val(''); 

      //Datos del form
      var data = new FormData();
      data.append('archivo', file);

      //Indicamos al usuario que el sistema ahora va a estar ocupado haciendo el backup
      $('#importador_descuentos_articuloModal_titulo').html('Subiendo Archivo...');
      $('#importador_descuentos_articuloModal_container').html(cargando());
      $('#importador_descuentos_articuloModal_footer').html('');

      //API
      apix('descuentos','importar_x_articulo',data, {
        file: true,
        ok: function(json){

          //Titulo...
          $('#importador_descuentos_articuloModal_titulo').html('Descuentos Importados');

          //Mensaje
          $('#importador_descuentos_articuloModal_container').html('Los descuentos para los articulos fueron importados y actualizados con exito!');

          //Append Cancel Button
          $('#importador_descuentos_articuloModal_footer').append('<button type="button" data-dismiss="modal" class="btn btn-warning" style="display: block; width: 100%; margin: 10px auto;">Cerrar</button>');

        },
        error: function(){
          //Volvemos a mostrar el popup
          importador_descuentos_articulo_open();
        }
      });
    }
  });
}
{/if}

{if="$GLOBALS['permisos']['descuentos_clientes_importar']"}
//Abrir Importador de descuentos por cliente
function descuentos_clientes_importar(){

  //Creamos el popup
  abrir_modal('importador_descuentos_cliente', 450, false);

  //Mostramos la ventana
  $('#importador_descuentos_clienteModal').modal({show:true});

  //Titulo...
  $('#importador_descuentos_clienteModal_titulo').html('Importar Descuentos por Cliente');
  $('#importador_descuentos_clienteModal_footer').html('');

  //Cargando...
  $('#importador_descuentos_clienteModal_container').html(cargando());

  //Cargando...
  $('#importador_descuentos_clienteModal_container').html('Para importar descuentos sobre los Clientes en Batch seleccione un archivo XLS. </br></br>Deberá tener el siguiente formato (son 10 descuentos): </br></br><ul><li>Columna A: <b>Cliente</b></li><li>Columna C: <b>Condición de venta</b></li><li>Columna E: <b>Letra de descuento</b></li><li>Columna F: <b>Prefijo</b></li><li>Columna G: <b>Descuento</b></li></ul><input type="file" id="importador_descuentos_cliente_input" name="archivo" style="display: none;">');

  //Boton Confirmar cambios
  $('#importador_descuentos_clienteModal_footer').append('<button type="button" class="btn btn-primary" style="display: block; width: 100%; margin: 10px auto;" onclick="$(\'#importador_descuentos_cliente_input\').click();">Seleccionar Archivo</button>');

  //Append Cancel Button
  $('#importador_descuentos_clienteModal_footer').append('<button type="button" data-dismiss="modal" class="btn btn-warning" style="display: block; width: 100%; margin: 10px auto;">Cancelar Importación</button>');

  //Al seleccionar...
  $('#importador_descuentos_cliente_input').on('change', function(){

    if($('#importador_descuentos_cliente_input').val() != ''){

      //Recolectamos las variables
      var file = $('#importador_descuentos_cliente_input').prop('files')[0];

      //Reseteamos el campo
      $('#importador_descuentos_cliente_input').val('');

      //Datos del form
      var data = new FormData();
      data.append('archivo', file);

      //Indicamos al usuario que el sistema ahora va a estar ocupado haciendo el backup
      $('#importador_descuentos_clienteModal_titulo').html('Subiendo Archivo...');
      $('#importador_descuentos_clienteModal_container').html(cargando());
      $('#importador_descuentos_clienteModal_footer').html('');

      //API
      apix('descuentos','importar_x_cliente',data, {
        file: true,
        ok: function(json){

          //Titulo...
          $('#importador_descuentos_clienteModal_titulo').html('Descuentos Importados');

          //Mensaje
          $('#importador_descuentos_clienteModal_container').html('Los descuentos para los articulos fueron importados y actualizados con exito!');

          if(typeof(json.result.deshabilitados) != 'undefined'){
          	$('#importador_descuentos_clienteModal_container').append('Algunos Clientes fueron deshabilitados: <ul>');
          	for(var i in json.result.deshabilitados)
          		$('#importador_descuentos_clienteModal_container').append('<li>'+json.result.deshabilitados[i]+'</li>');
          	$('#importador_descuentos_clienteModal_container').append('</ul>');
          }

          //Append Cancel Button
          $('#importador_descuentos_clienteModal_footer').append('<button type="button" data-dismiss="modal" class="btn btn-warning" style="display: block; width: 100%; margin: 10px auto;">Cerrar</button>');

        },
        error: function(){
          //Volvemos a mostrar el popup
          importador_descuentos_cliente();
        }
      });


    }
  });
}
{/if}

</script>