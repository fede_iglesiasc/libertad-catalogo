<script type="text/javascript">

//Config
var funciones_config = {
	modName: 					'funciones',
	modLabelSingular: 			'Funcion',
	modLabelPlural: 			'Funciones',
	creaLabel: 					'Agregada',
	editaLabel: 				'Editada',
	eliminaLabel: 				'Eliminada',
	nuevoLabel: 				'Nueva',
	artLabel: 					'La',
	max_height_edita_agrega: 	'300',
	max_height_lista: 			'400',
	campos:[
		{	
			id: 			"identificador",
			label: 			"Identificador",
			tipo: 			"input",
			mostrarEditar: 	true,
			mostrarCrear: 	true
		},
		{	
			id: 			"grupo",
			label: 			"Grupo",
			tipo: 			"input",
			mostrarEditar: 	true,
			mostrarCrear: 	true
		},
		{	
			id: 			"accion",
			label: 			"Accion",
			tipo: 			"input",
			mostrarEditar: 	true,
			mostrarCrear: 	true
		}
	]
};

//Lista
function funciones_listar(){

	//Creamos el popup
	abrir_modal(funciones_config.modName, 700, false);

	//Mostramos la ventana
	$('#'+funciones_config.modName+'Modal').modal({show:true});

	//Titulo...
	$('#'+funciones_config.modName+'Modal_titulo').html(funciones_config.modLabelPlural);

	//Cargando...
	$('#'+funciones_config.modName+'Modal_container').html( cargando() );

	//Footer...
	$('#'+funciones_config.modName+'Modal_footer').html('');

	//Traemos los datos de la API
	apix(funciones_config.modName,'listar',{}, {
		ok: function(json){

			var tabla = generaTabla({
							id_tabla: funciones_config.modName,
							items: json.result,
							ajaxModulo: 'funciones',
							ajaxAccion: 'listar',
							keys_busqueda: ['id','grupo','accion'],
							key_nombre: null,
							max_height: funciones_config.max_height_lista,
							funcion_activado: function(el){
								//Seleccionamos el activo anterior
								var elActivo = $('#'+funciones_config.modName+'_lista').find('.active_');
								var id = $(el).data('id');

								//Relaciones
								var boton_relaciones = '<button class="btn btn-xs btn-primary" style="margin: 0px 0px 8px 5px; width: 24px; height: 24px;" data-toggle="tooltip" title="Relaciones" onclick="'+funciones_config.modLabelPlural.toLowerCase()+'_relaciones_listar('+$(el).data('id')+');"><i class="fa fa-yelp"></i></button>';

								//Permisos
								var boton_permisos = '<button class="btn btn-xs btn-primary" style="margin: 0px 0px 8px 5px; width: 24px; height: 24px;" data-toggle="tooltip" title="Permisos" onclick="'+funciones_config.modLabelPlural.toLowerCase()+'_permisos_listar('+$(el).data('id')+');"><i class="fa fa-unlock-alt"></i></button>';

								//Boton Borrar
								var	boton_borrar = '<button class="btn btn-xs btn-warning" style="margin: 0px 0px 8px 5px; width: 24px; height: 24px;" data-toggle="tooltip" title="Borrar" onclick=" confirma(\'Eliminar '+funciones_config.modLabelSingular+'\',\'Si usted elimina '+ funciones_config.artLabel+' '+funciones_config.modLabelSingular+', todos los datos y usuarios se perderán de forma permanente.\',\'exclamacion\',\'danger\',\''+funciones_config.modLabelPlural.toLowerCase()+'_elimina(\\\''+$(el).data('id')+'\\\')\');"><i class="fa fa-remove"></i></button>';

								//Boton editar
								var	boton_editar = '<button class="btn btn-xs btn-primary" style="margin: 0px 0px 8px 5px; width: 24px; height: 24px;" data-toggle="tooltip" title="Editar" onclick="'+funciones_config.modLabelPlural.toLowerCase()+'_edita_agrega(\''+$(el).data('id')+'\');"><i class="fa fa-pencil"></i></button>';

								//Obtenemos los permisos
								var permisos = $(el).data('api_permisos');

								
								//Mostramos los permisos o 'ningun acceso'
								if(objectLength(permisos)) var api = $('<ul class="fa-ul" style="margin-left: 40px;"/>');
								else var api = $('<span style="display: block;">(Ningún acceso)</span>');

								
								for(var k in permisos)
									api.append('<li style="color: ' + ( permisos[k].heredado ? "gray" : "black" ) + ';"><i class="fa-li fa fa-check-square"></i>'+permisos[k].label+'</li>');

								
								//Obtenemos los permisos
								var relaciones = $(el).data('relaciones');

								//Mostramos los relaciones o 'ningun acceso'
								if(objectLength(relaciones)) var relaciones_lista = $('<ul class="fa-ul" style="margin-left: 40px;"/>');
								else var relaciones_lista = $('<span style="display: block;">(Ninguna Relación)</span>');


								for(var k in relaciones)
									relaciones_lista.append('<li style="color: black;"><i class="fa-li fa fa-check-square"></i>'+relaciones[k].label+'</li>');

								
								//Agregamos los datos
								$(el).html('<b>' + $(el).data('grupo') + '</b><i class="badge" style="padding: 5px 10px; float: none; border-radius: 3px; background-color: #e6e6e6; margin-left: 7px; color: #444444; font-size: 12px; border: solid 1px #dadada; font-weight: 600;">' + $(el).data('accion') + '</i><small style="display: block; margin-top: 10px;"><strong>ID: </strong>'+$(el).data('identificador')+'</small><div style="width: 50%; display: inline-block; vertical-align: top;"><small><strong>API: </strong>'+api.outerHTML()+'</small></div><div style="width: 50%; display: inline-block; vertical-align: top;"><small><strong>RELACIONES: </strong>'+relaciones_lista.outerHTML()+'</small></div><div class="pull-right descripcion" style="width: 120px; text-align: right; position: absolute; top: 8px; right: 10px;">'+boton_relaciones+boton_permisos+boton_editar+boton_borrar+'</div>');


							},
							funcion_desactivado: function(el){
								var html = '<b>' + $(el).data('grupo') + '</b><i class="badge" style="padding: 5px 10px; float: none; border-radius: 3px; background-color: #e6e6e6; margin-left: 7px; color: #444444; font-size: 12px; border: solid 1px #dadada; font-weight: 600;">' + $(el).data('accion') + '</i>';
								//cambiamos HTML
								$(el).html(html);
							}
						});

			//Agregamos los tooltips
			tabla.tooltip({selector: '[data-toggle="tooltip"]', placement: 'left', animation: false});

			//Generamos el listado
			$('#'+funciones_config.modName+'Modal_container').html('');
			$('#'+funciones_config.modName+'Modal_container').append(tabla);


			//Agregamos scrollbars
			$('#'+funciones_config.modName+'Modal_container .scrollable').mCustomScrollbar({theme: 'minimal-dark',scrollInertia: 100, mouseWheel:{ scrollAmount: 70 }});

			//Boton Confirmar cambios
			$('#'+funciones_config.modName+'Modal_footer').append('<button type="button" class="btn btn-primary" style="display: block; width: 100%; margin: 10px auto;" onclick="'+funciones_config.modName+'_edita_agrega();">'+funciones_config.nuevoLabel+' '+funciones_config.modLabelSingular+'</button>');

			//Append Cancel Button
			$('#'+funciones_config.modName+'Modal_footer').append('<button type="button" data-dismiss="modal" class="btn btn-warning" style="display: block; width: 100%; margin: 10px auto;">Cerrar</button>');
		}
	});
}

//Edita/Agrega
function funciones_edita_agrega(id){

	//Si estamos agregando...
	if(typeof id === 'undefined') var agrega = true;
	else var agrega = false;

	//Titulo...
	if(agrega) $('#'+funciones_config.modName+'Modal_titulo').html(funciones_config.nuevoLabel+' '+funciones_config.modLabelSingular);
	else $('#'+funciones_config.modName+'Modal_titulo').html('Editar '+funciones_config.modLabelSingular);

	//Cargando...
	$('#'+funciones_config.modName+'Modal_container').html(cargando());

	//Footer...
	$('#'+funciones_config.modName+'Modal_footer').html('');

	//Contenido
	var contenido = $('<div class="scrollable" style="max-height: '+funciones_config.max_height_edita_agrega+'px; overflow-y: hidden;"></div>');

	//Agregamos todos los campos
	for(var k in funciones_config.campos){
		var campo = funciones_config.campos[k];
		if( (campo.mostrarEditar && !agrega) || (campo.mostrarCrear && agrega)){
			//Agregamos el campo
			contenido.append( genera_campo(funciones_config.campos[k], funciones_config.modName ,true) );
		}
	}


	//Agregamos el contenido y los botones
	//si estamos creando un registro nuevo
	if(agrega){
		//Agregamos el contenido
		$('#'+funciones_config.modName+'Modal_container').html(contenido);

		//Agregamos scrollbars
		$('#'+funciones_config.modName+'Modal_container .scrollable').mCustomScrollbar({theme: 'minimal-dark',scrollInertia: 100, mouseWheel:{ scrollAmount: 70 }});

		//Agregamos los botones al footer
		$('#'+funciones_config.modName+'Modal_footer').append('<button type="button" class="btn btn-primary" style="display: block; width: 100%; margin: 10px auto;" onclick="'+funciones_config.modLabelPlural.toLowerCase()+'_agrega_confirm();">Agregar '+funciones_config.modLabelSingular+'</button>');
		$('#'+funciones_config.modName+'Modal_footer').append('<button type="button" class="btn btn-warning" style="display: block; width: 100%; margin: 10px auto;" onclick="'+funciones_config.modLabelPlural.toLowerCase()+'_listar();">Volver</button>');
	}


	//Si editamos
	if(!agrega){
		var data = {};
		data['funcion'] = id;

		apix(funciones_config.modName,'info',data, {
            ok: function(json){

				//Titulo
				$('#'+funciones_config.modName+'Modal_titulo').html('Editar '+funciones_config.modLabelSingular);

		    	//Llenamos los datos
		    	for(var k in funciones_config.campos){
		    		var campo = funciones_config.campos[k];
		    		if((campo.tipo == 'input') || (campo.tipo == 'checkbox'))
			    		if( !(typeof(json.result[ campo.id ]) == "undefined") && !(json.result[ campo.id ] === null) ){
			    			console.log(campo.id);
			    			$('#'+funciones_config.modName+'_'+campo.id,contenido).val_(json.result[ campo.id ]);
			    		}
		    	}

		    	//Asignamos el contenido al popup
				$('#'+funciones_config.modName+'Modal_container').html(contenido);

				//Agregamos scrollbars
				$('#'+funciones_config.modName+'Modal_container .scrollable').mCustomScrollbar({theme: 'minimal-dark',scrollInertia: 100, mouseWheel:{ scrollAmount: 70 }});

				//Agregamos los botones al footer
				$('#'+funciones_config.modName+'Modal_footer').append('<button type="button" class="btn btn-primary" style="display: block; width: 100%; margin: 10px auto;" onclick="'+funciones_config.modLabelPlural.toLowerCase()+'_edita_confirm(\''+id+'\');">Editar '+funciones_config.modLabelSingular+'</button>');
				$('#'+funciones_config.modName+'Modal_footer').append('<button type="button" class="btn btn-warning" style="display: block; width: 100%; margin: 10px auto;" onclick="'+funciones_config.modLabelPlural.toLowerCase()+'_listar();">Volver</button>');
			}
		});
	}
}

//Edita
function funciones_edita_confirm(id){

	//Data
	var data = {};
	data['funcion'] = id;

	//Agregamos todos los campos
	for(var k in funciones_config.campos){
		var campo = funciones_config.campos[k];
		if(campo.type = 'input')
			if(campo.mostrarEditar)
				data[campo.id] = $('#'+funciones_config.modName+'_'+campo.id).val();
	}

	apix(funciones_config.modName,'editar',data, {
		ok: function(json){
			notifica(	funciones_config.modLabelSingular+' '+funciones_config.editaLabel.toLowerCase(),
						funciones_config.artLabel+' '+funciones_config.modLabelSingular+' se editó con éxito',
						'confirmacion','success');
			funciones_listar();
		}
	});
}

//Agrega
function funciones_agrega_confirm(){

	//Data
	var data = {};

	//Agregamos todos los campos
	for(var k in funciones_config.campos){
		var campo = funciones_config.campos[k];
		if(campo.mostrarEditar)
			if(campo.type = 'input')
				data[campo.id] = $('#'+funciones_config.modName+'_'+campo.id).val();
	}

	apix(funciones_config.modName,'agregar',data, {
		ok: function(json){
			//Notifica
			notifica(	funciones_config.modLabelSingular+' '+funciones_config.creaLabel.toLowerCase(),
						funciones_config.artLabel+' '+funciones_config.modLabelSingular+' se creó con éxito',
						'confirmacion',
						'success'
					);
			//Update
			funciones_listar();
		}
	});
}

//Elimina
function funciones_elimina(id){

	//Data array
	var data = {};
	data['funcion'] = id;
	
	apix(funciones_config.modName,'eliminar',data, {
		ok: function(json){
	    	//Notifica
	    	notifica(	funciones_config.modLabelSingular+' '+funciones_config.eliminaLabel,
	    				funciones_config.artLabel+' '+funciones_config.modLabelSingular+' se eliminó con éxito',
	    				'informacion',
	    				'info'
	    			);
	    	//Update
	    	funciones_listar();
		}
	});
}

//Lista
function funciones_permisos_listar(id){

	//Modificamos ancho
	abrir_modal(funciones_config.modName, 580, false);

	//Titulo...
	$('#'+funciones_config.modName+'Modal_titulo').html('Permisos a la API');

	//Cargando...
	$('#'+funciones_config.modName+'Modal_container').html( cargando() );

	//Footer...
	$('#'+funciones_config.modName+'Modal_footer').html('');

	//Traemos los datos de la API
	var data = {};
	data['funcion'] = id;
	apix(funciones_config.modName,'permisos_listar',data, {
		ok: function(json){

			//Generamos tabla
			var tabla = generaTabla({
					id_tabla: funciones_config.modName+'_permisos_api',
					items: json.result,
					keys_busqueda: ['id','modulo_nombre','accion_nombre'],
					key_nombre: null,
					max_height: funciones_config.max_height_lista,
					funcion_activado: function(el){
						
					},
					funcion_desactivado: function(el){
						//Checkbox
						var checkbox = $('<button class="btn btn-xs permiso_val" style="position: absolute; top: 10px; right: 10px;" data-type="checkbox"></button>');

						$(checkbox).boton({
							claseActivo: 'btn-success',
							claseInactivo: 'btn-danger',
							valueActivo: '1',
							valueInactivo: '0',
							valueDefault: $(el).data('acceso'),
							labelActivo: function(el){
								$(el).html('<i class="fa fa-check-square-o"></i> Habilitado');
							},
							labelInactivo: function(el){
								$(el).html('<i class="fa fa-ban"></i> Deshabilitado');
							}
						});

						var html = $('<div><b>' + $(el).data('modulo_nombre') + '</b><br/><small>' + $(el).data('accion_nombre') + '</div>');
						html.append(checkbox);
						//cambiamos HTML
						$(el).html(html);
					}
				});

			//Agregamos los tooltips
			tabla.tooltip({selector: '[data-toggle="tooltip"]', placement: 'left', animation: false});

			//Generamos el listado
			$('#'+funciones_config.modName+'Modal_container').html('');
			$('#'+funciones_config.modName+'Modal_container').append(tabla);

			//Agregamos scrollbars
			$('#'+funciones_config.modName+'Modal_container .scrollable').mCustomScrollbar({theme: 'minimal-dark',scrollInertia: 100, mouseWheel:{ scrollAmount: 70 }});

			//Boton Confirmar cambios
			$('#'+funciones_config.modName+'Modal_footer').append('<button type="button" class="btn btn-primary" style="display: block; width: 100%; margin: 10px auto;" onclick="'+funciones_config.modName+'_permisos_editar('+id+');">Guardar cambios</button>');

			//Append Cancel Button
			$('#'+funciones_config.modName+'Modal_footer').append('<button type="button" class="btn btn-warning" style="display: block; width: 100%; margin: 10px auto;" onclick="'+funciones_config.modName+'_listar();">Volver</button>');
		}
	});
}

//Guarda Permisos
function funciones_permisos_editar(id){
	//Array parametro permisos
	var permisos = Array();

	//Creamos array
	$('#'+funciones_config.modName+'_permisos_api_lista .list-group-item').each(function(i, e){
		//Value
		var value = parseInt($('button[data-type="checkbox"]',this).val());
		
		//Agregamosel valor solo si esta habilitado
		if(value != 0) 
			permisos.push( parseInt($(this).data('id')) );
	});

	//Data
	var data = {};
	data['funcion'] = id;
	data['permisos'] = JSON.stringify(permisos);

	//DB confirm
	apix(funciones_config.modName,'permisos_editar',data, {
		ok: function(json){
			notifica('Permisos editados','Los permisos a la API se editaron con éxito','confirmacion','success');
			funciones_listar();
		}
	});
}

//Relaciones
function funciones_relaciones_listar(id){

	//Modificamos ancho
	abrir_modal(funciones_config.modName, 580, false);
	
	//Titulo...
	$('#'+funciones_config.modName+'Modal_titulo').html('Relaciones con otras Funciones');

	//Cargando...
	$('#'+funciones_config.modName+'Modal_container').html( cargando() );

	//Footer...
	$('#'+funciones_config.modName+'Modal_footer').html('');

	//Traemos los datos de la API
	var data = {};
	data['funcion'] = id;
	apix(funciones_config.modName,'relaciones_listar',data, {
		ok: function(json){

			//Generamos tabla
			var tabla = generaTabla({
					id_tabla: funciones_config.modName+'_relaciones',
					items: json.result,
					keys_busqueda: ['id','grupo','accion'],
					key_nombre: null,
					max_height: funciones_config.max_height_lista,
					funcion_activado: function(el){
						
					},
					funcion_desactivado: function(el){
						//Checkbox
						var checkbox = $('<button class="btn btn-xs permiso_val" style="position: absolute; top: 10px; right: 10px;" data-type="checkbox"></button>');

						$(checkbox).boton({
							claseActivo: 'btn-success',
							claseInactivo: 'btn-danger',
							valueActivo: 1,
							valueInactivo: 0,
							valueDefault: parseInt($(el).data('activo')),
							labelActivo: function(el){
								$(el).html('<i class="fa fa-check-square-o"></i> Habilitado');
							},
							labelInactivo: function(el){
								$(el).html('<i class="fa fa-ban"></i> Deshabilitado');
							}
						});

						var html = $('<div><b>' + $(el).data('grupo') + '</b><br/><small>' + $(el).data('accion') + '</div>');
						html.append(checkbox);
						//cambiamos HTML
						$(el).html(html);
					}
				});

			//Agregamos los tooltips
			tabla.tooltip({selector: '[data-toggle="tooltip"]', placement: 'left', animation: false});

			//Generamos el listado
			$('#'+funciones_config.modName+'Modal_container').html('');
			$('#'+funciones_config.modName+'Modal_container').append(tabla);

			//Agregamos scrollbars
			$('#'+funciones_config.modName+'Modal_container .scrollable').mCustomScrollbar({theme: 'minimal-dark',scrollInertia: 100, mouseWheel:{ scrollAmount: 70 }});

			//Boton Confirmar cambios
			$('#'+funciones_config.modName+'Modal_footer').append('<button type="button" class="btn btn-primary" style="display: block; width: 100%; margin: 10px auto;" onclick="'+funciones_config.modName+'_relaciones_editar('+id+');">Guardar cambios</button>');

			//Append Cancel Button
			$('#'+funciones_config.modName+'Modal_footer').append('<button type="button" class="btn btn-warning" style="display: block; width: 100%; margin: 10px auto;" onclick="'+funciones_config.modName+'_listar();">Volver</button>');
		}
	});
}

//Guarda Relaciones
function funciones_relaciones_editar(id){
	//Array parametro permisos
	var items = Array();

	//Creamos array
	$('#'+funciones_config.modName+'_relaciones_lista .list-group-item').each(function(i, e){
		//Value
		var value = parseInt($('button[data-type="checkbox"]',this).val());
		
		//Agregamosel valor solo si esta habilitado
		if(value != 0) 
			items.push( parseInt($(this).data('id')) );
	});

	//Data
	var data = {};
	data['funcion'] = id;
	data['relaciones'] = JSON.stringify(items);

	//DB confirm
	apix(funciones_config.modName,'relaciones_editar',data, {
		ok: function(json){
			notifica('Relaciones editadas','Las relaciones se editaron con éxito','confirmacion','success');
			funciones_listar();
		}
	});
}


</script>