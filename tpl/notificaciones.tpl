<script type="text/javascript">

var notificaciones_audio = new Audio('./img/mensaje.mp3');

console.log('notificaciones 5');

{if="$GLOBALS['permisos']['notificaciones_mostrar']"}
// Abre cuadro de dialogo para generar
// Backup de datos en el servidor
function notificaciones_open(){

  //Creamos el popup
  abrir_modal('notificaciones', 500, false);

  //Mostramos la ventana
  $('#notificacionesModal').modal({show:true});

  //Titulo...
  $('#notificacionesModal_titulo').html('Notificaciones');



  //Cargando...
  $('#notificacionesModal_container').html('<ul class="list-group" id="notificacionesnotificaciones_lista" style="padding: 0px; box-shadow: none; margin-bottom: 0px;"><li class="list-group-item" style="padding: 8px 12px;" data-tipo="notificacion"><strong>13-11-2014</strong>Nueva linea cargada<span>acciones</span></li></ul>');

  //Botones
  $('#notificacionesModal_footer').append('<button type="button" class="btn btn-primary" style="display: block; width: 100%; margin: 10px auto;" onclick="crearBackup_confirm();">Nueva Notificación</button><button type="button" data-dismiss="modal" class="btn btn-warning" style="display: block; width: 100%; margin: 10px auto;">Cerrar</button>');
}
{/if}

{if="$GLOBALS['permisos']['notificaciones_mostrar']"}
// Da la orden al servidor para eliminar la notificacion
//para este usuario
function notificaciones_confirm(){
  
  //Indicamos al usuario que el sistema ahora va a estar ocupado haciendo el backup
  $('#notificacionesModal_titulo').html('Creando Backup...');
  $('#notificacionesModal_container').html('<div style="display: block; text-align: center;"><br/><img src="./img/procesando.gif" width="30" height="30"></div>');
  $('#notificacionesModal_footer').html('');


  //Data array
  var data = {};
  data['module'] = 'backup';
  data['run'] = 'backup_confirm';
  

  //Make ajax call to api
  $.ajax({
    type: "GET",
    url: "api.php",
    data: data,
    contentType: "application/json; charset=utf-8",
    dataType: "json",
    error: function (json) {
    },
    success: function (json) {
        //If session expire reload page...
        if(!json.logued) location.reload();

        //If result is ok
        if(json.status == '1'){

          //Titulo...
          $('#notificacionesModal_titulo').html('El Backup se creó exitosamente');

          //Cargando...
          $('#notificacionesModal_container').html('Se creo el backup con éxito, podra encontrarlo en la carpeta de respaldos.');

          //Boton Confirmar cambios
          $('#notificacionesModal_footer').append('<button type="button" data-dismiss="modal" class="btn btn-primary" style="display: block; width: 100%; margin: 10px auto;">OK</button>');


        //Si existio error...
        }else{
          showErrorModal('Error al crear Backup.');
        }
    }
  });
}
{/if}


//CHEQUEA NOTIFICACIONES Y LAS MUESTRA
function checkNotificaciones(){
  
  //DB Query
  apix('notificaciones','read',{}, {
    reload: true,
    ok: function(json){


      {if="$GLOBALS['permisos']['mensajes_chat']"}
      //MENSAJES: Actualizamos conversaciones
      if(json.result.mensajes_conversaciones_version > parseInt($('#mensajes_conversaciones').attr('data-version')))
        mensajes_conversaciones_update(); 

      //MENSAJES: Actualizamos usuarios Online
      if(watable_modo == 'mensajes') mensajes_contactos_online(json.result.mensajes_contactos_version);

      //MENSAJES: Mostramos nuevos mensajes
      if(watable_modo != 'mensajes'){
        if(json.result.mensajes_no_leidos == 0) $('#boton-cabecera-mensajes span.badge-notify').html(json.result.mensajes_no_leidos).hide();
        else{
          $('#boton-cabecera-mensajes span.badge-notify').html(json.result.mensajes_no_leidos).show();
          mensajes_notificacion(); 
        }
      }

      //Mostramos notificacion estando dentro de mensajes
      if((watable_modo == 'mensajes') && $('.icon-meta.unread-count').length) mensajes_notificacion();

      {/if}

      //LISTAS: Mostramos actualizaciones de precio
      if(json.result.listas > 0) $('#boton-cabecera-listas span.badge-notify').html(json.result.listas).show();
      else $('#boton-cabecera-listas span.badge-notify').html(json.result.listas).hide();




      for(var i in json.result.carteles){

        //Chequeamos si ya se encuentran abiertas
        //las notificaciones anteriores...
        var existe = $('div[data-id="'+json.result.carteles[i].id+'"]').length;
        

        if(!existe){
          
          //Mensaje
          var mensaje = json.result.carteles[i].mensaje+"</br><button type=\"button\" data-growl=\"dismiss\" class=\"btn btn-info btn-sm\" style=\"float: right; margin: 20px 0px 0px 0px;\" ";
          if(json.result.carteles[i].id == -1) mensaje += ">Enterado!</button>";
          else mensaje += "onClick=\"confirmNotificacion("+json.result.carteles[i].id+")\">No volver a mostrar</button>";

          //Notificacion
          $.growl({
            title: "<strong>"+json.result.carteles[i].titulo+"</strong></br>",
            message: mensaje},
            {
              type: "growl",
              placement: {
                from: "bottom",
                align: "left"
              },

              delay: 0,
              template: '<div data-growl="container" data-id="'+json.result.carteles[i].id+'" class="alert" role="alert" style="width: 400px; background: url(\'./img/header_bg.png\'); background-size: auto 50px;"> <button type="button" class="close" data-growl="dismiss"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button><span data-growl="icon"></span><span data-growl="title" style="margin-right: 30px; display: block;"></span><span data-growl="message" style="margin-right: 30px; display: block;"></span><a href="#" data-growl="url"></a></div>'
          });
        }
      }
    }
  });
}



//DESACTIVA LA NOTIFICACION
function confirmNotificacion(id){
  //API
  var data = {};
  data['notificacion'] = id;
  apix('notificaciones','readed',data, {
  });
}

function notificaciones_autorizar() {
    Notification.requestPermission(function(perm){
        notificaciones_permiso = perm;
    });
}

document.addEventListener('DOMContentLoaded', function () {
  //Notificaciones no disponibles
  if(!Notification) return;

  if (Notification.permission !== "granted")
    Notification.requestPermission();
});

//Chequea las notificaciones
setInterval(checkNotificaciones, 15000);


</script>