<script type="text/javascript">

{if="$GLOBALS['permisos']['articulos_edicion_equivalencias']"}

//Abrir Equivalencias
function equivalencias_listar(id,cod,rub){

  //Si el artículo no tiene rubro designado
  //evitamos abrir el popup y alertamos al usuario
  if(rub.trim() == ''){
    alert("ATENCIÓN \n\nPara poder asignar al artículo Equivalencias este deberá pertencer a algún Rubro previamente.");
  
  }else{
    //Creamos el popup
    abrir_modal('articuloEditar', 630, false);

    //Mostramos la ventana
    $('#articuloEditarModal').modal({show:true});

    //Guardamos el id
    $('#articuloEditarModal').data('id',id);
    
    //Titulo...
    $('#articuloEditarModal_titulo').html('Cargando');

    //Cargando...
    $('#articuloEditarModal_container').html('<div style="display: block; text-align: center;"><br/><img src="./img/procesando.gif" width="30" height="30"></div>');

    //Llamada a la API
    var data = {};
    data['articulo'] = id;
    apix('equivalencias','listar',data, {
      ok: function(json){
        //Cargamos lista de aplicaciones
        moduloarticuloEditarAplicaciones_lista = json;

        //Titulo...
        $('#articuloEditarModal_titulo').html('Equivalencias Artículo <b>' + cod.split(' ').join('') + '</b>');

        //Contenido
        $('#articuloEditarModal_container').html('<div style="margin: 0px 9px;"><input type="text" id="articuloEditarEquivalencias_codigos" style="width: 100%;"><div id="articuloEditarEquivalencias_dialog" class="alert alert-success" role="alert" style="margin-top: 10px; display: none; text-align: center;"></div><div class="scrollable" style="max-height: 500px; min-height: 200px; overflow-y: hidden;"><ul class="list-group" id="articuloEditarEquivalencias_lista" style="padding: 0px; box-shadow: none; margin-bottom: 0px; margin-top: 40px;"></ul></div></div>');

        //PREFIJO SELECT BOX
        $('#articuloEditarEquivalencias_codigos').select2({
            minimumInputLength: 0,
            placeholder: 'Ingrese Marca y Código',
            allowClear: true,
            width: 'resolve',
            id: function(e) { return e.articulo_id; },
            formatNoMatches: function() { return 'Sin resultados'; },
            formatSearching: function(){ return "Buscando..."; },
            ajax: {
                url: "api.php",
                dataType: 'json',
                quietMillis: 200,
                data: function(term, page) {
                    return {
                        module: 'equivalencias',
                        run: 'listar_codigos',
                        articulo: id,
                        q: term,
                        p: page
                    };
                },
                results: function(data, page ) {
                    //If session expire reload page...
                    if(!data.logued) location.reload();

                    var more = (page * 40) < data.result.total; // whether or not there are more results available

                    // notice we return the value of more so Select2 knows if more results can be loaded
                    return {results: data.result.items, more: more};
                }
            },
            formatInputTooShort: function () {
                      return "";
                  },  
            formatResult: function(item) {
                
                var label = '<i class="badge" style="padding: 4px 6px; float: none; border-radius: 2px; background-color: #e6e6e6; margin-left: 7px; color: #444444; font-size: 10px; border: solid 1px #dadada;">';
                
                //Marcas solas para agregar articulo nuevo...
                if ( (item.articulo_id == null) && (item.codigo == null) && (item.codigo_original == null) )
                  item.nombre = '<b>' + item.marca_label + '</b> ' + label + 'Agregar nuevo Código </i>';

                //Articulos existentes en la base de datos
                if ( (item.articulo_id != null) && (item.codigo_original == null) )
                  item.nombre = '<b>' + item.marca_label + '</b> ' + item.codigo + label + 'Articulo existente </i>';
                
                //Articulos existentes en la base de datos
                if ( (item.articulo_id == null) && (item.codigo_original != null) )
                  item.nombre = '<b>' + item.marca_label + '</b> ' + item.codigo_original + label + 'Otros fabricantes </i>';

                return "<div class='select2-user-result'>" + item.nombre + "</div>"; 
            },
            formatSelection: function(item) { 
                return item.nombre; 
            },
            initSelection : function (element, callback) {
                var elementText = $(element).attr('data-init-text');
            }
        }).on("change", function(e) { 

          //Agregar
          equivalencia_agregar();

        }).on("select2-removed", function(e) {
        });

        //Creamos grupo
        if(json.result.length == 0)
          $('#articuloEditarEquivalencias_lista').append('<li class="list-group-item" data-tipo="no-equiv" style="padding: 8px 12px; text-align: center; color: darkgrey; border: none;">Este artículo no contiene Equivalencias</li>');

        //Agregamos las aplicaciones existentes
        for(var k in json.result)
          equivalencia_append( json.result[k].articulo_id, json.result[k].articulo_codigo, json.result[k].marca_id, json.result[k].marca_label, json.result[k].codigo_original, json.result[k].sincroniza_dimensiones, json.result[k].sincroniza_aplicaciones, json.result[k].sincroniza_imagenes, 0 );

        //Agregamos scrollbars
        $('#articuloEditarModal_container .scrollable').mCustomScrollbar({theme: 'minimal-dark',scrollInertia: 100, mouseWheel:{ scrollAmount: 70 }});

        //Focus en auto
        setTimeout(function(){ $('#articuloEditarEquivalencias_codigos').select2('focus'); }, 500);

        //Boton Insertar imagen
        $('#articuloEditarModal_footer').append('<button type="button" class="btn btn-primary" style="display: block; width: 100%; margin: 10px auto;" onclick="equivalencias_confirmar('+id+');">Guardar cambios</button>');

        //Append Cancel Button
        $('#articuloEditarModal_footer').append('<button type="button" data-dismiss="modal" class="btn btn-warning" style="display: block; width: 100%; margin: 10px auto;">Descartar cambios</button>');

      }
    });
  }
}

//Agrega Equivalencia al listado
function equivalencia_append( articulo_id, articulo_codigo, marca_id, marca_label, codigo_original, sincroniza_dimensiones, sincroniza_aplicaciones, sincroniza_imagenes, valida ){

  // Si no queremos validar la equivalencia antes de insertarla
  // seteamos este valor en 0. Por defecto valida.
  if(typeof(valida)==='undefined') valida = 1;

  //Inicializamos la bandera que nos indica si
  //el articulo fue validado o no.
  var agregarBandera = true;
  // Si no debemos validar los articulos
  // antes de insertarlos salteamos esta parte.
  if(valida)
    $('#articuloEditarEquivalencias_lista li[data-tipo=equiv]').each(function(){
      if( (articulo_id == $(this).data('articulo_id')) && (articulo_codigo == $(this).data('articulo_codigo')) && (marca_id == $(this).data('marca_id')) && (marca_label == $(this).data('marca_label')) && (codigo_original == $(this).data('codigo_original')) ){
        agregarBandera = false;
        return false;
      }
    });


  if(agregarBandera){

    //Creamos el item
    var li = '<li class="list-group-item" style="padding: 8px 12px;" data-tipo="equiv" data-articulo_id="'+articulo_id+'" data-articulo_codigo="'+articulo_codigo+'" data-marca_id="'+marca_id+'" data-marca_label="'+marca_label+'" data-codigo_original="'+codigo_original+'" data-sincroniza_aplicaciones="'+sincroniza_aplicaciones+'" data-sincroniza_dimensiones="'+sincroniza_dimensiones+'" data-sincroniza_imagenes="'+sincroniza_imagenes+'"><b>'+marca_label+'</b> ';

    //Si es un articulo existente
    if(articulo_codigo != null)
      li += articulo_codigo + '<i class="badge" style="padding: 7px; float: none; border-radius: 3px; background-color: #e6e6e6; margin-left: 7px; color: black;">Artículo existente</i>';

    //Si el fabricante no lo laburamos
    if(codigo_original != null)
      li += codigo_original + '<i class="badge" style="padding: 7px; float: none; border-radius: 3px; background-color: #e6e6e6; margin-left: 7px; color: black;">Otro Fabricante</i>';

    //Agregamos boton Compartir Aplicaciones
    li += '<div class="pull-right" style="width: 120px; text-align: right; margin-top: 2px;">';

    //Solo lo agregamos si el art
    //Lo distribuimos nosotros
    if(codigo_original == null){
      li += '<button class="btn btn-xs no_focus ';
      var tt = '<b>No</b> Sincroniza<br>las Aplicaciones';
      if(parseInt(sincroniza_aplicaciones)){ li += 'btn-info'; tt = 'Sincroniza las<br>Aplicaciones'; }
      li += '" style="margin-left: 5px;" data-toggle="tooltip" data-html="true" title="'+tt+'" onclick="var p = $(this).parent().parent(); var v = parseInt(p.data(\'sincroniza_aplicaciones\')); v = (v)? 0 : 1; p.data(\'sincroniza_aplicaciones\', v); $(this).toggleClass(\'btn-info\'); var title = \'<b>No</b> Sincroniza<br>las Aplicaciones\'; if(v) title = \'Sincroniza las<br>Aplicaciones\'; $(this).attr(\'title\',title).tooltip(\'fixTitle\').next().find(\'div.tooltip-inner\').html(title);"><i class="fa fa-automobile"></i></button>';

      //Agregamos boton Compartir Dimensiones
      li += '<button class="btn btn-xs no_focus ';
      var tt = '<b>No</b> Sincroniza<br>las Dimensiones';
      if(parseInt(sincroniza_dimensiones)){ li += 'btn-info'; tt = 'Sincroniza las<br>Dimensiones'; }
      li += '" style="margin-left: 5px;" data-toggle="tooltip" data-html="true" title="'+tt+'" onclick="var p = $(this).parent().parent(); var v = parseInt(p.data(\'sincroniza_dimensiones\')); v = (v)? 0 : 1; p.data(\'sincroniza_dimensiones\', v); $(this).toggleClass(\'btn-info\'); var title = \'<b>No</b> Sincroniza<br>las Dimensiones\'; if(v) title = \'Sincroniza las<br>Dimensiones\'; $(this).attr(\'title\',title).tooltip(\'fixTitle\').next().find(\'div.tooltip-inner\').html(title);"><i class="fa fa-cube"></i></button>';

      //Agregamos boton Compartir Imágenes
      li += '<button class="btn btn-xs no_focus ';
      var tt = '<b>No</b> Comparte<br>las Imágenes';
      if(parseInt(sincroniza_imagenes)){ li += 'btn-info'; tt = 'Comparte las<br>Imágenes'; }
      li += '" style="margin-left: 5px;" data-toggle="tooltip" data-html="true" title="'+tt+'" onclick="var p = $(this).parent().parent(); var v = parseInt(p.data(\'sincroniza_imagenes\')); v = (v)? 0 : 1; p.data(\'sincroniza_imagenes\', v); $(this).toggleClass(\'btn-info\'); var title = \'<b>No</b> Comparte<br>las Imágenes\'; if(v) title = \'Comparte las<br>Imágenes\'; $(this).attr(\'title\',title).tooltip(\'fixTitle\').next().find(\'div.tooltip-inner\').html(title);"><i class="fa fa-camera"></i></button>';
    }


    //Agregamos boton borrar
    li += '<button class="btn btn-xs btn-warning" style="margin-left: 5px;" data-toggle="tooltip" title="Borrar Equivalencia" onclick="equivalencia_borrar(this);"><i class="fa fa-remove"></i></button></div></li>';
    
    //Aseguremonos de quitar el aviso 'Este artículo no contiene aplicaciones'
    $('#articuloEditarEquivalencias_lista li[data-tipo=no-equiv]').remove();

    //Agregamos el Item
    $('#articuloEditarEquivalencias_lista').append(li);

    //Actualizamos tooltips
    $('#articuloEditarEquivalencias_lista').tooltip({selector: '[data-toggle="tooltip"]' });

    //Ordenamos la lista
    articuloEditar_ordenarLista('articuloEditarEquivalencias_lista');

    //Recalculamos alto de la lista
    articuloEditar_recalcAlt('articuloEditarEquivalencias_lista');
  }

  //Vaciamos los campos
  $('#articuloEditarEquivalencias_codigos').select2('data',null);
}

//Borrar Equivalencia
function equivalencia_borrar(e){
  var r = confirm("¿Esta seguro que desea quitar esta Equivalencias? \n\nAunque la elimine, siempre puede hacer click en 'Descartar cambios' para mantener la lista de equivalencias original.");
  if(r == true){
      //Eliminamos el item
      $(e).parent().parent().remove();
      
      //Creamos texto 'Este artículo no contiene aplicaciones'
      if( $('#articuloEditarEquivalencias_lista li').length == 0 )
        $('#articuloEditarEquivalencias_lista').html('<li class="list-group-item" data-tipo="no-equiv" style="padding: 8px 12px; text-align: center; color: darkgrey; border: none;">Este artículo no contiene Equivalencias</li>');

      //Recalculamos alto de la lista
      articuloEditar_recalcAlt('articuloEditarEquivalencias_lista');
  }
}

//Click al boton agregar
var equivPendientes;
function equivalencia_agregar(){

  //Art. seleccionados
  var art = $('#articuloEditarEquivalencias_codigos').select2('data');

  //Cerramos dialogos abiertos anteriormente
  if( $('#articuloEditarEquivalencias_dialog').html().length > 0 ) 
    equivalencias_close_dialog();

  //Si hicimos un cambio en el Select2 pero no hay seleccion
  if(art != null){
    var agregar = false;
    var codigo_original = '';
    if((art != null) && (art.articulo_id == null) && (art.codigo == null) && (art.codigo_original == null)){
      dialogo('Ingrese el Código','Ingrese el código del faltante.','interrogacion','info','Código',function(el){
        $('button.btn-primary',el).on('click',function(){
          //Codigo original
          codigo_original = $('#dialogo_input',el).val();
          if(codigo_original != ''){
            equivalencia_agregar_confirmar(art.marca_id,undefined,codigo_original);
            dialogo_.close();
          }
        });
        $('button.btn-warning',el).on('click',function(){
          $('#faltantes_codigo').val_(null);
        });
      });
    }else{
      equivalencia_agregar_confirmar(art.marca_id,art.articulo_id,undefined);
    }
  }
}

//Chequeamos si existen equivalencias asociadas al articulo que tratamos de agregar
function equivalencia_agregar_confirmar(fabricante,articulo,codigo){
  //Data
  var data = {};

  //Fabricante
  data['articulo_actual'] = $('#articuloEditarModal').data('id');

  //Fabricante
  data['fabricante'] = fabricante;

  //Articulo
  if(typeof articulo != 'undefined')
    data['articulo'] = articulo;
  
  //Codigo
  if(typeof codigo != 'undefined')
    data['codigo_original'] = codigo;
  
  apix('equivalencias','getGrupo',data, {
    ok: function(json){
      
      //Art. seleccionados
      var art = $('#articuloEditarEquivalencias_codigos').select2('data');

      //si existen equivalencias para el articulo que estamos tratando de agregar...
      if(json.result.length){
        //Asignamos las equivalencias pendientes
        equivPendientes = json.result;
        
        //Creamos el cuadro de dialogo y lo mostramos
        $('#articuloEditarEquivalencias_dialog').html('El artículo que esta tratando de agregar tiene <b>'+(json.result.length-1)+'</b> equivalencias, desea:<br><button type="button" class="btn btn-success" style="margin: 10px auto;" onclick="for(var k in equivPendientes) equivalencia_append( equivPendientes[k].articulo_id, equivPendientes[k].codigo, equivPendientes[k].marca_id, equivPendientes[k].marca_label, equivPendientes[k].codigo_original, equivPendientes[k].sincroniza_dimensiones, equivPendientes[k].sincroniza_aplicaciones, equivPendientes[k].sincroniza_imagenes ); equivalencias_close_dialog();">Agregar Todas</button> <button type="button" class="btn btn-primary" style="margin: 10px auto;" onclick=" var art = $(\'#articuloEditarEquivalencias_codigos\').select2(\'data\'); equivalencia_append( art.articulo_id, art.codigo, art.marca_id, art.marca_label, art.codigo_original, 0, 0, 0 ); equivalencias_close_dialog();">Agregar sólo Artículo seleccionado</button> <button type="button" class="btn btn-danger" style="margin: 10px auto;" onclick="equivalencias_close_dialog();">Cancelar</button>');

        //Mostramos el cuadro de dialogo
        $('#articuloEditarEquivalencias_dialog').show(500);

        //Evento que se ejecute solo una vez luego muere...
        $('body').on('click.Equivalencias_dialog', function(e) {
          if ( $(e.target).closest('#articuloEditarEquivalencias_dialog').length === 0 )
            equivalencias_close_dialog();
        });

      }else{
        //Agregamos un item a la lista directamente y vaciamos el select...
        equivalencia_append( art.articulo_id, art.codigo, art.marca_id, art.marca_label, art.codigo_original, 0, 0, 0 );
      }
    }
  });
}

//Confirmamos los cambios en el servidor
function equivalencias_confirmar(id){
  
  //Array de Equivalencias
  var equivalencias = [];

  $('#articuloEditarEquivalencias_lista li[data-tipo=equiv]').each(function(){

  //Agregamos la aplicacion
  equivalencias.push(  {
                        articulo_id: $(this).data("articulo_id"), 
                        codigo: $(this).data("codigo"),
                        marca_id: $(this).data("marca_id"),
                        marca_label: $(this).data("marca_label"),
                        codigo_original: $(this).data("codigo_original"),
                        sincroniza_aplicaciones: $(this).data("sincroniza_aplicaciones"),
                        sincroniza_imagenes: $(this).data("sincroniza_imagenes"),
                        sincroniza_dimensiones: $(this).data("sincroniza_dimensiones"),
                      });
  });


  //Llamada a la API
  var data = {};
  data['articulo'] = id;
  data['equivalencias'] = JSON.stringify(equivalencias);
  apix('equivalencias','editar',data, {
    ok: function(json){
      //Actualizamos la lista de articulos
      watable_volver = true;
      articulosUpdate();

      //Mostrmaos alerta si existen conflictos entre
      //versiones de aplicaciones sincronizadas
      if(json.result.warning == 1)
      notifica( 'Aplicaciones Sincronizadas!',
                'Las aplicaciones se sincronizaron correctamente, pero existen algunos vehículos que debería revisar. Ingrese en el editor de Aplicaciones de alguno de los artículos para corregirlo.',
            'confirmacion','success');

      //Cerramos el popUp
      $('#articuloEditarModal').modal('hide');
    }
  });
}

//Cerrar dialogo
function equivalencias_close_dialog(){
  
  //Cerramos y vaciamos el cuadro de dialogo
  $('#articuloEditarEquivalencias_dialog').hide(500, function(){ 
    $(this).empty(); 
  });

  //Seteamos en null la variable de Equivalencias pendientes
  equivPendientes = null;

  //Desbindeamos el evento
  $('body').unbind( "click.Equivalencias_dialog" );

  //Vaciamos los campos
  $('#articuloEditarEquivalencias_codigos').select2('data',null);

  //Hacemos foco
  $('#articuloEditarEquivalencias_codigos').select2('focus');
}

{/if}

</script>