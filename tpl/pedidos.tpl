<script type="text/javascript">


{if="$GLOBALS['permisos']['pedidos_listar']"}
  
function pedido_recalcula(){

	//Subtotal
	var subtotal = 0;
	for(var i in resultados.data.rows)
	  subtotal += parseFloat(resultados.data.rows[i].total_lista.replace("$ ", ""));

	//Subtotal
	var neto = 0;
	for(var i in resultados.data.rows)
	  neto += parseFloat(resultados.data.rows[i].total.replace("$ ", ""));

	var iva = neto * 0.21;
	var total = neto + iva;

	//Neto
	/*var descuento = (parseFloat($('#carro_descuento_porcentaje').val()).toFixed(2) / 100 * subtotal).toFixed(2);
	var neto = subtotal - descuento; 
	var iva = subtotal * 0.21;
	var total = neto + iva;*/

	//Mostramos los datos
	//$('#carro_descuento_monto').html('$ '+descuento);
	$('#carro_subtotal').html('$ ' + subtotal.toFixed(2));
	$('#carro_neto').html('$ ' + neto.toFixed(2));
	$('#carro_iva').html('$ ' + iva.toFixed(2));
	$('#carro_total').html('$ ' + total.toFixed(2)); 


	//Aca vamos a guardar los Articulos
	var articulos = [];

	//Conseguimos todos los resultados
	for(var i in resultados.data.rows){
	  var articulo = {};
	  articulo['id'] = resultados.data.rows[i].id;
	  articulo['cantidad'] = resultados.data.rows[i].cantidad;
	  articulos.push(articulo);
	}

	//Data array
	var data = {};
	data['notas'] = $('#carro_notas').val();
	data['articulos'] = JSON.stringify(articulos);

	//Api
	apix('pedidos','guardar',data, {
	  ok: function(json){
	  }
	});
}

//Elimina un articulos de la lista
function pedidos_articulo_eliminar(id){
	//Data array
	var data = {};
	data['articulo'] = id;
	
	apix('pedidos','eliminar',data, {
		ok: function(json){
	    	
	    	//Notifica
	    	notifica(	'Articulo eliminado',
	    				'El articulo se eliminó con éxito',
	    				'informacion',
	    				'info'
	    			);

	    	//Update
	    	articulosUpdate();
		}
	});
}

//Agregar
function pedidos_agregar(id){

	//Data array
	var data = {};
	data['cantidad'] = 1;
	data['articulo'] = id;
	
	if(typeof($('#pedidos_agregar_cantidad').val()) != 'undefined') 
		data['cantidad'] = $('#pedidos_agregar_cantidad').val();
	
	apix('pedidos','agregar',data, {
		ok: function(json){
	    	
	    	if(json.result.existe){


				//Notifica
				notifica(	'El articulo ya existe', 
							'Ya tiene pedidas '+json.result.existe+' unidades del articulo que intenta agregar al pedido',
							'informacion',
							'info'
						);
	    	}else{

				//Notifica
				notifica(	'Articulo agregado', 
							'El articulo se agrego al Pedido',
							'informacion',
							'info'
						);

				//Update
				cambiar_modo('carro');
	    	}
		}
	});
}

//Descartar el Pedido
function pedidos_descartar(){

	if (confirm('¿Está seguro que desea quitar todos los articulos del pedido?')) {
		//Llamamos a la API
		apix('pedidos','descartar',{}, {
			ok: function(json){
		    	
		    	//Notifica
		    	notifica(	'Pedido descartado',
		    				'se han quitado todos los artículos del Pedido',
		    				'informacion',
		    				'info'
		    			);

		    	//Update
		    	cambiar_modo('carro');
			}
		});
	} else {
	}
}


//Confirmar el Pedido
function pedidos_confirmar(){

	console.log(resultados.data.rows.length);

	if( resultados.data.rows.length == 0){
		//Notifica
		notifica(	'Pedido vacio',
					'El pedido no tiene articulos, agregue un articulo antes de enviarlo.',
					'informacion',
					'info'
				);
		return;
	}

	//Aca vamos a guardar los Articulos
	var articulos = [];

	//Conseguimos todos los resultados
	for(var i in resultados.data.rows){
	  var articulo = {};
	  articulo['id'] = resultados.data.rows[i].id;
	  articulo['cantidad'] = resultados.data.rows[i].cantidad;
	  articulos.push(articulo);
	}

	//Data array
	var data = {};
	data['notas'] = $('#carro_notas').val();
	data['articulos'] = JSON.stringify(articulos);

	//Deberiamos mostrar un cartel, informando
	//que si el cliente confirma las modificaciones
	//debera informarlas por email o telefonicamente
	$('#pedidosModal').modal('hide');


	//Creamos el popup
	abrir_modal('pedidosConfirmar', 600, false);

	//Mostramos la ventana
	$('#pedidosConfirmarModal').modal({show:true});

	//Guardamos la data del pedido
	$('#pedidosConfirmarModal').data('pedido',data);

	//Titulo...
	$('#pedidosConfirmarModal_titulo').html('Confirmar Pedido');

	//Cargando...
	$('#pedidosConfirmarModal_container').html('Esta por enviar el pedido, por favor <b>revise todos los datos antes de confirmar</b>. </br>En cuanto confirme, una copia de la nota de pedido le estara llegando a su email.</br> </br> <div class="alert alert-info" role="alert">Tenga en cuenta que, al confirmar, prepararemos y despacharemos el pedido lo antes posible. Es por eso que recomendamos que revise antes todos los datos. Si desea <b>cancelar</b> o <b>modificar cantidades</b>, podra hacerlo</br></br> <span class="glyphicon glyphicon-envelope" aria-hidden="true"></span> Enviando un email a <b>pedidos@distribuidoralibertad.com</b></br><span class="glyphicon glyphicon-star" aria-hidden="true"></span> Comunicandose al <b>(0223) 474-1222</b></div>');


	$('#pedidosConfirmarModal_footer').append('<button type="button" class="btn btn-warning" style="display: block; width: 49%; margin: 0px 0px 10px 0px; float: left;" data-dismiss="modal">Volver</button>');

	//Agregamos los botones al footer
	$('#pedidosConfirmarModal_footer').append('<button type="button" class="btn btn-success" style="display: block; width: 49%; margin: 0px 0px 10px 0px; float: right;" onclick="pedidos_confirmar_ok();">Confirmar</button>');
}

//Finalizar
function pedidos_confirmar_ok(){

	//Titulo...
	$('#pedidosConfirmarModal_titulo').html('Enviando Pedido...');

	//Cargando...
	$('#pedidosConfirmarModal_container').html( cargando() );

	//Footer...
	$('#pedidosConfirmarModal_footer').html('');

	//Recuperamos el pedido
	var data = $('#pedidosConfirmarModal').data('pedido');

	//Llamamos a la API
	apix('pedidos','confirmar',data, {
		ok: function(json){
	    	
	    	//Notifica
	    	notifica(	'Pedido confirmado',
	    				'En breve comenzaremos a preparar su pedido. Le enviamos un email con una copia del mismo.',
	    				'confirmacion',
	    				'success'
	    			);

	    	//Update
	    	$('#pedidosConfirmarModal').modal('hide');
	    	articulosUpdate();
		}
	});
}

function pedidos_imprimir(){
 
	if(resultados.data.rows.length){
		$('body').append('<iframe class="iframe_descargas" style="display:none;" src="descargas.php?modo=pedido"></iframe>');
		//pasados 10 segundos quitamos todos los iframes
		setTimeout(function(){
		$('.iframe_descargas').remove();
		}, 10000);
	}else{
		//Notifica
		notifica(	'Pedido vacio',
					'Para poder imprimir una copia del pedido, antes debe agregar articulos al mismo',
					'informacion',
					'info'
				);
	}
}


{/if}

{if="$GLOBALS['permisos']['pedidos_reporte_listar']"}

function pedidos_reporte_listar(){

	//Creamos el popup
	abrir_modal('pedidosReporte', 650, false);

	//Mostramos la ventana
	$('#pedidosReporteModal').modal({show:true});

	//Titulo...
	$('#pedidosReporteModal_titulo').html('Pedidos');

	//Cargando...
	$('#pedidosReporteModal_container').html( cargando() );

	//Footer...
	$('#pedidosReporteModal_footer').html('');

	//Traemos los datos de la API
	apix('pedidos','reporte_listar',{}, {
		ok: function(json){

			var tabla = generaTabla({
							id_tabla: 'pedidosReporte',
							items: json.result.pedidos,
							ajaxModulo: 'pedidos',
							ajaxAccion: 'reporte_listar',
							keys_busqueda: ['id','grupo','accion'],
							key_nombre: null,
							max_height: 450,
							campobusqueda: false,
							funcion_activado: function(el){

								var html = '<b>'+ $(el).data('cliente') + '</b> - ' + $(el).data('razon_social') + '<br/>';
								html += '<b>Pedido:</b> '+ $(el).data('comprobante') + ' <br/>';
								html += '<b>Fecha:</b> ' + $(el).data('fecha') + '<br/>';
								html += '<b>Hora:</b> ' + $(el).data('hora');
								
								//Impreso
								if(parseInt($(el).data('impreso'))) html += '<span class="label label-success" style="padding: 6px 7px; font-size: 10px; position: absolute; top: 10px; right: 15px;">IMPRESO</span>';
								
								//Pendiente
								if(!parseInt($(el).data('impreso'))) html += '<span class="label label-warning" style="padding: 6px 7px; font-size: 10px; position: absolute; top: 10px; right: 15px;">PENDIENTE...</span>';

								//Reimprimir
								html += '<span class="label label-primary" style="padding: 6px 7px; font-size: 10px; position: absolute; top: 35px; right: 15px;" onclick="pedidos_reporte_reimprimir('+$(el).data('comprobante')+')">REIMPRIMIR</span>';

								//Descargar
								html += '<span class="label label-primary" style="padding: 6px 7px; font-size: 10px; position: absolute; top: 60px; right: 15px;" onclick="window.open(\'./pedidos/pedido_'+$(el).data('comprobante')+'.pdf\',\'_blank\');">DESCARGAR PDF</span>';

								$(el).html(html);
							},
							funcion_desactivado: function(el){
								var html = '<b>'+ $(el).data('cliente') + '</b> - ' + $(el).data('razon_social') + '<br/>';
								html += '<b>Pedido:</b> '+ $(el).data('comprobante') + ' <br/>';
								html += '<b>Fecha:</b> ' + $(el).data('fecha') + '<br/>';
								html += '<b>Hora:</b> ' + $(el).data('hora');
								
								//Impreso
								if(parseInt($(el).data('impreso'))) html += '<span class="label label-success" style="padding: 6px 7px; font-size: 10px; position: absolute; top: 10px; right: 15px;">IMPRESO</span>';
								
								//Pendiente
								if(!parseInt($(el).data('impreso'))) html += '<span class="label label-warning" style="padding: 6px 7px; font-size: 10px; position: absolute; top: 10px; right: 15px;">PENDIENTE...</span>';

								//Reimprimir
								html += '<span class="label label-primary" style="padding: 6px 7px; font-size: 10px; position: absolute; top: 35px; right: 15px;" onclick="pedidos_reporte_reimprimir('+$(el).data('comprobante')+')">REIMPRIMIR</span>'; 

								//Descargar
								html += '<span class="label label-primary" style="padding: 6px 7px; font-size: 10px; position: absolute; top: 60px; right: 15px;" onclick="window.open(\'./pedidos/pedido_'+$(el).data('comprobante')+'.pdf\',\'_blank\');">DESCARGAR PDF</span>';
									
								$(el).html(html);
							}
						});

			//Agregamos los tooltips
			tabla.tooltip({selector: '[data-toggle="tooltip"]', placement: 'left', animation: false});

			//Generamos el listado
			$('#pedidosReporteModal_container').html('');
			$('#pedidosReporteModal_container').append(tabla);


			//Agregamos scrollbars
			$('#pedidosReporteModal_container .scrollable').mCustomScrollbar({theme: 'minimal-dark',scrollInertia: 100, mouseWheel:{ scrollAmount: 70 }});


			//Append Cancel Button
			$('#pedidosReporteModal_footer').append('<button type="button" data-dismiss="modal" class="btn btn-warning" style="display: block; width: 100%; margin: 10px auto;">Cerrar</button>');
		}
	});
}

function pedidos_reporte_reimprimir(pedido){
	
	var data = {pedido: pedido};

	//Llamamos a la API
	apix('pedidos','reporte_reimprimir',data, {
		ok: function(json){
	    	
	    	//Notifica
	    	notifica(	'Pedido reimpreso',
	    				'En breve se reimprimirá el pedido.',
	    				'confirmacion',
	    				'success'
	    			);
		}
	});
}



{/if}

</script>