<script>

var busqueda_avanzada_config = {
	campos: [
		{ 
			id: 			"fabricante",
			label: 			"Fabricante",
			tipo: 			"select2",
			mostrarEditar: 	true,
			mostrarCrear: 	true,
			config: 		{
				minimumInputLength: 0,
				placeholder: $(this).attr('placeholder'),
				allowClear: true,
				width: 'resolve',
				id: function(e) { return e.id; },
				formatNoMatches: function() { return 'Sin resultados'; },
				formatSearching: function(){ return "Buscando..."; },
				ajax: {
				    url: "api.php",
				    dataType: 'json',
				    quietMillis: 200,
				    data: function(term, page) {
				        return {
				            module: 'busqueda',
				            run: 'advSearch_fabricante',
				            q: term,
				            p: page
				        };
				    },
				    results: function(data, page ) {
				        //Si el ususario no esta logueado refresh
				        if(!data.logued) location.reload();

				        //Calcula si existen mas resultados a mostrar
				        var more = (page * 40) < data.result.total;

				        //Devolvemos el valor de more para que selec2
				        //sepa que debemos cargar mas resultados.
				        return {results: data.result.items, more: more};
				    }
				},
				formatInputTooShort: function () {
					return "";
				},
				formatResult: function(item) {
					
					var logo = '';
					if(typeof item.logo != 'undefined')
						logo += '<div style="height: 50px; width: 50px; margin-right: 15px; display: inline-block;"><img src="./fabricantes/'+item.logo+'" style="height: 100%; width: 100%;"></div>'

					return logo + item.nombre;
				},
				formatSelection: function(item) { 
					return item.nombre;
				},
				initSelection : function (element, callback) {
					var elementText = $(element).attr('data-init-text');
				}
			}
		},
		{ 
			id: 			"vehiculo",
			label: 			"Vehículo",
			tipo: 			"select2",
			mostrarEditar: 	true,
			mostrarCrear: 	true,
			config: 		{
				minimumInputLength: 0,
				placeholder: $(this).attr('placeholder'),
				allowClear: true,
				width: 'resolve',
				id: function(e) { return e.id; },
				formatNoMatches: function() { return 'Sin resultados'; },
				formatSearching: function(){ return "Buscando..."; },
				ajax: {
				    url: "api.php",
				    dataType: 'json',
				    quietMillis: 200,
				    data: function(term, page) {
				        return {
				            module: 'busqueda',
				            run: 'advSearch_vehiculo',
				            q: term,
				            p: page
				        };
				    },
				    results: function(data, page ) {
				        //Si el ususario no esta logueado refresh
				        if(!data.logued) location.reload();

				        //Calcula si existen mas resultados a mostrar
				        var more = (page * 40) < data.result.total;

				        //Devolvemos el valor de more para que selec2
				        //sepa que debemos cargar mas resultados.
				        return {results: data.result.items, more: more};
				    }
				},
				formatInputTooShort: function () {
					return "";
				},
				formatResult: function(item){

					var id = item.id.split('#');

					//Todos los modelos
					if(id[1] == '-1') 
						return item.nom + '<i class="badge" style="padding: 4px 6px; float: none; border-radius: 2px; background-color: #e6e6e6; margin-left: 7px; color: #444444; font-size: 11px; border: solid 1px #dadada;">Todos los modelos</i> ';
					
					//Solo nombre
					return item.nom;
				},
				formatSelection: function(item) { 
					var id = item.id.split('#');

					//Todos los modelos
					if(id[1] == '-1') 
						return item.nom + '<i class="badge" style="padding: 4px 6px; float: none; border-radius: 2px; background-color: #e6e6e6; margin-left: 7px; color: #444444; font-size: 11px; border: solid 1px #dadada;">Todos los modelos</i> ';
					
					//Solo nombre
					return item.nom;
				},
				initSelection : function (element, callback) {
					//var elementText = $(element).attr('data-init-text');
				}
			}
		},
		{	
			id: 			"vehiculo_filas_individuales",
			label: 			"",
			tipo: 			"checkbox",
			mostrarEditar: 	true,
			mostrarCrear: 	true,
			config: 		{
							claseActivo: 'btn-info btn-sm',
							claseInactivo: 'btn-sm',
							valueActivo: '1',
							valueInactivo: '-1',
							valueDefault: '-1',
							labelActivo: function(el){
								el.html('<i class="fa fa-check-square-o" style="width: 15px;"></i> Mostrar cada Vehículo en una fila individual');
							},
							labelInactivo: function(el){
								el.html('<i class="fa fa-square-o" style="width: 15px;"></i> Mostrar cada Vehículo en una fila individual');
							}
			}
		},
		{ 
			id: 			"rubro",
			label: 			"Rubro",
			tipo: 			"select2",
			mostrarEditar: 	true,
			mostrarCrear: 	true,
			config: 		{
				minimumInputLength: 0,
				placeholder: $(this).attr('placeholder'),
				allowClear: true,
				width: 'resolve',
				id: function(e) { return e.id; },
				formatNoMatches: function() { return 'Sin resultados'; },
				formatSearching: function(){ return "Buscando..."; },
				ajax: {
				    url: "api.php",
				    dataType: 'json',
				    quietMillis: 200,
				    data: function(term, page) {
				        return {
				            module: 'busqueda',
				            run: 'advSearch_rubro',
				            q: term,
				            p: page
				        };
				    },
				    results: function(data, page ) {
				        //Si el ususario no esta logueado refresh
				        if(!data.logued) location.reload();

				        //Calcula si existen mas resultados a mostrar
				        var more = (page * 40) < data.result.total;

				        //Devolvemos el valor de more para que selec2
				        //sepa que debemos cargar mas resultados.
				        return {results: data.result.items, more: more};
				    }
				},
				formatInputTooShort: function () {
					return "";
				},
				formatResult: function(item) {


					if(parseInt(item.depth) == 0)
					return '<i class="badge" style="padding: 4px 6px; float: none; border-radius: 2px; background-color: #e6e6e6; margin-left: 0px; color: #444444; font-size: 11px; border: solid 1px #dadada;">'+item.bread+'</i> ';

					if(parseInt(item.depth) == 1)
					return '<i class="badge" style="padding: 4px 6px; float: none; border-radius: 2px; background-color: #e6e6e6; margin-left: 25px; color: #444444; font-size: 11px; border: solid 1px #dadada;">'+item.bread+'</i> ';

					if(parseInt(item.depth) == 2)
					return '<i class="badge" style="padding: 4px 6px; float: none; border-radius: 2px; background-color: #e6e6e6; margin-left: 50px; color: #444444; font-size: 11px; border: solid 1px #dadada;">'+item.bread+'</i> ';

					if(parseInt(item.depth) == 3)
					return '<i class="badge" style="padding: 4px 6px; float: none; border-radius: 2px; background-color: #e6e6e6; margin-left: 50px; color: #444444; font-size: 11px; border: solid 1px #dadada;">'+item.bread+'</i> ';

					return item.bread;
				},
				formatSelection: function(item) {
					return item.bread;
				},
				initSelection : function (element, callback) {
					var elementText = $(element).attr('data-init-text');
				}
			}
		},
		{	
			id: 			"busqueda_dim",
			label: 			"Dimensiones",
			tipo: 			"checkbox",
			mostrarEditar: 	true,
			mostrarCrear: 	true,
			config: 		{
							claseActivo: 'btn-info btn-sm',
							claseInactivo: 'btn-sm',
							valueActivo: '1',
							valueInactivo: '-1',
							valueDefault: '-1',
							labelActivo: function(el){
								el.html('<i class="fa fa-check-square-o" style="width: 15px;"></i> Buscar por dimensiones de la pieza <i class="fa fa-arrow-circle-right" style="float: right; margin-top: 3px;"></i>');
							},
							labelInactivo: function(el){
								el.html('<i class="fa fa-square-o" style="width: 15px;"></i> Buscar por dimensiones de la pieza <i class="fa fa-arrow-circle-right" style="float: right; margin-top: 3px;"></i>');
							}
			}
		},
		{	
			id: 			"dim_en_columnas",
			label: 			"Activo",
			tipo: 			"checkbox",
			mostrarEditar: 	true,
			mostrarCrear: 	true,
			config: 		{
							claseActivo: 'btn-info btn-sm',
							claseInactivo: 'btn-sm',
							valueActivo: '1',
							valueInactivo: '-1',
							valueDefault: '-1',
							labelActivo: function(el){
								el.html('<i class="fa fa-check-square-o" style="width: 15px;"></i> Mostrar dimensiones en columnas individuales');
							},
							labelInactivo: function(el){
								el.html('<i class="fa fa-square-o" style="width: 15px;"></i> Mostrar dimensiones en columnas individuales');
							}
			}
		},
	]
};

function busqueda_avanzada(){

	if(!$('#busqueda_avanzadaModal').length){
		//Creamos el popup
		abrir_modal('busqueda_avanzada', 450, false);

		//Mostramos la ventana
		$('#busqueda_avanzadaModal').modal({show:true});

		//Titulo...
		$('#busqueda_avanzadaModal_titulo').html('Busqueda Avanzada <i class="fa fa-cog fa-spin" id="busqueda_avanzada_cargando" style="float: right; font-size: 20px; margin: 0px 20px 0px -40px;"></i>');
	}

	//Mostramos cargando
	$('#busqueda_avanzada_cargando').show();


	//Buscamos los datos de la busqueda
	apix('busqueda','advSearch_getSessionVars',{}, {
		ok: function(json){

			//variables de sesion
			var vars = json.result.vars;

			//Contenido
			var basica = $('<div id="busqueda_avanzada_basica" class="scrollable" style="max-height: 500px; overflow-y: hidden; float:left;"></div>');

			//Codigo a mano
			var codigo = $('<div style="display: table-cell; vertical-align: bottom;"></div>');

			//Prefijo
			codigo.append( $('<div style="width: 100px; display: inline-block;"></div>').append( genera_campo({ 
					id: 			"prefijo",
					label: 			"Prefijo",
					tipo: 			"select2",
					mostrarEditar: 	true,
					mostrarCrear: 	true,
					config: 		{
						minimumInputLength: 0,
						placeholder: $(this).attr('placeholder'),
						allowClear: true,
						width: 'resolve',
						id: function(e) { return e.id; },
						formatNoMatches: function() { return 'Sin resultados'; },
						formatSearching: function(){ return "Buscando..."; },
						ajax: {
						    url: "api.php",
						    dataType: 'json',
						    quietMillis: 200,
						    data: function(term, page) {
						        return {
						            module: 'busqueda',
						            run: 'advSearch_prefijo',
						            q: term,
						            p: page
						        };
						    },
						    results: function(data, page ) {
						        //Si el ususario no esta logueado refresh
						        if(!data.logued) location.reload();

						        //Calcula si existen mas resultados a mostrar
						        var more = (page * 40) < data.result.total;

						        //Devolvemos el valor de more para que selec2
						        //sepa que debemos cargar mas resultados.
						        return {results: data.result.items, more: more};
						    }
						},
						formatInputTooShort: function () {
							return "";
						},
						formatResult: function(item) {
							return item.nombre;
						},
						formatSelection: function(item) { 
							return item.nombre;
						},
						initSelection : function (element, callback) {
							var elementText = $(element).attr('data-init-text');
						}
					}
					},
					'advSearch',
					true)
				)
			);

			//Codigo
			codigo.append( $('<div style="width: 170px; display: inline-block; margin-left: 10px;"></div>').append( genera_campo({ 
					id: 			"codigo",
					label: 			"Codigo",
					tipo: 			"select2",
					mostrarEditar: 	true,
					mostrarCrear: 	true,
					config: 		{
						minimumInputLength: 0,
						placeholder: $(this).attr('placeholder'),
						allowClear: true,
						width: 'resolve',
						id: function(e) { return e.id; },
						formatNoMatches: function() { return 'Sin resultados'; },
						formatSearching: function(){ return "Buscando..."; },
						ajax: {
						    url: "api.php",
						    dataType: 'json',
						    quietMillis: 200,
						    data: function(term, page) {
						        return {
						            module: 'busqueda',
						            run: 'advSearch_codigo',
						            q: term,
						            p: page
						        };
						    },
						    results: function(data, page ) {
						        //Si el ususario no esta logueado refresh
						        if(!data.logued) location.reload();

						        //Calcula si existen mas resultados a mostrar
						        var more = (page * 40) < data.result.total;

						        //Devolvemos el valor de more para que selec2
						        //sepa que debemos cargar mas resultados.
						        return {results: data.result.items, more: more};
						    }
						},
						formatInputTooShort: function () {
							return "";
						},
						formatResult: function(item) {
							return item.nombre;
						},
						formatSelection: function(item) { 
							return item.nombre;
						},
						initSelection : function (element, callback) {
							var elementText = $(element).attr('data-init-text');
						}
					}
					},
					'advSearch',
					true)
				)
			);

			//Sufijo
			codigo.append( $('<div style="width: 102px; display: inline-block; margin-left: 10px;"></div>').append( genera_campo({ 
					id: 			"sufijo",
					label: 			"Sufijo",
					tipo: 			"select2",
					mostrarEditar: 	true,
					mostrarCrear: 	true,
					config: 		{
						minimumInputLength: 0,
						placeholder: $(this).attr('placeholder'),
						allowClear: true,
						width: 'resolve',
						id: function(e) { return e.id; },
						formatNoMatches: function() { return 'Sin resultados'; },
						formatSearching: function(){ return "Buscando..."; },
						ajax: {
						    url: "api.php",
						    dataType: 'json',
						    quietMillis: 200,
						    data: function(term, page) {
						        return {
						            module: 'busqueda',
						            run: 'advSearch_sufijo',
						            q: term,
						            p: page
						        };
						    },
						    results: function(data, page ) {
						        //Si el ususario no esta logueado refresh
						        if(!data.logued) location.reload();

						        //Calcula si existen mas resultados a mostrar
						        var more = (page * 40) < data.result.total;

						        //Devolvemos el valor de more para que selec2
						        //sepa que debemos cargar mas resultados.
						        return {results: data.result.items, more: more};
						    }
						},
						formatInputTooShort: function () {
							return "";
						},
						formatResult: function(item) {
							return item.nombre;
						},
						formatSelection: function(item) { 
							return item.nombre;
						},
						initSelection : function (element, callback) {
							var elementText = $(element).attr('data-init-text');
						}
					}
					},
					'advSearch',
					true)
				)
			);

			basica.append(codigo);

			//Agregamos todos los campos
			for(var k in busqueda_avanzada_config.campos)
				basica.append( genera_campo(busqueda_avanzada_config.campos[k], 'advSearch' ,true) );

			//Busqueda por dimensiones
			$('#advSearch_busqueda_dim',basica).on('mousedown',function(){
				console.log('mousedown');
				if ($('#advSearch_rubro',basica).val_() == null){
					notifica(	'Rubro Vacio',
								'Para poder buscar por dimensiones en columnas debe seleccionar un Rubro',
								'informacion',
								'info'
							);
					$(this).data('stop',true);
				}else{
					$(this).data('stop',false);
				}
			});

			//Listar dimensiones en columnas separadas
			$('#advSearch_busqueda_dim',basica).on('click',function(){
				console.log('segundo click');
				apix('busqueda','advSearch_setSessionVar',{variable: 'advSearch_busqueda_dim', value: $(this).val()},{
					ok: function(json){
						busqueda_avanzada();
						}
					});
			});

			//Listar dimensiones en columnas separadas
			$('#advSearch_dim_en_columnas',basica).on('mousedown',function(e){
				if ($('#advSearch_rubro',basica).val_() == null){
					notifica(	'Rubro Vacio',
								'Para poder listar las dimensiones en columnas debe seleccionar un Rubro',
								'informacion',
								'info'
							);
					$(this).data('stop',true);
				}else{
					$(this).data('stop',false);
				}
			});

			//Listar dimensiones en columnas separadas
			$('#advSearch_dim_en_columnas',basica).on('click',function(){
				console.log('segundo click');
				apix('busqueda','advSearch_setSessionVar',{variable: 'advSearch_dim_en_columnas', value: $(this).val()});
			});

			//Listar dimensiones en columnas separadas
			$('#advSearch_vehiculo_filas_individuales',basica).on('click',function(){
				var val = $(this).val();
				apix('busqueda','advSearch_setSessionVar',{variable: 'advSearch_vehiculo_filas_individuales', value: $(this).val()});
			});

			//Eventos para Prefijo
			$('#advSearch_prefijo',basica).on("select2-selecting", function(e){
				apix('busqueda','advSearch_setSessionVar',{variable: 'advSearch_prefijo', value: e.val});
			}).on("select2-removed", function(e) {
				apix('busqueda','advSearch_setSessionVar',{variable: 'advSearch_prefijo', value: '-1'});
			});

			//Eventos para codigo
			$('#advSearch_codigo',basica).on("select2-selecting", function(e){
				apix('busqueda','advSearch_setSessionVar',{variable: 'advSearch_codigo', value: e.val});
			}).on("select2-removed", function(e) {
				apix('busqueda','advSearch_setSessionVar',{variable: 'advSearch_codigo', value: '-1'});
			});

			//Eventos para Sufijo
			$('#advSearch_sufijo',basica).on("select2-selecting", function(e){
				apix('busqueda','advSearch_setSessionVar',{variable: 'advSearch_sufijo', value: e.val});
			}).on("select2-removed", function(e) {
				apix('busqueda','advSearch_setSessionVar',{variable: 'advSearch_sufijo', value: '-1'});
			});

			//Eventos para Fabricante
			$('#advSearch_fabricante',basica).on("select2-selecting", function(e){
				apix('busqueda','advSearch_setSessionVar',{variable: 'advSearch_fabricante', value: e.val});
			}).on("select2-removed", function(e) {
				apix('busqueda','advSearch_setSessionVar',{variable: 'advSearch_fabricante', value: '-1'});
			});

			//Eventos para Vehiculo
			$('#advSearch_vehiculo',basica).on("select2-selecting", function(e){
				apix('busqueda','advSearch_setSessionVar',{variable: 'advSearch_vehiculo', value: e.val});
			}).on("select2-removed", function(e) {
				apix('busqueda','advSearch_setSessionVar',{variable: 'advSearch_vehiculo', value: '-1'});
			});

			//Eventos para Rubro
			$('#advSearch_rubro',basica).on("select2-selecting", function(e){
				apix('busqueda','advSearch_setSessionVar',{variable: 'advSearch_rubro', value: e.val}, {
            		ok: function(json){
						busqueda_avanzada();
					}
				});
			}).on("select2-removed", function(e) {
				apix('busqueda','advSearch_setSessionVar',{variable: 'advSearch_rubro', value: '-1'}, {
            		ok: function(json){
						busqueda_avanzada();
					}
				});
			});


			//Values para S2
			if(vars.prefijo.id != '-1') $('#advSearch_prefijo',basica).val_({id: vars.prefijo.id, nombre: vars.prefijo.nom});
			if(vars.codigo.id != '-1') $('#advSearch_codigo',basica).val_({id: vars.codigo.id, nombre: vars.codigo.nom});
			if(vars.sufijo.id != '-1') $('#advSearch_sufijo',basica).val_({id: vars.sufijo.id, nombre: vars.sufijo.nom});
			if(vars.fabricante.id != '-1') $('#advSearch_fabricante',basica).val_({id: vars.fabricante.id, nombre: vars.fabricante.nom});
			if(vars.vehiculo.id != '-1') $('#advSearch_vehiculo',basica).val_({id: vars.vehiculo.id, nom: vars.vehiculo.nom});
			if(vars.rubro.id != '-1') $('#advSearch_rubro',basica).val_({id: vars.rubro.id, bread: vars.rubro.nom});

			//Values para Checkboxes
			$('#advSearch_vehiculo_filas_individuales',basica).val_(vars.vehiculo_filas_individuales);
			$('#advSearch_busqueda_dim',basica).val_(vars.busqueda_dim);
			$('#advSearch_dim_en_columnas',basica).val_(vars.dim_en_columnas);

			//Mostramos la busqueda por dimensiones si es que seleccionamos un rubro
			//if( (vars.rubro.id != '-1') && (typeof vars.dimensiones !== 'undefined')) 
			//	$('#advSearch_busqueda_dim',basica).show();

			//Mostramos busqueda por columnas individuales solo si estamos en busqueda dimensional
			if(vars.busqueda_dim == '1') $('#advSearch_dim_en_columnas',basica).show();
				

			//Por defecto el contenedor es simple en caso de tener una busqueda dimensional
			//lo transformamos para que sea doble tamaño y alberque las dimensiones.
			var contenedor = $('<div class="row"><div class="col-md-12" id="busqueda_avanzada_contenedor_basica"></div><div class="col-md-6" id="busqueda_avanzada_contenedor_dimensional" style="display: none;"></div></div>');


			//Si estamos haciendo una busqueda por dimensiones
			if((vars.busqueda_dim == '1')){

				//El contenedor ahora va a albergar a dos bloques de busqueda la basica y la dimensional
				contenedor = $('<div class="row"><div class="col-md-6" id="busqueda_avanzada_contenedor_basica"></div><div class="col-md-6" id="busqueda_avanzada_contenedor_dimensional"></div></div>');

				var dimensional = $('<div class="scrollable" style="max-height: 500px; overflow-y: hidden;"></div>');

				for(var k in vars.dimensiones){
					
					//Campo
					dimensional.append( genera_campo({ 
							id: 			"dimension_"+vars.dimensiones[k].id,
							label: 			vars.dimensiones[k].label,
							tipo: 			"select2",
							mostrarEditar: 	true,
							mostrarCrear: 	true,
							config: 		{
								minimumInputLength: 0,
								placeholder: $(this).attr('placeholder'),
								allowClear: true,
								width: 'resolve',
								id: function(e) { return e.id; },
								formatNoMatches: function() { return 'Sin resultados'; },
								formatSearching: function(){ return "Buscando..."; },
								ajax: {
								    url: "api.php",
								    dataType: 'json',
								    quietMillis: 200,
								    data: function(term, page) {
								        return {
								            module: 'busqueda',
								            run: 'advSearch_dimension',
								            dimension: this.attr('id').replace("advSearch_dimension_", ""),
								            q: term,
								            p: page
								        };
								    },
								    results: function(data, page ) {
								        //Si el ususario no esta logueado refresh
								        if(!data.logued) location.reload();

								        //Calcula si existen mas resultados a mostrar
								        var more = (page * 40) < data.result.total;

								        //Devolvemos el valor de more para que selec2
								        //sepa que debemos cargar mas resultados.
								        return {results: data.result.items, more: more};
								    }
								},
								formatInputTooShort: function () {
									return "";
								},
								formatResult: function(item) {
									return item.nombre;
								},
								formatSelection: function(item) { 
									return item.nombre;
								},
								initSelection : function (element, callback) {
									var elementText = $(element).attr('data-init-text');
								}
							}
							},
							'advSearch',
							true)
					);
		
					//Eventos
					$('#advSearch_dimension_'+vars.dimensiones[k].id, dimensional)
					.on("select2-selecting", function(e) {
						var id = $(this).attr('id').replace("advSearch_dimension_", "");
						apix('busqueda','advSearch_setSessionVar',{variable: 'advSearch_dimension_'+id, value: e.val});
					}).on("select2-removed", function(e) {
						var id = $(this).attr('id').replace("advSearch_dimension_", "");
						apix('busqueda','advSearch_setSessionVar',{variable: 'advSearch_dimension_'+id, value: '-1'});
					});

					//Values
					if(vars.dimensiones[k].value.id != '-1'){
						$('#advSearch_dimension_'+vars.dimensiones[k].id, dimensional).val_({
							id: vars.dimensiones[k].value.id,
							nombre: vars.dimensiones[k].value.nom
						});
					}
				}
			}

			//Agregamos la busqueda basica y la dimensional al contenedor
			
			if(typeof dimensional !== 'undefined'){
				abrir_modal('busqueda_avanzada', 900, false);
				$('#busqueda_avanzada_contenedor_basica',contenedor).append(basica);
				$('#busqueda_avanzada_contenedor_dimensional',contenedor).append(dimensional);
			}else{
				abrir_modal('busqueda_avanzada', 450, false);
				$('#busqueda_avanzada_contenedor_basica',contenedor).append(basica);
			}

			$('#busqueda_avanzada_cargando').hide();
			$('#busqueda_avanzadaModal_container').html( contenedor );

			//Agregamos scrollbars
			$('#busqueda_avanzadaModal_container .scrollable').mCustomScrollbar({theme: 'minimal-dark',scrollInertia: 100, mouseWheel:{ scrollAmount: 70 }});

			//Si los botones ya los habiamos agregado antes
			//No los volvemos a agregar...
			if( !$('#busqueda_avanzadaModal_footer button').length){
				//Contenedor de los botones
				var contenedor_footer = $('<div style="float: right; width: 394px; margin-right: 0px;" class="row"></div>');

				//Boton Cerrar
				contenedor_footer.append('<div class="col-md-4" style="padding: 0px 5px 0px 0px;"><button type="button" data-dismiss="modal" class="btn btn-danger" style="width: 100%">Cerrar</button></div>');

				//Boton Limpiar
				contenedor_footer.append('<div class="col-md-4" style="padding: 0px 5px 0px 5px;"><button type="button" class="btn btn-warning" style="width: 100%" onclick="busqueda_avanzada_reset();">Limpiar</button></div>');

				//Boton Buscar
				contenedor_footer.append('<div class="col-md-4" data-dismiss="modal" style="padding: 0px 0px 0px 5px;"><button type="button" class="btn btn-primary" style="width: 100%">Buscar</button></div>');

				//Agregamos el contenedor al footer
				$('#busqueda_avanzadaModal_footer').append(contenedor_footer);
			}
		}
	});
}

function busqueda_avanzada_reset(){
	//Llamada a la API
	apix('busqueda','advSearch_reset',{}, {
		ok: function(json){
			busqueda_avanzada();
		}
	});
}

//Actualizamos los articulos
function busqueda_avanzadaModal_onClose(){
	articulosUpdate('busqueda');
}

</script>