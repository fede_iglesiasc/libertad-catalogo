<script type="text/javascript">

{if="$GLOBALS['permisos']['menu_administrar']"}

//Config Usuarios
var menu_config = {
	modName: 					'menu',
	modLabelSingular: 			'Menu',
	modLabelPlural: 			'Menu',
	creaLabel: 					'Agregado',
	editaLabel: 				'Editado',
	eliminaLabel: 				'Eliminado',
	nuevoLabel: 				'Nuevo',
	artLabel: 					'El',
	max_height_edita_agrega: 	'300',
	max_height_lista: 			'300',
	data: 						{}, 
	campos:[
		{	
			id: 			"click",
			label: 			"OnCLick JS (accion al hacer click al item)",
			tipo: 			"textarea",
			mostrarEditar: 	true,
			mostrarCrear: 	true
		},
		{	
			id: 			"permiso",
			label: 			"Permiso PHP (retorna boolean para saber permiso)",
			tipo: 			"textarea",
			mostrarEditar: 	true,
			mostrarCrear: 	true
		}
	]
};

//Lista
function menu_listar(){

	//Creamos el popup
	abrir_modal(menu_config.modName, 650, false);

	//Mostramos la ventana
	$('#'+menu_config.modName+'Modal').modal({show:true});

	//Titulo...
	$('#'+menu_config.modName+'Modal_titulo').html(menu_config.modLabelPlural);

	//Cargando...
	$('#'+menu_config.modName+'Modal_container').html( cargando() );

	//Footer...
	$('#'+menu_config.modName+'Modal_footer').html('');

	//Traemos los datos de la API
	apix(menu_config.modName,'listar',{}, {
		ok: function(json){

			var html = '<div style="margin: 10px;"> <button type="button" class="btn btn-success btn-sm" onclick="'+menu_config.modName+'_listar_crear();"><i class="glyphicon glyphicon-asterisk"></i> Crear</button> <button type="button" class="btn btn-warning btn-sm" onclick="'+menu_config.modName+'_listar_renombrar();"><i class="glyphicon glyphicon-pencil"></i> Renombrar</button> <button type="button" class="btn btn-danger btn-sm" onclick=" confirma(\'Eliminar Item\',\'Si usted elimina el Item, todos los subitems se perderán de forma permanente.\',\'exclamacion\',\'danger\',\'menu_listar_eliminar();\')" ><i class="glyphicon glyphicon-remove"></i> Eliminar</button> <input type="text" value="" style="float: right; width: 200px; padding: 6px; border-radius: 3px; border: 1px solid silver; font-size: 12px;" id="'+menu_config.modName+'_listar_buscar" placeholder="Buscar" /> </div> <div id="menu_tree" class="scrollable demo" style="max-width: 100%; overflow: auto; font: 12px Verdana, sans-serif; box-shadow: 0 0 5px #ccc; margin: 10px; border-radius: 3px; min-height: 300px; max-height: 400px;"></div>';


			//Resultados
			menu_config.data = json.result;
			

			//Generamos el listado
			$('#'+menu_config.modName+'Modal_container').html('');
			$('#'+menu_config.modName+'Modal_container').append(html);


			$('#menu_tree').jstree({
			  "core" : {
			    "animation" : 1,
			    "multiple" : false,
			    "check_callback" : true,
				"themes" : {
				      "dots" : false // no connecting dots between dots
				 },
		        "data" : function (obj, cb) {
		            cb.call(this, menu_config.data);
		        }
			  },
			  "state" : { "key" : "menu" },
			  "types" : {
				"0" : {
					"icon":"glyphicon glyphicon-flash"
				},
				"1" : {
				}
			  },
			  "plugins" : [ "contextmenu", "types", "dnd", "state" ]
			}).on('rename_node.jstree', function (e,obj) {
				if(obj.node.original.type == 'new'){
					//Llamada a la API
					var data = {};
					data['parent'] = obj.node.parent;
					data['nombre'] = obj.text;
					apix('menu','crear',data, {
						ok: function(json){
					    	//Notifica
					    	notifica(	'Item Creado',
					    				'El Item se creó con éxito',
					    				'informacion',
					    				'info'
					    			);
				    		//Update
							menu_listar_update();
						}
					});
				}else{
					//Llamada a la API
					var data = {};
					data['item'] = obj.node.id;
					data['nombre'] = obj.text;
					apix('menu','renombrar',data, {
            			ok: function(json){
					    	//Notifica
					    	notifica(	'Item editado',
					    				'El Item se editó con éxito',
					    				'informacion',
					    				'info'
					    			);
					    	//Update
							menu_listar_update();
						}
					});
				}
			}).on('delete_node.jstree', function(e,obj){
				//Llamada a la API
				var data = {};
				data['item'] = obj.node.id;
				apix('menu','eliminar',data, {
            		ok: function(json){
				    	//Notifica
				    	notifica(	'Item eliminado',
				    				'El Item se eliminó con éxito',
				    				'informacion',
				    				'info'
				    			);
				    	//Update
						menu_listar_update();
					}
				});
			}).on('move_node.jstree', function(e,obj){
				//Llamada a la API
				var data = {};
				data['item'] = obj.node.id;
				data['parent'] = obj.parent;
				data['posicion'] = obj.position;
				apix('menu','mover',data, {
        			ok: function(json){
				    	//Notifica
				    	notifica(	'Item Movido',
				    				'El Item cambió su posición con éxito',
				    				'informacion',
				    				'info'
				    			);
				    	//Update
						menu_listar_update();
					}
				});
			});


			//Boton Agregar
			$('#'+menu_config.modName+'Modal_footer').append('<button type="button" class="btn btn-primary" style="display: block; width: 100%; margin: 10px auto;" onclick="menu_item_config($(\'#menu_tree\').data(\'jstree\').get_selected()[0])">Configuración</button>');

			//Append Cancel Button
			$('#'+menu_config.modName+'Modal_footer').append('<button type="button" data-dismiss="modal" class="btn btn-warning" style="display: block; width: 100%; margin: 10px auto;">Cerrar</button>');
		}
	});
}

//Actualiza Tree
function menu_listar_update(){
	apix('menu','listar',{}, {
		ok: function(json){
			menu_config.data = json.result;
			$('#menu_tree').jstree("refresh");
		}
	});
}

//Crea Item
function menu_listar_crear() {
	var ref = $('#'+menu_config.modName+'_tree').jstree(true),
		sel = ref.get_selected();
	if(!sel.length) { return false; }
	sel = sel[0];
	sel = ref.create_node(sel, {"type":"new"});
	if(sel) {
		ref.edit(sel);
	}
}

//Renombra Item
function menu_listar_renombrar() {
	var ref = $('#'+menu_config.modName+'_tree').jstree(true),
		sel = ref.get_selected();
	var data = ref.settings.core.data;

	//Si no hay seleccionado ningun elemento...
	if(!sel.length) { return false; }
	//Si hay elemento seleccionado traemos el primero
	sel = sel[0];
	ref.edit(sel);
}

//Elimina un Item
function menu_listar_eliminar(){
	var ref = $('#'+menu_config.modName+'_tree').jstree(true),
		sel = ref.get_selected();
	if(!sel.length) { return false; }
	ref.delete_node(sel,'Nuevo item');
}

//Configuración del item
function menu_item_config(id){
	//Titulo...
	$('#'+menu_config.modName+'Modal_titulo').html('Editar '+menu_config.modLabelSingular);

	//Cargando...
	$('#'+menu_config.modName+'Modal_container').html(cargando());

	//Footer...
	$('#'+menu_config.modName+'Modal_footer').html('');

	var contenido = $('<div class="scrollable" style="max-height: '+menu_config.max_height_edita_agrega+'px; overflow-y: hidden;"></div>');

	//Agregamos todos los campos
	for(var k in menu_config.campos){
		var campo = menu_config.campos[k];
		contenido.append( genera_campo(menu_config.campos[k], menu_config.modName ,true) );
	}

	var data = {};
	data['item'] = id;

	apix(menu_config.modName,'info',data, {
		ok: function(json){

			//Titulo
			$('#'+menu_config.modName+'Modal_titulo').html('Configurar Item <b>'+json.result.text+'</b>');

	    	//Llenamos los datos
	    	for(var k in menu_config.campos){
	    		var campo = menu_config.campos[k];
	    		$('#'+menu_config.modName+'_'+campo.id,contenido).val_(json.result[ campo.id ]);
	    	}

	    	//Asignamos el contenido al popup
			$('#'+menu_config.modName+'Modal_container').html(contenido);

			//Agregamos scrollbars
			$('#'+menu_config.modName+'Modal_container .scrollable').mCustomScrollbar({theme: 'minimal-dark',scrollInertia: 100, mouseWheel:{ scrollAmount: 70 }});

			//Agregamos los botones al footer
			$('#'+menu_config.modName+'Modal_footer').append('<button type="button" class="btn btn-primary" style="display: block; width: 100%; margin: 10px auto;" onclick="'+menu_config.modLabelPlural.toLowerCase()+'_item_config_confirm(\''+id+'\');">Editar '+menu_config.modLabelSingular+'</button>');
			$('#'+menu_config.modName+'Modal_footer').append('<button type="button" class="btn btn-warning" style="display: block; width: 100%; margin: 10px auto;" onclick="'+menu_config.modLabelPlural.toLowerCase()+'_listar();">Volver</button>');
		}
	});
}

//Guardar la configuracion
function menu_item_config_confirm(id){

	//Data
	var data = {};
	data['item'] = id;
	data['click'] = $('#'+menu_config.modName+'_click').val();
	data['permiso'] = $('#'+menu_config.modName+'_permiso').val();

	//DB confirm
	apix(menu_config.modName,'editar',data, {
		ok: function(json){
			notifica('Item editado','El Item del Menú se editó con éxito','confirmacion','success');
			menu_listar();
		}
	});
}

{/if}

</script>