<!-- RESULTADOS -->
<table id="layout_watable">
  <tr>
    <td id="resultados_container">
      <div id="resultados"></div>
    </td>
    <td id="carro_detalles" style="display: none;">
      <table>
        <thead><tr><th colspan="2">Detalles del Pedido</th></tr></thead>
        <tbody>
          <tr><td>Subtotal (Costo)</td><td id="carro_neto" align="right"></td></tr>
          <tr><td style="border-bottom: 1px solid grey;">IVA</td><td id="carro_iva" align="right" style="border-bottom: 1px solid grey;"></td></tr>
          <tr><td style="border-bottom: 0px; line-height: 27px; font-size: 18px; padding: 0px 12px;">Total <small style="font-size: 11px;">c/IVA</small></td><td align="right" id="carro_total" style="padding: 0px 12px; border-bottom: 0px; line-height: 27px; font-size: 18px;"></td></tr>
          <tr><td colspan="2">

            <textarea class="form-control" id="carro_notas" style="width: 100%; height: 69px; margin-bottom: 7px; font-size: 13px; font-weight: normal;" placeholder="Notas / Transporte"></textarea>

            <button type="button" class="btn btn-success btn-sm" style="width: 100%;" onclick="pedidos_confirmar();">
              <i class="fa fa-shopping-cart" style="font-size: 18px; margin: 0px 5px 0px 0px;"></i> Enviar Pedido
            </button>
            <button type="button" class="btn btn-primary btn-sm" style="margin-top: 5px; width: 104px;" onclick="cambiar_modo('busqueda')">
              <span class="glyphicon glyphicon-chevron-left" aria-hidden="true" style="font-size: 12px; margin: 0px 3px 0px 0px; top: 2px;"></span> Volver
            </button>
            <button type="button" class="btn btn-warning btn-sm" style="margin-top: 5px; width: 105px; float: right;" onclick="pedidos_descartar();">
              <span class="glyphicon glyphicon-trash" aria-hidden="true" style="font-size: 12px; margin: 0px 3px 0px 0px; top: 2px;"></span> Limpiar</button>
            <button type="button" class="btn btn-default btn-sm" style="width: 100%; margin-top: 5px; margin-bottom: 5px;" onclick="pedidos_imprimir();">
              <span class="glyphicon glyphicon-print" aria-hidden="true" style="font-size: 15px; margin: 0px 3px 0px 0px; top: 4px;"></span> Imprimir Pedido
            </button>
          </td></tr>
        </tbody>
      </table>
    </td>
    <td id="listas_detalles" style="display: none;">
      <table>
        <thead><tr><th colspan="2">Exportar listas</th></tr></thead>
        <tbody>
          <tr>
            <td>
              <div class="checkbox" style="line-height: 25px; margin-bottom: 20px;">
                <label style="font-weight: bold; margin-left: 5px;">
                  <input type="checkbox" style="height: 19px; width: 19px;" id="listas_seleccionar_todo"> DESCARGAR LISTA COMPLETA
                </label>
              </div>
              <p id="listas_sumatoria_fabricantes" style="font-size: 10px;"></p>
              <p  id="listas_sumatoria_piezas" style="font-size: 10px;"></p>
            <hr style="margin: 20px 0px 10px 0px;"/>
            <div onclick="listas_descargar_pdf();">
              <button type="button" class="btn btn-success btn-sm" style="width: 100%; margin-top: 6px; margin-bottom: 3px;" id="listas_descargar_pdf">
                <i class="fa fa-file-pdf-o"  style="font-size: 17px; margin: 0px 3px 0px 0px;"></i> Descargar PDF
              </button>
            </div>
            <button type="button" class="btn btn-success btn-sm" style="width: 100%; margin-top: 3px; margin-bottom: 3px;" id="listas_descargar_xls" onclick="listas_descargar_xls();">
              <i class="fa fa-file-excel-o"  style="font-size: 17px; margin: 0px 3px 0px 0px;"></i> Descargar Excel</button>

            

            {if="$GLOBALS['permisos']['listas_editar']"}
            <hr style="margin: 10px 0px 10px 0px;"/>

            <button type="button" class="btn btn-primary btn-sm" style="width: 100%; margin: 6px 0px 3px 0px;" onclick="listas_edita();">
              <span class="glyphicon glyphicon-pencil" aria-hidden="true" style="font-size: 15px; margin: 0px 3px 0px 0px;"></span> Editar Aumento
            </button>
            {/if}
            </td>
          </tr>
        </tbody>
      </table>
    </td>
  </tr>
</table>


<div id="procesando"><i class="fa fa-cog fa-spin" style="font-size: 3em;"></i></div>

<!-- RESUMEN -->



<script type="text/javascript">

  /* TABLA DE RESULTADOS */
  var resultados;
  var debug = 1;
  var watable_modo = '';
  var detalles_call;

//Capturamos los arrows del teclado
$(document).keydown(function(e) {
    if(watable_modo != 'mensajes'){
        switch(e.which) {
            case 37: // left
              resultados.prevPage();
            break;

            case 38: // up

              //seleccionamos el primero
              var item_seleccionado = $('#resultados_table tbody tr.seleccionado');
              var items = $("#resultados_table tbody tr");
              var index = items.index( item_seleccionado );

              if(index > 0) {
                //Quitamos el seleccionado
                $('#resultados_table tbody tr.seleccionado').removeClass('seleccionado');
                //Seleccionamos el anterior
                $('#resultados_table tbody tr:eq('+ (index-1) +')').addClass('seleccionado');
                //Conseguimos el id
                if(resultados.data.rows.length){
                  var id = resultados.data.rows[$('#resultados_table tbody tr.seleccionado').data('index')].id;
                  //Traemos los datos
                  detallesArticulo(id);
                }
              }
              
            break;

            case 39: // right
              resultados.nextPage();
            break;

            case 40: // down

              //seleccionamos el primero
              var item_seleccionado = $('#resultados_table tbody tr.seleccionado');
              var items = $("#resultados_table tbody tr");
              var cantidad = items.length;
              var index = items.index( item_seleccionado );

              if(index < cantidad-1) {
                //Quitamos el seleccionado
                $('#resultados_table tbody tr.seleccionado').removeClass('seleccionado');
                //Seleccionamos el anterior
                $('#resultados_table tbody tr:eq('+ (index+1) +')').addClass('seleccionado');
                //Conseguimos el id
                if(resultados.data.rows.length){
                  var id = resultados.data.rows[$('#resultados_table tbody tr.seleccionado').data('index')].id;
                  //Traemos los datos
                  detallesArticulo(id);
                }
              }

            break;

            default: return; // exit this handler for other keys
        }
        e.preventDefault(); // prevent the default action (scroll / move caret)
      }
  });

  //Traemos toda la info del articulo
  function detallesArticulo(id){

    if(id == -1){
      $('#detalles').html('');
      return;
    }

    if(typeof(detalles_call) != 'undefined')
      detalles_call.abort();

    detalles_call = $.ajax({
        type: "POST",
        url: "api.php?module=articulos&run=get_detalles",
        data: "articulo="+id,
        dataType: "json",
        cache: false,
        success: function(json){
          var equivalencias = $('<table></table>');
          for(var i in json.result.equivalencias) {
            equivalencias.append('<tr class="tr-item" data-id="'+json.result.equivalencias[i][0]+'" onClick="detallesArticulo($(this).data(\'id\'));"><td>'+json.result.equivalencias[i][1]+'</td><td>'+json.result.equivalencias[i][2]+'</td><td>$ '+json.result.equivalencias[i][3]+'</td></tr><tr class="tr-spacer"></tr>');
          }

          var acciones = '';

          //Permisos para la edicion general
          {if="$GLOBALS['permisos']['articulos_edicion_general']"}
            acciones += '<img src="./img/editar-azul.png" onClick="articuloEditar_open('+id+',\''+json.result.codigo+'\');"/>';
          {/if}

          {if="$GLOBALS['permisos']['articulos_edicion_aplicaciones']"}
            acciones += '<img src="./img/aplicaciones-azul.png" onClick="aplicaciones_listar('+id+');"/>';
          {/if}

          {if="$GLOBALS['permisos']['articulos_edicion_equivalencias']"}
            acciones += '<img src="./img/equivalencias-azul.png" onClick="equivalencias_listar('+id+',\''+json.result.codigo+'\',\''+json.result.rubro_id+'\');"/>';
          {/if}

          {if="$GLOBALS['permisos']['articulos_edicion_dimensiones']"}
            acciones += '<img src="./img/dimensiones-azul.png" onClick="dimensiones_articulo_listar('+id+',\''+json.result.codigo+'\',\''+json.result.rubro_id+'\');"/>';
          {/if}


          {if="$GLOBALS['permisos']['articulos_edicion_imagenes']"}
            acciones += '<img src="./img/fotos-azul.png" onClick="imagenes_listar('+id+');"/>';
          {/if}

          {if="$GLOBALS['permisos']['descuentos_articulos_administrar']"}
            acciones += '<img src="./img/descuentos-azul.png" onClick="descuentos_articulo_listar('+id+',\''+json.result.codigo+'\',\''+json.result.rubro_id+'\');"/>';
          {/if}

          if( (acciones != '') && (json.result.modo == '-1') )
            acciones = '<td width="70" rowspan="2" class="detalles-tabla-acciones"><div>' + acciones + '</div></td>';
          else acciones = '';

          var detalles_html = '<table class="detalles-tabla"><tr>'+acciones+'<td class="detalles-codigo"><div class="detalles-label">CODIGO</div>'+json.result.codigo+'</td><td class="detalles-descripcion" ' + ((json.result.modo == '-1') ? 'colspan="2"' : '') + '><div class="detalles-label">DESCRIPCION</div><div class="detalles-descripcion-value">'+json.result.rubro+' '+json.result.descripcion+' '+json.result.fabricante+'</div></td><td class="detalles-aplicaciones" rowspan="2"><div class="detalles-label" style="margin-botom: 5px;">APLICACIONES</div><div class="detalles-scroll">'+json.result.aplicaciones+'</div></td><td class="detalles-dimensiones"rowspan="2"><div class="detalles-label"  style="margin-botom: 5px;">DIMENSIONES</div><div class="detalles-scroll">'+json.result.dimensiones+'</div></td><td class="detalles-fotos" rowspan="2"><div id="detalles-fotos-prev" onclick="cambiar_thumb(\'down\');"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span></div><div id="detalles-fotos-next" onclick="cambiar_thumb(\'up\');"><span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></div><div style="max-height: 150px;" class="detalles-fotos-container"></div></td></tr><tr><td class="detalles-precios">';


            //Agregamos precio de costo
            if(json.result.precios.mostrar.costo)
              detalles_html += '<div id="detalles-precios-costo"><div class="detalles-label">PRECIO DE COSTO</div><div class="detalles-precios-precio">$ '+json.result.precios.costo+'<span style="margin-left: 10px;font-size: 10px;color: white;">S/IVA</span></div><div class="detalles-precios-precio">$ '+json.result.precios.costo_iva+'<span style="margin-left: 10px;font-size: 10px;color: white;">C/IVA</span></div></div>';

            //Agregamos precio de lista
            if(json.result.precios.mostrar.lista)
              detalles_html += '<div id="detalles-precios-lista"><div class="detalles-label">PRECIO DE LISTA</div><div class="detalles-precios-precio">$ '+json.result.precios.lista+'<span style="margin-left: 10px;font-size: 10px;color: white;">S/IVA</span></div><div class="detalles-precios-precio">$ '+json.result.precios.lista_iva+'<span style="margin-left: 10px;font-size: 10px;color: white;">C/IVA</span></div></div>';

            //Agregamos precio de venta
            if(json.result.precios.mostrar.venta)
              detalles_html += '<div id="detalles-precios-venta"><div class="detalles-label">PRECIO DE VENTA</div><div class="detalles-precios-precio">$ '+json.result.precios.venta+'<span style="margin-left: 10px;font-size: 10px;color: white;">S/IVA</span></div><div class="detalles-precios-precio">$ '+json.result.precios.venta_iva+'<span style="margin-left: 10px;font-size: 10px;color: white;">C/IVA</span></div></div>';

            detalles_html += '</td>' + ((json.result.modo == '-1') ? '<td class="detalles-agregar_articulo" onClick="pedidos_agregar('+id+')"><div class="detalles-label" style="text-align: center; margin: 0px 0px 5px 0px;">AGREGAR AL PEDIDO</div><i class="fa fa-shopping-cart" style="font-size: 40px; margin-top: 15px; color: #33CAF9;"></i></td>' : '') + '<td class="detalles-equivalencias"><div class="detalles-label">EQUIVALENCIAS <span class="btn-ampliar-equivalencias" onClick="ampliar_equivalencias();">AMPLIAR</span></div><div class="equivalencias-container">'+$('<div></div>').append(equivalencias).html()+'</div></td></tr></table>';

          var detalles = $(detalles_html);

          //Agregamos el data de equivalencias
          $('.detalles-equivalencias',detalles).data('equivalencias',json.result.equivalencias); 

          //Agregamos margen al segundo precio
          $('td.detalles-precios>div:last-child',detalles).css('margin-top','5px');

          //Si tenemos imagenes mostramos
          if(json.result.fotos.length){

            if(json.result.fotos.length > 1)
              detalles.find('.detalles-fotos').mouseenter(function() {
                  $('#detalles-fotos-next').show();
                  $('#detalles-fotos-prev').show();
              })
              .mouseleave(function() {
                  $('#detalles-fotos-next').hide();
                  $('#detalles-fotos-prev').hide();
              });

            var foto_obj = $('<img src="" onClick="abrir_galeria();"/>');
            foto_obj.data('imagenes',json.result.fotos);
            foto_obj.data('indice',0);
            foto_obj.attr('src', './articulos/thumbs/'+json.result.fotos[0]+'?'+Math.random());
            detalles.find('.detalles-fotos-container').append(foto_obj);
          }

          $('#detalles').html(detalles);

          detalles_call = undefined;
        }
    });
  }
  
  //AMPLIAR EQUIVALENCIAS
  function ampliar_equivalencias(){
    //Creamos el popup
    abrir_modal('ampliarEquivalencias', 400, false);

    //Mostramos la ventana
    $('#ampliarEquivalenciasModal').modal({show:true});

    //Codigo..
    var codigo = $('#detalles .detalles-codigo').clone().children().remove().end().text();

    //Titulo...
    $('#ampliarEquivalenciasModal_titulo').html('Equivalencias Artículo <b>' + codigo + ' </b> ');

    //Codigo...
    var equivalencias = $('.detalles-equivalencias').data('equivalencias');

    var contenedor = $('<div></div>');
    for(var i in equivalencias){
      contenedor.append('<div class="detalles-equivalencias-lista-ampliada" onClick="detallesArticulo('+equivalencias[i][0]+'); $(\'#ampliarEquivalenciasModal\').modal(\'toggle\');"><b>'+equivalencias[i][1]+'</b> '+equivalencias[i][2]+'<div><span>$</span><span>'+equivalencias[i][3]+'</span></div>');
    }

    //Contenido...
    $('#ampliarEquivalenciasModal_container').append(contenedor);

    //Cerrar
    $('#ampliarEquivalenciasModal_footer').append('<button type="button" data-dismiss="modal" class="btn btn-warning" style="display: block; width: 100%; margin: 10px auto;">Cerrar</button>');
  }

  function cambiar_thumb(direccion){
    var imagen = $('#detalles .detalles-fotos img');
    var indice = imagen.data('indice');
    var imagenes = imagen.data('imagenes');
    var cantidad = imagenes.length;

    //si vamos para arriba
    if(direccion == 'up'){
      if(indice == cantidad - 1) indice = 0;
      else indice++;
    }

    //Si vamos para abajo
    if(direccion == 'down'){
      if(indice == 0) indice = cantidad - 1;
      else indice--;
    }

    //Cambiamos todos los valores
    imagen.attr('src','./articulos/thumbs/'+imagenes[indice]+'?'+Math.random());
    imagen.data('indice',indice);
  }

  //Abre la Galeria de imagenes en el listado de arts.
  function abrir_galeria(){

    var imagen = $('#detalles .detalles-fotos img');
    var indice = imagen.data('indice');
    var imagenes = imagen.data('imagenes');
    var cantidad = imagenes.length;


    //Creamos el arreglo de imagenes
    var imgs = Array();
    for(var i in imagenes)
        imgs.push({ "href": './articulos/'+imagenes[i]+'?'+Math.random()});


    //Mostramos la galeria
    $.fancybox.open(imgs, {
        padding : 0,
        index: indice,
        minHeight: 2,
        minWidth: 2,
        openSpeed: 'fast', 
        closeSpeed: 'fast',
        nextSpeed: 'fast',
        prevSpeed: 'fast',
        wrapCSS    : 'fancybox-custom',
        closeClick : true,
        openEffect : 'none',
        padding: 20,
        helpers : {
                    title : {
                      type : 'inside'
                    }
        }
    });
  }

  //MUESTRA 'PROCESANDO'
  function processingTable(){
    $('#sin_resultados').hide();
    $('#procesando').fadeIn();
    $('#table_head').hide();
    $('#foot').hide();
    $('#resultados').hide();
    $('#sin_resultados').hide();
  }

  //ACTUALIZA RESULTADOS
  function articulosUpdate(modo){

      console.log('Articulos Update');

      //Ocultamos todo
      $('#layout_watable').hide();
      $('#configuracion').hide();
      $('div.tags').css('visibility','hidden');
      $('div.resumen').hide();
      $('#procesando').show();

      var data = {};
      if(typeof(modo) != 'undefined') 
        data.modo = modo;

      //Llamada a la API
      apix('articulos','busqueda',data, {
        ok: function(json){

          // Cambiamso el modo
          watable_modo = json.result.modo;

          //Creamos BreadCrumb
          createBreadCrumb();

          //Actualizamos layout
          actualiza_layout();

          //Ocultamos 'cargando...'
          $('#procesando').hide();

          //Mostramos lo necesario
          if( (watable_modo != 'configuracion_precios') && 
              (watable_modo != 'configuracion_cuenta') &&
              (watable_modo != 'mensajes')){
            //Cargamos Resultados
            resultados.setData(json.result);
            cambiaCantidadFilas();
          }

        }
      });
  }

  //ACTUALIZA INDICES
  function updateIndexes(){
      //Llamada a la API
      apix('busqueda','quick_search_reindex',{}, {
        ok: function(json){
          notifica('Indices Actualizados','Los indices se actualizaron con éxito','confirmacion','success');
        }
      });
  }

  //CAMBIAR MODO
  function cambiar_modo(modo){
      console.log('cambiar modo');
      //Vaciamos la configuracion
      if(modo != 'configuracion_precios')
        $('#configuracion').empty();

      //Vaciamos la configuracion
      if(modo != 'configuracion_cuenta')
        $('#configuracion').empty();

      //Vaciamos la configuracion
      if(modo != 'mensajes')
        $('#mensajes').empty();

      //Llamada a la API
      apix('busqueda','cambiar_modo',{modo: modo}, {
        ok: function(json){
          articulosUpdate();
        }
      });
  }

  //Ordena Alfanumerico natural
  function alphanum(a, b) {
    function chunkify(t) {
      var tz = [], x = 0, y = -1, n = 0, i, j;

      while (i = (j = t.charAt(x++)).charCodeAt(0)) {
        var m = (i == 46 || (i >=48 && i <= 57));
        if (m !== n) {
          tz[++y] = "";
          n = m;
        }
        tz[y] += j;
      }
      return tz;
    }

    var aa = chunkify(a);
    var bb = chunkify(b);

    for (x = 0; aa[x] && bb[x]; x++) {
      if (aa[x] !== bb[x]) {
        var c = Number(aa[x]), d = Number(bb[x]);
        if (c == aa[x] && d == bb[x]) {
          return c - d;
        } else return (aa[x] > bb[x]) ? 1 : -1;
      }
    }
    return aa.length - bb.length;
  }

  function mailer(){
      //Llamada a la API
      apix('usuarios','obtener_emails',{}, {
        ok: function(json){
          //Destinatarios
          var mailto = 'mailto:federico.colombo@distribuidoralibertad.com?bcc=';
          for(var i in json.result){
            if(i>0) mailto += ',';
            mailto += json.result[i];
          }
          
          //Tema
          mailto += '&subject=Nuevas Actualizaciones';

        }
      });
  }

  //Chequea si existen items seleccionados
  function listas_checkear_seleccionados(){

    var fabricantes = new Array();
    var articulos = 0;

    var todos_seleccionados = true;
    var existen_descargas = false;
    for (var i in resultados.data.rows){
      //Existe alguna descarga?
      if(resultados.data.rows[i].descarga == '1'){
        existen_descargas = true;
        //Contabilizamos
        fabricantes.push(resultados.data.rows[i].fabricante);
        articulos = articulos + parseInt(resultados.data.rows[i].piezas_descargas);
      }else{
        todos_seleccionados = false;
      }
    }

    var uniqueVals = [];
    $.each(fabricantes, function(i, el){
        if($.inArray(el, uniqueVals) === -1) uniqueVals.push(el);
    });

    $('#listas_sumatoria_fabricantes').html('FABRICANTES: <span style="font-weight: normal;">' + uniqueVals.length + ' SELECCIONADOS</span>');
    $('#listas_sumatoria_piezas').html('ARTICULOS: <span style="font-weight: normal;">' + articulos+ '</span>');

    //Prendemos los botones o los apagamos
    if(existen_descargas)
      $('#listas_descargar_xls, #listas_imprimir').prop('disabled', false);
    else
      $('#listas_descargar_xls, #listas_imprimir').prop('disabled', true);

    if(todos_seleccionados)
      $('#listas_descargar_pdf').prop('disabled', false);
    else
      $('#listas_descargar_pdf').prop('disabled', true);
  }

  function listas_descargar_xls(){
    
    var cont = 0;
    for(var i in resultados.data.rows)
      if(resultados.data.rows[i].descarga == '1')
        cont++;

    //Lista completa
    if(cont == resultados.data.rows.length)
        $('body').append('<iframe class="iframe_descargas" style="display:none;" src="descargas.php?extension=xls&seleccionados=todos"></iframe>');
    
    //Lista Armada
    if(cont != resultados.data.rows.length){

      var aumentos = [];
      for(var i in resultados.data.rows)
        if(resultados.data.rows[i].descarga == '1'){
          var variacion = [];

          //Agregamos aumento
          variacion.push(parseInt(resultados.data.rows[i].fabricante));
          variacion.push(resultados.data.rows[i].precio_update);
          variacion.push(parseInt(resultados.data.rows[i].descarga_por_rubro));          
          aumentos.push(variacion);
        }

      $('body').append('<iframe class="iframe_descargas" style="display:none;" src="descargas.php?&extension=xls&seleccionados='+encodeURIComponent(JSON.stringify(aumentos))+'"></iframe>');
    }

      //pasados 10 segundos quitamos todos los iframes
      setTimeout(function(){
        $('.iframe_descargas').remove();
      }, 10000);
  }

  function listas_descargar_pdf(){
    if( $('#listas_descargar_pdf').attr('disabled') == 'disabled'){
      notifica( 'Descarga de lista',
                'La descarga en PDF esta disponible solo para la lista completa.',
                'informacion',
                'info'
      );
      return;
    }

    var cont = 0;
    for(var i in resultados.data.rows)
      if(resultados.data.rows[i].descarga == '1')
        cont++;


    //Lista completa
    if(cont == resultados.data.rows.length)
        $('body').append('<iframe class="iframe_descargas" style="display:none;" src="descargas.php?extension=pdf&seleccionados=todos"></iframe>');
    // rimad1962@yahoo.com.ar
    //Lista Armada
    if(cont != resultados.data.rows.length){

      var aumentos = [];
      for(var i in resultados.data.rows)
        if(resultados.data.rows[i].descarga == '1'){
          var archivo = resultados.data.rows[i].fabricante_nombre;
          if(resultados.data.rows[i].descarga_por_rubro) 
            archivo += '(' + resultados.data.rows[i].precio_update + ')';
          aumentos.push(archivo);
        }

      $('body').append('<iframe class="iframe_descargas" style="display:none;" src="descargas.php?&extension=pdf&seleccionados='+encodeURIComponent(JSON.stringify(aumentos))+'"></iframe>');
    }

      //pasados 10 segundos quitamos todos los iframes
      setTimeout(function(){
        $('.iframe_descargas').remove();
      }, 10000);
  }

  function listas_edita(){

    //inice
    var index = $('#resultados tr.seleccionado').data('index');

    var descripcion = resultados.data.rows[index].descripcion;
    var tipo_descarga = resultados.data.rows[index].descarga_por_rubro;

    //Creamos el popup
    abrir_modal('listas', 550, false);

    //Mostramos la ventana
    $('#listasModal').modal({show:true});

    //Titulo...
    $('#listasModal_titulo').html('Editar Aumento');

    //Footer...
    $('#listasModal_footer').html('');

    //Contenido
    var contenido = $('<div class="scrollable" style="max-height: 300px; overflow-y: hidden;"></div>');

    //Agregamos el campo Nombre singular
    contenido.append( genera_campo({  
      id:       "descripcion",
      label:      "Descripción",
      tipo:       "input",
      mostrarEditar:  true,
      mostrarCrear:   true
    }, 'listas' ,true) );


    //Agregamos el campo Nombre singular
    contenido.append( genera_campo({ 
      id:       "descarga_tipo",
      tipo:       "checkbox",
      mostrarEditar:  true,
      mostrarCrear:   true,
      config:     {
              claseActivo: 'btn-primary',
              claseInactivo: 'btn-success',
              valueActivo: '1',
              valueInactivo: '0',
              valueDefault: '1',
              labelActivo: function(el){
                el.html('La descarga es por Rubros');
              },
              labelInactivo: function(el){
                el.html('La descarga es por Fabricante');
              }
      }
    }, 'listas' ,true) );


    //Agregamos el valor
    $('#listas_descripcion', contenido).val_(descripcion);
    $('#listas_descarga_tipo', contenido).val_(tipo_descarga);

      //Asignamos el contenido al popup
    $('#listasModal_container').html(contenido);

    //Agregamos scrollbars
    $('#listasModal_container .scrollable').mCustomScrollbar({theme: 'minimal-dark',scrollInertia: 100, mouseWheel:{ scrollAmount: 70 }});

    //Agregamos los botones al footer
    $('#listasModal_footer').append('<button type="button" class="btn btn-primary" style="display: block; width: 48%; margin: 10px auto; float: left;" onclick="listas_edita_confirmar();">Guardar cambios</button>');
    $('#listasModal_footer').append('<button type="button" class="btn btn-warning" data-dismiss="modal" style="display: block; width: 48%; margin: 10px auto; float: right;">Cancelar</button>');
  }

  function listas_edita_confirmar(){
    
    var data = {};
    var index = $('#resultados tr.seleccionado').data('index');
    data.descripcion = $('#listas_descripcion').val();
    data.tipo = $('#listas_descarga_tipo').val();
    data.fecha = resultados.data.rows[index].precio_update;
    data.fabricante = resultados.data.rows[index].fabricante;

    $('#listasModal_titulo').html('Procesando...');
    $('#listasModal_container').html( cargando() );
    $('#listasModal_footer').html('');

    apix('listas','editar',data, {
      ok: function(json){
        $('#listasModal').modal('hide');
        articulosUpdate();
      }
    });
  }

  function openFlyer(index){
    var imgs = Array();
    $('#publicidad div img:not(.unslider-clone)').each(function(){
      var click = $(this).attr('onClick');
      if(typeof(click) != 'undefined'){
        var fullPath = $(this).attr('src');
        var archivo = fullPath.replace(/^.*[\\\/]/, '');
        var nombre = archivo.replace(".jpg", "_grande.jpg");
        imgs.push({ "href": './img/publicidad/'+nombre});
      }
    });

    //Mostramos la galeria
    $.fancybox.open(imgs, {
        padding : 0,
        index: index,
        openSpeed: 'fast', 
        closeSpeed: 'fast',
        nextSpeed: 'fast',
        prevSpeed: 'fast',
        wrapCSS    : 'fancybox-custom',
        closeClick : true,
        openEffect : 'none',
        padding: 20,
        helpers : {
                    title : {
                      type : 'inside'
                    }
        }
    });
  }

  function cambiaCantidadFilas(){
    var altoNavegador = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
    
    var zoom = document.body.style.zoom;
    var espacioLibre = altoNavegador - ((204 + 211 + 50) * zoom);
    var espacioLibreListas = altoNavegador - ((35 + 211 + 65) * zoom);

    var altoFila = $('#resultados_table > tbody > tr > td').height() * zoom;

    if(watable_modo != 'listas')
      var lineas = Math.floor(espacioLibre/altoFila);

    if(watable_modo == 'listas')
      var lineas = Math.floor(espacioLibreListas/altoFila);

    //Actualizamos watable
    resultados.option('pageSize',lineas);
  }

  function patch_height(){
    
    var alto = window.screen.availHeight;
    //Para < a 768
    if((alto >= 741) && (alto <= 760)){
      console.log('760 a 740');
      document.body.style.zoom=0.90;
      this.blur();
    } else if( (alto >= 680) && (alto <= 740)){
      console.log('menor a 760');
      document.body.style.zoom=0.83;
      this.blur();
    } else if(alto <= 679){
      console.log('800x600');
      document.body.style.zoom=0.65;
      this.blur();
    } else if(alto >= 761){
      console.log('mayor a 760');
      document.body.style.zoom=1;
      this.blur();
    }
  }

  function listar_por_vehiculo(){

      var data = {};
      data.variable = 'advSearch_vehiculo_filas_individuales';
      var valorActual = $('#btn_busqueda_modo_listar_vehiculos').data('estado');

      if(valorActual == '1'){
        data.value = '-1';
        $('#btn_busqueda_modo_listar_vehiculos').addClass('btn-default').removeClass('btn-info').data('estado','-1');
      }else{
        data.value = '1';
        $('#btn_busqueda_modo_listar_vehiculos').addClass('btn-info').removeClass('btn-default').data('estado','1');
      }

      //Llamada a la API
      apix('busqueda','advSearch_setSessionVar',data, {
        ok: function(json){
          articulosUpdate();
        }
      });
  }

  function mostrar_dimensiones(){

      //Si no hay resultados chau
      if(!resultados.data.rows.length) return;

      //Variables de control
      var check_rubro = resultados.data.rows[0].rubro_id;
      var mismo_rubro = true;
      var check_rubro_padre = resultados.data.rows[0].rubro_padre;
      var mismo_rubro_padre = true;

      //Chequeamos el rubro
      for(var i in resultados.data.rows)
        if((resultados.data.rows[i].rubro != check_rubro) || (resultados.data.rows[i].rubro == null) || (check_rubro == null)) {
          mismo_rubro = false;
          break;
        }

      //chequeamos el rubro padre
      for(var i in resultados.data.rows)
        if( (resultados.data.rows[i].rubro_padre != check_rubro_padre) || (resultados.data.rows[i].rubro_padre == null) || (check_rubro_padre == null)) {
          mismo_rubro_padre = false;
          break;
        }

      //Avisamos
      if(!mismo_rubro && !mismo_rubro_padre){
          notifica( 'Rubro Vacio',
                'Para poder listar las dimensiones en columnas los articulos deben pertenecer al mismo Rubro',
                'informacion',
                'info'
              );
              return;
      }


      var data = {};
      data.variable = 'advSearch_dim_en_columnas';
      var valorActual = $('#btn_busqueda_modo_mostrar_dimensiones').data('estado');

      if(valorActual == '1'){
        data.value = '-1';
        $('#btn_busqueda_modo_mostrar_dimensiones').addClass('btn-default').removeClass('btn-info').data('estado','-1');
      }else{
        data.value = '1';
        $('#btn_busqueda_modo_mostrar_dimensiones').addClass('btn-info').removeClass('btn-default').data('estado','1');
      }

      //Llamada a la API
      apix('busqueda','advSearch_setSessionVar',data, {
        ok: function(json){
          articulosUpdate();
        }
      });
  }

  function actualiza_layout(){

      //Ocultamos todo cualquiera sea el modo
      $('#carro_detalles').hide();
      $('#listas_detalles').hide();
      $('#detalles').hide();
      $('#configuracion').hide();
      $('#mensajes').hide();
      $('.boton-cabecera').removeClass('boton-cabecera-activo');


      ///////////////////////////////////////////////////////////////////////
      //                    MODO MENSAJES
      ///////////////////////////////////////////////////////////////////////
      if(watable_modo == 'mensajes'){
        $('#mensajes').show();
        //$('#detalles').hide();
        $('#boton-cabecera-mensajes').addClass('boton-cabecera-activo');
        mensajes_layout();
        return;
      }

      ///////////////////////////////////////////////////////////////////////
      //                    MODO CONFIGURACION PRECIOS
      ///////////////////////////////////////////////////////////////////////
      if(watable_modo == 'configuracion_precios'){
        $('#configuracion').show();
        //$('#detalles').hide();
        $('#boton-cabecera-configuracion').addClass('boton-cabecera-activo');
        configuracion_precios();

        return;
      }

      ///////////////////////////////////////////////////////////////////////
      //                    MODO CONFIGURACION CUENTA
      ///////////////////////////////////////////////////////////////////////
      if(watable_modo == 'configuracion_cuenta'){
        $('#configuracion').show();
        $('#boton-cabecera-configuracion').addClass('boton-cabecera-activo');
        configuracion_cuenta();
        return;
      }

      ///////////////////////////////////////////////////////////////////////
      //                           MODO BUSQUEDA
      ///////////////////////////////////////////////////////////////////////
      if(watable_modo == '-1'){
        //Mostramos detalles
        $('#layout_watable').show();
        $('div.resumen').show();
        $('#detalles').show();
        $('#boton-cabecera-busqueda').addClass('boton-cabecera-activo');
        return;
      }

      ///////////////////////////////////////////////////////////////////////
      //                           MODO LISTAS 
      ///////////////////////////////////////////////////////////////////////
      if(watable_modo == 'listas'){
        $('#layout_watable').show();
        $('div.resumen').show();
        $('#listas_detalles').show();
        $('#boton-cabecera-listas').addClass('boton-cabecera-activo');
        return;
      }

      ///////////////////////////////////////////////////////////////////////
      //                           MODO CARRO
      ///////////////////////////////////////////////////////////////////////
      if(watable_modo == 'carro'){
        $('#layout_watable').show();
        $('div.resumen').show();
        $('#detalles').show();
        $('#carro_detalles').show();
        $('#boton-cabecera-carro').addClass('boton-cabecera-activo');
        return;
      }
  }

  //Guarda la ultima pagina de watable
  var watable_ultima_pagina = 1;
  var watable_ordenado_por = 1;
  var watable_ = 1;
  var watable_volver = false;

/* CUANDO EL DOCUMENTO TERMINA DE CARGAR */
$(document).ready(function() {

  //Hay notificaciones?
  checkNotificaciones();

  //Patch para resoluciones chicas
  patch_height();

  $('#publicidad').unslider({
    autoplay: true,
    keys: false,
    speed: 800,
    delay: 5000,
    nav: false,
    animation: 'horizontal',
    infinite: true,
    arrows: {
      //  Unslider default behaviour
      prev: '<a class="fa fa-arrow-circle-o-left unslider-arrow prev"></a>',
      next: '<a class="fa fa-arrow-circle-o-right unslider-arrow next"></a>'
    },
    selectors: {
      container: 'div',
      slides: 'img'
    }
  });

  $('#carro_notas').change(function(){
    //Recalculamos el carro
    pedido_recalcula();
  });


  $('#listas_seleccionar_todo').on('click',function(){

    if($(this).is(":checked")){
        //Seleccionamos todos los Aumentos
        $("#resultados_table tbody tr input:checkbox").prop('checked', true);
        $(this).data('seleccion','1');
        for(var i in resultados.data.rows)
          resultados.data.rows[i].descarga = '1';
    }else{
        //Deseleccionamos todos los Aumentos
        $("#resultados_table tbody tr input:checkbox").prop('checked', false);
        $(this).data('seleccion','0');
        for(var i in resultados.data.rows)
          resultados.data.rows[i].descarga = '0';
    }

    listas_checkear_seleccionados();
  });

  //Close al tooltips when click outside Anchor
  $('html').on('click', function (e) {
    $("a[rel='tooltip']").each(function () {
        if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.tooltip').has(e.target).length === 0)
            $(this).tooltip('hide');
    });
  });


  $( window ).resize(function() {
    if( (watable_modo != 'configuracion_precios') &&
        (watable_modo != 'configuracion_cuenta') &&
        (watable_modo != 'mensajes'))
      cambiaCantidadFilas();
  });

  
  resultados = $('#resultados').WATable({
      pageSize: 1,                //Sets the initial pagesize
      filter: false,               //Show filter fields
      columnPicker: false,         //Show the columnPicker button
      pageSizes: [],  //Set custom pageSizes. Leave empty array to hide button.
      hidePagerOnEmpty: true,     //Removes the pager if data is empty.
      checkboxes: true,           //Make rows checkable. (Note. You need a column with the 'unique' property)
      preFill: false,              //Initially fills the table with empty rows (as many as the pagesize).
      //url: 'api.php?module=articulos&run=busqueda',    //Url to a webservice if not setting data manually as we do in this example
      //urlData: { },     //Any data you need to pass to the webservice
      urlPost: true,             //Use POST httpmethod to webservice. Default is GET.

      types: {                    //Following are some specific properties related to the data types
          string: {
              //filterTooltip: "Giggedi..."    //What to say in tooltip when hoovering filter fields. Set false to remove.
              //placeHolder: "Type here..."    //What to say in placeholder filter fields. Set false for empty.
          },
          number: {
              //decimals: 1   //Sets decimal precision for float types
          },
          bool: {
              //filterTooltip: false
          },
          date: {
            //utc: true,            //Show time as universal time, ie without timezones.
            //format: 'yy/dd/MM',   //The format. See all possible formats here http://arshaw.com/xdate/#Formatting.
            //datePicker: true      //Requires "Datepicker for Bootstrap" plugin (http://www.eyecon.ro/bootstrap-datepicker).
          }
      },
      tableCreated: function(data) {    //Fires when the table is created / recreated. Use it if you want to manipulate the table in any way.



          checkNotificaciones();

          //Run across 
          var index = 0;
          var descripciones = 0;
          var modo = '-1';

          if(typeof(resultados.data.modo) != 'undefined')
            modo = resultados.data.modo;

          //Guardamos el modo en la tabla
          $('#resultados_table').data('modo',modo);

          //Ocultamos todo cualquiera sea el modo
          $('#carro_detalles').hide();
          $('#listas_detalles').hide();
          $('#detalles').hide();

          $('.boton-cabecera').removeClass('boton-cabecera-activo');

          ///////////////////////////////////////////////////////////////////////
          //                           MODO BUSQUEDA
          ///////////////////////////////////////////////////////////////////////
          if(modo == '-1'){
            //Mostramos detalles
            $('#detalles').show();
            $('#boton-cabecera-busqueda').addClass('boton-cabecera-activo');
          }



          ///////////////////////////////////////////////////////////////////////
          //                           MODO LISTAS 
          ///////////////////////////////////////////////////////////////////////
          if(modo == 'listas'){

            //Unimos dos columnas de fabricante
            $('#resultados_table thead tr th:eq(1)').remove();
            $('#resultados_table thead tr th:eq(0)').attr('colspan','2');
            //Mostramos detalles
            $('#listas_detalles').show();
            $('#boton-cabecera-listas').addClass('boton-cabecera-activo');

            //Argregamos todos los checkbox
            var checkbox = $('<input type="checkbox" style="height: 19px; width: 19px;">');

            //Al hacer click...
            checkbox.on('click',function(){
              var index = $(this).closest('tr').data('index');
              if( $(this).is(':checked') ) resultados.data.rows[index].descarga = '1';
              else resultados.data.rows[index].descarga = '0';

              //Chequeamos seleccionados
              listas_checkear_seleccionados();

              //Check o no en 'todas seleccionadas'
              var chekear_todas = true;
              for(var i in resultados.data.rows)
                if(resultados.data.rows[i].descarga == '0'){
                  chekear_todas = false;
                  break;
                }

              //Chequeamos o no el boton
              if(chekear_todas) $('#listas_seleccionar_todo').prop('checked',true);
              else $('#listas_seleccionar_todo').prop('checked',false);

            });

            //Lo agregamos al final de la tabla
            $('#resultados_table tbody tr td:last-child').prepend(checkbox);

            //Contamos las variables para crear los iconos
            for (var i = resultados.data.fromRow; i <= resultados.data.toRow; i++){
              if(resultados.data.rows[i] != undefined)
                if(resultados.data.rows[i].descarga == '1') 
                  $("#resultados_table tbody tr[data-index='" + i + "'] input:checkbox").prop('checked',true);
                else
                  $("#resultados_table tbody tr[data-index='" + i + "'] input:checkbox").prop('checked',false);

              index++
            }

            //Chequeamos seleccionados
            listas_checkear_seleccionados();
          }




          ///////////////////////////////////////////////////////////////////////
          //                           MODO CARRO
          ///////////////////////////////////////////////////////////////////////
          if(modo == 'carro'){

            $('#carro_notas').val(resultados.data.notas);
            //Mostramos detalles
            pedido_recalcula();
            $('#detalles').show();
            $('#carro_detalles').show();
            $('#boton-cabecera-carro').addClass('boton-cabecera-activo');
            
            

            //Agregamos eventos a los inputs
            $('#resultados .carro-cantidad').each(function(){
              
              //Solo permitimos numeros
              $(this).on('keypress',function(event){
                return (event.charCode) >= 48 && (event.charCode <= 57);
              });

              //Al cambiar
              $(this).on('change',function(){
                //No puede quedar vacio
                if( $(this).val() == '' )
                  $(this).val( $(this).data('anterior') );

                //No puede ser menor a 1
                if($(this).val() < 1) $(this).val(1);

                //No puede ser mayor a 9999
                if($(this).val() > 9999) $(this).val(9999);

                //Nuevo valor
                var cantidad = parseInt($(this).val());

                //Guardamos el nuevo Value
                $(this).data('anterior',cantidad);

                //Indice
                var indice = $(this).parent().parent().data('index');
                var precio = parseFloat(resultados.data.rows[indice].precio).toFixed(2);
                var precio_lista = parseFloat(resultados.data.rows[indice].precio_lista).toFixed(2);

                resultados.data.rows[indice].total = (cantidad * precio).toFixed(2);
                resultados.data.rows[indice].total_lista = (cantidad * precio_lista).toFixed(2);
                resultados.data.rows[indice].cantidad = cantidad;

                //Cambiamos la tabla
                //$(this).parent().next().html('$ ' + resultados.data.rows[indice].total);

                //Actualizamos todo
                pedido_recalcula();
              });

              //Paramos el burbujeo al hacer click
              $(this).on('click',function(e){
                e.stopPropagation();
              });

              //Seleccionamos el boton UP
              $(this).siblings('.carro-cantidad-botones').find('.carro-cantidad-up').on('click',function(e){
                //Paramos el burbujeo
                e.stopPropagation();

                var input = $(this).parent().prev();
                var nuevoValor = parseInt(input.val()) + 1
                if(nuevoValor <= 9999){
                  input.val(nuevoValor);
                  input.trigger('change');
                }
              });


              //Seleccionamos el boton DOWN
              $(this).siblings('.carro-cantidad-botones').find('.carro-cantidad-down').on('click',function(e){
                //Paramos el burbujeo
                e.stopPropagation();

                var input = $(this).parent().prev();
                var nuevoValor = parseInt(input.val()) - 1
                if(nuevoValor >= 1){
                  input.val(nuevoValor);
                  input.trigger('change');
                }
              });

            });

          }


          //reset index
          index = 0;

          //Generamos los iconos de la tabla
          for (var i = resultados.data.fromRow; i < resultados.data.toRow; i++) {
            
            //Si existe la fila...
            if(resultados.data.rows[i] != undefined){
              var td = $('#resultados_table tbody tr:eq('+index+')');

              // Si el articulo esta deshabilitado para los clientes
              // lo pintamos de rojito como para avisarle al usuario
              if(resultados.data.rows[i].habilitado == 0) td.css('background','#ffecec');
            }

            //add one to counter index
            index++;
          };
          

          //seleccionamos el primero}
          var permitir_seleccion = true;
          if(modo == 'listas') permitir_seleccion = false;

          //Si podemos editar las listas de precio permitimos seleccion
          {if="$GLOBALS['permisos']['listas_editar']"}
          if(modo == 'listas') permitir_seleccion  = true;
          {/if}

          {if="$GLOBALS['permisos']['faltantes_crear']"}
          //Mostramos el boton de 'Sin Stock'
          if(modo == '-1') {$('#btn_faltantes_agregar').show();}
          {/if}

          //Seleccionamos primer articulo
          if(permitir_seleccion)
            if(resultados.data.rows.length){
              var primer_item = $('#resultados_table tbody tr').first();
              var id = resultados.data.rows[ primer_item.data('index') ].id;
              primer_item.addClass('seleccionado');
              if(modo != 'listas') detallesArticulo(id);
            }else{
              if(modo != 'listas') detallesArticulo(-1);
            }

          //Generamos los tooltips de los titulos
          $('tr.sort a').tooltip({placement: "right"});

          //Volvemos a la pagina anterior
          if(watable_volver) {
            watable_volver = false;
            resultados.goToPage(watable_ultima_pagina);
          }
          
      },
      rowClicked: function(data) { 

          //Se puede seleccinar siempre
          //Excepto que estemos en listas
          //y no se tenga permiso para editar
          permitir_seleccion  = true;
          if(watable_modo == 'listas') permitir_seleccion = false;

          //Si tenemos permisos de edicion...
          {if="$GLOBALS['permisos']['listas_editar']"}
          if(watable_modo == 'listas')
            permitir_seleccion  = true;
          {/if}
          
          //Editamos listas
          if(permitir_seleccion){
            //Quitamos selecciones anteriores
            $(data.event.target).closest("tr").siblings().each(function(){
              $(this).removeClass('seleccionado');
            });

            $(data.event.target).closest("tr").addClass('seleccionado');

            //Pedimos los detalles del articulo
            if(watable_modo != 'listas') detallesArticulo(data.row.id);
          }

          //Fires when a row is clicked (Note. You need a column with the 'unique' property).
          //console.log('row clicked');   //data.event holds the original jQuery event.
          //console.log(data);            //data.row holds the underlying row you supplied.
                                        //data.column holds the underlying column you supplied.
                                        //data.checked is true if row is checked.
                                        //'this' keyword holds the clicked element.

      },
      columnClicked: function(data) {    //Fires when a column is clicked
        //console.log('column clicked');  //data.event holds the original jQuery event
        //console.log(data);              //data.column holds the underlying column you supplied
                                        //data.descending is true when sorted descending (duh)
      },
      pageChanged: function(data) {      //Fires when manually changing page
        watable_ultima_pagina = data.page;
        //console.log('page changed');    //data.event holds the original jQuery event
        //console.log('holaa' + data);              //data.page holds the new page index
      },
      pageSizeChanged: function(data) {  //Fires when manually changing pagesize
        //console.log('pagesize changed');//data.event holds teh original event
        //console.log(data);              //data.pageSize holds the new pagesize
      }
  }).data('WATable');  //This step reaches into the html data property to get the actual WATable object. Important if you want a reference to it as we want here.

  articulosUpdate();

  //setTimeout(function(){ resultados.goToPage(5); }, 3000);

  
});



</script>

