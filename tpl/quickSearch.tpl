<button type="button" class="btn btn-info btn-sm pull-right quicksearch_avanzada" style="" onclick="busqueda_avanzada();">
Avanzada
</button>
<div id="quicksearch_container"><input type="text" id="quicksearch" class="quicksearch pull-right" placeholder="BÚSQUEDA RÁPIDA"/></div>


<script type="text/javascript">

  //Select2 #quicksearch
  $('#quicksearch').select2({
    minimumInputLength: 1,
    placeholder: $(this).attr('placeholder'),
    allowClear: false,
    width: 'resolve',
    id: function(e) { return e.id; },
    formatNoMatches: function() { return 'No encontramos articulos.. Ingrese más palabras o cambielas.'; },
    formatSearching: function(){ return "Buscando..."; },
    ajax: {
        url: "api.php",
        dataType: 'json',
        quietMillis: 200,
        data: function(term, page) {
            return {
                module: 'busqueda',
                run: 'quick_search',
                q: term,
                p: page
            };
        },
        results: function(data, page ) {
            //Si el ususario no esta logueado refresh
            if(!data.logued) location.reload();

            //Calcula si existen mas resultados a mostrar
            var more = (page * 40) < data.result.total;

            //Devolvemos el valor de more para que selec2
            //sepa que debemos cargar mas resultados.
            return {results: data.result.items, more: more};
        }
    },
    formatInputTooShort: function () {
      return "";
    },
    formatResult: function(item){
      if(parseInt(item.id) < 0)
        return '<div style="text-align: right; width: 100%; height: 30px; line-height: 30px; font-weight: bold; padding-right: 20px;"><i>Listar los primeros 20 resultados</i></div>';
      
        return '<div class="quicksearch-codigo">'+item.codigo_completo+'</div><div class="quicksearch-descripcion">'+item.descripcion+'</div><div class="quicksearch-precio">$ '+item.precio+'</div>';
    },
    formatSelection: function(item){
      if(parseInt(item.id) < 0) return 'Listar los primeros 20 resultados coincidentes con <b>"'+item.term+'"</b>';
      return item.descripcion;
    },
    initSelection : function (element, callback) {
      var elementText = $(element).attr('data-init-text');
      callback({"terms":elementText});
    }
  }).on("change", function(e) {


  }).on("select2-removed", function(e){
  }).on("select2-focus", function(e){
  }).on("change", function(e){
    //Opcion seleccionada 
    var select = $(this).select2('data');
    //Si seleccionamos algun valor...
    if(select != null){

        var data = {}
        data['q'] = select.term;
        data['articulo'] = select.id;

        apix('busqueda','quick_search_commit',data, {
          ok: function(json){
              articulosUpdate();
          }
        });
      }

  }).on("select2-loaded", function(){
    $('.select2-result-label').addClass('select2-result-label_fix');
    $('.select2-results .select2-result-label').css('padding','0px');
    $('.select2-results').css('max-height',450);
  }).on("select2-open", function(){ 
      $('.menu_button_busqueda').hide();
  }).on("select2-close", function(){
      $('.menu_button_busqueda').show();
  });

  //Recalcula ancho de Quicksearch
  function recalcWidthQuicksearch(){
    $('#quicksearch').select2("close");

    var width_ventana = $(window).width();

    //Si el tamaño de la ventana es menor a 700
    //mostramos el logo solo...
    if(width_ventana < 700){
      $('.logo_solo').show();
      $('.logo').hide();
      var width_search = width_ventana - 110;
    }else{
      $('.logo').show();
      $('.logo_solo').hide();
      var width_search = width_ventana - 300;
    }

    //Tope para el ancho de la busqueda
    if(width_search > 600) width_search = 600;

    $('#s2id_quicksearch').width(width_search);
  } 

  $(window).resize(function(){
    recalcWidthQuicksearch();
  });

  //Recalculamos a penas abrimos
  //la ventana
  recalcWidthQuicksearch();


</script>