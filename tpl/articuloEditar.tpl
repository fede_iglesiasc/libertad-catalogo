<script type="text/javascript">

//GENERAL
var articuloEditar_campos = [
      {
      id:       "rubro",
      label:      "Rubro",
      tipo:       "select2",
      mostrarEditar:  true,
      mostrarCrear:   true,
      config:     {
        minimumInputLength: 0,
        placeholder: $(this).attr('placeholder'),
        allowClear: true,
        width: 'resolve',
        id: function(e) { return e.id; },
        formatNoMatches: function() { return 'Sin resultados'; },
        formatSearching: function(){ return "Buscando..."; },
        ajax: {
            url: "api.php",
            dataType: 'json',
            quietMillis: 200,
            data: function(term, page) {
                return {
                    module: 'editarArticulo',
                    run: 'listar_rubros',
                    articulo: $('#articuloEditarModal_titulo').data("id_articulo"),
                    q: term,
                    p: page
                };
            },
            results: function(data, page ) {
                //Si el ususario no esta logueado refresh
                if(!data.logued) location.reload();

                //Calcula si existen mas resultados a mostrar
                var more = (page * 40) < data.result.total;

                //Devolvemos el valor de more para que selec2
                //sepa que debemos cargar mas resultados.
                return {results: data.result.items, more: more};
            }
        },
        formatInputTooShort: function () {
          return "";
        },
        formatResult: function(item) {

          if(parseInt(item.depth) == 0)
            return '<i class="badge" style="padding: 4px 6px; float: none; border-radius: 2px; background-color: #e6e6e6; margin-left: 0px; color: #444444; font-size: 11px; border: solid 1px #dadada;">'+item.bread+'</i> ';

          if(parseInt(item.depth) == 1)
            return '<i class="badge" style="padding: 4px 6px; float: none; border-radius: 2px; background-color: #e6e6e6; margin-left: 25px; color: #444444; font-size: 11px; border: solid 1px #dadada;">'+item.bread+'</i> ';

          if(parseInt(item.depth) == 2)
            return '<i class="badge" style="padding: 4px 6px; float: none; border-radius: 2px; background-color: #e6e6e6; margin-left: 50px; color: #444444; font-size: 11px; border: solid 1px #dadada;">'+item.bread+'</i> ';

          return item.bread;
        },
        formatSelection: function(item) {
          return item.bread;
        },
        initSelection : function (element, callback) {
          var elementText = $(element).attr('data-init-text');
        }
      }
    },
    { 
      id:       "fabricante",
      label:      "Fabricante",
      tipo:       "select2",
      mostrarEditar:  true,
      mostrarCrear:   true,
      config: {
        minimumInputLength: 0,
        placeholder: $(this).attr('placeholder'),
        allowClear: true,
        width: 'resolve',
        id: function(e) { return e.id; },
        formatNoMatches: function() { return 'Sin resultados'; },
        formatSearching: function(){ return "Buscando..."; },
        ajax: {
            url: "api.php",
            dataType: 'json',
            quietMillis: 200,
            data: function(term, page) {
                return {
                    module: 'editarArticulo',
                    run: 'listar_fabricantes',
                    articulo: $('#articuloEditarModal_titulo').data("id_articulo"),
                    q: term,
                    p: page
                };
            },
            results: function(data, page ) {
                //If session expire reload page...
                if(!data.logued) location.reload();

                var more = (page * 40) < data.result.total; // whether or not there are more results available

                // notice we return the value of more so Select2 knows if more results can be loaded
                return {results: data.result.items, more: more};
            }
        },
        formatInputTooShort: function () {
                  return "";
              },  
        formatResult: function(item) {
            
            if(item.id == -1) return '<i class="badge" style="padding: 4px 6px; float: none; border-radius: 2px; background-color: #e6e6e6; margin-right: 7px; color: #444444; font-size: 10px; border: solid 1px #dadada;">Detectar automáticamente</i> <b>'+item.nombre+' </b>';
            else
              return item.nombre;

        },
        formatSelection: function(item) {
            //return item.nombre;
            var nombre; 

            // Si el Fabricante lo detectamos
            // automaticamente
            if(item.id == -1) 
              return item.nombre + '<i class="badge" style="padding: 4px 6px; float: none; border-radius: 2px; background-color: #e6e6e6; margin-left: 7px; color: #444444; font-size: 10px; border: solid 1px #dadada;">Detectar Automáticamente</i>';
            else
              return item.nombre;

        },
        initSelection : function (element, callback) {
            var elementText = $(element).attr('data-init-text');
        }
      }
    },
    { 
      id:       "precio",
      label:      "Precio",
      tipo:       "input",
      mostrarEditar:  true,
      mostrarCrear:   true
    },
    { 
      id:       "descripcion",
      label:      "Descripción",
      tipo:       "input",
      mostrarEditar:  true,
      mostrarCrear:   true
    },
    { 
      id:       "habilitado",
      label:      "Habilitado",
      tipo:       "checkbox",
      mostrarEditar:  true,
      mostrarCrear:   true,
      config:     {
              claseActivo: 'btn-success',
              claseInactivo: 'btn-danger',
              valueActivo: '1',
              valueInactivo: '0',
              valueDefault: '1',
              labelActivo: function(el){
                el.html('Éste Artículo se encuentra habilitado, haga click para deshabilitarlo.');
              },
              labelInactivo: function(el){
                el.html('Éste Artículo se encuentra deshabilitado, haga click para habilitarlo.');
              }
      }
    }
];

//Abrir edicion General
//var articuloEditar_General_temp;
function articuloEditar_open(id,cod){

  //Creamos el popup
  abrir_modal('articuloEditar',600,false);

  //Mostramos la ventana
  $('#articuloEditarModal').modal({show:true});

  //Titulo...
  $('#articuloEditarModal_titulo').html('Cargando').data("id_articulo",id);

  //Cargando...
  $('#articuloEditarModal_container').html('<div style="display: block; text-align: center;"><br/><img src="./img/procesando.gif" width="30" height="30"></div>');


  //Llamada a la API
  var data = {};
  data['articulo'] = id;
  apix('editarArticulo','info',data, {
    ok: function(json){
      //Cargamos lista de aplicaciones
      moduloarticuloEditarAplicaciones_lista = json;

      //Titulo...
      $('#articuloEditarModal_titulo').html('Edición General Artículo <b>' + cod.split(' ').join('') + '</b>');

       //Contenido
      var contenido = $('<div class="scrollable" style="max-height: 400px; overflow-y: hidden;"></div>');

      //Agregamos todos los campos
      for(var k in articuloEditar_campos){
        var campo = articuloEditar_campos[k];
        contenido.append(genera_campo(campo, 'articuloEditarGeneral' ,true));
      }

      //Asignamos el contenido al popup
      $('#articuloEditarModal_container').html('<div id="articuloEditarGeneral_dialog" class="alert alert-success" role="alert" style="margin-top: 10px; display: none; text-align: left; max-height: 300px; overflow-y: auto;"></div>');
      $('#articuloEditarModal_container').append(contenido);

      //Evento al Rubro...
      $('#articuloEditarGeneral_rubro').on("change", function(e) {
        //Mostramos alerta en caso de ser necesario
        if($('#articuloEditarGeneral_dialog').html() != ''){
          $('#articuloEditarGeneral_dialog').show(500);
          abrir_modal('articuloEditar',800,false);
        }
      });

      //Agregamos scrollbars
      $('#articuloEditarModal_container .scrollable').mCustomScrollbar({theme: 'minimal-dark',scrollInertia: 100, mouseWheel:{ scrollAmount: 70 }});

      // Si el rubro esta definido
      if(json.result.rubro_id)
        $('#articuloEditarGeneral_rubro').val_({id: json.result.rubro_id, bread: json.result.rubro_bread});

      // Si el rubro esta definido
      if(json.result.marca_id)
        $('#articuloEditarGeneral_fabricante').val_({id: json.result.marca_id, nombre: json.result.marca_label});

      // Insertamos la descripcion
      $('#articuloEditarGeneral_descripcion').val_(json.result.descripcion);

      // Insertamos el precio
      $('#articuloEditarGeneral_precio').val_(json.result.precio);

      // Insertamos habilitado si/no
      $('#articuloEditarGeneral_habilitado').val_(json.result.habilitado);


      //Armamos el cuadro de dialogo
      var dialogoHtml = '';

      //Si el art. Tiene solamente Dimensiones asignadas
      if(json.result.dimensiones.length){
        dialogoHtml += 'Éste artículo tiene <a style="cursor: pointer;" rel="tooltip" data-html="true" title="<table style=\'margin: 5px;\'>';
        for(var k in json.result.dimensiones) dialogoHtml += '<tr><td align=\'right\'><b>'+json.result.dimensiones[k].nombre+':</b></td><td align=\'left\'>&nbsp;'+json.result.dimensiones[k].valor+'</td></tr>';
        dialogoHtml += '</table>">éstas dimensiones</a> asignadas';
      }

      //Si el art. Tiene equivalencias y ademas comparte las dimensiones
      if(json.result.dimensiones.length && json.result.equivalencias_dim.length){
        dialogoHtml += ' y las sincroniza con  <a style="cursor: pointer;" rel="tooltip" data-html="true" title="<table style=\'margin: 5px;\'>';
        for(var k in json.result.equivalencias_dim) dialogoHtml += '<tr><td align=\'right\'><b>'+json.result.equivalencias[k].marca+'</b></td><td align=\'left\'>&nbsp;'+json.result.equivalencias[k].codigo+'</td></tr>';
        dialogoHtml += '</table>">éstos artículos</a>. </br>Tenga en cuenta que, si cambia el Rubro de éste artículo o de cualquiera equivalente que sincronize dimensiones con él, las dimensiones se perderán así como el grupo de sincronización.';
      }

      //Si el art. Tiene solamente Dimensiones asignadas
      if(json.result.dimensiones.length && !json.result.equivalencias_dim.length)
        dialogoHtml += '.</br>Tenga en cuenta que, si cambia el Rubro de éste artículo, las dimensiones se perderán.';

      //Si el art. Tiene equivalencias y ademas comparte las dimensiones
      if(json.result.equivalencias.length){

        if(dialogoHtml != '') dialogoHtml += '</br></br>';
        dialogoHtml += 'En la siguiente lista figuran los artículos que conforman el grupo de equivalencias, si desea cambiar el Rubro de alguno de ellos marquelo con un tilde: <table style="width:100%; margin-top: 10px;">';
        
        //Creamos la tabla de equivalencias
        for(var k in json.result.equivalencias) dialogoHtml += '<tr><td style="width: 10px;"><input type="checkbox" value="'+json.result.equivalencias[k].id+'" name="articuloEditarGeneral_equivalencias[]"/></td><td style="text-align: center; padding: 0px 5px 0px 5px; white-space: nowrap;"><b>'+json.result.equivalencias[k].codigo+'</b></td><td style="text-align: center; padding: 0px 5px 0px 5px; white-space: nowrap;">'+json.result.equivalencias[k].marca+'</td><td width="70%" style="padding: 0px 5px 0px 5px; white-space: nowrap;">'+json.result.equivalencias[k].rubro+'</td></tr>';
        dialogoHtml += '</table>';
      }

      $('#articuloEditarGeneral_dialog').html(dialogoHtml);
      $('#articuloEditarModal_container [rel=tooltip]').tooltip();

      //Boton Confirmar cambios
      $('#articuloEditarModal_footer').append('<button type="button" class="btn btn-primary" style="display: block; width: 100%; margin: 10px auto;" onclick="articuloEditar_confirmar('+id+');">Guardar cambios</button>');

      //Append Cancel Button
      $('#articuloEditarModal_footer').append('<button type="button" data-dismiss="modal" class="btn btn-warning" style="display: block; width: 100%; margin: 10px auto;">Descartar cambios</button>');
    }
  });
}

//Confirmamos los cambios en el servidor
function articuloEditar_confirmar(id){
  
  var data = {};

  //ID del artículo
  data['articulo'] = $('#articuloEditarModal_titulo').data("id_articulo");

  //Obtenemos la descripcion
  data['descripcion'] = $('#articuloEditarGeneral_descripcion').val();

  //Obtenemos precio
  data['precio'] = $('#articuloEditarGeneral_precio').val();

  //Habilitado o no ?
  data['habilitado'] = parseInt( $('#articuloEditarGeneral_habilitado').val() );

  //Obtenemos el rubro
  data['rubro'] = $('#articuloEditarGeneral_rubro').select2('data');
  if(data['rubro'] != null) data['rubro'] = data['rubro'].id;

  //Obtenemos la fabricante
  data['fabricante'] = $('#articuloEditarGeneral_fabricante').select2('data');
  if(data['fabricante'] != null) data['fabricante'] = data['fabricante'].id;

  //Obtenemos equivalencias a las cuales queremos cambiar su Rubro tambien
  var equivalencias = new Array();
  $.each($("input[name='articuloEditarGeneral_equivalencias[]']:checked"), function() {
    equivalencias.push( parseInt($(this).val()) );
  });
  //Guardamos en data..
  data['equivalencias'] = JSON.stringify(equivalencias);
    
  //Llamada a la API
  apix('editarArticulo','editar',data, {
    ok: function(json){
      //Actualizamos la lista de articulos
      watable_volver = true;
      articulosUpdate();

      //Cerramos el popUp
      $('#articuloEditarModal').modal('hide');
    }
  });
}



//FUNCIONES COMUNES

//Recalcula la altura de la lista
function articuloEditar_recalcAlt(elId){
  //Calculamos altura de la lista total
  var altTot = ( $('#'+elId+' li').length * 44) + 10;

  //Altura disponible
  var altDisp = $(window).height() - 400;

  //Si la altura de los items es mayor a la altura 
  if(altTot > altDisp ) $('#'+elId).height(altDisp);
  else $('#'+elId).css('height','auto');
}

</script>