{if="$GLOBALS['permisos']['usuarios_listar']"}

<script type="text/javascript">

//Config Usuarios
var vehiculos_config = {
	modName: 					'parametrosAPI',
	modLabelSingular: 			'Parametro',
	modLabelPlural: 			'Parametros',
	creaLabel: 					'Agregado',
	editaLabel: 				'Editado',
	eliminaLabel: 				'Eliminado',
	nuevoLabel: 				'Nuevo',
	artLabel: 					'El',
	max_height_edita_agrega: 	'300',
	max_height_lista: 			'300',

};


//Lista Principal Vehiculos
function vehiculos_listar(id){

	//Creamos el popup
	abrir_modal(vehiculos_config.modName, 550, false);

	//Mostramos la ventana
	$('#'+vehiculos_config.modName+'Modal').modal({show:true});

	//Titulo...
	$('#'+vehiculos_config.modName+'Modal_titulo').html('Vehículos');

	//Cargando...
	$('#'+vehiculos_config.modName+'Modal_container').html( cargando() );

	//Footer...
	$('#'+vehiculos_config.modName+'Modal_footer').html('');

	//Traemos los datos de la API

	//Contenido
	var contenido = $('<div class="scrollable" style="max-height: '+vehiculos_config.max_height_edita_agrega+'px; overflow-y: hidden;"></div>');

	//Generamos Seleccionador de Acciones de la API
	var apiselect2  = genera_campo({	
			id: 			"marcas_modelos",
			label: 			"Modelo de Vehículo",
			tipo: 			"select2",
			mostrarEditar: 	true,
			mostrarCrear: 	true,
			config: 		{
								minimumInputLength: 0,
								placeholder: $(this).attr('placeholder'),
								allowClear: false,
								width: 'resolve',
								id: function(e) { return e.modelo_id; },
								formatNoMatches: function() { return 'Sin resultados'; },
								formatSearching: function(){ return "Buscando..."; },
								ajax: {
								    url: "api.php",
								    dataType: 'json',
								    quietMillis: 200,
								    data: function(term, page) {
								        return {
								            module: 'vehiculos',
								            run: 'listar_marcas_modelos_s2',
								            q: term,
								            p: page
								        };
								    },
								    results: function(data, page ) {
								        //Si el ususario no esta logueado refresh
								        if(!data.logued) location.reload();

								        //Calcula si existen mas resultados a mostrar
								        var more = (page * 40) < data.result.total;

								        //Devolvemos el valor de more para que selec2
								        //sepa que debemos cargar mas resultados.
								        return {results: data.result.items, more: more};
								    }
								},
								formatInputTooShort: function () {
									return "";
								},
								formatResult: function(item) {
									return '<i class="badge" style="padding: 4px 6px; float: none; border-radius: 2px; background-color: #e6e6e6; margin-right: 7px; color: #444444; font-size: 11px; border: solid 1px #dadada;">'+item.marca_nombre+'</i> '+item.modelo_nombre;
								},
								formatSelection: function(item) { 
									return '<i class="badge" style="padding: 4px 6px; float: none; border-radius: 2px; background-color: #e6e6e6; margin-right: 7px; color: #444444; font-size: 11px; border: solid 1px #dadada;">'+item.marca_nombre+'</i> '+item.modelo_nombre;
								},
								initSelection : function (element, callback) {
									var elementText = $(element).attr('data-init-text');
								}
							}
			}, 'vehiculos' ,true);

	//Agregamos el select al contenido
	contenido.append(apiselect2);

	var tabla = generaTabla({
					id_tabla: 'vehiculos_versiones',
					items: {},
					keys_busqueda: ['descripcion'],
					key_nombre: null,
					campobusqueda: false,
					max_height: vehiculos_config.max_height_lista,
					funcion_activado: function(el){

						//Boton Borrar
						var boton_eliminar = '<button class="btn btn-xs btn-danger" style="margin: 0px 0px 8px 5px; width: 24px; height: 24px;" data-toggle="tooltip" title="Borrar" onclick=" confirma(\'Eliminar Versión\',\'La Versión y las aplicaciones a los Artículos se eliminarán de forma permanente.\',\'exclamacion\',\'danger\',\'vehiculos_version_elimina(\\\''+$(el).data('id')+'\\\')\');"><i class="fa fa-remove"></i></button>';

						//Boton editar
						var boton_editar = '<button class="btn btn-xs btn-primary" style="margin: 0px 0px 8px 5px; width: 24px; height: 24px;" data-toggle="tooltip" title="Editar" onclick="vehiculos_version_edita_agrega(\''+$(el).data('id')+'\');"><i class="fa fa-pencil"></i></button>';

						var html = '<small><b>' + $(el).data('descripcion') + '</b>';

						html += '<div style="float: right; display: inline-block; margin-top: -2px;">'+boton_editar+boton_eliminar+'</div></small>';

						//Agregamos los datos
						$(el).html(html);

					},
					funcion_desactivado: function(el){
						$(el).html('<small><b>' + $(el).data('descripcion') + '</b></small>');
					}
				});
	
	//Evento onchange de select2
	apiselect2.on("change", function(e) {
		//Select2 val..
		var modelo = $('#vehiculos_marcas_modelos').val_();

		//Rescatamos el ID
		if(modelo != null) modelo = modelo.modelo_id;

		console.log(modelo);

		//Llamada a la API
		var data = {};
		data['modelo'] = modelo;
		apix('vehiculos','listar_versiones',data, {
            ok: function(json){
				$('#vehiculos_versiones_lista').data('plugin_update')( $('#vehiculos_versiones_lista'), json.result);
			}
		});
	});
	
	//Cargamos datos anteriores
	if(typeof id !== 'undefined'){
		var data = {};
		data['modelo'] = id;

		//Cargamos info del modelo
		apix('vehiculos','info_modelo',data, {
            ok: function(json){
				//Cargamos el modelo
				$('#vehiculos_marcas_modelos',contenido).val_({
					marca_id: json.result.marca_id, 
					marca_nombre: json.result.marca_nombre,
					modelo_id: json.result.id,
					modelo_nombre: json.result.nombre });
			}
		});

		//Cargamos versiones
		apix('vehiculos','listar_versiones',data, {
            ok: function(json){
				$('#vehiculos_versiones_lista').data('plugin_update')( $('#vehiculos_versiones_lista'), json.result);
			}
		});
	}



	//Agregamos los tooltips
	tabla.tooltip({selector: '[data-toggle="tooltip"]', placement: 'left', animation: false});

	//Generamos el listado
	$('#'+vehiculos_config.modName+'Modal_container').html('');
	//$('#'+vehiculos_config.modName+'Modal_container').append(tabla);
	$('#'+vehiculos_config.modName+'Modal_container').append(contenido, tabla);

	//Agregamos scrollbars
	$('#'+vehiculos_config.modName+'Modal_container .scrollable').mCustomScrollbar({theme: 'minimal-dark',scrollInertia: 100, mouseWheel:{ scrollAmount: 70 }});

	//Boton Agregar
	$('#'+vehiculos_config.modName+'Modal_footer').append('<button type="button" class="btn btn-success" style="display: block; width: 49%; margin: 0px 0px 10px 0px; float: left;" onclick="vehiculos_marcas_listar();">Administrar Marcas</button>');

	//Boton Agregar
	$('#'+vehiculos_config.modName+'Modal_footer').append('<button type="button" class="btn btn-success" style="display: block; width: 49%; margin: 0px 0px 10px 0px; float: right;" onclick="vehiculos_modelos_listar();">Administrar Modelos</button>');

	//Append Cancel Button
	$('#'+vehiculos_config.modName+'Modal_footer').append('<button type="button" class="btn btn-primary" style="display: block; width: 100%; margin: 10px auto;" onclick="vehiculos_version_edita_agrega();">Agregar Versión</button>');

	//Append Cancel Button
	$('#'+vehiculos_config.modName+'Modal_footer').append('<button type="button" data-dismiss="modal" class="btn btn-warning" style="display: block; width: 100%; margin: 10px auto;">Cerrar</button>');
}

//Edita/Agrega Version (layout)
function vehiculos_version_edita_agrega(id){
	
	//Conseguimos el modelo
	var modelo = $('#vehiculos_marcas_modelos').val_();
	$('#'+vehiculos_config.modName+'Modal').data('marca_modelo',modelo);

	//Si estamos agregando...
	if(typeof id === 'undefined'){
		var agrega = true;

		//Si el modelo es null
		if(modelo == null){
			notifica(	'Atención!',
						'Debe seleccionar un modelo antes de agregar una Versión.',
						'exclamacion','warning');
			return;
		}
	}else{
		var agrega = false;
	}

	//Titulo...
	if(agrega) $('#'+vehiculos_config.modName+'Modal_titulo').html('Vehículos <br/> <small> Agregar Versión</small>');
	else $('#'+vehiculos_config.modName+'Modal_titulo').html('Vehículos <br/> <small> Editar Versión</small>');

	//Cargando...
	$('#'+vehiculos_config.modName+'Modal_container').html(cargando());

	//Footer...
	$('#'+vehiculos_config.modName+'Modal_footer').html('');

	//Contenido
	var contenido = $('<div class="scrollable" style="max-height: '+vehiculos_config.max_height_edita_agrega+'px; overflow-y: hidden;"></div>');

	//Campo
	contenido.append( genera_campo({	
		id: 			"nombre",
		label: 			"Nombre de la Versión",
		tipo: 			"input",
		mostrarEditar: 	false,
		mostrarCrear: 	true
	}, 'vehiculos_version' ,true));

	//Agregamos el contenido y los botones
	//si estamos creando un registro nuevo
	if(agrega){
		//Agregamos el contenido
		$('#'+vehiculos_config.modName+'Modal_container').html(contenido);

		//Agregamos scrollbars
		$('#'+vehiculos_config.modName+'Modal_container .scrollable').mCustomScrollbar({theme: 'minimal-dark',scrollInertia: 100, mouseWheel:{ scrollAmount: 70 }});

		//Agregamos los botones al footer
		$('#'+vehiculos_config.modName+'Modal_footer').append('<button type="button" class="btn btn-primary" style="display: block; width: 100%; margin: 10px auto;" onclick="vehiculos_version_agrega_confirm('+id+','+modelo.modelo_id+');">Agregar Versión</button>');
		$('#'+vehiculos_config.modName+'Modal_footer').append('<button type="button" class="btn btn-warning" style="display: block; width: 100%; margin: 10px auto;" onclick="vehiculos_listar('+modelo.modelo_id+');">Volver</button>');
	}


	//Si editamos
	if(!agrega){
		var data = {};
		data['version'] = id;

		apix('vehiculos','info_version',data, {
            ok: function(json){

				//Asignamos el nombre
		    	$('#vehiculos_version_nombre',contenido).val_(json.result.nombre);

		    	//Asignamos el contenido al popup
				$('#'+vehiculos_config.modName+'Modal_container').html(contenido);

				//Agregamos scrollbars
				$('#'+vehiculos_config.modName+'Modal_container .scrollable').mCustomScrollbar({theme: 'minimal-dark',scrollInertia: 100, mouseWheel:{ scrollAmount: 70 }});

				//Agregamos los botones al footer
				$('#'+vehiculos_config.modName+'Modal_footer').append('<button type="button" class="btn btn-primary" style="display: block; width: 100%; margin: 10px auto;" onclick="vehiculos_version_edita_confirm('+id+','+modelo.modelo_id+');">Editar Versión</button>');

				$('#'+vehiculos_config.modName+'Modal_footer').append('<button type="button" class="btn btn-warning" style="display: block; width: 100%; margin: 10px auto;" onclick="vehiculos_listar('+modelo.modelo_id+');">Volver</button>');
			}
		});
	}
}

//Edita Confirm
function vehiculos_version_edita_confirm(id,modelo){
	//Llamada a la API
	var data = {};
	data['version'] = id;
	data['nombre'] = $('#vehiculos_version_nombre').val_();
	apix('vehiculos','editar_version',data, {
		ok: function(json){
			notifica(	'Versión Modificada!',
						'La Versión del Vehículo se editó con éxito',
						'confirmacion','success');
			vehiculos_listar(modelo);
		}
	});
}

//Agrega Confirm
function vehiculos_version_agrega_confirm(id,modelo){
	var marca_modelo_recuperado = $('#'+vehiculos_config.modName+'Modal').data('marca_modelo');
	//Llamada a la API
	var data = {};
	data['modelo'] = marca_modelo_recuperado.modelo_id;
	data['nombre'] = $('#vehiculos_version_nombre').val_();
	apix('vehiculos','agregar_version',data, {
		ok: function(json){
			notifica(	'Versión Agregada!',
						'Se creó la nueva Versión para <b>'+marca_modelo_recuperado.modelo_nombre+' '+marca_modelo_recuperado.marca_nombre,
						'confirmacion','success');
			vehiculos_listar(modelo);
		}
	});
}

//Elimina Confirm
function vehiculos_version_elimina(id,modelo){
	
	//Conseguimos el modelo
	var modelo = $('#vehiculos_marcas_modelos').val_();
	$('#'+vehiculos_config.modName+'Modal').data('marca_modelo',modelo);

	//Llamada a la API
	var data = {};
	data['version'] = id;
	apix('vehiculos','eliminar_version',data, {
		ok: function(json){
			notifica(	'Versión Eliminada!',
						'La Versión del Vehículo se eliminó con éxito',
						'confirmacion','success');
			vehiculos_listar(modelo.modelo_id);
		}
	});
}

//Lista las Marcas
function vehiculos_marcas_listar(){

	//Mostramos la ventana
	$('#'+vehiculos_config.modName+'Modal').modal({show:true});

	//Titulo...
	$('#'+vehiculos_config.modName+'Modal_titulo').html('Vehículos <br/> <small> Administra las Marcas</small>');

	//Cargando...
	$('#'+vehiculos_config.modName+'Modal_container').html( cargando() );

	//Footer...
	$('#'+vehiculos_config.modName+'Modal_footer').html('');

	//Traemos los datos de la API
	apix('vehiculos','listar_marcas',{}, {
		ok: function(json){

			var tabla = generaTabla({
							id_tabla: vehiculos_config.modName,
							items: json.result,
							keys_busqueda: ['nombre'],
							key_nombre: null,
							max_height: vehiculos_config.max_height_lista,
							funcion_activado: function(el){

								//Seleccionamos el activo anterior
								var elActivo = $('#usuarios_lista').find('.active_');

								//Boton Borrar
								var boton_eliminar = '<button class="btn btn-xs btn-danger" style="margin: 0px 0px 8px 5px; width: 24px; height: 24px;" data-toggle="tooltip" title="Borrar" onclick=" confirma(\'Eliminar Marca\',\'Si elimina la Marca perderán todos los Modelos, Versiones y Aplicaciones asociadas a esta.\',\'exclamacion\',\'danger\',\'vehiculos_marcas_elimina(\\\''+$(el).data('id')+'\\\')\');"><i class="fa fa-remove"></i></button>';

								//Boton editar
								var boton_editar = '<button class="btn btn-xs btn-primary" style="margin: 0px 0px 8px 5px; width: 24px; height: 24px;" data-toggle="tooltip" title="Editar" onclick="vehiculos_marcas_edita_agrega(\''+$(el).data('id')+'\');"><i class="fa fa-pencil"></i></button>';

								//Boton Versiones
								var boton_modelos = '<button class="btn btn-xs btn-primary" style="margin: 0px 0px 8px 5px; height: 24px;" data-toggle="tooltip" title="Administrar Modelos" onclick="vehiculos_modelos_listar(\''+$(el).data('id')+'\');"><i class="fa fa-sitemap"></i></button>';

								var html = '<b style="line-height: 22px;">' + $(el).data('nombre') + '</b>';

								if(parseInt($(el).data('modelos'))) html += '<i class="badge" style="padding: 4px 7px; display: inline-block; float:none; border-radius: 3px; background-color: #e6e6e6; margin: -2px 0px 0px 7px; color: #444444; font-size: 11px; border: solid 1px #dadada; font-weight: 600;"><b>' + $(el).data('modelos') + ' Modelos</b></i>';
								else html += '<i class="badge" style="padding: 4px 7px; display: inline-block; float:none; border-radius: 3px; background-color: #e6e6e6; margin: -2px 0px 0px 7px; color: #444444; font-size: 11px; border: solid 1px #dadada; font-weight: 600;"><b>Sin Modelos</b></i>';

								html += '<div style="display: inline-block; margin-top: -2px; float: right;">'+boton_modelos+boton_editar+boton_eliminar+'</div>';

								//Agregamos los datos
								$(el).html(html);

							},
							funcion_desactivado: function(el){
								$(el).html('<b style="line-height: 22px;">'+ $(el).data('nombre') + '</b>');
							}
						});

			//Agregamos los tooltips
			tabla.tooltip({selector: '[data-toggle="tooltip"]', placement: 'left', animation: false});

			//Generamos el listado
			$('#'+vehiculos_config.modName+'Modal_container').html('');
			$('#'+vehiculos_config.modName+'Modal_container').append(tabla);


			//Agregamos scrollbars
			$('#'+vehiculos_config.modName+'Modal_container .scrollable').mCustomScrollbar({theme: 'minimal-dark',scrollInertia: 100, mouseWheel:{ scrollAmount: 70 }});

			{if="$GLOBALS['permisos']['usuarios_crear']"}
			//Boton Agregar
			$('#'+vehiculos_config.modName+'Modal_footer').append('<button type="button" class="btn btn-primary" style="display: block; width: 100%; margin: 10px auto;" onclick="vehiculos_marcas_edita_agrega();">Agregar Marca</button>');
			{/if}

			$('#'+vehiculos_config.modName+'Modal_footer').append('<button type="button" class="btn btn-warning" style="display: block; width: 100%; margin: 10px auto;" onclick="vehiculos_listar();">Volver</button>');
		}
	});
}

//Edita/Agrega Marca (layout)
function vehiculos_marcas_edita_agrega(id){
	
	//Si estamos agregando...
	if(typeof id === 'undefined') var agrega = true;
	else var agrega = false;

	//Titulo...
	if(agrega) $('#'+vehiculos_config.modName+'Modal_titulo').html('Vehículos <br/> <small> Agregar Marca</small>');
	else $('#'+vehiculos_config.modName+'Modal_titulo').html('Vehículos <br/> <small> Editar Marca</small>');

	//Cargando...
	$('#'+vehiculos_config.modName+'Modal_container').html(cargando());

	//Footer...
	$('#'+vehiculos_config.modName+'Modal_footer').html('');

	//Contenido
	var contenido = $('<div class="scrollable" style="max-height: '+vehiculos_config.max_height_edita_agrega+'px; overflow-y: hidden;"></div>');

	//Campo
	contenido.append( genera_campo({	
		id: 			"nombre",
		label: 			"Nombre de la Marca",
		tipo: 			"input",
		mostrarEditar: 	false,
		mostrarCrear: 	true
	}, 'vehiculos_marcas' ,true));

	//Agregamos el contenido y los botones
	//si estamos creando un registro nuevo
	if(agrega){
		//Agregamos el contenido
		$('#'+vehiculos_config.modName+'Modal_container').html(contenido);

		//Agregamos scrollbars
		$('#'+vehiculos_config.modName+'Modal_container .scrollable').mCustomScrollbar({theme: 'minimal-dark',scrollInertia: 100, mouseWheel:{ scrollAmount: 70 }});

		//Agregamos los botones al footer
		$('#'+vehiculos_config.modName+'Modal_footer').append('<button type="button" class="btn btn-primary" style="display: block; width: 100%; margin: 10px auto;" onclick="vehiculos_marcas_agrega_confirm();">Agregar Versión</button>');
		$('#'+vehiculos_config.modName+'Modal_footer').append('<button type="button" class="btn btn-warning" style="display: block; width: 100%; margin: 10px auto;" onclick="vehiculos_marcas_listar();">Volver</button>');
	}


	//Si editamos
	if(!agrega){
		var data = {};
		data['marca'] = id;

		apix('vehiculos','info_marca',data, {
            ok: function(json){

				//Asignamos el nombre
		    	$('#vehiculos_marcas_nombre',contenido).val_(json.result.nombre);

		    	//Asignamos el contenido al popup
				$('#'+vehiculos_config.modName+'Modal_container').html(contenido);

				//Agregamos scrollbars
				$('#'+vehiculos_config.modName+'Modal_container .scrollable').mCustomScrollbar({theme: 'minimal-dark',scrollInertia: 100, mouseWheel:{ scrollAmount: 70 }});

				//Agregamos los botones al footer
				$('#'+vehiculos_config.modName+'Modal_footer').append('<button type="button" class="btn btn-primary" style="display: block; width: 100%; margin: 10px auto;" onclick="vehiculos_marcas_edita_confirm(\''+id+'\');">Editar Marca</button>');

				$('#'+vehiculos_config.modName+'Modal_footer').append('<button type="button" class="btn btn-warning" style="display: block; width: 100%; margin: 10px auto;" onclick="vehiculos_marcas_listar();">Volver</button>');
			}
		});
	}
}

//Edita Confirm
function vehiculos_marcas_edita_confirm(id){
	//Llamada a la API
	var data = {};
	data['marca'] = id;
	data['nombre'] = $('#vehiculos_marcas_nombre').val_();
	apix('vehiculos','editar_marca',data, {
		ok: function(json){
			notifica(	'Marca Modificada!',
						'La Marca del Vehículo se editó con éxito',
						'confirmacion','success');
			vehiculos_marcas_listar();
		}
	});
}

//Agrega Confirm
function vehiculos_marcas_agrega_confirm(id){
	//Llamada a la API
	var data = {};
	data['nombre'] = $('#vehiculos_marcas_nombre').val_();
	apix('vehiculos','agregar_marca',data, {
		ok: function(json){
			notifica(	'Marca Agregada!',
						'Se creó la nueva Marca <b>'+$('#vehiculos_marcas_nombre').val_()+'</b>',
						'confirmacion','success');
			vehiculos_marcas_listar();
		}
	});
}

//Elimina Confirm
function vehiculos_marcas_elimina(id){

	//Llamada a la API
	var data = {};
	data['marca'] = id;
	apix('vehiculos','eliminar_marca',data, {
		ok: function(json){
			notifica(	'Marca Eliminada!',
						'La Marca del Vehículo se eliminó con éxito',
						'confirmacion','success');
			vehiculos_marcas_listar();
		}
	});
}

//Lista Principal Vehiculos
function vehiculos_modelos_listar(id){

	//Creamos el popup
	abrir_modal(vehiculos_config.modName, 550, false);

	//Mostramos la ventana
	$('#'+vehiculos_config.modName+'Modal').modal({show:true});

	//Titulo...
	$('#'+vehiculos_config.modName+'Modal_titulo').html('Vehículos <br/> <small> Administra los Modelos</small>');

	//Cargando...
	$('#'+vehiculos_config.modName+'Modal_container').html( cargando() );

	//Footer...
	$('#'+vehiculos_config.modName+'Modal_footer').html('');

	//Traemos los datos de la API

	//Contenido
	var contenido = $('<div class="scrollable" style="max-height: '+vehiculos_config.max_height_edita_agrega+'px; overflow-y: hidden;"></div>');

	//Generamos Seleccionador de Acciones de la API
	var apiselect2  = genera_campo({	
			id: 			"marcas",
			label: 			"Marca del Vehículo",
			tipo: 			"select2",
			mostrarEditar: 	true,
			mostrarCrear: 	true,
			config: 		{
								minimumInputLength: 0,
								placeholder: $(this).attr('placeholder'),
								allowClear: false,
								width: 'resolve',
								id: function(e) { return e.id; },
								formatNoMatches: function() { return 'Sin resultados'; },
								formatSearching: function(){ return "Buscando..."; },
								ajax: {
								    url: "api.php",
								    dataType: 'json',
								    quietMillis: 200,
								    data: function(term, page) {
								        return {
								            module: 'vehiculos',
								            run: 'listar_marcas_s2',
								            q: term,
								            p: page
								        };
								    },
								    results: function(data, page ) {
								        //Si el ususario no esta logueado refresh
								        if(!data.logued) location.reload();

								        //Calcula si existen mas resultados a mostrar
								        var more = (page * 40) < data.result.total;

								        //Devolvemos el valor de more para que selec2
								        //sepa que debemos cargar mas resultados.
								        return {results: data.result.items, more: more};
								    }
								},
								formatInputTooShort: function () {
									return "";
								},
								formatResult: function(item) {
									return item.nombre;
								},
								formatSelection: function(item) { 
									return item.nombre;
								},
								initSelection : function (element, callback) {
									var elementText = $(element).attr('data-init-text');
								}
							}
			}, 'vehiculos' ,true);

	//Agregamos el select al contenido
	contenido.append(apiselect2);

	var tabla = generaTabla({
					id_tabla: 'vehiculos_marcas',
					items: {},
					keys_busqueda: ['nombre'],
					key_nombre: null,
					campobusqueda: false,
					max_height: vehiculos_config.max_height_lista,
					funcion_activado: function(el){

						//Boton Borrar
						var boton_eliminar = '<button class="btn btn-xs btn-danger" style="margin: 0px 0px 8px 5px; width: 24px; height: 24px;" data-toggle="tooltip" title="Borrar" onclick=" confirma(\'Eliminar Modelo\',\'Si elimina el Modelo se perderán todas las Versiones y Aplicaciones asociadas a ésta.\',\'exclamacion\',\'danger\',\'vehiculos_modelos_elimina(\\\''+$(el).data('id')+'\\\')\');"><i class="fa fa-remove"></i></button>';

						//Boton editar
						var boton_editar = '<button class="btn btn-xs btn-primary" style="margin: 0px 0px 8px 5px; width: 24px; height: 24px;" data-toggle="tooltip" title="Editar" onclick="vehiculos_modelos_edita_agrega(\''+$(el).data('id')+'\');"><i class="fa fa-pencil"></i></button>';

						//Boton Versiones
						var boton_versiones = '<button class="btn btn-xs btn-primary" style="margin: 0px 0px 8px 5px; height: 24px;" data-toggle="tooltip" title="Administrar Versiones" onclick="vehiculos_listar(\''+$(el).data('id')+'\');"><i class="fa fa-sitemap"></i></button>';

						$('#'+vehiculos_config.modName+'Modal').data('versiones_modelo')

						var html = '<b style="line-height: 22px;">' + $(el).data('nombre') + '</b>';

						if(parseInt($(el).data('versiones'))) html += '<i class="badge" style="padding: 4px 7px; display: inline-block; float:none; border-radius: 3px; background-color: #e6e6e6; margin: -2px 0px 0px 7px; color: #444444; font-size: 11px; border: solid 1px #dadada; font-weight: 600;"><b>' + $(el).data('versiones') + ' Versiones</b></i>';
						else html += '<i class="badge" style="padding: 4px 7px; display: inline-block; float:none; border-radius: 3px; background-color: #e6e6e6; margin: -2px 0px 0px 7px; color: #444444; font-size: 11px; border: solid 1px #dadada; font-weight: 600;"><b>Sin Versiones</b></i>';

						html += '<div style="display: inline-block; margin-top: -2px; float: right;">'+boton_versiones+boton_editar+boton_eliminar+'</div>';

						//Agregamos los datos
						$(el).html(html);

					},
					funcion_desactivado: function(el){
						$(el).html('<small><b>' + $(el).data('nombre') + '</b></small>');
					}
				});
	
	//Evento onchange de select2
	apiselect2.on("change", function(e) {
		//Select2 val..
		var marca = $('#vehiculos_marcas').val_();

		//Rescatamos el ID
		if(marca != null){
			marca = marca.id;

			//Llamada a la API
			var data = {};
			data['marca'] = marca;
			apix('vehiculos','listar_modelos',data, {
            	ok: function(json){
					$('#vehiculos_marcas_lista').data('plugin_update')( $('#vehiculos_marcas_lista'), json.result);
				}
			});
		}
	});
	
	//Cargamos datos anteriores
	if(typeof id !== 'undefined'){
		var data = {};
		data['marca'] = id;

		//Cargamos info de la marca
		apix('vehiculos','info_marca',data, {
            ok: function(json){
				//Cargamos el modelo
				$('#vehiculos_marcas',contenido).val_({ id: id, nombre: json.result.nombre });
			}
		});

		//Cargamos modelos
		apix('vehiculos','listar_modelos',data, {
            ok: function(json){
				$('#vehiculos_marcas_lista').data('plugin_update')( $('#vehiculos_marcas_lista'), json.result);
			}
		});
	}

	//Agregamos los tooltips
	tabla.tooltip({selector: '[data-toggle="tooltip"]', placement: 'left', animation: false});

	//Generamos el listado
	$('#'+vehiculos_config.modName+'Modal_container').html('');
	//$('#'+vehiculos_config.modName+'Modal_container').append(tabla);
	$('#'+vehiculos_config.modName+'Modal_container').append(contenido, tabla);

	//Agregamos scrollbars
	$('#'+vehiculos_config.modName+'Modal_container .scrollable').mCustomScrollbar({theme: 'minimal-dark',scrollInertia: 100, mouseWheel:{ scrollAmount: 70 }});

	//Append Cancel Button
	$('#'+vehiculos_config.modName+'Modal_footer').append('<button type="button" class="btn btn-primary" style="display: block; width: 100%; margin: 10px auto;" onclick="vehiculos_modelos_edita_agrega();">Agregar Modelo</button>');

	//Append Cancel Button
	$('#'+vehiculos_config.modName+'Modal_footer').append('<button type="button" class="btn btn-warning" style="display: block; width: 100%; margin: 10px auto;" onclick="vehiculos_listar();">Volver</button>');
}

//Edita/Agrega Marca (layout)
function vehiculos_modelos_edita_agrega(id){

	//Conseguimos el modelo
	var marca = $('#vehiculos_marcas').val_();

	//Si estamos agregando...
	if(typeof id === 'undefined'){
		var agrega = true;

		//Si el modelo es null
		if(marca == null){
			notifica(	'Atención!',
						'Debe seleccionar una Marca antes de agregar un Modelo.',
						'exclamacion','warning');
			return;
		}
	}else{
		var agrega = false;
	}

	
	//Si estamos agregando...
	if(typeof id === 'undefined') var agrega = true;
	else var agrega = false;

	//Titulo...
	if(agrega) $('#'+vehiculos_config.modName+'Modal_titulo').html('Vehículos <br/> <small> Agregar Modelo</small>');
	else $('#'+vehiculos_config.modName+'Modal_titulo').html('Vehículos <br/> <small> Editar Modelo</small>');

	//Cargando...
	$('#'+vehiculos_config.modName+'Modal_container').html(cargando());

	//Footer...
	$('#'+vehiculos_config.modName+'Modal_footer').html('');

	//Contenido
	var contenido = $('<div class="scrollable" style="max-height: '+vehiculos_config.max_height_edita_agrega+'px; overflow-y: hidden;"></div>');

	//Campo
	contenido.append( genera_campo({	
		id: 			"nombre",
		label: 			"Nombre del Modelo",
		tipo: 			"input",
		mostrarEditar: 	false,
		mostrarCrear: 	true
	}, 'vehiculos_modelos' ,true));

	//Agregamos el contenido y los botones
	//si estamos creando un registro nuevo
	if(agrega){
		//Agregamos el contenido
		$('#'+vehiculos_config.modName+'Modal_container').html(contenido);

		//Agregamos scrollbars
		$('#'+vehiculos_config.modName+'Modal_container .scrollable').mCustomScrollbar({theme: 'minimal-dark',scrollInertia: 100, mouseWheel:{ scrollAmount: 70 }});

		//Agregamos los botones al footer
		$('#'+vehiculos_config.modName+'Modal_footer').append('<button type="button" class="btn btn-primary" style="display: block; width: 100%; margin: 10px auto;" onclick="vehiculos_modelos_agrega_confirm('+marca.id+');">Agregar Modelo</button>');
		$('#'+vehiculos_config.modName+'Modal_footer').append('<button type="button" class="btn btn-warning" style="display: block; width: 100%; margin: 10px auto;" onclick="vehiculos_modelos_listar('+marca.id+');">Volver</button>');
	}


	//Si editamos
	if(!agrega){
		var data = {};
		data['modelo'] = id;

		apix('vehiculos','info_modelo',data, {
            ok: function(json){

				//Asignamos el nombre
		    	$('#vehiculos_modelos_nombre',contenido).val_(json.result.nombre);

		    	//Asignamos el contenido al popup
				$('#'+vehiculos_config.modName+'Modal_container').html(contenido);

				//Agregamos scrollbars
				$('#'+vehiculos_config.modName+'Modal_container .scrollable').mCustomScrollbar({theme: 'minimal-dark',scrollInertia: 100, mouseWheel:{ scrollAmount: 70 }});

				//Agregamos los botones al footer
				$('#'+vehiculos_config.modName+'Modal_footer').append('<button type="button" class="btn btn-primary" style="display: block; width: 100%; margin: 10px auto;" onclick="vehiculos_modelos_edita_confirm('+id+','+marca.id+');">Editar Modelo</button>');

				$('#'+vehiculos_config.modName+'Modal_footer').append('<button type="button" class="btn btn-warning" style="display: block; width: 100%; margin: 10px auto;" onclick="vehiculos_modelos_listar('+marca.id+');">Volver</button>');
			}
		});
	}
}

//Edita Confirm
function vehiculos_modelos_edita_confirm(id,marca){
	//Llamada a la API
	var data = {};
	data['modelo'] = id;
	data['nombre'] = $('#vehiculos_modelos_nombre').val_();
	apix('vehiculos','editar_modelo',data, {
		ok: function(json){
			notifica(	'Modelo Modificado!',
						'El Modelo <b>'+$('#vehiculos_modelos_nombre').val_()+'</b> se editó con éxito',
						'confirmacion','success');
			vehiculos_modelos_listar(marca);
		}
	});
}

//Agrega Confirm
function vehiculos_modelos_agrega_confirm(marca){
	//Llamada a la API
	var data = {};
	data['nombre'] = $('#vehiculos_modelos_nombre').val_();
	data['marca'] = marca;
	apix('vehiculos','agregar_modelo',data, {
		ok: function(json){
			notifica(	'Modelo Agregado!',
						'Se creó el nuevo Modelo <b>'+$('#vehiculos_modelos_nombre').val_()+'</b>',
						'confirmacion','success');
			vehiculos_modelos_listar(marca);
		}
	});
}

//Elimina Confirm
function vehiculos_modelos_elimina(id,marca){

	//Conseguimos el marca
	var marca = $('#vehiculos_marcas').val_();

	//Llamada a la API
	var data = {};
	data['modelo'] = id;
	apix('vehiculos','eliminar_modelo',data, {
		ok: function(json){
			notifica(	'Modelo Eliminado!',
						'El Modelo del Vehículo se eliminó con éxito',
						'confirmacion','success');
			vehiculos_modelos_listar(marca.id);
		}
	});
}
</script>

{/if}