<script type="text/javascript">

var login_crear_cuenta_config = {
  modName:          'clientes',
  modLabelSingular:       'Cliente',
  modLabelPlural:       'Clientes',
  creaLabel:          'Agregado',
  editaLabel:         'Editado',
  eliminaLabel:         'Eliminado',
  nuevoLabel:         'Nuevo',
  artLabel:           'El',
  max_height_edita_agrega:  '350',
  max_height_lista:       '350',
  campos:[
    { 
      id:       "cliente",
      label:      "Cliente",
      tipo:       "input",
      mostrarEditar:  false,
      mostrarCrear:   true
    },
    { 
      id:       "cuit",
      label:      "CUIT",
      tipo:       "input",
      mostrarEditar:  true,
      mostrarCrear:   true
    },
    { 
      id:       "nombre_del_comercio",
      label:      "Nombre del Comercio",
      tipo:       "input",
      mostrarEditar:  true,
      mostrarCrear:   true
    },
    { 
      id:       "razon_social",
      label:      "Razón Social",
      tipo:       "input",
      mostrarEditar:  true,
      mostrarCrear:   true
    },
    { 
      id:       "email",
      label:      "Email",
      tipo:       "input",
      mostrarEditar:  true,
      mostrarCrear:   true
    },
    { 
      id:       "persona_de_contacto",
      label:      "Persona de contacto",
      tipo:       "input",
      mostrarEditar:  true,
      mostrarCrear:   true
    },
    { 
      id:       "telefono",
      label:      "Teléfono",
      tipo:       "input",
      mostrarEditar:  true,
      mostrarCrear:   true
    },
    { 
      id:       "domicilio",
      label:      "Domicilio",
      tipo:       "input",
      mostrarEditar:  true,
      mostrarCrear:   true
    },
    { 
      id:       "localidad",
      label:      "Localidad",
      tipo:       "select2",
      mostrarEditar:  true,
      mostrarCrear:   true,
      config:     {
                minimumInputLength: 0,
                placeholder: $(this).attr('placeholder'),
                allowClear: false,
                width: 'resolve',
                id: function(e) { return e.id; },
                formatNoMatches: function() { return 'Sin resultados'; },
                formatSearching: function(){ return "Buscando..."; },
                ajax: {
                    url: "api.php",
                    dataType: 'json',
                    quietMillis: 200,
                    data: function(term, page) {
                        return {
                            module: 'localidades',
                            run: 'listar_select',
                            q: term,
                            p: page
                        };
                    },
                    results: function(data, page ) {
                        //Si el ususario no esta logueado refresh
                        //if(!data.logued) location.reload();

                        //Calcula si existen mas resultados a mostrar
                        var more = (page * 40) < data.result.total;

                        //Devolvemos el valor de more para que selec2
                        //sepa que debemos cargar mas resultados.
                        return {results: data.result.items, more: more};
                    }
                },
                formatInputTooShort: function () {
                  return "";
                },
                formatResult: function(item) {
                  return item.nombre;
                },
                formatSelection: function(item) { 
                  return item.nombre;
                },
                initSelection : function (element, callback) {
                  var elementText = $(element).attr('data-init-text');
                }
              }
    },
    { 
      id:       "password",
      label:      "Contraseña",
      tipo:       "input",
      mostrarEditar:  true,
      mostrarCrear:   true
    }
  ]
};

// Ventana de login
function login(){

  //Creamos el popup
  abrir_modal('login', 500, false);

  //Mostramos la ventana
  $('#loginModal').modal({show:true});

  //Footer...
  $('#loginModal_footer').html('');

  //Titulo...
  $('#loginModal_titulo').html('<h1 class="text-center">Ingresar al Catálogo</h1>');

  //Contenido
  var contenido = $('<div class="scrollable" style="max-height: 500px; overflow-y: hidden;"></div>');

  //Agregamos contenido
  contenido.append( genera_campo({ 
      id:     "usuario",
      label:  "Usuario",
      tipo:   "input",
      mostrarEditar:  true,
      mostrarCrear:   true
    }, 'login', true));

  //Agregamos contenido
  contenido.append( genera_campo({ 
      id:     "contrasenia",
      label:  "Contraseña",
      tipo:   "input",
      mostrarEditar:  true,
      mostrarCrear:   true
    }, 'login', true));

  //Agregamos names attr...
  $('#login_contrasenia',contenido).attr('type','password');

  $("#login_usuario, #login_contrasenia",contenido).keyup(function (e) {
      if(e.keyCode == 13)
        if( ($('#login_usuario').val().trim() != '') && 
            ($('#login_contrasenia').val().trim() != '') )
              login_confirm();
  });

  //Cargando...
  $('#loginModal_container').html(contenido);

  //Agregamos scrollbars
  $('#loginModal_container .scrollable').mCustomScrollbar({theme: 'minimal-dark',scrollInertia: 100, mouseWheel:{ scrollAmount: 70 }});

  //Botones
  $('#loginModal_footer').append('<button type="button" class="btn btn-primary" style="display: block; width: 100%; margin: 10px auto;" onclick="login_confirm();">Ingresar</button>');

  //Botones
  $('#loginModal_footer').append('<button type="button" class="btn btn-warning" style="display: inline-block; width: 215px; margin: auto; margin-right: 5px; float: left;" onclick="recordar_contrasenia();">Olvide mi contraseña</button>');

  //Botones
  $('#loginModal_footer').append('<button type="button" class="btn btn-success" style="display: inline-block; width: 215px; margin: auto; margin-left: 5px; float: right;" onclick="login_crear_cuenta();">Deseo crear una Cuenta</button>');
}

//enviamos datos al API
function login_confirm(){

  //API
  var data = {};
  data['usuario'] = $('#login_usuario').val();
  data['password'] = $('#login_contrasenia').val();
  apix('auth','login',data, {
    reload: false,
    ok: function(json){
      location.reload(); 
    }
  });
}

// Recordar Cotraseña
function recordar_contrasenia(){

  //Creamos el popup
  abrir_modal('login', 650, false);

  //Mostramos la ventana
  $('#loginModal').modal({show:true});

  //Cargando...
  $('#loginModal_container').html( cargando() );

  //Titulo...
  $('#loginModal_titulo').html('<h1 class="text-center">Recordar Contraseña</h1>');

  //Contenido
  var contenido = $('<div class="scrollable" style="max-height: 500px; overflow-y: hidden;"></div>');

  //Agregamos Info
  contenido.append('<p style="margin-bottom: 25px;">Ingrese la cuenta de correo con la que se registró en el sistema y en breve le estaremos enviando los datos de acceso de su cuenta.</p>');

  //Agregamos contenido
  contenido.append( genera_campo({ 
      id:     "email",
      label:  "Email",
      tipo:   "input",
      mostrarEditar:  true,
      mostrarCrear:   true
    }, 'recordar_contrasenia', true));

  $("#recordar_contrasenia_email",contenido).keyup(function (e) {
      if(e.keyCode == 13)
        if($(this).val().trim() != '')
              recordar_contrasenia_confirm();
  });

  //Cargando...
  $('#loginModal_container').html(contenido);

  //Agregamos scrollbars
  $('#loginModal_container .scrollable').mCustomScrollbar({theme: 'minimal-dark',scrollInertia: 100, mouseWheel:{ scrollAmount: 70 }});

  //Botones
  $('#loginModal_footer').append('<button type="button" class="btn btn-primary" style="display: block; width: 100%; margin: 10px auto;" onclick="recordar_contrasenia_confirm();">Enviar Contraseña</button>');

  //Botones
  $('#loginModal_footer').append('<button type="button" data-dismiss="modal" class="btn btn-warning" style="display: block; width: 100%; margin: 10px auto;">Volver</button>');


}

//enviamos datos al API
function recordar_contrasenia_confirm(){

  //Llamada a la API
  var data = {};
  data['email'] = $('#recordar_contrasenia_email').val();

  //Titulo...
  $('#loginModal_titulo').html('<h2 class="text-center">Espere por favor...</h2>');

  //Espere por favor
  $('#loginModal_container').html(cargando());

  //Footer...
  $('#loginModal_footer').html('');

  //API
  apix('auth','remPass',data, {
    reload: false,
    ok: function(json){
      notifica( 'Le enviamos un Email!',
                'Revise su cuenta de Correo, le enviamos los datos de acceso.',
                'confirmacion','success');
      $('#loginModal').modal('toggle');
    },
    error: function(json){
      recordar_contrasenia();
    }
  });
}

//Crea una Cuenta
function login_crear_cuenta(){

  //Footer...
  $('#loginModal_footer').html('');

  //Titulo...
  $('#loginModal_titulo').html('<h1 class="text-center">Crear una Cuenta</h1>');

  //Contenido
  var contenido = $('<div class="scrollable" style="max-height: 450px; overflow-y: hidden;"></div>');


  //Agregamos todos los campos
  for(var k in login_crear_cuenta_config.campos){
    var campo = login_crear_cuenta_config.campos[k];
      //Agregamos el campo
      contenido.append( genera_campo(login_crear_cuenta_config.campos[k], 'login_crear_cuenta' ,true) );
  }

  //Agregamos evento Blur en CUIT
  $('#login_crear_cuenta_cuit',contenido).on('blur', function(){
    if( $(this).val().trim() != ''){
      $('#login_crear_cuenta_razon_social').val('cargando...').attr('disabled','disabled');

      //API
      var data = {};
      data['cuit'] = $(this).val();
      apix('clientes','info_razon_social',data, {
        reload: false,
        ok: function(json){
          $('#login_crear_cuenta_razon_social').val_(json.result);
          $('#login_crear_cuenta_razon_social').removeAttr('disabled');
        }
      });

    }
  });

  //Cargando...
  $('#loginModal_container').html(contenido);

  //Agregamos scrollbars
  $('#loginModal_container .scrollable').mCustomScrollbar({theme: 'minimal-dark',scrollInertia: 100, mouseWheel:{ scrollAmount: 70 }});

  //Botones
  $('#loginModal_footer').append('<button type="button" class="btn btn-primary" style="display: block; width: 100%; margin: 10px auto;" onclick="login_crear_cuenta_confirm();">Crear Cuenta</button>');

  //Botones
  $('#loginModal_footer').append('<button type="button" class="btn btn-warning" style="display: block; width: 100%; margin: 10px auto;" onclick="login();">Volver</button>');
}

//Crea una Cuenta
function login_crear_cuenta_confirm(){
  //Llamada a la API
  var data = {};

   //Agregamos todos los campos
  for(var k in login_crear_cuenta_config.campos){
    var campo = login_crear_cuenta_config.campos[k];
    if(campo.tipo == 'input')
      data[ campo.id ] = $('#login_crear_cuenta_'+campo.id).val();
  }

  //Localidad
  var localidad = $('#login_crear_cuenta_localidad').select2('data');
  if(localidad != null){ 
    data['localidad'] = localidad.id;
  }

  //Agregamos todos los campos
  for(var k in login_crear_cuenta_config.campos){
    var campo = login_crear_cuenta_config.campos[k];
    if(campo.type = 'input')
      if(campo.mostrarEditar)
        data[campo.id] = $('#'+login_crear_cuenta_config.modName+'_'+campo.id).val();
  }

  apix('clientes','crear_cuenta',data, {
    reload: false,
    ok: function(json){ 
      //Aviso
      notifica( "Cuenta Creada!",
            'La cuenta ha sido creada, en breve recibirá un correo con los datos de acceso.',
            'confirmacion',
            'success'
          );
      //Volvemos al login
      login();
    }
  });
}
</script>