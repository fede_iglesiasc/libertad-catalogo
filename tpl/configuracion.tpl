<script type="text/javascript">


// Genera todo el layout de Opciones
function configuracion_precios(){

  //Cargando...
  $('#cofiguracion').html( cargando() );

  //Parametros
  var data = {};

  //Llamada a la API
  apix('cuenta','info',data, {
    ok: function(json){

      var table = $('<table style="width: 100%;"></table>');
      var tr = $('<tr></tr>');
      var td = $('<td class="configuracion_contenedor"></td>');
      var div = $('<div style="max-height: 450px; height: 450px; overflow: hidden; padding: 15px; margin: 10px;"></div>');

      var div1 = $('<div style="padding: 15px; width: 280px; float: left; background: rgba(255, 255, 255, 0.5);"></div>');

      div1.append('<br><span class="configuracion_subtitulo">Calcular precio de venta</span>');

      //Agregamos P. sobre Costo
      div1.append('<div class="radio"><label><input type="radio" name="configuracion_precio_venta" value="1">Sobre el <b>Precio de Lista</b></label></div>');

      //Agregamos P. sobre Lista
      div1.append('<div class="radio"><label><input type="radio" name="configuracion_precio_venta" value="2">Sobre el <b>Precio de Costo</b></label></div>');

      $('input[type=radio][value="'+json.result.precio_venta+'"]',div1).first().attr('checked','checked');

      div1.append('<br><span class="configuracion_subtitulo">Ajustar precio de venta</span><br>');

      //Agregamos Ajuste
      div1.append( genera_campo({  
          id:         "ajuste",
          label:      "",
          tipo:       "input",
          mostrarEditar:  true,
          mostrarCrear:   true
        }, 'configuracion_precios' ,true) );


      //Valor al Ajuste
      $('#configuracion_precios_ajuste',div1).val_(json.result.ajuste);
      $('#configuracion_precios_ajuste',div1).width(180);
      $('#configuracion_precios_ajuste',div1).on('change',function(event){
        //Obtenemos el valor
        var val = $(this).val();
        //reemplazamos caracteres no numericos
        val = val.replace(/[^\d,.-]/g, '');
        //Reemplazamos la coma por el punto
        val = val.replace(',','.');
        //Parseamos a un Float de dos decimales
        val = parseFloat(val).toFixed(2)
        //Corregimos valor NAN
        if(isNaN(val)) val = '0.00';
        //Le agregamos el simbolo de porcentaje
        val = val + ' %';
        //Asignamos el valor
        $(this).val(val);
      });
      $('#configuracion_precios_ajuste',div1).change();
      div1.append('<br><span class="configuracion_subtitulo">¿Qué precio mostramos?</span>');

      //Agregamos Mostrar Precio Costo
      div1.append('<div class="checkbox"><label><input type="checkbox" id="configuracion_precios_mostrar_costo" value="0">Mostrar Precio de Costo</label></div>');
      $("#configuracion_precios_mostrar_costo",div1).attr("checked", Boolean(parseInt(json.result.mostrar_precio_costo)));

      //Agregamos Mostrar Precio Lista
      div1.append('<div class="checkbox"><label><input type="checkbox" id="configuracion_precios_mostrar_lista" value="0">Mostrar Precio de Lista</label></div>');
      $("#configuracion_precios_mostrar_lista",div1).attr("checked", Boolean(parseInt(json.result.mostrar_precio_lista)));

      //Agregamos Mostrar Precio Venta
      div1.append('<div class="checkbox"><label><input type="checkbox" id="configuracion_precios_mostrar_venta">Mostrar Precio de Venta</label></div>');
      $("#configuracion_precios_mostrar_venta",div1).attr("checked", Boolean(parseInt(json.result.mostrar_precio_venta)));

      div1.append('<button type="button" id="configuracion_precios_boton_guardar" class="btn btn-primary" style="display: block; margin: 10px auto; width: 100%;" onclick="configuracion_precios_guardar();">Guardar cambios</button>');

      $('#configuracion_precios_mostrar_costo, #configuracion_precios_mostrar_lista, #configuracion_precios_mostrar_venta',div1).on('click',function() {
            var cantidad = $('#configuracion_precios_mostrar_costo:checked, #configuracion_precios_mostrar_lista:checked, #configuracion_precios_mostrar_venta:checked').length;


            if( cantidad == 3){ 
              //Avisamos y destildamos
              notifica( 'Solo se pueden mostrar 2 precios',
                    'Para poder mostrar este precio, desmarque uno de los ya marcados.',
                    'informacion','info');
              $(this).prop('checked',false);
            }
      });

      //Agregamos evento cambio
      $('input',div1).on('change',function(){
        configuracion_precios_actualiza_vp();
      });

      div.append(div1);

      //Agregamos Vista Previa
      div.append('<div id="configuracion_precios_vista_previa" data-costo="'+json.result.vista_previa_precio_costo+'" data-lista="'+json.result.vista_previa_precio_lista+'"><span>VISTA PREVIA</span><div class="detalles-precios" style="padding: 6px; "></div><p style="color: #000000;">* Precio de lista ejemplo: $'+json.result.vista_previa_precio_lista+'</p></div>');
      
      td.append(div);


      //Acoplamos todo
      tr.append(td);
      table.append(tr);
      tr.append(configuracion_menu());
      $('#configuracion').html('');
      $('#configuracion').append(table);

      //Vista previa actualizar
      configuracion_precios_actualiza_vp();
    }
  });
}

function configuracion_menu(){
  var html = $('<td style="width: 200px;"><ul class="nav nav-pills nav-stacked"><li role="presentation" id="configuracion_menu_precios"><a href="#" onclick="cambiar_modo(\'configuracion_precios\')">Precios<span class="glyphicon glyphicon-usd pull-right"></span></i></a></li><li role="presentation" id="configuracion_menu_cuenta"><a href="#" onclick="cambiar_modo(\'configuracion_cuenta\')">Cuenta<span class="glyphicon glyphicon-user pull-right"></span></a></li></ul></td>');

  if(watable_modo == 'configuracion_precios')
    $('#configuracion_menu_precios',html).addClass('active');

  if(watable_modo == 'configuracion_cuenta')
    $('#configuracion_menu_cuenta',html).addClass('active');

  return html;
}

function configuracion_precios_guardar(){
  $('#configuracion_precios_boton_guardar').append('<i class="fa fa-cog fa-spin fa-2 fa-fw margin-bottom"></i>');
  var data = {};
  data.precio_venta = $("input[type='radio'][name='configuracion_precio_venta']:checked").val();
  data.ajuste = $('#configuracion_precios_ajuste').val();
  data.ajuste = data.ajuste.replace(/[^\d,.-]/g, '');

  //seteamos todos faslso por defecto
  data.mostrar_precio_costo = $('#configuracion_precios_mostrar_costo').prop('checked') ? 1 : 0;
  data.mostrar_precio_venta = $('#configuracion_precios_mostrar_venta').prop('checked') ? 1 : 0;
  data.mostrar_precio_lista = $('#configuracion_precios_mostrar_lista').prop('checked') ? 1 : 0;

  apix('cuenta','precios_editar',data, {
    ok: function(json){

      configuracion_precios();
    }
  });
}

function configuracion_precios_actualiza_vp(){
  var costo = parseFloat($('#configuracion_precios_vista_previa').data('costo'));
  var lista = parseFloat($('#configuracion_precios_vista_previa').data('lista'));
  var ajuste = $('#configuracion_precios_ajuste').val_();
  ajuste = ajuste.replace(/[^\d,.-]/g, '');
  var sobre_costo_lista = $('input[name=configuracion_precio_venta]:checked').val();

  if(sobre_costo_lista == 1){
    var venta = costo + (costo*(ajuste/100));
  }else{
    var venta = costo + (costo*(ajuste/100));
  }


  $('#configuracion_precios_vista_previa>div').html('');

  //Creamos el precio de Lista
  var precio_lista = $('<div><div class="detalles-label">PRECIO DE LISTA</div><div class="detalles-precios-precio">$ '+(lista*1)+'<span style="margin-left: 10px;font-size: 10px;color: white;">S/IVA</span></div><div class="detalles-precios-precio">$ '+ (lista*1.21).toFixed(2) +'<span style="margin-left: 10px;font-size: 10px;color: white;">C/IVA</span></div></div>');

  //Creamos el precio de Costo
  var precio_costo = $('<div><div class="detalles-label">PRECIO DE COSTO</div><div class="detalles-precios-precio">$ '+costo+'<span style="margin-left: 10px;font-size: 10px;color: white;">S/IVA</span></div><div class="detalles-precios-precio">$ '+(costo*1.21).toFixed(2)+'<span style="margin-left: 10px;font-size: 10px;color: white;">C/IVA</span></div></div>');

  //Creamos el precio de Venta
  var precio_venta = $('<div><div class="detalles-label">PRECIO DE VENTA</div><div class="detalles-precios-precio">$ '+venta.toFixed(2)+'<span style="margin-left: 10px;font-size: 10px;color: white;">S/IVA</span></div><div class="detalles-precios-precio">$ '+(venta*1.21).toFixed(2)+'<span style="margin-left: 10px;font-size: 10px;color: white;">C/IVA</span></div></div>');

  //Agregamos Costo
  if( $('#configuracion_precios_mostrar_costo').is(':checked') )
    $('#configuracion_precios_vista_previa>div').append(precio_costo);

  //Agregamos Lista
  if( $('#configuracion_precios_mostrar_lista').is(':checked') )
    $('#configuracion_precios_vista_previa>div').append(precio_lista);

  //Agregamos Venta
  if( $('#configuracion_precios_mostrar_venta').is(':checked') )
    $('#configuracion_precios_vista_previa>div').append(precio_venta);
}

// Genera todo el layout de Confuguracion: Cuenta
function configuracion_cuenta(){

  //Cargando...
  $('#cofiguracion').html( cargando() );

  //Parametros
  var data = {};

  //Llamada a la API
  apix('cuenta','info',data, {
    ok: function(json){

      var table = $('<table style="width: 100%;"></table>');
      var tr = $('<tr></tr>');
      var td = $('<td class="configuracion_contenedor"></td>');
      var div = $('<div style="max-height: 450px; height: 450px; overflow-y: auto; padding-right: 10px; margin-top: 10px;"></div>');
      var div1 = $('<div style="overflow: hidden; padding: 15px; margin: 10px; width: 490px; background: rgba(255, 255, 255, 0.5);"></div>');

       //Agregamos todos los campos
      for(var k in cuenta_config.campos){
        var campo = cuenta_config.campos[k];
        div1.append( genera_campo(cuenta_config.campos[k], 'configuracion_cuenta' ,true) );
      }

        //Llenamos los datos
        for(var k in cuenta_config.campos){
          var campo = cuenta_config.campos[k];
          if((campo.tipo == 'input') || (campo.tipo == 'checkbox'))
            if( !(typeof(json.result[ campo.id ]) == "undefined") && !(json.result[ campo.id ] === null) ){
              $('#configuracion_cuenta_'+campo.id,div1).val_(json.result[ campo.id ]);
            }
        }

        

      //Creamos el campo especial para popular el S2 de Fabricantes / Codigo
      $('#configuracion_cuenta_localidad',div1).val_({ id: json.result.localidad, nombre: json.result.localidad_label });

      div1.append('<button type="button" class="btn btn-primary" style="display: block; margin: 10px auto; width: 100%;" onclick="configuracion_cuenta_edita_confirm();">Guardar cambios</button>');

      // Dejamos los inputs de un ancho de 400
      $('input',div1).width(460);
      $('div.select2-container',div1).css('display','block').width(460);

      //Acoplamos todo
      div.append(div1);
      td.append(div);


      tr.append(td);
      //Agregamos scrollbars
      $('.scrollable',tr).mCustomScrollbar({theme: 'minimal-dark',scrollInertia: 100, mouseWheel:{ scrollAmount: 70 }});
      table.append(tr);
      tr.append(configuracion_menu());
      $('#configuracion').html('');
      $('#configuracion').append(table);

      //Vista previa actualizar
      configuracion_precios_actualiza_vp();
    }
  });
}

function configuracion_cuenta_edita_confirm(){
  //Data
  var data = {};

  //Agregamos todos los campos
  for(var k in cuenta_config.campos){
    var campo = cuenta_config.campos[k];
    if(campo.mostrarEditar && (campo.tipo == 'input'))
      data[campo.id] = $('#configuracion_cuenta_'+campo.id).val_();
  }

  {if="$GLOBALS['session']->getData('rol') == 2"}
  //Campo especial Codigo / Fabricante
  var localidad = $('#configuracion_cuenta_localidad').select2('data');
  if(localidad != null) {
    data['localidad'] = localidad.id;
  }
  {/if}

  //Llamada a la API
  {if="$GLOBALS['session']->getData('rol') != 2"}
  var modulo = 'cuenta';
  var accion = 'editar';
  {/if}
  {if="$GLOBALS['session']->getData('rol') == 2"}
  var modulo = 'clientes';
  var accion = 'editar_cuenta';
  {/if}
  apix(modulo,accion,data,{
    ok: function(json){
      notifica( 'Cambios Guardados',
            'La Cuenta se editó con éxito',
            'confirmacion','success');
      $('#'+cuenta_config.modName+'Modal').modal('hide');
    }
  });
}
</script>

