<script>

/* LOAD SESSION VALUES */
function createBreadCrumb(){

  //Llamada a la API
  apix('busqueda','advSearch_getSessionVars',{}, {
    ok: function(json){

        //Guardamos las variables
        var vars = json.result.vars;
        
        //Quitamos anteriores
        $('div.tags ul li').remove();

        ///////////////////////////////////////////////////////////////////////
        //MODO CCONFIGURACION PRECIOS
        ///////////////////////////////////////////////////////////////////////
        if(json.result.modo == 'configuracion_precios'){
          $('div.tags ul').append('<li><a><b>CONFIGURACION: </b>Precios</a></li>');
          $('div.tags').css('display','block');
          $('div.tags').css('visibility','visible');
          return;
        }

        ///////////////////////////////////////////////////////////////////////
        //MODO MENSAJES
        ///////////////////////////////////////////////////////////////////////
        if(json.result.modo == 'mensajes'){
          $('div.tags ul').append('<li><a><b>MENSAJES</b></a></li>');
          $('div.tags').css('display','block');
          $('div.tags').css('visibility','visible');
          return;
        }

        ///////////////////////////////////////////////////////////////////////
        //MODO CCONFIGURACION CUENTA
        ///////////////////////////////////////////////////////////////////////
        if(json.result.modo == 'configuracion_cuenta'){
          $('div.tags ul').append('<li><a><b>CONFIGURACION: </b>Cuenta</a></li>');
          $('div.tags').css('display','block');
          $('div.tags').css('visibility','visible');
          return;
        }

        ///////////////////////////////////////////////////////////////////////
        //MODO CARRO
        ///////////////////////////////////////////////////////////////////////
        if(json.result.modo == 'carro'){
          $('div.tags ul').append('<li><a><b>MI PEDIDO:</b> '+ json.result.nombre +'</a></li>');
          $('div.tags').css('display','block');
          $('div.tags').css('visibility','visible');
          return;
        }

        ///////////////////////////////////////////////////////////////////////
        //MODO CARRO
        ///////////////////////////////////////////////////////////////////////
        if(json.result.modo == 'listas'){
          $('div.tags ul').append('<li><a><b>ÚLTIMAS MODIFICACIONES DE PRECIO POR FABRICANTE</b></a></li>');
          $('div.tags').css('display','block');
          $('div.tags').css('visibility','visible');
          return;
        }

        //Rubro
        if( (vars.rubro.id != -1) && (vars.quicksearch_term.id == -1)) $('div.tags ul').append('<li><a data-sessvar="advSearch_rubro" data-toggle="tooltip" title="Click para quitar">'+vars.rubro.nom+'</a></li>');

        //Fabricante
        if(vars.fabricante.id != -1)  $('div.tags ul').append('<li><a data-sessvar="advSearch_fabricante" data-toggle="tooltip" title="Click para quitar">'+vars.fabricante.nom+'</a></li>');

        //Vehiculo
        if(vars.vehiculo.id != -1) $('div.tags ul').append('<li><a data-sessvar="advSearch_vehiculo" data-toggle="tooltip" title="Click para quitar">'+vars.vehiculo.nom+'</a></li>');

        //Prefijo
        if( (vars.prefijo.id != -1) || (vars.codigo.id != -1) || (vars.sufijo.id != -1) ){
          var label = 'Codigo: ';
          label += (vars.prefijo.id != -1)? vars.prefijo.id : '*';
          label += ' - ';
          label += (vars.codigo.id != -1)? vars.codigo.id : '*';
          label += ' - ';
          label += (vars.sufijo.id != -1)? vars.sufijo.id : '*';

          $('div.tags ul').append('<li><a data-sessvar="advSearch_codigo" data-toggle="tooltip" title="Click para quitar">'+label+'</a></li>');
        }

        //Dimensiones
        if(json.result.vars.dimensiones != undefined)
          for(var i in json.result.vars.dimensiones)
            if(json.result.vars.dimensiones[i].value.id != -1)
              $('div.tags ul').append('<li><a data-sessvar="advSearch_dimension_'+json.result.vars.dimensiones[i].id+'" data-toggle="tooltip" title="Click para quitar">'+json.result.vars.dimensiones[i].label+':  '+json.result.vars.dimensiones[i].value.nom+'</a></li>');

        //Busqueda general
        if(vars.quicksearch_term.id != -1){
          $('div.tags ul').append('<li><a data-sessvar="quicksearch_term" data-toggle="tooltip" title="Click para quitar">Últimos 20 resultados coincidentes con <b>"'+vars.quicksearch_term.id+'"</b></a></li>');
        }else{
          $('#quicksearch').select2('data', null);
        }

        //Si no estamos teniendo terminos de busqueda
        //Significa que estamos mostrando las incorporaciones
        if( (vars.prefijo.id == -1) && (vars.codigo.id == -1) && (vars.sufijo.id == -1) && 
            (vars.rubro.id == -1) && (vars.fabricante.id == -1) && (vars.vehiculo.id == -1) && 
            (vars.quicksearch_term.id == -1)){

          var bandera_dim = false;
          if(json.result.vars.dimensiones != undefined)
            for(var i in json.result.vars.dimensiones)
              if(json.result.vars.dimensiones[i].value.id != -1)
                bandera_dim = true;

          if(!bandera_dim) $('div.tags ul').append('<li><a><b>Incorporaciones</b> de Artículos realizadas en los <b>últimos 30 días</b></a></li>');
        }

      //Agregamos los botones para quitar los items de busqueda
      $('div.tags ul li a').each(function(){

        if( typeof $(this).data('sessvar') !== 'undefined' ){
          //Agregamos eventos
          $(this).on('click',function(){

            //Nombre
            var sessvarname = $(this).data('sessvar');

            $(this).parent().remove();

            //Si es el codigo
            if(sessvarname == 'advSearch_codigo'){
              apix('busqueda','advSearch_setSessionVar', {variable: 'advSearch_prefijo', value: '-1'}, {
                ok: function(json){
                  apix('busqueda','advSearch_setSessionVar', {variable: 'advSearch_codigo', value: '-1'}, {
                    ok: function(json){
                      apix('busqueda','advSearch_setSessionVar', {variable: 'advSearch_sufijo', value: '-1'}, {
                        ok: function(json){
                          articulosUpdate();
                        }
                      });
                    }
                  });
                }
              });

            //Para cualquier
            //otra variable
            }else{
              apix('busqueda','advSearch_setSessionVar',{variable: sessvarname, value: '-1'}, {
                ok: function(json){
                  articulosUpdate();
                }
              });
            }
          });

          //Agregamos tooltips
          $('div.tags').tooltip({selector: '[data-toggle="tooltip"]', placement: 'bottom', animation: false});
        }
      });

      $('div.tags').css('visibility','visible');
      $('div.tags').css('display','block');
    }
  });
}


</script>