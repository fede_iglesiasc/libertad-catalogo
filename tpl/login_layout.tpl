<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Distribuidora Libertad</title>

        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

        <!-- Favicon -->
        <link href="img/favicon.ico" rel="shortcut icon">

        <!-- Custom fonts -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,100" rel="stylesheet">
        <link href="css/social-font.css" rel="stylesheet">

        <!-- Bootstrap stylesheets -->
        <link href="css/index.php" rel="stylesheet">

        <!-- Template stylesheets -->
        <link href="css/shader.css" rel="stylesheet">

        <!-- Scripts -->
        <script src="./js/index.php"></script>
        <!---->
        
    </head>

    <body>

        <div id="container" class="container">
          <div id="output" class="container"></div>
        </div>
        <div id="vignette" class="background-vignette"></div>
        <div id="noise" class="background-noise"></div>


        <!-- Main content -->
        <div class="content" style="font: 300 20px/1.4 Roboto, 'Open Sans', Helvetica, Arial, sans-serif; text-shadow: 0 1px 2px rgba(0, 0, 0, 0.66); ">
            <div id="contenido">
                <div style="width: 1000px; position: relative; left: 50%; margin-left: -500px;">
                    <div style="width: 750px; background: url('./img/mdp.png') 0px 0px; float: left; height: 118px; margin-top: 1px;">
                        <img src="./img/logo_grande.png" width="320" style="float: left; margin-top: 20px;"/>
                    </div>
                    <div style="float: left; text-align: right; width: 250px;">
                        <input type="input" class="form-control input-sm" id="login_usuario" style="display: inline; width: 170px; margin-top: 7px; height: 28px;" placeholder="Email / Cliente">
                        <input type="password" class="form-control input-sm" id="login_contrasenia" style="display: inline; width: 170px; margin-top: 3px; height: 28px;" placeholder="Contraseña">
                        <!--<button class="btn btn-info btn-sm" onclick="contacto();" style="margin-top: 5px; width: 83px;">Contacto</button>
                        <button class="btn btn-info btn-sm" onclick="login_confirm();" style="margin-top: 5px; width: 82px;">Ingresar</button>-->
                        <button class="btn btn-info btn-sm" onclick="login_confirm();" style="margin-top: 3px; width: 170px;">Ingresar</button>
                        <button class="btn btn-link" onclick="recordar_contrasenia();" style="width: 170px; text-align: right; font-size: 10px !important; padding-right: 0px; margin-top: -5px; color: #D0D0D0;">OLVIDE LA CONTRASEÑA</button>
                    </div>
                </div>
            </div>
        </div>



        <div class="row" style="position: fixed; width: 100%; height: 50%; margin-top: 50px; text-align: center; color: #fff;">
            <h1 class="header col-sm-8 col-sm-offset-2" style="margin-bottom: 5px; font-size: 30px; line-height: 1.2; font-weight: 300;">Estamos trabajando para brindarle un mejor servicio.</h1>
        </div>

359*
        <div id="footer_login">
            TEL: (0223) 474-1222 (ROTATIVAS) | FAX: (0223) 475-7170 / 0800-333-6590 | info@distribuidoralibertad.com | Chile 2019, Mar del Plata, Bs. As. Argentina
        </div>

{$login}


        <script src="./js/flat-surface-shader.js"></script>

    </body>
</html>
