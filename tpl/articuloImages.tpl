<!-- SEARCH -->
<div id="articuloImagesModal" class="modal fade" role="dialog" aria-hidden="true" style="width: 500px;">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h4 class="text-center">Subir Imagen</h4>
  </div>
  <div class="modal-body">
    <div class="row clearfix">
	    <div class="col-sm-12" id="articuloImages_container">
	    	<!-- Plugin to upload pictures and edit Pic form-->
	    </div>
    </div>
  </div>
  <div class="modal-footer" style="border-top: 0px; margin-top: 0px;" id="articuloImages_footer">
        <!-- Action Buttons -->
  </div>
</div>


<script type="text/javascript">

//FUNCTION TO DELETE A PICTURE
function delete_pic(pic,id){

    if(confirm('¿Realmente desea eliminar la Imagen?')){

      //Hide gallery tooltip
      $('#'+id).tooltip('hide');
      //Rset flag to reload content
      $('#'+id).data('loaded',"1");

      //Data array
      var data = {};
      data['module'] = 'imagenes';
      data['run'] = 'delete';
      data['pic'] = pic;

      //Make ajax call to api
      $.ajax({
        type: "GET",
        url: "api.php",
        data: data,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        error: function (json) {
        },
        success: function (json) {
            //If session expire reload page...
            if(!json.logued) location.reload();

            //If result is ok
            if(json.status == '1'){
              //If status is ok, reload the tooltip generated previously
              articulosUpdate();

            //If result is error..
            }else{
              showErrorModal('Error al eliminar Imagen.');
            }
        }
      });
    }
  }

//FUNCTION TO UPLOAD NEW PICTURE
var jcrop_api;
function upload_pic(codigo){
    //Check if this article has temp images uploaded
    $.ajax({
        type: 'HEAD',
        url: './upload_tmp/'+codigo+'.jpg',
        cache: false,
        success: function(){
            createEditForm(codigo);
        },
        error: function(jqXHR, textStatus, errorThrown) {
            startUploadPlugin(codigo);
        }
    });
}

//CANCEL THE UPLOAD PROCESS
function upload_pic_cancel(){
	//Hide upload_pic plugin
	$('#drop').show();
	$('#upload_pic ul').html('').hide();
}

//DISCARD TEMP IMAGE
function upload_pic_discard(codigo){
  //Data array
  var data = {};
  data['module'] = 'imagenes';
  data['run'] = 'delete_tmp';
  data['codigo'] = codigo;

  //Make ajax call to api
  $.ajax({
    type: "GET",
    url: "api.php",
    data: data,
    contentType: "application/json; charset=utf-8",
    dataType: "json",
    error: function (json) {
    },
    success: function (json) {
        //If session expire reload page...
        if(!json.logued) location.reload();

        //If result is ok
        if(json.status == '1'){
        //If result is error..
        }else{
          showErrorModal('Error al eliminar Imagen.');
        }
    }
  }); 
}


//ON MODAL CLOSE
$('#articuloImagesModal').on('hidden.bs.modal', function () {
    //Destroy Jcrop if exist
    if(jcrop_api != undefined)
        jcrop_api.destroy();

    //Destroy Fileupload Plugin
    $('#upload_pic').fileupload('destroy');

    //Clear HTML
    $('#articuloImages_container').html('');
    $('#articuloImages_footer').html('');
});

//COMMIT UPLOAD IMG
function commit_upload(codigo){

  //Data array
  var data = {};
  data['module'] = 'imagenes';
  data['run'] = 'commit_upload';
  data['codigo'] = codigo;

  //Add crop if is defined
  var final_data = $.extend(data, coords);

  //Make ajax call to api
  $.ajax({
    type: "GET",
    url: "api.php",
    data: final_data,
    contentType: "application/json; charset=utf-8",
    dataType: "json",
    error: function (json) {
    },
    success: function (json) {
        //If session expire reload page...
        if(!json.logued) location.reload();

        //If result is ok
        if(json.status == '1'){
            //Upload articles
            watable_volver = true; 
            articulosUpdate();

        //If result is error.. 
        }else{
          showErrorModal('Error al subir la imagen.');
        }
    }
  });  
}


//FUNCTION TO CREATE EDIT IMAGE FORM
function createEditForm(codigo){

    //Delete content
    $('#articuloImages_container').html('');
    $('#articuloImages_footer').html('');

    //Append pendant image
    $('#articuloImages_container').append('<img id="uploaded_tmp_img" src="./upload_tmp/'+codigo+'.jpg?'+Math.random()+'" style="width: 100%; display: none !important;"/>');

    //Boton Insertar imagen
    $('#articuloImages_footer').append('<button type="button" id="button_confirm_upload_pic" data-dismiss="modal" class="btn btn-primary" style="display: block; width: 100%; margin: 10px auto;" onclick="commit_upload(\''+codigo+'\')">Insertar imágen</button>');

    //Boton descartar imagen
    $('#articuloImages_footer').append('<button type="button" data-dismiss="modal" class="btn btn-default" style="display: block; width: 100%; margin: 10px auto;" onclick="upload_pic_discard(\''+codigo+'\');">Descartar Imágen</button>');

    $("<img/>") // Make in memory copy of image to avoid css issues
    .attr("src", $('#uploaded_tmp_img').attr("src"))
    .load(function() {

      //Load jcrop
      $('#uploaded_tmp_img').Jcrop({
              boxWidth: 460,
              boxHeight: 460,
              trueSize: [this.width,this.height],
              onChange:   setCoords,
              onSelect:   setCoords
       },function(){
          jcrop_api = this;
      });

    });



    $('#articuloImagesModal').modal({show:true});
}


/* INITIALIZE */
$( document ).ready(function() {


});


//Function to set Crop area
var coords = {};
function setCoords(c){
  coords = { "x": c.x, "y": c.y, "x2": c.x2, "y2": c.y2, "h": c.h, "w": c.w };
}

function startUploadPlugin(codigo){

    //Append Upload form
    $('#articuloImages_container').append('<form id="upload_pic" method="post" action="api.php?module=imagenes&run=upload&codigo='+codigo+'" enctype="multipart/form-data"><div id="drop">Arrastrar Aquí</br><a onclick="$(this).parent().find(\'input\').click();">Buscar</a><input type="file" name="upl" /></div><ul style="display: none;"></ul></form>');

     //Append Cancel Button
     $('#articuloImages_footer').append('<button type="button" data-dismiss="modal" class="btn btn-default" style="display: block; width: 100%; margin: 10px auto;">Cancelar</button>');

     //Show Modal
    $('#articuloImagesModal').modal({show:true});


    var ul = $('#upload_pic ul');


    // Initialize the jQuery File Upload plugin
    $('#upload_pic').fileupload({

        // This element will accept file drag/drop uploading
        dropZone: $('#drop'),

        // This function is called when a file is added to the queue;
        // either via the browse button, or via drag/drop:
        add: function (e, data) {
        	$('#drop').hide();
        	ul.show();

            var tpl = $('<li class="working"><input type="text" value="0" data-width="48" data-height="48"'+
                ' data-fgColor="#0788a5" data-readOnly="1" data-bgColor="#3e4043" /><p></p><span></span></li>');

            // Append the file name and file size
            tpl.find('p').text(data.files[0].name)
                         .append('<i>' + formatFileSize(data.files[0].size) + '</i>');

            // Add the HTML to the UL element
            data.context = tpl.appendTo(ul);

            // Initialize the knob plugin
            tpl.find('input').knob();

            // Listen for clicks on the cancel icon
            tpl.find('span').click(function(){

                if(tpl.hasClass('working')){
                    jqXHR.abort();
                }

                tpl.fadeOut(function(){
                    tpl.remove();
                });

            });

            // Automatically upload the file once it is added to the queue
            var jqXHR = data.submit();
        },

        progress: function(e, data){

            // Calculate the completion percentage of the upload
            var progress = parseInt(data.loaded / data.total * 100, 10);

            // Update the hidden input field and trigger a change
            // so that the jQuery knob plugin knows to update the dial
            data.context.find('input').val(progress).change();

            if(progress == 100){
                data.context.removeClass('working');
            }
        },

        fail:function(e, data){
            // Something has gone wrong!
            data.context.addClass('error');
        },

        done: function (e, data) {

            //Get filename
            var result = JSON.parse(data.result);

            //Explode to get filename withouth extension
            var codigo = result.file.split('.');

            //Create edit form
            createEditForm(codigo[0]);
        }

    });


	// Prevent the default action when a file is dropped on the window
	$(document).on('drop dragover', function (e) {
	    e.preventDefault();
	});

    $('#articuloImagesModal').modal({show:true});

    // Helper function that formats the file sizes
    function formatFileSize(bytes) {
        if (typeof bytes !== 'number') {
            return '';
        }

        if (bytes >= 1000000000) {
            return (bytes / 1000000000).toFixed(2) + ' GB';
        }

        if (bytes >= 1000000) {
            return (bytes / 1000000).toFixed(2) + ' MB';
        }

        return (bytes / 1000).toFixed(2) + ' KB';
    }

}

//Importador de imágenes
var importadorImagenes_opened = 0;
function importadorImagenes_open(){

  //Creamos el popup
  abrir_modal('importadorImagenes', 450, false);

  //Titulo...
  $('#importadorImagenesModal_titulo').html('Importar Precios');

  //Cargando...
  $('#importadorImagenesModal_container').html('Para importar las imágenes en Batch seleccione un ZIP. </br></br>El nombre de cada imágen deberá tener el siguiente formato: </br></br> <b>prefijo</b>-<b>codigo</b>-<b>sufijo</b>-<b>cantidad</b></br></br>Las extensiones pueden ser: <b>jpg png gif</b><input type="file" id="importadorImagenes_input" style="display:none;" accept=".zip"/>');

  //Botones
  $('#importadorImagenesModal_footer').append('<button type="button" class="btn btn-primary" style="display: block; width: 100%; margin: 10px auto;" onclick="$(\'#importadorImagenes_input\').click();">Seleccionar Archivo</button><button type="button" data-dismiss="modal" class="btn btn-warning" style="display: block; width: 100%; margin: 10px auto;">Cancelar Importación</button>');

  //Mostramos la ventana
  $('#importadorImagenesModal').modal({show:true});

  //Al seleccionar...
  $('#importadorImagenes_input').on('change', function(){

    if(!importadorImagenes_opened && ($('#importadorImagenes_input').val() != '')){
      
      //Abrimos el popup
      importadorImagenes_opened = 1;

      //Recolectamos las variables
      var file = $('#importadorImagenes_input').prop('files')[0];

      //Obtenemos la extension del archivo
      var ext = file.name.split('.')[file.name.split('.').length - 1].toLowerCase();

      //Si no es csv: ERROR
      if( ext != 'zip' ){
        alert('Solo se aceptan archivos JPG | GIF | PNG .');
        $('#importadorImagenes_input').val(''); 
        importadorImagenes_opened = 0;
      }else{

        //Reseteamos el campo
        $('#importadorImagenes_input').val(''); 
        importadorImagenes_opened = 0;

        //Datos del form
        var data = new FormData();
        data.append('module', 'imagenes');
        data.append('run', 'importadorImagenes_confirm');
        data.append('file', file);

        //Indicamos al usuario que el sistema ahora va a estar ocupado haciendo el backup
        $('#importadorImagenesModal_titulo').html('Importando imágenes...');
        $('#importadorImagenesModal_container').html('<div style="display: block; text-align: center;"><br/><img src="./img/procesando.gif" width="30" height="30"></div>');
        $('#importadorImagenesModal_footer').html('');
  
        //Make ajax call to api
        $.ajax({
          type: "POST",
          url: "api.php",
          data: data,
          contentType: false,
          processData: false,
          dataType: 'json',
          error: function (json) {
          },
          success: function (json) {
            //If session expire reload page...
            if(!json.logued) location.reload();

              //If result is ok
              if(json.status == '1'){
                //Actualizamos lista de art.
                articulosUpdate();

                //Cambiamos titulo
                $('#importadorImagenesModal_titulo').html('Importación finalizada');

                //Tendriamos que mostrar un reporte
                var container = $('<div/>');
                if(typeof json.result.importadas != 'undefined')
                  container.append('<span>Imágenes importadas: <b>'+json.result.importadas.length+'</b></span></br>');

                if(typeof json.result.inexistentes != 'undefined')
                  container.append('<span>Artículos que no figuran en la DB: <b>'+json.result.inexistentes.length+'</b></span></br>');

                if(typeof json.result.no_soportado != 'undefined')
                  container.append('<span>Archivos no soportados: <b>'+json.result.no_soportado.length+'</b></span></br>');
                
                $('#importadorImagenesModal_container').html(container);                

                //Append Cancel Button
                $('#importadorImagenesModal_footer').append('<button type="button" data-dismiss="modal" class="btn btn-warning" style="display: block; width: 100%; margin: 10px auto;">Cerrar</button>');
              }else{
                //Mostramos errores
                $('#importadorImagenesModal_container').html('<button type="button" data-dismiss="modal" class="btn btn-warning" style="display: block; width: 100%; margin: 10px auto;">Cancelar Importación</button>');
                for(k in json.errors)
                  $('#importadorImagenesModal_container').append(json.errors[k]+'</br>');
                $('#importadorImagenesModal_titulo').html('Error');
              }
          }
        });
      }
    }
  });
}

//Cerramos la ventana modal
function importadorImagenesModal_onClose(){
  importadorImagenes_opened = 0;
}

</script>

<!-- jQuery File Upload Dependencies -->
<script src="./js/jquery.knob.js"></script>
<script src="./js/jquery.ui.widget.js"></script>
<script src="./js/jquery.iframe-transport.js"></script>
<script src="./js/jquery.fileupload.js"></script>
<script src="./js/jquery.Jcrop.min.js"></script>
<link rel="stylesheet" href="css/jquery.Jcrop.min.css">
