<?php

ini_set('display_errors', 1); 
error_reporting(E_ALL);
ini_set('display_errors', '1');

//Config
include_once('mysql.php');

ini_set('memory_limit', '1024M');

set_time_limit(0);

//Obtenemos la session
try{ 
  

  //Creamos o recuperamos la session
  $GLOBALS['session'] = new Session();

	//Si no tiene definido un usuario entonces es anonimo
	if( $GLOBALS['session']->getData('usuario') ){

		$modo = 'listas';
		if(isset($_GET["modo"]))
			$modo = $_GET["modo"];


		if($modo == 'listas'){

			//Extension
			$extension = 'xls';
			if(isset($_GET["extension"]) && ($_GET["extension"] == 'pdf'))
				$extension = 'pdf';

			if($extension == 'xls'){

				require_once './classes/PHPExcel.php';

				//Si no hay seleccion.. chau
				if(!isset($_GET['seleccionados'])) exit;

				//Lista completa
				if($_GET['seleccionados'] == 'todos'){

					//Traemos todos los articulos
					$stmt = $GLOBALS['conf']['pdo']->prepare(" 	SELECT 	prefijo, 
																		codigo, 
																		sufijo,
																		(SELECT nombre FROM catalogo_marcas WHERE id = t.id_marca) as fabricante,
																		CONCAT( 
																			IF( (SELECT nombre_singular FROM catalogo_rubros WHERE id = t.id_rubro) <> '',
																				CONCAT(
																					(SELECT nombre_singular FROM catalogo_rubros WHERE id = t.id_rubro), 
																					' '
																				),
																				''
																			),

																			IF( (SELECT descripcion FROM catalogo_articulos WHERE id = t.id) <> '', 
																				(SELECT descripcion FROM catalogo_articulos WHERE id = t.id),
																				''
																			),

																			IF( (SELECT nombre FROM catalogo_marcas WHERE id = t.id_marca) <> '',
																				CONCAT(
																					' ',
																					(SELECT nombre FROM catalogo_marcas WHERE id = t.id_marca) 
																				),
																				''
																			)

																			

														 				) as descripcion,
																		REPLACE(FORMAT(precio, 2), ',', '') as precio

																FROM 	catalogo_articulos as t
																WHERE 	precio > 0 AND	habilitado = 1");
					$stmt->execute();
					$articulos = $stmt->fetchAll(PDO::FETCH_NUM);

			        //Ordenamos naturalmente
					usort($articulos, function($a, $b) {
						if($a[0] == $b[0]) 
							return strnatcmp($a[1], $b[1]);
						return strnatcmp($a[0], $b[0]);
					});

					//Agregamos encabezados
					array_unshift( $articulos, array('Prefijo','Codigo','Sufijo','Fabricante','Descripcion','Precio Lista') );

					//Creamos el excel y agregamos los datos
					$objPHPExcel = new PHPExcel();
					$objPHPExcel->getActiveSheet()->fromArray($articulos, NULL, 'A1');
					$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

					header('Content-Type: application/vnd.ms-excel');
					header('Content-Disposition: attachment;filename="Lista Completa.xls"');
					header('Cache-Control: max-age=0');
					$objWriter->save('php://output');
					exit;
				}


				//Convertimos de JSON a Array
				$seleccionados = json_decode($_GET['seleccionados']);

				//Validaciones
				if(!$seleccionados) exit;
				if(!is_array($seleccionados)) exit;


				//Validaciones
				foreach ($seleccionados as $k=>$v)
					if( !((string)(int)$v[0] == $v[0]) ) exit;
				
				$sql_condicional = array();
				foreach ($seleccionados as $k=>$v)
					$sql_condicional[] = '(id_marca = '.$v[0].' AND precio_update = "'.$v[1].'")';

				$sql = "SELECT 	id_marca, 
								precio_update,
								DATE_FORMAT(precio_update,'%d/%m/%Y') as fecha,
								(SELECT nombre FROM catalogo_marcas WHERE id = id_marca) as fabricante,

								IF(	(SELECT COUNT(*) FROM aumentos WHERE fabricante = t.id_marca AND fecha = precio_update) = 1,
									(SELECT descripcion FROM aumentos WHERE fabricante = t.id_marca AND fecha = precio_update),
									''
								) as descripcion,
								IF(	(SELECT COUNT(*) FROM aumentos WHERE fabricante = t.id_marca AND fecha = precio_update AND descarga_por_rubro = 1) = 1,
									1,
									0
								) as descarga_por_rubro

						FROM 	(	SELECT DISTINCT 	id_marca, 
														precio_update 
									FROM 				catalogo_articulos 
									WHERE 				".implode(' OR ', $sql_condicional).") as t";

				
				$stmt = $GLOBALS['conf']['pdo']->prepare($sql);
		        $stmt->execute();
		        $aumentos = $stmt->fetchAll(PDO::FETCH_ASSOC);


				$articulos = array();
				foreach ($aumentos as $k=>$s){
					$sql = "	SELECT 	prefijo, codigo, sufijo,
										(SELECT nombre FROM catalogo_marcas WHERE id = t.id_marca) as fabricante,
										CONCAT( 
												IF( (SELECT nombre_singular FROM catalogo_rubros WHERE id = t.id_rubro) <> '',
													CONCAT(
														(SELECT nombre_singular FROM catalogo_rubros WHERE id = t.id_rubro), 
														' '
													),
													''
												),

												IF( (SELECT descripcion FROM catalogo_articulos WHERE id = t.id) <> '', 
													(SELECT descripcion FROM catalogo_articulos WHERE id = t.id),
													''
												),

												IF( (SELECT nombre FROM catalogo_marcas WHERE id = t.id_marca) <> '',
													CONCAT(
														' ',
														(SELECT nombre FROM catalogo_marcas WHERE id = t.id_marca) 
													),
													''
												)
							 			) as descripcion,
										precio,
										(IF(precio_update = '".$s['precio_update']."',precio_porcentaje,'')) as porcentaje
								FROM 	catalogo_articulos as t
								WHERE 	(habilitado = 1) AND (id_marca = ".$s['id_marca'].")";

					if( (int)$s['descarga_por_rubro'] == 1 )
						$sql = "	SELECT 	prefijo, codigo, sufijo,
											(SELECT nombre FROM catalogo_marcas WHERE id = t.id_marca) as fabricante,
											CONCAT( 
													IF( (SELECT nombre_singular FROM catalogo_rubros WHERE id = t.id_rubro) <> '',
														CONCAT(
															(SELECT nombre_singular FROM catalogo_rubros WHERE id = t.id_rubro), 
															' '
														),
														''
													),

													IF( (SELECT descripcion FROM catalogo_articulos WHERE id = t.id) <> '', 
														(SELECT descripcion FROM catalogo_articulos WHERE id = t.id),
														''
													),

													IF( (SELECT nombre FROM catalogo_marcas WHERE id = t.id_marca) <> '',
														CONCAT(
															' ',
															(SELECT nombre FROM catalogo_marcas WHERE id = t.id_marca) 
														),
														''
													)
								 			) as descripcion,
											precio,
											(IF(precio_update = '".$s['precio_update']."',precio_porcentaje,'')) as porcentaje
									FROM 	catalogo_articulos  as t
									WHERE 	id_marca = ".$s['id_marca']."
											AND (habilitado = 1)
											AND id_rubro IN (
													SELECT 	id_rubro 
													FROM 	catalogo_articulos 
													WHERE 	id_marca = ".$s['id_marca']." AND
															precio_update = '".$s['precio_update']."' AND 
															id_rubro IS NOT NULL)";


					//Traemos todos los articulos
					$stmt = $GLOBALS['conf']['pdo']->prepare($sql);
					$stmt->execute();
					$articulos_tmp = $stmt->fetchAll(PDO::FETCH_NUM);

					//Ordenamos naturalmente
					usort($articulos_tmp, function($a, $b) {
						if($a[0] == $b[0]) 
							return strnatcmp($a[1], $b[1]);
						return strnatcmp($a[0], $b[0]);
					});


					//Agregamos encabezados
					array_unshift( $articulos_tmp, array('Prefijo','Codigo','Sufijo','Fabricante','Descripción','Precio','Porcentaje') );
					$titulo = $s['fabricante'] . ' - ' . $s['fecha'];
					
					
					if(trim($s['descripcion']) != '')
						$titulo .= ' - ' . $s['descripcion'];

					array_unshift( $articulos_tmp, array($titulo));

					if($k>0){
						array_unshift($articulos_tmp, array(''));
						array_unshift($articulos_tmp, array(''));
						array_unshift($articulos_tmp, array(''));
					}

					$articulos = array_merge($articulos, $articulos_tmp);
				}

				require_once './classes/PHPExcel.php';
				$objPHPExcel = new PHPExcel();

				//Agergamos los datos al objeto PHPExcel
				$objPHPExcel->getActiveSheet()->fromArray($articulos, NULL, 'A1');

				$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

				//echo "hola nuevo 8";
				header('Content-Type: application/vnd.ms-excel');
				header('Content-Disposition: attachment;filename="lisssta.xls"');
				header('Cache-Control: max-age=0');
				$objWriter->save('php://output');

				//Agregamos encabezados
				array_unshift( $articulos, array('Prefijo','Codigo','Sufijo','Precio','Porcentaje') );

				require_once './classes/PHPExcel.php';
				$objPHPExcel = new PHPExcel();

				//Agergamos los datos al objeto PHPExcel
				$objPHPExcel->getActiveSheet()->fromArray($articulos, NULL, 'A1');

				$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

				header('Content-Type: application/vnd.ms-excel');
				header('Content-Disposition: attachment;filename="Distribuidora Libertad (descargada el '.date("d-m-Y").').xls"');
				header('Cache-Control: max-age=0');
				$objWriter->save('php://output');
			}


			if($extension == 'pdf'){

				//Incluimos la clase
				include_once('./classes/PDFMerger/PDFMerger.php');

				//Si no hay seleccion.. chau
				if(!isset($_GET['seleccionados'])) exit;

				//Lista completa
				if($_GET['seleccionados'] == 'todos'){
					$pdf = new PDFMerger;
					$pdf->addPDF('./listas/completa.pdf', 'all');
					$pdf->merge('download', 'lista.pdf');
					exit;
				}

				//Convertimos de JSON a Array
				$seleccionados = json_decode($_GET['seleccionados']);

				//Si no es Array... chau
				if(!is_array($seleccionados)) exit;

				//Si alguno no existe.. chau
				foreach ($seleccionados as $k=>$s)
					if(!file_exists('./listas/'.$s.'.pdf'))
						exit;
				
				//Creamos el PDF
				$pdf = new PDFMerger;

				//Agregamos todas las listas
				foreach ($seleccionados as $k=>$s)
					$pdf->addPDF('./listas/'.$s.'.pdf', 'all');

				//Enviamos el archivo al navegador
				$pdf->merge('download', 'Distribuidora Libertad (descargada el '.date("d-m-Y").').pdf');

			}

		}

		if($modo == 'pedido'){
	        //Incluimos el generador del PDF
	        include("./classes/mpdf57/mpdf.php");

	        $usuario = $GLOBALS['session']->getData('usuario');

            //Obtenemos el pedido pendiente
	        $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT * FROM pedidos WHERE (cliente = '".$usuario."') AND (fecha_creacion IS NULL)");
	        $stmt->execute();
	        $pedido = $stmt->fetchAll(PDO::FETCH_ASSOC);
	        $pedido = $pedido[0];

	        //OBtenemos todos los datos del Cliente
	        $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT  
	                        (SELECT razon_social FROM clientes WHERE usuario = '".$usuario."') as razon_social,
	                        (SELECT persona_de_contacto FROM clientes WHERE usuario = '".$usuario."') as persona_de_contacto,
	                        CONCAT( (SELECT domicilio FROM clientes WHERE usuario = '".$usuario."'),
	                                ', ',
	                                (SELECT nombre FROM sys_localidades WHERE id = (    SELECT  localidad 
	                                                                                    FROM    clientes 
	                                                                                    WHERE   usuario = '".$usuario."')),
	                                ', ',
	                                (SELECT nombre FROM sys_provincias WHERE id = (     SELECT  provincia_id 
	                                                                                    FROM    sys_localidades 
	                                                                                    WHERE   id = (  SELECT  localidad 
	                                                                                                    FROM    clientes 
	                                                                                                    WHERE   usuario = '".$usuario."')))
	                        )as domicilio,
	                        (SELECT telefono FROM clientes WHERE usuario = '".$usuario."') as telefono,
	                        (SELECT nombre_del_comercio FROM clientes WHERE usuario = '".$usuario."') as nombre_del_comercio,
	                        email
	                FROM usuarios
	                WHERE usuario = '".$usuario."'");

	        //Cliente
	        $stmt->execute();
	        $cliente = $stmt->fetchAll(PDO::FETCH_ASSOC);
	        $cliente = $cliente[0];



	        $stmt = $GLOBALS['conf']['pdo']->query("SELECT  articulo,
	        (SELECT CONCAT(prefijo,'-',codigo,'-',sufijo) FROM catalogo_articulos WHERE catalogo_articulos.id = articulo) as codigo,
	        (SELECT nombre_singular FROM catalogo_rubros WHERE id = (SELECT id_rubro FROM catalogo_articulos WHERE catalogo_articulos.id = articulo)) as rubro,
	        (SELECT descripcion FROM catalogo_articulos WHERE catalogo_articulos.id = articulo) as descripcion,
	        (SELECT nombre FROM catalogo_marcas WHERE id = (SELECT id_marca FROM catalogo_articulos WHERE catalogo_articulos.id = articulo)) as fabricante,
	        (SELECT GROUP_CONCAT(
	            CONCAT  ((IF(marca_id IS NULL,'Universal (Cualquier vehículo)',(SELECT nombre FROM catalogo_autos_marca WHERE id = marca_id))),' ',
	                    (IF(modelo_id IS NULL,'(Todos los modelos)',(SELECT nombre FROM catalogo_autos_modelo WHERE id = modelo_id))), ' ',
	                    (IF(version_id IS NULL,'',(SELECT descripcion FROM catalogo_autos_versiones WHERE id = version_id))), ' ',
	                    (CONCAT ( IF( anio_inicio = -1 AND anio_final = -1, '', CONCAT( IF( anio_inicio = -1, '[--', CONCAT('[',SUBSTRING(anio_inicio,3,2))), ' / ', IF( anio_final = -1, '--]', CONCAT(SUBSTRING(anio_final,3,2),']')))))) ) SEPARATOR '</li><li>')
	        FROM    catalogo_articulos_aplicaciones as g
	        WHERE   g.articulo_id = p.articulo) as aplicaciones,
	        cantidad
	        FROM    pedidos_articulos p
	        WHERE   pedido = (SELECT id FROM pedidos t WHERE (t.cliente = '".$usuario."') AND (t.fecha_creacion IS NULL));");
	        $stmt->execute();
	        $articulos = $stmt->fetchAll(PDO::FETCH_ASSOC);
	        $id = str_pad($pedido['id'], 6, '0', STR_PAD_LEFT);
	        $fecha = date("d").'/'.date("m").'/'.date("Y");


	        //Creamos el PDF Vacio
	        $mpdf =new mPDF('','', 11, '', 11, 10, 10, 10, 0, 0, '');

	        //Encabezado
	        $html = <<<EOD
	        <!doctype html>
	        <html>
	            <head>
	                <meta charset="utf-8">
	            </head>
	            <body style="font-family: 'opensans', sans-serif;">
	            
	            <!-- Cabecera -->
	            <table width="100%" cellspacing="0" cellpadding="0">
	                <tr>
	                    <td>
	                        <img src="./img/logo_1.png" width="38%"/>
	                        <p style="font-size: 9px; white-space: nowrap; margin: 0px 0px 0px 10px;">
	                            Chile 2019 (B7604-FHI) - Mar del Plata. Buenos Aires. Argentina.<br>
	                            TEL: (0223) 474-1222 | FAX: (0223) 475-7170 / 0800-333-6590<br>
	                            PEDIDOS: pedidos@distribuidoralibertad.com
	                        </p>
	                    </td>
	                    <td width="220"></td>
	                    <td align="right" style="white-space: nowrap; vertical-align: top; padding: 10px 10px 0px 0px; font-size: 12px; font-weight: bold;">
	                        <span style="text-align: center; font-size: 18px; font-family: arial black; text-transform: uppercase;">NOTA DE PEDIDO</span><br><br>
	                        REF. NRO.:  [[id]]<br>FECHA: [[fecha]]
	                    </td>
	                </tr>

	            </table>

	            <!-- Datos del Cliente -->
	            <table width="100%" cellspacing="0" cellpadding="0" style="margin-top: 0px;">
	                <tr>
	                    <td style="height: 20px;"></td>
	                </tr>
	                <tr>
	                    <td style="background: #f4f4f4; color: #656565; padding: 5px 10px 0px 10px; font-size: 12px;"><b>[[cliente]]  -  [[razon_social]]</b></td>
	                </tr>
	                <tr>
	                    <td style="background: #f4f4f4; color: #656565; padding: 1px 10px 5px 10px; font-size: 10px; text-transform: uppercase;">
	                        [[domicilio]]   -   TEL: [[telefono]]
	                    </td>
	                </tr>
	            </table>

	            <!-- Articulos -->
	            <table width="100%" cellspacing="0" cellpadding="0" class="lista_precios">
	                <tr> 
	                    <td class="lista_precios_encabezado" width="50">Cant.</td>
	                    <td class="lista_precios_encabezado" width="150">Código</td>
	                    <td class="lista_precios_encabezado" style="text-align: left;">Descripción</td>
	                </tr>
	                [[articulos]]
	            </table>


	            <!-- Notas -->
	            <table class="notas">
	                <tr>
	                    <td style="font-size: 1em; padding: 10px; border: #edecec 1px solid; color: #3d4040; vertical-align: top;" width="50%">
	                        <i>Notas / Transporte</i><br>
	                        <i style="font-size: 0.75em; height: 134px; padding: 10px; vertical-align: top;">[[notas]]</i>
	                    </td>

	                    <td style="font-size: 12px; color: #69a3c2; padding: 0px 10px 10px 10px;" width="50%"><i>Estimado cliente, si desea cancelar este pedido o hacer modificaciones al mismo podrá hacerlo comunicandose telefonicamente al <b>(0223) 4741222</b>, o bien enviando un email a <b>pedidos@distribuidoralibertad.com</b>. Tenga en cuenta que de no informar modificaciones / cancelaciones el pedido sera despachado directamente a su domicilio.</i></td>
	                </tr>
	            </table>

	        </body>
	    </html>
EOD;



	        $css = <<<EOD

	                     .lista_precios{
	                        margin-top: 10px;
	                        margin-bottom: 40px;
	                        margin-left: 0px;
	                        border: #edecec 0px solid; 
	                        -moz-border-radius: 10px; 
	                        -webkit-border-radius: 10px; 
	                        border-radius: 10px; 
	                        text-align: center;
	                    }

	                    .lista_precios_encabezado{
	                        height: 25px; 
	                        text-align: 
	                        center; color: #ffffff;
	                        padding: 0px 10px 0px 10px;
	                        background: -webkit-linear-gradient(top, #444748 0%, #37393a 100%); 
	                        background-attachment:fixed; 
	                        color: #ffffff;
	                        text-transform: uppercase;
	                        font-size: 12px;
	                        font-weight: bold;
	                    }

	                    .lista_precios_celda{
	                        color: #656565;
	                        font-size: 12px;
	                        height: 30px;
	                        background: #f6f6f6;
	                        padding: 4px 10px 4px 10px;
	                    }

	                    .lista_precios_espaciador{
	                        height: 2px;
	                        background: #ffffff;
	                    }

	                    .a_la_izq{
	                        text-align: left;
	                    }

	                    .a_la_der{
	                        text-align: right;
	                    }

	                    .notas{
	                        width: 100%;
	                        text-align: left;
	                    }

	                    .detalle{
	                        display: block;
	                        width: 100%;
	                        float: left;
	                    }

	                    .detalle_encabezado{
	                        height: 48px; 
	                        text-align: center !important;
	                        padding: 0px 10px 0px 10px  !important;
	                        background: -webkit-linear-gradient(top, #6da7c6 0%, #3d9acb 100%)  !important; 
	                        background-attachment:fixed  !important; 
	                        color: #ffffff  !important;
	                    }

	                    .detalle_espaciador td{
	                        height: 3px;
	                        background: #ffffff;
	                    }

	                    .detalle_fila td{
	                        height: 40px;
	                        background: #f6f6f6;
	                        padding: 0px 10px 0px 10px;
	                        text-align: right;
	                    }

	                    .aplicaciones{
	                        padding-top: 10px;
	                        font-weight: bold;
	                        font-style: italic;
	                        font-size: 10px;
	                    }
EOD;


	        //Numero de Cliente
	        $html = str_replace("[[cliente]]", $usuario, $html);

	        //Razon Social
	        $html = str_replace("[[razon_social]]", $cliente['razon_social'], $html);

	        //Domicilio
	        $html = str_replace("[[domicilio]]", $cliente['domicilio'], $html);

	        //Telefono
	        $html = str_replace("[[telefono]]", $cliente['telefono'], $html);

	        //id
	        $html = str_replace("[[id]]", $id, $html);

	        //fecha
	        $html = str_replace("[[fecha]]", $fecha, $html);

	        //Notas
	        $html = str_replace("[[notas]]", $pedido['notas'], $html);

	        //Articulos
	        $html_tmp = '';
	        foreach ($articulos as $k=>$a){
	            
	            $a['aplicaciones'] = str_replace("</li><li>", ' <img src="./img/bullet.png" style="margin: 0px 7px 2px 7px;"> ', $a['aplicaciones']);

	            $html_tmp .= '<tr><td class="lista_precios_espaciador" colspan="3"></td></tr><tr>';
	            $html_tmp .= '<td class="lista_precios_celda" width="50"><b>'.$a['cantidad'].'</b></td>';
	            $html_tmp .= '<td class="lista_precios_celda" width="150"><b>'.$a['codigo'].'</b></td>';
	            $html_tmp .= '<td class="lista_precios_celda a_la_izq">' .$a['rubro'] .' '. $a['descripcion'] .' '. $a['fabricante'];
	            $html_tmp .= '<img src="./img/separador.png" style="width: 300px; height: 10px;"><p class="aplicaciones"><img src="./img/bullet.png" style="margin: 0px 7px 2px 0px;">'.$a['aplicaciones'].'</p></td></tr>';
	        }

	        //Articulos
	        $html = str_replace("[[articulos]]", $html_tmp, $html);

	        //Notas
	        $html = str_replace("[[notas]]", $pedido['notas'], $html);

	        $mpdf->WriteHTML($css,1);
	        $mpdf->WriteHTML($html);

	        // SALIDA
	        $mpdf->Output('pedido_'.$id.'.pdf','D');
		}

	}

}catch (Exception $e) {
  echo 'No se pudo acceder a la session';
  echo $e;
  exit;
}

?>