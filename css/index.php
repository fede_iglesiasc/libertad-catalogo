<?php

// Array of css files
$css = array(
 'normalise.css',
 'fonts.css',
 'bootstrap.min.css',
 'select2.css',
 'watable.css',
 'header.css',
 'listar.css',
 'scrollbars.css',
 'growl.css',
 'select2-custom.css',
 'floatlabel.css',
 'resultados.css',
 'general.css',
 'imagenes.css',
 'modal.css',
 'bootstrap-modal.css',
 'datepicker.min.css',
 'datepicker_modificado.css',
 'jquery.fancybox.css',
 'font-awesome.min.css',
 'CustomScrollbar.min.css',
 'reporte.css',
 'jstree.min.css',
 'jquery-ui.min.css',
 'bootstrap-spinedit.css',
 'listas.css',
 'unslider.css',
 'configuracion.css',
 'mensajes.css'
);





$mergeCSS = "";
// Loop the css Array
foreach ($css as $css_file) {
    // Load the content of the css file 
    $mergeCSS.= file_get_contents($css_file);
}

// Remove comments also applicable in javascript
$mergeCSS= preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $mergeCSS);

// Remove space after colons
$mergeCSS= str_replace(': ', ':', $mergeCSS);

// Remove whitespace
$mergeCSS= str_replace(array("\n", "\t", '  ', '    ', '    '), '', $mergeCSS);


$mergeCSS = trim(preg_replace('/\s+/', ' ', $mergeCSS));

//Generate Etag
$genEtag = md5_file($_SERVER['SCRIPT_FILENAME']);

// call the browser that support gzip, deflate or none at all, if the browser doest      support compression this function will automatically return to FALSE
ob_start('ob_gzhandler');

// call the generated etag
header("Etag: ".$genEtag); 

// Same as the cache-control and this is optional
header("Pragma: public");

// Enable caching
header("Cache-Control: public ");

// Expire in one day
header('Expires: ' . gmdate('D, d M Y H:i:s', time() + 86400 ) . ' GMT');

// Set the correct MIME type, because Apache won't set it for us
header("Content-type: text/css");

// Write everything out
echo($mergeCSS);

?>