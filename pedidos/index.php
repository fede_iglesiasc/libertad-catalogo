﻿<?php
/////////////////////////////////////////////////////////////////////
// QUITAMOS LIMITACION DE TIEMPO Y MEMORIA PARA EJECUTAR SCRIPT    //
/////////////////////////////////////////////////////////////////////

set_time_limit(0);
ini_set('memory_limit','1024M');
ini_set ('display_errors', 'on');
ini_set ('log_errors', 'on');
ini_set ('display_startup_errors', 'on');
ini_set ('error_reporting', E_ALL);
  

/////////////////////////////////////////////////////////////////////
// INCLUIMOS LAS LIBRERIAS NECESARIAS PARA TRABAJAR			       //
/////////////////////////////////////////////////////////////////////

include_once '../mysql.php';
include_once '../classes/class.mail.php';
include '../classes/PrintNode/Bootstrap.php';


/////////////////////////////////////////////////////////////////////
// ELIMINAMOS PEDIDOS VIEJOS
/////////////////////////////////////////////////////////////////////

$sql = "SELECT    comprobante
        FROM      pedidos 
        WHERE     (fecha_creacion IS NOT NULL) AND (DATEDIFF(CURDATE(), fecha_creacion) > 180)  
        ORDER BY  comprobante DESC";

$stmt = $GLOBALS['conf']['pdo']->query($sql);
$pedidos_eliminar = $stmt->fetchAll(PDO::FETCH_ASSOC);

foreach ($pedidos_eliminar as $k=>$p){ 
  $file = './pedido_'.$p['comprobante'].'.pdf';
  if(file_exists($file)) unlink($file);
  
}


/////////////////////////////////////////////////////////////////////
// CONECTAMOS A PRINTNODE PARA VER SI LA IMPRESORA ESTA ACTIVA
/////////////////////////////////////////////////////////////////////

$credentials = new \PrintNode\Credentials\ApiKey('dfb6891945c14d6022326e7f59140d7dc1832245');
$client = new \PrintNode\Client($credentials);
$computers = $client->viewComputers();
$connectedPc = null;

//PRINTSERVER 
if(isset($computers[55393]) && ($computers[55393]->state == 'connected')){
    $connectedPc = $computers[55393];
}

//SSD
else if(isset($computers[55137]) && ($computers[55137]->state == 'connected')){
    $connectedPc = $computers[55137];
}

//Mechanic
elseif(isset($computers[55360]) && ($computers[55360]->state == 'connected')){
    $connectedPc = $computers[55360];
}

//Gabriel
elseif(isset($computers[55294]) && ($computers[55294]->state == 'connected')){
    $connectedPc = $computers[55294];
}
	
if(!$connectedPc) return;



/////////////////////////////////////////////////////////////////////
// ENVIAMOS SMS o WATSAPP SI EL MAIL NO SE IMPRIME HACE MAS DE 10 MINUTOS
/////////////////////////////////////////////////////////////////////

$sql = "    SELECT  * 
            FROM    pedidos 
            WHERE   (fecha_creacion IS NOT NULL) 
                    AND 
                    (fecha_creacion < DATE_SUB(NOW(),INTERVAL 15 MINUTE)) 
                    AND 
                    (impreso = 0)";

$stmt = $GLOBALS['conf']['pdo']->query($sql);
$pedidos_retrasados = $stmt->fetchAll(PDO::FETCH_ASSOC);
/*if(count($pedidos_retrasados)){
    $printJob = new \PrintNode\Entity\PrintJob($client);
    $printJob->title = "Pedido ".$pedidos_retrasados[$k]['comprobante'];
    $printJob->source = 'Catalogo';
    $printJob->contentType = 'pdf_base64';
    $printJob->addPdfFile('./aviso.pdf');

    //printserver
    if($connectedPc->id == 55393) $printJob->printer = '336723';
    //ssd
    if($connectedPc->id == 55137) $printJob->printer = '333894';
    //mecanico
    if($connectedPc->id == 55360) $printJob->printer = '336289';
    //gabriel
    if($connectedPc->id == 55294) $printJob->printer = '391255';


    $printJobId = $client->createPrintJob($printJob);
}*/

///////////////////////////////////////////////////////////////////////////////////
// CONECTAMOS A LA DB PARA VER SI EXISTEN TRABAJOS PENDIENTES DE IMPRESION       //
///////////////////////////////////////////////////////////////////////////////////


//Obtenemos pedidos por imprimir
$sql = "SELECT      *,
                    (SELECT email FROM usuarios WHERE usuario = p.cliente) as email,
                    (SELECT persona_de_contacto FROM clientes WHERE usuario = p.cliente) as persona_de_contacto
        FROM        pedidos p
        WHERE       (impreso = 0) AND 
                    (comprobante <> 0) 
        ORDER BY    comprobante ASC";

$stmt = $GLOBALS['conf']['pdo']->query($sql);
$pedidos = $stmt->fetchAll(PDO::FETCH_ASSOC);

//Imprimimos todo
foreach ($pedidos as $k=>$p){ 

	//////////////////////////////////////////////////////
	// IMPRIMIMOS TODOS LOS PEDIDOS PENDIENTES	   		//
	//////////////////////////////////////////////////////
	echo "Hay pedidos por imprimir.... imprimiendo....";
	$printJob = new \PrintNode\Entity\PrintJob($client);
	$printJob->title = "Pedido ".$pedidos[$k]['comprobante'];
	$printJob->source = 'Catalogo';
	$printJob->contentType = 'pdf_base64';
	$printJob->addPdfFile('./pedido_'.$pedidos[$k]['comprobante'].'.pdf');

    //printserver
    if($connectedPc->id == 55393) $printJob->printer = '388963';
    //ssd
    if($connectedPc->id == 55137) $printJob->printer = '378619';
    //mecanico
    if($connectedPc->id == 55360) $printJob->printer = '336289';
    //gabriel
    if($connectedPc->id == 55294) $printJob->printer = '391255';


	$printJobId = $client->createPrintJob($printJob);
	
	//////////////////////////////////////////////////////
	// ENVIAMOS LOS CORREOS CORRESPONDIENTES	   		//
	//////////////////////////////////////////////////////

        //Creamos el correo
        $mail = new send_mail(true);

        for ($i=0; $i < 3; $i++) { 
            try {
                $mail->Subject = 'Recibimos el Pedido';
                 
                $mail->isHTML(true);

                $mail->Body    = '<html><head></head><body text="#000000" bgcolor="#FFFFFF"><table width="100%"><tbody><tr><td align="right"><img alt="logo" style="float: right;" src="http://www.distribuidoralibertad.com/logo_distribuidora.jpg" height="54" width="200" align="right"></td></tr><tr><td>';

                $mail->Body   .= "<strong>".$p['persona_de_contacto'].",</strong><br/> Acabamos de recibir su pedido, en instantes comenzaremos a prepararlo. Le adjuntamos en este correo una copia en formato PDF.<br/><br/>Si desea cancelar este pedido o hacer modificaciones al mismo podrá hacerlo comunicandose telefonicamente al <b>(0223) 4741222</b>, o bien enviando un email a <b>pedidos@distribuidoralibertad.com</b>. Tenga en cuenta que de no informar modificaciones / cancelaciones el pedido sera despachado directamente a su domicilio.";

                $mail->Body   .= "</td></tr><tr><td><hr><p style=\"font-size: 10px;\"><b style=\"font-size: 12px;\">Federico Iglesias Colombo<br>Distribuidora Libertad</b></p><p style=\"font-size: 10px;\">Mar del Plata. Buenos Aires. Argentina. <br>Chile 2019 (B7604-FHI)<br>TEL: (0223) 474-1222 / 474-1223 / 475-8352<br>FAX: (0223) 475- 7170 / 0800-333-6590<br></p></td></tr></tbody></table></body></html>";

                $mail->AltBody = $p['persona_de_contacto'].",</strong><br/> Acabamos de recibir su pedido, en instantes comenzaremos a prepararlo. Le adjuntamos en este correo una copia en formato PDF.\n \nSi desea cancelar este pedido o hacer modificaciones al mismo podrá hacerlo comunicandose telefonicamente al (0223) 4741222 o 4741223, o bien enviando un email a pedidos@distribuidoralibertad.com.";
                 
                $mail->AddAddress($p['email']);
                $mail->AddAddress('pedidos@distribuidoralibertad.com');

                $mail->AddAttachment('./pedido_'.$p['comprobante'].'.pdf', "Pedido ".$p['comprobante'], 'base64', 'application/pdf');

                $mail->Send();

                //Si mando bien el correo salimos
                break;

            } catch (phpmailerException $e) {
                //Si fallo tres veces salimos
                if($i==3){
                	break;
                }
            }
        }


    //Actualizamos pedido impreso
    $GLOBALS['conf']['pdo']->query("UPDATE pedidos SET impreso = 1, fecha_impresion = now() WHERE id=".$p['id']);
}


//Guardamos el log
//$GLOBALS['conf']['pdo']->query("INSERT INTO log_impresiones (`log`) VALUES ('".$log."');");

//Eliminamos logs mayores a tres dias de antiguedad
//$GLOBALS['conf']['pdo']->query("DELETE 	FROM `log_impresiones` WHERE fecha < UNIX_TIMESTAMP(DATE_SUB(NOW(), INTERVAL 3 DAY))");

?>