<?php

include_once('../classes/Minifier.php');

$js = array();
$js[] = array('file'=>'modernizr-2.0.6.min.js', 'minify'=> true);
$js[] = array('file'=>'jquery2-min.js', 'minify'=> true);
$js[] = array('file'=>'jquery-ui.min.js', 'minify'=> false);
$js[] = array('file'=>'bootstrap.js', 'minify'=> false);
$js[] = array('file'=>'select2.min.js', 'minify'=> true);
$js[] = array('file'=>'jquery.watable.js', 'minify'=> true);
$js[] = array('file'=>'jquery.transit.min.js', 'minify'=> true);
$js[] = array('file'=>'toolbox.js', 'minify'=> true);
$js[] = array('file'=>'jquery.field.js', 'minify'=> true);
$js[] = array('file'=>'jquery.fancybox.pack.js', 'minify'=> false);
$js[] = array('file'=>'bootstrap-datepicker.js', 'minify'=> true);
$js[] = array('file'=>'bootstrap-growl.min.js', 'minify'=> false);
$js[] = array('file'=>'CustomScrollbar.js', 'minify'=> false);
$js[] = array('file'=>'jstree.min.js', 'minify'=> false);
$js[] = array('file'=>'jquery.Jcrop.min.js', 'minify'=> false);
$js[] = array('file'=>'print.js', 'minify'=> true);
$js[] = array('file'=>'dmuploader.min.js', 'minify'=> false);
$js[] = array('file'=>'bootstrap-spinedit.js', 'minify'=> true);
$js[] = array('file'=>'unslider-min.js', 'minify'=> false);


//Obtenemos fecha de modificacion
//del archivo JS mas reciente.
$max = 0;
foreach($js as $file){
	$seconds = filemtime('./'.$file['file']);
	if($seconds > $max)
		$max = $seconds;
}

//Bandera para saber si debemos
//regenerar el codigo comprimido
$genera = false;

//Si no esta generado el archivo tmp
//lo generamos a la fuerza.
if(!file_exists('./tmp.js'))
	$genera = true;

//Si esta vencido tambien
if($max > filemtime('./tmp.js')) 
	$genera = true;


$mergeJS = "";

//Si hay que modificar
//generamos el temporal
//de nuevo
if($genera){
	foreach ($js as $js_file)
	    if($js_file['minify']) $mergeJS.= Minifier::minify(file_get_contents($js_file['file']));
	    else $mergeJS.= file_get_contents($js_file['file']);

	//Guardamos el codigo generado
	file_put_contents('tmp.js', $mergeJS);
}


//Si no hay modificaciones usamos el temporal
else $mergeJS.= file_get_contents('./tmp.js');



//Generate Etag
$genEtag = md5_file($_SERVER['SCRIPT_FILENAME']);

// call the browser that support gzip, deflate or none at all, if the browser doest      support compression this function will automatically return to FALSE
ob_start('ob_gzhandler');

// call the generated etag
header("Etag: ".$genEtag); 

// Same as the cache-control and this is optional
header("Pragma: public");

// Enable caching
header("Cache-Control: public ");

// Expire in one day
header('Expires: ' . gmdate('D, d M Y H:i:s', time() + 86400 ) . ' GMT');

// Set the correct MIME type, because Apache won't set it for us
header("Content-type: text/javascript");

// Write everything out
echo($mergeJS);

?>