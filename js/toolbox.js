
//ABRIMMOS VENTANA MODAL
function abrir_modal(nombre, width, close_button){

  //Instanciamos el modal...
  var modal = $('#'+nombre+'Modal');
  
  //Si el modal ya existe no hacemos nada
  if( !(modal.length) ){
    //Seteamos ancho
    if( (typeof(width)==='undefined') || ( (typeof width === 'string') && (width == 'auto') )) width = '';
    else if( (typeof width === 'string') && (width != 'auto') ) width = ' style="width: '+width+';"';
    else width = ' style="width: '+width+'px;"';


    if(typeof(close_button)==='undefined')
      close_button = true;

    //Creamos el modal
    var popup = '<div id="'+nombre+'Modal" class="modal fade" role="dialog" aria-hidden="true" '+width+'><div class="modal-header">';

    //Agregamos el boton de cerrar?
    if(close_button) popup += '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>';

    //agregamos resto del cuerpo del modal...
    popup += '<h4 class="text-center" id="'+nombre+'Modal_titulo"></h4></div><div class="modal-body"><div class="row clearfix"><div class="col-sm-12" id="'+nombre+'Modal_container"></div></div></div><div class="modal-footer" style="text-align: center !important;" id="'+nombre+'Modal_footer"></div></div>';

    //Agregamos al body
    $(popup).appendTo("body");

    //Al cerrar eliminamos la ventana modal
    $('#'+nombre+'Modal').on('hidden.bs.modal', function () {

      //Si habiamos creado algun 'scroll' lo eliminamos
      $('.scrollable',this).each(function(){
        $(this).mCustomScrollbar("destroy");
      });

      //Quitamos confirmaciones abiertas
      if(typeof confirma_ !== 'undefined')
        confirma_.close();

      //Quitamos dialogos abiertos
      if(typeof dialogo_ !== 'undefined')
        dialogo_.close();

      //Destruimos los S2
      $('input',this).each(function(){
        
        var s2 = $(this).data('select2');
        if(s2){
          $(this).select2('destroy');
        }
      });

      //Despues de animar el cierre
      setTimeout(function(){

        //Si existe una funcion callback la llamamos...
        if ( eval("typeof "+nombre+"Modal_onClose === 'function'") )
          eval(nombre+"Modal_onClose();");

        //Eliminamos el popup
        $('#'+nombre+'Modal').remove();

      }, 100);
    });
  
  //Si el modal ya esta abierto
  //cambiamos el ancho...
  }else{
    modal.width(width);
  }
}

//Muestra icono Cargando...
function cargando(){ return '<div style="display: block; text-align: center;"><br/><img src="./img/procesando.gif" width="30" height="30"></div>'; }


//Plugin para ejecutar funcion despues del ultimo
//keyup presionado en un input(function ($) {
$.fn.delayKeyup = function(callback, ms){
    var timer = 0;
    var el = $(this);
    $(this).keyup(function(){                   
    clearTimeout (timer);
    timer = setTimeout(function(){
        callback(el)
        }, ms);
    });
    return $(this);
};


//Plugin que genera todos los eventos para
// que el input muestre un tooltip con su nombre
$.fn.tooltipLabel = function(){

  //Si el Campo esta vacio no mostramos el tooltip
  $(this).on('focusin',function(e){
    if($(this).val().trim() == '') $(this).tooltip('hide');
    else $(this).tooltip('show');
  });

  //Si el Campo esta vacio no mostramos el tooltip
  $(this).on('focusout',function(e){
    $(this).tooltip('hide');
  });

  //Si el Campo esta vacio no mostramos el tooltip
  $(this).keyup(function(e){
    if($(this).val().trim() != '') $(this).tooltip('show');
    else $(this).tooltip('hide');
  });

  $(this).each(function(){
    $(this).tooltip({ animation: false,
              trigger: 'manual',
              placement: 'left',
              template: '<div class="tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner" style="padding: 10px 15px; font-weight: bold; font-size: 12px;"></div></div>'});
  });

};

//Funcion para genera los labels
//flotantes cuando el placeholder
//no se esta mostrando.
$.fn.floatLabel = function(){
    
    //Si ya existe el label para el input no hacer nada
    if(!$('span[data-input="'+$(this).attr('id')+'"]').length){

      var type = $(':first-child',this).data('input_type');
      var config = $(':first-child',this).data('input_config');


      //Si es Input
      if((type == 'input') || (type == 'textarea')){
        //Input
        var input = $(':first-child',this);
        //Label
        var label = $('<span class="floatLabel" data-input="'+input.attr('id')+'">'+input.attr('placeholder')+'</span>');
        
        
        //Si el input ya muestra su Placeholder por estar vacio
        //No mostramos el label
        if(input.val() != '') {
          label.css('display', 'block');
          input.addClass('floatLabel_conVal');
        }else{
          label.css('display', 'none');
          input.removeClass('floatLabel_conVal');
        }

        //Si el imput esta oculto tampoco mostramos el label
        if(input.css('display') == 'none') label.css('display', 'none');

        //Si el Campo esta vacio no mostramos el tooltip
        input.on('focusin',function(e){
          $(this).prev().css('color','#66afe9');
          $(this).removeClass('floatLabel_conVal');
        });

        //Si el Campo esta vacio no mostramos el tooltip
        input.on('focusout',function(e){
          $(this).prev().css('color','black');
          if($(this).val() != '') $(this).addClass('floatLabel_conVal');
          else $(this).removeClass('floatLabel_conVal');
        });

        //Si el Campo esta vacio no mostramos el tooltip
        input.keyup(function(e){
          if($(this).val() != ''){
            $('span[data-input="'+$(this).attr('id')+'"]').show(200);
            $(this).removeClass('floatLabel_conVal');
          }else{
            $('span[data-input="'+$(this).attr('id')+'"]').hide(200);
            $(this).removeClass('floatLabel_conVal');
          }
        });
      }

      //Si es un Select2
      if(type == 'select2'){
        //Input
        var input = $(':first-child',this);

        //Label
        var label = $('<span class="floatLabel" data-input="'+input.attr('id')+'">'+input.attr('placeholder')+'</span>');

        //Ocultamos el campo
        label.css('display', 'none');
        input.removeClass('floatLabel_conVal');
      }

      //Si es un daterange
      if(type == 'daterange'){
        var input_desde = $('input:first-child',this);
        var input_hasta = $('input:last-child',this);

        //Label
        var label = $('<div class="floatLabelDaterange"><div> <span class="floatLabel" data-input="'+input_desde.attr('id')+'">'+input_desde.attr('placeholder')+'</span> <span style="width: 50px;"></span> <span class="floatLabel" data-input="'+input_desde.attr('id')+'">'+input_hasta.attr('placeholder')+'</span> </div></div>');

        //Update method
        $(':first-child',this).data('update_method',function(el){
          
          var input_desde = $('input:first-child',el);
          var input_hasta = $('input:last-child',el);
          var icono = $('span',el);

          //Desde vacio?
          var desde_vacio = false;
          if($.trim(input_desde.val()) == '') 
            desde_vacio = true;

          //Hasta vacio?
          var hasta_vacio = false;
          if($.trim(input_hasta.val()) == '') 
            hasta_vacio = true;

          //Punteros a los labels
          var label_container = el.prev();
          var label_desde = label_container.find('span:first-child');
          var label_hasta = label_container.find('span:last-child');
          var label_container_visible = (label_container.css('display') != 'none' ? true : false);
          var label_desde_visible = (label_desde.css('opacity') == '1' ? true : false);
          var label_hasta_visible = (label_hasta.css('opacity') == '1' ? true : false);

          //Ocultamos todo
          if(desde_vacio && hasta_vacio && label_container_visible){
            label_desde.animate({opacity: 0},300);
            label_hasta.animate({opacity: 0},300);
            label_container.css('display','none');
          }

          //Si debemos mostrar el contenedor
          if(!label_container_visible && (!desde_vacio || !hasta_vacio))
            label_container.show();

          //Mostramos Desde
          if(!desde_vacio && !label_desde_visible)
            label_desde.fadeTo(300, 1);

          //Mostramos Hasta
          if(!hasta_vacio && !label_hasta_visible)
            label_hasta.fadeTo(300, 1);

          //Ocultamos Desde
          if(desde_vacio && label_desde_visible)
            label_desde.fadeTo(300, 0);

          //Ocultamos Hasta
          if(hasta_vacio && label_hasta_visible)
            label_hasta.fadeTo(300, 0);

          //Cambiamos el label
          label_desde.css('color','black');
          if(!desde_vacio) input_desde.addClass('floatLabel_conVal');
          else input_desde.removeClass('floatLabel_conVal');

          //Cambiamos el label
          label_hasta.css('color','black');
          if(!hasta_vacio) input_hasta.addClass('floatLabel_conVal');
          else input_hasta.removeClass('floatLabel_conVal');

          //Quitamos o agregamos los bordes del icono
          if(desde_vacio || hasta_vacio)
            icono.css('border','1px solid #ccc');

          if(!desde_vacio && !hasta_vacio)
            icono.css('border','0px solid #ccc');

        });

        //Evento focusin
        input_desde.on('focusin',function(){
          var label = $(this).parent().parent().prev().find('span:first-child');
          label.css('color','#66afe9');
          $(this).removeClass('floatLabel_conVal');
        });

        //Evento focusin
        input_hasta.on('focusin',function(){
          var label = $(this).parent().parent().prev().find('span:last-child');
          label.css('color','#66afe9');
          $(this).removeClass('floatLabel_conVal');
        });
      }



      //Si es un daterange
      if(type == 'date'){
        var input = $('input',this);

        //Label
        var label = $('<span class="floatLabel" data-input="'+input.attr('id')+'">'+input.attr('placeholder')+'</span>');
        
        
        //Si el input ya muestra su Placeholder por estar vacio
        //No mostramos el label
        if(input.val() != '') {
          label.css('display', 'block');
          input.addClass('floatLabel_conVal');
        }else{
          label.css('display', 'none');
          input.removeClass('floatLabel_conVal');
        }

        //Si el imput esta oculto tampoco mostramos el label
        if(input.css('display') == 'none') label.css('display', 'none');

        //Si el Campo esta vacio no mostramos el tooltip
        input.on('focusin',function(e){
          $(this).prev().css('color','#66afe9');
          $(this).removeClass('floatLabel_conVal');
        });

        //Si el Campo esta vacio no mostramos el tooltip
        input.on('focusout',function(e){
          $(this).prev().css('color','black');
          if($(this).val() != '') $(this).addClass('floatLabel_conVal');
          else $(this).removeClass('floatLabel_conVal');
        });

        //Si el Campo esta vacio no mostramos el tooltip
        input.keyup(function(e){
          if($(this).val() != ''){
            $('span[data-input="'+$(this).attr('id')+'"]').show(200);
            $(this).removeClass('floatLabel_conVal');
          }else{
            $('span[data-input="'+$(this).attr('id')+'"]').hide(200);
            $(this).removeClass('floatLabel_conVal');
          }
        });

        //Update method
        $('input',this).data('update_method',function(el){
          


        });
      }


      //Lo insertamos
      return label;
    }
};


//Funcion Fix (floatLabel) para poder setear
//el value de un campo y que muestre
//o no el label dependiendo de su valor
$.fn.val_ = function(value){

    //Define metodo
    var metodo = 'set';
    if(typeof value === 'undefined')
      metodo = 'get';

    //Chequeamos si tiene Select2
    var s2 = $(this).data('select2');
    var tipo = '';

    //¿QUE TIPO DE ELEMENTO ES?

    //Textarea
    if($(this).data('input_type') == 'textarea')
      tipo = 'textarea';

    //Checkbox
    if($(this).data('input_type') == 'checkbox')
      tipo = 'checkbox';

    //Input
    if((!s2) && ($(this).prop("tagName") == 'INPUT') )
      tipo = 'input';

    //Select2
    if(s2) tipo = 'select2';


    //Si estamos seteando valor
    if(metodo == 'set'){

      //Checkbox
      if(tipo == 'checkbox'){
        var config = $(this).data('config');
        $(this).val(value);
        config.update($(this),value);
      }

      //Checkbox
      if(tipo == 'textarea'){
        $(this).val(value);
      }

      //ASIGNAMOS EL VALOR

      //Input Común
      if((tipo == 'input') || (tipo == 'textarea')){
        $(this).val(value);
        //Si el imput esta oculto tampoco mostramos el label
        if($(this).css('display') != 'none')
          if($(this).val() != '') $(this).prev().show();
          else $(this).prev().hide();

        //Pintamos el input dependiendo de su contenido
        if($(this).val() != '') $(this).addClass('floatLabel_conVal');
        else $(this).removeClass('floatLabel_conVal');
      }

      //Select2
      if(tipo == 'select2'){
        $(this).select2("data",value);

        if(value == null)$(this).prev().prev().hide();
        else $(this).prev().prev().show();
      }
    }


    //Si estamos obteniendo el valor
    if(metodo == 'get'){

      //Si es textarea o input
      if((tipo == 'input') || (tipo == 'textarea') || (tipo == 'checkbox'))
        return $(this).val();

      //Si es un S2
      if(tipo == 'select2')
        return $(this).select2("data");
    
    }
};



function notifica(titulo,mensaje,icono,estilo){

    if(icono == 'exclamacion') icono = 'glyphicon glyphicon-exclamation-sign';
    if(icono == 'interrogacion') icono = 'glyphicon glyphicon-question-sign';
    if(icono == 'informacion') icono = 'glyphicon glyphicon-info-sign';
    if(icono == 'confirmacion') icono = 'glyphicon glyphicon-ok-sign';

    //mostramos el growl
    $.growl({
    title: '<strong style="font-size: 14px;">'+titulo+'</strong></br>',
    message: mensaje,
    icon: icono },
    {
      type: estilo,
      placement: {
        from: "bottom",
        align: "left"
      },
      delay: 10000,
      z_index: 5000,
      template: '<div data-growl="container" class="alert" role="alert" style="width: 400px;"> <button type="button" class="close" data-growl="dismiss"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button><span data-growl="icon" style="float: left; margin: 5px 15px 0px 0px; font-size: 45px;"></span><span data-growl="title" style="margin-right: 30px; display: block;"></span><span data-growl="message" style="margin-right: 30px; display: block;"></span><a href="#" data-growl="url"></a></div>'
  });
}

//Notifica a los usuarios...
var confirma_;
function confirma(titulo,mensaje,icono,estilo,callback){
    if(icono == 'exclamacion') icono = 'glyphicon glyphicon-exclamation-sign';
    if(icono == 'interrogacion') icono = 'glyphicon glyphicon-question-sign';
    if(icono == 'informacion') icono = 'glyphicon glyphicon-info-sign';


    //mostramos el growl
    confirma_ = $.growl({
                  title: '<strong style="font-size: 14px;">'+titulo+'</strong></br>',
                  message: mensaje,
                  icon: icono },
                  {
                    type: estilo,
                    placement: {
                      from: "bottom",
                      align: "left"
                    },
                    delay: 0,
                    allow_dismiss: false,
                    z_index: 5000,
                    allow_dismiss: true,
                    onHidden: function(){
                      confirma_ = undefined;
                    },
                    template: '<div data-growl="container" class="alert" role="alert" style="width: 400px;"><span data-growl="icon" style="float: left; margin: 1px 15px 0px 0px; font-size: 40px;"></span><span data-growl="title" style="margin-right: 30px; display: block;"></span><span data-growl="message" style="margin: 0px 0px 10px 45px; display: block;"></span><div style="text-align: center;"><button type="button" class="btn btn-warning growl-close-fixed" style="display: inline-block; margin: 10px;" onclick="confirma_.close();">Cancelar</button><button type="button" class="btn btn-primary" style="display: inline-block; margin: 10px;" onclick="'+callback+'; confirma_.close();">Confirmar</button></div><a href="#" data-growl="url"></a></div>'
                });
}

//Notifica a los usuarios...
var dialogo_;
function dialogo(titulo,mensaje,icono,estilo,placeholder,callback){
    if(icono == 'exclamacion') icono = 'glyphicon glyphicon-exclamation-sign';
    if(icono == 'interrogacion') icono = 'glyphicon glyphicon-question-sign';
    if(icono == 'informacion') icono = 'glyphicon glyphicon-info-sign';


    //mostramos el growl
    dialogo_ = $.growl({
                  title: '<strong style="font-size: 14px;">'+titulo+'</strong></br>',
                  message: mensaje,
                  icon: icono },
                  {
                    type: estilo,
                    placement: {
                      from: "bottom",
                      align: "left"
                    },
                    delay: 0,
                    allow_dismiss: false,
                    z_index: 5000,
                    allow_dismiss: true,
                    onShown: function(){

                      //disparamos en enter
                      $('#dialogo_input',dialogo_.$template).keypress(function(event) {
                        if (event.keyCode == 13)
                          $('button.btn-primary',dialogo_.$template).trigger("click");
                      });

                      //Hacemos foco al abrir
                      $('#dialogo_input',dialogo_.$template).focus();
                      //Disparamos callback
                      callback(dialogo_.$template);
                    },
                    
                    onHidden: function(){
                      confirma_ = undefined;
                    },
                    template: '<div data-growl="container" class="alert" role="alert" style="width: 400px;"><span data-growl="icon" style="float: left; margin: 1px 15px 0px 0px; font-size: 40px;"></span><span data-growl="title" style="margin-right: 30px; display: block;"></span><span data-growl="message" style="margin: 0px 0px 10px 45px; display: block;"></span><input type="text" id="dialogo_input" class="form-control" style="width: 100%; margin-bottom: 10px; margin-top: 20px;" placeholder="'+placeholder+'"/><div style="text-align: center;"><button type="button" class="btn btn-warning growl-close-fixed" style="display: inline-block; margin: 10px;" onclick="dialogo_.close();">Cancelar</button><button type="button" class="btn btn-primary" style="display: inline-block; margin: 10px;">Confirmar</button></div><a href="#" data-growl="url"></a></div>'
                });
}

//Llamada al server
function apix(module, run, data, config){

  //Config Vacio
  if(typeof config === 'undefined')
    config = {};

  //Reload
  var reload = false;
  if(typeof config.reload !== 'undefined') 
    reload = config.reload;

  //Files
  var files = false;
  if(typeof config.file !== 'undefined') 
    files = config.file;

  //Debug
  var debug = false;
  if(typeof config.debug !== 'undefined') 
    debug = config.debug;

  var url = "api.php?module="+module+'&run='+run;
  if(debug)
    url = "api_debug.php?module="+module+'&run='+run

  //Si son archivos seteamos la configuracuion para
  //el llamado ajax de jquery
  var ajaxcall = {
    type: 'POST',
    url: url,
    data: data,
    dataType: "json",
    cache: false,
    error: function (json) {
    },
    success: function (json) {

        //If session expire reload page...
        if(reload)
          if(!json.logued) location.reload();

        //Si ocurrio un error lo mostramos
        if(json.status == '0'){
          notifica('Error',json.errors.join('<br>'),'exclamacion','danger');
          if(typeof config.error !== 'undefined')
            config.error(json);
        }else{
          if(typeof config.ok !== 'undefined')
            config.ok(json);
        }
    }
  }

  //Agregamos los parametros
  //si estamos enviando files
  if(files){
    ajaxcall.processData =  false; // No procesar los archivos
    ajaxcall.contentType =  false; // Query string request
  }

  //Ajax
  $.ajax(ajaxcall);
}

//Llamada a la API
function api(modulo,accion,data,callbackOK,callbackERROR, method, reload){

  //Si estamos debugueando no refrescamos la pagina
  //si el usuario no esta logueado
  if(typeof method === 'undefined') method = 'GET';

  if(typeof reload === 'undefined') reload = true;

  //var data
  data['module'] = modulo;
  data['run'] = accion;

  //Make ajax call to api
  $.ajax({
    type: 'POST',
    url: "api.php",
    data: data,
    dataType: "json",
    cache: false,
    error: function (json) {
    },
    success: function (json) {
        //If session expire reload page...
        if(reload)
          if(!json.logued) location.reload();

        //Si ocurrio un error lo mostramos
        if(json.status == '0'){
          notifica('Error',json.errors.join('<br>'),'exclamacion','danger');
          if(typeof callbackERROR !== 'undefined')
            callbackERROR(json);
        }else{
          callbackOK(json);
        }
    }
  }); 
}

//Generamos tabla
function generaTabla(config){
  //Tabla base
  var tabla = $('<div class="list-group scrollable" id="'+config.id_tabla+'_lista" style="max-height: '+config.max_height+'px; overflow-y: hidden;"></div>');

  if(typeof config.campobusqueda === 'undefined')
    config.campobusqueda = true;

  //Esta permitido hacerle click?
  if(typeof(config.permitir_click) === 'undefined')
    config.permitir_click = 1;

  //Texto para 'Sin resultados'
  if(typeof(config.sinResultados_label) === 'undefined')
    config.sinResultados_label = 'Sin Resultados';

  //Guardamos la configuracion
  tabla.data('plugin_config',config);

  //Metodo para refrescar la tabla con nuevos items
  tabla.data('plugin_update', function(el, items){

    //Configuracion
    var config = el.data('plugin_config');

    //Vaciamos la lista
    $('.list-group-item',el).remove();


    //Recorremos los items
    for(var k in items){
      //Creamos el item
      if(items[k][config.key_nombre] != null)
        var item = $('<a class="list-group-item" href="#"><b>'+items[k][config.key_nombre]+'</b></a>');
      else{
        var item = $('<a class="list-group-item" href="#"><b></b></a>');
      }

      //Le asignamos la data
      for(var x in items[k]){
        item.data(x, toObject(items[k][x]));
      }

      //Lo marcamos inicialmente como Cerrado
      item.data('abierto',0);

      if(config.permitir_click){
        //Le asignamos el evento click
        item.on('click',function(){
            
            //Si al anchor que le hicimos click
            //esta abierto lo cerramos.
            if($(this).data('abierto') == 1){

              $(this).removeClass('active_');
              $('.descripcion',this).remove();
              $('h4',this).contents().unwrap();
              $(this).data('abierto',0);

              //Activamos el estado cerrado
              config.funcion_desactivado(this);

            }else{
              //Existe algun otro elemento abierto antes?
              var elActivo = $(this).parent().find('.active_');

              if(elActivo.length){

                //Lo marcamos como cerrado al anterior
                elActivo.data('abierto',0);
                elActivo.removeClass('active_');
                elActivo.find('h4').contents().unwrap();
                elActivo.find('.descripcion').remove();
                elActivo.blur();
                //desactivamos
                config.funcion_desactivado(elActivo);
              }

              //Hacemos los cambios al actual
              $(this).addClass('active_');
              $(this).data('abierto',1);

              //Activamos el estado abierto
              config.funcion_activado(this);
            }
        });
      }

      if(items[k][config.key_nombre] == null)
        config.funcion_desactivado(item);

      //Insertamos el item a la tabla
      //si existe un scrollbar lo metemos dentro
      //del contenedor del scrollbar
      if($('.mCSB_container',el).length){
        $('.mCSB_container',el).append(item);
      }else{
        el.append(item);
      }
    }

      //Si no existen items mostramos 'Sin items'
      if(!items.length) {
        $("#"+config.id_tabla+"_sin_resultados", el).show();
      }else{
        $("#"+config.id_tabla+"_sin_resultados", el).hide();
      }
  });

  //Agergamos 'No se encontraron coincidencias'
  tabla.append('<div style="display: none; color: gray; text-align: center; border: none;" id="'+config.id_tabla+'_sin_resultados">'+config.sinResultados_label+'</div>');


  //Si tenemos los datos de la API ajax cargamos los items
  //desde el servidor, sino usamos la propiedad items que nos
  //pasan por parametro
  if(typeof config.items == null){
      //Llamada a la API
      var data = {};
      data['q'] = $(el).val().trim().toLowerCase();
      
      //Agregamos paramnetros adic..
      if(typeof config.ajaxData != 'undefined')
        for(var k in config.ajaxData)
          data[k] = config.ajaxData[k];

      //API CALL
      api(config.ajaxModulo,config.ajaxAccion,data,function(json){
        //Asignamos los items
        tabla.data('plugin_update')(tabla,json.result);
      });
  }else{
    //Asignamos los items
    tabla.data('plugin_update')(tabla,config.items);
  }

  //Creamos el campo de busqueda
  var busqueda = $('<form class="form-search form-search-normal form-inline" action="javascript:void(0);"><input type="text" id="'+config.id_tabla+'_lista_busqueda" data-keys="'+config.keys_busqueda.join(',')+'" class="form-control search-query" style="width: 100%; margin-bottom: 20px;" placeholder="Busqueda rápida"/></form>');

  //Cuando escribimos cambiamos el icono
  $('input',busqueda).keydown(function(){
    $(this).parent().removeClass('form-search-normal').addClass('form-search-loading');
  });

  //Mecanismo de busqueda rapida
  $('input',busqueda).delayKeyup(function(el){

    //OBtenemos id del plugin
    var id = $(el).attr('id');
    id = id.replace('_lista_busqueda','');

    var tabla = $('#'+id+'_lista');
    var config = tabla.data('plugin_config');


    if(typeof config.ajaxModulo != 'undefined'){
      //Llamada a la API
      var data = {};
      data['q'] = $(el).val().trim().toLowerCase();
      
      //Agregamos paramnetros adic..
      if(typeof config.ajaxData != 'undefined')
        for(var k in config.ajaxData)
          data[k] = config.ajaxData[k];
      
      //API CALL
      api(config.ajaxModulo,config.ajaxAccion,data,function(json){
        //Asignamos los items
        tabla.data('plugin_update')(tabla,json.result);

        //Existen resultados ?
        if(!json.result.length)
          $("#"+id+'_sin_resultados',tabla).show();
        else
          $("#"+id+'_sin_resultados',tabla).hide();

        //Quitamos loading
        $(el).parent().addClass('form-search-normal').removeClass('form-search-loading');
      });
    }else{

      var keywords = $(el).data('keys').split(',');
      var search = $(el).val().trim().toLowerCase().split(' ');
      var cont = 0;

      //Si no tenemos nada x buscar
      //mostramos todos los items
      if(search.length == 0){
        tabla.find('.list-group-item').show();
        cont = 1;
      
      //Busqueda
      }else{
        
        tabla.find('.list-group-item').each(function(){
          //Ocultamos  todos
          $(this).hide();
          //Mostramos coincidencias
          for(var k in keywords)
            for(var y in search){
              //Quitarle los acentos
              var str = $(this).data(keywords[k]).toLowerCase();
              str = removeDiacritics(str);
              if( str.indexOf(search[y]) > -1 ){
                $(this).show();
                cont++;
              }
            }
          });

        //Mostramos aviso
        if(!cont) $("#"+id+'_sin_resultados',tabla).show();
        else $("#"+id+'_sin_resultados',tabla).hide();

        //Quitamos loading
        $(el).parent().addClass('form-search-normal').removeClass('form-search-loading');
      }
    }

  },300);

  var returnn = $('<div/>');

  //Agregamosla busqueda
  if(config.campobusqueda) returnn.append(busqueda);

  //Agregamos la tabla
  returnn.append(tabla);

  //Retornamos la tabla
  return returnn;
}

//Ultima vez que ejecutamos un evento
var delay = (function(){
  var timer = 0;
  return function(callback, ms){
    clearTimeout (timer);
    timer = setTimeout(callback, ms);
  };
})();

//Obtener todo el HTmL de un objeto jquery
jQuery.fn.outerHTML = function() {
   return (this[0]) ? this[0].outerHTML : '';  
};



//Genera un boton!
$.fn.boton = function(config){
  /*
  $(boton).boton({
    claseActivo: '',
    claseInactivo: '',
    valueActivo: '',
    valueInactivo: '',
    valueDefault: '',
    labelActivo: function(el){},
    labelInactivo: function(el){}
  });
  */

  config.update = function(el,value){
    
    //cofiguracion
    var config = $(el).data('config');
    var value_actual = $(el).val();
    var value_final = false;

    //Si no nos pasan el valor
    //Cambiamos el valor actual
    //Por el otro valor
    if(typeof value === 'undefined'){
      if(value_actual == config.valueActivo)
        value_final = config.valueInactivo;
      if(value_actual == config.valueInactivo)
        value_final = config.valueActivo;
    
    //Si nos estan pasando el valor
    //por parametro revisamos que sea
    //Uno de los valores permitidos
    }else{
      if((value == config.valueActivo) || (value == config.valueInactivo))
        value_final = value;
    }


    //Si el valor final es Activo...
    if(value_final == config.valueActivo){
        //Cambaimos el valor
        $(el).val(config.valueActivo);
        //Cambiamos la clase
        $(el).removeClass(config.claseInactivo);
        $(el).addClass(config.claseActivo);
        //Ejecutam os activo
        config.labelActivo($(el));
    }

    //Si el valor final es Inactivo...
    if(value_final == config.valueInactivo){
      //Cambiamos el valor
      $(el).val(config.valueInactivo);
      //Cambiamos la clase si es diferente
      $(el).removeClass(config.claseActivo);
      $(el).addClass(config.claseInactivo);
      //Ejecutam os activo
      config.labelInactivo($(el));
    }
  }

  //Tipo checkbox
  $(this).data('input_type','checkbox');

  //si no tiene ningun valor asignado
  //asignamos el por defecto
  var default_val = config.valueDefault;

  //Si tiene un valor asignado...
  if($(this).val().length > 0)
    default_val = $(this).val();

  //Guardamos la config en el boton
  $(this).data('config',config);

  //Click
  $(this).on('click', function(e){

    console.log('click');

    console.log($(this).data('stop'));

    //Si paramos antes...
    if( (typeof( $(this).data('stop') ) != 'undefined') && $(this).data('stop') ){
      console.log('Frena y no hace nada');
      return;
    }

    console.log('sigue');


    var config = $(this).data('config');
    config.update(this);
    e.stopPropagation();
  });

  //Inicializamos todo...
  config.update(this,default_val);
};

//Le pasamos la configuracion del campo
//Genera el campo, su label le asigna los
//eventos necesarios y lo retorna.
function genera_campo(campo,modulo,agrega){
    var campo_final = $('<div></div>');
    if( (campo.mostrarEditar && !agrega) || (campo.mostrarCrear && agrega) ){
      

      //Generamos el codigo HTML del campo 
      //dependiendo que tipo sea

      //Si el campo es un input
      if(campo.tipo == 'input'){
        campo_final.append('<input type="text" id="'+modulo +'_'+ campo.id +'" class="form-control" data-input_type="input" style="width: 100%; margin-bottom: 10px;" placeholder="'+ campo.label +'"/>');
      }

      //Si el campo es un textarea
      if(campo.tipo == 'textarea'){
        campo_final.append('<textarea id="'+modulo +'_'+ campo.id +'" class="form-control" data-input_type="textarea" style="width: 100%; margin-bottom: 10px; height: 100px;" placeholder="'+ campo.label +'"></textarea>');
      }

      //Si el campo es un boton/checkbox
      if(campo.tipo == 'checkbox'){
        campo_final.append('<button class="btn" id="'+modulo +'_'+ campo.id +'" style="margin-bottom: 10px; width: 100%;" data-input_type="checkbox"></button>');
        $('button',campo_final).boton(campo.config);
      }

      //Si el campo es un select2
      if(campo.tipo == 'select2'){
        campo_final.append('<input type="text" id="'+modulo+'_'+campo.id+'" style="width: 100%; margin-bottom: 10px;" data-input_type="select2" placeholder="'+ campo.label +'"/>');
      }

      //si el campo es un datepicker
      if(campo.tipo == 'daterange'){
        campo_final.append('<div class="input-daterange" data-input_type="daterange"><div><input id="'+modulo+'_'+campo.id+'_desde" class="form-control" placeholder="'+campo.labelDesde+'"/><span class="add-on"><i class="fa fa-calendar"></i></span><input id="'+modulo+'_'+campo.id+'_hasta" class="form-control" placeholder="'+campo.labelHasta+'"/></div></div>');
      }

      //Si el campo es un input
      if(campo.tipo == 'date'){
        campo_final.append('<input type="text" id="'+modulo +'_'+ campo.id +'" class="form-control" data-input_type="date" style="width: 100%; margin-bottom: 10px;" placeholder="'+ campo.label +'"/>');
      }

      
      //Guardamos la configuracion del campo
      $(':first-child',campo_final).data('input_config',campo);
      
      //Le damos todas los eventos al input
      //y ademas obtenemos el label de nuevo
      campo_final.prepend( campo_final.floatLabel() );

      //Instanciamos los select2
      if(campo.tipo == 'select2'){
        //Instanciamos
        $('#'+modulo+'_'+campo.id,campo_final).select2(campo.config)
        .on("change", function(e) {
          var val = $(this).select2('data');
          if(val == null) $(this).prev().prev().hide(200);
          else $(this).prev().prev().show(200);
          //editar a partir de aquí

        }).on("select2-removed", function(e){
        }).on("select2-focus", function(e){
            $(this).prev().prev().css('color','#66afe9');
        }).on("select2-blur", function(e){
          $(this).prev().prev().css('color','black');
        });
      }

      //Inicializamos datepicker
      if(campo.tipo == 'daterange'){

        $('.input-daterange',campo_final).datepicker(campo.config)
        .on('changeDate', function(e){
          $(this).data('update_method')($(this));
        }).on('clearDate', function(e){
          $(this).data('update_method')($(this));
        }).on('hide', function(e){
          $(this).data('update_method')($(this));
        });

        //Inicializamos
        $('.input-daterange',campo_final).data('update_method')( $('.input-daterange',campo_final) );
      }

      //Inicializamos datepicker
      if(campo.tipo == 'date'){

        $('input',campo_final).datepicker(campo.config)
        .on('changeDate', function(e){
          $(this).data('update_method')($(this));
        }).on('clearDate', function(e){
          $(this).data('update_method')($(this));
        }).on('hide', function(e){
          $(this).data('update_method')($(this));
        });

        //Inicializamos
        $('input',campo_final).data('update_method')( $('input',campo_final) );
      }

      //devolvemos
      return campo_final.children();
    }
}

//Convierte un Array en Objeto
function toObject(arr){
  if(!$.isArray(arr))
    return arr;

  //convert
  var rv = {};
  for (var i = 0; i < arr.length; ++i)
    if (arr[i] !== undefined) rv[i] = arr[i];
  return rv;
}

//Length de un objeto
function objectLength(obj){
  var L=0;
  $.each(obj, function(i, elem) {
      L++;
  });
  return L;
};

//Reportes
function reporte(items){
  //Reporte
  var reporte = $('<table class="reporte"><tbody></tbody></table>');


  //Header
  var head = $('<tr/>')
  for(var k in items.header)
    head.append('<td>'+items.header[k].label+'</td>');
  
  //Agregamos el head
  reporte.find('tbody').append(head);

  for(var k in items.items){
    var tr = $('<tr/>');
    for(var x in items.items[k]){
      if(x == 'comentario') tr.append('<td style="white-space: normal">'+items.items[k][x]+'</td>');
      else tr.append('<td>'+items.items[k][x]+'</td>');
    }

    //Agregamos el item
    reporte.find('tbody').append(tr);
  }

  return reporte;
}

//Primera letra en mayuscula
function capitalise(string) {
    return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
}


function removeDiacritics(str){

  var defaultDiacriticsRemovalMap = [
    {'base':'A', 'letters':/[\u0041\u24B6\uFF21\u00C0\u00C1\u00C2\u1EA6\u1EA4\u1EAA\u1EA8\u00C3\u0100\u0102\u1EB0\u1EAE\u1EB4\u1EB2\u0226\u01E0\u00C4\u01DE\u1EA2\u00C5\u01FA\u01CD\u0200\u0202\u1EA0\u1EAC\u1EB6\u1E00\u0104\u023A\u2C6F]/g},
    {'base':'AA','letters':/[\uA732]/g},
    {'base':'AE','letters':/[\u00C6\u01FC\u01E2]/g},
    {'base':'AO','letters':/[\uA734]/g},
    {'base':'AU','letters':/[\uA736]/g},
    {'base':'AV','letters':/[\uA738\uA73A]/g},
    {'base':'AY','letters':/[\uA73C]/g},
    {'base':'B', 'letters':/[\u0042\u24B7\uFF22\u1E02\u1E04\u1E06\u0243\u0182\u0181]/g},
    {'base':'C', 'letters':/[\u0043\u24B8\uFF23\u0106\u0108\u010A\u010C\u00C7\u1E08\u0187\u023B\uA73E]/g},
    {'base':'D', 'letters':/[\u0044\u24B9\uFF24\u1E0A\u010E\u1E0C\u1E10\u1E12\u1E0E\u0110\u018B\u018A\u0189\uA779]/g},
    {'base':'DZ','letters':/[\u01F1\u01C4]/g},
    {'base':'Dz','letters':/[\u01F2\u01C5]/g},
    {'base':'E', 'letters':/[\u0045\u24BA\uFF25\u00C8\u00C9\u00CA\u1EC0\u1EBE\u1EC4\u1EC2\u1EBC\u0112\u1E14\u1E16\u0114\u0116\u00CB\u1EBA\u011A\u0204\u0206\u1EB8\u1EC6\u0228\u1E1C\u0118\u1E18\u1E1A\u0190\u018E]/g},
    {'base':'F', 'letters':/[\u0046\u24BB\uFF26\u1E1E\u0191\uA77B]/g},
    {'base':'G', 'letters':/[\u0047\u24BC\uFF27\u01F4\u011C\u1E20\u011E\u0120\u01E6\u0122\u01E4\u0193\uA7A0\uA77D\uA77E]/g},
    {'base':'H', 'letters':/[\u0048\u24BD\uFF28\u0124\u1E22\u1E26\u021E\u1E24\u1E28\u1E2A\u0126\u2C67\u2C75\uA78D]/g},
    {'base':'I', 'letters':/[\u0049\u24BE\uFF29\u00CC\u00CD\u00CE\u0128\u012A\u012C\u0130\u00CF\u1E2E\u1EC8\u01CF\u0208\u020A\u1ECA\u012E\u1E2C\u0197]/g},
    {'base':'J', 'letters':/[\u004A\u24BF\uFF2A\u0134\u0248]/g},
    {'base':'K', 'letters':/[\u004B\u24C0\uFF2B\u1E30\u01E8\u1E32\u0136\u1E34\u0198\u2C69\uA740\uA742\uA744\uA7A2]/g},
    {'base':'L', 'letters':/[\u004C\u24C1\uFF2C\u013F\u0139\u013D\u1E36\u1E38\u013B\u1E3C\u1E3A\u0141\u023D\u2C62\u2C60\uA748\uA746\uA780]/g},
    {'base':'LJ','letters':/[\u01C7]/g},
    {'base':'Lj','letters':/[\u01C8]/g},
    {'base':'M', 'letters':/[\u004D\u24C2\uFF2D\u1E3E\u1E40\u1E42\u2C6E\u019C]/g},
    {'base':'N', 'letters':/[\u004E\u24C3\uFF2E\u01F8\u0143\u00D1\u1E44\u0147\u1E46\u0145\u1E4A\u1E48\u0220\u019D\uA790\uA7A4]/g},
    {'base':'NJ','letters':/[\u01CA]/g},
    {'base':'Nj','letters':/[\u01CB]/g},
    {'base':'O', 'letters':/[\u004F\u24C4\uFF2F\u00D2\u00D3\u00D4\u1ED2\u1ED0\u1ED6\u1ED4\u00D5\u1E4C\u022C\u1E4E\u014C\u1E50\u1E52\u014E\u022E\u0230\u00D6\u022A\u1ECE\u0150\u01D1\u020C\u020E\u01A0\u1EDC\u1EDA\u1EE0\u1EDE\u1EE2\u1ECC\u1ED8\u01EA\u01EC\u00D8\u01FE\u0186\u019F\uA74A\uA74C]/g},
    {'base':'OI','letters':/[\u01A2]/g},
    {'base':'OO','letters':/[\uA74E]/g},
    {'base':'OU','letters':/[\u0222]/g},
    {'base':'P', 'letters':/[\u0050\u24C5\uFF30\u1E54\u1E56\u01A4\u2C63\uA750\uA752\uA754]/g},
    {'base':'Q', 'letters':/[\u0051\u24C6\uFF31\uA756\uA758\u024A]/g},
    {'base':'R', 'letters':/[\u0052\u24C7\uFF32\u0154\u1E58\u0158\u0210\u0212\u1E5A\u1E5C\u0156\u1E5E\u024C\u2C64\uA75A\uA7A6\uA782]/g},
    {'base':'S', 'letters':/[\u0053\u24C8\uFF33\u1E9E\u015A\u1E64\u015C\u1E60\u0160\u1E66\u1E62\u1E68\u0218\u015E\u2C7E\uA7A8\uA784]/g},
    {'base':'T', 'letters':/[\u0054\u24C9\uFF34\u1E6A\u0164\u1E6C\u021A\u0162\u1E70\u1E6E\u0166\u01AC\u01AE\u023E\uA786]/g},
    {'base':'TZ','letters':/[\uA728]/g},
    {'base':'U', 'letters':/[\u0055\u24CA\uFF35\u00D9\u00DA\u00DB\u0168\u1E78\u016A\u1E7A\u016C\u00DC\u01DB\u01D7\u01D5\u01D9\u1EE6\u016E\u0170\u01D3\u0214\u0216\u01AF\u1EEA\u1EE8\u1EEE\u1EEC\u1EF0\u1EE4\u1E72\u0172\u1E76\u1E74\u0244]/g},
    {'base':'V', 'letters':/[\u0056\u24CB\uFF36\u1E7C\u1E7E\u01B2\uA75E\u0245]/g},
    {'base':'VY','letters':/[\uA760]/g},
    {'base':'W', 'letters':/[\u0057\u24CC\uFF37\u1E80\u1E82\u0174\u1E86\u1E84\u1E88\u2C72]/g},
    {'base':'X', 'letters':/[\u0058\u24CD\uFF38\u1E8A\u1E8C]/g},
    {'base':'Y', 'letters':/[\u0059\u24CE\uFF39\u1EF2\u00DD\u0176\u1EF8\u0232\u1E8E\u0178\u1EF6\u1EF4\u01B3\u024E\u1EFE]/g},
    {'base':'Z', 'letters':/[\u005A\u24CF\uFF3A\u0179\u1E90\u017B\u017D\u1E92\u1E94\u01B5\u0224\u2C7F\u2C6B\uA762]/g},
    {'base':'a', 'letters':/[\u0061\u24D0\uFF41\u1E9A\u00E0\u00E1\u00E2\u1EA7\u1EA5\u1EAB\u1EA9\u00E3\u0101\u0103\u1EB1\u1EAF\u1EB5\u1EB3\u0227\u01E1\u00E4\u01DF\u1EA3\u00E5\u01FB\u01CE\u0201\u0203\u1EA1\u1EAD\u1EB7\u1E01\u0105\u2C65\u0250]/g},
    {'base':'aa','letters':/[\uA733]/g},
    {'base':'ae','letters':/[\u00E6\u01FD\u01E3]/g},
    {'base':'ao','letters':/[\uA735]/g},
    {'base':'au','letters':/[\uA737]/g},
    {'base':'av','letters':/[\uA739\uA73B]/g},
    {'base':'ay','letters':/[\uA73D]/g},
    {'base':'b', 'letters':/[\u0062\u24D1\uFF42\u1E03\u1E05\u1E07\u0180\u0183\u0253]/g},
    {'base':'c', 'letters':/[\u0063\u24D2\uFF43\u0107\u0109\u010B\u010D\u00E7\u1E09\u0188\u023C\uA73F\u2184]/g},
    {'base':'d', 'letters':/[\u0064\u24D3\uFF44\u1E0B\u010F\u1E0D\u1E11\u1E13\u1E0F\u0111\u018C\u0256\u0257\uA77A]/g},
    {'base':'dz','letters':/[\u01F3\u01C6]/g},
    {'base':'e', 'letters':/[\u0065\u24D4\uFF45\u00E8\u00E9\u00EA\u1EC1\u1EBF\u1EC5\u1EC3\u1EBD\u0113\u1E15\u1E17\u0115\u0117\u00EB\u1EBB\u011B\u0205\u0207\u1EB9\u1EC7\u0229\u1E1D\u0119\u1E19\u1E1B\u0247\u025B\u01DD]/g},
    {'base':'f', 'letters':/[\u0066\u24D5\uFF46\u1E1F\u0192\uA77C]/g},
    {'base':'g', 'letters':/[\u0067\u24D6\uFF47\u01F5\u011D\u1E21\u011F\u0121\u01E7\u0123\u01E5\u0260\uA7A1\u1D79\uA77F]/g},
    {'base':'h', 'letters':/[\u0068\u24D7\uFF48\u0125\u1E23\u1E27\u021F\u1E25\u1E29\u1E2B\u1E96\u0127\u2C68\u2C76\u0265]/g},
    {'base':'hv','letters':/[\u0195]/g},
    {'base':'i', 'letters':/[\u0069\u24D8\uFF49\u00EC\u00ED\u00EE\u0129\u012B\u012D\u00EF\u1E2F\u1EC9\u01D0\u0209\u020B\u1ECB\u012F\u1E2D\u0268\u0131]/g},
    {'base':'j', 'letters':/[\u006A\u24D9\uFF4A\u0135\u01F0\u0249]/g},
    {'base':'k', 'letters':/[\u006B\u24DA\uFF4B\u1E31\u01E9\u1E33\u0137\u1E35\u0199\u2C6A\uA741\uA743\uA745\uA7A3]/g},
    {'base':'l', 'letters':/[\u006C\u24DB\uFF4C\u0140\u013A\u013E\u1E37\u1E39\u013C\u1E3D\u1E3B\u017F\u0142\u019A\u026B\u2C61\uA749\uA781\uA747]/g},
    {'base':'lj','letters':/[\u01C9]/g},
    {'base':'m', 'letters':/[\u006D\u24DC\uFF4D\u1E3F\u1E41\u1E43\u0271\u026F]/g},
    {'base':'n', 'letters':/[\u006E\u24DD\uFF4E\u01F9\u0144\u00F1\u1E45\u0148\u1E47\u0146\u1E4B\u1E49\u019E\u0272\u0149\uA791\uA7A5]/g},
    {'base':'nj','letters':/[\u01CC]/g},
    {'base':'o', 'letters':/[\u006F\u24DE\uFF4F\u00F2\u00F3\u00F4\u1ED3\u1ED1\u1ED7\u1ED5\u00F5\u1E4D\u022D\u1E4F\u014D\u1E51\u1E53\u014F\u022F\u0231\u00F6\u022B\u1ECF\u0151\u01D2\u020D\u020F\u01A1\u1EDD\u1EDB\u1EE1\u1EDF\u1EE3\u1ECD\u1ED9\u01EB\u01ED\u00F8\u01FF\u0254\uA74B\uA74D\u0275]/g},
    {'base':'oi','letters':/[\u01A3]/g},
    {'base':'ou','letters':/[\u0223]/g},
    {'base':'oo','letters':/[\uA74F]/g},
    {'base':'p','letters':/[\u0070\u24DF\uFF50\u1E55\u1E57\u01A5\u1D7D\uA751\uA753\uA755]/g},
    {'base':'q','letters':/[\u0071\u24E0\uFF51\u024B\uA757\uA759]/g},
    {'base':'r','letters':/[\u0072\u24E1\uFF52\u0155\u1E59\u0159\u0211\u0213\u1E5B\u1E5D\u0157\u1E5F\u024D\u027D\uA75B\uA7A7\uA783]/g},
    {'base':'s','letters':/[\u0073\u24E2\uFF53\u00DF\u015B\u1E65\u015D\u1E61\u0161\u1E67\u1E63\u1E69\u0219\u015F\u023F\uA7A9\uA785\u1E9B]/g},
    {'base':'t','letters':/[\u0074\u24E3\uFF54\u1E6B\u1E97\u0165\u1E6D\u021B\u0163\u1E71\u1E6F\u0167\u01AD\u0288\u2C66\uA787]/g},
    {'base':'tz','letters':/[\uA729]/g},
    {'base':'u','letters':/[\u0075\u24E4\uFF55\u00F9\u00FA\u00FB\u0169\u1E79\u016B\u1E7B\u016D\u00FC\u01DC\u01D8\u01D6\u01DA\u1EE7\u016F\u0171\u01D4\u0215\u0217\u01B0\u1EEB\u1EE9\u1EEF\u1EED\u1EF1\u1EE5\u1E73\u0173\u1E77\u1E75\u0289]/g},
    {'base':'v','letters':/[\u0076\u24E5\uFF56\u1E7D\u1E7F\u028B\uA75F\u028C]/g},
    {'base':'vy','letters':/[\uA761]/g},
    {'base':'w','letters':/[\u0077\u24E6\uFF57\u1E81\u1E83\u0175\u1E87\u1E85\u1E98\u1E89\u2C73]/g},
    {'base':'x','letters':/[\u0078\u24E7\uFF58\u1E8B\u1E8D]/g},
    {'base':'y','letters':/[\u0079\u24E8\uFF59\u1EF3\u00FD\u0177\u1EF9\u0233\u1E8F\u00FF\u1EF7\u1E99\u1EF5\u01B4\u024F\u1EFF]/g},
    {'base':'z','letters':/[\u007A\u24E9\uFF5A\u017A\u1E91\u017C\u017E\u1E93\u1E95\u01B6\u0225\u0240\u2C6C\uA763]/g}
  ];

  for(var i=0; i<defaultDiacriticsRemovalMap.length; i++) {
    str = str.replace(defaultDiacriticsRemovalMap[i].letters, defaultDiacriticsRemovalMap[i].base);
  }

  return str;
}


var delay = (function(){
  var timer = 0;
  return function(callback, ms){
    clearTimeout (timer);
    timer = setTimeout(callback, ms);
  };
})();

function formatSecondsAsTime(secs, format) {
  secs = parseInt(secs);
  var hr  = Math.floor(secs / 3600);
  var min = Math.floor((secs - (hr * 3600))/60);
  var sec = Math.floor(secs - (hr * 3600) -  (min * 60));

  //if (min < 10) min = "0" + min; 
  if (sec < 10) sec  = "0" + sec;

  return min + ':' + sec;
}

function formatBytes(bytes,decimals) {
   if(bytes == 0) return '0 Bytes';
   var k = 1000,
       dm = decimals + 1 || 3,
       sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'],
       i = Math.floor(Math.log(bytes) / Math.log(k));
   return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
}

/* GENERADOR DE ID UNICO */
function guid() {
  return s4() + s4() + s4() + s4() +
    s4() + s4() + s4() + s4();
}

function s4() {
  return Math.floor((1 + Math.random()) * 0x10000)
    .toString(16)
    .substring(1);
}

function pad(num, size) {
    var s = num+"";
    while (s.length < size) s = "0" + s;
    return s;
}