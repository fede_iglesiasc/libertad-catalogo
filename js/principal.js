
/* TABLA DE RESULTADOS */
var resultados;
var debug = 1;

 



//BUSCAR POR ...
function buscar(tipo) {

	$('#loading').show();

	//BUSQUEDA POR APLICACIONES
	if (tipo == 'aplicacion') {


		//JUNTAMOS TODAS LAS VARIABLES Y HACEMOS UN LLAMADO AJAX
		waTable2.update(function(){ 

			//OCULTAMOS AL TERMINAR...
			$('#loading').fadeOut();
			
			//RECOLOCAMOS LA TABLA ...
			$('#listado_por_aplicacion table').css('margin-left', ($('#listado_por_aplicacion table').width() - $('#listado_por_aplicacion').width()) /2 *-1 +'px');
			
		});


	//BUSQUEDA POR DIMENSIONES
	} else if (tipo == 'dimension') {

		//Generamos parametros
		var params = {
			mode: "resultado_dim"
		};

		//Update de la tabla
		waTable.update(function(){ 
			//Reacomodamos la tabla ...
			$('#loading').fadeOut();
		});

 
	} else if (tipo == 'codigo') {

		//Eligieron busqueda por marca?
		var id_marca = 'null';
		if($("#busxcod_usa_marca").is(':checked')){
			id_marca = $('#s2id_busxcod_marca').select2('val');
		}

		//JUNTAMOS TODAS LAS VARIABLES Y HACEMOS UN LLAMADO AJAX
		waTable3.option("urlData", {mode: 'resultado_cod', 
								prefijo: $('#busxcod_prefijo').val(), 
								codigo: $('#busxcod_codigo').val(), 
								sufijo: $('#busxcod_sufijo').val(), 
								id_marca: id_marca
		});


		waTable3.update(function(){ 
			//Reacomodamos la tabla ...
			$('#listado_por_codigo table').css('margin-left', ($('#listado_por_codigo table').width() - $('#listado_por_codigo').width()) /2 *-1 +'px');
 

			$('#loading').fadeOut();
		});



	}

}




var rubros_variables = [];
var pre_selected_rub = '';

/* GENERA OPCIONES EN FORM 'BUSQUEDA POR APLICACIONES' */
function generaForm_dim(updatetable) {

	if(typeof(updatetable)==='undefined') updatetable = 0;

	$('#loading').show();

	//Generamos parametros
	var params = {
		mode: "busqueda_dim",
		marca: $('#busxdim_marca').val(),
		rubro: $('#busxdim_rubro').val(),
		app_marca: $('#busxdim_app_marca').val(),
		app_modelo: $('#busxdim_app_modelo').val()
	};

	//Generamos extra params
	for (var i = 0; i < rubros_variables.length; i++) {
		params['extra_' + rubros_variables[i]] = $('#extra_' + rubros_variables[i]).val();
	}
	

	//Enviamos las variables existentes
	$.ajax({
		type: "POST",
		dataType: "json",
		url: "sandbox.php",
		data: params,
		cache: false,
		success: function(data) {

			var resultado = data;


			//Cargamos las marcas
			$('#busxdim_marca').select2( {data: resultado['marcas'] } );
			//Seleccionamos valor
			$('#busxdim_marca').select2('val', resultado['marca_selected']);


			//Cargamos las rubros
			$('#busxdim_rubro').select2( {data: resultado['rubros'] } );
			//Seleccionamos valor
			$('#busxdim_rubro').select2('val', resultado['rubro_selected']);


			//Cargamos las app_marcas
			$('#busxdim_app_marca').select2( {data: resultado['app_marcas'] } );
			//Seleccionamos valor
			$('#busxdim_app_marca').select2('val', resultado['app_marca_selected']);


			//Cargamos las app_modelos
			$('#busxdim_app_modelo').select2( {data: resultado['app_modelos'] } );
			//Seleccionamos valor
			$('#busxdim_app_modelo').select2('val', resultado['app_modelo_selected']);


			//Hubo un cambio de rubro??
			var cambioRubro = 0;
			if($('#busxdim_rubro').val() != pre_selected_rub)
				cambioRubro = 1;

			//Por cada dimension extra generamos un campo nuevo
			if(cambioRubro){
				$('#container-dimensiones').slideUp(500, function(){
					$('#container-dimensiones-selects').html('');
					muestraVars();
				});
			}else{
				muestraVars();
			}
			

			//Si updateamos la tabla y luego quitamos luego el loader
			if(updatetable) { 
						
				waTable.update(function(){ 

					$('#loading').fadeOut();
				});
				
			//escondemos el indicador de carga
			} else { 
				$('#loading').fadeOut(); 
			}

			//Mustra variables
			function muestraVars(){

				//Vaciamos el arreglo de variables
				if(cambioRubro)
					rubros_variables = [];

				for (var i = 0; i < resultado['vars'].length; i++) {
					
					if(cambioRubro){
						var temp = '<div class="container-medida"><h2 class="select-label">'+resultado['vars'][i]['nombre']+'</h2><input id="extra_'+resultado['vars'][i]['id']+'" value="'+resultado['vars'][i]['valor']+'" style="width:100%;"/></div>';
						$('#container-dimensiones-selects').append(temp);
					}

					$('#'+'extra_'+resultado['vars'][i]['id']).select2( {data: resultado['vars'][i]['valores'] } );
					$('#'+'extra_'+resultado['vars'][i]['id']).select2('val', resultado['vars'][i]['valor']);
					

					if(cambioRubro)
						$("#extra_"+resultado['vars'][i]['id']).on("change", function(){
							generaForm_dim();
						});
       				


					//Acumulamos el id de la dimension
					if(cambioRubro) rubros_variables.push(resultado['vars'][i]['id']);
				}

				//Mostramos las dimensiones
				if( (resultado['vars'].length > 0) && cambioRubro) 
					$('#container-dimensiones').slideDown(500);

				//Rubro seleccionado
				pre_selected_rub = $('#busxdim_rubro').val();
			}
		}
	});
}

/* LIMPIA CAMPOS DEL FORMULARIO DE BUSQUEDA POR CODIGO */
function limpiaForm_cod(){

	$('#loading').show();

	//Vaciamos los imputs
	$('#busxcod_prefijo').val('');
	$('#busxcod_codigo').val('');
	$('#busxcod_sufijo').val('');

	//Ocultamos las marcas
	$('#s2id_busxcod_marca').hide();
	$("#busxcod_prefijo").show();

	//Quitamos el check
	$('#busxcod_usa_marca').attr('checked', false);

	//Eligieron busqueda por marca?
	var id_marca = 'null';
	if($("#busxcod_usa_marca").is(':checked')){
		id_marca = $('#s2id_busxcod_marca').select2('val');
	}

	//JUNTAMOS TODAS LAS VARIABLES Y HACEMOS UN LLAMADO AJAX
	waTable3.option("urlData", {mode: 'resultado_cod', 
							prefijo: $('#busxcod_prefijo').val(), 
							codigo: $('#busxcod_codigo').val(), 
							sufijo: $('#busxcod_sufijo').val(), 
							id_marca: id_marca
	});


	//Vaciamos la tabla...
	waTable3.update(function(){ 
			$('#loading').fadeOut();
			//Reacomodamos la tabla ...
			$('#listado_por_codigo table').css('margin-left', ($('#listado_por_codigo table').width() - $('#listado_por_codigo').width()) /2 *-1 +'px');
	});
}

/* LIMPIA CAMPOS DEL FORMULARIO DE BUSQUEDA POR DIMENSIONES */
function limpiaForm_dim(){
	//Seteamos valores a 'Todos'
	$('#busxdim_marca').select2('val', '-1');
	$('#busxdim_rubro').select2('val', '-1');
	$('#busxdim_app_marca').select2('val', '-1');
	$('#busxdim_ap_modelo').select2('val', '-1');
	

	//Recreamos el form
	generaForm_dim(1);
}

/* GENERA OPCIONES EN FORM 'BUSQUEDA POR APLICACIONES' */
function generaForm_app(updatetable) {

	if(typeof(updatetable)==='undefined') updatetable = 0;

	$('#loading').show();

	//Generamos parametros
	var params = {
		mode: "busqueda_app",
		marca: $('#busxapp_marca').val(),
		rubro: $('#busxapp_rubro').val(),
		app_marca: $('#busxapp_app_marca').val(),
		app_modelo: $('#busxapp_app_modelo').val()
	};

	//Enviamos las variables existentes
	$.ajax({
		type: "POST",
		dataType: "json",
		url: "sandbox.php",
		data: params,
		cache: false,
		success: function(data) {

			var resultado = data;

			//Cargamos las marcas
			$('#busxapp_marca').select2( {data: resultado['marcas'] } );
			//Seleccionamos valor
			$('#busxapp_marca').select2('val', resultado['marca_selected']);


			//Cargamos las rubros
			$('#busxapp_rubro').select2( {data: resultado['rubros'] } );
			//Seleccionamos valor
			$('#busxapp_rubro').select2('val', resultado['rubro_selected']);


			//Cargamos las app_marcas
			$('#busxapp_app_marca').select2( {data: resultado['app_marcas'] } );
			//Seleccionamos valor
			$('#busxapp_app_marca').select2('val', resultado['app_marca_selected']);


			//Cargamos las app_modelos
			$('#busxapp_app_modelo').select2( {data: resultado['app_modelos'] } );
			//Seleccionamos valor
			$('#busxapp_app_modelo').select2('val', resultado['app_modelo_selected']);

			//Si updateamos la tabla y luego quitamos luego el loader
			if(updatetable) { 
				waTable2.update(function(){ 

					$('#loading').fadeOut();
				});

			//escondemos el indicador de carga
			} else { 
				$('#loading').fadeOut(); 
			}
 
		}
	});
}
/* LIMPIA CAMPOS DEL FORMULARIO DE BUSQUEDA POR APLICACIONES */
function limpiaForm_app(){
	//Seteamos valores a 'Todos'
	$('#busxapp_marca').select2('val', '-1');
	$('#busxapp_rubro').select2('val', '-1');
	$('#busxapp_app_marca').select2('val', '-1');
	$('#busxapp_app_modelo').select2('val', '-1');
	

	//Recreamos el form
	generaForm_app(1);
}

/* OBTIENE PARAMETROS POR URI */
function getParameterByName(name) {
  name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
  var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
      results = regex.exec(location.search);
  return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}


var busxcod_pref = '';
var busxcod_codigo = '';
var busxcod_suf = '';
var busxcod_marca = 'null';





/* DISPARADOR AL CAMBIA DE TAMAÑO LA VENTANA*/
var resizeTimer;
$(window).resize(function() {
    clearTimeout(resizeTimer);
    resizeTimer = setTimeout(handlerWindowResize, 0);
});

/* ACCIONES AL CAMBIAR DE TAMAÑO LA VENTANA*/
function handlerWindowResize(){
	/* Cambiamos el tamaño de la busqueda */
	$('#busqueda_opciones').height( $(window).height() - 250);

	/* Redimensionamos cabecera de tabla flotante */
	recalculaHeader();
}

/* RECALCULA HEADER */
function recalculaHeader(){
	/* Redimensionamos cabecera de tabla flotante */
	$('#resultados_table thead tr th').each(function(index){
		var celwidth = $(this).outerWidth(true);
		if( index == 0 ) celwidth += 2;

		$('#table_head').width( $('#resultados_table').width() + 1 );
		$('#table_head').find('a:nth-child('+(index+1)+')').width( celwidth + 'px');
		$('#table_head').css({'left': $('#resultados_table').offset().left + 'px'});
	});
}




/* INICIALIZADOR DE LA BUSQUEDA RAPIDA */
function eventsQuickSearch(){
	//Necesario para renderizar el typeahead
	_.compile = function(templ) {
	    var compiled = this.template(templ);
	    compiled.render = function(ctx) {
	       return this(ctx);
	    }
	    return compiled;
	}

	//Necesario para el Typeahead
	$('#busqueda_general').typeahead({
	    name: 'productos',
	    limit: 10,
	    valueKey: 'value',
	    remote: {
	        url: './quick_search.php?q=%QUERY'
	    },
	    engine: _,
	    template: '<div class="tt-suggestion-cell-codigo"><%= codigo_completo %></div><div class="tt-suggestion-cell-descripcion"><%= value %></div><div class="tt-suggestion-cell-precio">$ <%= precio %></div>'
	});

	//Cuando seleccionamos un elemento de la busqueda
	$('#busqueda_general').on('typeahead:selected', function(event, datum) {
	  //Obtenemos partes del codigo
	  var codigo_completo = datum.codigo_completo.split('-');
	  if(codigo_completo.length == 3){
	      //Hacemos el llamado ajax...
	      resultados.option("urlData", {mode: 'resultado_cod', 
	                  prefijo: codigo_completo[0], 
	                  codigo: codigo_completo[1], 
	                  sufijo: codigo_completo[2], 
	                  id_marca: ''
	      });

	      resultados.update(function(){
	        //alert(resultados._data);
	        $('#procesando').fadeOut('fast', function(){
	          //$('#sin_resultados').show('slow');
	        });
	      });
	  }
	});

  //Cambiamos el fondo a blanco
  $('#busqueda_general').css('background','white');
}

/* INICIALIZADORES DE LA BUSQUEDA */
function eventsFormBusqueda(){

	//Cuando hacemos foco en un input se autoselecciona el texto
	$("input[type=text]").focus(function(){
	    this.select();
	});

	/* Click en boton Limpiar */
	$('#busqueda_boton_limpiar').click(function(){
		//Obtenemos boton activo
		var active = '';

		//Obtenemos id de boton activo
		$('#busqueda_botonera>a').each(function(){ if($(this).hasClass('busqueda_botonera_active')) active = $(this).attr('id'); });

		//Dependiendo que tipo de busqueda está activa derivamos acción
		if(active == 'busqueda_botonera_codigo'){
			limpiaBusquedaCodigo();
		}else if(active == 'busqueda_botonera_aplicacion'){
			limpiaBusquedaAplicacion();
		}else if(active == 'busqueda_botonera_dimension'){
			limpiaBusquedaDimension();
		}
	});

	/* Click en boton Buscar */
	$('#busqueda_boton_buscar').click(function(){
		handlerBotonBusqueda();
	});

	/* Enter o TAB en boton Buscar */
	$('#busqueda_boton_buscar').keydown(function(){
		event.preventDefault();
		//Si apretamos 'ENTER' o TAB
		if( (event.keyCode == 13) || (event.keyCode == 9) ){
			//Disparamos la busqueda
			handlerBotonBusqueda();
        }
	});

	/* Click en la botonera de busqueda */
	$('#busqueda_botonera a').click(function(event){
		//Si el boton esta activo, hacemos cosas...
		if( !$(event.target).hasClass('busqueda_botonera_active') ){
			busquedaClickBotonera( '#' + $(event.target).attr('id') );
		}
		//cortamos el bubbling
		event.preventDefault();
	});

	/* Le damos accion al boton "Buscar" de la UI principal*/
	$('#busqueda_button').click(function(event){
		animaBusqueda();
	});

	/* Cerramos la busqueda si apretamos escape */
	$(document).keyup(function(e) {

  		//Escape
  		if (e.keyCode == 27) { animaBusqueda('escape'); }

  		//Barra numpad
  		if (e.keyCode == 46) { animaBusqueda('supr'); }

   		//Asterisco numpad
  		if (e.keyCode == 35) { animaBusqueda('fin'); }

   		//Guion numpad
  		if (e.keyCode == 34) { animaBusqueda('avpag'); }

	});
}

/* INICIALIZADORES DE LA BUSQUEDA POR CODIGO */
function eventsFormBusquedaCodigo(){
	
	var marcas_cod = Array();
	//Cargamos las marcas en el dropdown
	$.ajax({
		type: "POST",
		dataType: "json",
		url: "sandbox.php",
		data: { mode: "busqueda_cod_marcas" },
		cache: false,
		success: function(data) {

			var cont = 0;
			for (var i = 0; i < data.length; i++) {
				if(data[i]['nivel'] == '2'){
					marcas_cod[cont] = Array();
					marcas_cod[cont]['id'] = data[i]['id'];
					marcas_cod[cont]['text'] = data[i]['nombre'];
					cont++;
				}
			}

			//Generamos el dropdown
			$('#busqueda_codigo_fabricante').select2( {data: marcas_cod, placeholder: "Fabricante", openOnEnter: false, formatNoMatches: function(term){ return 'No hay resultados' } } );

			//Cuando apretamos enter seguimos de largo
			$('#s2id_busqueda_codigo_fabricante input').on('keydown',function(e){
    			if(e.keyCode == 13){
    				$('#busqueda_codigo_codigo').focus();
    			}
    		});

		}
	});


	/* Validador para campo Prefijo (Busqueda por codigo) */
	$('#busqueda_codigo_prefijo').keydown(function(event) {
		//alert(event.keyCode);
        // Allow: backspace, delete, tab, escape, enter and .
        if ( $.inArray(event.keyCode,[46,8,9,27,190]) !== -1 ||
             // Allow: Ctrl+A
            (event.keyCode == 65 && event.ctrlKey === true) || 
             // Allow: home, end, left, right
            (event.keyCode >= 35 && event.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;

        }else if(event.keyCode == 13){
				$('#busqueda_codigo_codigo').focus();
        
        } else {

            // Ensure that it is a number and stop the keypress
            if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
                event.preventDefault();

                //Cambiar Foco
                if(event.keyCode == 109){
                	$('#busqueda_codigo_codigo').focus();
                }
            }   
        }
    });

	/* Validador para campo Codigo (Busqueda por codigo) */
	$('#busqueda_codigo_codigo').keydown(function(event) {
		//alert(event.keyCode);
        // Allow: backspace, delete, tab, escape, enter and .
        if ( $.inArray(event.keyCode,[46,9,27,190]) !== -1 ||
             // Allow: Ctrl+A
            (event.keyCode == 65 && event.ctrlKey === true) || 
             // Allow: home, end, left, right
            (event.keyCode >= 35 && event.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;

        } else {
        	 
        	//Si no hay nada mas que borrar hacemos foco en el campo código
        	if( (event.keyCode == 8) ){
        		if($('#busqueda_codigo_codigo').val() == ''){
        			//No borramos mas
        			event.preventDefault();
        			//Si estamos mostrando Fabricantes
        			if( $('#busqueda_codigo_mostrarFabricantesbt').hasClass('active') ) {
        				$('#busqueda_codigo_fabricante').select2('focus');
            		}else{
            			$('#busqueda_codigo_prefijo').focus();
            		}

            	}else{
            		return;
            	}

            }else if(event.keyCode == 13){
				$('#busqueda_codigo_sufijo').focus();
            }else{

	            // Ensure that it is a number and stop the keypress
	            if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
	                event.preventDefault();

	                //Cambiar Foco
	                if(event.keyCode == 109){
	                	$('#busqueda_codigo_sufijo').focus();
	                }
	            }
            }  
        }
    });

	/* Validador para campo Sufijo (Busqueda por codigo) */
	$('#busqueda_codigo_sufijo').keydown(function(event) {
		//alert(event.keyCode);
        // Permitir: delete, tab, escape, enter and .
        if ( $.inArray(event.keyCode,[46,9,27,190]) !== -1 ||
             // Allow: Ctrl+A
            (event.keyCode == 65 && event.ctrlKey === true) || 
             // Allow: home, end, left, right
            (event.keyCode >= 35 && event.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;

        } else {
        	//Si no hay nada mas que borrar hacemos foco en el campo código
        	if( event.keyCode == 8 ){
        		if($('#busqueda_codigo_sufijo').val() == ''){
        			event.preventDefault();
            		$('#busqueda_codigo_codigo').focus();
            	}else{
            		return;
            	}

            }else if( event.keyCode == 13 ){
            	$('#busqueda_codigo_mostrarFabricantesbt').focus();
            	event.preventDefault();

            }else{

            	if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
	                event.preventDefault();

	                //Si es un '-' cambiamos el foco al siguiente campo
	                if(event.keyCode == 109){
	                	$('#busqueda_codigo_mostrarFabricantesbt').focus();
	                }
            	}
           }
        }
    });

	/* Evento boton 'Mostrar Fabricantes' (Busqueda por codigo) */
	$('#busqueda_codigo_mostrarFabricantesbt').click(function(){
		//Si está checked, mostramos los fabricantes y ocultamos el prefijo
		if( !$(this).hasClass('activo') ){
			$(this).addClass('active');
			$(this).html('Ocultar Fabricantes');
			$('#busqueda_codigo_prefijo_container').hide();
			$('#busqueda_codigo_prefijo').val('');
			$('#busqueda_codigo_fabricante_container').show();
			$('#busqueda_codigo_fabricante').select2('focus');
			$(this).addClass('activo');
		}else{
			$(this).removeClass('active');
			$(this).html('Mostrar Fabricantes');
			$('#busqueda_codigo_prefijo_container').show();
			$('#busqueda_codigo_fabricante_container').hide();
			$('#busqueda_codigo_prefijo').focus();
			$(this).removeClass('activo');
		}
	});

	/* Evento boton 'Mostrar Fabricantes' (Busqueda por codigo) */
	$('#busqueda_codigo_mostrarFabricantesbt').keydown(function(event){
		//Si apretamos 'ENTER' o TAB
		if( (event.keyCode == 13) || (event.keyCode == 9) ){
			event.preventDefault();
			//hacemos foco en buscar
			$('#busqueda_boton_buscar').focus();
        }
	});

	/* Evento al seleccionar Fabricante */
	$('#busqueda_codigo_fabricante').on("change", function(e) {
	   	$('#busqueda_codigo_codigo').focus();
	});
}

/* INICIALIZADORES DE LA BUSQUEDA POR APLICACION */
function eventsFormBusquedaAplicacion(){
	formBusquedaAplicacionUpdate('ninguno');
	
	// Cambiamos select vehiculo
	$('#busqueda_aplicacion_vehiculo').on("change", function(e) {
		formBusquedaAplicacionUpdate('rubro');
		$('#busqueda_aplicacion_rubro').select2('focus');
	});

	// Cambiamos select rubro
	$('#busqueda_aplicacion_rubro').on("change", function(e) {
		formBusquedaAplicacionUpdate('fabricante');
		$('#busqueda_aplicacion_fabricante').select2('focus');
	});

	// Cambiamos select fabricante
	$('#busqueda_aplicacion_fabricante').on("change", function(e) {
		formBusquedaAplicacionUpdate('buscar');
		$('#busqueda_button').focus();
	});
	
}

/* UPDATEA CAMPOS EN BUSQUEDA POR APLICACION */
function formBusquedaAplicacionUpdate(campo){

	//Mostramos 'Spin indicador de procesando'
	$('#busqueda_procesando').fadeIn('fast');

	//Deshabilitamos los campos
	$('#busqueda_aplicacion_vehiculo').select2("readonly", true);
	$('#busqueda_aplicacion_rubro').select2("readonly", true);
	$('#busqueda_aplicacion_fabricante').select2("readonly", true);

	//Obtenemos marca y modelo de vehiculo
	var vehiculo = $('#busqueda_aplicacion_vehiculo').val().split('#');
	if(vehiculo.length == 1) vehiculo[0] = vehiculo[1] = '-1';

	var marca = $('#busqueda_aplicacion_fabricante').val();
	if( marca == '' ) marca = '-1';

	var rubro = $('#busqueda_aplicacion_rubro').val();
	if( rubro == '' ) rubro = '-1';

	//Generamos parametros
	var params = {
		mode: "busqueda_app",
		marca: marca,
		rubro: rubro,
		app_marca: vehiculo[1],
		app_modelo: vehiculo[0]
	};

	//Enviamos las variables existentes
	$.ajax({
		type: "POST",
		dataType: "json",
		url: "sandbox.php",
		data: params,
		cache: false,
		success: function(data) {

			var resultado = data;

			//Cargamos las marcas
			$('#busqueda_aplicacion_fabricante').select2( {data: resultado['marcas'], placeholder: "Tipee el Vehículo...", openOnEnter: false, formatNoMatches: function(term){ return 'No hay resultados' } } );
			//Seleccionamos valor
			$('#busqueda_aplicacion_fabricante').select2('val', resultado['marca_selected']);
			//Cuando apretamos enter seguimos de largo
			$('#s2id_busqueda_aplicacion_fabricante input').on('keydown',function(e){
    			if(e.keyCode == 13){
    				$('#busqueda_boton_buscar').focus();
    			}else if(e.keyCode == 8){
    				//Si tenemos seleccionada una opcion distinta a todos la borramos
    				if( !$('#s2id_busqueda_aplicacion_fabricante').hasClass('select2-dropdown-open') ){
	    				if($('#busqueda_aplicacion_fabricante').val() != '-1'){
	    					$('#busqueda_aplicacion_fabricante').select2('val', '-1');
	    					formBusquedaAplicacionUpdate('fabricante');
	    				}else{
	    					$('#busqueda_aplicacion_rubro').select2('focus');
	    				}
	    			}
    			}
    		});

			//Cargamos las rubros
			$('#busqueda_aplicacion_rubro').select2( {data: resultado['rubros'], placeholder: "Tipee el Rubro...", openOnEnter: false, formatNoMatches: function(term){ return 'No hay resultados' } } );
			//Seleccionamos valor
			$('#busqueda_aplicacion_rubro').select2('val', resultado['rubro_selected']);
			//Cuando apretamos enter seguimos de largo
			$('#s2id_busqueda_aplicacion_rubro input').on('keydown',function(e){
    			if(e.keyCode == 13){
    				$('#busqueda_aplicacion_fabricante').select2('focus');
    			//Si apretamos backspace
    			}else if(e.keyCode == 8){
    				//Si tenemos seleccionada una opcion distinta a todos la borramos
    				if( !$('#s2id_busqueda_aplicacion_rubro').hasClass('select2-dropdown-open') ){
	    				if($('#busqueda_aplicacion_rubro').val() != '-1'){
	    					$('#busqueda_aplicacion_rubro').select2('val', '-1');
	    					formBusquedaAplicacionUpdate('rubro');
	    				//Sino seleccionamos el dropdown superior
	    				}else{
	    					$('#busqueda_aplicacion_vehiculo').select2('focus');
	    				}
	    			}
    			}
    		});

			//Cargamos las app_marcas_modelos
			$('#busqueda_aplicacion_vehiculo').select2( {data: resultado['app_modelos_marcas'], placeholder: "Tipee el Vehículo...", openOnEnter: false, formatNoMatches: function(term){ return 'No hay resultados' } } );
			//Seleccionamos valor
			$('#busqueda_aplicacion_vehiculo').select2('val', resultado['app_modelo_selected'] +'#'+ resultado['app_marca_selected']);
			//Cuando apretamos enter seguimos de largo
			$('#s2id_busqueda_aplicacion_vehiculo input').on('keydown',function(e){
    			if(e.keyCode == 13){
    				$('#busqueda_aplicacion_rubro').select2('focus');
    			}else if(e.keyCode == 8){
    				//Si tenemos seleccionada una opcion distinta a todos la borramos
    				if( !$('#s2id_busqueda_aplicacion_vehiculo').hasClass('select2-dropdown-open') ){
	    				if($('#busqueda_aplicacion_vehiculo').val() != '-1'){
	    					$('#busqueda_aplicacion_vehiculo').select2('val', '-1#-1');
	    					formBusquedaAplicacionUpdate('vehiculo');
	    				//Sino seleccionamos el dropdown superior
	    				}
	    			}
    			}
    		});

		 	//Habilitamos los campos
			$('#busqueda_aplicacion_vehiculo').select2("readonly", false);
			$('#busqueda_aplicacion_rubro').select2("readonly", false);
			$('#busqueda_aplicacion_fabricante').select2("readonly", false);

			//Mostramos los campos por primera vez (estan ocultos)
			$('#busqueda_aplicacion_vehiculo_container').show();
			$('#busqueda_aplicacion_rubro_container').show();
			$('#busqueda_aplicacion_fabricante_container').show();

			//Ocultamos 'Spin indicador de procesando'
			$('#busqueda_procesando').fadeOut('fast');

			//Cambiamos el foco
			if(campo == 'vehiculo')
				$('#busqueda_aplicacion_vehiculo').select2('focus');
			else if(campo == 'rubro')
				$('#busqueda_aplicacion_rubro').select2('focus');
			else if(campo == 'fabricante')
				$('#busqueda_aplicacion_fabricante').select2('focus');
			else if(campo == 'buscar')
				$('#busqueda_boton_buscar').focus();


		}
	});
}

/* CLICK EN ALGUN BOTON DE LA BOTONERA */
function busquedaClickBotonera(boton){

		//Si el Boton que nos pasaron no esta activo..
		if( !$(boton).hasClass('busqueda_botonera_active')){

			//Sacamos a todos los botones la clase active
			$('#busqueda_botonera>a').removeClass('busqueda_botonera_active');
			
			//Agregamos al boton apretado la clase active
			$(boton).addClass('busqueda_botonera_active');

			//Escondemos todos los contenedores y le ponemos la opacidad a 0
			$('#busqueda_opciones>div').hide().css({ opacity: 0 });

			//Mostramos el correspondiente
			$( $(boton).attr('href') ).show();
			
			//Animamos la opacidad y hacemos foco en el input correspondiente
			$( $(boton).attr('href') ).transition({ opacity: 1 }, 300, function(){
				//Solapa Codigo hacemos foco en prefijo...
				if( boton == '#busqueda_botonera_codigo') {
        			//Si estamos mostrando Fabricantes
        			if( $('#busqueda_codigo_mostrarFabricantesbt').hasClass('active') ) {
        				$('#busqueda_codigo_fabricante').select2('focus');
            		}else{
            			$('#busqueda_codigo_prefijo').focus();
            		}
				}else if(boton == '#busqueda_botonera_aplicacion'){
					$('#busqueda_aplicacion_vehiculo').select2('focus');
				}
			});


		//Si ya esta seleccinada la solapa pero le hacen click
		//Hacemos foco de todas formas...
		}else{

			//Solapa Codigo hacemos foco en prefijo...
			if( boton == '#busqueda_botonera_codigo') {
    			//Si estamos mostrando Fabricantes
    			if( $('#busqueda_codigo_mostrarFabricantesbt').hasClass('active') ) {
    				$('#busqueda_codigo_fabricante').select2('focus');
        		}else{
        			$('#busqueda_codigo_prefijo').focus();
        		}
			}else if(boton == '#busqueda_botonera_aplicacion'){
				$('#busqueda_aplicacion_vehiculo').select2('focus');
			}
			
		}
}


var busquedaAbierta = false;
var busquedaAnimando = false;
function animaBusqueda(tecla){
	
	//Nos pasaron alguna tecla?
	tecla = typeof tecla !== 'undefined' ? tecla : 'ninguna';

	//Si la busqueda estaba abierta la cerramos...
	if( (busquedaAbierta) && (!busquedaAnimando) && ( (tecla == 'escape') || (tecla == 'ninguna') ) ){
		//Cerramos la busqueda
		closeBusqueda();

	//Si la busqueda estaba cerrada la abrimos...
	}else if( (!busquedaAbierta) && (!busquedaAnimando) && ( (tecla == 'ninguna') || (tecla == 'supr') || (tecla == 'fin') || (tecla == 'avpag') ) ){
    	
    	//Altura de la ventana?
    	$('#busqueda_opciones').height( $(window).height() - 250);

    	//Estamos animando
    	busquedaAnimando = true;

    	//Ahora la busqueda está activa
    	$('#busqueda_button').addClass('active');

    	//Mostramos la busqueda
    	$("#busqueda").show();

    	//Aparece el overlay con opacity 0
    	$("#overlay").show();

    	//Cambiamos el overlay opacity a 1
    	$("#overlay").transition({ opacity: 1 }, 200);

    	//Aparece la busqueda
    	$("#busqueda").transition({ opacity: 1 }, 200, function() {
			
			//La busqueda esta abierta
			busquedaAbierta = true;
			
			//Bandera que indica Termino de animacion
			busquedaAnimando = false;

	    	//Seleccionamos la solapa correspondiente segun la tecla presionada
    		if( tecla == 'supr') {
    			busquedaClickBotonera('#busqueda_botonera_codigo');
    		}else if(tecla == 'fin'){
    			busquedaClickBotonera('#busqueda_botonera_aplicacion');
    		}else if(tecla == 'avpag'){
    			busquedaClickBotonera('#busqueda_botonera_dimension');
    		}
		});

	//Si la busqueda estaba abierta y siguen apretando las teclas de acceso
    }else if( busquedaAbierta && !busquedaAnimando && ((tecla == 'supr') || (tecla == 'fin') || (tecla == 'avpag')) ){
    	//Seleccionamos la solapa correspondiente segun la tecla presionada
		if( tecla == 'supr') {
			busquedaClickBotonera('#busqueda_botonera_codigo');
		}else if(tecla == 'fin'){
			busquedaClickBotonera('#busqueda_botonera_aplicacion');
		}else if(tecla == 'avpag'){
			busquedaClickBotonera('#busqueda_botonera_dimension');
		}
    }
}

/* CIERRA EL CUADRO DE BUSQUEDA */
function closeBusqueda(){
	//Estamos animando la busqueda
	busquedaAnimando = true;

	//El boton ya no esta activo
	$('#busqueda_button').removeClass('active');

	//Ocultamos overlay
	$('#overlay').transition({ opacity: 0 }, 100, function() {
		$("#overlay").hide();
	});

	//Ocultamos 
    $("#busqueda").transition({ opacity: 0 }, 200, function() {
    	$("#busqueda").hide();
		busquedaAbierta = false;
		busquedaAnimando = false;
	});
}


/* LIMPIAMOS FORMULARIO DE BUSQUEDA POR CÓDIGO */
function limpiaBusquedaCodigo(){
	$('#busqueda_codigo_prefijo').val('');
	$('#busqueda_codigo_codigo').val('');
	$('#busqueda_codigo_sufijo').val('');
	$('#busqueda_codigo_mostrarFabricantesbt').removeClass('active');
	$('#busqueda_codigo_mostrarFabricantesbt').html('Mostrar Fabricantes');
	$('#busqueda_codigo_fabricante_container').hide();
	$('#busqueda_codigo_fabricante').select2('data', null);
	$('#busqueda_codigo_prefijo_container').show();
	$('#busqueda_codigo_prefijo').focus();

}

/* LIMPIAMOS FORMULARIO DE BUSQUEDA POR APLICACION */
function limpiaBusquedaAplicacion(){

}

/* LIMPIAMOS FORMULARIO DE BUSQUEDA POR DIMENSION */
function limpiaBusquedaDimension(){

}

/* REALIZAMOS LA BUSQUEDA */
function handlerBotonBusqueda(){
	//Obtenemos boton activo
	var active = '';

	//Obtenemos id de boton activo
	$('#busqueda_botonera>a').each(function(){ if($(this).hasClass('busqueda_botonera_active')) active = $(this).attr('id'); });

	//Ocultamos resultados anteriores
	$('#resultados').hide();
	$('#table_head').hide();
	$('#foot').hide();

	//Cerramos la busqueda
	closeBusqueda();

	//Quitamos (sin-resultado)
	$('#sin_resultados').hide();

	//Mostrmaos cargando
	$('#procesando').fadeIn('fast');

	//Dependiendo que tipo de busqueda está activa derivamos acción
	if(active == 'busqueda_botonera_codigo'){
		disparaBusquedaCodigo();
	}else if(active == 'busqueda_botonera_aplicacion'){
		disparaBusquedaAplicacion();
	}else if(active == 'busqueda_botonera_dimension'){
		disparaBusquedaDimension();
	}
}

/* BUSQUEDA POR CODIGO */
function disparaBusquedaCodigo(){

		$('#sin_resultados').hide();

		//Eligieron busqueda por marca?
		var id_marca = 'null';
		if( $('#busqueda_codigo_mostrarFabricantesbt').hasClass('active') ){
			id_marca = $('#s2id_busqueda_codigo_fabricante').select2('val');
		}

		//JUNTAMOS TODAS LAS VARIABLES Y HACEMOS UN LLAMADO AJAX
		resultados.option("urlData", {mode: 'resultado_cod', 
								prefijo: $('#busqueda_codigo_prefijo').val(), 
								codigo: $('#busqueda_codigo_codigo').val(), 
								sufijo: $('#busqueda_codigo_sufijo').val(), 
								id_marca: id_marca
		});


		resultados.update(function(){
			//alert(resultados._data);
			$('#procesando').fadeOut('fast', function(){});
		});
}

