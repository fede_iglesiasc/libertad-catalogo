// jQuery Plugin Boilerplate
// A boilerplate for jumpstarting jQuery plugins development
// version 2.0, July 8th, 2011
// by Stefan Gabos

;(function($) {

    $.field = function(el, options) {

        var defaults = {

            ajax_url: '',                       //Direccion URL contra la que chequearemos validez
            form: 'default',
            ajax_wait: 500,                    //Tiempo de espera desde que el usuario preciona la ultima tecla para hacer consulta ajax.
            ajax_wait_id: undefined,            //id del icono de espera...
            data: { },                          //Al llamado ajax podemos agregarle parametros
            value_id: undefined,
            show_results: false,
            onResult: function(plugin) {},
            onError: function(plugin){}
        }

        //Public variables
        var plugin = this;          //Reference to 'this'
        plugin.el;                     //field html element reference
        plugin.wait_icon;              //Validate status icon reference
        plugin.validated = false;      //If field is validated at least once
        plugin.procesing = false;      //Is validating actually?
        plugin.form;                   //Form belongs this field
        plugin.param_name;             //Param name to use in ajax request
        plugin.value = '';             //Value of the field
        plugin.value_id = undefined;   //ID of the element contains the value
        plugin.value_el;               //Value element reference
        plugin.errors = new Array();       //Array of errors
        plugin.results = new Array();      //Array of results
        plugin.timer;                  //Timer for keypress field event handler
        plugin.ajaxCall;               //Ajax unique instance for server validation

        plugin.settings = {}

        var init = function() {
            if(debug) console.log('Inicializa field.');
            //Set default settings
            plugin.settings = $.extend({}, defaults, options);


            //Transform from name in local
            plugin.form = plugin.settings.form;

            //Instanciate element in public var
            plugin.el = el;

            //Value id
            if( plugin.settings.value_id != undefined ){
                plugin.value_id = plugin.settings.value_id;
                plugin.value_el = $('#'+plugin.value_id);
                plugin.value = plugin.value_el.val();
            }

            //Get the param name 
            plugin.param_name = plugin.el.attr('id').split("_");
            plugin.param_name = plugin.param_name[plugin.param_name.length-1];

            //If have especific id for validate icon...
            if(plugin.settings.ajax_wait_id == undefined)
                plugin.wait_icon = $('#'+el.attr('id')+'_valida_icon');
            else
                plugin.wait_icon = $('#'+plugin.settings.ajax_wait_id);

            //We need to set an event handler looking for changes if custom id value field isset
            if(plugin.settings.value_id != undefined){
                $('#'+plugin.settings.value_id).change(function(){
                    plugin.valida();
                });
            }

            //When tipping in field, trigger this function...
            el.keyup(function(event) {

                //Get Key pressed code
                var code = event.keyCode || event.which;

                //If keypress code is non permited... return
                if( (code == 13) || (code == 9) || (code == 9 && event.shiftKey) ||
                    (code == 16) || (code == 17) || (code == 91) || (code == 9) || 
                    (code == 37) || (code == 38) || (code == 39) || (code == 40))
                    return;

                //reset ajax results
                plugin.valid = 0;
                plugin.errors = Array();
                plugin.result = Array();
                //Reset tooltip
                el.tooltip('destroy');

                //We are procesing validation
                setProcesing(true);

                if (plugin.ajaxCall) { plugin.ajaxCall.abort() }    // If there is an existing XHR, abort it.
                clearTimeout(plugin.timer);                         // Clear the timer so we don't end up with dupes.

                //Set timer
                plugin.timer = setTimeout(function() {
                    //Execute the validation
                    plugin.valida();

                }, plugin.settings.ajax_wait); // delay

            });
        }

        //Update satate of Processing
        var setProcesing = function(value) {
            plugin.procesing = value;
            createTooltip();
            updateIcon();
        }

        plugin.valida = function() {

            //Preparamos parametros ajax
            plugin.settings.data['run'] = el.attr('id');

            //Si el value lo almacenamos en el propio elemento...
            if(plugin.settings.value_id == undefined){
                plugin.settings.data[plugin.param_name] = el.val();
                plugin.value = el.val()
            }

            //Si el value lo almacenamos en otro elemento...
            else{
                plugin.settings.data[plugin.param_name] = plugin.value_el.val();
                plugin.value = plugin.value_el.val();
            }

            //Run ajax request and store in a variable (so, we can cancel)
            plugin.ajaxCall =  $.ajax( {
                                    type: "GET",
                                    url: plugin.settings.ajax_url,
                                    data: plugin.settings.data,
                                    contentType: "application/json; charset=utf-8",
                                    dataType: "json",
                                    error: function (json) {
                                        //alert(json.statusText);
                                    },
                                    success: function (json) {

                                        //Save results in object...
                                        plugin.valid = parseInt(json.status);
                                        
                                        //Array of errors
                                        for( var k in json.errors) plugin.errors.push(json.errors[k]);

                                        //Array of results
                                        for( var k in json.result) plugin.results.push(json.result[k]);

                                        //We are procesing validation
                                        setProcesing(false);

                                        //The field was validate at least once
                                        plugin.validated = true;

                                        //Execute callback..
                                        if(plugin.errors.length) plugin.settings.onError(plugin);

                                        //Execute callback...
                                        plugin.settings.onResult(plugin);
                                    }
                                });
        }

        /* RESET THE INPUT AND VALIDATION */
        plugin.reset = function() {
            //reset internal value
            plugin.value = '';
            //hide the validate icon...
            plugin.wait_icon.hide();
            //Set initial validated flag false
            plugin.validated = false;
            //set valid flag to false...
            plugin.valid = false;
            //Set input value to empty
            plugin.el.val('');

            plugin.errors = new Array();
            plugin.results = new Array();
            //Reset tooltip
            plugin.el.tooltip('destroy');
        }

        /* RESET THE INPUT AND VALIDATION 
        plugin.value = function(val) {
            
            //Si el value lo almacenamos en el propio elemento...
            if(plugin.settings.value_id == undefined){
                el.val(val)
            }

            //Si el value lo almacenamos en otro elemento...
            else{
                $('#'+plugin.settings.value_id).val(val);
            }

            //reset internal value
            plugin.value = val;

        }*/

        //Update state of tooltip
        var createTooltip = function() {
            //Log
            if(debug) console.log('Crear tooltip.');

            //Delete previous tooltip
            plugin.el.tooltip('destroy');

            //If error to display...
            if( plugin.errors.length > 0){
                //Create the tooltip...
                el.tooltip({ title : plugin.errors[0], placement: "right", trigger: "focus" });
                //If element still having focus..
                if(el.is(":focus")) el.tooltip('show');

            }

            //If results to display...
            if(plugin.settings.show_results && ( plugin.results.length > 0)){
                //Create the tooltip...
                el.tooltip({ title : plugin.results[0], placement: "right", trigger: "focus" });
                //If element still having focus..
                if(el.is(":focus")) el.tooltip('show');
            }
        }

        //Update validate icon
        var updateIcon = function() {

            //If we are making an ajax call...
            if(plugin.procesing){
                if(debug) console.log('Update icon processing.');
                //Remove all CSS classes
                plugin.wait_icon.removeClass();
                //Add refreshing icon
                plugin.wait_icon.addClass('glyphicon glyphicon-cog iconValidaUpdate');
                plugin.wait_icon.show();
            }

            //If we are not making an ajax call and field value is valid
            if( !plugin.procesing && plugin.valid){
                if(debug) console.log('Update icon valid.');
                //Remove all CSS classes
                plugin.wait_icon.removeClass();
                //Add refreshing icon
                plugin.wait_icon.addClass('glyphicon glyphicon-ok iconValidUpdate');
                plugin.wait_icon.show();
            }

            //If we are not making an ajax call and field value is invalid
            if( !plugin.procesing && !plugin.valid){
                if(debug) console.log('Update icon invalid.');
                //Remove all CSS classes
                plugin.wait_icon.removeClass();
                //Add refreshing icon
                plugin.wait_icon.addClass('glyphicon glyphicon-remove iconInvalidUpdate');
                //Show the icon
                plugin.wait_icon.show();
            }
        }

        /* Add error programatically */
        plugin.setError = function(error) {
            //Set initial validated flag false
            plugin.validated = true;
            //set valid flag to false...
            plugin.valid = false;
            //Add errror
            plugin.errors.push(error);
            updateIcon();
            createTooltip();
        }

        /* Add error programatically */
        plugin.deleteError = function(error) {
            //Search in array of errrors
            for( var k in plugin.errors){
                if(debug) console.log('Borrando Error.');
                if(plugin.errors[k] == error){
                    plugin.errors.splice(k, 1);
                    console.log('Error borrado');
                }
            }

            //Set initial validated flag false
            plugin.validated = true;

            plugin.procesing = false;

            //set valid flag to...
            plugin.valid = (plugin.errors.length) ? false : true;

            //Refresh visual elements
            updateIcon();
            createTooltip();
        }


        init();
    }
})(jQuery);