DROP FUNCTION IF EXISTS GET_IMGS_BY_ART;

DELIMITER $$
CREATE FUNCTION GET_IMGS_BY_ART(art_id INT) 
RETURNS TEXT
BEGIN

	DECLARE equivalencias TEXT;

	SET equivalencias = '[]';

	SELECT CONCAT('[', GROUP_CONCAT(CONCAT( '"', t.codigo, '"') separator ','), ']') INTO equivalencias

	FROM (
			SELECT 	(SELECT 	CONCAT(prefijo, '-', codigo, '-', sufijo) 
					 FROM 		catalogo_articulos 
					 WHERE 		id = z.articulo_id) as codigo,

					1 as grupo

			FROM 	catalogo_articulos_equiv_img z

			WHERE 	id IN (SELECT id FROM catalogo_articulos_equiv_img WHERE articulo_id = art_id) AND
					(articulo_id <> art_id)

			UNION 	SELECT 	(	SELECT 	CONCAT(prefijo, '-', codigo, '-', sufijo) 
					 			FROM 	catalogo_articulos 
					 			WHERE 	id = art_id) as codigo,
							1 as grupo
		) t

	GROUP BY grupo;


	RETURN equivalencias;

END$$

DELIMITER ;