DROP FUNCTION IF EXISTS GET_EQUIV_BY_ART;

DELIMITER $$
CREATE FUNCTION GET_EQUIV_BY_ART(art_id INT) 
RETURNS TEXT
BEGIN

	DECLARE equivalencias TEXT;

	SET equivalencias = '[]';

    SELECT
    	CONCAT(	'[',
    			GROUP_CONCAT(CONCAT(
    				'{	"codigo" : 	"',t.codigo,'", ',
    				'	"id"  : 	',t.id,', ',
    				'	"marca"  : 	"',t.marca,'", ',
    				'	"marca_id"  : 	',t.marca_id,', ',
    				' 	"precio" :	',t.precio,'}') separator ','),
    			']') INTO equivalencias
    FROM (
			SELECT 	z.articulo_id as id,

					(SELECT 	CONCAT(prefijo, '-', codigo, '-', sufijo) 
					 FROM 		catalogo_articulos 
					 WHERE 		id = z.articulo_id) as codigo,
					
					(SELECT 	nombre 
					 FROM 		catalogo_marcas 
					 WHERE 		id = (SELECT marca_id FROM catalogo_articulos WHERE id = z.articulo_id)) as marca,
					
					(SELECT marca_id FROM catalogo_articulos WHERE id = z.articulo_id) as marca_id,

					(SELECT 	precio 
					 FROM 		catalogo_articulos 
					 WHERE 		id = z.articulo_id) as precio,

					1 as grupo

			FROM 	catalogo_articulos_equiv z

			WHERE 	id IN (SELECT id FROM catalogo_articulos_equiv WHERE articulo_id = art_id) AND 
					(articulo_id <> art_id)
		) t

	GROUP BY grupo;


	RETURN equivalencias;

END$$

DELIMITER ;