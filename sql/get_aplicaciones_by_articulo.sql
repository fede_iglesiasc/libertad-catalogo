DROP FUNCTION IF EXISTS GET_APLIC_BY_ART;

DELIMITER $$
CREATE FUNCTION GET_APLIC_BY_ART(art_id INT) 
RETURNS TEXT
BEGIN

	DECLARE aplicaciones TEXT;

	SELECT CONCAT('[', 
    		GROUP_CONCAT(CONCAT(
            '{',
            		'"marca":"', (SELECT nombre FROM catalogo_autos_marca WHERE id = marca_id), '", ',
            		'"marca_id": ', marca_id, ', ',
            		'"modelo": "', (SELECT nombre FROM catalogo_autos_modelo WHERE id = modelo_id), '", ',
            		'"modelo_id": ', modelo_id, ', ',
            		'"version": ', IF(version_id IS NULL, 'null', CONCAT('"',(SELECT descripcion FROM catalogo_autos_versiones WHERE id = version_id),'"')) ,', ',
            		'"desde": ', anio_inicio, ', ',
            		'"hasta": ', anio_final,
            '}'
                ) separator ','),
        ']') INTO aplicaciones
	FROM catalogo_articulos_aplicaciones a 
	WHERE articulo_id = art_id 
	GROUP BY articulo_id;

	RETURN aplicaciones;

END$$

DELIMITER ;