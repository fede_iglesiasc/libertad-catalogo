DROP FUNCTION IF EXISTS GET_DIMENSIONS;

DELIMITER $$
CREATE FUNCTION GET_DIMENSIONS(art_id INT, rub_id INT) 
RETURNS TEXT
BEGIN

    DECLARE dimensiones TEXT;
    


    SELECT CONCAT(  '[',
                    GROUP_CONCAT(CONCAT(
                        '{  "id" : ', t.id ,', ',
                        '   "nombre" : "', t.nombre , '", ',
                        '   "opciones" : ', t.opciones , '}'    
                    ) separator ','),
                    ']'
                ) INTO dimensiones
    FROM (
            SELECT      *,
                        (IF( rub_id = rubro_id, 1 , 0 )) as propia,
                        1 as grupo,
                        (   SELECT      CONCAT('[',GROUP_CONCAT(CONCAT('{"id" : ',id, ', "nombre": "', nombre, '"}') separator ','),']') 
                            FROM        catalogo_rubros_datos_opciones 
                            WHERE       (id_datos = x.id) 
                            GROUP BY    id_datos
                        ) as opciones

            FROM        catalogo_rubros_datos x

            WHERE       (rubro_id = rub_id) OR
                        (rubro_id IN (

                            SELECT  T2.id
                            FROM (
                                SELECT
                                    @r AS _id,
                                    (SELECT @r := padre FROM catalogo_rubros WHERE id = _id) AS padre,
                                    @l := @l + 1 AS lvl
                                FROM
                                    (SELECT @r := rub_id, @l := 0) vars,
                                    catalogo_rubros m
                                WHERE @r <> 0) T1
                            JOIN catalogo_rubros T2
                            ON T1._id = T2.id
                            WHERE T2.id <> rub_id
                            ORDER BY T1.lvl DESC


                        ) AND cede_decendencia = 'Y')

            ORDER BY propia ASC, orden ASC
    ) t

    GROUP BY t.grupo;


    RETURN dimensiones;

END$$

DELIMITER ;