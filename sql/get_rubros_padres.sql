DROP FUNCTION IF EXISTS GET_RUBROS_PADRES;

DELIMITER $$
CREATE FUNCTION GET_RUBROS_PADRES(rubro_id INT) 
RETURNS VARCHAR(65535)
BEGIN

	DECLARE rubros_padres VARCHAR(65535);

	SET rubros_padres = '[]';

    IF rubro_id IS NOT NULL THEN 

		SELECT  (CONCAT('[', 
					GROUP_CONCAT(CONCAT(
						'{',
							'"id" : ',T2.id,', ',
							'"nombre" : "',T2.nombre,'", ',
							'"nombre_singular" : "', T2.nombre_singular ,'", ',
							'"level" : ',T1.lvl,
						'}')
					separator ','),
				']')) INTO rubros_padres
		FROM (
		    SELECT
		        @r AS _id,
		        (SELECT @r := padre FROM catalogo_rubros WHERE id = _id) AS padre,
		        @l := @l + 1 AS lvl
		    FROM
		        (SELECT @r := rubro_id, @l := 0) vars,
		        catalogo_rubros m
		    WHERE @r <> 0) T1
		JOIN catalogo_rubros T2
		ON T1._id = T2.id
		ORDER BY T1.lvl DESC;

	END IF;

	RETURN rubros_padres;

END$$

DELIMITER ;