<?php

class Field{

     /**
     * @description Field ID.
     * @access public
     * @var string
     * */
    public $_id                                 = 'default';


     /**
     * @description Mandatory field.
     * @access public
     * @var bool
     * */
    public $_obligatorio                        = FALSE;


     /**
     * @description Field Label.
     * @access public
     * @var string
     * */
    public $_label                            = 'DEFAULT';


     /**
     * @description Field value.
     * @access public
     * @var string
     * */
    public $_value                                 = '';


     /**
     * @description Field is empty?.
     * @access public
     * @var string
     * */
    public $_empty                                    = TRUE;


     /**
     * @description Field Label.
     * @access public
     * @var mixed
     * */
    private $_filter                                    = FALSE;   //FALSE disabled, filter chars string...

     /**
     * @description Field Label.
     * @access public
     * @var mixed
     * */
    private $_filter_chars                            = FALSE;   //FALSE disabled, filter chars string...

     /**
     * @description Show filter Error.
     * @access public
     * @var mixed
     * */
    private $_filter_error_msg                          = FALSE;   //FALSE disabled, Enabled: MSG string..

     /**
     * @description We must Validate?.
     * @access private
     * @var bool
     * */
    private $_validar                           = FALSE;


     /**
     * @description Function that VALIDATE FIELD
     * @access public
     * @var mixed
     * */
    private $_validacion;   //function validate


     /**
     * @description Field errors.
     * @access public
     * @var array
     * */
    public $_errors                                 = array();



    /**
     * @description Initializes the FIELD.
     * @access public
     * @param array - configuration options
     * @return void
     * */
    public function __construct(array $config)
    {
        //id
        $this->_id = $config['field'];

        //obligatorio
        $this->_obligatorio = $config['obligatorio'];

        

        //label
        $this->_label = $config['label'];

        //filter_chars
        $this->_filter_chars = $config['filter_chars'];

        //filter
        if(isset($config['filter']))
        $this->_filter = $config['filter'];

        //filter_chars
        $this->_filter_error_msg = $config['filter_error_msg'];

        //validar
        $this->_validar = $config['validar'];

        //validacion
        $this->_validacion = $config['validacion'];

        //get $_GET value
        if(isset($_GET[$this->_id])) $this->_value = trim($_GET[$this->_id]);

        //The field has value
        if($this->_value != '') $this->_empty = FALSE;

        //The field is empty
        if($this->_empty && $this->_obligatorio){
            //if set a custom empty msg
            if(isset($config['obligatorio_msg']))
                $this->setError(str_replace('#label#', $this->_label, $config['obligatorio_msg']));
            else
                $this->setError("El campo ".$this->_label." es obligatorio.");
        }

        //Add filter Error if is needed...
        if($this->_filter_error_msg && $this->_filter_chars && ((!$this->_empty && $this->_obligatorio) || (!$this->_obligatorio)) )
            if($this->filtrar() != $this->_value)
                $this->setError(str_replace('#label#',$this->_label,$this->_filter_error_msg));


        //Filter field value...
        if($this->_filter_chars && $this->_filter) $this->_value = $this->filtrar();


        //Validamos...
        if($this->_validar) $this->validar();
    }


    /**
     * @description Validate with function passed by param.
     * @access public
     * @return void
     * */
    public function validar()
    {
        $this->_validacion($this);
    }


    /**
     * @description Filter value
     * @access public
     * @return string
     * */
    public function filtrar()
    {
        $pattern = "/[^".preg_quote($this->_filter_chars, "/")."]/u";
        return preg_replace($pattern, "", $this->_value); 
    }


    /**
     * @description Set error
     * @access public
     * @return void
     * */
    public function setError($error)
    {
        array_push($this->_errors, $error);
    }

    /**
     * @description Call magic method
     * @access public
     * @return void
     * */
    public function __call($method, $args) {
         if(isset($this->$method) && is_callable($this->$method)) {
             return call_user_func_array(
                 $this->$method, 
                 $args
             );
         }
      }


}

?>