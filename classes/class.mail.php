<?php
require_once('class.phpmailer.php');
 
class send_mail extends PHPMailer
{
    var $priority = 3;
    var $to_name;
    var $to_email;
    var $From = null;
    var $FromName = null;
    var $Sender = null;
 
    function send_mail()
    {
 
        if($GLOBALS['conf']['smtp_mode'] == 'enabled')
        {
            $this->isSMTP();
            $this->Host = $GLOBALS['conf']['smtp_host'];
            $this->Port = $GLOBALS['conf']['smtp_port'];
            if($GLOBALS['conf']['smtp_username'] != ''){
                $this->SMTPAuth = true;
                $this->Username = $GLOBALS['conf']['smtp_username'];
                $this->Password = $GLOBALS['conf']['smtp_password'];
            }
            $this->Mailer = "smtp";
        }
        if(!$this->From)
        {
            $this->From = $GLOBALS['conf']['from_email'];
        }
        if(!$this->FromName)
        {
            $this->FromName = $GLOBALS['conf']['from_name'];
        }
        if(!$this->Sender)
        {
            $this->Sender = $GLOBALS['conf']['from_email'];
        }
        $this->Priority = $this->priority;
        $this->CharSet = 'UTF-8';
    }
}
?>