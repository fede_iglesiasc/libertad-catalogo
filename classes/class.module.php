<?php
class module{
	function __destruct() {
        //Print results
        echo $GLOBALS['resultado']->getResult();
	}

	function __caller($accion){

		//Si existe el metodo pre() lo llamamos
		if(method_exists($this, '__pre')){ 
          return call_user_func_array(array($this, '__pre'), array($accion)); 
        }

        //llamamos al metodo directamente
        return call_user_func_array(array($this, $accion), array());
	}
}
?>