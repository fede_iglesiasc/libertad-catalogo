<?php
require_once('class.sessionhandler.php');
 
class Session extends SessionHandlerx
{
 
    function __construct()
    {
        $config['database']                 =  $GLOBALS['conf']['pdo'];
        $config['cookie_name']              =  $GLOBALS['conf']['session_cookie_name'];
        $config['table_name']               =  $GLOBALS['conf']['session_table_name'];
        $config['seconds_till_expiration']  =  $GLOBALS['conf']['session_expiration'];
        $config['renewal_time']             =  $GLOBALS['conf']['session_expiration'];
        $config['expire_on_close']          =  $GLOBALS['conf']['session_exp_on_close'];
        $config['secure_cookie']            =  $GLOBALS['conf']['session_sec_cookie'];           
        $config['check_ip_address']         =  $GLOBALS['conf']['session_check_ip'];
        $config['check_user_agent']         =  $GLOBALS['conf']['session_check_agent'];

        //Llamamos al padre
        parent::__construct($config);
    }
}
?>