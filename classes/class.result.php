<?php
class Result
{
     /**
     * @description Errors array.
     * @access private
     * @var array
     * */
    private $_errors               = array();


     /**
     * @description Status result.
     * @access private
     * @var int
     * */
    public $_status               = 1;     // 0 complete action with errors - 1 complete action withouth errors


     /**
     * @description Result array.
     * @access private
     * @var array
     * */
    public $_result              = array();

     /**
     * @description Debug array.
     * @access private
     * @var array
     * */
    public $_debug              = array();


     /**
     * @description Cronometro.
     * @access private
     * @var array
     * */
    public $_cronometro              = array();

    public function __construct(){

    }


    /**
     * @description Add error to the errors array.
     * @access public
     * @param string - Error value
     * @return void
     * */
    public function setError($error)
    {
        $this->_errors[] = $error;
        $this->_status = 0;
    }

    /**
     * @description Get results array.
     * @access public
     * @return array
     * */
    public function getResult($json = TRUE){
    	$result['status'] = $this->_status;
    	$result['result'] = $this->_result;
    	$result['errors'] = $this->_errors;
        $result['debug'] = $this->_debug;
        $result['cron'] = $this->_cronometro;
        $result['logued'] = ($GLOBALS['session']->getData('usuario'))? true : false;
        
        //Convertir a json?
        if($json) $result =  json_encode($result);
        
        return $result;
    }


    /**
     * @description Set results array.
     * @access public
     * @return void
     * */
    public function setResult($result)
    {
        /*if(gettype($result) == 'string') array_push($this->_result, $result);

        elseif(gettype($result) == 'array') foreach($result as $k=>$v){ array_push($this->_result, $v); }*/
        array_push($this->_result, $result);
    }

    /**
     * @description Get status.
     * @access public
     * @return int
     * */
    public function getStatus()
    {
    	return $this->_status;
    }
}
?>