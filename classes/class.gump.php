<?php

/**
 * GUMP - A fast, extensible PHP input validation class
 *
 * @author      Sean Nieuwoudt (http://twitter.com/SeanNieuwoudt)
 * @copyright   Copyright (c) 2014 Wixelhq.com
 * @link        http://github.com/Wixel/GUMP
 * @version     1.0
 */

class GUMP
{
	// Validation rules for execution
	protected $validation_rules = array();

	// Filter rules for execution
	protected $filter_rules = array();

	// Instance attribute containing errors from last run
	protected $errors = array();

	// Tiene los mensajes de error
	public static $errores = array(
			'mismatch' => 'return "No hay regla de validación para el campo $field.";',
			'validate_required' => 'return "El campo $field es obligatorio.";',
			'validate_valid_email' => 'return "El campo {$field} contiene una dirección de email inválida.";',
			'validate_max_len' => 'return "El campo $field debe de tener $param caracteres como máximo.";',
			'validate_min_len' => 'return "El campo $field debe de tener al menos $param caracteres.";',
			'validate_exact_len' => 'return "El campo $field debe de tener $param caracteres de largo.";',
			'validate_alpha' => 'return "El campo $field sólo puede estar formado por letras (a-z).";',
			'validate_alpha_numeric' => 'return "El campo $field sólo puede contener caracteres alfanuméricos.";',
			'validate_alpha_dash' => 'return "El campo $field sólo puede contener letras y guiones.";',
			'validate_numeric' => 'return "El campo $field sólo puede contener caracteres numéricos.";',
			'validate_integer' => 'return "El campo $field sólo puede contener un número entero como valor.";',
			'validate_boolean' => 'return "El campo $field sólo puede contener TRUE o FALSE como valor.";',
			'validate_float' => 'return "El campo $field sólo puede contener un número decimal como valor.";',
			'validate_valid_url' => 'return "El campo $field debe de ser una URL válida.";',
			'validate_url_exists' => 'return "El campo $field URL no existe.";',
			'validate_valid_ip' => 'return "El campo $field debe contener una IP válida.";',
			'validate_valid_cc' => 'return "El campo $field contiene un número inválido.";',
			'validate_valid_name' => 'return "El campo $field tiene un nombre humano inválido;',
			'validate_contains' => 'return "El campo $field debe contener uno de los siguientes valores: ".implode(", ", $param);',
			'validate_containsList' => 'return "El campo $field debe contener uno de los siguientes valores: ".implode(", ", $param);',
			'validate_doesNotContainList' => 'return "El campo $field contiene un valor incorrecto.";',
			'validate_street_address' => 'return "El campo $field no contiene un domicilio válido.";',
			'validate_date' => 'return "El campo $field no contiene una fecha válida.";',
			'validate_min_numeric' => 'return "El campo $field debe contener un valor numérico igual o mayor que $param .";',
			'validate_max_numeric' => 'return "El campo $field debe contener un valor numérico igual o menor que $param .";',
			'validate_cantidad_dec' => 'return "El campo $field debe contener $param cifras decimales.";',
			'validate_starts' => 'return "El campo $field debe comenzar con $param;',
			//Validadores personalizados
			'validate_valores_permitidos' => 'return "Los valores permitidos para el campo $field son: ".implode(", ", json_decode(str_replace("_##coma##_", ",", $param)));',
			'validate_valores_no_permitidos' => 'return "El campo $field no admite los siguientes valores: ".implode(", ", json_decode(str_replace("_##coma##_", ",", $param)));',
			'validate_json' => 'return "El campo $field tiene un formato JSON inválido.";',
			'validate_caracteres_permitidos' => 'return "El campo $field debe estar formado por: ".$param;',
			'validate_required_file'=>'return "El archivo es requerido para continuar";',
			'validate_extension'=>'return "Las extensiones permitidas para el archivo son: ".$param;',
			'validate_archivo_maxsize'=>'return "El tamaño máximo del archivo es de ".$GLOBALS["toolbox"]->formatBytes($param);',
			'validate_archivo_mover'=>'return "Ocurrio un error al intentar mover el archivo";',
			'default' => 'return "El campo $field es inválido.";'

	);



	// Custom validation methods
	protected static $validation_methods = array();

	// Customer filter methods
	protected static $filter_methods = array();

	// ** ------------------------- Validation Data ------------------------------- ** //

	public static $basic_tags     = "<br><p><a><strong><b><i><em><img><blockquote><code><dd><dl><hr><h1><h2><h3><h4><h5><h6><label><ul><li><span><sub><sup>";

	public static $en_noise_words = "about,after,all,also,an,and,another,any,are,as,at,be,because,been,before,
									 being,between,both,but,by,came,can,come,could,did,do,each,for,from,get,
									 got,has,had,he,have,her,here,him,himself,his,how,if,in,into,is,it,its,it's,like,
									 make,many,me,might,more,most,much,must,my,never,now,of,on,only,or,other,
									 our,out,over,said,same,see,should,since,some,still,such,take,than,that,
									 the,their,them,then,there,these,they,this,those,through,to,too,under,up,
									 very,was,way,we,well,were,what,where,which,while,who,with,would,you,your,a,
									 b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,$,1,2,3,4,5,6,7,8,9,0,_";

	// ** ------------------------- Validation Helpers ---------------------------- ** //

	/**
	 * Shorthand method for inline validation
	 *
	 * @param array $data The data to be validated
	 * @param array $validators The GUMP validators
	 * @return mixed True(boolean) or the array of error messages
	 */
	public static function is_valid(array $data, array $validators)
	{
		$gump = new Gump();

		$gump->validation_rules($validators);

		if($gump->run($data) === false) {
			return $gump->get_readable_errors(false);
		} else {
			return true;
		}
	}

	/**
	 * Shorthand method for running only the data filters
	 *
	 * @param array $data
	 * @param array $filters
	 */
	public static function filter_input(array $data, array $filters)
	{
		$gump = new Gump();

		return $gump->filter($data, $filters);
	}

	/**
	 * Magic method to generate the validation error messages
	 *
	 * @return string
	 */
	public function __toString()
	{
		return $this->get_readable_errors(true);
	}

	/**
	 * Perform XSS clean to prevent cross site scripting
	 *
	 * @static
	 * @access public
	 * @param  array $data
	 * @return array
	 */
	public static function xss_clean(array $data)
	{
		foreach($data as $k => $v)
		{
			$data[$k] = filter_var($v, FILTER_SANITIZE_STRING);
		}

		return $data;
	}

	/**
	 * Adds a custom validation rule using a callback function
	 *
	 * @access public
	 * @param string $rule
	 * @param callable $callback
	 * @return bool
	 */
	public static function add_validator($rule, $callback)
	{
		$method = 'validate_'.$rule;
		
		if(method_exists(__CLASS__, $method) || isset(self::$validation_methods[$rule])) {
			throw new Exception("Validator rule '$rule' already exists.");
		}

		self::$validation_methods[$rule] = $callback;

		return true;
	}

	/**
	 * Adds a custom filter using a callback function
	 *
	 * @access public
	 * @param string $rule
	 * @param callable $callback
	 * @return bool
	 */
	public static function add_filter($rule, $callback)
	{
		$method = 'filter_'.$rule;
		
		if(method_exists(__CLASS__, $method) || isset(self::$filter_methods[$rule])) {
			throw new Exception("Filter rule '$rule' already exists.");
		}

		self::$filter_methods[$rule] = $callback;

		return true;
	}

	/**
	 * Getter/Setter for the validation rules
	 *
	 * @param array $rules
	 * @return array
	 */
	public function validation_rules(array $rules = array())
	{
		if(empty($rules)) {
			return $this->validation_rules;
		}

		$this->validation_rules = $rules;
	}

	/**
	 * Getter/Setter for the filter rules
	 *
	 * @param array $rules
	 * @return array
	 */
	public function filter_rules(array $rules = array())
	{
		if(empty($rules)) {
			return $this->filter_rules;
		}

		$this->filter_rules = $rules;
	}

	/**
	 * Run the filtering and validation after each other
	 *
	 * @param array $data
	 * @return array
	 * @return boolean
	 */
	public function run(array $data, $check_fields = false)
	{
		$data = $this->filter($data, $this->filter_rules());

		$validated = $this->validate(
			$data, $this->validation_rules()
		);

		if($check_fields === true) {
			$this->check_fields($data);
		}

		if($validated !== true) {
			return false;
		}

		return $data;
	}

	/**
	 * Ensure that the field counts match the validation rule counts
	 *
	 * @param array $data
	 */
	private function check_fields(array $data)
	{
		$ruleset  = $this->validation_rules();
		$mismatch = array_diff_key($data, $ruleset);
		$fields   = array_keys($mismatch);

		foreach ($fields as $field) {
			$this->errors[] = array(
				'field' => $field,
				'value' => $data[$field],
				'rule'  => 'mismatch',
				'param' => NULL
			);
		}
	}

	/**
	 * Sanitize the input data
	 *
	 * @access public
	 * @param  array $data
	 * @return array
	 */
	public function sanitize(array $input, $fields = NULL, $utf8_encode = true)
	{
		$magic_quotes = (bool)get_magic_quotes_gpc();

		if(is_null($fields))
		{
			$fields = array_keys($input);
		}

		foreach($fields as $field)
		{
			if(!isset($input[$field]))
			{
				continue;
			}
			else
			{
				$value = $input[$field];

				if(is_string($value))
				{
					if($magic_quotes === TRUE)
					{
						$value = stripslashes($value);
					}

					if(strpos($value, "\r") !== FALSE)
					{
						$value = trim($value);
					}

					if(function_exists('iconv') && function_exists('mb_detect_encoding') && $utf8_encode)
					{
						$current_encoding = mb_detect_encoding($value);

						if($current_encoding != 'UTF-8' && $current_encoding != 'UTF-16') {
							$value = iconv($current_encoding, 'UTF-8', $value);
						}
					}

					$value = filter_var($value, FILTER_SANITIZE_STRING);
				}

				$input[$field] = $value;
			}
		}

		return $input;
	}

	/**
	 * Return the error array from the last validation run
	 *
	 * @return array
	 */
	public function errors()
	{
		return $this->errors;
	}

	/**
	 * Perform data validation against the provided ruleset
	 *
	 * @access public
	 * @param  mixed $input
	 * @param  array $ruleset
	 * @return mixed
	 */
	public function validate(array $input, array $ruleset)
	{
		$this->errors = array();

		foreach($ruleset as $field => $rules)
		{
			#if(!array_key_exists($field, $input))
			#{
			#   continue;
			#}

			$rules = explode('|', $rules);
			
	        //if(in_array("required", $rules) || (isset($input[$field]) && trim($input[$field]) != ''))
			$is_empty = true;
			if (!is_array($input[$field])) {
				$is_empty = isset($input[$field]) && trim($input[$field]) != '';
			}

			if(in_array("required", $rules) || $is_empty)
	        {			
				foreach($rules as $rule)
				{
					$method = NULL;
					$param  = NULL;

					if(strstr($rule, ',') !== FALSE) // has params
					{
						$rule   = explode(',', $rule);
						$method = 'validate_'.$rule[0];
						$param  = $rule[1];
						$rule   = $rule[0];
					}
					else
					{
						$method = 'validate_'.$rule;
					}

					if(is_callable(array($this, $method)))
					{
						$result = $this->$method($field, $input, $param);

						if(is_array($result)) // Validation Failed
						{
							$this->errors[] = $result;
						}
					}
					else if (isset(self::$validation_methods[$rule]))
					{
						if (isset($input[$field])) {
							$result = call_user_func(self::$validation_methods[$rule], $field, $input, $param);

							if (!$result) // Validation Failed
							{
								$this->errors[] = array(
									'field' => $field,
									'value' => $input[$field],
									'rule'  => $method,
									'param' => $param
								);
							}
						}
					}
					else
					{

						throw new Exception("Validator method '$method' does not exist.");
					}
				}
			}
		}

		return (count($this->errors) > 0)? $this->errors : TRUE;
	}

	/**
	 * Process the validation errors and return human readable error messages
	 *
	 * @param bool $convert_to_string = false
	 * @param string $field_class
	 * @param string $error_class
	 * @return array
	 * @return string
	 */
	public function get_readable_errors($convert_to_string = false, $field_class="field", $error_class="error-message")
	{
		if(empty($this->errors)) {
			return ($convert_to_string)? null : array();
		}

		$resp = array();

		foreach($this->errors as $e) {

			$field = ucwords(str_replace(array('_','-'), chr(32), $e['field']));
			$param = $e['param'];

			if( isset(self::$errores[ $e['rule'] ])) 
				$resp[] = eval(self::$errores[$e['rule']]);
			else 
				$resp[] = eval(self::$errores['default']);

		}

		if(!$convert_to_string) {
			return $resp;
		} else {
			$buffer = '';
			foreach($resp as $s) {
				$buffer .= "<span class=\"$error_class\">$s</span>";
			}
			return $buffer;
		}
	}

	/**
	 * Filter the input data according to the specified filter set
	 *
	 * @access public
	 * @param  mixed $input
	 * @param  array $filterset
	 * @return mixed
	 */
	public function filter(array $input, array $filterset)
	{

		foreach($filterset as $field => $filters)
		{
			if(!array_key_exists($field, $input))
			{
				continue;
			}

			$filters = explode('|', $filters);

			foreach($filters as $filter)
			{
				$params = NULL;

				if(strstr($filter, ',') !== FALSE)
				{
					$filter = explode(',', $filter);

					$params = array_slice($filter, 1, count($filter) - 1);

					$filter = $filter[0];
				}

				if(is_callable(array($this, 'filter_'.$filter)))
				{
					$method = 'filter_'.$filter;
					$input[$field] = $this->$method($input[$field], $params);
				}
				else if(function_exists($filter))
				{
					$input[$field] = $filter($input[$field]);
				}
				else if (isset(self::$filter_methods[$filter]))
				{
					$input[$field] = call_user_func(self::$filter_methods[$filter], $input[$field], $params);
				}
				else
				{
					throw new Exception("Filter method '$filter' does not exist.");
				}
			}
		}

		return $input;
	}

	// ** ------------------------- Filters --------------------------------------- ** //

	/**
	 * Replace noise words in a string (http://tax.cchgroup.com/help/Avoiding_noise_words_in_your_search.htm)
	 *
	 * Usage: '<index>' => 'noise_words'
	 *
	 * @access protected
	 * @param  string $value
	 * @param  array $params
	 * @return string
	 */
	protected function filter_noise_words($value, $params = NULL)
	{
		$value = preg_replace('/\s\s+/u', chr(32),$value);

		$value = " $value ";

		$words = explode(',', self::$en_noise_words);

		foreach($words as $word)
		{
			$word = trim($word);

			$word = " $word "; // Normalize

			if(stripos($value, $word) !== FALSE)
			{
				$value = str_ireplace($word, chr(32), $value);
			}
		}

		return trim($value);
	}

	/**
	 * Remove all known punctuation from a string
	 *
	 * Usage: '<index>' => 'rmpunctuataion'
	 *
	 * @access protected
	 * @param  string $value
	 * @param  array $params
	 * @return string
	 */
	protected function filter_rmpunctuation($value, $params = NULL)
	{
		return preg_replace("/(?![.=$'€%-])\p{P}/u", '', $value);
	}

	/**
	 * Translate an input string to a desired language [DEPRECIATED]
	 *
	 * Any ISO 639-1 2 character language code may be used
	 *
	 * See: http://www.science.co.il/language/Codes.asp?s=code2
	 *
	 * @access protected
	 * @param  string $value
	 * @param  array $params
	 * @return string
	 */
	/*
	protected function filter_translate($value, $params = NULL)
	{
		$input_lang  = 'en';
		$output_lang = 'en';

		if(is_null($params))
		{
			return $value;
		}

		switch(count($params))
		{
			case 1:
				$input_lang  = $params[0];
				break;
			case 2:
				$input_lang  = $params[0];
				$output_lang = $params[1];
				break;
		}

		$text = urlencode($value);

		$translation = file_get_contents(
			"http://ajax.googleapis.com/ajax/services/language/translate?v=1.0&q={$text}&langpair={$input_lang}|{$output_lang}"
		);

		$json = json_decode($translation, true);

		if($json['responseStatus'] != 200)
		{
			return $value;
		}
		else
		{
			return $json['responseData']['translatedText'];
		}
	}
	*/

	/**
	 * Sanitize the string by removing any script tags
	 *
	 * Usage: '<index>' => 'sanitize_string'
	 *
	 * @access protected
	 * @param  string $value
	 * @param  array $params
	 * @return string
	 */
	protected function filter_sanitize_string($value, $params = NULL)
	{
		return filter_var($value, FILTER_SANITIZE_STRING);
	}

	/**
	 * Sanitize the string by urlencoding characters
	 *
	 * Usage: '<index>' => 'urlencode'
	 *
	 * @access protected
	 * @param  string $value
	 * @param  array $params
	 * @return string
	 */
	protected function filter_urlencode($value, $params = NULL)
	{
		return filter_var($value, FILTER_SANITIZE_ENCODED);
	}

	/**
	 * Sanitize the string by converting HTML characters to their HTML entities
	 *
	 * Usage: '<index>' => 'htmlencode'
	 *
	 * @access protected
	 * @param  string $value
	 * @param  array $params
	 * @return string
	 */
	protected function filter_htmlencode($value, $params = NULL)
	{
		return filter_var($value, FILTER_SANITIZE_SPECIAL_CHARS);
	}

	/**
	 * Sanitize the string by removing illegal characters from emails
	 *
	 * Usage: '<index>' => 'sanitize_email'
	 *
	 * @access protected
	 * @param  string $value
	 * @param  array $params
	 * @return string
	 */
	protected function filter_sanitize_email($value, $params = NULL)
	{
		return filter_var($value, FILTER_SANITIZE_EMAIL);
	}

	/**
	 * Sanitize the string by removing illegal characters from numbers
	 *
	 * @access protected
	 * @param  string $value
	 * @param  array $params
	 * @return string
	 */
	protected function filter_sanitize_numbers($value, $params = NULL)
	{
		return filter_var($value, FILTER_SANITIZE_NUMBER_INT);
	}

	/**
	 * Filter out all HTML tags except the defined basic tags
	 *
	 * @access protected
	 * @param  string $value
	 * @param  array $params
	 * @return string
	 */
	protected function filter_basic_tags($value, $params = NULL)
	{
		return strip_tags($value, self::$basic_tags);
	}
	
	/**
	 * Convert the provided numeric value to a whole number
	 *
	 * @access protected
	 * @param  string $value
	 * @param  array $params
	 * @return string
	 */
	protected function filter_whole_number($value, $params = NULL)
	{
		return intval($value);
	}	

	// ** ------------------------- Validators ------------------------------------ ** //

	/**
	 * Verify that a value is contained within the pre-defined value set
	 *
	 * Usage: '<index>' => 'contains,value value value'
	 *
	 * @access protected
	 * @param  string $field
	 * @param  array $input
	 * @return mixed
	 */
	protected function validate_contains($field, $input, $param = NULL)
	{
		if(!isset($input[$field]))
		{
			return;
		}

		$param = trim(strtolower($param));

		$value = trim(strtolower($input[$field]));

		if (preg_match_all('#\'(.+?)\'#', $param, $matches, PREG_PATTERN_ORDER)) {
			$param = $matches[1];
		} else  {
			$param = explode(chr(32), $param);
		}

		if(in_array($value, $param)) { // valid, return nothing
			return;
		}

		return array(
			'field' => $field,
			'value' => $value,
			'rule'  => __FUNCTION__,
			'param' => $param
		);
	}

	/**
	 * Check if the specified key is present and not empty
	 *
	 * Usage: '<index>' => 'required'
	 *
	 * @access protected
	 * @param  string $field
	 * @param  array $input
	 * @return mixed
	 */
	protected function validate_required($field, $input, $param = NULL)
	{
    if(isset($input[$field]) && ($input[$field] === false || $input[$field] === 0 || $input[$field] === 0.0 || $input[$field] === "0" || !empty($input[$field])))
		{
			return;
		}

		return array(
		'field' => $field,
		'value' => NULL,
		'rule'  => __FUNCTION__,
		'param' => $param
		);
	}

	/**
	 * Determine if the provided email is valid
	 *
	 * Usage: '<index>' => 'valid_email'
	 *
	 * @access protected
	 * @param  string $field
	 * @param  array $input
	 * @return mixed
	 */
	protected function validate_valid_email($field, $input, $param = NULL)
	{
		if(!isset($input[$field]) || empty($input[$field]))
		{
			return;
		}

		//Si estamos conectados a internet
		//chuequeamos el dominio del email
		$domailExist = true;
		if(!$GLOBALS['toolbox']->es_local()){
			//Existe el dominio?
			$domailExist = false;
			$domain = substr($input[$field], strpos($input[$field], '@') + 1);
			if  (checkdnsrr($domain) !== FALSE) {
			    $domailExist = true;
			}
		}


		if(!filter_var($input[$field], FILTER_VALIDATE_EMAIL) || !$domailExist)
		{
			return array(
				'field' => $field,
				'value' => $input[$field],
				'rule'  => __FUNCTION__,
				'param' => $param
			);
		}
	}

	/**
	 * Determine if the provided value length is less or equal to a specific value
	 *
	 * Usage: '<index>' => 'max_len,240'
	 *
	 * @access protected
	 * @param  string $field
	 * @param  array $input
	 * @return mixed
	 */
	protected function validate_max_len($field, $input, $param = NULL)
	{
		if(!isset($input[$field]))
		{
			return;
		}

		if(function_exists('mb_strlen'))
		{
			if(mb_strlen($input[$field]) <= (int)$param)
			{
				return;
			}
		}
		else
		{
			if(strlen($input[$field]) <= (int)$param)
			{
				return;
			}
		}

		return array(
			'field' => $field,
			'value' => $input[$field],
			'rule'  => __FUNCTION__,
			'param' => $param
		);
	}

	/**
	 * Determine if the provided value length is more or equal to a specific value
	 *
	 * Usage: '<index>' => 'min_len,4'
	 *
	 * @access protected
	 * @param  string $field
	 * @param  array $input
	 * @return mixed
	 */
	protected function validate_min_len($field, $input, $param = NULL)
	{
		if(!isset($input[$field]))
		{
			return;
		}

		if(function_exists('mb_strlen'))
		{
			if(mb_strlen($input[$field]) >= (int)$param)
			{
				return;
			}
		}
		else
		{
			if(strlen($input[$field]) >= (int)$param)
			{
				return;
			}
		}

		return array(
			'field' => $field,
			'value' => $input[$field],
			'rule'  => __FUNCTION__,
			'param' => $param
		);
	}

	/**
	 * Determine if the provided value length matches a specific value
	 *
	 * Usage: '<index>' => 'exact_len,5'
	 *
	 * @access protected
	 * @param  string $field
	 * @param  array $input
	 * @return mixed
	 */
	protected function validate_exact_len($field, $input, $param = NULL)
	{
		if(!isset($input[$field]))
		{
			return;
		}

		if(function_exists('mb_strlen'))
		{
			if(mb_strlen($input[$field]) == (int)$param)
			{
				return;
			}
		}
		else
		{
			if(strlen($input[$field]) == (int)$param)
			{
				return;
			}
		}

		return array(
			'field' => $field,
			'value' => $input[$field],
			'rule'  => __FUNCTION__,
			'param' => $param
		);
	}

	/**
	 * Determine if the provided value contains only alpha characters
	 *
	 * Usage: '<index>' => 'alpha'
	 *
	 * @access protected
	 * @param  string $field
	 * @param  array $input
	 * @return mixed
	 */
	protected function validate_alpha($field, $input, $param = NULL)
	{
		if(!isset($input[$field]) || empty($input[$field]))
		{
			return;
		}

		if(!preg_match("/^([a-zÀÁÂÃÄÅÇÈÉÊËÌÍÎÏÒÓÔÕÖÙÚÛÜÝàáâãäåçèéêëìíîïðòóôõöùúûüýÿ])+$/i", $input[$field]) !== FALSE)
		{
			return array(
				'field' => $field,
				'value' => $input[$field],
				'rule'  => __FUNCTION__,
				'param' => $param
			);
		}
	}

	/**
	 * Determine if the provided value contains only alpha-numeric characters
	 *
	 * Usage: '<index>' => 'alpha_numeric'
	 *
	 * @access protected
	 * @param  string $field
	 * @param  array $input
	 * @return mixed
	 */
	protected function validate_alpha_numeric($field, $input, $param = NULL)
	{
		if(!isset($input[$field]) || empty($input[$field]))
		{
			return;
		}

		if(!preg_match("/^([a-z0-9ÀÁÂÃÄÅÇÈÉÊËÌÍÎÏÒÓÔÕÖÙÚÛÜÝàáâãäåçèéêëìíîïðòóôõöùúûüýÿ])+$/i", $input[$field]) !== FALSE)
		{
			return array(
				'field' => $field,
				'value' => $input[$field],
				'rule'  => __FUNCTION__,
				'param' => $param
			);
		}
	}

	/**
	 * Determine if the provided value contains only alpha characters with dashed and underscores
	 *
	 * Usage: '<index>' => 'alpha_dash'
	 *
	 * @access protected
	 * @param  string $field
	 * @param  array $input
	 * @return mixed
	 */
	protected function validate_alpha_dash($field, $input, $param = NULL)
	{
		if(!isset($input[$field]) || empty($input[$field]))
		{
			return;
		}

		if(!preg_match("/^([a-z0-9ÀÁÂÃÄÅÇÈÉÊËÌÍÎÏÒÓÔÕÖÙÚÛÜÝàáâãäåçèéêëìíîïðòóôõöùúûüýÿ_-])+$/i", $input[$field]) !== FALSE)
		{
			return array(
				'field' => $field,
				'value' => $input[$field],
				'rule'  => __FUNCTION__,
				'param' => $param
			);
		}
	}

	/**
	 * Determine if the provided value is a valid number or numeric string
	 *
	 * Usage: '<index>' => 'numeric'
	 *
	 * @access protected
	 * @param  string $field
	 * @param  array $input
	 * @return mixed
	 */
	protected function validate_numeric($field, $input, $param = NULL)
	{
		if(!isset($input[$field]) || empty($input[$field]))
		{
			return;
		}

		if(!is_numeric($input[$field]))
		{
			return array(
				'field' => $field,
				'value' => $input[$field],
				'rule'  => __FUNCTION__,
				'param' => $param
			);
		}
	}

	/**
	 * Determine if the provided value is a valid integer
	 *
	 * Usage: '<index>' => 'integer'
	 *
	 * @access protected
	 * @param  string $field
	 * @param  array $input
	 * @return mixed
	 */
	protected function validate_integer($field, $input, $param = NULL)
	{
		if(!isset($input[$field]) || empty($input[$field]))
		{
			return;
		}

		if(!preg_match('/^\-?[0-9]+$/', $input[$field]))
		{
			return array(
				'field' => $field,
				'value' => $input[$field],
				'rule'  => __FUNCTION__,
				'param' => $param
			);
		}
	}

	/**
	 * Determine if the provided value is a PHP accepted boolean
	 *
	 * Usage: '<index>' => 'boolean'
	 *
	 * @access protected
	 * @param  string $field
	 * @param  array $input
	 * @return mixed
	 */
	protected function validate_boolean($field, $input, $param = NULL)
	{
		if(!isset($input[$field]) || empty($input[$field]))
		{
			return;
		}

		$bool = filter_var($input[$field], FILTER_VALIDATE_BOOLEAN);

		if(!is_bool($bool))
		{
			return array(
				'field' => $field,
				'value' => $input[$field],
				'rule'  => __FUNCTION__,
				'param' => $param
			);
		}
	}

	/**
	 * Determine if the provided value is a valid float
	 *
	 * Usage: '<index>' => 'float'
	 *
	 * @access protected
	 * @param  string $field
	 * @param  array $input
	 * @return mixed
	 */
	protected function validate_float($field, $input, $param = NULL)
	{
		if(!isset($input[$field]) || empty($input[$field]))
		{
			return;
		}

		//Habilitamos la coma como separador de decimal
		$input[$field] = str_replace(',', '.', $input[$field]);

		//Matcheamos
		$matches = preg_match('/^[+\-]?([0-9]+,)*[0-9]+(\.[0-9]+)?$/', $input[$field]);

		if(!$matches){
			return array(
				'field' => $field,
				'value' => $input[$field],
				'rule'  => __FUNCTION__,
				'param' => $param
			);
		}
	}

	/**
	 * Determine if the provided value is a valid URL
	 *
	 * Usage: '<index>' => 'valid_url'
	 *
	 * @access protected
	 * @param  string $field
	 * @param  array $input
	 * @return mixed
	 */
	protected function validate_valid_url($field, $input, $param = NULL)
	{
		if(!isset($input[$field]) || empty($input[$field]))
		{
			return;
		}

		if(!filter_var($input[$field], FILTER_VALIDATE_URL))
		{
			return array(
				'field' => $field,
				'value' => $input[$field],
				'rule'  => __FUNCTION__,
				'param' => $param
			);
		}
	}




	/**
	 * Determine if a URL exists & is accessible
	 *
	 * Usage: '<index>' => 'url_exists'
	 *
	 * @access protected
	 * @param  string $field
	 * @param  array $input
	 * @return mixed
	 */
	protected function validate_url_exists($field, $input, $param = NULL)
	{
		if(!isset($input[$field]) || empty($input[$field]))
		{
			return;
		}
		
		$url = parse_url(strtolower($input[$field]));
		
		if(isset($url['host'])) {
			$url = $url['host'];
		}

		if(function_exists('checkdnsrr'))
		{
			if(checkdnsrr($url) === false)
			{
				return array(
					'field' => $field,
					'value' => $input[$field],
					'rule'  => __FUNCTION__,
					'param' => $param
				);
			}
		}
		else
		{
			if(gethostbyname($url) == $url)
			{
				return array(
					'field' => $field,
					'value' => $input[$field],
					'rule'  => __FUNCTION__,
					'param' => $param
				);
			}
		}
	}

	/**
	 * Determine if the provided value is a valid IP address
	 *
	 * Usage: '<index>' => 'valid_ip'
	 *
	 * @access protected
	 * @param  string $field
	 * @param  array $input
	 * @return mixed
	 */
	protected function validate_valid_ip($field, $input, $param = NULL)
	{
		if(!isset($input[$field]) || empty($input[$field]))
		{
			return;
		}

		if(!filter_var($input[$field], FILTER_VALIDATE_IP) !== FALSE)
		{
			return array(
				'field' => $field,
				'value' => $input[$field],
				'rule'  => __FUNCTION__,
				'param' => $param
			);
		}
	}

	/**
	 * Determine if the provided value is a valid IPv4 address
	 *
	 * Usage: '<index>' => 'valid_ipv4'
	 *
	 * @access protected
	 * @param  string $field
	 * @param  array $input
	 * @return mixed
	 * @see http://pastebin.com/UvUPPYK0
	 */

	/*
	 * What about private networks? http://en.wikipedia.org/wiki/Private_network
	 * What about loop-back address? 127.0.0.1
	 */
	protected function validate_valid_ipv4($field, $input, $param = NULL)
	{
		if(!isset($input[$field]) || empty($input[$field]))
		{
			return;
		}

		if(!filter_var($input[$field], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) // removed !== FALSE
		{ // it passes
			return array(
				'field' => $field,
				'value' => $input[$field],
				'rule'  => __FUNCTION__,
				'param' => $param
			);
		}
	}

	/**
	 * Determine if the provided value is a valid IPv6 address
	 *
	 * Usage: '<index>' => 'valid_ipv6'
	 *
	 * @access protected
	 * @param  string $field
	 * @param  array $input
	 * @return mixed
	 */
	protected function validate_valid_ipv6($field, $input, $param = NULL)
	{
		if(!isset($input[$field]) || empty($input[$field]))
		{
			return;
		}

		if(!filter_var($input[$field], FILTER_VALIDATE_IP, FILTER_FLAG_IPV6))
		{
			return array(
				'field' => $field,
				'value' => $input[$field],
				'rule'  => __FUNCTION__,
				'param' => $param
			);
		}
	}

	/**
	 * Determine if the input is a valid credit card number
	 *
	 * See: http://stackoverflow.com/questions/174730/what-is-the-best-way-to-validate-a-credit-card-in-php
	 * Usage: '<index>' => 'valid_cc'
	 *
	 * @access protected
	 * @param  string $field
	 * @param  array $input
	 * @return mixed
	 */
	protected function validate_valid_cc($field, $input, $param = NULL)
	{
		if(!isset($input[$field]) || empty($input[$field]))
		{
			return;
		}

		$number = preg_replace('/\D/', '', $input[$field]);

		if(function_exists('mb_strlen'))
		{
			$number_length = mb_strlen($number);
		}
		else
		{
			$number_length = strlen($number);
		}

		$parity = $number_length % 2;

		$total = 0;

		for($i = 0; $i < $number_length; $i++)
		{
			$digit = $number[$i];

			if ($i % 2 == $parity)
			{
				$digit *= 2;

				if ($digit > 9)
				{
					$digit -= 9;
				}
			}

			$total += $digit;
		}

		if($total % 10 == 0)
		{
			return; // Valid
		}

		return array(
			'field' => $field,
			'value' => $input[$field],
			'rule'  => __FUNCTION__,
			'param' => $param
		);
	}

	/**
	 * Determine if the input is a valid human name [Credits to http://github.com/ben-s]
	 *
	 * See: https://github.com/Wixel/GUMP/issues/5
	 * Usage: '<index>' => 'valid_name'
	 *
	 * @access protected
	 * @param  string $field
	 * @param  array $input
	 * @return mixed
	 */
	protected function validate_valid_name($field, $input, $param = NULL)
	{
		if(!isset($input[$field])|| empty($input[$field]))
		{
			return;
		}

		if(!preg_match("/^([a-zÀÁÂÃÄÅÇÈÉÊËÌÍÎÏÒÓÔÕÖÙÚÛÜÝàáâãäåçèéêëìíîïñðòóôõöùúûüýÿ '-])+$/i", $input[$field]) !== FALSE)
		{
			return array(
				'field' => $field,
				'value' => $input[$field],
				'rule'  => __FUNCTION__,
				'param' => $param
			);
		}
	}

	/**
	 * Determine if the provided input is likely to be a street address using weak detection
	 *
	 * Usage: '<index>' => 'street_address'
	 *
	 * @access protected
	 * @param  string $field
	 * @param  array $input
	 * @return mixed
	 */
	protected function validate_street_address($field, $input, $param = NULL)
	{
		if(!isset($input[$field])|| empty($input[$field]))
		{
			return;
		}

		// Theory: 1 number, 1 or more spaces, 1 or more words
		$hasLetter = preg_match('/[a-zA-Z]/', $input[$field]);
		$hasDigit  = preg_match('/\d/'      , $input[$field]);
		$hasSpace  = preg_match('/\s/'      , $input[$field]);

		$passes = $hasLetter && $hasDigit && $hasSpace;

		if(!$passes) {
			return array(
				'field' => $field,
				'value' => $input[$field],
				'rule'  => __FUNCTION__,
				'param' => $param
			);
		}
	}

	/**
	 * Determine if the provided value is a valid IBAN
	 *
	 * Usage: '<index>' => 'iban'
	 *
	 * @access protected
	 * @param  string $field
	 * @param  array $input
	 * @return mixed
	 */
	protected function validate_iban($field, $input, $param = NULL)
	{
		if(!isset($input[$field]) || empty($input[$field]))
		{
			return;
		}

		static $character = array (
			'A' => 10, 'C' => 12, 'D' => 13, 'E' => 14, 'F' => 15, 'G' => 16,
			'H' => 17, 'I' => 18, 'J' => 19, 'K' => 20, 'L' => 21, 'M' => 22,
			'N' => 23, 'O' => 24, 'P' => 25, 'Q' => 26, 'R' => 27, 'S' => 28,
			'T' => 29, 'U' => 30, 'V' => 31, 'W' => 32, 'X' => 33, 'Y' => 34,
			'Z' => 35,
		);

		if (!preg_match("/\A[A-Z]{2}\d{2} ?[A-Z\d]{4}( ?\d{4}){1,} ?\d{1,4}\z/", $input[$field])) {
			return array(
				'field' => $field,
				'value' => $input[$field],
				'rule'  => __FUNCTION__,
				'param' => $param
			);
		}

		$iban = str_replace(' ', '', $input[$field]);
		$iban = substr($iban, 4) . substr($iban, 0, 4);
		$iban = strtr($iban, $character);

		if(bcmod($iban, 97) != 1){
			return array(
				'field' => $field,
				'value' => $input[$field],
				'rule'  => __FUNCTION__,
				'param' => $param
			);
		}
	}

	/**
	 * Determine if the provided input is a valid date (ISO 8601)
	 *
	 * Usage: '<index>' => 'date'
	 *
	 * @access protected
	 * @param string $field
	 * @param string $input date ('Y-m-d') or datetime ('Y-m-d H:i:s')
	 * @param null   $param
	 *
	 * @return mixed
	 */
	protected function validate_date($field, $input, $param = null)
	{
		if (!isset($input[$field]) || empty($input[$field])) {
			return;
		}

		$cdate1 = date('Y-m-d', strtotime($input[$field]));
		$cdate2 = date('Y-m-d H:i:s', strtotime($input[$field]));


		if ($cdate1 != $input[$field] && $cdate2 != $input[$field]) {
			return array(
				'field' => $field,
				'value' => $input[$field],
				'rule'  => __FUNCTION__,
				'param' => $param
			);
		}
	}

	/**
	 * Determine if the provided numeric value is lower or equal to a specific value
	 *
	 * Usage: '<index>' => 'max_numeric,50'
	 *
	 * @access protected
	 * @param string $field
	 * @param array  $input
	 * @param null   $param
	 *
	 * @return mixed
	 */
	protected function validate_max_numeric($field, $input, $param = null)
	{
		if (!isset($input[$field]) || empty($input[$field])) {
			return;
		}

		if (is_numeric($input[$field]) && is_numeric($param) && ($input[$field] <= $param)) {
			return;
		}

		return array(
			'field' => $field,
			'value' => $input[$field],
			'rule'  => __FUNCTION__,
			'param' => $param
		);
	}

	/**
	 * Determine if the provided numeric value is higher or equal to a specific value
	 *
	 * Usage: '<index>' => 'min_numeric,1'
	 *
	 * @access protected
	 * @param string $field
	 * @param array  $input
	 * @param null   $param
	 *
	 * @return mixed
	 */
	protected function validate_min_numeric($field, $input, $param = null)
	{
		if (!isset($input[$field]) || empty($input[$field])) {
			return;
		}

		if (is_numeric($input[$field]) && is_numeric($param) && ($input[$field] >= $param)) {
			return;
		}

		return array(
			'field' => $field,
			'value' => $input[$field],
			'rule'  => __FUNCTION__,
			'param' => $param
		);
	}

	/**
	 * Trims whitespace only when the value is a scalar
	 *
	 * @param mixed $value
	 * @return mixed
	 */
	private function trimScalar($value)
	{
		if (is_scalar($value)) {
			$value = trim($value);
		}

		return $value;
	}













	//VALIDADORES Y FILTROS PERSONALIZADOS






	/**
	 * Chequea si la cantidad de cifras decimales es correcta
	 *
	 * Usage: '<precio>' => 'cantidad_dec,1'
	 *
	 * @access protected
	 * @param string $field
	 * @param array  $input
	 * @param null   $param
	 *
	 * @return mixed
	 */
	protected function validate_cantidad_dec($field, $input, $param = NULL)
	{
		//Si no esta definido ningun valor
		if(!isset($input[$field]) || empty($input[$field])){
			return;
		}

		//La coma la convertimos en punto
		$valor = str_replace(',', '.', $input[$field]);

		//Si existen cifras decimales..
		if(strpos($valor,'.') !== false){
			$valor = explode('.', $valor);
			$decimales = strlen($valor[1]);
			//validamos
			if((int)$param[0] == $decimales){
				return;
			}
		}


		return array(
			'field' => $field,
			'value' => $input[$field],
			'rule'  => __FUNCTION__,
			'param' => $param
		);
	}

	/**
	 * Valida contra Expresiones regulares
	 *
	 * Usage: '<precio>' => 'regex,/asdasd#coma#/'
	 *
	 * @access protected
	 * @param string $field
	 * @param array  $input
	 * @param null   $param
	 *
	 * @return mixed
	 */
	protected function validate_regex($field, $input, $param = NULL)
	{
		//Si no esta definido ningun valor
		if(!isset($input[$field]) || empty($input[$field])){
			return;
		}

		//Convertimos las comas de la expresion regular
		$regex = str_replace('_##coma##_', ',', $param[0]);

		echo "----".$regex."----";

		if(!preg_match($regex, $input[$field])){
			return array(
				'field' => $field,
				'value' => $input[$field],
				'rule'  => __FUNCTION__,
				'param' => $param
			);
		}
	}


	/**
	 * Chequea que el valor del campo exista entre los permitidos
	 *
	 * Usage: '<precio>' => 'valores_permitidos,{arreglo JSON stringificado}'
	 *
	 * @access protected
	 * @param string $field
	 * @param array  $input
	 * @param null   $param
	 *
	 * @return mixed
	 */
	protected function validate_valores_permitidos($field, $input, $param = NULL)
	{
		//Si no esta definido ningun valor
		if(!isset($input[$field]) || empty($input[$field])){
			return;
		}

		//Convertimos las comas de la expresion regular
		$valores = json_decode(str_replace('_##coma##_', ',', $param));

		if(!in_array($input[$field], $valores)){
			return array(
				'field' => $field,
				'value' => $input[$field],
				'rule'  => __FUNCTION__,
				'param' => $param
			);
		}
	}


	/**
	 * Chequea que el valor pertenezca a la lista de no permitidos
	 *
	 * Usage: '<precio>' => 'regex,/asdasd#coma#/'
	 *
	 * @access protected
	 * @param string $field
	 * @param array  $input
	 * @param null   $param
	 *
	 * @return mixed
	 */
	protected function validate_valores_no_permitidos($field, $input, $param = NULL)
	{
		//Si no esta definido ningun valor
		if(!isset($input[$field]) || empty($input[$field])){
			return;
		}

		//Convertimos las comas de la expresion regular
		$valores = json_decode(str_replace('_##coma##_', ',', $param));

		if(in_array($input[$field], $valores)){
			return array(
				'field' => $field,
				'value' => $input[$field],
				'rule'  => __FUNCTION__,
				'param' => $param
			);
		}
	}

	/**
	 * Chequea el valor contra una expresion regular
	 *
	 * Usage: '<precio>' => 'regexp,{espresion regular}'
	 *
	 * @access protected
	 * @param string $field
	 * @param array  $input
	 * @param null   $param
	 *
	 * @return mixed
	 */
	protected function validate_regexp($field, $input, $param = NULL)
	{
		//Si no esta definido ningun valor
		if(!isset($input[$field]) || empty($input[$field]) || ($input[$field] == '')){
			return;
		}

		//Convertimos las comas de la expresion regular
		$regex = str_replace('_##coma##_', ',', $param);
		$regex = str_replace('_##palo##_', '|', $regex);
		//echo $regex;
		
		if(!preg_match($regex, $input[$field])){
			return array(
				'field' => $field,
				'value' => $input[$field],
				'rule'  => __FUNCTION__,
				'param' => $param
			);
		}
	}

	/**
	 * Chequea si el string es convertible a un arreglo JSON
	 *
	 * Usage: '<precio>' => 'json,{string JSON}'
	 *
	 * @access protected
	 * @param string $field
	 * @param array  $input
	 * @param null   $param
	 *
	 * @return mixed
	 */
	protected function validate_json($field, $input, $param = NULL)
	{
		//Si no esta definido ningun valor
		if(!isset($input[$field]) || empty($input[$field])){
			return;
		}

		//Convertimos las comas de la expresion regular
		$json = json_decode(str_replace('_##coma##_', ',', $input[$field]), true);
		
		if(!is_array($json)){
			return array(
				'field' => $field,
				'value' => $input[$field],
				'rule'  => __FUNCTION__,
				'param' => $param
			);
		}
	}


	/**
	 * Chequea el valor contiene caracteres no validos
	 *
	 * Usage: '<precio>' => 'json,{string JSON}'
	 *
	 * @access protected
	 * @param string $field
	 * @param array  $input
	 * @param null   $param
	 *
	 * @return mixed
	 */
	protected function validate_caracteres_permitidos($field, $input, $param = NULL)
	{
		//Si no esta definido ningun valor
		if(!isset($input[$field]) || empty($input[$field])){
			return;
		}

		//Convertimos las comas de la expresion regular
		$filtro = str_replace('_##coma##_', ',', $param)."|^";
		//valor temporal
		$tempval = $input[$field];



		//Quitamos todos los caracteres
		//del filtro
		foreach(str_split($filtro) as $c)
			$tempval = str_replace($c, '',	$tempval);
		


		//Si aun quedan caracteres
		//entonces este es invalido
		if(strlen($tempval)){
			return array(
				'field' => $field,
				'value' => $input[$field],
				'rule'  => __FUNCTION__,
				'param' => $param
			);
		}
	}

	/**
	* checks if a file was uploaded
	* 
	* Usage: '<index>' => 'required_file'
	*	
	* @access protected
	* @param  string $field
	* @param  array $input
	* @return mixed
	*/
	protected function validate_required_file($field, $input, $param = NULL)
	{
		//Si directamente no lo setearon
		if(is_array($input)){
		  return;
		}

		//O dio error...
		if ($input[$field]['error'] !== 4) {
		  return;
		}

		return array(
		  'field' => $field,
		  'value' => $input[$field],
		  'rule' => __FUNCTION__,
		  'param' => $param
		);
	}
	      
	/**
	* check the uploaded file for extension 
	       * for now checks onlt the ext should add mime type check
	* 
	* Usage: '<index>' => 'starts,Z'
	*	
	* @access protected
	* @param  string $field
	* @param  array $input
	* @return mixed
	*/
	protected function validate_extension($field, $input, $param = NULL){
	if(isset($input[$field]['error']))
	  if ($input[$field]['error'] !== 4) {
	      $param = trim(strtolower($param));
	      $allowed_extensions = explode(";", $param);
	      $path_info = pathinfo($input[$field]['name']);
	      $extension = strtolower($path_info['extension']);

	      if (in_array($extension, $allowed_extensions)) {

			//Documentos
			$mime['doc'] = 'application/msword';
			$mime['bin'] = 'application/octet-stream';
			$mime['exe'] = 'application/octet-stream';
			$mime['pdf'] = 'application/pdf';
			$mime['ps'] = 'application/postscript';
			$mime['ai'] = 'application/postscript';
			$mime['eps'] = 'application/postscript';
			$mime['rtf'] = 'application/rtf';

			//Audio
			$mime['aiff'] = 'audio/x-aiff';
			$mime['ua'] = 'audio/basic';
			$mime['midi'] = 'audio/x-midi';
			$mime['midi'] = 'audio/x-midi';
			$mime['wav'] = 'audio/x-wav';
			$mime['mp3'] = 'audio/mp3';

			//Imagen
			$mime['bmp'] = 'image/bmp';
			$mime['gif'] = 'image/gif';
			$mime['png'] = 'image/png';
			$mime['jpeg'] = 'image/jpeg';
			$mime['jpg'] = 'image/jpeg';
			$mime['jpe'] = 'image/jpeg';
			$mime['tiff'] = 'image/tiff';
			$mime['tiff'] = 'image/tiff';
			$mime['tif'] = 'image/tiff';
			$mime['xbm'] = 'image/x-xbitmap';
			$mime['ico'] = 'image/vnd.microsoft.icon';
			$mime['svg'] = 'image/svg+xml';
			$mime['svgz'] = 'image/svg+xml';

			//Comprimidos
			$mime['zip'] = 'application/zip';
			$mime['rar'] = 'application/x-rar-compressed';
			$mime['exe'] = 'application/x-msdownload';
			$mime['msi'] = 'application/x-msdownload';
			$mime['cab'] = 'application/vnd.ms-cab-compressed';
			$mime['tar'] = 'application/x-tar';
			$mime['gtar'] = 'application/x-gtar';
			$mime['gz'] = 'application/x-gzip';


			//Texto
			$mime['htm'] = 'text/html';
			$mime['html'] = 'text/html';
			$mime['txt'] = 'text/plain';
			$mime['rtf'] = 'text/richtext';
			$mime['rtx'] = 'text/richtext';

			//Video
			$mime['mpg'] = 'video/mpeg';
			$mime['mpeg'] = 'video/mpeg';
			$mime['mpe'] = 'video/mpeg';
			$mime['viv'] = 'video/vnd.vivo';
			$mime['vivo'] = 'video/vnd.vivo';
			$mime['qt'] = 'video/quicktime';
			$mime['mov'] = 'video/quicktime';
			$mime['avi'] = 'video/x-msvideo';
			$mime['mp4'] = 'video/mp4';

            // adobe
			$mime['pdf'] = 'application/pdf';
			$mime['psd'] = 'image/vnd.adobe.photoshop';
			$mime['ai'] = 'application/postscript';
			$mime['eps'] = 'application/postscript';
			$mime['ps'] = 'application/postscript';

			//Office
			$mime['doc'] = 'application/msword';
			$mime['rtf'] = 'application/rtf';
			$mime['xls'] = 'application/vnd.ms-excel';
			$mime['ppt'] = 'application/vnd.ms-powerpoint';

			//Office nuevos
			$mime['pptx'] = 'application/vnd.openxmlformats-officedocument.presentationml.presentation';
			$mime['sldx'] = 'application/vnd.openxmlformats-officedocument.presentationml.slide';
			$mime['ppsx'] = 'application/vnd.openxmlformats-officedocument.presentationml.slideshow';
			$mime['potx'] = 'application/vnd.openxmlformats-officedocument.presentationml.template';
			$mime['xlsx'] = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
			$mime['xltx'] = 'application/vnd.openxmlformats-officedocument.spreadsheetml.template';
			$mime['docx'] = 'application/vnd.openxmlformats-officedocument.wordprocessingml.document';
			$mime['dotx'] = 'application/vnd.openxmlformats-officedocument.wordprocessingml.template';

			//Si el mime es correcto
			if($input[$field]['type'] == $mime[$extension])
	          return;
	      }
	      
	      return array(
	          'field' => $field,
	          'value' => $input[$field],
	          'rule' => __FUNCTION__,
	          'param' => $param
	      );
	  }
	}

	/**
	* check the uploaded file for max size
	* 
	* Usage: '<index>' => 'archivo_maxsize,Z'
	*	
	* @access protected
	* @param  string $field
	* @param  array $input
	* @return mixed
	*/
	protected function validate_archivo_maxsize($field, $input, $param = NULL)
	{
		if(isset($input[$field]['error']))
			if ($input[$field]['error'] !== 4) {

			  //Mientras que el tamaño sea menor al
			  //permitido continuamos.
			  if($input[$field]['size'] < (int)$param)
			  	return;
			  
			  return array(
			      'field' => $field,
			      'value' => $input[$field],
			      'rule' => __FUNCTION__,
			      'param' => $param
			  );
			}
	}


	/**
	* Mover archivo subido al nuevo directorio
	* 
	* Usage: '<index>' => 'archivo_maxsize,Z'
	*	
	* @access protected
	* @param  string $field
	* @param  array $input
	* @return mixed
	*/
	protected function validate_archivo_mover($field, $input, $param = NULL)
	{
		if(isset($input[$field]['error']))
			if ($input[$field]['error'] !== 4) {
				
				//Obtenemos la info del directorio
				$archivo = pathinfo($param);


				//Si es un directorio el parametro
				//movemos el archivo manteniendo su
				//nombre original.
				if(!isset($archivo['extension'])){

					//Si no tiene barra al final se la agregamos
					$path = rtrim(trim($param), '/') . '/' ;


					//Comprobamos que el directorio exista y si lo pudimos mover con exito salimos de la comprobacion
					if( (file_exists($path)) && move_uploaded_file($input[$field]['tmp_name'], utf8_decode($path.$input[$field]['name'])) ){
						return;
					}

				//Sino buscamos el nombre de archivo
				//detallado en el path y lo tratamos
				//de guardar con ese nombre en el server.
				}else{

					$archivo_entrada = pathinfo($input[$field]['name']);

					$path = rtrim($archivo["dirname"], '/') . '/' ;

					if( (file_exists($path)) && move_uploaded_file($input[$field]['tmp_name'], $path.$archivo['filename'].'.'.$archivo_entrada['extension']) ){
						return;
					}
				}
			}
	      

	      
	      return array(
	          'field' => $field,
	          'value' => $input[$field],
	          'rule' => __FUNCTION__,
	          'param' => $param
	      );
	 }





	//FILTROS





	/**
	 * Quita los caracteres que no son necesarios
	 *
	 * @access protected
	 * @param  string $value
	 * @param  array $params
	 * @return string
	 */
	protected function filter_archivo_mover($value, $params = NULL)
	{


		if( isset($value['error']) && ($value['error'] !== 4)){
			
			//Obtenemos la info del directorio
			$archivo = pathinfo($params[0]);


			//Si es un directorio el parametro
			//movemos el archivo manteniendo su
			//nombre original.
			if(!isset($archivo['extension'])){

				//Si no tiene barra al final se la agregamos
				$path = rtrim($params[0], '/') . '/' ;

				$value['nuevo_directorio'] = $path;

			//Sino buscamos el nombre de archivo
			//detallado en el path y lo tratamos
			//de guardar con ese nombre en el server.
			}else{

				$archivo_entrada = pathinfo($value['name']);

				$path = rtrim($archivo["dirname"], '/') . '/' ;

				$value['nuevo_directorio'] = $path;
				$value['name'] = $archivo['filename'].'.'.$archivo_entrada['extension'];
			}
		}

	    return $value;
	}




	/**
	 * Quita los caracteres que no son necesarios
	 *
	 * @access protected
	 * @param  string $value
	 * @param  array $params
	 * @return string
	 */
	protected function filter_caracteres_permitidos($value, $params = NULL)
	{
		$params[0] = str_replace('_##coma##_', ',', $params[0])."|^";
		//echo "valor inciial: " .$value;
		//echo "valor final: ".$GLOBALS['toolbox']->filtrar($value,$params[0]);

	    return $GLOBALS['toolbox']->filtrar($value,$params[0]);
	}



	/**
	 * Quita los tildes y caracteres raros
	 *
	 * @access protected
	 * @param  string $value
	 * @param  array $params
	 * @return string
	 */
	protected function filter_stripAccents($value, $params = NULL)
	{
		return $GLOBALS['toolbox']->stripAccents($value);
	}



	/**
	 * Quita los tildes y caracteres raros
	 *
	 * @access protected
	 * @param  string $value
	 * @param  array $params
	 * @return string
	 */
	protected function filter_upper($value, $params = NULL)
	{
		return strtoupper($value);
	}



	/**
	 * Quita los tildes y caracteres raros
	 *
	 * @access protected
	 * @param  string $value
	 * @param  array $params
	 * @return string
	 */
	protected function filter_lower($value, $params = NULL)
	{
		return strtolower($value);
	}



	/**
	 * Trunca el string a un tamaño especifico
	 *
	 * @access protected
	 * @param  string $value
	 * @param  array $params
	 * @return string
	 */
	protected function filter_trimAt($value, $params = NULL)
	{
		//Cortamos al maximo de caracteres
		if(strlen($value) < (int)$params[0])
			$value = substr($value, 0, (int)$params[0]);
		//retornamos valor
	    return $value;
	}



	/**
	 * convertimos a decimal
	 *
	 * @access protected
	 * @param  string $value
	 * @param  array $params
	 * @return string
	 */
	protected function filter_toFloat($value, $params = NULL)
	{

		//Si estan unsando comas las reemplazamos por puntos
		$value = str_replace(',', '.', $value);

	    //Quitamos los puntos duplicados
	    $pos = strpos($value,'.');
	    if($pos !== false)
	        $value = substr($value,0,$pos+1) . str_replace('.','',substr($value,$pos+1));

	    //Damos formato al numero
	    $value = number_format(	(float)$value, 		//Número
	    						(int)$params[0], 	//Cantidad de decimales
	    						'.',				//Separador de decimales 
	    						'');				//Separador de miles

		//retornamos valor
	    return $value;
	}




	/**
	 * convertimos a entero
	 *
	 * @access protected
	 * @param  string $value
	 * @param  array $params
	 * @return string
	 */
	protected function filter_toInteger($value, $params = NULL)
	{
		//retornamos valor
		if($value == '') return '';
	    return (string)intval($value);
	}




	/**
	 * Convierte el string JSON a un array
	 *
	 * @access protected
	 * @param  string $value
	 * @param  array $params
	 * @return mixed
	 */
	protected function filter_toArray($value, $params = NULL)
	{
		//Array
		$value = json_decode($value, true);

		//Devolvemos el arreglo
		if(is_array($value)) return $value;
		
		return '';
	}



	/**
	 * Quita los valores especificados en el arreglo JSON
	 *
	 * @access protected
	 * @param  string $value
	 * @param  array $params
	 * @return string
	 */
	protected function filter_removeValues($value, $params = NULL)
	{
		$params[0] = str_replace('_##coma##_', ',', $params[0]);
		$valores = json_decode($params[0]);

		//Si no tiene valores no permitidos devolvemos valor orig
		if(is_array($valores) && !in_array($value, $valores)) 
			return $value;

		//Sino retornamos vacio
		return '';
	}



	/**
	 * Trunca el string a un tamaño especifico
	 *
	 * @access protected
	 * @param  string $value
	 * @param  array $params
	 * @return string
	 */
	protected function filter_allowedValues($value, $params = NULL)
	{
		//echo $params[0];
		$params[0] = str_replace('_##coma##_', ',', $params[0]);
		$valores = json_decode($params[0]);

		//Si el valor es permitido
		//devolvemos tal cual esta
		if(in_array($value, $valores)) 
			return $value;

		//sino vacio
		return '';
	}



	/**
	 * Trunca el string a un tamaño especifico
	 *
	 * @access protected
	 * @param  string $value
	 * @param  array $params
	 * @return string
	 */
	protected function filter_regexp($value, $params = NULL)
	{

		//Si viene vacio es porque 
		//el campo no es obligatorio
		if($value == '') return '';

		//echo $value;
		$regex = str_replace('_##coma##_', ',', $params[0]);
		$regex = str_replace('_##palo##_', '|', $regex);
		$resultado = preg_match($regex, $value, $matches);

		if($resultado) return $matches[0];
		else return '';
	}



	/**
	 * Si el valor es menor que el minimo devolvemos el minimo
	 *
	 * @access protected
	 * @param  string $value
	 * @param  array $params
	 * @return string
	 */
	protected function filter_minimo($value, $params = NULL)
	{
		//Si viene vacio es porque 
		//el campo no es obligatorio
		if($value == '') return '';

		//Si esta OK
		if(is_numeric($value))
			//y es mayor que el minimo
			if($value >= $params[0])
				//Esta ok el valor
				return $value;

		//Sino devolvemos el minimo
		return $params[0];
	}




	/**
	 * Si el valor es mayor que el maximo devolvemos el maximo
	 *
	 * @access protected
	 * @param  string $value
	 * @param  array $params
	 * @return string
	 */
	protected function filter_maximo($value, $params = NULL)
	{
		//Si viene vacio es porque 
		//el campo no es obligatorio
		if($value == '') return '';

		//Si esta OK
		if(is_numeric($value))
			//y es mayor que el minimo
			if($value <= $params[0])
				//Esta ok el valor
				return $value;

		//Sino devolvemos el minimo
		return $params[0];
	}




	/**
	 * Maxima cantidad de caracteres del valor
	 *
	 * @access protected
	 * @param  string $value
	 * @param  array $params
	 * @return string
	 */
	protected function filter_max_caracteres($value, $params = NULL)
	{
		//Si sobrepasamos el maximo de caracteres
		if(strlen($value) > (int)$params[0])
			//Cortamos el string
			return substr($value, 0, (int)$params[0]);

		//Sino devolvemos
		//el valor original
		return $value;
	}



	/**
	 * Escapamos las comillas simples para MYSQL
	 *
	 * @access protected
	 * @param  string $value
	 * @param  array $params
	 * @return string
	 */
	protected function filter_escapar_comillas_simples($value, $params = NULL)
	{
		//Devolvemos el valor sanatizado
		return str_replace('\'', '\\\'', $value);
	}



	/**
	 * Escapamos las barras invertidas para MYSQL
	 *
	 * @access protected
	 * @param  string $value
	 * @param  array $params
	 * @return string
	 */
	protected function filter_escapar_barras_invertidas($value, $params = NULL)
	{
		//Devolvemos el valor sanatizado
		return str_replace("\\", "\\\\", $value);
	}

} // EOC
