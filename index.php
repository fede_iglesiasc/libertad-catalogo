<?php


// Archivo de configuracion
include_once('config.php');
include_once('toolbox.php');
include_once('classes/class.session.php');
include_once('classes/class.mail.php');
include_once('classes/class.result.php');
include_once('classes/class.field.php');
include_once('classes/rain.tpl.class.php');
include_once('modules/auth.php');
include_once('modules/cuenta.php');


//Configuramos motor de template
raintpl::$tpl_dir = "tpl/"; // template directory
raintpl::$cache_dir = "tmp/"; // cache directory
raintpl::configure( 'tpl_ext', 'tpl' ); //extension de los templates
raintpl::configure( 'path_replace', false );
raintpl::configure( "php_enabled", true ); //Permite codigo php en el template


preg_match('/MSIE (.*?);/', $_SERVER['HTTP_USER_AGENT'], $matches);
if(count($matches)<2){
  preg_match('/Trident\/\d{1,2}.\d{1,2}; rv:([0-9]*)/', $_SERVER['HTTP_USER_AGENT'], $matches);
}

if(count($matches)>1) $version = $matches[1];

if (count($matches)>1){
  //Then we're using IE
  $version = $matches[1];

  if($version<=9){
      $tpl = new RainTPL;
      $html = $tpl->draw( 'updateBrowser', $return_string = true );
      echo $html;
      exit;
  }
}


//Si no estamos logueados
if( !$GLOBALS['session']->getData('usuario') ){

    //Main template
    $login = new RainTPL;

    //Print main Template
    $html = $login->draw( 'web', $return_string = true );
    echo $html;
    exit;
}

//Si estamos logueados
if($GLOBALS['session']->getData('usuario')){

  //Main Template
  $template = new RainTPL;

  //Creamos el menu
  $GLOBALS['toolbox']->createMenu();

  //Convertimos el menu a HTML
  $GLOBALS['menuHTML'] = $GLOBALS['toolbox']->menuToHTML();

  //Nuevo template vacio
  $tpl = new RainTPL;

  //Inyectamos todos los subtemplates en el principal
  $template->assign( "aplicaciones", $tpl->draw( 'aplicaciones', $return_string = true ) );
  $template->assign( "articuloEditar", $tpl->draw( 'articuloEditar', $return_string = true ) );
  $template->assign( "backup", $tpl->draw( 'backup', $return_string = true ) );
  $template->assign( "busqueda_avanzada", $tpl->draw( 'busqueda_avanzada', $return_string = true ) );
  $template->assign( "breadcrumb", $tpl->draw( 'breadcrumb', $return_string = true ) );
  $template->assign( "clientes", $tpl->draw( 'clientes', $return_string = true ) );
  $template->assign( "configuracion", $tpl->draw( 'configuracion', $return_string = true ) );
  $template->assign( "cuenta", $tpl->draw( 'cuenta', $return_string = true ) );
  $template->assign( "descuentos", $tpl->draw( 'descuentos', $return_string = true ) );
  $template->assign( "dimensiones", $tpl->draw( 'dimensiones', $return_string = true ) );
  $template->assign( "equivalencias", $tpl->draw( 'equivalencias', $return_string = true ) );
  $template->assign( "fabricantes", $tpl->draw( 'fabricantes', $return_string = true ) );
  $template->assign( "faltantes", $tpl->draw( 'faltantes', $return_string = true ) );
  $template->assign( "funciones", $tpl->draw( 'funciones', $return_string = true ) );
  $template->assign( "horarios", $tpl->draw( 'horarios', $return_string = true ) );
  $template->assign( "importadorPrecios", $tpl->draw( 'importadorPrecios', $return_string = true ) );
  $template->assign( "imagenes", $tpl->draw( 'imagenes', $return_string = true ) );
  $template->assign( "mensajes", $tpl->draw( 'mensajes', $return_string = true ) );
  $template->assign( "menu", $tpl->draw( 'menu', $return_string = true ) );
  $template->assign( "notificaciones", $tpl->draw( 'notificaciones', $return_string = true ) );
  $template->assign( "parametrosAPI", $tpl->draw( 'parametrosAPI', $return_string = true ) );
  $template->assign( "pedidos", $tpl->draw( 'pedidos', $return_string = true ) );
  $template->assign( "quickSearch", $tpl->draw( 'quickSearch', $return_string = true ) );
  $template->assign( "roles", $tpl->draw( 'roles', $return_string = true ) );
  $template->assign( "rubros", $tpl->draw( 'rubros', $return_string = true ) );
  $template->assign( "sugerencias", $tpl->draw( 'sugerencias', $return_string = true ) );
  $template->assign( "usuarios", $tpl->draw( 'usuarios', $return_string = true ) );
  $template->assign( "vehiculos", $tpl->draw( 'vehiculos', $return_string = true ) );
  $template->assign( "watable", $tpl->draw( 'watable', $return_string = true ) );
  $template->assign( "printnode", $tpl->draw( 'printnode', $return_string = true ) );

  

  //Imprimimos el template general
  $html = $template->draw( 'buscador', $return_string = true );
  echo $html;
}

?>