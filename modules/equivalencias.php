<?php
/**
 * @nombre: Equivalencias
 * @descripcion: Equivalencias entre artículos
 */
class equivalencias  extends module{


    /*
	 * Constructor
	 */
    public function __construct(){
    }

    /**
     * Validaciones previas
     */
    public function __pre($accion){

        //Si existe el parametro articulo...
        if(isset($GLOBALS['parametros']['articulo']) && ($GLOBALS['parametros']['articulo'] != '')){
            $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT id FROM catalogo_articulos WHERE id = ".$GLOBALS['parametros']['articulo']);
            $stmt->execute();
            $articulo = $stmt->rowCount();
        }

        //Si existe el articulo
        if( isset($articulo) && !$articulo && 
            in_array($accion, array('editar'))){
            $GLOBALS['resultado']->setError("El artículo no existe.");
            return;
        }

        //llamamos a la accion
        return call_user_func_array(array($this, $accion), array());
    }

    /**
     * @nombre: Lista las Equivalencias
     * @descripcion: Lista las equivalencias
     */
    public function listar(){


        //Obtenemos el id del grupo de equivalencias, exepto el propio...
        $sql = "
                SELECT  
                        id,
                        articulo_id,
                        articulo_id as articulo_idB,
                        IF(articulo_id IS NULL, NULL, (SELECT CONCAT(prefijo,'-',codigo,'-',sufijo) FROM catalogo_articulos WHERE id = articulo_id )) as articulo_codigo,
                        marca_id,
                        (SELECT nombre FROM catalogo_marcas WHERE id = marca_id) as marca_label,
                        codigo_original,
                        
                        IF( (codigo_original IS NULL) AND
                            (   
                                (   SELECT COUNT(*) 
                                    FROM catalogo_articulos_equiv_dim 
                                    WHERE   (   articulo_idB IN (   
                                                    SELECT articulo_id 
                                                    FROM catalogo_articulos_equiv_dim 
                                                    WHERE id = ( SELECT id 
                                                                 FROM catalogo_articulos_equiv_dim 
                                                                 WHERE articulo_id = " . $GLOBALS['parametros']['articulo'].")
                                                    ) 
                                            )
                                ) > 1 
                            ), 
                        1, 
                        0
                        ) as sincroniza_dimensiones,

                        IF( (codigo_original IS NULL) AND
                            (   
                                (   SELECT COUNT(*) 
                                    FROM catalogo_articulos_equiv_app 
                                    WHERE   (   articulo_idB IN (   
                                                    SELECT articulo_id 
                                                    FROM catalogo_articulos_equiv_app 
                                                    WHERE id = ( SELECT id 
                                                                 FROM catalogo_articulos_equiv_app 
                                                                 WHERE articulo_id = " . $GLOBALS['parametros']['articulo'].")
                                                    ) 
                                            )
                                ) > 1 
                            ), 
                        1, 
                        0
                        ) as sincroniza_aplicaciones,

                        IF( (codigo_original IS NULL) AND
                            (   
                                (   SELECT COUNT(*) 
                                    FROM catalogo_articulos_equiv_img 
                                    WHERE   (   articulo_idB IN (   
                                                    SELECT articulo_id 
                                                    FROM catalogo_articulos_equiv_img 
                                                    WHERE id = ( SELECT id 
                                                                 FROM catalogo_articulos_equiv_img 
                                                                 WHERE articulo_id = " . $GLOBALS['parametros']['articulo'].")
                                                    ) 
                                            )
                                ) > 1 
                            ), 
                        1, 
                        0
                        ) as sincroniza_imagenes                   

                FROM 
                        catalogo_articulos_equiv 

                WHERE 
                        (id = (SELECT id FROM catalogo_articulos_equiv WHERE articulo_id = " . $GLOBALS['parametros']['articulo'].")) AND 
                        (articulo_id <> " . $GLOBALS['parametros']['articulo']." OR  articulo_id IS NULL)
        ";


        //Make query
        $stmt = $GLOBALS['conf']['pdo']->prepare($sql);
        $stmt->execute();
        $GLOBALS['resultado']->_result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * @nombre: Chequea si ya existe el Artículo
     * @descripcion: Agrega nueva Equivalencia
     */
    public function check_articulo(){

        // Como nos damos cuenta si la marca la trabajamos o no??
        //  - tiene que figurar dentro del listrado de marcas
        //  - no tiene que hacer uso de los codigos
        //  - no tiene que tener sub-rubros asignados
        //  - no tienen que existir articulos asignados a esta marca....

        //Si parece ser un codigo interno
        if( preg_match("/([a-zA-Z0-9]{1,3})-([a-zA-Z0-9]{1,11})-([0-9]{1,2})/", $GLOBALS['parametros']['codigo_art'], $art) ){
            
            //Consultamos si existe el art.
            $sql = "
                SELECT
                    id as articulo_id,
                    CONCAT(prefijo,'-',codigo,'-',sufijo) as codigo,
                    id_marca as marca_id,
                    (SELECT nombre FROM catalogo_marcas WHERE id = id_marca) as marca_label,
                    NULL as codigo_original
                FROM
                    catalogo_articulos
                WHERE
                    (id_marca = ".$GLOBALS['parametros']['fabricante'].") AND
                    (prefijo = ".$art[1].") AND 
                    (codigo = ".$art[2].") AND
                    (sufijo = ".$art[3].")
            ";

            //Make query
            $stmt = $GLOBALS['conf']['pdo']->prepare($sql);
            $stmt->execute();
            $art = $stmt->fetchAll(PDO::FETCH_ASSOC);

            //Si hay coincidencias del articulo...
            if(count($art)){
                //Agregamos error 
                $GLOBALS['resultado']->setError("El artículo ya existe.");
                return;
            }
        }

        //Guardamos resultados
        $GLOBALS['resultado']->_result = $art;
    }

    /**
     * @nombre: Obtiene grupos
     * @descripcion: Agrega nueva Equivalencia
     */
    public function getGrupo(){

        //Si ni codigo ni id de articulo esta definido tiramos error
        if( ($GLOBALS['parametros']['codigo_original'] == '') && ($GLOBALS['parametros']['articulo'] == '') ){
            //Agregamos error 
            $GLOBALS['resultado']->setError("Error de parametros");
            return;
        }


        // Como nos damos cuenta si la marca la trabajamos o no??
        //  - tiene que figurar dentro del listrado de marcas
        //  - no tiene que hacer uso de los codigos
        //  - no tiene que tener sub-rubros asignados
        //  - no tienen que existir articulos asignados a esta marca....


        //Si estamos buscando equivalencias de art. que no laburamos
        if($GLOBALS['parametros']['articulo'] != ''){


            $sql = "
                SELECT
                    articulo_id,
                    if(articulo_id IS NULL, NULL, (SELECT CONCAT(prefijo,'-',codigo,'-',sufijo) FROM catalogo_articulos WHERE id = articulo_id) ) as codigo,
                    marca_id,
                    (SELECT nombre FROM catalogo_marcas WHERE id = marca_id) as marca_label,
                    codigo_original,

                    IF( (codigo_original IS NULL) AND
                        (   
                            (   SELECT COUNT(*) 
                                FROM catalogo_articulos_equiv_dim 
                                WHERE   (   articulo_id IN (   
                                                SELECT articulo_id 
                                                FROM catalogo_articulos_equiv_dim 
                                                WHERE id = ( SELECT id 
                                                             FROM catalogo_articulos_equiv_dim 
                                                             WHERE articulo_id = ".$GLOBALS['parametros']['articulo'].")
                                                ) 
                                        )
                            ) > 1 
                        ), 
                    1, 
                    0
                    ) as sincroniza_dimensiones,

                    IF( (codigo_original IS NULL) AND
                        (   
                            (   SELECT COUNT(*) 
                                FROM catalogo_articulos_equiv_app 
                                WHERE   (   articulo_id IN (   
                                                SELECT articulo_id 
                                                FROM catalogo_articulos_equiv_app 
                                                WHERE id = ( SELECT id 
                                                             FROM catalogo_articulos_equiv_app 
                                                             WHERE articulo_id = ".$GLOBALS['parametros']['articulo'].")
                                                ) 
                                        )
                            ) > 1 
                        ), 
                    1, 
                    0
                    ) as sincroniza_aplicaciones,

                    IF( (codigo_original IS NULL) AND
                        (   
                            (   SELECT COUNT(*) 
                                FROM catalogo_articulos_equiv_img 
                                WHERE   (   articulo_id IN (   
                                                SELECT articulo_id 
                                                FROM catalogo_articulos_equiv_img 
                                                WHERE id = ( SELECT id 
                                                             FROM catalogo_articulos_equiv_img
                                                             WHERE articulo_id = ".$GLOBALS['parametros']['articulo'].")
                                                ) 
                                        )
                            ) > 1 
                        ), 
                    1, 
                    0
                    ) as sincroniza_imagenes
                FROM
                    catalogo_articulos_equiv
                WHERE
                    (id = (SELECT id FROM catalogo_articulos_equiv WHERE articulo_id = ".$GLOBALS['parametros']['articulo'].")) 
            ";



        //Si estamos buscando equivalencias por el id del art.
        }elseif($GLOBALS['parametros']['codigo_original'] != ''){

            $sql = "
                SELECT
                    articulo_id,
                    articulo_id as articulo_id_alias, 
                    if(articulo_id IS NULL, NULL, (SELECT CONCAT(prefijo,'-',codigo,'-',sufijo) FROM catalogo_articulos WHERE id = articulo_id) ) as codigo,
                    marca_id,
                    (SELECT nombre FROM catalogo_marcas WHERE id = marca_id) as marca_label,
                    codigo_original,

                    IF( (codigo_original IS NULL) AND
                        (   
                            (   SELECT COUNT(*) 
                                FROM catalogo_articulos_equiv_dim 
                                WHERE   (   articulo_id IN (   
                                                SELECT articulo_id 
                                                FROM catalogo_articulos_equiv_dim 
                                                WHERE id = ( SELECT id 
                                                             FROM catalogo_articulos_equiv_dim 
                                                             WHERE articulo_id = articulo_id_alias)
                                                ) 
                                        )
                            ) > 1 
                        ), 
                    1, 
                    0
                    ) as sincroniza_dimensiones,

                    IF( (codigo_original IS NULL) AND
                        (   
                            (   SELECT COUNT(*) 
                                FROM catalogo_articulos_equiv_app 
                                WHERE   (   articulo_id IN (   
                                                SELECT articulo_id 
                                                FROM catalogo_articulos_equiv_app 
                                                WHERE id = ( SELECT id 
                                                             FROM catalogo_articulos_equiv_app 
                                                             WHERE articulo_id = articulo_id_alias)
                                                ) 
                                        )
                            ) > 1 
                        ), 
                    1, 
                    0
                    ) as sincroniza_aplicaciones,

                    IF( (codigo_original IS NULL) AND
                        (   
                            (   SELECT COUNT(*) 
                                FROM catalogo_articulos_equiv_img 
                                WHERE   (   articulo_id IN (   
                                                SELECT articulo_id 
                                                FROM catalogo_articulos_equiv_img 
                                                WHERE id = ( SELECT id 
                                                             FROM catalogo_articulos_equiv_img
                                                             WHERE articulo_id = articulo_id_alias)
                                                ) 
                                        )
                            ) > 1 
                        ), 
                    1, 
                    0
                    ) as sincroniza_imagenes

                FROM
                    catalogo_articulos_equiv
                WHERE
                    (id = (SELECT id FROM catalogo_articulos_equiv WHERE codigo_original = '".$GLOBALS['parametros']['codigo_original']."')) 
            ";


        }
       


        //Make query
        $stmt = $GLOBALS['conf']['pdo']->prepare($sql);
        $stmt->execute();
        $equivs = $stmt->fetchAll(PDO::FETCH_ASSOC);

        //Quitamos a el mismo
        foreach ($equivs as $k=>$v)
            if($v['articulo_id'] == $GLOBALS['parametros']['articulo_actual'])
                unset($equivs[$k]);



        //Guardamos resultados
        $GLOBALS['resultado']->_result = $equivs;
    }

    /**
     * @nombre: Lista los codigos
     * @descripcion: Lista los codigo para las equivalencias
     */
    public function listar_codigos(){


        //Separamos las palabras clave en un arreglo
        $keyword_tokens = explode(' ', trim($GLOBALS['parametros']['q']));

        //Calculate paging
        $page =  (intval($GLOBALS['parametros']['p'])-1) * 40;

        //Sql busqueda de equivalencias
        $sql = "(SELECT * FROM (
                    SELECT
                        NULL as codigo_original,
                        NULL as articulo_id,
                        id as marca_id,
                        nombre as marca_label,
                        NULL as codigo,
                        NULL as codbin
                    FROM
                        catalogo_marcas
                    WHERE
                        padre = 1
                    ORDER BY
                        marca_label) as a
                )

                UNION (
                    SELECT *
                        FROM
                        (

                        SELECT 
                            NULL as codigo_original,
                            id as articulo_id,
                            id_marca as marca_id,
                            (SELECT nombre FROM catalogo_marcas WHERE id = id_marca) as marca_label,
                            CONCAT(prefijo,'-',codigo,'-',sufijo) as codigo,
                            CONCAT(prefijo,'-',codigo,'-',sufijo) as codbin

                        FROM 
                            catalogo_articulos

                        WHERE 
                            (id_marca IS NOT NULL) AND
                            (id <> ".$GLOBALS['parametros']['articulo'].") AND
                            (id_rubro = (SELECT id_rubro FROM catalogo_articulos WHERE id = ".$GLOBALS['parametros']['articulo']."))
                        
                        ORDER BY 
                            marca_label ASC, codbin ASC, codigo ASC) as b
                )
                ";

        /*

        ((id_rubro = (SELECT id_rubro FROM catalogo_articulos WHERE id = " . $GLOBALS['parametros']['articulo'].")) OR
        (id_rubro IS NULL)) AND

        */


        //Make query
        $stmt = $GLOBALS['conf']['pdo']->prepare($sql);
        $stmt->execute();

        //Guardamos los resultados
        $equivalencias =  $stmt->fetchAll(PDO::FETCH_ASSOC);
        
        //Calculamos score
        if($keyword_tokens[0] != '')
        foreach($equivalencias as $k=>$e){
            
            //Inicializamos el score en 0
            $equivalencias[$k]['score'] = 0;

            //Creamos el label del SELECT
            $equivalencias[$k]['nombre'] = $e['marca_label'];

            //Agregamos el codigo si es art. original
            if( $e['codigo'] != NULL ) $equivalencias[$k]['nombre'] .= ' ' . $e['codigo'];

            //Si es NULL...
            if( $e['codigo_original'] != NULL )
            $equivalencias[$k]['nombre'] .= ' ' . $e['codigo_original'];

            //Desglosamos el nombre
            $nombre = explode(' ', $equivalencias[$k]['nombre']);

            //Si encuentra un pedazo de la busqueda suma un punto
            foreach($keyword_tokens as $w)
                //Le damos un punto si la palabra esta entre otras
                if( stripos( strtolower($equivalencias[$k]['nombre']), strtolower($w)) !== false){
                    $equivalencias[$k]['score']++;
                }

            //Por cada palabra clave
            foreach($keyword_tokens as $w)
                //Y por cada palabra del nombre...
                foreach($nombre as $x)
                    // Si la palabra buscada es exacta sumamos puntos
                    if( strtolower($w) == strtolower($x) ){
                        // Score ++
                        $equivalencias[$k]['score']++;
                    }

            //Si es match exacto sumamos puntos tambien
            if( strtolower(trim($GLOBALS['parametros']['q'])) == strtolower($equivalencias[$k]['nombre']) )
                $equivalencias[$k]['score']++;

            //Si estamos haciendo una busqueda ( No listando ... )
            if($keyword_tokens[0] != '')
                //Y el score resulto 0
                if($equivalencias[$k]['score'] == 0)
                    //Quitamos el articulo
                    unset($equivalencias[$k]);
                
        }

        

        //Si estamos haciendo una busqueda...
        if($keyword_tokens[0] != ''){
            //Ordenamos por score
            usort($equivalencias, function($a, $b) {
                //Si el score es el mismo
                //Desempatan con natural order
                if ($a['score'] == $b['score'])
                    return strnatcmp ( $a['nombre'] , $b['nombre'] );

                //Sino por score
                return ($a['score'] < $b['score']) ? 1 : -1;
            });
        }

        //Calculamos totales
        $tot = count($equivalencias);
        $GLOBALS['resultado']->_result['total'] = $tot;

        //Cortamos el pedazo de array que nos interesa
        $res = array_slice($equivalencias, $page, 40);

        //Eliminamos datos que no utilizamos
        foreach($res as $k=>$e){
            unset($res[$k]['score'], $res[$k]['codbin']);
        }

        //Save items to result Object
        $GLOBALS['resultado']->_result['items'] = $res;

    }

    /**
     * @nombre: Guarda los cambios
     * @descripcion: Guarda los cambios en la DB
     */
    public function editar(){


        //Parseamos
        $equivalencias = $GLOBALS['parametros']['equivalencias'];


        //Recorremos el arreglo..
        foreach($equivalencias as $k=>$v){
            //Filtramos los valores
            $equivalencias[$k]['articulo_id'] = ((string)(int)$v['articulo_id'] == $v['articulo_id']) ? intval($v['articulo_id']) : NULL;
            $equivalencias[$k]['marca_id'] = ($v['marca_id'] == (int) $v['marca_id']) ? (int) $v['marca_id'] : NULL;
            $equivalencias[$k]['codigo_original'] = (trim($v['codigo_original']) != '') ? $v['codigo_original'] : NULL;
            $equivalencias[$k]['sincroniza_aplicaciones'] = ((int)$v['sincroniza_aplicaciones'] == 1) ? 1 : 0;
            $equivalencias[$k]['sincroniza_dimensiones'] = ((int)$v['sincroniza_dimensiones'] == 1) ? 1 : 0;
            $equivalencias[$k]['sincroniza_imagenes'] = ((int)$v['sincroniza_imagenes'] == 1) ? 1 : 0;

            //Validamos los datos
            if( 
                !(  //Agregamos equivalencia standard interna
                    (is_int($equivalencias[$k]['articulo_id']) && is_null($equivalencias[$k]['codigo_original']) ) ||
                    //Agregamos equivalencia externa
                    (!is_null($equivalencias[$k]['codigo_original']) && !is_null($equivalencias[$k]['marca_id']) && is_null($equivalencias[$k]['articulo_id']) ) 
                )
            ){

                //Error
                $GLOBALS['resultado']->setError("Error de parametros. Tipos inesperado");
                return;
            }
        }


        // Traemos todas las equivalencias que
        // tengan codigo de articulo...
        $equivsTodas = array();
        foreach ($equivalencias as $k=>$v)
            if(!is_null($v['articulo_id']))
                $equivsTodas[] = $v['articulo_id'];


        if(count($equivsTodas)){
            //Filtramos solo articulos que existan en la DB
            $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT id FROM catalogo_articulos WHERE id IN (".implode(',',$equivsTodas).")");
            $stmt->execute();
            $equivValids =  $stmt->fetchAll(PDO::FETCH_ASSOC);

            //Eliminamos los arts que no existen...
            foreach ($equivalencias as $k=>$art){
                $f = false;
                foreach ($equivValids as $y=>$artDB)
                    if( $artDB['id'] == $art['articulo_id'])
                        $f = true;
                //Borramos...
                if(!$f && !is_null($art['articulo_id']))  unset($equivalencias[$k]);
            }
            //Reindexamos
            array_keys($equivalencias);

        }



        //Debemos realizar todas las modificaciones en la base de datos o no la tocamos en absoluto
        //Para eso hacemos uso de la funcio ATOMIC de MYSQL

        // Begin Transaction
        $GLOBALS['conf']['pdo']->beginTransaction();

        try {




            //articulo Actual
            $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT * FROM catalogo_articulos WHERE id = ".$GLOBALS['parametros']['articulo']);
            $stmt->execute();
            $art =  $stmt->fetch(PDO::FETCH_ASSOC);

            //Si el articulo nos vuelve vacio.. cuack error
            if(!count($art)){
                $GLOBALS['resultado']->setError("Error, el artículo no existe.");
                return;
            }

            //Si el articulo nos vuelve vacio.. cuack error
            if(is_null($art['id_rubro'])){
                $GLOBALS['resultado']->setError("Error, el artículo no tiene rubro.");
                return;
            }














            //GRUPO DE APLICACIONES

            //print_r($equivalencias);

            //Generar id unico que identifique el grupo
            $idEquiv = uniqid(); 


            // CONFORMAMOS EL NUEVO GRUPO DE ARTICULOS
            // Para poder trabajar mejor con las equivalencias separamos las
            // equivalencias que pertnecen a los articulos que trabajamos
            // en la distribuidora y aquellas que son equivalencias con fabricantes
            // externos o que no trabjamos.
            $equivs = array();
            $equivsExts = array();
            $equivsExtsMarca = array();

            foreach($equivalencias as $x=>$eq){
                //Agregamos articulos id
                if( is_int($eq['articulo_id']) && is_null($eq['codigo_original']) )
                    $equivs[] = $eq['articulo_id'];
                
                //Agregamos codigos originales (no laburamos)
                if( is_null($eq['articulo_id']) && !is_null($eq['codigo_original']) ){
                    $equivsExtsMarca[] = $eq['marca_id'];
                    $equivsExts[] = $eq['codigo_original'];
                }
            }

            //Agregamos el mismo articulo a la lista de equivalencias
            $equivs[] = $GLOBALS['parametros']['articulo'];




            // LIMPIAMOS LA TABLA DE EQUIVALENCIAS
            // Antes que nada eliminamos las equivalencias
            // existentes previamente en la tabla.
            // Si antes el articulo tenia equivalencias y ahora no,
            // quitamos el vinculo que habia entre este y el resto.
            // Si estamos agregando nuevas equivalencias quitamos las
            // relaciones que existian de estas equivalencias con otros
            // grupos.

            $sql = "DELETE FROM catalogo_articulos_equiv 
                    WHERE ";

            //Agregamos articulos de la consulta
            if(count($equivs))
                $sql .= "(articulo_id IN (".implode(',',$equivs)."))";
            
            //Or...
            if(count($equivsExts) && count($equivs)) $sql .= " OR ";

            //Agregamos piezas externas
            if(count($equivsExts))
                $sql .= "(codigo_original IN ('".implode("','",$equivsExts)."') AND marca_id IN (".implode(",",$equivsExtsMarca)."))";

            //echo $sql;
            //Ejecutamos query...
            $stmt = $GLOBALS['conf']['pdo']->prepare($sql);
            $stmt->execute();


            // Al eliminar articulos de forma indidual puede ser
            // que alguno nos haya quedado suelto, solo.. Al no
            // conformar ningun grupo, debemos de quitarlo.
            // Limpiamos toda la tabla de equivalencias de articulos
            // individuales.
            $stmt = $GLOBALS['conf']['pdo']->prepare("DELETE FROM catalogo_articulos_equiv WHERE id IN ( SELECT id FROM ( SELECT id FROM catalogo_articulos_equiv GROUP BY id HAVING COUNT(*) = 1 ) AS ONLY_ONCE)");
            $stmt->execute();



























            //DIMENSIONES


            //Generar id unico que identifique el grupo
            $idDim = uniqid(); 

            // Para poder trabajar con las equivalencias que sincronizan dimensiones
            // Antes debemos conseguirla del grupo de equivalencias que generamos
            // en el listado.
            $equivsDim = array();
            foreach($equivalencias as $x=>$eq)
                if( is_int($eq['articulo_id']) && is_null($eq['codigo_original']) && ($eq['sincroniza_dimensiones'] == 1))
                    $equivsDim[] = $eq['articulo_id'];

            //Si existen articulos con los que sincronizamos dimensiones lo agregamos
            if(count($equivsDim)) $equivsDim[] = $GLOBALS['parametros']['articulo'];


            /*  Esto no hace falta ya que al agregar una equivalencia
                le avisamos al usuario que estamos agregando un articulo
                que tiene equivalencias existentes.
                
                En este caso solo debemos generar un nuevo id para los articulos
                que ahora si van a compartir las equivalencias.

                Descartando el proceso de busqueda de articulos que sincronizan
                dimensiones y no estan listado en el arreglo de aplicaciones    
            */

            
            //Obtenemos todos los grupos de equivalencias
            //que sincronizan dimensiones, incluso art. no
            //Listado en el arreglo de eq. pasadas por el user
            $equivsDimDB = array();
            if(count($equivsDim)){
                $stmt = $GLOBALS['conf']['pdo']->prepare("  SELECT * 
                                                            FROM catalogo_articulos_equiv_dim 
                                                            WHERE (id IN ( SELECT id 
                                                                        FROM catalogo_articulos_equiv_dim 
                                                                        WHERE articulo_id IN (".implode(',',$equivsDim).")))");
                $stmt->execute();
                $equivsDimDB =  $stmt->fetchAll(PDO::FETCH_ASSOC);
            }

            //Luego de buscar todos los compañeros de equivalencias que
            //sincronizan apps agregamos el articulo actual a la lista
            $equivsDimDB[]['articulo_id'] = (int)$GLOBALS['parametros']['articulo'];
            



            // Agregamos tanto a la lista de equivalencias como
            // a la lista de articulos que sincronizan dimensiones
            // aquellos articulos que, si bien estaban sincronizando
            // dimensiones en la DB, no figuraban en el listado enviado
            // por el usuario.
            foreach($equivsDimDB as $x=>$eq){

                //Esto esta mal porque si la aplicacion no se encuentra dentro del grupo
                //de equivalencias creado, sincronize o no aplicaciones no debe agregarse
                //al nuevo grupo...

                //Si nos etan diciendo que el articulo es equivalente con otro en dimensiones
                //Y este otro articulo tiene equivalencias con otro articulo que pertenece a
                //listado de equivalencias, lo agregamos a todos al mismo grupo
                $esEquivalencia = false;
                foreach($equivs as $k=>$eq1){
                    if( (int)$eq['articulo_id'] == $eq1 )
                        $esEquivalencia = true;
                }


                $encontro = false;
                foreach($equivsDim as $k=>$eq1){
                    if( (int)$eq['articulo_id'] == $eq1 )
                        $encontro = true;
                }

                //Agregamos
                if(!$encontro) $equivsDim[] = (int)$eq['articulo_id'];

                $encontro = false;
                foreach($equivs as $k=>$eq1)
                    if( (int)$eq['articulo_id'] == $eq1 )
                        $encontro = true;

                //Agregamos
                if(!$encontro) $equivs[] = (int)$eq['articulo_id'];
            }   



            // Si la equivalencia a la cual queremos sincronizarle las dimensiones no 
            // pertenece al mismo rubro al que pertenece el articulo actual la quitamos

            //foreach($equivDimDBfiltro as $x=>$d) 
              //  $equivsDim[] = $d['id'];



            // Quitamos el articulo actual y sus nuevos articulos compañeros 
            // del grupo con quien sincronizaba las dimensiones antes...
            $stmt = $GLOBALS['conf']['pdo']->prepare("DELETE FROM catalogo_articulos_equiv_dim WHERE articulo_id IN (".implode(',',$equivsDim).")");
            $stmt->execute();

            // Quitamos tambien los articulos que quedaron sueltos en la tabla
            // de sincroniza dimensiones....
            $stmt = $GLOBALS['conf']['pdo']->prepare("DELETE FROM catalogo_articulos_equiv_dim WHERE id IN ( SELECT id FROM ( SELECT id FROM catalogo_articulos_equiv_dim GROUP BY id HAVING COUNT(*) = 1 ) AS ONLY_ONCE)");
            $stmt->execute();


            // Si existen compañeros de sincronizacion de dimensiones para el
            // articulo actual, hacemos todo el proceso de actualizacion de 
            // dimensiones y creación del nuevo grupo en la DB.
            if(count($equivsDim) > 1){

                // Con el nuevo grupo de de articulos que sincronizan dimensiones
                // ya conformado, debemos de emparejar todas sus dimensiones, por primera vez.
                // De ahora en mas, cada vez que modifiquemos las dimensiones de alguno de los
                // articulos que conforman el grupo, debemos de modificar todos sus compañeros de grupo.


                // Para poder sincronizar las dimensiones, los articulos deben de pertenecer al mismo
                // rubro, ya que las dimensiones estan relacionadas a cada rubro.
                // Se supone que esto esta validado desde el principio y este articulo ya pertenece
                // a un rubro especifico.

                //Conseguimos el listado completo de Rubros
                $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT * FROM catalogo_rubros");
                $stmt->execute();
                $rubros =  $stmt->fetchAll(PDO::FETCH_ASSOC);

                //Obtenemos mediante esta funcion todos los Rubros Progenitores
                $rubroFamilia = $GLOBALS['toolbox']->traeRubrosPadres($rubros, $art['id_rubro']);

                //Obtenemos datos del Rubro (propios y heredados)
                $stmt = $GLOBALS['conf']['pdo']->prepare("  SELECT id 
                                                            FROM catalogo_rubros_datos 
                                                            WHERE 
                                                                ( (rubro_id IN (".implode(',',$rubroFamilia).")) AND (cede_decendencia = 'Y') )
                                                                OR
                                                                (rubro_id = ".$art['id_rubro'].")");
                $stmt->execute();
                $datosRubros =  $stmt->fetchAll(PDO::FETCH_ASSOC);



                // Una vez que obtenemos el rubro del articulo actual y sus datos
                // tomamos todos los datos del articulo actual, y los copiamos al
                // resto de los integranges del grupo de dimensiones siempre y cuando
                // no se trate del mismo 

                // Solo hacemos esto si es que existen dimensiones asociadas al rubro
                // o heredadas

                //Emparejamos todas las dimensiones en un solo DB Query
                $sql = "UPDATE 
                            catalogo_articulos 
                        SET 
                            id_rubro = ".$art['id_rubro']." ";

                //Si existen datos por actualizar...
                $sqlElements = array();
                foreach($datosRubros as $x=>$dataRub)
                    $sqlElements[] = "extra_" . $dataRub['id'] . " = '" . $art[ 'extra_'.$dataRub['id'] ] . "'";

                //Si existen dimensiones las actualizamos...
                if(count($datosRubros)) $sql .= ', '.implode(', ', $sqlElements);

                //Creamos el WHERE
                $sql .= " WHERE id IN (".implode(',', $equivsDim).")";


                //Ejecutamos consulta a la base de datos
                $stmt = $GLOBALS['conf']['pdo']->prepare($sql);
                $stmt->execute();



                //Si existen datos por actualizar...
                $sqlElements = array();
                foreach($equivsDim as $x=>$eq)
                    $sqlElements[] = "('".$idDim."',".$eq.")";

                //echo "INSERT INTO catalogo_articulos_equiv_dim (id, articulo_id) VALUES ".implode(',',$sqlElements);

                //Insertamos las equivalencias en la DB
                $stmt = $GLOBALS['conf']['pdo']->prepare("INSERT INTO catalogo_articulos_equiv_dim (id, articulo_id) VALUES ".implode(',',$sqlElements));
                $stmt->execute();
                
            }
            







            //APLICACIONES

            //Generar id unico que identifique el grupo
            $idApp = uniqid();

            // Para poder trabajar con las equivalencias que sincrinizan dimensiones
            // Antes debemos conseguirla del grupo de equivalencias que generamos
            // en el listado.
            $equivsApp = array();
            foreach($equivalencias as $x=>$eq)
                if( is_int($eq['articulo_id']) && is_null($eq['codigo_original']) && ($eq['sincroniza_aplicaciones'] == 1))
                    $equivsApp[] = $eq['articulo_id'];

            //Si existen articulos con los que sincronizamos aplicaciones lo agregamos
            if(count($equivsApp)) $equivsApp[] = $GLOBALS['parametros']['articulo'];

            
            //Obtenemos todos los grupos de equivalencias
            //que sincronizan dimensiones, incluso art. no
            //Listado en el arreglo de eq. pasadas por el user
            $equivsAppDB = array();
            if(count($equivsApp)){
                $stmt = $GLOBALS['conf']['pdo']->prepare("  SELECT * 
                                                            FROM catalogo_articulos_equiv_app 
                                                            WHERE (id IN ( SELECT id 
                                                                        FROM catalogo_articulos_equiv_app 
                                                                        WHERE articulo_id IN (".implode(',',$equivsApp).")))");
                $stmt->execute();
                $equivsAppDB =  $stmt->fetchAll(PDO::FETCH_ASSOC);
            }

            //Luego de buscar todos los compañeros de equivalencias que
            //sincronizan apps agregamos el articulo actual a la lista
            $equivsAppDB[]['articulo_id'] = (int)$GLOBALS['parametros']['articulo'];


            // Agregamos tanto a la lista de equivalencias como
            // a la lista de articulos que sincronizan aplicaciones
            // aquellos articulos que, si bien estaban sincronizando
            // aplicaciones en la DB, no figuraban en el listado enviado
            // por el usuario.
            foreach($equivsAppDB as $x=>$eq){
                $encontro = false;
                foreach($equivsApp as $k=>$eq1)
                    if( (int)$eq['articulo_id'] == $eq1 )
                        $encontro = true;

                //Agregamos
                if(!$encontro) $equivsApp[] = (int)$eq['articulo_id'];

                $encontro = false;
                foreach($equivs as $k=>$eq1)
                    if( (int)$eq['articulo_id'] == $eq1 )
                        $encontro = true;

                //Agregamos
                if(!$encontro) $equivs[] = (int)$eq['articulo_id'];
            }




            //Obtenemos las aplicaciones de todos los articulos
            //Para sumarlas todas
            $stmt = $GLOBALS['conf']['pdo']->prepare("  SELECT * 
                                                        FROM catalogo_articulos_aplicaciones 
                                                        WHERE
                                                            articulo_id IN (".implode(',',$equivsApp).")");
            $stmt->execute();
            $equivAppSumadas =  $stmt->fetchAll(PDO::FETCH_ASSOC);


            //Eliminamos viejos grupos de la lista de Aplicaciones
            $stmt = $GLOBALS['conf']['pdo']->prepare("DELETE FROM catalogo_articulos_equiv_app WHERE articulo_id IN (".implode(',',$equivsApp).")");
            $stmt->execute();

            // Quitamos tambien los articulos que quedaron sueltos en la tabla
            // de sincroniza aplicaciones....
            $stmt = $GLOBALS['conf']['pdo']->prepare("DELETE FROM catalogo_articulos_equiv_app WHERE id IN ( SELECT id FROM ( SELECT id FROM catalogo_articulos_equiv_app GROUP BY id HAVING COUNT(*) = 1 ) AS ONLY_ONCE)");
            $stmt->execute();


            // Si entre todos los articulos tienen alguna aplicacion
            // y ademas, en la lista de articulos hay mas artls. que
            // el articulo actual  
            if(count($equivsApp) > 1){


                // Combinamos y filtramos las aplicaciones
                $equivAppSumadas = $GLOBALS['toolbox']->combina_apps($equivAppSumadas);
                // La funcion anterior de la caja de herramientas devuelve un array
                // y en el elemento 0 define el valor 0 o 1 dependiendo si- se detecto
                // algun conflicto por solucionar con las versiones al combinar las aplicaciones
                $appsBanderaConflicto = $equivAppSumadas[0];
                // Quitamos el elemento 0
                unset($equivAppSumadas[0]);
                // Reindexamos el array
                $equivAppSumadas = array_values($equivAppSumadas);


                // Para cada uno de los articulos que conforman las equivalencias y sincronizan las APPS
                // igualamos todas las aplicaciones, para eso:
                // 
                // - Eliminamos todas las aplicaciones que existian asociadas a los articulos
                // - Creamos nuevas aplicaciones desde el arreglo que obtuvimos de la DB anteriormente

                $stmt = $GLOBALS['conf']['pdo']->prepare("  DELETE 
                                                            FROM catalogo_articulos_aplicaciones
                                                            WHERE articulo_id IN (".implode(',',$equivsApp).")");
                $stmt->execute();


                if(count($equivAppSumadas)){
                    //Armamos un array de aplicaciones
                    $sqlElements = array();
                    foreach ($equivsApp as $z=>$a)
                        foreach($equivAppSumadas as $k=>$app)
                            $sqlElements[] = "(".$a.",".( is_null($app['marca_id']) ? 'NULL' : $app['marca_id']).","
                                                         .( is_null($app['modelo_id']) ? 'NULL' : $app['modelo_id']).","
                                                         .( is_null($app['version_id']) ? 'NULL' : $app['version_id']).","
                                                         .( is_null($app['anio_inicio']) ? 'NULL' : $app['anio_inicio']).","
                                                         .( is_null($app['anio_final']) ? 'NULL' : $app['anio_final']).")";

                    // Insertamos todas las aplicaciones
                    $stmt = $GLOBALS['conf']['pdo']->prepare("INSERT INTO catalogo_articulos_aplicaciones (articulo_id, marca_id, modelo_id, version_id, anio_inicio, anio_final) VALUES " . implode(", ", $sqlElements));
                    $stmt->execute();
                }


                //Insertamos nuevas equivalencias
                $sqlElements = array();
                foreach($equivsApp as $k=>$eq)
                    $sqlElements[] = "('".$idApp."',".$eq.")";
                
                $stmt = $GLOBALS['conf']['pdo']->prepare("INSERT INTO catalogo_articulos_equiv_app (id, articulo_id) VALUES " . implode(',', $sqlElements));
                $stmt->execute();

            }












            //IMAGENES



            //Generar id unico que identifique el grupo
            $idImg = uniqid();

            // Para poder trabajar con las equivalencias que sincrinizan dimensiones
            // Antes debemos conseguirla del grupo de equivalencias que generamos
            // en el listado.
            $equivsImg = array();
            foreach($equivalencias as $x=>$eq)
                if( is_int($eq['articulo_id']) && is_null($eq['codigo_original']) && ($eq['sincroniza_imagenes'] == 1))
                    $equivsImg[] = $eq['articulo_id'];

            //Si existen articulos con los que sincronizamos aplicaciones lo agregamos
            if(count($equivsImg)) $equivsImg[] = $GLOBALS['parametros']['articulo'];


            //Obtenemos todos los grupos de equivalencias
            //que sincronizan dimensiones, incluso art. no
            //Listado en el arreglo de eq. pasadas por el user
            $equivsImgDB = array();
            if(count($equivsImg)){
                $stmt = $GLOBALS['conf']['pdo']->prepare("  SELECT * 
                                                            FROM catalogo_articulos_equiv_img 
                                                            WHERE (id IN ( SELECT id 
                                                                        FROM catalogo_articulos_equiv_img 
                                                                        WHERE articulo_id IN (".implode(',',$equivsImg).")))");
                $stmt->execute();
                $equivsImgDB =  $stmt->fetchAll(PDO::FETCH_ASSOC);
            }


            $equivsImgDB[]['articulo_id'] = (int)$GLOBALS['parametros']['articulo'];

            // Agregamos tanto a la lista de equivalencias como
            // a la lista de articulos que sincronizan aplicaciones
            // aquellos articulos que, si bien estaban sincronizando
            // aplicaciones en la DB, no figuraban en el listado enviado
            // por el usuario.
            foreach($equivsImgDB as $x=>$eq){
                $encontro = false;
                foreach($equivsImg as $k=>$eq1)
                    if( (int)$eq['articulo_id'] == $eq1 )
                        $encontro = true;

                //Agregamos
                if(!$encontro) $equivsImg[] = (int)$eq['articulo_id'];

                $encontro = false;
                foreach($equivs as $k=>$eq1)
                    if( (int)$eq['articulo_id'] == $eq1 )
                        $encontro = true;

                //Agregamos
                if(!$encontro) $equivs[] = (int)$eq['articulo_id'];
            }


            //Eliminamos viejos grupos de la lista de Aplicaciones
            $stmt = $GLOBALS['conf']['pdo']->prepare("DELETE FROM catalogo_articulos_equiv_img WHERE articulo_id IN (".implode(',',$equivsImg).")");
            $stmt->execute();

            // Quitamos tambien los articulos que quedaron sueltos en la tabla
            // de sincroniza aplicaciones....
            $stmt = $GLOBALS['conf']['pdo']->prepare("DELETE FROM catalogo_articulos_equiv_img WHERE id IN ( SELECT id FROM ( SELECT id FROM catalogo_articulos_equiv_img GROUP BY id HAVING COUNT(*) = 1 ) AS ONLY_ONCE)");
            $stmt->execute();


            // Si existen equivalencias
            // por agregar a la tabla
            if(count($equivsImg) > 1){

               //Insertamos nuevas equivalencias
                $sqlElements = array();
                foreach($equivsImg as $k=>$eq)
                    $sqlElements[] = "('".$idImg."',".$eq.")";
                
                $stmt = $GLOBALS['conf']['pdo']->prepare("INSERT INTO catalogo_articulos_equiv_img (id, articulo_id) VALUES " . implode(',', $sqlElements));
                $stmt->execute();
            }


            // FIX: si solo queda una y es el articulo actual lo quitamos
            if( (count($equivsExts) == 0) && (count($equivs) == 1) && ($equivs[0] == $art['id']) )
                unset($equivs[0]);


            // Una vez que tenemos el arreglo completol grupo de equivalencias
            // Cometemos los cambios en la tabla del grupo general de equivalencias.
            if(count($equivsExts) || count($equivs)){

                //print_r($equivs);

                // Primero agregamos articulos que laburamos....
                foreach($equivs as $k=>$eq)
                    $sqlAddsEquivs[] = "('".$idEquiv."',".$eq.",(SELECT id_marca FROM catalogo_articulos WHERE id = ".$eq."), NULL)";

                // Ahora los articulos que no laburamos....
                foreach($equivalencias as $x=>$eq)
                    if( is_null($eq['articulo_id']) && !is_null($eq['codigo_original']) )
                        $sqlAddsEquivs[] = "('".$idEquiv."', NULL, ".$eq['marca_id'].", '".$eq['codigo_original']."')";
                
                //Ejecutamos el query
                $stmt = $GLOBALS['conf']['pdo']->prepare("INSERT INTO catalogo_articulos_equiv (id, articulo_id, marca_id, codigo_original) VALUES " . implode(', ', $sqlAddsEquivs));
                $stmt->execute();

                //Copiamos la descripcion al resto de los articulos
                $stmt = $GLOBALS['conf']['pdo']->prepare("UPDATE catalogo_articulos SET descripcion = '".$art['descripcion']."' WHERE id IN (".implode(',',$equivs).")");
                $stmt->execute();

            }





            // Commit Transaction
            $GLOBALS['conf']['pdo']->commit();

        } // Si existieron errores generando las Querys de arriba...
        catch (PDOException $e) {
            // hacemos un Rollback
            $GLOBALS['conf']['pdo']->rollback();

            //Agregamos error 
            $GLOBALS['resultado']->setError($e);
            return;
        }


        // Si existen conflictos entre las versiones
        // se lo comunicamos a la UI para que muestre
        // el aviso correspondiente al usuario.
        $GLOBALS['resultado']->_result['warning'] = 0;
        if(isset($appsBanderaConflicto) && $appsBanderaConflicto)
            $GLOBALS['resultado']->_result['warning'] = 1;
    }

}
?>