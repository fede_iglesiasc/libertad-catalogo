<?php
/**
 * @nombre: Localidades
 * @descripcion: Lista los Localidades.
 */
class localidades extends module{

    /*
	 * Constructor
	 */
    public function __construct(){
    }

    /**
     * Validaciones previas
     */
    public function __pre($accion){

        //Si existe el parametro localidad...
        if(isset($GLOBALS['parametros']['localidad'])){
            $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT id FROM sys_localidades WHERE id = ".$GLOBALS['parametros']['localidad']);
            $stmt->execute();
            $localidad = $stmt->rowCount();
        }

        //Si no existe la localidad
        if( isset($localidad) && !$localidad && 
            in_array($accion, array('editar'))){
            $GLOBALS['resultado']->setError("La Localidad no existe.");
            return;
        }

        //llamamos a la accion
        return call_user_func_array(array($this, $accion), array());
    }

    /**
     * @nombre: Lista Localidades (completa)
     * @descripcion: Listado de Localidades completo.
     */
    public function listar(){
    }

    /**
     * @nombre: Lista Localidades (para select2)
     * @descripcion: Listado de Localidades completo.
     */
    public function listar_select(){

      
        //Calculate paging
        $page =  (intval($GLOBALS['parametros']['p'])-1) * 40;

        //EStamos buscando o solo listando?
        $busqueda = true;
        if($GLOBALS['parametros']['q'] == '') 
            $busqueda = false;

        //SQL
        $stmt = $GLOBALS['conf']['pdo']->prepare("  SELECT
                                                        id,
                                                        (CONCAT(
                                                            sys_localidades.nombre,
                                                            ', ',
                                                            (SELECT nombre FROM sys_provincias WHERE id = sys_localidades.provincia_id)
                                                        )) as nombre
                                                    FROM sys_localidades
                                                    ORDER BY nombre ASC");
        $stmt->execute();
        $localidades =  $stmt->fetchAll(PDO::FETCH_ASSOC);


        //Si estamos buscando...
        if($busqueda){
            //Score
            foreach($localidades as $k=>$v)
                $localidades[$k]['score'] = $GLOBALS['toolbox']->compare_strings($GLOBALS['parametros']['q'], $v['nombre']);

            //Eliminamos 0's
            foreach($localidades as $k=>$v)
                if($v['score'] == 0)
                    unset($localidades[$k]);

            //Ordenamos
            usort($localidades, function($a, $b) {
                return $b['score'] - $a['score'];
            });
        }

        //Calculamos totales
        $GLOBALS['resultado']->_result['total'] = count($localidades);

        //Devolvemos items y total
        $GLOBALS['resultado']->_result['items'] = array_slice($localidades, $page, 40);
    }

}
?>