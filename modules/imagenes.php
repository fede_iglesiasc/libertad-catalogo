<?php
/**
 * @nombre: Imágenes
 * @descripcion: Upload, edicion y Borrado de imágenes.
 */
class Imagenes extends module{
 
	public function __construct(){
	}

	/**
	 * Validaciones previas
	 */
	public function __pre($accion){

		//Si existe el parametro articulo...
		if(isset($GLOBALS['parametros']['articulo']) && ($GLOBALS['parametros']['articulo'] != '') ){
			$stmt = $GLOBALS['conf']['pdo']->prepare("SELECT id FROM catalogo_articulos WHERE id = ".$GLOBALS['parametros']['articulo']);
			$stmt->execute();
			$articulo = $stmt->rowCount();
		}

		//Si existe el articulo
		if( isset($articulo) && !$articulo && 
			in_array($accion, array('listar','posicion','upload','eliminar','pendiente_eliminar','pendiente_confirmar','dimension_thumb'))){
			$GLOBALS['resultado']->setError("El articulo no existe.");
			return;
		}

		//llamamos a la accion
		return call_user_func_array(array($this, $accion), array());
	}

	/**
	 * @nombre: Listar imágenes del artículo
	 * @descripcion: Listar imágenes del artículo
	 */
	public function listar(){

		//Articulo
		$stmt = $GLOBALS['conf']['pdo']->prepare("  SELECT  CONCAT(prefijo,'-',codigo,'-',sufijo) as codigo
													FROM    catalogo_articulos
													WHERE   id = '".$GLOBALS['parametros']['articulo']."'");
		$stmt->execute();
		$datos = $stmt->fetch(PDO::FETCH_ASSOC);

		//imagenes
		$archivos = array_slice(scandir('./articulos'), 2);
		$archivos = array_flip($archivos);

		//Contador
		$cont = 0;
		for ($i=0; $i < 100; $i++)
			if(isset($archivos[$datos['codigo'].'-'.$i.'.jpg'])) $cont++;
			else break;

		//Imagen pendiente
		$pendiente = 0;
		$ancho = 0;
		$alto = 0;
		if(file_exists('./upload_tmp/'.$datos['codigo'].'.jpg')){
			list($ancho, $alto, $tipo, $atributos) = getimagesize('./upload_tmp/'.$datos['codigo'].'.jpg');
			$pendiente = 1;
		}

		//Medidas de Imágen pendiente
		

		//Devolvemos cantida de imagenes del art.
		$GLOBALS['resultado']->_result['cantidad'] = $cont;
		$GLOBALS['resultado']->_result['codigo'] = $datos['codigo'];
		$GLOBALS['resultado']->_result['pendiente'] = $pendiente;
		$GLOBALS['resultado']->_result['pendiente_w'] = $ancho;
		$GLOBALS['resultado']->_result['pendiente_h'] = $alto;
	}

	/**
	 * @nombre: Cambia la posición de la Imágen en la Galería
	 * @descripcion: Listar Cambia la posición de la Imágen en la Galería
	 */
	public function posicion(){

		//Articulo
		$stmt = $GLOBALS['conf']['pdo']->prepare("  SELECT  id, CONCAT(prefijo,'-',codigo,'-',sufijo) as codigo
													FROM    catalogo_articulos
													WHERE   id = '".$GLOBALS['parametros']['articulo']."'");
		$stmt->execute();
		$datos = $stmt->fetch(PDO::FETCH_ASSOC);

		$anterior = $datos['codigo'].'-'.$GLOBALS['parametros']['posicion_anterior'].'.jpg';
		$nueva = $datos['codigo'].'-'.$GLOBALS['parametros']['posicion_nueva'].'.jpg';

		//Cambiamos los thumbs (que siempre existen)
		if( file_exists('./articulos/thumbs/'.$anterior) && file_exists('./articulos/thumbs/'.$nueva) ){
		   rename ('./articulos/thumbs/'.$anterior, './articulos/thumbs/tmp_'.$anterior); 
		   rename ('./articulos/thumbs/'.$nueva, './articulos/thumbs/'.$anterior);
		   rename ('./articulos/thumbs/tmp_'.$anterior, './articulos/thumbs/'.$nueva);
		}

		//Cambiamos las imagenes grandes
		if(file_exists('./articulos/'.$anterior) && file_exists('./articulos/'.$nueva)){
		   rename ('./articulos/'.$anterior, './articulos/tmp_'.$anterior); 
		   rename ('./articulos/'.$nueva, './articulos/'.$anterior);
		   rename ('./articulos/tmp_'.$anterior, './articulos/'.$nueva);
		}

		//Devolvemos el id
		$GLOBALS['resultado']->_result['articulo'] = $datos['id'];
	}

	/**
	 * @nombre: Sube imágen temporal
	 * @descripcion: Sube imágen temporal
	 */
	public function upload(){

		//Articulo
		$stmt = $GLOBALS['conf']['pdo']->prepare("  SELECT  id, CONCAT(prefijo,'-',codigo,'-',sufijo) as codigo
													FROM    catalogo_articulos
													WHERE   id = '".$GLOBALS['parametros']['articulo']."'");
		$stmt->execute();
		$datos = $stmt->fetch(PDO::FETCH_ASSOC);

		//Nombre nuevo
		$nuevo = $datos['codigo'].'.jpg';

		//Cambiamos el nombre de la imagen por el codigo
		//del articulo.
		rename( $GLOBALS['parametros']['imagen']['nuevo_directorio'].$GLOBALS['parametros']['imagen']['name'], 
				$GLOBALS['parametros']['imagen']['nuevo_directorio'].$nuevo);

		//Obtenemos dimensiones
		$ancho = $alto = 0;
		if(file_exists('./upload_tmp/'.$datos['codigo'].'.jpg'))
			list($ancho, $alto, $tipo, $atributos) = getimagesize('./upload_tmp/'.$datos['codigo'].'.jpg');

		$GLOBALS['resultado']->_result['articulo'] = $datos['codigo'];
		$GLOBALS['resultado']->_result['pendiente_w'] = $ancho;
		$GLOBALS['resultado']->_result['pendiente_h'] = $alto;
	}

	/**
	 * @nombre: Elimina una Imágen de la Galería
	 * @descripcion: Elimina una Imágen de la Galería
	 */
	public function eliminar(){
		//Articulo
		$stmt = $GLOBALS['conf']['pdo']->prepare("  SELECT  CONCAT(prefijo,'-',codigo,'-',sufijo) as codigo
													FROM    catalogo_articulos
													WHERE   id = '".$GLOBALS['parametros']['articulo']."'");
		$stmt->execute();
		$datos = $stmt->fetch(PDO::FETCH_ASSOC);

		//Nombre de la Imágen
		$imagen = $datos['codigo'].'-'.$GLOBALS['parametros']['imagen'].'.jpg';

		//imagenes
		$archivos = array_slice(scandir('./articulos'), 2);
		$archivos = array_flip($archivos);

		//Contador
		$cont = 0;
		for ($i=0; $i < 100; $i++)
			if(isset($archivos[$datos['codigo'].'-'.$i.'.jpg'])) $cont++;
			else break;

		//Si nos pasaron un ID de imagen que no existe
		if($GLOBALS['parametros']['imagen'] >= $cont){
			$GLOBALS['resultado']->setError("El ID de la Imágen no existe");
			return;
		}

		//Eliminamos Imagen grande
		if(file_exists('./articulos/'.$imagen))
			unlink('./articulos/'.$imagen);

		//Eliminamos thumbnail
		if(file_exists('./articulos/thumbs/'.$imagen))
			unlink('./articulos/thumbs/'.$imagen);

		//Renombramos el resto
		for ($i= $GLOBALS['parametros']['imagen']; $i < $cont; $i++)
			if( file_exists('./articulos/'.$datos['codigo'].'-'.($i+1).'.jpg') && 
				file_exists('./articulos/thumbs/'.$datos['codigo'].'-'.($i+1).'.jpg')){
				rename('./articulos/'.$datos['codigo'].'-'.($i+1).'.jpg', './articulos/'.$datos['codigo'].'-'.$i.'.jpg');
				rename('./articulos/thumbs/'.$datos['codigo'].'-'.($i+1).'.jpg', './articulos/thumbs/'.$datos['codigo'].'-'.$i.'.jpg');
			}
	}

	/**
	 * @nombre: Eliminar Imágen pendiente
	 * @descripcion: Eliminar Imágen pendiente
	 */
	public function pendiente_eliminar(){
		//Articulo
		$stmt = $GLOBALS['conf']['pdo']->prepare("  SELECT  id, CONCAT(prefijo,'-',codigo,'-',sufijo) as codigo
													FROM    catalogo_articulos
													WHERE   id = '".$GLOBALS['parametros']['articulo']."'");
		$stmt->execute();
		$datos = $stmt->fetch(PDO::FETCH_ASSOC);

		//Eliminamos la imagen pendiente para este articulo
		if(file_exists('./upload_tmp/'.$datos['codigo'].'.jpg'))
			unlink('./upload_tmp/'.$datos['codigo'].'.jpg');
	}

	/**
	 * @nombre: Confirmar Imágen pendiente
	 * @descripcion: Confirmar Imágen pendiente
	 */
	public function pendiente_confirmar(){
		//Articulo
		$stmt = $GLOBALS['conf']['pdo']->prepare("  SELECT  id, CONCAT(prefijo,'-',codigo,'-',sufijo) as codigo
													FROM    catalogo_articulos
													WHERE   id = '".$GLOBALS['parametros']['articulo']."'");
		$stmt->execute();
		$datos = $stmt->fetch(PDO::FETCH_ASSOC);

		//El path a la imagen pendiente
		$img_pendiente = './upload_tmp/'.$datos['codigo'].'.jpg';

		//Por alguna razon la imagen no existe
		if(!file_exists($img_pendiente)){
			$GLOBALS['resultado']->setError("La Imágen ya no existe.");
			return;
		}

		//Debemos saber la posicion en que se agregara
		//la nueva imagen, al final de todas las demas
		$archivos = array_slice(scandir('./articulos'), 2);
		$archivos = array_flip($archivos);

		$cont = 0;
		for ($i=0; $i < 100; $i++)
			if(isset($archivos[$datos['codigo'].'-'.$i.'.jpg'])) $cont++;
			else break;

		//Nuevo nombre con el que se va a guardar la img
		$nuevo_nombre = $datos['codigo'].'-'.$cont.'.jpg';

		//Medidas originales
		list($ancho_original, $alto_original, $tipo, $atributos) = getimagesize($img_pendiente);

		//Si la imagen es demasiado pequeña para generar un thumbnail
		//la movemos sin modificaciones al directorio de thumbs y archivos
		if(($ancho_original <= 150) && ($alto_original <= 150)){
			copy( $img_pendiente, './articulos/'.$nuevo_nombre);
			copy( $img_pendiente, './articulos/thumbs/'.$nuevo_nombre);
			unlink($img_pendiente);
			return;
		}


		//Creamos el recorte segun nos indica el usuario
		$img_pendiente_copy = imagecreatefromjpeg($img_pendiente);
		$nuevo_canvas = ImageCreateTrueColor( $GLOBALS['parametros']['w'], $GLOBALS['parametros']['h'] );

		//Rexample image
		imagecopyresampled( $nuevo_canvas,
							$img_pendiente_copy,
							0,
							0,
							$GLOBALS['parametros']['x'],
							$GLOBALS['parametros']['y'],
							$GLOBALS['parametros']['w'],
							$GLOBALS['parametros']['h'],
							$GLOBALS['parametros']['w'],
							$GLOBALS['parametros']['h']); 
		
		//Create the jpg image
		imagejpeg($nuevo_canvas, $img_pendiente, 100);
		imagedestroy($nuevo_canvas);


		//Ahora que recortamos la imagen y creamos el Crop
		//Si la imagen es demasiado pequeña para generar un thumbnail
		//la movemos sin modificaciones al directorio de thumbs y archivos
		if(($GLOBALS['parametros']['w'] <= 150) && ($GLOBALS['parametros']['h'] <= 150)){
			copy( $img_pendiente, './articulos/'.$nuevo_nombre);
			copy( $img_pendiente, './articulos/thumbs/'.$nuevo_nombre);
			unlink($img_pendiente);
			$GLOBALS['resultado']->_result['estado'] = "Se paso directamente como thumb por ser menor a 150px";
			return;
		}

		//Si nos estamos pasando del tamaño maximo
		//remuestreamos la Imágen
		if(($GLOBALS['parametros']['w'] > 800) || ($GLOBALS['parametros']['h'] > 800)){
			$GLOBALS['toolbox']->resizeImage( $img_pendiente, 800, 800, $img_pendiente);
			$GLOBALS['resultado']->_result['achicamos'] = "Achicamos porque se pasaba de los 800";
		}


		//Si llegamos aca es porque podemos crear el thumbnail
		$GLOBALS['toolbox']->resizeImage( $img_pendiente, 150, 150, './articulos/thumbs/'.$nuevo_nombre);
		$GLOBALS['resultado']->_result['thumb'] = "creado";

		//Creamos una copia waterkarkeada
		$GLOBALS['toolbox']->watermark($img_pendiente, './articulos/'.$nuevo_nombre);
		$GLOBALS['resultado']->_result['watermark'] = "creado";

		//Borramos el temp
		unlink($img_pendiente);
	}

	/**
	 * @nombre: Importar imágenes
	 * @descripcion: Recibe por parametro un ZIP con imagenes y las importa
	 */
	public function importar(){

		//Max execution time
		set_time_limit(0);
		ini_set('memory_limit','1024M');

		//Aca va a parar a upload_tmp/imagenes.xip
		$archivo = $GLOBALS['parametros']['archivo']['nuevo_directorio'].$GLOBALS['parametros']['archivo']['name'];


		$filePath = "./upload_tmp/imagenes.zip";
		$tempFolder = "./upload_tmp/imageBatch";

		//Creamos el directorio temporal si no existe
		if (!file_exists($tempFolder))
			mkdir($tempFolder, 0777, true);


		//Descomprimimos el zip en el directorio
		$zip = new ZipArchive;
		if ($zip->open($filePath) === TRUE) {
			$zip->extractTo($tempFolder.'/');
			$zip->close();
		} else {
			unlink($filePath);
			$GLOBALS['resultado']->setError("No se pudo descomprimir el archivo ZIP.");
			return;
		}


		//Borramos el zip
		unlink($filePath);


		//Leemos todo el directorio de imagenes a procesar
		$fotos = array();
		if($handle = opendir($tempFolder.'/'))
			while (false !== ($entry = readdir($handle))) 
				if ( ($entry != ".") && ($entry != "..") ){
					
					//Si contiene mayusculas cambiamos el nombre por minusculas
					if(!ctype_lower($entry)) 
						rename($tempFolder.'/'.$entry, $tempFolder.'/'.$entry);

					$ext = pathinfo($entry, PATHINFO_EXTENSION);

					if( preg_match("/^[a-zA-Z0-9]{1,3}-[a-zA-Z0-9]{1,11}-[a-zA-Z0-9]{1,2}-[0-9]{1,2}.(jpg|png|gif|JPG|PNG|GIF){1,1}$/i", $entry) ){
						
						preg_match("/([a-zA-Z0-9]{1,3})-([a-zA-Z0-9]{1,11})-([a-zA-Z0-9]{1,2})-([0-9]{1,2}).(jpg|png|gif|JPG|PNG|GIF{1,1})/", $entry, $f);


						//Limpiamos array
						$foto = array();
						//Nombre de archivo completo
						$foto['archivo'] = $f[0];
						//Conseguimos la extension
						$foto['extension'] = $f[5];
						//Le quitamos el numerador
						$foto['cantidad'] = $f[4];
						//Prefijo
						$foto['prefijo'] = $f[1];
						//codigo
						$foto['cod'] = $f[2];
						//sufijo
						$foto['sufijo'] = $f[3];
						//codigo limpio
						$foto['codigo'] = $f[1].'-'.$f[2].'-'.$f[3];
						//Agregamos el archivo a la lita
						$fotos[] = $foto;
					}else{
						unlink($tempFolder.'/'.$entry);
					}
				}

		//Todas las imagenes que vinieron en el zip
		$GLOBALS['resultado']->_result['archivo']= $fotos;

		//Primero chequeamos que exista entre los articulos
		if(count($fotos)){
			//inicializa sql
			$sql = "SELECT CONCAT(prefijo,'-',codigo,'-',sufijo) as codigo FROM catalogo_articulos";

			//Añadimos los codigos
			foreach ($fotos as $k=>$v)
				if(!$k) $sql .= " WHERE ( prefijo = '".$v['prefijo']."' AND codigo = '".$v['cod']."' AND sufijo = '".$v['sufijo']."' )";
				else $sql .= " OR ( prefijo = '".$v['prefijo']."' AND codigo = '".$v['cod']."' AND sufijo = '".$v['sufijo']."' )";

			//Obtenemos tabla de Marcas
			$stmt = $GLOBALS['conf']['pdo']->query($sql);
			$fotosDB = $stmt->fetchAll(PDO::FETCH_ASSOC);
		}

		//Eliminamos las fotos si estos no existen en la DB
		foreach ($fotos as $k=>$f){
			$existe = false;
			foreach($fotosDB as $y=>$art){
				//Si el articulo existe en la DB, continuamos con la siguiente foto 
				if($art['codigo'] == $f['codigo']){
					$existe = true;
					break;
				}
			}

			// Si el articulo no existe borramos la imagen
			// y borramos el item del arreglo.
			if(!$existe){
				//Ya borramos el archivo
				if(file_exists($tempFolder.'/'.$f['archivo']))
					unlink($tempFolder.'/'.$f['archivo']);
				//quitamos el item del array
				unset($fotos[$k]);
			}
		}


		//Convertimos todas las imagenes en JPG
		foreach($fotos as $k=>$foto){

			//Chequeamos gif o png
			$mime_type = $GLOBALS['toolbox']->mime_content_type($tempFolder.'/'.$foto['archivo']);
			switch(strtolower($mime_type)) {
				case 'image/gif':
					//Transformamos el png en jpg
					$GLOBALS['toolbox']->gif2jpg($tempFolder.'/'.$foto['archivo'], $tempFolder.'/'.$foto['codigo'].'-'.$foto['cantidad'].'.jpg', 100);
					//Borramos archivo original
					if(file_exists($tempFolder.'/'.$f['archivo']))
						unlink($tempFolder.'/'.$f['archivo']);
					//Cambiamos extension
					$fotos[$k]['extension'] = 'jpg';
					//Cambiamos nombre de archivo
					$fotos[$k]['archivo'] = str_replace('.gif', '.jpg', $fotos[$k]['archivo']);
					break;
				case 'image/png':
					//Transformamos el png en jpg
					$GLOBALS['toolbox']->png2jpg($tempFolder.'/'.$foto['archivo'], $tempFolder.'/'.$foto['codigo'].'-'.$foto['cantidad'].'.jpg', 100);
					//Borramos archivo original
					if(file_exists($tempFolder.'/'.$f['archivo']))
						unlink($tempFolder.'/'.$f['archivo']);
					//Cambiamos extension
					$fotos[$k]['extension'] = 'jpg';
					//Cambiamos nombre de archivo
					$fotos[$k]['archivo'] = str_replace('.png', '.jpg', $fotos[$k]['archivo']);
					break;
			}

			//Obtenemos el alto y el ancho originales
			list($widthOriginal, $heightOriginal) = getimagesize($tempFolder.'/'.$fotos[$k]['archivo']);
			//Lo insertamos en el arreglo
			$fotos[$k]['alto'] = $heightOriginal;
			$fotos[$k]['ancho'] = $widthOriginal;

		}

		// Revisamos todas las imagenes en busqueda de corruptas
		// y las eliminamos en caso de no poder abrirlas}
		// ademas generamos un arreglo para informarlas al usuario
		$GLOBALS['resultado']->_result['corruptas'] = array();
		foreach($fotos as $k=>$foto)
			if( @imagecreatefromjpeg( $tempFolder.'/'.$foto['archivo'] ) === false ){
				unlink($tempFolder.'/'.$foto['archivo']);
				$GLOBALS['resultado']->_result['corruptas'][] = $fotos[$k];
				unset($fotos[$k]);
			}


		//Obtenemos lista de imagenes existentes
		$fotosExistentes = array();
		if($handle = opendir('./articulos/'))
			while (false !== ($entry = readdir($handle))) 
				if ( ($entry != ".") && ($entry != "..") && ($entry != "thumbs") )
					$fotosExistentes[$entry] = true;


		//Recorremos el listado de fotos a importar
		foreach($fotos as $k=>$foto){

			//Obtenemos cantidad de fotos preexistentes
			for ($i=0; $i < 100; $i++) 
				if(!file_exists('./articulos/'.$foto['codigo'].'-'.$i.'.jpg'))
					break;

			// Guardamos un registro de cada foto que se importo y su resultado
			// Al generar el thumbnail
			// Al generar la original
			// Al agregar watermark

			$foto['i_asignada'] = $i;
			$GLOBALS['resultado']->_result['sobreescritura'][] = $foto;

			//Creamos thumbnail
			$operaciones[$foto['archivo']]['thumb'] = $GLOBALS['toolbox']->resizeImage( $tempFolder.'/'.$foto['archivo'], 150, 150, './articulos/thumbs/'.$foto['codigo'].'-'.$i.'.jpg');

			
			// Si el ancho de la imagen original es mayor
			// a 800 pixeles remuestreamos la imagen
			$operaciones[$foto['archivo']]['original'] = true;
			if( ($foto['ancho'] > 800) || ($foto['alto'] > 800) )
				$operaciones[$foto['archivo']]['original'] = $GLOBALS['toolbox']->resizeImage( $tempFolder.'/'.$foto['archivo'], 800, 800, './articulos/'.$foto['codigo'].'-'.$i.'.jpg');
			else
				$operaciones[$foto['archivo']]['original'] = copy($tempFolder.'/'.$foto['archivo'] , './articulos/'.$foto['codigo'].'-'.$i.'.jpg');
			
			//Creamos la marca de agua sobre archivo final
			$operaciones[$foto['archivo']]['watermark'] = $GLOBALS['toolbox']->watermark('./articulos/'.$foto['codigo'].'-'.$i.'.jpg', './articulos/'.$foto['codigo'].'-'.$i.'.jpg');
			
		}

		//Guardamos los resultados de las operaciones
		$GLOBALS['resultado']->_result['operaciones'] = $operaciones;

		//Eliminamos los archivos temporales
		if(file_exists($tempFolder))
			$GLOBALS['toolbox']->delete_files($tempFolder);
	}
}

?>