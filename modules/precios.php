<?php
/**
 * @nombre: Importador de Precios
 * @descripcion: Importa, actualiza y sincroniza Artículos.
 */
class Precios extends module{


    /*
	 * Constructor
	 */
    public function __construct(){
    }

    /**
     * @nombre: Importador de precios
     * @descripcion: Pasamos por parametro archivo CSV y actualiza DB
     */
    public function importadorPrecios_confirm(){

        //Cronometro
        function microtime_float(){
            list($usec, $sec) = explode(" ", microtime());
            return ((float)$usec + (float)$sec);
        }

        $filePath = $GLOBALS['parametros']['archivo']['nuevo_directorio'].$GLOBALS['parametros']['archivo']['name'];


        //MOMENTO DE INICIO DE EJECUCION
        $tiempo_inicio = microtime_float();

        //Obtenemos tabla de Marcas
        $stmt = $GLOBALS['conf']['pdo']->query("SELECT 

                                                    id,
                                                    IF(padre = 1, id, padre) as padre,
                                                    IF(padre = 1,  nombre, (SELECT nombre FROM (SELECT id, nombre FROM catalogo_marcas) as k WHERE k.id= padre) ) as nombre_padre,
                                                    IF(padre <> 1, CONCAT((SELECT nombre FROM (SELECT id, nombre FROM catalogo_marcas) as k WHERE k.id= padre), ' ->  ', nombre), nombre) as nombre, 
                                                    IF(prefijo IS NULL, 'NULL', prefijo) as prefijo, 
                                                    IF(sufijo IS NULL, 'NULL', sufijo) as sufijo,
                                                    IF(codigo_max IS NULL, 'NULL', codigo_max) as codigo_max,
                                                    IF(codigo_min IS NULL, 'NULL', codigo_min) as codigo_min

                                                FROM catalogo_marcas WHERE (id <> 1) AND (usa_codigos = 'Y')

                                                ORDER BY padre");
        $marcas = $stmt->fetchAll(PDO::FETCH_ASSOC);

        //Contadores
        $artArchivoRep = 0;
        $artArchivoCrudo = 0;

        //ABRIMOS EL CSV Y LO GUARDAMOS EN MEMORIA

        if ( !$file = fopen($filePath, 'r') ) {
            $GLOBALS['resultado']->setError("Error al leer archivo de importación");
            return;
        }

        //Leemos el archivo
        while (($line = fgetcsv($file,0,';')) !== FALSE) {
            
            //SUMAMOS UNO AL TOTAL
            $artArchivoCrudo++;
            
            //VALIDAMOS PREFIJO
            $artTemp['prefijo'] = (string)trim($line[0]);
            
            //VALIDAMOS CODIGO
            $artTemp['codigo'] = (string)trim($line[1]);
            
            //VALIDAMOS SUFIJO
            $artTemp['sufijo'] = (string)trim($line[2]);
            
            //KEY DEL ARRAY
            $key = $artTemp['prefijo'] . '-' . $artTemp['codigo'] . '-' . $artTemp['sufijo'];
            
           
            //SI EL ARTÍCULO YA EXISTE EN LA LISTA
            if(isset($articulosArchivo[$key])) {
                $artArchivoRep++;
        
                
            //SI NO EXISTE EN LA LISTA LO AGREGAMOS
            }else{

                //Match de la marca...
                $marcaMatch = $GLOBALS['toolbox']->idMarca($marcas,$artTemp['prefijo'],$artTemp['sufijo'],$artTemp['codigo']);
                
                //VALIDAMOS PRECIO
                $artTemp['precio'] = round(str_replace(",",".",trim($line[3])), 2);
                
                //OBTENEMOS EL ID DE LA MARCA
                $artTemp['marca'] = $marcaMatch['padre'];

                //OBTENEMOS EL NOMBRE DE LA MARCA
                $artTemp['marca_nombre'] = $marcaMatch['nombre'];
                
           
                //APILAMOS EL ARTICULO
                $articulosArchivo[$key] = $artTemp;
            }
        }

        fclose($file);
        
        //Total de artículos en el archivo de importación
        $artArchivoTotal = count($articulosArchivo);

        //Obtenemos tabla de Marcas
        $stmt = $GLOBALS['conf']['pdo']->query("SELECT 
                                        id, 
                                        prefijo, 
                                        codigo, 
                                        sufijo,
                                        CONCAT(prefijo,'-',codigo,'-',sufijo) as codigo_completo,
                                        precio, 
                                        habilitado, 
                                        (SELECT nombre FROM catalogo_marcas WHERE id = id_marca) as marca_nombre 
                                    FROM catalogo_articulos");
        $artdb = $stmt->fetchAll(PDO::FETCH_ASSOC);


        //Genramos array articulos DB
        $articulosDB = array();
        foreach ($artdb as $k=>$v)
            $articulosDB[ $v['codigo_completo'] ] = $v;

        $DBArchivoTotal = count($articulosDB);

        //CONTADORES
        $articulosSinCambios = 0;
        
        
        $articulosDBInsert = array();
        $articulosDBUpdate = array();
        
        //CLASIFICAMOS LOS ARTICULOS SEGÚN EL PRECIO EN: ALTA, BAJA, ACTUALIZACIÓN O SIN CAMBIOS
        foreach($articulosArchivo as $key=>$artArchivo){
            
            //SI EL ARTICULO QUE TRATAMOS, YA EXISTE EN LA BASE DE DATOS....
            if(isset($articulosDB[$key])){
                
                //Y ADEMAS DICHO ARTICULO NO CAMBIO SU PRECIO....
                if($articulosDB[$key]['precio'] == $articulosArchivo[$key]['precio']){
                    $articulosSinCambios++;
                    //NO HAY NADA QUE HACER, QUITEMOS EL ARCHIVO DE LAS LISTAS.
                    unset($articulosArchivo[$key], $articulosDB[$key]);
                
                //Y ADEMAS DICHO ARTICULO, CAMBIO SU PRECIO...
                }else{
                    //Agregamos su id
                    $artArchivo['id'] = $articulosDB[$key]['id'];
                    $artArchivo['habilitado'] = $articulosDB[$key]['habilitado'];
                    //AGREGAMOS DICHO ARTICULO A LA COLA DE 'UPDATES' PARA ACTUALIZAR CON EL RESTO DE LOS ARTICULOS MODIFICADOS....
                    $articulosDBUpdate[$key] = $artArchivo;
                    //Descartamos el articulo de ambas listas
                    unset($articulosArchivo[$key], $articulosDB[$key]);
                }
            
            //SI EL ARTICULO QUE TRATAMOS NO EXISTE EN LA BASE DE DATOS, DEBE DARSE DE ALTA...
            }else{
                //Lo agregamos al listado de inserts
                $articulosDBInsert[$key] = $artArchivo;
                //Descartamos el articulo de ambas listas
                unset($articulosArchivo[$key], $articulosDB[$key]);
            }
        
        }

        //GENERAMOS EL LISTADO DE REINCORPORACIONES...
        $reincorporaciones = array();
        foreach($articulosDBUpdate as $key=>$art){
            if($art['habilitado'] == 0){
                $reincorporaciones[] = $art;
                unset($articulosDBUpdate[$key]);
            }
        }
        
        
        //CONTADORES
        
        //Altas
        (isset($articulosDBInsert)) ? $articulosNuevos = count($articulosDBInsert) : $articulosNuevos = 0;
        
        //Actualizaciones
        (isset($articulosDBUpdate)) ? $articulosActualizados = count($articulosDBUpdate) : $articulosActualizados = 0;
        
        //Bajas
        (isset($articulosDB)) ? $articulosEliminados = count($articulosDB) : $articulosEliminados = 0;
        
        //Reincorporaciones
        (isset($reincorporaciones)) ? $articulosReincorporados = count($reincorporaciones) : $articulosReincorporados = 0;
        

        //Generamos backup
        $GLOBALS['toolbox']->backup();

        // Begin Transaction
        $GLOBALS['conf']['pdo']->beginTransaction();
        
        try{

            //Si existe algun alta pendiente
            if($articulosNuevos){
                
                //Generamos el sql
                foreach($articulosDBInsert as $k=>$v){
                    if(!isset($sqlInsert)) 
                        $sqlInsert = "INSERT INTO catalogo_articulos (id_marca,prefijo,codigo,sufijo,precio,precio_update,fecha_incorporacion) VALUES (".$v['marca'].",'".$v['prefijo']."','".$v['codigo']."','".$v['sufijo']."',".$v['precio'].", CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)";
                    else 
                        $sqlInsert .= ", (".$v['marca'].",'".$v['prefijo']."','".$v['codigo']."','".$v['sufijo']."',".$v['precio'].", CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)";
                }
                
                //Realizamos la consulta
                if(isset($sqlInsert)){
                    //Ejecutamos el SQL
                    $stmt = $GLOBALS['conf']['pdo']->prepare($sqlInsert);
                    $stmt->execute();
                }
                
                //Limpiamos la memoria
                unset($sqlInsert);
            }
            
            
            
            
            
            //BAJAS
            //Si existe algun alta pendiente
            if($articulosEliminados){
                
                //Generamos el sql
                foreach($articulosDB as $k=>$v){
                    
                    /*if(!isset($sqlDelete)) 
                        $sqlDelete = "DELETE FROM catalogo_articulos WHERE (id = ".$v['id'].")";
                    else 
                        $sqlDelete .= " OR (id = ".$v['id'].")";*/
                }
                
                //ACTUALIACION: ESTO YA NO ES NECESARIO, AHORA LAS BAJAS SE HACEN MANUALES...
                if(isset($sqlDelete)){
                    //Ejecutamos el SQL
                    //$stmt = $GLOBALS['conf']['pdo']->prepare($sqlDelete);
                    //$stmt->execute();
                }
                
                //Limpiamos la memoria
                unset($sqlDelete);
            }
            
            
            
            
            //ACTUALIZACIONES
            
            //Si existe algun alta pendiente
            if($articulosActualizados){
                
                //Generamos el sql
                foreach($articulosDBUpdate as $k=>$v){
                    if(!isset($sqlUpdate)) 
                        $sqlUpdate = "INSERT INTO catalogo_articulos_actualizacion_precios (id,prefijo,codigo,sufijo,precio) VALUES (".$v['id'].",'".$v['prefijo']."','".$v['codigo']."','".$v['sufijo']."',".$v['precio'].")";
                    else 
                        $sqlUpdate .= ", (".$v['id'].",'".$v['prefijo']."','".$v['codigo']."','".$v['sufijo']."',".$v['precio'].")";
                

                }
                
                //Realizamos la consulta
                if(isset($sqlUpdate)){
                    //Ingresamos todos los datos a la Tabla Temporal
                    //Ejecutamos el SQL
                    $stmt = $GLOBALS['conf']['pdo']->prepare($sqlUpdate);
                    $stmt->execute();
                    $GLOBALS['resultado']->_result['banderaUpdate'] = 'true';
                    
                    //Hacemos efectiva la actualizacion
                    $sqlMigrate = " UPDATE catalogo_articulos a, catalogo_articulos_actualizacion_precios b SET 
                                        a.precio = b.precio, a.precio_update = CURRENT_TIMESTAMP
                                    WHERE
                                        (a.id = b.id) AND (a.prefijo = b.prefijo) AND (a.codigo = b.codigo) AND (a.sufijo = b.sufijo)";
                    
                    //Query para migrar
                    $stmt = $GLOBALS['conf']['pdo']->prepare($sqlMigrate);
                    $stmt->execute();
                    $GLOBALS['resultado']->_result['banderaUpdate2'] = 'true';
                    
                    //Vaciamos la tabla temporal
                    $stmt = $GLOBALS['conf']['pdo']->prepare("DELETE FROM catalogo_articulos_actualizacion_precios");
                    $stmt->execute();
                }
                
                //Limpiamos la memoria
                unset($sqlUpdate, $sqlMigrate);
            }

            //Cometemos los cambios a la base
            $GLOBALS['conf']['pdo']->commit();

        } // Si existieron errores generando las Querys de arriba...
        catch (PDOException $e) {
            // hacemos un Rollback
            $GLOBALS['conf']['pdo']->rollback();

            //Agregamos error 
            $GLOBALS['resultado']->setError("ERROR DB");
            return;
        }




        //Realizamos una reindexacion de los articulos para una busqueda rapida
        $GLOBALS['toolbox']->crawler();



       //MOMENTO FINAL DE EJECUCION
        $tiempo_fin = microtime_float();
        $tiempo = $tiempo_fin - $tiempo_inicio;
        
        //TODAVIA QUEDA DISPONIBLE EL ARREGLO $ART CON LOS CODIGOS DE LOS ARTICULOS ELIMINADOS
        //CHEQUEAR SI CONVIENE ELIMINAR LOS ARTICULOS O MARCARLOS COMO ELIMINADOS CON UNA BANDERA
        //POR SI EL DIA DE MAÑANA QUEREMOS GENERAR PEDIDOS (CON UN HISTORIAL DE PEDIDOS POR CLIENTE)
        //NECESITAREMOS DE LOS ARTICULOS QUE QUIZA HOY YA NO SE UTILICEN.
        
        //ACTUALIZACION: EN EL FUTURO EL HISTORIAL DE PEDIDOS, GUARDARA EL PEDIDO COMPLETO COMO 'STRING'
        //Y NO COMO LISTADO DE REFERENCIA A ARTICULOS ANTIGUOS, DE ESTA FORMA DEJAMOS LA DB MAS LIMPIA
        
        
        
        $resultados = '<div style="height: 300px; overflow-y: auto;">';
        
        //POSTEAMOS LOS RESULTADOS DE LA IMPORTACION
        $resultados .= "Tiempo empleado para procesar la actualización: <b>" . str_replace(".",",",round(trim(($tiempo_fin - $tiempo_inicio)), 2))."</b> segundos.<br><br>";
        
        $resultados .= "El archivo de importacion contenía un total de <b>".$artArchivoCrudo. "</b> Artículos, de los cuales <b>".$artArchivoRep."</b> estaban repetidos.<br>";
        $resultados .= "Quedando de ésta manera <b>". $artArchivoTotal ."</b> Artículos por procesar.<br><br>";
        $resultados .= "El total de Artículos existentes en la Base de datos antes de importar es de <b>".$DBArchivoTotal."</b><br>Antes de modificar la base de datos se creó un Backup.<br><br>";

        if(!$articulosNuevos) $resultados .= "No se agregaron nuevos Artículos </br>";
        else $resultados .= "Se agregaron <b>".$articulosNuevos. "</b> nuevos Artículos. Se generará un listado debajo para su posterior edición.<br>";

        if(!$articulosActualizados) $resultados .= "No se actualizaron Artículos. </br>";
        else $resultados .= "Se actualizaron los precios de <b>".$articulosActualizados. "</b> Artículos. </br>";
 
        if(!$articulosEliminados) $resultados .= "No se eliminaron Artículos. </br>";
        else $resultados .= "<b>".$articulosEliminados."</b> Artículos se eliminaron.<br>";

        if($articulosSinCambios) $resultados .= "<b>".$articulosSinCambios."</b> Artículos se mantuvieron sin cambios. <br>";        
        
        //MOSTRAMOS LAS INCORPORACIONES
        if($articulosActualizados > 0){
            $resultados .= "<br><br><b>Artículos con datos por actualizar:</b><br>";
        
        
            $resultados .= "<table id=\"hor-zebra\" summary=\"Articulos por actualizar\"><thead><tr>";
            $resultados .= "<th scope=\"col\" style=\"text-align: left;\">Código</th>";
            $resultados .= "<th scope=\"col\" style=\"text-align: left;\">Marca</th>";
            $resultados .= "</tr></thead><tbody>";
                 
            foreach($articulosDBInsert as $k=>$v){
                $resultados .= "<tr class=\"odd\"><td width=\"150\"><b>".$v['prefijo']."-".$v['codigo']."-".$v['sufijo']."</b></td>";
                $resultados .= "<td>".$v['marca_nombre']."</td></tr>";
            }
            
            $resultados .= "</tbody></table>";
        }
        
        //MOSTRAMOS LAS REINCORPORACIONES
        if($articulosReincorporados > 0){
            
            $resultados .= "<br><br><b>Artículos que se reincorporaron (habilitar nuevamente de forma manual):</b><br>";
            
            
            $resultados .= "<table id=\"hor-zebra\" summary=\"Articulos para reincorporar\"><thead><tr>";
            $resultados .= "<th scope=\"col\" style=\"text-align: left;\">Código</th>";
            $resultados .= "<th scope=\"col\" style=\"text-align: left;\">Marca</th>";
            $resultados .= "</tr></thead><tbody>";
                 
            foreach($reincorporaciones as $k=>$v){
                $resultados .= "<tr class=\"odd\"><td width=\"150\"><b>".$v['prefijo']."-".$v['codigo']."-".$v['sufijo']."</b></td>";
                $resultados .= "<td>".$v['marca_nombre']."</td></tr>";
            }
            
            $resultados .= "</tbody></table>";
        }

        $resultados .= "</div>";


        //Generamos el PDF con la lista completa
        //$GLOBALS['toolbox']->genera_lista_pdf();


        $GLOBALS['resultado']->_result['html'] = $resultados;
    }

    /**
     * @nombre: Sube Lista de Precios nueva
     * @descripcion: Sube Lista de Precios nueva
     */
    public function subir_lista(){

        ini_set('memory_limit', '2048M');

        //Cronometro
        function microtime_float(){
            list($usec, $sec) = explode(" ", microtime());
            return ((float)$usec + (float)$sec);
        }

        //MOMENTO DE INICIO DE EJECUCION
        $tiempo_inicio = microtime_float();


        //Otenemos informacion del archivo Excel
        require_once './classes/PHPExcel.php';

        //Ruta del archivo de dimensiones
        $archivo = $GLOBALS['parametros']['archivo']['nuevo_directorio'].$GLOBALS['parametros']['archivo']['name'];


        // array contains letters of column names
        $arrColumns = array(0=>'A',1=>'B',2=>'C',3=>'D',4=>'E',5=>'F',6=>'G',7=>'H',8=>'I',9=>'J',10=>'K',11=>'L',12=>'M',13=>'N',14=>'O',15=>'P',16=>'Q',17=>'R',18=>'S',19=>'T',20=>'U',21=>'V',22=>'W',23=>'X',24=>'Y',25=>'Z',26=>'AA',27=>'AB',28=>'AC',29=>'AD',30=>'AE',31=>'AF',32=>'AG',33=>'AH',34=>'AI',35=>'AJ',36=>'AK',37=>'AL',38=>'AM',39=>'AN',40=>'AO',41=>'AP',42=>'AQ',43=>'AR',44=>'AS',45=>'AT',46=>'AU',47=>'AV',48=>'AW',49=>'AX',50=>'AY',51=>'AZ',52=>'BA',53=>'BB',54=>'BC',55=>'BD',56=>'BE',57=>'BF',58=>'BG',59=>'BH',60=>'BI',61=>'BJ',62=>'BK',63=>'BL',64=>'BM',65=>'BN',66=>'BO',67=>'BP',68=>'BQ',69=>'BR',70=>'BS',71=>'BT',72=>'BU',73=>'BV',74=>'BW',75=>'BX',76=>'BY',77=>'BZ');


        $objPHPExcel = PHPExcel_IOFactory::load($archivo);
        $objPHPExcel->setActiveSheetIndex(0);
        $aSheet = $objPHPExcel->getActiveSheet();


        //Que cantidad de Filas
        $countRows = $aSheet->getHighestRow();

        //Que cantidad de columnas tiene el Arhivo
        $highestColumn = $aSheet->getHighestColumn();
        $countCols = PHPExcel_Cell::columnIndexFromString($highestColumn);
        $excel = array();


        //Generamos el Arreglo de Dimensiones
        for($i = 1; $i <= $countRows; $i++)
            for($y=0; $y<$countCols; $y++)
                $excel[$i][$arrColumns[$y]] = $aSheet->getCell($arrColumns[$y].$i)->getValue();

        

        //Borramos el archivo
        unlink($archivo);

       
        $excelLimpio = array();

        //Recorremos fixeando todo el arreglo
        foreach ($excel as $k=>$v){
            //Validamos todos los articulos del excel
            //$codigo = $GLOBALS['toolbox']->desglosa_codigo($v['A']);
            
            //Quitamos espacios de principio y fin
            $prefijo = preg_replace("/[^A-Za-z0-9]/", '', $v['A']);
            $codigo = preg_replace("/[^A-Za-z0-9]/", '', $v['B']);
            $sufijo = preg_replace("/[^A-Za-z0-9]/", '', $v['C']);

            $key = $prefijo . '-' . $codigo . '-' . $sufijo;
            $excelLimpio[$key]['prefijo'] = $prefijo;
            $excelLimpio[$key]['codigo'] = $codigo;
            $excelLimpio[$key]['sufijo'] = $sufijo;
            $excelLimpio[$key]['precio'] = number_format((float)$v['D'], 3, '.', '');
        }


        //Vaciamos los datos anteriores de la tabla
        $sql = "TRUNCATE catalogo_articulos_actualizacion_precios;";
        $sql .= "INSERT INTO catalogo_articulos_actualizacion_precios (prefijo, codigo, sufijo, precio) VALUES ";

        //Agregamos todos los datos nuevos
        $cont = 0;
        foreach ($excelLimpio as $k=>$v){
            if($cont > 0) $sql .= ", ";
            $sql .= "('".$v['prefijo']."','".$v['codigo']."','".$v['sufijo']."','".$v['precio']."')";
            $cont++;
        }

        //Ejecutamos la query
        $GLOBALS['conf']['pdo']->query($sql);
    }


    /**
     * @nombre: Chequea si existe lista de precio Anterior
     * @descripcion: Chequea si existe lista de precio Anterior
     */
    public function check_lista(){
        //Obtenemos los usuarios
        $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT COUNT(*) FROM catalogo_articulos_actualizacion_precios");
        $stmt->execute();
        $datos = $stmt->fetch(PDO::FETCH_COLUMN);

        //Cantidad de articulos pendientes en la nueva lista
        $GLOBALS['resultado']->_result['pendiente'] = (int)$datos;

        //Si existen art. pendientes
        if((int)$datos > 0){

            //Agregamos el SQL para saber que cantidad 
            //de articulos hay por actualizar
            $sql = "    SELECT      COUNT(*)
                        FROM        catalogo_articulos a
                        INNER JOIN  catalogo_articulos_actualizacion_precios aa
                        ON          (a.prefijo = aa.prefijo) AND 
                                    (a.codigo = aa.codigo) AND 
                                    (a.sufijo = aa.sufijo) AND 
                                    (a.precio <> aa.precio);";

            //Almacenamos resultados
            $stmt = $GLOBALS['conf']['pdo']->query($sql);
            $actualizaciones = (int)$stmt->fetchColumn();

            //Agregamos el SQL para saber que cantidad
            //de articulos hay que dar de baja (deshabilitar = 0)
            $sql = "   SELECT      COUNT(*)
                        FROM        catalogo_articulos a
                        LEFT JOIN   catalogo_articulos_actualizacion_precios aa
                        ON          (a.prefijo = aa.prefijo) AND 
                                    (a.codigo = aa.codigo) AND 
                                    (a.sufijo = aa.sufijo)
                        WHERE       (aa.prefijo IS NULL) AND
                                    (a.habilitado = 1)";

            //Almacenamos resultados
            $stmt = $GLOBALS['conf']['pdo']->query($sql);
            $bajas = (int)$stmt->fetchColumn();

            //Agregamos el SQL para saber que cantidad
            //de articulos hay que dar de alta (agregar a la tabla)
            $sql = "   UPDATE catalogo_articulos_actualizacion_precios SET id=NULL;
                        UPDATE catalogo_articulos_actualizacion_precios a 
                        JOIN catalogo_articulos b 
                        ON (a.prefijo = b.prefijo) AND (a.codigo = b.codigo) AND (a.sufijo = b.sufijo)  
                        SET a.id = b.id;";
            
            //Almacenamos resultados
            $stmt = $GLOBALS['conf']['pdo']->query($sql);
            $stmt = $GLOBALS['conf']['pdo']->query("SELECT COUNT(*) FROM catalogo_articulos_actualizacion_precios WHERE id IS NULL");
            $altas = (int)$stmt->fetchColumn();

            //Agregamos el SQL para saber la cantidad
            //de articulos que estaban dados de baja
            //y los reincorporamos (habilitado = 0  --->  habilitado = 1)
            $sql = "    SELECT      COUNT(*)
                        FROM        catalogo_articulos a
                        INNER JOIN  catalogo_articulos_actualizacion_precios aa
                        ON          (a.prefijo = aa.prefijo) AND 
                                    (a.codigo = aa.codigo) AND 
                                    (a.sufijo = aa.sufijo) AND 
                                    (habilitado = 0)";

            //Almacenamos resultados
            $stmt = $GLOBALS['conf']['pdo']->query($sql);
            $reincorporaciones = (int)$stmt->fetchColumn();

            //Guardamos los resultados
            $GLOBALS['resultado']->_result['altas'] = $altas;
            $GLOBALS['resultado']->_result['bajas'] = $bajas;
            $GLOBALS['resultado']->_result['reincorporaciones'] = $reincorporaciones;
            $GLOBALS['resultado']->_result['actualizaciones'] = $actualizaciones;
        }
    }


    /**
     * @nombre: Descartar lista pendiente
     * @descripcion: Descartar lista pendiente
     */
    public function descartar_lista(){
        //Obtenemos los usuarios
        $stmt = $GLOBALS['conf']['pdo']->query("TRUNCATE catalogo_articulos_actualizacion_precios");
    }

    /**
     * @nombre: Confirmar actualizacion de Precios
     * @descripcion: Confirmar actualizacion de Precios
     */
    public function confirmar_actualizaciones(){

        //return;

        //Calculamos una fecha comun para todas las modificaciones
        $fecha = date('Y-m-d H:i:s');

        //Traemos todos los articulos, con sus rubros y marcas
        //A las que pertenecen.
        $sql = "    SELECT  DISTINCT a.id_marca, a.id_rubro  
                    FROM        catalogo_articulos a
                    INNER JOIN  catalogo_articulos_actualizacion_precios aa ON ( a.prefijo = aa.prefijo ) 
                    AND         ( a.codigo = aa.codigo ) AND ( a.sufijo = aa.sufijo ) AND ( a.precio <> aa.precio );";


        //Insertamos aumentos + rubros modificados
        $stmt = $GLOBALS['conf']['pdo']->query($sql);

        //Listamos los resultados 
        $actualizaciones = $stmt->fetchAll(PDO::FETCH_ASSOC);

        //Arreglo de fabricantes
        $fabricantes = array();


        //Guardamos los rubros por Fabricante
        foreach($actualizaciones as $k=>$v)
            if(($v['id_marca'] != '') && ($v['id_rubro'] != ''))
                $fabricantes[$v['id_marca']][] = $v['id_rubro'];

        // Begin Transaction
        $GLOBALS['conf']['pdo']->beginTransaction();

        try {

            //////////////////////////////////////////////////////////////////////
            // Actualizamos todos los precios de los Articulos
            //////////////////////////////////////////////////////////////////////

            //Copiamos el precio actual al anterior
            $sql = "UPDATE catalogo_articulos SET precio_anterior = precio WHERE id IN (
                        SELECT  id  FROM (
                            SELECT      a.id  
                            FROM        catalogo_articulos a
                            INNER JOIN  catalogo_articulos_actualizacion_precios aa ON ( a.prefijo = aa.prefijo ) 
                            AND         (a.codigo=aa.codigo) AND (a.sufijo=aa.sufijo) AND ( a.precio <> aa.precio )
                        ) as x
            );";

            //Actualizamos todos los precios
            $sql .= "   UPDATE      catalogo_articulos a
                        INNER JOIN  catalogo_articulos_actualizacion_precios aa
                        ON          (a.prefijo = aa.prefijo) AND 
                                    (a.codigo = aa.codigo) AND 
                                    (a.sufijo = aa.sufijo) AND 
                                    (a.precio <> aa.precio)
                        SET         a.precio = aa.precio,
                                    a.precio_update = '".$fecha."';";

            //Recalculamos porcentajes de aumentos
            $sql .= "UPDATE catalogo_articulos SET precio_porcentaje = (precio - precio_anterior) / precio_anterior * 100";

            //Hacemos la consulta
            $stmt = $GLOBALS['conf']['pdo']->query($sql);

            // Commit Transaction
            $GLOBALS['conf']['pdo']->commit();


            ini_set('memory_limit', '3000M');

            ini_set ('display_errors', 'on');
            ini_set ('log_errors', 'on');
            ini_set ('display_startup_errors', 'on');
            ini_set ('error_reporting', E_ALL);
            
            //Quitamos el limite de ejecucuin
            set_time_limit(0);

            //Incluimos clase para generar PDF
            include("./classes/mpdf57/mpdf.php");

            //Borramos todos los archivos anteriores
            $files = glob('./listas/*');
            foreach($files as $file)
              if(is_file($file))
                unlink($file);


            $css = "body{font-family: sans-serif; font-size: 10px} ";
            $css .= ".col1{width: 157px; height: 900px; position: fixed; left: 0px; top: 0px;} ";
            $css .= ".col2{width: 157px; height: 900px; position: fixed; left: 186px; top: 0px;} ";
            $css .= ".col3{width: 157px; height: 900px; position: fixed; right: 186px; top: 0px;} ";
            $css .= ".col4{width: 157px; height: 900px; position: fixed; right: 0px; top: 0px;} ";
            $css .= ".articulo{width: 157px; height: 17px; border-bottom: 1px dotted black;} ";

            //Columna 1
            $css .= ".pref-1{width: 24px; height: 17px; position: fixed; left: 0px; line-height: 17px; text-align: center; overflow: hidden;} ";
            $css .= ".codigo-1{width: 60px; height: 17px; position: fixed; left: 24px; line-height: 17px; text-align: center; overflow: hidden;} ";
            $css .= ".suf-1{width: 24px; height: 17px; position: fixed; left: 84px; line-height: 17px; text-align: center; overflow: hidden;} ";
            $css .= ".moneda-1{width: 8px; height: 17px; position: fixed; left: 108px; line-height: 17px; text-align: right; overflow: hidden;} ";
            $css .= ".precio-1{width: 50px; height: 17px; position: fixed; left: 116px; line-height: 17px; text-align: right; overflow: hidden;} ";
            $css .= ".guiones-1{width: 166px; height: 1px; position: fixed; left: 0px; line-height: 1px; overflow: hidden;} ";
            $css .= ".col1-cod{width: 108px; height: 17px; position: fixed; left: 0px; line-height: 17px; overflow: hidden; text-align: center; font-weight: bold;} ";
            $css .= ".col1-precio{width: 58px; height: 17px; position: fixed; left: 108px; line-height: 17px; overflow: hidden; text-align: center; font-weight: bold;} ";

            //Columna 2
            $css .= ".pref-2{width: 24px; height: 17px; position: fixed; left: 184px; line-height: 17px; text-align: center; overflow: hidden;} ";
            $css .= ".codigo-2{width: 60px; height: 17px; position: fixed; left: 208px; line-height: 17px; text-align: center; overflow: hidden;} ";
            $css .= ".suf-2{width: 24px; height: 17px; position: fixed; left: 268px; line-height: 17px; text-align: center; overflow: hidden;} ";
            $css .= ".moneda-2{width: 8px; height: 17px; position: fixed; left: 292px; line-height: 17px; text-align: right; overflow: hidden;} ";
            $css .= ".precio-2{width: 50px; height: 17px; position: fixed; left: 300px; line-height: 17px; text-align: right; overflow: hidden;} ";
            $css .= ".guiones-2{width: 166px; height: 1px; position: fixed; left: 184px; line-height: 1px; overflow: hidden;} ";
            $css .= ".col2-cod{width: 108px; height: 17px; position: fixed; left: 184px; line-height: 17px; overflow: hidden; text-align: center; font-weight: bold;} ";
            $css .= ".col2-precio{width: 58px; height: 17px; position: fixed; left: 292px; line-height: 17px; overflow: hidden; text-align: center; font-weight: bold;} ";

            //Columna 3
            $css .= ".pref-3{width: 24px; height: 17px; position: fixed; left: 369px; line-height: 17px; text-align: center; overflow: hidden;} ";
            $css .= ".codigo-3{width: 60px; height: 17px; position: fixed; left: 393px; line-height: 17px; text-align: center; overflow: hidden;} ";
            $css .= ".suf-3{width: 24px; height: 17px; position: fixed; left: 453px; line-height: 17px; text-align: center; overflow: hidden;} ";
            $css .= ".moneda-3{width: 8px; height: 17px; position: fixed; left: 477px; line-height: 17px; text-align: right; overflow: hidden;} ";
            $css .= ".precio-3{width: 50px; height: 17px; position: fixed; left: 485px; line-height: 17px; text-align: right; overflow: hidden;} ";
            $css .= ".guiones-3{width: 166px; height: 1px; position: fixed; left: 369px; line-height: 1px; overflow: hidden;} ";
            $css .= ".col3-cod{width: 108px; height: 17px; position: fixed; left: 368px; line-height: 17px; overflow: hidden; text-align: center; font-weight: bold;} ";
            $css .= ".col3-precio{width: 58px; height: 17px; position: fixed; left: 476px; line-height: 17px; overflow: hidden; text-align: center; font-weight: bold;} ";

            //Columna 4
            $css .= ".pref-4{width: 24px; height: 17px; position: fixed; right: 142px; line-height: 17px; text-align: center; overflow: hidden;} ";
            $css .= ".codigo-4{width: 60px; height: 17px; position: fixed; right: 82px; line-height: 17px; text-align: center; overflow: hidden;} ";
            $css .= ".suf-4{width: 24px; height: 17px; position: fixed; right: 58px; line-height: 17px; text-align: center; overflow: hidden;} ";
            $css .= ".moneda-4{width: 8px; height: 17px; position: fixed; right: 50px; line-height: 17px; text-align: right; overflow: hidden;} ";
            $css .= ".precio-4{width: 50px; height: 17px; position: fixed; right: 0px; line-height: 17px; text-align: right; overflow: hidden;} ";
            $css .= ".guiones-4{width: 166px; height: 1px; position: fixed; right: 0px; line-height: 1px; overflow: hidden;} ";
            $css .= ".col4-cod{width: 108px; height: 17px; position: fixed; right: 58px; line-height: 17px; overflow: hidden; text-align: center; font-weight: bold;} ";
            $css .= ".col4-precio{width: 58px; height: 17px; position: fixed; right: 0px; line-height: 17px; overflow: hidden; text-align: center; font-weight: bold;} ";


            //Ahora generamos la lista completa
            $sql = "    SELECT  prefijo, codigo, sufijo, precio
                        FROM    catalogo_articulos
                        WHERE   habilitado = 1
                        ORDER BY prefijo ASC, codigo ASC, sufijo ASC";

            $stmt = $GLOBALS['conf']['pdo']->prepare($sql);
            $stmt->execute();
            $articulos = $stmt->fetchAll(PDO::FETCH_ASSOC);

            //Ordenamos naturalmente
            usort($articulos, function($a, $b) {
                //Primero ordenamos por porefijo
                if($a['prefijo'] != $b['prefijo']) 
                    return strnatcmp($a['prefijo'], $b['prefijo']);

                //Si el prefijo es igual ordenamos por codigo
                if($a['prefijo'] == $b['prefijo'])
                    return strnatcmp($a['codigo'], $b['codigo']);

                //Si el codigo es igual desempata por sufijo
                if(($a['codigo'] == $b['codigo']) && ($a['prefijo'] == $b['prefijo']))
                    return strnatcmp($a['sufijo'], $b['sufijo']);
            });

            $html = '';
            $art_col = 54;
            $art_hoja = ($art_col*4);
            $articulos_cant = count($articulos);
            $paginas = ceil($articulos_cant/$art_hoja);


            for ($p=0; $p < $paginas; $p++){
                
                $icol1 = ($art_hoja*$p);
                $icol2 = ($art_hoja*$p)+($art_col*1);
                $icol3 = ($art_hoja*$p)+($art_col*2);
                $icol4 = ($art_hoja*$p)+($art_col*3);

                //Cabeceras
                if(isset($articulos[$icol1]))
                    $html .= '<div class="col1-cod">CODIGO</div><div class="col1-precio">PRECIO</div>';
                if(isset($articulos[$icol2]))
                    $html .= '<div class="col2-cod">CODIGO</div><div class="col2-precio">PRECIO</div>';
                if(isset($articulos[$icol3]))
                    $html .= '<div class="col3-cod">CODIGO</div><div class="col3-precio">PRECIO</div>';
                if(isset($articulos[$icol4]))
                    $html .= '<div class="col4-cod">CODIGO</div><div class="col4-precio">PRECIO</div>';
            
                for ($i=0; $i < $art_col; $i++){
                    
                    $icol1 = $i+($art_hoja*$p);
                    $icol2 = $i+($art_hoja*$p)+($art_col*1);
                    $icol3 = $i+($art_hoja*$p)+($art_col*2);
                    $icol4 = $i+($art_hoja*$p)+($art_col*3);

                    $y = ($i*17)+17;
                    $yg = $i*17+17;

                    if(isset($articulos[$icol1]))
                        $html .= '<div class="pref-1" style="top: '. $y .'px;">' . $articulos[$icol1]['prefijo'] . '</div><div class="codigo-1" style="top: '. $y .'px;">' . $articulos[$icol1]['codigo'] . '</div><div class="suf-1" style="top: '. $y .'px;">' . $articulos[$icol1]['sufijo'] . '</div><div class="moneda-1" style="top: '. $y .'px;">$</div><div class="precio-1" style="top: '. $y .'px;">'.number_format($articulos[$icol1]['precio'],2).'</div><div class="guiones-1" style="top: '. $yg .'px;"><img src="./img/guiones.svg"/></div>';
                    
                    if(isset($articulos[$icol2]))
                        $html .= '<div class="pref-2" style="top: '. $y .'px;">' . $articulos[$icol2]['prefijo'] . '</div><div class="codigo-2" style="top: '. $y .'px;">' . $articulos[$icol2]['codigo'] . '</div><div class="suf-2" style="top: '. $y .'px;">' . $articulos[$icol2]['sufijo'] . '</div><div class="moneda-2" style="top: '. $y .'px;">$</div><div class="precio-2" style="top: '. $y .'px;">'.number_format($articulos[$icol2]['precio'],2).'</div><div class="guiones-2" style="top: '. $yg .'px;"><img src="./img/guiones.svg"/></div>';

                    if(isset($articulos[$icol3]))
                        $html .= '<div class="pref-3" style="top: '. $y .'px;">' . $articulos[$icol3]['prefijo'] . '</div><div class="codigo-3" style="top: '. $y .'px;">' . $articulos[$icol3]['codigo'] . '</div><div class="suf-3" style="top: '. $y .'px;">' . $articulos[$icol3]['sufijo'] . '</div><div class="moneda-3" style="top: '. $y .'px;">$</div><div class="precio-3" style="top: '. $y .'px;">'.number_format($articulos[$icol3]['precio'],2).'</div><div class="guiones-3" style="top: '. $yg .'px;"><img src="./img/guiones.svg"/></div>';

                    if(isset($articulos[$icol4]))
                        $html .= '<div class="pref-4" style="top: '. $y .'px;">' . $articulos[$icol4]['prefijo'] . '</div><div class="codigo-4" style="top: '. $y .'px;">' . $articulos[$icol4]['codigo'] . '</div><div class="suf-4" style="top: '. $y .'px;">' . $articulos[$icol4]['sufijo'] . '</div><div class="moneda-4" style="top: '. $y .'px;">$</div><div class="precio-4" style="top: '. $y .'px;">'.number_format($articulos[$icol4]['precio'],2).'</div><div class="guiones-4" style="top: '. $yg .'px;"><img src="./img/guiones.svg"/></div>';
                }


                if( !($paginas == ($p+1)) )
                    $html .= '<pagebreak />';
            }

            //Generamos el PDF
            $mpdf=new mPDF('c','A4','','',12,10,25,20,10,10);
            $mpdf->SetDisplayMode('fullpage');
            $mpdf->SetHTMLHeader('<table style="width: 800px;"><tr><td width="268"><img src="fabricantes/logo.png" style="max-height: 50px;"/></td><td width="266" align="center" style="font-size: 18px;"><b>LISTA DE PRECIOS</b></td><td align="right" width="266"></td></tr></table>');
            $mpdf->SetHTMLFooter('<table style="width: 800px;"><tr><td width="268" style="font-size: 13px;">Los precios no incluyen IVA</td><td width="266" align="center" style="font-size: 17px; font-weight: bold;">{PAGENO}</td><td align="right" style="font-size: 13px;" width="266">'.$aum['fecha'].'</td></tr></table>');
            $mpdf->WriteHTML($css,1);
            $mpdf->WriteHTML($html,2);
            $mpdf->Output('./listas/completa.pdf', 'F');



        } // Si existieron errores generando las Querys de arriba...
        catch (PDOException $e) {
            echo $e;
            // hacemos un Rollback
            $GLOBALS['conf']['pdo']->rollback();
            return;
        }
    }

    /**
     * @nombre: Confirmar reincorporaciones de Articulos
     * @descripcion: Confirmar reincorporaciones de Articulos
     */
    public function confirmar_reincorporaciones(){
        //Consultamos todas las bajas
        $sql = "    UPDATE      catalogo_articulos a
                    INNER JOIN  catalogo_articulos_actualizacion_precios aa
                    ON          (a.prefijo = aa.prefijo) AND 
                                (a.codigo = aa.codigo) AND 
                                (a.sufijo = aa.sufijo) AND 
                                (a.habilitado = 0)
                    SET         a.habilitado = 1,
                                a.precio = aa.precio,
                                a.precio_update = CURRENT_TIMESTAMP;";

        //Hacemos la consulta
        $stmt = $GLOBALS['conf']['pdo']->query($sql);
    }

    /**
     * @nombre: Confirmar bajas de Articulos
     * @descripcion: Confirmar bajas de Articulos
     */
    public function confirmar_bajas(){
        //Consultamos todas las bajas
        $sql = "    SELECT      a.id
                    FROM        catalogo_articulos a
                    LEFT JOIN   catalogo_articulos_actualizacion_precios aa
                    ON          (a.prefijo = aa.prefijo) AND 
                                (a.codigo = aa.codigo) AND 
                                (a.sufijo = aa.sufijo) AND
                                (a.habilitado = 1)
                    WHERE       aa.prefijo IS NULL
                    ORDER BY    a.prefijo ASC, a.codigo ASC, a.sufijo ASC;";

        //Hacemos la consulta
        $stmt = $GLOBALS['conf']['pdo']->query($sql);

        //Listamos los resultados 
        $ids = $stmt->fetchAll(PDO::FETCH_COLUMN);
        

        //Si existen Art. por dar de baja
        if(count($ids)){
            $sql = "UPDATE      catalogo_articulos
                    SET         habilitado = 0 
                    WHERE       id IN(".implode(',', $ids).")";

            //Hacemos la consulta
            $stmt = $GLOBALS['conf']['pdo']->query($sql);
        }
    }

    /**
     * @nombre: Confirmar altas de Articulos
     * @descripcion: Confirmar altas de Articulos
     */
    public function confirmar_altas(){
        //Actualizamos los ids
        $sql = "    UPDATE catalogo_articulos_actualizacion_precios SET id=NULL;
                    UPDATE catalogo_articulos_actualizacion_precios a 
                    JOIN catalogo_articulos b 
                    ON (a.prefijo = b.prefijo) AND (a.codigo = b.codigo) AND (a.sufijo = b.sufijo)  
                    SET a.id = b.id;
                    SELECT      *
                    FROM        catalogo_articulos_actualizacion_precios
                    WHERE       id IS NULL;";

        //Hacemos la consulta
        $stmt = $GLOBALS['conf']['pdo']->prepare($sql);
        $stmt->execute();
        $stmt->nextRowset();
        $stmt->nextRowset();
        $altas = $stmt->fetchAll(PDO::FETCH_ASSOC);

     

        //Si existen Art. por dar de alta
        if(count($altas)){
            $sql = "    INSERT INTO catalogo_articulos 
                                    (prefijo, codigo, sufijo, precio, fecha_incorporacion, precio_update) 
                        VALUES ";
            
            //Agregamos los articulos
            foreach ($altas as $k=>$v){
                if($k) $sql .= ", ";
                $sql .= "('".$v['prefijo']."', '".$v['codigo']."', '".$v['sufijo']."', '".$v['precio']."', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)";
            }

            //Hacemos la consulta
            $stmt = $GLOBALS['conf']['pdo']->query($sql);
        }
    }

    /**
     * @nombre: Listar Bajas 43-107-0
     * @descripcion: Listar Bajas
     */
    public function listar_bajas(){

        $sql = "    SELECT      CONCAT(a.prefijo, '-', a.codigo, '-', a.sufijo) as codigo
                    FROM        catalogo_articulos a
                    LEFT JOIN   catalogo_articulos_actualizacion_precios aa
                    ON          (a.prefijo = aa.prefijo) AND 
                                (a.codigo = aa.codigo) AND 
                                (a.sufijo = aa.sufijo)
                    WHERE       (aa.prefijo IS NULL) AND
                                (a.habilitado = 1)
                    ORDER BY    a.prefijo ASC, a.codigo ASC, a.sufijo ASC;";

        //Hacemos la consulta
        $stmt = $GLOBALS['conf']['pdo']->query($sql);

        //Listamos los resultados 
        $GLOBALS['resultado']->_result = $stmt->fetchAll(PDO::FETCH_NUM);
    }

    /**
     * @nombre: Listar Altas
     * @descripcion: Listar Altas
     */
    public function listar_altas(){

        //Actualizamos los ids
        $sql = "    UPDATE catalogo_articulos_actualizacion_precios SET id=NULL;
                    UPDATE catalogo_articulos_actualizacion_precios a 
                    JOIN catalogo_articulos b 
                    ON (a.prefijo = b.prefijo) AND (a.codigo = b.codigo) AND (a.sufijo = b.sufijo)  
                    SET a.id = b.id;
                    SELECT      CONCAT(prefijo,'-',codigo,'-',sufijo) as codigo
                    FROM        catalogo_articulos_actualizacion_precios
                    WHERE       id IS NULL
                    ORDER BY    codigo ASC;";

        //Hacemos la consulta
        $stmt = $GLOBALS['conf']['pdo']->prepare($sql);
        $stmt->execute();
        $stmt->nextRowset();
        $stmt->nextRowset();
        $resultados = $stmt->fetchAll(PDO::FETCH_COLUMN);
        natsort($resultados);

        //Listamos los resultados 
        $GLOBALS['resultado']->_result = $resultados;
    }

    /**
     * @nombre: Listar Reincorporaciones
     * @descripcion: Listar Reincorporaciones
     */
    public function listar_reincorporaciones(){

        //Consultamos todas las bajas
        $sql = "    SELECT      CONCAT(a.prefijo,'-',a.codigo,'-',a.sufijo) as codigo
                    FROM        catalogo_articulos a
                    INNER JOIN  catalogo_articulos_actualizacion_precios aa
                    ON          (a.prefijo = aa.prefijo) AND 
                                (a.codigo = aa.codigo) AND 
                                (a.sufijo = aa.sufijo) AND 
                                (habilitado = 0)";

        //Hacemos la consulta
        $stmt = $GLOBALS['conf']['pdo']->query($sql);

        //Listamos los resultados 
        $GLOBALS['resultado']->_result = $stmt->fetchAll(PDO::FETCH_COLUMN);
    }

    /**
     * @nombre: Informacion campo Archivo
     * @descripcion: Informacion campo Archivo
     */
    public function info_archivo(){
        //Obtenemos los usuarios
        $stmt = $GLOBALS['conf']['pdo']->prepare("  SELECT 
                                                        IF( parametros.filtro_archivo_extensiones IS NULL, '*', filtro_archivo_extensiones) as extensiones,
                                                        IF( parametros.filtro_archivo_peso IS NULL, '_PHP_', filtro_archivo_peso ) as tamanio
                                                    FROM parametros
                                                    WHERE id = 211");
        $stmt->execute();
        $datos = $stmt->fetch(PDO::FETCH_ASSOC);

        //Tamaño maximo segun php o definido por el usuario 1048576
        if($datos['tamanio'] != '_PHP_') $datos['tamanio'] = $GLOBALS['toolbox']->return_bytes($datos['tamanio'].'M'); 
        else $datos['tamanio'] = $GLOBALS['toolbox']->return_bytes(ini_get('post_max_size'));

        //Guardamos los datos
        $GLOBALS['resultado']->_result = $datos;
    }

}
?>