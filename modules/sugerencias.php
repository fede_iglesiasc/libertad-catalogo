<?php
/**
 * @nombre: Sugerencias
 * @descripcion: Envia sugerencias sobre modificaciones en el sistema.
 */
class sugerencias extends module{

    /*
	 * Constructor
	 */
    public function __construct(){
    }

    /**
     * Validaciones previas
     */
    public function __pre($accion){

        //Si existe el parametro articulo...
        if(isset($GLOBALS['parametros']['articulo']) && ($GLOBALS['parametros']['articulo'] != '')){
            $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT id FROM catalogo_articulos WHERE id = ".$GLOBALS['parametros']['articulo']);
            $stmt->execute();
            $articulo = $stmt->rowCount();
        }

        //Si no existe el Artículo
        if( isset($articulo) && !$articulo && 
            in_array($accion, array('agregar'))){
            $GLOBALS['resultado']->setError("El Artículo no existe.");
            return;
        }

        //llamamos a la accion
        return call_user_func_array(array($this, $accion), array());
    }

    /**
     * @nombre: Listar Fabricantes y codigos
     * @descripcion: Lista fabricantes y codigos de Artículos
     */
    public function fabricantes_codigo_listar(){

        //EStamos buscando o solo listando?
        $busqueda = false;
        if($GLOBALS['parametros']['q'] != ''){
            $busqueda = true;
            $keyword_tokens = explode(' ', $GLOBALS['parametros']['q']);
        }

        //Calculate paging
        $page =  (intval($GLOBALS['parametros']['p'])-1) * 40;


        $sql1 = "SELECT 
                        codigo, 
                        articulo_id, 
                        marca_id, 
                        marca_label, 
                        busqueda, 
                        (";

        //Si hacemos una 
        //busqueda...
        if(!$busqueda) $sql1 .= "1";
        else
            foreach($keyword_tokens as $key=>$palabra){
                if($key > 0) $sql1 .= "+";
                $sql1 .= "IF(LOCATE('".$palabra."',LOWER(busqueda))>0,1,0)";
            }

        $sql1 .= ") AS score  
                    
                    FROM    (
                                SELECT 
                                        (CONCAT(t.prefijo,'-',t.codigo,'-',t.sufijo)) as codigo, 
                                        t.id as articulo_id, 
                                        t.id_marca as marca_id, 
                                        (SELECT nombre FROM catalogo_marcas WHERE id = t.id_marca) as marca_label, 
                                        CONCAT((SELECT nombre FROM catalogo_marcas WHERE id = t.id_marca),' ', t.prefijo,'-',t.codigo,'-',t.sufijo) as busqueda
                                FROM    catalogo_articulos as t
                                WHERE   t.id_marca IS NOT NULL
                            ) as r 

                    HAVING      score>0 

                    ORDER BY    score DESC, 
                                marca_label ASC";



        //Make query
        $stmt = $GLOBALS['conf']['pdo']->prepare($sql1);
        $stmt->execute();
        $fabricantes =  $stmt->fetchAll(PDO::FETCH_ASSOC);
        
        //Si estamos buscando...
        if($busqueda){
            //Score
            foreach($fabricantes as $k=>$v){
                $fabricantes[$k]['score'] = $GLOBALS['toolbox']->compare_strings($GLOBALS['parametros']['q'], $v['marca_label'].' '.$v['codigo']);
                if(is_null($v['codigo']) && is_null($v['articulo_id']) && ($fabricantes[$k]['score'] > 1))
                    $fabricantes[$k]['score'] = $fabricantes[$k]['score'] + 10;
            }

            //Eliminamos 0's
            foreach($fabricantes as $k=>$v)
                if($v['score'] == 0)
                    unset($fabricantes[$k]);

            //Ordenamos
            usort($fabricantes, function($a, $b) {
                return $b['score'] - $a['score'];
            });
        }


        //Calculamos totales
        $tot = count($fabricantes);
        $GLOBALS['resultado']->_result['total'] = $tot;

        //Cortamos el pedazo de array que nos interesa
        $res = array_slice($fabricantes, $page, 40);

        //Eliminamos datos que no utilizamos
        foreach($res as $k=>$e){
            unset($res[$k]['score'], $res[$k]['codbin']);
        }

        //Save items to result Object
        $GLOBALS['resultado']->_result['items'] = $res;
    }

    /**
     * @nombre: Envia Sugerencia
     * @descripcion: Envia Sugerencia
     */
    public function agregar(){
        $usuario = $GLOBALS['session']->getData('usuario');
        $usuario = $GLOBALS['session']->getData('usuario');
        $sql = "    INSERT INTO     sugerencias (usuario, articulo, comentario) 
                    VALUES          ('".$usuario."',".$GLOBALS['parametros']['articulo'].",'".$GLOBALS['parametros']['comentarios']."')";
        //Query DB
        $stmt = $GLOBALS['conf']['pdo']->query($sql);
    }

    /**
     * @nombre: Lista las Sugerencias
     * @descripcion: Lista las Sugerencias
     */
    public function listar(){

        $usuario = $GLOBALS['session']->getData('usuario');
        $rol = $GLOBALS['session']->getData('rol');

        $administra = false;

        //Permisos del rol actual
        $stmt = $GLOBALS['conf']['pdo']->query("SELECT COUNT(*) as permiso FROM permisos_roles WHERE rol_id=".$rol." AND funcion_id = 74");
        $permiso_rol = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $permiso_rol = $permiso_rol[0]['permiso'];

        if((int)$permiso_rol) $administra = true;

        //Permisos del usuario actual
        $stmt = $GLOBALS['conf']['pdo']->query("SELECT acceso FROM permisos_usuarios WHERE funcion_id=74 AND usuario_id='".$usuario."'");
        $permiso_usuario = $stmt->fetchAll(PDO::FETCH_ASSOC);
        
        //Guardamos el usuario para luego er isi tiene permisos para administrar o no
        if(count($permiso_usuario)) $administra = (bool)(int)$permiso_usuario[0]['permiso'];

        //Si es administrador
        if($rol == -1) $administra = true;

        //Traemos todas las sugerencias
        $sql = "    SELECT          id, 
                                    (   SELECT  CONCAT(prefijo,'-',codigo,'-',sufijo) 
                                        FROM    catalogo_articulos 
                                        WHERE   id = articulo) as codigo, 
                                    usuario,
                                    (SELECT razon_social FROM clientes WHERE usuario = s.usuario) as r, 
                                    comentario, 
                                    DATE_FORMAT(fecha,'%m-%d-%Y') as fecha, 
                                    completado 

                    FROM            sugerencias as s ";

        //Filtramos por usuario
        if(!$administra) $sql .= " WHERE usuario='".$usuario."' ";

        //Agregamos orden
        $sql .= "ORDER BY completado ASC, fecha DESC";

        //Query DB
        $stmt = $GLOBALS['conf']['pdo']->query($sql);
        $sugerencias = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $GLOBALS['resultado']->_result = $sugerencias;
    }

    /**
     * @nombre: Marca como resuelta una Sugerencia
     * @descripcion: Marca como resuelta una Sugerencia
     */
    public function resolver(){
        //Query DB
        $stmt = $GLOBALS['conf']['pdo']->query("UPDATE sugerencias SET completado = 1 WHERE id = ".$GLOBALS['parametros']['sugerencia']);
    }

    /**
     * @nombre: Elimina una Sugerencia
     * @descripcion: Elimina una Sugerencia
     */
    public function eliminar(){
        $usuario = $GLOBALS['session']->getData('usuario');
        $rol = $GLOBALS['session']->getData('rol');

        $administra = false;

        //Permisos del rol actual
        $stmt = $GLOBALS['conf']['pdo']->query("SELECT COUNT(*) as permiso FROM permisos_roles WHERE rol_id=".$rol." AND funcion_id = 74");
        $permiso_rol = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $permiso_rol = $permiso_rol[0]['permiso'];

        if((int)$permiso_rol) $administra = true;

        //Permisos del usuario actual
        $stmt = $GLOBALS['conf']['pdo']->query("SELECT acceso FROM permisos_usuarios WHERE funcion_id=74 AND usuario_id='".$usuario."'");
        $permiso_usuario = $stmt->fetchAll(PDO::FETCH_ASSOC);
        
        //Guardamos el usuario para luego er isi tiene permisos para administrar o no
        if(count($permiso_usuario)) $administra = (bool)(int)$permiso_usuario[0]['permiso'];

        //Si es administrador
        if($rol == -1) $administra = true;

        //Query DB
        $sql = "DELETE FROM sugerencias WHERE (completado = 0)";
        if(!$administra) $sql .= " AND (usuario = '".$usuario."')";
        $sql .= " AND (id = ".$GLOBALS['parametros']['sugerencia'].")";
                                    ;
        $stmt = $GLOBALS['conf']['pdo']->query($sql);
    }

    /**
     * @nombre: Info de una Sugerencia
     * @descripcion: Info de una Sugerencia
     */
    public function info(){
        $sql = "SELECT * FROM sugerencias WHERE id = ".$GLOBALS['parametros']['sugerencia'];
        $stmt = $GLOBALS['conf']['pdo']->query($sql);
        $sugerencias = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if(count($sugerencias)) $sugerencias = $sugerencias[0];
        $GLOBALS['resultado']->_result = $sugerencias;
    }

    /**
     * @nombre: Edita una Sugerencia
     * @descripcion: Edita una Sugerencia
     */
    public function editar(){
        $usuario = $GLOBALS['session']->getData('usuario');

        $sql = "    UPDATE  sugerencias 
                    SET     comentario='".$GLOBALS['parametros']['comentarios']."' 
                    WHERE   (id=".$GLOBALS['parametros']['sugerencia'].") AND (usuario = '".$usuario."')";

        $stmt = $GLOBALS['conf']['pdo']->query($sql);
    }

}
?>