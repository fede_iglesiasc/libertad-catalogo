<?php
/**
 * @nombre: Menu
 * @descripcion: Controlador del Menu principal
 */
class menu extends module{


    /*
	 * Constructor
	 */
    public function __construct(){
    }

    /**
     * Validaciones previas
     */
    public function __pre($accion){

        //Si existe el parametro parent...
        if(isset($GLOBALS['parametros']['parent'])){
            $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT id FROM menu WHERE id = ".$GLOBALS['parametros']['parent']);
            $stmt->execute();
            $parent = $stmt->rowCount();
        }

        //Si existe el parametro item...
        if(isset($GLOBALS['parametros']['item'])){
            $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT id FROM menu WHERE id = ".$GLOBALS['parametros']['item']);
            $stmt->execute();
            $item = $stmt->rowCount();
        }

        //Si no existe la parent
        if( isset($parent) && !$parent && 
            in_array($accion, array('crear','mover'))){
            $GLOBALS['resultado']->setError("El Item Padre no existe.");
            return;
        }

        //Si no existe la item
        if( isset($item) && !$item && 
            in_array($accion, array('renombrar','mover','eliminar','info','editar'))){
            $GLOBALS['resultado']->setError("El Item no existe.");
            return;
        }

        //llamamos a la accion
        return call_user_func_array(array($this, $accion), array());
    }

    /**
     * @nombre: Lista el menu completo
     * @descripcion: Devuelve el menu completo en forma de Arbol
     */
    public function listar(){

        //Obtenemos los usuarios
        $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT *, (SELECT COUNT(*) as hijos FROM menu WHERE parent = t.id) as type FROM menu as t ORDER BY posicion");
        $stmt->execute();
        $menu = $stmt->fetchAll(PDO::FETCH_ASSOC);

        //Recorremos en busqueda del item id 0
        //y lo transformamos en #
        foreach($menu as $k=>$v){

            //Agregamos estado
            $menu[$k]['state']['disabled'] = false;
            $menu[$k]['state']['selected'] = false;



            //Para el root definimos padre '#'
            if($v['id'] == 1){
                $menu[$k]['parent'] = '#';
                $menu[$k]['state']['selected'] = true;
            }

        }

        //Guardamos los datos
        $GLOBALS['resultado']->_result = $menu;
    }

    /**
     * @nombre: Crear un Item en el Menu
     * @descripcion: Crear item en el menu
     */
    public function crear(){

        // Begin Transaction
        $GLOBALS['conf']['pdo']->beginTransaction();

        try {

            //Obtenemos última posición
            $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT COUNT(*) as pos FROM menu WHERE parent = ".$GLOBALS['parametros']['parent']);
            $stmt->execute();
            $pos = $stmt->fetch(PDO::FETCH_ASSOC);
            $pos = $pos['pos'];

            //DB query
            $stmt = $GLOBALS['conf']['pdo']->prepare("INSERT INTO menu (parent, text, posicion) VALUES (".$GLOBALS['parametros']['parent'].",'".$GLOBALS['parametros']['nombre']."', ".$pos." )");
            $stmt->execute();
 
            // Commit Transaction
            $GLOBALS['conf']['pdo']->commit();

        } // Si existieron errores
        catch (PDOException $e) {
            // hacemos un Rollback
            $GLOBALS['conf']['pdo']->rollback();

            //Agregamos error 
            $GLOBALS['resultado']->setError($e);
            return;
        }
    }

    /**
     * @nombre: Renombra un Item del Menu
     * @descripcion: Renombra item del menu
     */
    public function renombrar(){

        //DB query
        $stmt = $GLOBALS['conf']['pdo']->prepare("UPDATE menu SET text='".$GLOBALS['parametros']['nombre']."' WHERE id = ".$GLOBALS['parametros']['item']);
        $items = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $stmt->execute();
    }

    /**
     * @nombre: Mover un Item del Menu
     * @descripcion: Mover item del menu
     */
    public function mover(){

        //Obtenemos todos los hijos del padre al que queremos mover el item
        $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT id, posicion FROM menu WHERE parent = ".$GLOBALS['parametros']['parent']." ORDER BY posicion ASC");
        $stmt->execute();
        $hijos = $stmt->fetchAll(PDO::FETCH_ASSOC);

        //Si este item ya se encontraba 
        //entre los hijos lo quitamos
        foreach($hijos as $k=>$v)
            if((int)$v['id'] == (int)$GLOBALS['parametros']['item']){
                unset($hijos[$k]);
                $hijos = array_values($hijos);
                break;
            }

        //Generamos Array
        if(count($hijos)){
            $hijos = $GLOBALS['toolbox']->insertArrayIndex(  $hijos,
                array("id"=>$GLOBALS['parametros']['item'], "posicion"=>$GLOBALS['parametros']['posicion']),
                (int)$GLOBALS['parametros']['posicion']
            );

        }else{
            $hijos[] = array("id"=>$GLOBALS['parametros']['item'], "posicion"=>$GLOBALS['parametros']['posicion']);
        }

        // Begin Transaction
        $GLOBALS['conf']['pdo']->beginTransaction();

        try {

            //Hacemos los updates
            foreach($hijos as $k=>$v){
                $stmt = $GLOBALS['conf']['pdo']->prepare("UPDATE menu SET parent = ".$GLOBALS['parametros']['parent'].", posicion=".$k." WHERE id = ".$v['id']);
                $stmt->execute();
            }
 
            // Commit Transaction
            $GLOBALS['conf']['pdo']->commit();

        } // Si existieron errores
        catch (PDOException $e) {
            // hacemos un Rollback
            $GLOBALS['conf']['pdo']->rollback();

            //Agregamos error 
            $GLOBALS['resultado']->setError($e);
            break;
        }
    }

    /**
     * @nombre: Elimina un Item del Menú
     * @descripcion: Elimina item del menu
     */
    public function eliminar(){

        //DB query
        $stmt = $GLOBALS['conf']['pdo']->prepare("DELETE FROM menu WHERE id = ".$GLOBALS['parametros']['item']);
        $stmt->execute();
    }

    /**
     * @nombre: Obtiene Información del Item del Menú
     * @descripcion: Información del Item
     */
    public function info(){

        //Obtenemos los usuarios
        $stmt = $GLOBALS['conf']['pdo']->prepare("  SELECT  text, click, permiso
                                                    FROM    menu
                                                    WHERE   id = ".$GLOBALS['parametros']['item']);
        $stmt->execute();
        $item = $stmt->fetch(PDO::FETCH_ASSOC);

        //Guardamos los datos
        $GLOBALS['resultado']->_result = $item;
    }

    /**
     * @nombre: Edita la configuracion del Item del Menú
     * @descripcion: Información del Item
     */
    public function editar(){
        
        //Obtenemos los usuarios
        $stmt = $GLOBALS['conf']['pdo']->prepare("  UPDATE  menu 
                                                    SET     click='".$GLOBALS['parametros']['click']."', 
                                                            permiso='".$GLOBALS['parametros']['permiso']."'

                                                    WHERE   id=".$GLOBALS['parametros']['item']);
        $stmt->execute();
    }
}
?>