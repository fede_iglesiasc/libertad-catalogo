<?php
/**
 * @nombre: Fabricantes
 * @descripcion: Lista, Edita, Crea y Elimina Fabricantes.
 */
class fabricantes extends module{


    /**
	 * Constructor
	 */
    public function __construct(){

    }

    /**
     * Validaciones previas
     */
    public function __pre($accion){

        //Si existe el parametro fabricante...
        if(isset($GLOBALS['parametros']['fabricante']) && ($GLOBALS['parametros']['fabricante'] != '')){
            $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT id FROM catalogo_marcas WHERE  id= ".$GLOBALS['parametros']['fabricante']);
            $stmt->execute();
            $fabricante = $stmt->rowCount();
        }

        //Si existe el fabricante
        if( isset($fabricante) && !$fabricante && 
            in_array($accion, array('info','eliminar','editar'))){
            $GLOBALS['resultado']->setError("El Fabricante no existe");
            return;
        }

        //llamamos a la accion
        return call_user_func_array(array($this, $accion), array());
    }

    /**
     * @nombre: Lista los Fabricantes
     * @descripcion: Lista los Fabricantes
     */
    public function listar(){

        //Obtenemos los usuarios
        $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT id, nombre, descripcion FROM catalogo_marcas WHERE padre = 1 ORDER BY nombre");
        $stmt->execute();
        $fabricantes = $stmt->fetchAll(PDO::FETCH_ASSOC);

        //Tenemos los logos?
        foreach($fabricantes as $k=>$fabricante){
            $fabricantes[$k]['descripcion'] = nl2br($fabricantes[$k]['descripcion']);
            $fabricantes[$k]['logo'] = 'no-image.svg';
            if(file_exists('./fabricantes/'.$fabricante['id'].'.svg'))
                $fabricantes[$k]['logo'] = $fabricante['id'].'.svg';
        }

        //Resultados
        $GLOBALS['resultado']->_result = $fabricantes;
    }

    /**
     * @nombre: Información de Fabricante
     * @descripcion: Información de Fabricante
     */
    public function info(){

        //Obtenemos los usuarios
        $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT nombre, descripcion, promocion_prioridad FROM catalogo_marcas WHERE id =".$GLOBALS['parametros']['fabricante']);
        $stmt->execute();
        $datos = $stmt->fetch(PDO::FETCH_ASSOC);

        $datos['promocion_prioridad'] = array('id'=>$datos['promocion_prioridad']);

        //Agregamos los labels
        if($datos['promocion_prioridad']['id'] == '11') $datos['promocion_prioridad']['nombre'] = 'Aleatorio';
        elseif($datos['promocion_prioridad']['id'] == '0') $datos['promocion_prioridad']['nombre'] = 'No mostrar';
        else $datos['promocion_prioridad']['nombre'] = $datos['promocion_prioridad']['id'];

        //Resultados
        $GLOBALS['resultado']->_result = $datos;
    }

    /**
     * @nombre: Edita un Fabricante
     * @descripcion: Edita un Fabricante
     */
    public function editar(){

        //Obtenemos los usuarios
        $stmt = $GLOBALS['conf']['pdo']->prepare("UPDATE catalogo_marcas SET nombre='".$GLOBALS['parametros']['nombre']."', descripcion='".$GLOBALS['parametros']['descripcion']."', promocion_prioridad=".$GLOBALS['parametros']['promocion_prioridad']." WHERE id=".$GLOBALS['parametros']['fabricante']);
        $stmt->execute();
    }

    /**
     * @nombre: Agregar un Fabricante
     * @descripcion: Agregar un Fabricante
     */
    public function agregar(){

        $stmt = $GLOBALS['conf']['pdo']->prepare("INSERT INTO catalogo_marcas (nombre, descripcion, padre, nivel) VALUES ('".$GLOBALS['parametros']['nombre']."','".$GLOBALS['parametros']['descripcion']."',1,2)");
        $items = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $stmt->execute();
    }

    /**
     * @nombre: Eliminar un Fabricante
     * @descripcion: Eliminar un Fabricante
     */
    public function eliminar(){

        //Obtenemos los usuarios
        $stmt = $GLOBALS['conf']['pdo']->prepare("DELETE FROM catalogo_marcas WHERE id=".$GLOBALS['parametros']['fabricante']);
        $stmt->execute();

        //Eliminamos el archivo con el logo si es que existe
        if(file_exists('./fabricantes/'.$GLOBALS['parametros']['fabricante'].'.svg'))
            unlink('./fabricantes/'.$GLOBALS['parametros']['fabricante'].'.svg');
    }

    /**
     * @nombre: Agregar Logo al Fabricante
     * @descripcion: Agregar Logo al Fabricante
     */
    public function logo(){

        //Nuevo Logo
        $logo = $GLOBALS['parametros']['archivo']['nuevo_directorio'].$GLOBALS['parametros']['archivo']['name'];

        if(file_exists($logo))
            rename($logo, $GLOBALS['parametros']['archivo']['nuevo_directorio'].$GLOBALS['parametros']['fabricante'].'.svg');       
    }

    /**
     * @nombre: Agregar Logo al Fabricante
     * @descripcion: Agregar Logo al Fabricante
     */
    public function promocion_prioridad(){

    }
}
?>