<?php
/**
 * @nombre: Cuentas
 * @descripcion: Crea nuevos usuarios (Clientes) y funciones relacionadas.
 */
class Cuenta extends module{
 
    public function __construct(){

    }

    /**
     * Validaciones previas
     */
    public function __pre($accion){

        //Si existe el parametro email...
        if(isset($GLOBALS['parametros']['email']) && ($GLOBALS['parametros']['email'] != '')){
            $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT * FROM usuarios WHERE email = '".$GLOBALS['parametros']['email']."'");
            $stmt->execute();
            $email = $stmt->rowCount();
            $usr_email = $stmt->fetch(PDO::FETCH_ASSOC);
        }

        //No repetir el Email
        if( in_array($accion, array('editar')) && isset($email) && $email &&
            ($usr_email['usuario'] != $GLOBALS['session']->getData('usuario')) ){
            //Error
            $GLOBALS['resultado']->setError("El Email que esta intentando agregar esta siendo usado por otro usuario.");
            return;
        }

        //llamamos a la accion
        return call_user_func_array(array($this, $accion), array());
    }

    /**
     * @nombre: Información de la Cuenta del Usuario
     * @descripcion: Nos devuelve los datos del usuario actual.
     */
    public function info(){

        //Obtenemos el usuario actual
        $usuario = $GLOBALS['session']->getData('usuario');

        $vista_previa_prefijo = '15';
        $vista_previa_codigo = '30112';
        $vista_previa_sufijo = '0';


        //Obtenemos los usuarios
        $stmt = $GLOBALS['conf']['pdo']->prepare("  SELECT
                                                        usuario,
                                                        activo,
                                                        email,
                                                        nombre,
                                                        precio_venta, 
                                                        ajuste, 
                                                        mostrar_precio_lista, 
                                                        mostrar_precio_costo, 
                                                        mostrar_precio_venta,
                                                        (   SELECT  FORMAT(precio,2) 
                                                            FROM    catalogo_articulos 
                                                            WHERE   (prefijo = '".$vista_previa_prefijo."') AND (codigo = '".$vista_previa_codigo."') AND (sufijo='".$vista_previa_sufijo."')) as vista_previa_precio_lista

                                                        
                                                    FROM usuarios
                                                    WHERE usuario = '".$usuario."'");
        $stmt->execute();
        $user = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if(count($user)) $user = $user[0];
        //

        //El precio de costo arranca siendo precio de lista
        $user['vista_previa_precio_costo'] =  $user['vista_previa_precio_lista'];

        //Si es cliente traemos su perfil
        if( $GLOBALS['session']->getData('rol') == '2' ){
            //Obtenemos los usuarios
            $stmt = $GLOBALS['conf']['pdo']->prepare("  SELECT
                                                            *,
                                                            (SELECT descuento FROM descuentos_cond_vta WHERE id = clientes.condicion_venta) as condicion_venta_descuento,
                                                            ( SELECT (CONCAT( 
                                                                sys_localidades.nombre,
                                                                ', ',
                                                                (SELECT nombre FROM sys_provincias WHERE id = sys_localidades.provincia_id)
                                                            )) as nombre
                                                            FROM sys_localidades WHERE id = clientes.localidad) as localidad_label
                                                        FROM clientes
                                                        WHERE usuario = '".$usuario."'");
            $stmt->execute();
            $cliente = $stmt->fetchAll(PDO::FETCH_ASSOC);
            if(count($cliente)) $cliente = $cliente[0];


            //Si el descuento de la condicion de venta es mayor a 0
            if((double)$cliente['condicion_venta_descuento'] > 0){
                $user['vista_previa_precio_costo'] = $user['vista_previa_precio_costo'] - ($user['vista_previa_precio_costo'] * ((double)$cliente['condicion_venta_descuento'] / 100));
            }

            //Descuento por Linea
            $stmt = $GLOBALS['conf']['pdo']->query("SELECT * FROM descuentos WHERE cliente = '".$usuario."' AND prefijo = '".$vista_previa_prefijo."'");
            $cliente_descuento_linea = $stmt->fetchAll(PDO::FETCH_ASSOC);


            //Si existe algun descuento sobre la linea actual
            if(count($cliente_descuento_linea)){
                //Aplicamos el descuento
                $user['vista_previa_precio_costo'] = $user['vista_previa_precio_costo'] - ($user['vista_previa_precio_costo'] * ((double)$cliente_descuento_linea[0]['descuento'] / 100));
            }else{


                //Traemos ademas el descuento por letra que el cliente tiene sobre los articulos
                $stmt = $GLOBALS['conf']['pdo']->query("SELECT descuento_letra FROM clientes WHERE usuario = '".$usuario."'");
                $cliente_letra_descuento = $stmt->fetchAll(PDO::FETCH_ASSOC);
                $cliente_letra_descuento = $cliente_letra_descuento[0]['descuento_letra'];


                if($cliente_letra_descuento != NULL){

                    $cliente_letra_descuento = strtoupper($cliente_letra_descuento);

                    //Descuento del Cliente segun Letra
                    //$stmt = $GLOBALS['conf']['pdo']->query(" SELECT descuento_".$cliente_letra_descuento." FROM catalogo_articulos WHERE (prefijo = '".$vista_previa_prefijo."') AND (codigo = '".$vista_previa_codigo."') AND (sufijo='".$vista_previa_sufijo."')");
                    //$cliente_descuento = $stmt->fetchAll(PDO::FETCH_ASSOC);
                    $sql = "SELECT      * 
                            FROM        catalogo_articulos_descuentos
                            WHERE       letra = '".$cliente_letra_descuento."' 
                                        AND (prefijo = '".$vista_previa_prefijo."') 
                                        AND (codigo = '".$vista_previa_codigo."') 
                                        AND (sufijo = '".$vista_previa_sufijo."')";

                    $stmt = $GLOBALS['conf']['pdo']->query($sql);
                    $cliente_descuento = $stmt->fetchAll(PDO::FETCH_ASSOC);

                    if(count($cliente_descuento)) 
                        $cliente_descuento = $cliente_descuento[0]['descuento'];

                    //Precio de costo
                    if((double)$cliente_descuento > 0)
                        $user['vista_previa_precio_costo'] = $user['vista_previa_precio_costo'] - ($user['vista_previa_precio_costo'] * ((double)$cliente_descuento / 100));
                }

            }


            //Redondeamos a dos decimales
            $user['vista_previa_precio_costo'] = number_format((float)$user['vista_previa_precio_costo'], 2, '.', '');
        }



        //Pegamos todos los datos
        if(isset($cliente)) $user = array_merge($user, $cliente);

        //Guardamos los datos
        $GLOBALS['resultado']->_result = $user;
        //$GLOBALS['resultado']->_result['letra_descuento'] = $cliente_descuento;
    }

    /**
     * @nombre: Editar Cuenta del Usuario
     * @descripcion: Comente cambios a la DB de la Cuenta
     */
    public function editar(){
        //Cambiamos la contraseña
        $stmt = $GLOBALS['conf']['pdo']->prepare("  UPDATE  usuarios 
                                                    SET     email='".$GLOBALS['parametros']['email']."',
                                                            nombre='".$GLOBALS['parametros']['nombre']."'
                                                    WHERE   usuario = '".$GLOBALS['session']->getData('usuario')."'");
        $stmt->execute();
    }


    /**
     * @nombre: Cambiar la Contraseña de la Cuenta
     * @descripcion: Cambiar la Contraseña de la Cuenta
     */
	public function chngPass(){

        //Obtenemos contraseña actual
        $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT password FROM usuarios WHERE usuario = '".$GLOBALS['session']->getData('usuario')."'");
        $stmt->execute();
        $usuario = $stmt->fetch();

        //Si la contraseña coincide con la actual...
        if($GLOBALS['parametros']['password_nuevo'] != $GLOBALS['parametros']['password_nuevo_repetido']){
            //Agregamos error 
            $GLOBALS['resultado']->setError("La nueva contraseña no coincide con la repetida.");
            return;
        }

        //Si la contraseña coincide con la actual...
        if($usuario['password'] != $GLOBALS['parametros']['password']){
            //Agregamos error 
            $GLOBALS['resultado']->setError("La contraseña actual no es la que ha ingresado.");
            return;
        }

        //Si las contraseñas son iguales... no las cambiamos
        if($GLOBALS['parametros']['password_nuevo'] == $usuario['password']){
            //Agregamos error 
            $GLOBALS['resultado']->setError("La contraseña actual y la nueva por la cual quiere cambiarla son iguales.");
            return;
        }

        //Cambiamos la contraseña
        $stmt = $GLOBALS['conf']['pdo']->prepare("  UPDATE  usuarios 
                                                    SET     password='".$GLOBALS['parametros']['password_nuevo']."' 
                                                    WHERE   usuario = '".$GLOBALS['session']->getData('usuario')."'");
        $stmt->execute();
	}

    /**
     * @nombre: Guardar Configuracion de Precios
     * @descripcion: Guardar Configuracion de Precios
     */
    public function precios_editar(){

        //Cambiamos la contraseña
       $stmt = $GLOBALS['conf']['pdo']->query("  UPDATE  usuarios 
                                                    SET     precio_venta = ".$GLOBALS['parametros']['precio_venta'].",
                                                            ajuste = ".$GLOBALS['parametros']['ajuste'].",
                                                            mostrar_precio_lista = ".$GLOBALS['parametros']['mostrar_precio_lista'].",
                                                            mostrar_precio_costo = ".$GLOBALS['parametros']['mostrar_precio_costo'].",
                                                            mostrar_precio_venta = ".$GLOBALS['parametros']['mostrar_precio_venta']."
                                                    WHERE   usuario = '".$GLOBALS['session']->getData('usuario')."'");
    }



    //////////////////////////////////////////////
    //              VACACIONES                  //
    //////////////////////////////////////////////
    
    /**
    * @nombre: Lista las Vacaciones del Usuario
    * @descripcion: Lista las Vacaciones del Usuario
    */
    public function vacaciones_listar(){

        //Usuario
        $usuario = $GLOBALS['session']->getData('usuario');

        //SQL
        $stmt = $GLOBALS['conf']['pdo']->query("    SELECT  id,
                                                            DATE_FORMAT(inicio,'%d-%m-%Y') as inicio,
                                                            DATE_FORMAT(fin,'%d-%m-%Y') as fin 
                                                    FROM    horarios_vacaciones 
                                                    WHERE   usuario='".$usuario."'");
        $datos =  $stmt->fetchAll(PDO::FETCH_ASSOC);

        $stmt = $GLOBALS['conf']['pdo']->query("SELECT * FROM usuarios WHERE usuario='".$usuario."'");
        $usuario =  $stmt->fetchAll(PDO::FETCH_ASSOC);

        $GLOBALS['resultado']->_result['nombre'] = $usuario[0]['nombre'];
        $GLOBALS['resultado']->_result['usuario'] = $usuario[0]['usuario'];
        $GLOBALS['resultado']->_result['vacaciones'] = $datos;
    }

    /**
     * @nombre: Información de las Vacaciones del Usuario
     * @descripcion: Información de las Vacaciones del Usuario
     */
    public function vacaciones_info(){

        //Usuario
        $usuario = $GLOBALS['session']->getData('usuario');

        //Obtenemos los usuarios
        $stmt = $GLOBALS['conf']['pdo']->query("    SELECT  id,
                                                            DATE_FORMAT(inicio,'%d-%m-%Y') as inicio,
                                                            DATE_FORMAT(fin,'%d-%m-%Y') as fin 
                                                    FROM    horarios_vacaciones
                                                    WHERE   (usuario='".$usuario."') AND (id = ".$GLOBALS['parametros']['vacaciones'].")");
        $datos =  $stmt->fetch(PDO::FETCH_ASSOC);

        //Guardamos los datos
        $GLOBALS['resultado']->_result = $datos;
    }

    /**
     * @nombre: Agregar Vacaciones a un Usuario
     * @descripcion: Agregar Vacaciones a un Usuario
     */
    public function vacaciones_agregar(){

        //Usuario
        $usuario = $GLOBALS['session']->getData('usuario');

        //Revisamos formatos de las fechas...
        //Validamos las fechas
        $inicio = array_map('intval', explode('/',$GLOBALS['parametros']['inicio']));
        $fin = array_map('intval', explode('/',$GLOBALS['parametros']['fin']));

        //Error de formato
        if(!checkdate($inicio[1],$inicio[0],$inicio[2])){
            //Agregamos error 
            $GLOBALS['resultado']->setError("El rango de Fechas tiene un formato incorrecto.");
            return;
        }

        //Error de formato
        if(!checkdate($fin[1],$fin[0],$fin[2])){
            //Agregamos error 
            $GLOBALS['resultado']->setError("El rango de Fechas tiene un formato incorrecto.");
            return;
        }

        $fecha_inicio = strtotime($inicio[1].'/'.$inicio[0].'/'.$inicio[2]);
        $fecha_fin = strtotime($fin[1].'/'.$fin[0].'/'.$fin[2]);

        //Error de rango
        if($fecha_inicio > $fecha_fin){
            //Agregamos error 
            $GLOBALS['resultado']->setError("El rango de fechas es incorrecto.");
            return;
        }



        //SQL
        $stmt = $GLOBALS['conf']['pdo']->prepare("  INSERT INTO     horarios_vacaciones (usuario, inicio, fin) 
                                                    VALUES          ('".$usuario."','".date("Y-m-d",$fecha_inicio)."','".date("Y-m-d",$fecha_fin)."')");
        
        $items = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $stmt->execute();
    }

    /**
     * @nombre: Edita las Vacaciones del Usuario
     * @descripcion: Edita las Vacaciones del Usuario
     */
    public function vacaciones_editar(){

        //Usuario
        $usuario = $GLOBALS['session']->getData('usuario');

        //Revisamos formatos de las fechas...
        //Validamos las fechas
        $inicio = array_map('intval', explode('/',$GLOBALS['parametros']['inicio']));
        $fin = array_map('intval', explode('/',$GLOBALS['parametros']['fin']));

        //Error de formato
        if(!checkdate($inicio[1],$inicio[0],$inicio[2])){
            //Agregamos error 
            $GLOBALS['resultado']->setError("El rango de Fechas tiene un formato incorrecto.");
            return;
        }

        //Error de formato
        if(!checkdate($fin[1],$fin[0],$fin[2])){
            //Agregamos error 
            $GLOBALS['resultado']->setError("El rango de Fechas tiene un formato incorrecto.");
            return;
        }

        $fecha_inicio = strtotime($inicio[1].'/'.$inicio[0].'/'.$inicio[2]);
        $fecha_fin = strtotime($fin[1].'/'.$fin[0].'/'.$fin[2]);

        //Error de rango
        if($fecha_inicio > $fecha_fin){
            //Agregamos error 
            $GLOBALS['resultado']->setError("El rango de fechas es incorrecto.");
            return;
        }

        //Obtenemos los usuarios
        $stmt = $GLOBALS['conf']['pdo']->prepare("  UPDATE  horarios_vacaciones 
                                                    SET     inicio='".date("Y-m-d",$fecha_inicio)."', 
                                                            fin='".date("Y-m-d",$fecha_fin)."' 
                                                    WHERE   (usuario = '".$usuario."') AND 
                                                            (id=".$GLOBALS['parametros']['vacaciones'].")");
        $stmt->execute();
    }

    /**
     * @nombre: Eliminar las Vacaciones del Usuario
     * @descripcion: Elimina las Vacaciones del Usuario
     */
    public function vacaciones_eliminar(){

        //Usuario
        $usuario = $GLOBALS['session']->getData('usuario');

        //Obtenemos los usuarios
        $stmt = $GLOBALS['conf']['pdo']->prepare("  DELETE FROM horarios_vacaciones 
                                                    WHERE   (id = ".$GLOBALS['parametros']['vacaciones'].") AND
                                                            (usuario = '".$usuario."')");
        $stmt->execute();
    }
}

?>