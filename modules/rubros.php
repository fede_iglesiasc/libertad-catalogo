<?php
/**
 * @nombre: Rubros
 * @descripcion: Lista, edita, elimina Rubros y funciones afines.
 */
class rubros extends module{

    /*
	 * Constructor
	 */
    public function __construct(){
    }

    /**
     * Validaciones previas
     */
    public function __pre($accion){

        //Si existe el parametro rubro...
        if(isset($GLOBALS['parametros']['rubro']) && ($GLOBALS['parametros']['rubro'] != '')){
            $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT id FROM catalogo_rubros WHERE id = ".$GLOBALS['parametros']['rubro']);
            $stmt->execute();
            $rubro = $stmt->rowCount();
        }

        //Si no existe la rubro
        if( isset($rubro) && !$rubro && 
            in_array($accion, array('editar'))){
            $GLOBALS['resultado']->setError("El Rubro no existe");
            return;
        }

        //llamamos a la accion
        return call_user_func_array(array($this, $accion), array());
    }

    /**
     * @nombre: Lista todos los Rubros
     * @descripcion: Lista todos los Rubros
     */
    public function listar(){

        //Obtenemos los usuarios
        $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT id, padre, nombre, nombre_singular FROM catalogo_rubros");
        $stmt->execute();
        $rubros = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT * FROM catalogo_rubros_datos ORDER BY orden");
        $stmt->execute();
        $dimensiones = $stmt->fetchAll(PDO::FETCH_ASSOC);

        //Guardamos las dimensiones
        foreach ($rubros as $k=>$rubro){
            $padres = $GLOBALS['toolbox']->traeRubrosPadres($rubros, $rubro['id']);
            foreach ($dimensiones as $y=>$dimension){
                if($dimension['rubro_id'] == $rubro['id'])
                    $rubros[$k]['dimensiones'][] = array("nombre"=> $dimension['nombre'], "propia"=>1);

                if(in_array($dimension['rubro_id'], $padres) && ($dimension['cede_decendencia'] == 'Y'))
                    $rubros[$k]['dimensiones'][] = array("nombre"=> $dimension['nombre'], "propia"=>0);
            }
        }

        

        //Recorremos en busqueda del item id 0
        //y lo transformamos en #
        foreach($rubros as $k=>$v){

            $rubros[$k]['parent'] = $rubros[$k]['padre'];
            $rubros[$k]['text'] = $rubros[$k]['nombre'];
            unset($rubros[$k]['nombre'], $rubros[$k]['padre']);

            //Para el root definimos padre '#'
            if($v['id'] == 1){
                $rubros[$k]['parent'] = '#';
                $rubros[$k]['state']['selected'] = true;
            }
        }

        //Guardamos los datos
        $GLOBALS['resultado']->_result = $rubros;
    }

    /**
     * @nombre: Crear un Rubro
     * @descripcion: Crear un Rubro
     */
    public function crear(){

        // Begin Transaction
        $GLOBALS['conf']['pdo']->beginTransaction();

        try {

            //Obtenemos última posición
            $stmt = $GLOBALS['conf']['pdo']->prepare("INSERT INTO catalogo_rubros (padre, nombre) VALUES (".$GLOBALS['parametros']['parent'].",'".$GLOBALS['parametros']['nombre']."')");
            $stmt->execute();
 
            //Obtenemos la id que se le asigno al nuevo rubro
            //$id = $GLOBALS['conf']['pdo']->lastInsertId();

            //echo $id;

            //Agregamos una nueva columna con el id nuevo
            //$stmt = $GLOBALS['conf']['pdo']->prepare("ALTER TABLE catalogo_articulos ADD extra_".$id." VARCHAR(150)");
            //$stmt->execute();

            // Commit Transaction
            $GLOBALS['conf']['pdo']->commit();

        } // Si existieron errores
        catch (PDOException $e) {
            // hacemos un Rollback
            $GLOBALS['conf']['pdo']->rollback();

            //Agregamos error 
            $GLOBALS['resultado']->setError($e);
            return;
        }
    }

    /**
     * @nombre: Renombra un Rubro
     * @descripcion: Renombra un Rubro
     */
    public function renombrar(){

        //DB query
        $stmt = $GLOBALS['conf']['pdo']->prepare("UPDATE catalogo_rubros SET nombre='".$GLOBALS['parametros']['nombre']."' WHERE id=".$GLOBALS['parametros']['rubro'] );
        $items = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $stmt->execute();
    }

    /**
     * @nombre: Elimina un Item del Menú
     * @descripcion: Elimina item del menu
     */
    public function eliminar(){

            //Conseguimos primero todos los descendientes
            $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT * FROM catalogo_rubros");
            $stmt->execute();
            $rubros = $stmt->fetchAll(PDO::FETCH_ASSOC);

            //Obtenemos todos los rubros que dependen del actual
            $rubros_eliminar = $GLOBALS['toolbox']->traeRubrosHijos($rubros,$GLOBALS['parametros']['rubro']);

            //Agregamos el actual
            array_push($rubros_eliminar, $GLOBALS['parametros']['rubro']);

            //Buscamos dimensiones asignadas a este rubro o a alguno de sus hijos
            $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT id FROM catalogo_rubros_datos WHERE rubro_id IN (".implode(',', $rubros_eliminar).")");
            $stmt->execute();
            $propiedades = $stmt->fetchAll(PDO::FETCH_COLUMN, 0);


        // Begin Transaction
        $GLOBALS['conf']['pdo']->beginTransaction();

        try {

            //Eliminamos las 
            //propiedades del rubro
            if(count($propiedades)){
                //Generamos arreglo con los nombres de las columnas
                foreach($propiedades as $k=>$p) $propiedades[$k] = 'extra_'.$p;

                //Obtenemos última posición
                $stmt = $GLOBALS['conf']['pdo']->prepare("ALTER TABLE catalogo_articulos DROP '".implode("','", $propiedades)."'");
                $stmt->execute();
            }

            //DB query
            $stmt = $GLOBALS['conf']['pdo']->prepare("DELETE FROM catalogo_rubros WHERE id IN (".implode(',' , $rubros_eliminar).")");
            $stmt->execute();

            // Commit Transaction
            $GLOBALS['conf']['pdo']->commit();

        } // Si existieron errores
        catch (PDOException $e) {
            // hacemos un Rollback
            $GLOBALS['conf']['pdo']->rollback();

            //Agregamos error 
            $GLOBALS['resultado']->setError($e);
            return;
        }
    }

    /**
     * @nombre: Lista los Rubros (Select2)
     * @descripcion: Lista los Rubros (Select2)
     */
    public function listar_rubros_s2(){

        //Calculate paging
        $page =  (intval($GLOBALS['parametros']['p'])-1) * 40;

        //EStamos buscando o solo listando?
        if($GLOBALS['parametros']['q'] == '') $busqueda = false;
        else $busqueda = true;

        //Make query
        $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT * FROM catalogo_rubros WHERE id <> 1 ORDER BY orden");
        $stmt->execute();
        $rubros =  $stmt->fetchAll(PDO::FETCH_ASSOC);

        //creamos el breadcrumb
        foreach($rubros as $k=>$r) 
            $rubros[$k]['bread'] = $GLOBALS['toolbox']->getBreadcrumbRubros($rubros,$r['id']);


        //Si estamos buscando...
        if($busqueda){
            //Score
            foreach($rubros as $k=>$v)
                $rubros[$k]['score'] = $GLOBALS['toolbox']->compare_strings($GLOBALS['parametros']['q'], $v['bread']);

            // revisamos que existan todos los 
            // padres de cada uno de los rubros
            foreach ($rubros as $k=>$v) {
                // Si sumo algo de score
                // Y tiene algun padre q
                // no sea el 1
                if($v['score'] > 0){
                    //ids de los progenitores
                    $padres =  $GLOBALS['toolbox']->traeRubrosPadres($rubros,$v['id']);
                    //print_r($padres);

                    foreach ($rubros as $x=>$r1)
                        if( in_array($r1['id'],$padres) ){
                            //echo "padre: ".$r1['id'];
                            if($r1['score'] < $v['score'])
                                $rubros[$x]['score'] = $v['score'];
                        }
                    }
            }

            //Eliminamos 0's
            foreach($rubros as $k=>$v)
                if($v['score'] == 0)
                    unset($rubros[$k]);

            //Ordenamos
            usort($rubros, function($a, $b) {
                //Si el score es el mismo
                //Desempatan con natural order
                if ($a['score'] == $b['score'])
                    return strnatcmp ( $a['bread'], $b['bread'] );

                //Sino por score
                return ($a['score'] < $b['score']) ? 1 : -1;
            });
        }

        //Borramos variables no usadas
        foreach($rubros as $k=>$v)
            unset(  $rubros[$k]['padre'],
                    $rubros[$k]['nombre_singular'],
                    $rubros[$k]['orden'],
                    $rubros[$k]['nombre']);

        //Calculamos totales
        $GLOBALS['resultado']->_result['total'] = count($rubros);

        //Devolvemos items y total
        $GLOBALS['resultado']->_result['items'] = array_slice($rubros, $page, 40);
    }

    /**
     * @nombre: Devuelve nombre Singular del Rubro
     * @descripcion: Devuelve nombre Singular del Rubro
     */
    public function info_nombre_singular(){


        $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT nombre_singular FROM catalogo_rubros WHERE id = ".$GLOBALS['parametros']['rubro']);
        $stmt->execute();
        $nombre_rubro = $stmt->fetchAll(PDO::FETCH_COLUMN, 0);

        //Devolvemos resultado
        $GLOBALS['resultado']->_result = $nombre_rubro[0];
    }

    /**
     * @nombre: Edita el nombre Singular de un Rubro
     * @descripcion: Edita el nombre Singular de un Rubro
     */
    public function edita_nombre_singular(){
        //Actualizamos la DB
        $stmt = $GLOBALS['conf']['pdo']->prepare("  UPDATE  catalogo_rubros 
                                                    SET     nombre_singular='".$GLOBALS['parametros']['rubro_nombre_singular']."' 
                                                    WHERE   id = ".$GLOBALS['parametros']['rubro']);
        $stmt->execute();
    }


    /**
     * @nombre: Devuelve nombres Alternativos del Rubro
     * @descripcion: Devuelve nombres Alternativos del Rubro
     */
    public function info_nombres_alternativos(){

        //Hacemos la consulta
        $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT nombres_alternativos FROM catalogo_rubros WHERE id = ".$GLOBALS['parametros']['rubro']);
        $stmt->execute();
        $nombre_rubro = $stmt->fetchAll(PDO::FETCH_COLUMN, 0);

        //Devolvemos resultado
        $GLOBALS['resultado']->_result = $nombre_rubro[0];
    }

    /**
     * @nombre: Edita los nombres Alternativos de un Rubro
     * @descripcion: Edita los nombres Alternativos de un Rubro
     */
    public function edita_nombres_alternativos(){
        //Actualizamos la DB
        $stmt = $GLOBALS['conf']['pdo']->prepare("  UPDATE  catalogo_rubros 
                                                    SET     nombres_alternativos='".$GLOBALS['parametros']['rubro_nombres_alternativos']."' 
                                                    WHERE   id = ".$GLOBALS['parametros']['rubro']);
        $stmt->execute();
    }
}
?>