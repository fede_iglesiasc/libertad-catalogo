<?php
/**
 * @nombre: Backup
 * @descripcion: Crea respaldos de la DB y las imágenes.
 */
class backup extends module{


    /*
	 * Constructor
	 */
    public function __construct(){

    }

    /**
     * @nombre: Crea backup
     * @descripcion: Genera backup completo del sistema.
     */
    public function backup_confirm(){

        //Quitamos el limite de tiempo
        set_time_limit(0);

        //Generamos backup
        $GLOBALS['toolbox']->backup();
    }

}
?>