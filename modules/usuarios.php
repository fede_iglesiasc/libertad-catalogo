<?php
/**
 * @nombre: Usuarios
 * @descripcion: Lista, Edita, Crea y Elimina Usuarios.
 */
class usuarios extends module{


    /**
	 * Constructor
	 */
    public function __construct(){

    }

    /**
     * Validaciones previas
     */
    public function __pre($accion){

        //Si existe el parametro usuario...
        if(isset($GLOBALS['parametros']['usuario']) && ($GLOBALS['parametros']['usuario'] != '')){
            $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT usuario FROM usuarios WHERE usuario = '".$GLOBALS['parametros']['usuario']."'");
            $stmt->execute();
            $usuario = $stmt->rowCount();
        }

        //Si existe el parametro email...
        if(isset($GLOBALS['parametros']['email']) && ($GLOBALS['parametros']['email'] != '')){
            $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT email FROM usuarios WHERE email = '".$GLOBALS['parametros']['email']."'");
            $stmt->execute();
            $email = $stmt->rowCount();
        }

        //Si no existe el usuario
        if( isset($usuario) && !$usuario && 
            in_array($accion, array('eliminar','editar','info','permisos_listar','permisos_editar'))){
            $GLOBALS['resultado']->setError("El Usuario no existe.");
            return;
        }

        //Si existe el usuario
        if( isset($usuario) && $usuario && 
            in_array($accion, array('agregar'))){
            $GLOBALS['resultado']->setError("El Usuario que intenta agregar ya existe.");
            return;
        }

        //Si existe el email
        if( isset($email) && $email && 
            in_array($accion, array('agregar'))){
            $GLOBALS['resultado']->setError("El email que intenta agregar ya existe.");
            return;
        }

        //llamamos a la accion
        return call_user_func_array(array($this, $accion), array());
    }

    /**
     * @nombre: Lista los Usuarios
     * @descripcion: Listado completo de usuarios y sus detalles.
     */
    public function listar(){

        //Obtenemos los usuarios
        $stmt = $GLOBALS['conf']['pdo']->prepare("  SELECT
                                                        usuario,
                                                        activo,
                                                        email,
                                                        nombre,
                                                        IF( (SELECT nombre FROM roles WHERE id = rol_id) IS NOT NULL, 
                                                            (SELECT nombre FROM roles WHERE id = rol_id),
                                                            'Permisos Manuales') as rol,
                                                        horario,
                                                        (SELECT nombre FROM horarios WHERE id = horario) as horario_label,
                                                        (   SELECT  COUNT(*) 
                                                            FROM    sessions 
                                                            WHERE   (latencia >= NOW() - INTERVAL 1 MINUTE) 
                                                                    AND 
                                                                    (usuario = usuarios.usuario)) as estado
                                                    FROM usuarios
                                                    WHERE   (rol_id <> 2) AND
                                                            (rol_id <> -1)
                                                    ORDER BY nombre ASC");
        $stmt->execute();
        $datos = $stmt->fetchAll(PDO::FETCH_ASSOC);

        //Horarios
        foreach ($datos as $k=>$v)
            if(is_null($v['horario'])){ 
                $datos[$k]['horario_label'] = "Sin Horario";
                $datos[$k]['horario'] = 0;
            }
        
        $GLOBALS['resultado']->_result = $datos;
    }

    /**
     * @nombre: Información de Usuario
     * @descripcion: Pasamos usuario y nos devuelve Email, nombre y activo.
     */
    public function info(){

        //Obtenemos los usuarios
        $stmt = $GLOBALS['conf']['pdo']->prepare("  SELECT
                                                        usuario,
                                                        activo,
                                                        email,
                                                        nombre,
                                                        rol_id,
                                                        (SELECT nombre FROM roles WHERE id = rol_id) as rol_label,
                                                        horario,
                                                        (SELECT nombre FROM horarios WHERE id = horario) as horario_label
                                                    FROM usuarios
                                                    WHERE usuario = '".$GLOBALS['parametros']['usuario']."'");
        $stmt->execute();
        $datos = $stmt->fetch(PDO::FETCH_ASSOC);

        //Horarios
        if(is_null($datos['horario'])){ 
            $datos['horario_label'] = "Sin Horario";
            $datos['horario'] = 0;
        }
        
        //Resultados
        $GLOBALS['resultado']->_result = $datos;
    }

    /**
     * @nombre: Edita un Usuario
     * @descripcion: Edita un Usuario, pasándole el nombre de Usuario.
     */
    public function editar(){

        //Password
        $password = $GLOBALS['parametros']['password'];
        if(($password != '') && (strlen($password) > 3)) $password = "password='".$password."',";
        else $password = '';

        if($GLOBALS['parametros']['horario'] == 0)
            $GLOBALS['parametros']['horario'] = 'NULL';

        //Obtenemos los usuarios
        $stmt = $GLOBALS['conf']['pdo']->prepare("  UPDATE usuarios 
                                                    SET 
                                                        ".$password."
                                                        email='".$GLOBALS['parametros']['email']."', 
                                                        rol_id=".$GLOBALS['parametros']['rol'].", 
                                                        activo=".$GLOBALS['parametros']['activo'].", 
                                                        nombre='".$GLOBALS['parametros']['nombre']."',
                                                        horario=".$GLOBALS['parametros']['horario']."
                                                    WHERE usuario='".$GLOBALS['parametros']['usuario']."'");
        $stmt->execute();
    }

    /**
     * @nombre: Agregar un Usuario
     * @descripcion: Agrega un Usuario, pasándole el nombre de Usuario.
     */
    public function agregar(){

        $stmt = $GLOBALS['conf']['pdo']->prepare("  INSERT INTO usuarios (usuario, password, email, activo, rol_id, nombre) 
                                                    VALUES (
                                                            '".$GLOBALS['parametros']['usuario']."',
                                                            '".$GLOBALS['parametros']['password']."',
                                                            '".$GLOBALS['parametros']['email']."',
                                                            ".$GLOBALS['parametros']['activo'].",
                                                            ".$GLOBALS['parametros']['rol'].",
                                                            '".$GLOBALS['parametros']['nombre']."'
                                                        )");
        $items = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $stmt->execute();
    }

    /**
     * @nombre: Eliminar un Usuario
     * @descripcion: Elimina un Usuario, pasándole el nombre de Usuario.
     */
    public function eliminar(){

        //Obtenemos los usuarios
        $stmt = $GLOBALS['conf']['pdo']->prepare("DELETE FROM usuarios WHERE usuario = '".$GLOBALS['parametros']['usuario']."'");
        $stmt->execute();
    }

    /**
     * @nombre: Listar Permisos
     * @descripcion: Lista los permisos heredados del Rol
     */
    public function permisos_listar(){

        $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT * FROM usuarios WHERE usuario = '".$GLOBALS['parametros']['usuario']."'");
        $stmt->execute();
        $usuario = $stmt->fetch(PDO::FETCH_ASSOC);

        //Obtenemos permisos
        $stmt = $GLOBALS['conf']['pdo']->prepare("  SELECT  id,
                                                            grupo,
                                                            accion,
                                                            (SELECT COUNT(*) FROM permisos_roles WHERE (funcion_id = id) AND (rol_id = ".$usuario['rol_id'].")) as acceso
                                                    FROM funciones
                                                    ORDER BY acceso DESC, grupo ASC, accion ASC");
        $stmt->execute();
        $permisos = $stmt->fetchAll(PDO::FETCH_ASSOC);

        //Tomaos todas las funciones con los permisos del rol del usuario
        //Si existe un permiso definido especificamente para el usuario lo
        //sobre escribimos
        $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT * FROM permisos_usuarios WHERE usuario_id = '".$GLOBALS['parametros']['usuario']."'");
        $stmt->execute();
        $permisos_usuario = $stmt->fetchAll(PDO::FETCH_ASSOC);

        //Nulleamos todo
        foreach($permisos as $x=>$v1) $permisos[$x]['acceso_manual'] = '-1';

        //Agreganos los permisos customizados a las funciones
        foreach($permisos_usuario as $k=>$v)
            foreach($permisos as $x=>$v1)
                if($v1['id'] == $v['funcion_id'])
                    $permisos[$x]['acceso_manual'] = $v['acceso'];


        //Asignamos resultados
        $GLOBALS['resultado']->_result = $permisos;
    }

    /**
     * @nombre: Editar Permisos
     * @descripcion: Edita los permisos del Usuario
     */
    public function permisos_editar(){


        //Creamos todos los permisos nuevos
        foreach($GLOBALS['parametros']['permisos'] as $k=>$v)
            if( !isset($v['id']) || !isset($v['acceso']) || 
                !( ((int)$v['acceso'] == 1) || ((int)$v['acceso'] == 0)) || 
                !( (int)$v['id'] > 0)){
                //Agregamos error 
                $GLOBALS['resultado']->setError("Error en los Permisos.");
                return;
            }


        // Begin Transaction
        $GLOBALS['conf']['pdo']->beginTransaction();

        try {

            //Borramos todos los permisos personalizados anteriormente...
            $stmt = $GLOBALS['conf']['pdo']->prepare("DELETE FROM permisos_usuarios WHERE usuario_id = '".$GLOBALS['parametros']['usuario']."'");
            $stmt->execute();

            //Agregamos los permisos
            if(count($GLOBALS['parametros']['permisos'])){

                //Generamos los inserts
                $sqlInsert = array();
                foreach($GLOBALS['parametros']['permisos'] as $k=>$v)
                    $sqlInsert[] = "(".$v['id'].",'".$GLOBALS['parametros']['usuario']."',".$v['acceso'].")";
                
                //Insertamos los permisos en la base de datos
                $stmt = $GLOBALS['conf']['pdo']->prepare("INSERT INTO permisos_usuarios(funcion_id, usuario_id, acceso) VALUES ".implode(', ', $sqlInsert));
                $stmt->execute();
            }

            // Commit Transaction
            $GLOBALS['conf']['pdo']->commit();

        } // Si existieron errores
        catch (PDOException $e) {
            // hacemos un Rollback
            $GLOBALS['conf']['pdo']->rollback();

            //Agregamos error 
            $GLOBALS['resultado']->setError($e);
            return;
        }


        //Habria que recrear todos los accesos en cada
        //una de las sessiones abiertas para mantener
        //a todos los usuarios logueados pero con los 
        //permisos nuevos.
    }

}
?>