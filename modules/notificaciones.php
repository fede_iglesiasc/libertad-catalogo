<?php
/**
 * @nombre: Notificaciones
 * @descripcion: Lista, edita, elimina Notificaciones y funciones afines.
 */
class notificaciones extends module{

    /*
	 * Constructor
	 */
    public function __construct(){
    }

    /**
     * Validaciones previas
     */
    public function __pre($accion){

        //Si existe el parametro notificacion...
        if(isset($GLOBALS['parametros']['notificacion'])){
            $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT id FROM notificaciones WHERE id = ".$GLOBALS['parametros']['notificacion']);
            $stmt->execute();
            $notificacion = $stmt->rowCount();
        }

        //Si no existe la notificacion
        if( isset($notificacion) && !$notificacion && 
            in_array($accion, array('readed'))){
            $GLOBALS['resultado']->setError("La Notificación no existe.");
            return;
        }

        //llamamos a la accion
        return call_user_func_array(array($this, $accion), array());
    }

    /**
     * @nombre: Checkea notificaciones
     * @descripcion: Checkea si existen notificaciones pendientes por mostrar
     */
    public function read(){

        //Obtenemos el usuario
        $usuario = $GLOBALS['session']->getData('usuario');
        $modo = $GLOBALS['session']->getData('modo');

        //Actualizamos la latencia de la Session
        $sql =  "   UPDATE  sessions SET latencia = NOW() 
                    WHERE   session_id = '".$GLOBALS['session']->getId()."';";

        //Obtenemos notificaciones
        $sql .= "   UPDATE  mensajes SET recibido = NOW() 
                    WHERE   receptor = '".$usuario."' AND recibido = 0;";

        //Obtenemos notificaciones
        $sql .= "   SELECT  id, titulo, mensaje 
                    FROM    notificaciones 
                    WHERE   id IN ( SELECT  notificacion_id 
                                    FROM    notificaciones_pendientes 
                                    WHERE   usuario_id = '".$usuario."');";

        //Obtenemos cambio de precios
        $sql .= "   SELECT  COUNT(DISTINCT id_marca, precio_update)
                    FROM    catalogo_articulos a
                    WHERE   (precio_update > (SELECT notificaciones_aumentos FROM usuarios WHERE usuario = '".$usuario."')) AND
                            (id_marca IS NOT NULL) AND
                            (precio_anterior > 0.000);";



        //Obtenemos cantidad de mensajes
        $sql .= "SELECT COUNT(*) as cantidad FROM mensajes WHERE (receptor = '".$usuario."') AND (leido = 0) GROUP BY emisor;";

        //Obtenemos MD5 de Usuarios Online
        if($modo == 'mensajes'){
            $sql .= "SELECT      usuario,
                            (SELECT COUNT(*) FROM horarios_vacaciones WHERE (inicio <= CURDATE()) AND (fin >= CURDATE()) AND (usuario = s.usuario)) as vacaciones,
                            (SELECT COUNT(*) FROM horarios_semanario WHERE (horario = (SELECT horario FROM usuarios WHERE usuario = s.usuario)) AND (dia = (WEEKDAY(CURDATE())+1)) AND (NOW() >= hora_inicio) AND (NOW() <= hora_fin)) as horario,
                            (SELECT horario FROM usuarios WHERE usuario = s.usuario) as horario_id,
                            (SELECT rol_id FROM usuarios WHERE usuario = s.usuario) as rol,
                            (   SELECT COUNT(*) 
                                FROM    horarios_feriados 
                                WHERE   (dia = CURDATE()) AND 
                                        (   (hora_inicio IS NULL) 
                                            OR (hora_fin IS NULL) 
                                            OR (    (NOW() >= hora_inicio) 
                                                    AND (NOW() <= hora_fin)))) as es_feriado
                FROM        sessions s
                WHERE       (latencia >= NOW() - INTERVAL 1 MINUTE) 
                            AND (usuario IS NOT NULL)
                ORDER BY    usuario ASC;";

                    //Obtenemos Timestamp ultimos mensajes
            $sql .= "   SELECT  UNIX_TIMESTAMP(MAX(actualizado)) AS actualizado
                        FROM    mensajes
                        WHERE   emisor = '".$usuario."' OR receptor = '".$usuario."';";
        }

        $stmt = $GLOBALS['conf']['pdo']->query($sql);
        $stmt->nextRowset();
        $stmt->nextRowset();
        //$GLOBALS['resultado']->_result =  $stmt->fetchAll(PDO::FETCH_ASSOC);
        $GLOBALS['resultado']->_result['carteles'] =  $stmt->fetchAll(PDO::FETCH_ASSOC);
        $stmt->nextRowset();
        $aumentos = (int)$stmt->fetchColumn();
        $stmt->nextRowset();
        $mensajes_no_leidos = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if($modo == 'mensajes'){
            $stmt->nextRowset();
            $contactos_update = $stmt->fetchAll(PDO::FETCH_ASSOC);

            //Quitamos usuarios de vacaciones
            foreach($contactos_update as $k=>$u)
                if( ((int)$u['rol'] <> 2) && (((int)$u['vacaciones']) || (!is_null($u['horario_id']) && !(int)$u['horario']) || (int)$u['es_feriado']) )
                    unset($contactos_update[$k]);

            $stmt->nextRowset();
            $mensajes_update = (int)$stmt->fetchColumn();
        }

        //Si el usuario es Administrador
        //chrqueamos si existen usuarios
        //pendientes de aprobación.
        $rol = $GLOBALS['session']->getData('rol');
        if(in_array($rol, array(1,-1))){
            $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT usuario FROM usuarios WHERE activo = -1");
            $stmt->execute();
            if($stmt->rowCount())
                $GLOBALS['resultado']->_result['carteles'][] = array( "id" => -1, "titulo"=>"Nuevos Clientes", "mensaje"=>"Nuevos Clientes se registraron y están esperando su aprobación." );
        }

        //Devolvemos Aumentos Nuevos
        $GLOBALS['resultado']->_result['listas'] = $aumentos;
        
        if($modo == 'mensajes'){

            //Timestamp Mensajes
            $GLOBALS['resultado']->_result['mensajes_conversaciones_version'] = $mensajes_update;
            
            //Checksum mensajes_contactos online
            $GLOBALS['resultado']->_result['mensajes_contactos_version'] = md5(serialize(array_column($contactos_update, 'usuario')));
        }
        
        //Devolvemos Mensajes Nuevos
        $GLOBALS['resultado']->_result['mensajes_no_leidos'] = count($mensajes_no_leidos);
    }

    /**
     * @nombre: Marca notificaciones como leida
     * @descripcion: No vuelve a mostrar la notificacion en el proximo check.
     */
    public function readed(){

        //Query DB
        $stmt = $GLOBALS['conf']['pdo']->query("DELETE FROM notificaciones_pendientes WHERE (usuario_id = '".$GLOBALS['session']->getData('usuario')."') AND (notificacion_id = ".$GLOBALS['parametros']['notificacion'].")");
        $stmt->execute();
    }
}
?>