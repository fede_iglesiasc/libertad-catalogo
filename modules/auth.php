<?php
/**
 * @nombre: Autentificacion
 * @descripcion: Login, logout a los usuarios y les da los permisos.
 */
class Auth extends module{
 
    public function __construct(){
    }

    /**
     * Validaciones previas
     */
    public function __pre($accion){

        //Si existe el parametro usuario...
        if(isset($GLOBALS['parametros']['usuario'])){
            $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT usuario FROM usuarios WHERE usuario = '".$GLOBALS['parametros']['usuario']."'");
            $stmt->execute();
            $usuario = $stmt->rowCount();
        }

        //Si existe el parametro email...
        if(isset($GLOBALS['parametros']['email'])){
            $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT email FROM usuarios WHERE email = '".$GLOBALS['parametros']['email']."'");
            $stmt->execute();
            $email = $stmt->rowCount();
        }

        //Si ya se encuentra identificado
        if( $GLOBALS['session']->getData('usuario') &&
            in_array($accion, array('login'))){
            $GLOBALS['resultado']->setError("Usted ya se encuentra identificado como ".$GLOBALS['session']->getData('usuario'));
            return;
        }

        //Si estamos recuperando la contraseña
        if( isset($email) && !$email &&
            in_array($accion, array('remPass'))){
            $GLOBALS['resultado']->setError("No existe ningún Usuario con ese Email.");
            return;
        }
        
        //llamamos a la accion
        return call_user_func_array(array($this, $accion), array());
    }

    /**
     * @nombre: Login
     * @descripcion: Crea una session
     */
    public function login(){

		//Obtenemos las notificaciones
		$stmt = $GLOBALS['conf']['pdo']->query("	SELECT 
		    												usuario,
		    												password,
		    												email,
		    												rol_id,
		    												IF( rol_id = 2,
		    													(SELECT nombre_del_comercio FROM clientes WHERE usuario = usuarios.usuario),
		    													nombre
		    												) as nombre

													FROM 	usuarios

													WHERE 	((email = '".$GLOBALS['parametros']['usuario']."') OR (usuario = '".$GLOBALS['parametros']['usuario']."')) 
															AND (activo = 1) ");
		$usuario =  $stmt->fetch(PDO::FETCH_ASSOC);


		$GLOBALS['resultado']->_result['usuario_param'] = $GLOBALS['parametros']['usuario'];

		if(!$stmt->rowCount()){
		    //Agregamos error 
		    $GLOBALS['resultado']->setError("El usuario o la contraseña son incorrectos.");
		    return;
		}

		if( $usuario['password'] != $GLOBALS['parametros']['password'] ){
		    //Agregamos error 
		    $GLOBALS['resultado']->setError("El usuario o la contraseña son incorrectos.");
		    return;
		}

			
		//Seteamos las variables de session
		$stmt = $GLOBALS['conf']['pdo']->query("UPDATE sessions SET usuario = '".$usuario['usuario']."' WHERE session_id='".$GLOBALS['session']->getId()."'");
		$GLOBALS['session']->setData('usuario',$usuario['usuario']);
		$GLOBALS['session']->setData('rol', (int)$usuario['rol_id']);
		$GLOBALS['session']->setData('nombre',$usuario['nombre']);
		$GLOBALS['resultado']->_result['usuario'] = $usuario['nombre'];
		$GLOBALS['resultado']->_result['profile'] = '';
		
    }

    /**
     * @nombre: Recuerda una contraseña
     * @descripcion: Envia un email recordando la contraseña
     */
	public function remPass(){

		//Clase de correos
		include_once('classes/class.mail.php');

		//Proteccion contra ataque
		if( ($GLOBALS['session']->getData('rem_pass_timestamp') && ( time() - $GLOBALS['session']->getData('rem_pass_timestamp') > $GLOBALS['conf']['rem_pass_limit'] )) 
			|| (!$GLOBALS['session']->getData('rem_pass_timestamp')) ){

			//Buscamos el usuario en la base de datos por EMAIL o #CLIENTE
			$stmt = $GLOBALS['conf']['pdo']->prepare("SELECT 	usuario, 
																email, 
																password, 
																IF( 	TRIM(usuarios.nombre) <> '', 
																		usuarios.nombre, 
																		(SELECT persona_de_contacto FROM clientes WHERE usuario = usuarios.usuario)  
																) as nombre 

														FROM usuarios

														WHERE (email = '".$GLOBALS['parametros']['email']."') AND (activo = 1)");
			$stmt->execute();

			//Guardamos los resultados
			$usuario = $stmt->fetch();


			$mail = new send_mail(true);

			try {

				$mail->Subject = 'Datos de acceso';
				 
				$mail->isHTML(true);

				//$mail->SMTPDebug  = 2; // Debugmode

				$mail->Body    = '<html><head></head><body text="#000000" bgcolor="#FFFFFF"><table width="100%"><tbody><tr><td align="right"><img alt="logo" style="float: right;" src="http://www.distribuidoralibertad.com/logo_distribuidora.jpg" height="54" width="200" align="right"></td></tr><tr><td>';

				$mail->Body   .= "<strong>".$usuario['nombre'].",</strong><br/> Tal como lo solicitó le enviamos los datos de acceso para ingresar a nuestro catálogo online: <br/><br/><br/><u>Usuario:</u> <b>".$usuario['email']."</b> (también puede usar su número de cliente <b>".$usuario['usuario']."</b>)<br/><u>Contraseña:</u> <b>".$usuario['password']."</b><br/><br/><br/>Si usted no solicitó que le enviémos información de acceso, por favor borre e ignore este correo.<br/><br/>";

				$mail->Body   .= "</td></tr><tr><td><hr><p style=\"font-size: 10px;\"><b style=\"font-size: 12px;\">Federico Iglesias Colombo<br>Distribuidora Libertad</b></p><p style=\"font-size: 10px;\">Mar del Plata. Buenos Aires. Argentina. <br>Chile 2019 (B7604-FHI)<br>TEL: (0223) 474-1222 / 474-1223 / 475-8352<br>FAX: (0223) 475- 7170 / 0800-333-6590<br></p></td></tr></tbody></table></body></html>";

				$mail->AltBody = $usuario['nombre'].", tal como lo solicitó le enviamos los datos de acceso para ingresar a nuestro catálogo online: \n \nUsuario: ".$usuario['email']." (también puede usar su número de cliente ".$usuario['email'].") \nContraseña: ".$usuario['password']." \n \n \n Si usted no solicitó que le enviémos información de acceso, por favor borre e ignore este correo.";
				 
				$mail->AddAddress($usuario['email']);

				$mail->Send();

			    $GLOBALS['resultado']->setResult("Hemos enviado un email con los datos de la cuenta a: <br/><b>".$usuario['email']."</b></br>Revisa tu correo en algunos minutos.</br>Recuerda revisar también la bandeja de 'Correo no deseado'");

			    //Seteamos protecciones contra bombardeo de correo
			    $GLOBALS['session']->setData('rem_pass_timestamp',time());
				$GLOBALS['session']->setData('rem_pass_email',$usuario['email']);

			} catch (phpmailerException $e) {
				//echo $e->errorMessage(); //Pretty error messages from PHPMailer
				$GLOBALS['resultado']->setError("Ocurrió un error al enviar el correo, por favor vuelva a intentarlo.");
                return;
			}


			$mail->ClearAddresses();
			$mail->ClearAttachments();
		}
	}

    /**
     * @nombre: Logout
     * @descripcion: Destruye la session
     */
	public function logout(){
	
		//Destruimos la session actual.
		$GLOBALS['session']->unsetData('usuario');
		$GLOBALS['session']->destroy();
	}
}
?>