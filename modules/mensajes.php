<?php
/**
 * @nombre: Mensajes
 * @descripcion: Administra panel de Mensajes
 */
class mensajes extends module{

    /* 
	 * Constructor
	 */
    public function __construct(){
    }

    /**
     * Validaciones previas
     */
    public function __pre($accion){

        //Si existe el parametro notificacion...
        if(isset($GLOBALS['parametros']['notificacion'])){
            $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT id FROM notificaciones WHERE id = ".$GLOBALS['parametros']['notificacion']);
            $stmt->execute();
            $notificacion = $stmt->rowCount();
        }

        //Si no existe la notificacion
        if( isset($notificacion) && !$notificacion && 
            in_array($accion, array('readed'))){
            $GLOBALS['resultado']->setError("La Notificación no existe.");
            return;
        }

        //llamamos a la accion
        return call_user_func_array(array($this, $accion), array());
    }

    /**
     * @nombre: Lista las conversaciones
     * @descripcion: Lista las conversaciones que el usuario tiene abiertas
     */
    public function conversaciones(){

        //Obtenemos el usuario
        $usuario = $GLOBALS['session']->getData('usuario');
        $rol = $GLOBALS['session']->getData('rol');

        //Roles que trae segun rol propio
        $rol_where = "-1,1,2,4,5";
        if($rol == 2) $rol_where = "-1,1,4";

        //Obtenemos todos los usuarios (menos el propio)
        $sql = "SELECT  usuario,
                        rol_id as rol,

                        if( rol_id <> 2,
                            nombre,
                            (SELECT CONCAT(usuario,' - ',razon_social) as nombre FROM clientes WHERE usuario = u.usuario) 
                        )as nombre,
                        
                        (   SELECT  COUNT(*) 
                            FROM    mensajes 
                            WHERE   (emisor=u.usuario AND receptor='".$usuario."') 
                                    AND leido = 0
                        ) as no_leidos,
                        
                        (   SELECT  COUNT(*) 
                            FROM    conversaciones 
                            WHERE   (usuario_a = '".$usuario."' AND usuario_b = u.usuario)
                        ) as estado,

                        (   SELECT  COUNT(*)
                            FROM    permisos_roles 
                            WHERE   rol_id = u.rol_id AND 
                                    funcion_id = 75
                        ) as rol_habilitado,

                        (   SELECT  acceso 
                            FROM    permisos_usuarios 
                            WHERE   usuario_id = u.usuario AND 
                                    funcion_id = 75
                        ) as usuario_habilitado

                FROM    usuarios u
                WHERE   (   rol_id IN (".$rol_where.") AND 
                            (activo = 1) AND 
                            (usuario <> '".$usuario."'))";

        //Agregamos el vendedor/cobrador del cliente
        if((int)$rol == 2) $sql .= " OR (u.usuario = (SELECT vendedor FROM clientes WHERE usuario = '".$usuario."'));";

        //Lista de Usuarios
        $stmt = $GLOBALS['conf']['pdo']->query($sql);
        $query_usuarios =  $stmt->fetchAll(PDO::FETCH_ASSOC);

        //Usamos usuario como key del arreglo
        foreach($query_usuarios as $k=>$v){
            $v['msg'] = array("version"=>0,"enviado"=>0);
            $v['rol'] = (int)$v['rol'];
            $v['no_leidos'] = (int)$v['no_leidos'];
            $v['estado'] = (int)$v['estado'];
            
            //Permiso para chatear

            $permiso = (int)$v['rol_habilitado'];
            if(!is_null($v['usuario_habilitado'])) 
                $permiso = (int)$v['usuario_habilitado'];
            
            if($v['rol'] == 2) $permiso = 1;

            //Quitamos info inecesaria
            unset($v['rol_habilitado'],$v['usuario_habilitado']);

            //Avatar
            $v['avatar'] = '';
            $avatars = array_values(preg_grep('~^'.$v['usuario'].'-.*\.(jpg)$~', scandir("avatar/")));
            if(count($avatars)) $v['avatar'] = './avatar/'.$avatars[0];


            //Agregamos usuario
            if($permiso || ($v['rol'] == -1)) $usuarios[$v['usuario']] = $v;
        }

        //Consultamos los ultimos mensajes
        $sql_condicional = array();
        foreach ($usuarios as $k=>$v)
            $sql_condicional[] = "((emisor='".$usuario."' AND receptor='".$v['usuario']."') OR (emisor='".$v['usuario']."' AND receptor='".$usuario."'))";

        $sql = "    SELECT  id,
                            mensaje,
                            tipo,
                            IF(emisor = '".$usuario."', receptor, emisor) as interlocutor,
                            IF(emisor = '".$usuario."', 1, 0) as propio,
                            UNIX_TIMESTAMP(actualizado) as version,
                            UNIX_TIMESTAMP(enviado) as enviado,
                            UNIX_TIMESTAMP(recibido) as recibido,
                            DATE_FORMAT(enviado,'%H:%i') as hora,
                            DATE_FORMAT(enviado,'%e/%m/%Y') as fecha,
                            UNIX_TIMESTAMP(leido) as leido
                    FROM    mensajes 
                    WHERE   id IN ( SELECT  id 
                                    FROM    (   SELECT  max(id) as id, 
                                                        IF( emisor = '".$usuario."', 
                                                            receptor, 
                                                            emisor
                                                        ) as interlocutor 
                                                FROM    mensajes 
                                                WHERE   ".implode(' OR ', $sql_condicional)."
                                                GROUP BY interlocutor
                                    ) t
                            )
                    ORDER BY actualizado";

        $stmt = $GLOBALS['conf']['pdo']->query($sql);
        $mensajes =  $stmt->fetchAll(PDO::FETCH_ASSOC);

        //Agregamos el mensaje a las charlas
        foreach($mensajes as $k=>$v){
            $v['id'] = (int)$v['id'];
            $v['propio'] = (int)$v['propio'];
            $v['enviado'] = (int)$v['enviado'];
            $v['recibido'] = (int)$v['recibido'];
            $v['leido'] = (int)$v['leido'];
            $v['version'] = (int)$v['version'];

            $label = date("j/n/y", (int)$v['enviado']);
            $numero_dia = date('N', (int)$v['enviado']);

            //Si paso menos de una semana del mensaje
            if((int)$v['enviado'] > strtotime("-1 week"))
                if($numero_dia == 1) $label = "Lunes";
                elseif($numero_dia == 2) $label = "Martes";
                elseif($numero_dia == 3) $label = "Miercoles";
                elseif($numero_dia == 4) $label = "Jueves";
                elseif($numero_dia == 5) $label = "Viernes";
                elseif($numero_dia == 6) $label = "Sabado";
                elseif($numero_dia == 7) $label = "Domingo";

            //Si la fecha es HOY o Ayer
            if(date("j/n/Y") == $v['fecha']) $label = $v['hora'];
            elseif(date("j/n/Y",time() - 60 * 60 * 24) == $v['fecha']) $label = "Ayer";

            //Agregamos el Label
            $v['fecha_label'] = $label;

            //Agregamos el mensaje al Usuario
            if(isset($usuarios[$v['interlocutor']]))
                $usuarios[$v['interlocutor']]['msg'] = $v;
        }

        //Ordenamos por fecha de actualizacion
        usort($usuarios, function($a,$b){

            //Si las dos conversaciones estan cerradas
            if(($a['estado'] == '0') && ($b['estado'] == '0'))
                    return $b['msg']['enviado'] - $a['msg']['enviado'];

            //Si A esta abierta y B no
            elseif(($a['estado'] == '1') && ($b['estado'] == '0')) return -1;

            //Si A esta cerrada y B no
            elseif(($a['estado'] == '0') && ($b['estado'] == '1')) return 1;

            //Si las dos estan abiertas
            elseif(($a['estado'] == '1') && ($b['estado'] == '1')) 
                return  $b['msg']['enviado'] - $a['msg']['enviado'];

            else 0;
        });

        //Resultados
        $GLOBALS['resultado']->_result = $usuarios;
    }

    /**
     * @nombre: Lista la conversacion con un usuario
     * @descripcion: Lista la conversacion con un usuario
     */
    public function conversacion(){
        $usuario = $GLOBALS['session']->getData('usuario');
        $con = $GLOBALS['parametros']['usuario'];

        $sql = "    SELECT      id,
                                emisor,
                                UNIX_TIMESTAMP(recibido) as recibido,
                                UNIX_TIMESTAMP(leido) as leido,
                                mensaje,
                                tipo,
                                enviado,
                                DATE_FORMAT(enviado,'%H:%i') as hora,
                                DATE_FORMAT(enviado, '%d-%m-%Y') as dia
                    FROM        mensajes 
                    WHERE       (emisor = '".$usuario."' AND receptor = '".$con."')
                                OR
                                (emisor = '".$con."' AND receptor = '".$usuario."')
                    ORDER BY    enviado;";

        $sql .= "   SELECT  IF( rol_id <> 2,
                                nombre,
                                (SELECT persona_de_contacto FROM clientes WHERE usuario = usuario)) as nombre
                    FROM    usuarios
                    WHERE   usuario = '".$con."';";


        $sql .= "   SELECT  UNIX_TIMESTAMP( MAX( mensajes.actualizado ) ) as actualizado
                    FROM    mensajes
                    WHERE   (emisor = '".$usuario."' AND receptor = '".$con."')
                            OR
                            (emisor = '".$con."' AND receptor = '".$usuario."');";


        $stmt = $GLOBALS['conf']['pdo']->query($sql);
        $conversacion =  $stmt->fetchAll(PDO::FETCH_ASSOC);
        $stmt->nextRowset();
        $contacto = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $GLOBALS['resultado']->_result['contacto'] = $contacto[0]['nombre'];
        $GLOBALS['resultado']->_result['usuario'] = $con;

        $stmt->nextRowset();
        $update = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $GLOBALS['resultado']->_result['actualizado'] = $update[0]['actualizado'];

        //Correcciones a los resultados
        $conv_final = array();
        $count_noleidos = 0;
        foreach($conversacion as $k=>$v){

            //Agregamos fecha?
            $agregar_fecha = false;

            //Agregamos fecha a la conversacion
            if( ($k == 0) ||  
                (isset($conversacion[$k+1]) && 
                ($conversacion[$k]['dia'] != $conversacion[$k+1]['dia']))){

                //Pasamos la fecha a unix
                if(!$k) $time = strtotime($v['dia']); 
                else $time = strtotime($conversacion[$k+1]['dia']);

                //Damos formato
                $fecha = date("d/m/Y", $time);

                //Si la semana es la misma
                if(date('W',$time) ==  date('W')){
                    $dia = date('N',$time);
                    if($dia == 1) $fecha_label = 'Lunes';
                    if($dia == 2) $fecha_label = 'Martes';
                    if($dia == 3) $fecha_label = 'Miercoles';
                    if($dia == 4) $fecha_label = 'Jueves';
                    if($dia == 5) $fecha_label = 'Viernes';
                    if($dia == 6) $fecha_label = 'Sabado';
                    if($dia == 7) $fecha_label = 'Domingo';
                }

                //Ayer
                if($fecha == date('d/m/Y', (time() - (24 * 60 * 60)))) 
                    $fecha_label = 'Ayer';
                
                //Hoy
                if($fecha == date('d/m/Y')) 
                    $fecha_label = 'Hoy';

                //Pasamos a fecha
                if(isset($fecha_label)) $fecha = $fecha_label;

                
                if($k) $agregar_fecha = true;
                else $conv_final[] = array(  "emisor"=>"sistema",
                                                "tipo"=>"fecha",
                                                "dia"=>$fecha,
                                                "leido"=>0,
                                                "recibido"=>0);
            }

            //tira de no leido
            if(!$v['leido'] && ($v['emisor'] != $usuario) && !$count_noleidos){

                //Contamos los no leidos
                foreach ($conversacion as $x=>$v1)
                    if(!(int)$v1['leido'] && ($v1['emisor'] != $usuario)) 
                        $count_noleidos++;

                //Agregamos cartel
                $conv_final[] = array(  "emisor"=>"sistema",
                                        "tipo"=>"nuevo_mensaje",
                                        "leido"=>'',
                                        "recibido"=>0,
                                        "mensaje"=> $count_noleidos . ' Mensaje' . ( $count_noleidos > 1 ? 's' : '') . ' sin leer');
            }

            //Mensajes continuos
            $v['continua'] = 0;
            if( isset($conversacion[$k+1]) &&
                ($v['emisor'] == $conversacion[$k+1]['emisor']) &&
                ($conversacion[$k]['dia'] == $conversacion[$k+1]['dia']))
                    $v['continua'] = 1;

            //Mensaje propio o ajeno
            $v['propio'] = 1;
            if($v['emisor'] == $con) $v['propio'] = 0;

            //Estado
            $conversacion[$k]['estado'] = 0;
            if(!(int)$v['recibido'] && !(int)$v['leido']) $v['estado'] = 1;
            if((int)$v['recibido'] && !(int)$v['leido']) $v['estado'] = 2;
            if((int)$v['recibido'] && (int)$v['leido']) $v['estado'] = 3;

            //Quitamos variables que no usamos
            unset($v['leido'], $v['recibido']);

            //Agregamos mensaje a la conversacion
            $conv_final[] = $v;

            //Agregamos la fecha
            if($agregar_fecha) $conv_final[] = array(   "emisor"=>"sistema",
                                                        "tipo"=>"fecha",
                                                        "dia"=>$fecha,
                                                        "leido"=>'',
                                                        "recibido"=>0);

        }

        //Marcamos los mensajes como leidos hasta ahora
        $sql = "    UPDATE  mensajes 
                    SET     leido = NOW(),
                            recibido = NOW() 
                    WHERE   (emisor = '".$con."') 
                            AND (receptor = '".$usuario."')
                            AND (leido = 0)";
        $stmt = $GLOBALS['conf']['pdo']->query($sql);

        //Devolvemos los resultados
        $GLOBALS['resultado']->_result['conversacion'] = $conv_final;
    }

    /**
     * @nombre: Envia un Mensaje a un Usuario
     * @descripcion: Envia un Mensaje a un Usuario
     */
    public function enviar(){
        //Tomaos usuario actual
        $usuario = $GLOBALS['session']->getData('usuario');
        //Su rol
        $rol = $GLOBALS['session']->getData('rol');
        //Tomamos destinatario
        $destinatario = $GLOBALS['parametros']['usuario'];
        //Tomamos destinatario
        $id = $GLOBALS['parametros']['id'];
        //timestamp Cliente
        $ts_cliente = $GLOBALS['parametros']['version'];
        //Email?
        $email = $GLOBALS['parametros']['email'];
        //Mensaje
        $mensaje = $GLOBALS['parametros']['mensaje'];


        //Detectamos tipo de mensaje
        $tipo = 'chat';
        $adjunto_peso = '';
        $adjunto_extension = '';
        $adjunto_duracion = '';
        if(isset($GLOBALS['parametros']['adjunto']['name'])){
            
            $GLOBALS['resultado']->_result['prueba'] = $GLOBALS['parametros']['adjunto'];

            //Guardamos extension
            $path_info = pathinfo($GLOBALS['parametros']['adjunto']['name']);
            
            $adjunto_anterior = $GLOBALS['parametros']['adjunto']['nuevo_directorio'];
            $adjunto_anterior .= $GLOBALS['parametros']['adjunto']['name'];
            $adjunto_nuevo_ruta = $GLOBALS['parametros']['adjunto']['nuevo_directorio'];
            $adjunto_nuevo_ext = strtolower($path_info['extension']);
            $adjunto_nuevo_archivo = $path_info['filename'];
            $adjunto_nuevo_nombre = $adjunto_nuevo_archivo . '.' . $adjunto_nuevo_ext;

            //Cambiamos nombre de extension por lowercase
            rename($adjunto_anterior , $adjunto_nuevo_ruta . $adjunto_nuevo_nombre);

            //Extension del ardjunto
            $adjunto_extension = $adjunto_nuevo_ext; 

            //El mensaje es el nombre original
            $mensaje = $adjunto_nuevo_nombre;

            //Extraemos el MIME
            $fp = fopen('./upload_tmp/'.$adjunto_nuevo_nombre, 'rb');
            $adjunto_mime = finfo_open(FILEINFO_MIME_TYPE);
            fclose($fp);

            //Detectamos tipo de archivo
            if(in_array($adjunto_nuevo_ext, array("jpg", "png", "gif")))
                $tipo = 'image';
            if(in_array($adjunto_nuevo_ext, array("xls","xlsx","doc","docx","pdf","ppt","pptx","txt")))
                $tipo = 'document';
            if(in_array($adjunto_nuevo_ext, array("mp4")))
                $tipo = 'video';
            if(in_array($adjunto_nuevo_ext, array("mp3")))
                $tipo = 'ptt';
        
        }


        if($GLOBALS['parametros']['captura'] == 'foto') { $tipo = 'image'; $mensaje = 'captura.png'; }
        if($GLOBALS['parametros']['captura'] == 'audio') $tipo = 'ptt';
        if($GLOBALS['parametros']['captura'] == 'video') $tipo = 'video';


        //Existe alguna sesion abierta y activa actualmente?
        $sql = "SELECT  COUNT(*) as cantidad,
                        (SELECT email FROM usuarios WHERE usuario = '".$destinatario."') as email,
                        IF( (SELECT email FROM usuarios WHERE usuario = '".$usuario."') <> 2, 
                            (SELECT nombre FROM usuarios WHERE usuario = '".$usuario."'),
                            (SELECT persona_de_contacto FROM clientes WHERE usuario = '".$usuario."')) as nombre
                FROM    sessions 
                WHERE   (usuario = '".$destinatario."') AND
                        (latencia >= NOW() - INTERVAL 1 MINUTE);";

                //Existe alguna sesion abierta y activa actualmente?
        $sql .= "SELECT  COUNT(*) as conversacion
                FROM    conversaciones
                WHERE   (usuario_a = '".$usuario."' AND usuario_b = '".$destinatario."');";

        //Timestamp antes de agregar el mensaje
        $sql .= "SELECT  UNIX_TIMESTAMP( MAX( mensajes.actualizado ) )
                 FROM    mensajes
                 WHERE   (emisor = '".$usuario."' AND receptor = '".$destinatario."')
                          OR
                         (emisor = '".$destinatario."' AND receptor = '".$usuario."');";

        $sql .= "SELECT     (   SELECT  COUNT(*) 
                                FROM    permisos_roles 
                                WHERE   (rol_id = ".$rol.") AND (funcion_id=77)) as permiso_rol,
                            (   SELECT  acceso 
                                FROM    permisos_usuarios 
                                WHERE   (funcion_id=77) AND 
                                        (usuario_id='".$usuario."')) as permiso_usuario;";

        $stmt = $GLOBALS['conf']['pdo']->query($sql);
        $session =  $stmt->fetchAll(PDO::FETCH_ASSOC);
        $stmt->nextRowset();
        $conversacion_abierta = (int)$stmt->fetchColumn();
        $stmt->nextRowset();
        $timestamp = $stmt->fetchColumn();
        $stmt->nextRowset();
        $permisos =  $stmt->fetchAll(PDO::FETCH_ASSOC);

        //Tenemos permisos para poder mandar emails?
        $permiso = (int)$permisos[0]['permiso_rol'];
        if(!is_null($permisos[0]['permiso_usuario']))
            $permiso = (int)$permisos[0]['permiso_usuario'];
        if(!$permiso && ($rol != -1)) $email = 0;


        $recibido = 0;
        if((int)$session[0]['cantidad'])
            $recibido = 'NOW()';

        //Agregamos el mensaje
        if(!$conversacion_abierta)
          $stmt = $GLOBALS['conf']['pdo']->query("  INSERT IGNORE INTO      conversaciones (usuario_a,usuario_b) 
                                                    VALUES                  ('".$usuario."','".$destinatario."'), 
                                                                            ('".$destinatario."','".$usuario."')");
                                                      
        $stmt = $GLOBALS['conf']['pdo']->query("    INSERT INTO     mensajes (emisor,receptor,enviado,recibido,mensaje,tipo) 
                                                    VALUES          ('".$usuario."','".$destinatario."',NOW(),".$recibido.",'".$mensaje."','".$tipo."')");
        $id_ = $GLOBALS['conf']['pdo']->lastInsertId();

        $stmt = $GLOBALS['conf']['pdo']->query("    SELECT  id,
                                                            emisor,
                                                            IF(emisor = '".$usuario."', receptor, emisor) as interlocutor,
                                                            UNIX_TIMESTAMP(enviado) as enviado,
                                                            UNIX_TIMESTAMP(recibido) as recibido,
                                                            UNIX_TIMESTAMP(leido) as leido,
                                                            UNIX_TIMESTAMP(actualizado) as actualizado,
                                                            UNIX_TIMESTAMP(actualizado) as version,
                                                            DATE_FORMAT(enviado,'%H:%i') as hora,
                                                            mensaje,
                                                            tipo
                                                    FROM    mensajes 
                                                    WHERE   id = ".$id_);
        $insertado =  $stmt->fetchAll(PDO::FETCH_ASSOC);
        $insertado = $insertado[0];
        
        //Si es un adjunto lo movemos a la carpeta de adjuntos
        if((($tipo == 'image') || ($tipo == 'document') || ($tipo == 'video') || ($tipo == 'ptt')) && isset($GLOBALS['parametros']['adjunto']['name'])){
            //Generamos la nueva ruta del adjunto
            $adjunto_nueva_ruta = './mensajes/'.$id_.'.'.$adjunto_nuevo_ext;

            //Movemos el archivo a la carpeta
            rename('./upload_tmp/'.$adjunto_nuevo_nombre, $adjunto_nueva_ruta);

            //Calculamos el tamaño
            $adjunto_peso = $GLOBALS['toolbox']->formatSizeUnits(filesize($adjunto_nueva_ruta));
        }

        //Si nos estan enviando una captura
        if($GLOBALS['parametros']['captura'] == 'foto'){
            $img = $GLOBALS['parametros']['captura_contenido'];
            $img = str_replace('data:image/png;base64,', '', $img);
            $img = str_replace(' ', '+', $img);
            $data = base64_decode($img);
            $success = file_put_contents('./mensajes/'.$id_.'.png', $data);
            $adjunto_nueva_ruta = './mensajes/'.$id_.'.png';
            $adjunto_extension = 'png';
        }

        //Si es una imagen creamos thumb y redimensionamos
        if($tipo == 'image'){
            //Obtenemos la dimension
            list($width, $height) = getimagesize($adjunto_nueva_ruta);
            
            //Si es muy grande redimensionamos
            if(($width > 1200) || ($height > 1200))
                $GLOBALS['toolbox']->resizeImage($adjunto_nueva_ruta, 1200, 1200, $adjunto_nueva_ruta);
            
            //Creamos thumb
            $GLOBALS['toolbox']->resizeImage($adjunto_nueva_ruta, 330, 330, './mensajes/'.$id_.'_thumb.'.$adjunto_extension);
        }


        //Agregamos Fecha y Label de Fecha
        $insertado['fecha'] = date("j/n/y",(int)$insertado['enviado']);
        $label = date("j/n/y", (int)$insertado['enviado']);
        $numero_dia = date('N', (int)$insertado['enviado']);

        //Si paso menos de una semana del mensaje
        if((int)$insertado['enviado'] > strtotime("-1 week"))
            if($numero_dia == 1) $label = "Lunes";
            elseif($numero_dia == 2) $label = "Martes";
            elseif($numero_dia == 3) $label = "Miercoles";
            elseif($numero_dia == 4) $label = "Jueves";
            elseif($numero_dia == 5) $label = "Viernes";
            elseif($numero_dia == 6) $label = "Sabado";
            elseif($numero_dia == 7) $label = "Domingo";

        //Si la fecha es HOY o Ayer
        if(date("j/n/y") == $insertado['fecha']) $label = "Hoy";
        elseif(date("j/n/y",time() - 60 * 60 * 24) == $insertado['fecha']) $label = "Ayer";

        //Agregamos el Label
        $insertado['fecha_label'] = $label;
        $insertado['fecha_label_conversaciones'] = $label;
        if($label == 'Hoy') $insertado['fecha_label_conversaciones'] = $insertado['hora'];

        if((int)$email){
            //Incluimos las librerias
            include_once('classes/class.phpmailer.php');


            //Reintentamos hasta
            //que se envia 2379
            $reintentar = 0;

            do {

                //Creamos el correo
                $mail = new phpmailer();
                $mail->isSMTP();
                $mail->Host = 'mail.distribuidoralibertad.com';
                $mail->Port = '587';
                $mail->SMTPAuth = true;
                $mail->Username = 'pedidos@distribuidoralibertad.com';
                $mail->Password = '2019Chile';
                $mail->Mailer = 'smtp';
                $mail->From = 'pedidos@distribuidoralibertad.com';
                $mail->Sender = 'pedidos@distribuidoralibertad.com';
                $mail->SetFrom('pedidos@distribuidoralibertad.com','Distribuidora Libertad');
                $mail->Priority = 3;
                $mail->CharSet = 'UTF-8';

                try {

                    if($tipo == 'chat') $mensaje_mail = $mensaje;
                    else $mensaje_mail = "Le enviamos en este correo un adjunto.";

                    $mail->Subject = 'Mensaje de Distribuidora Libertad';
                    $mail->isHTML(true);
                    $mail->Body    = '<html><head></head><body text="#000000" bgcolor="#FFFFFF" style="font-family: sans-serif;"><table width="100%"><tbody><tr><td align="right"><img alt="logo" style="float: right;" src="http://www.distribuidoralibertad.com/img/logo.png" height="54" width="200" align="right"></td></tr><tr><td>';
                    $mail->Body   .= "<strong>Estimado Cliente: </strong><br/> ".$mensaje_mail. "<br/><br/>Saludos Cordiales.";
                    $mail->Body   .= '</td></tr><tr><td><hr><p style="font-size: 10px;"><b style="font-size: 12px;">'.$session[0]['nombre'].'<br>Distribuidora Libertad</b></p><p style="font-size: 10px;">Mar del Plata. Buenos Aires. Argentina. <br>Chile 2019 (B7604-FHI)<br>TEL: (0223) 474-1222 (ROTATIVAS)<br>FAX: (0223) 475- 7170 / 0800-333-6590<br></p></td></tr></tbody></table></body></html>';

                    $mail->AddAddress($session[0]['email']);

                    //Agregamos los adjuntos
                    if(($tipo == 'image') || ($tipo == 'document') || ($tipo == 'video') || ($tipo == 'ptt'))
                        $mail->AddAttachment($adjunto_nueva_ruta, $mensaje, 'base64', $adjunto_mime);

                    //Enviamos
                    $mail->Send();

                } catch (phpmailerException $e) {
                    sleep(2);
                    $reintentar = 1;
                }

            } while ($reintentar);
        }

        //Timestamp antes
        $GLOBALS['resultado']->_result['email'] = $email;
        $GLOBALS['resultado']->_result['rol'] = $rol;
        $GLOBALS['resultado']->_result['timestamp'] = $timestamp;
        $GLOBALS['resultado']->_result['id_tmp'] = (int)$id;
        $GLOBALS['resultado']->_result['id'] = (int)$insertado['id'];
        $GLOBALS['resultado']->_result['hora'] = $insertado['hora'];
        $GLOBALS['resultado']->_result['enviado'] = (int)$insertado['enviado'];
        $GLOBALS['resultado']->_result['recibido'] = (int)$insertado['recibido'];
        $GLOBALS['resultado']->_result['leido'] = (int)$insertado['leido'];
        $GLOBALS['resultado']->_result['actualizado'] = (int)$insertado['actualizado'];
        $GLOBALS['resultado']->_result['version'] = (int)$insertado['version'];
        $GLOBALS['resultado']->_result['mensaje'] = $insertado['mensaje'];
        $GLOBALS['resultado']->_result['tipo'] = $insertado['tipo'];
        $GLOBALS['resultado']->_result['peso'] = $adjunto_peso;
        $GLOBALS['resultado']->_result['extension'] = $adjunto_extension;
        $GLOBALS['resultado']->_result['emisor'] = $insertado['emisor'];
        $GLOBALS['resultado']->_result['interlocutor'] = $insertado['interlocutor'];
        $GLOBALS['resultado']->_result['modo'] = 'actualiza';
        $GLOBALS['resultado']->_result['propio'] = true;
        $GLOBALS['resultado']->_result['fecha'] = $insertado['fecha'];
        $GLOBALS['resultado']->_result['fecha_label'] = $insertado['fecha_label'];
    }

    /**
     * @nombre: Obtiene lista de usuarios conectados
     * @descripcion: Obtiene lista de usuarios conectados
     */
    public function usuarios_online(){

        //Su rol
        $rol = $GLOBALS['session']->getData('rol');

        $rol_where = "";
        if($rol == 2) $rol_where = " AND (2 <> (SELECT rol_id FROM usuarios WHERE usuario = s.usuario))"; 

        //Actualizamos la latencia de la Session
        $sql = "SELECT      usuario,
                            (SELECT COUNT(*) FROM horarios_vacaciones WHERE (inicio <= CURDATE()) AND (fin >= CURDATE()) AND (usuario = s.usuario)) as vacaciones,
                            (SELECT COUNT(*) FROM horarios_semanario WHERE (horario = (SELECT horario FROM usuarios WHERE usuario = s.usuario)) AND (dia = (WEEKDAY(CURDATE())+1)) AND (NOW() >= hora_inicio) AND (NOW() <= hora_fin)) as horario,
                            (SELECT horario FROM usuarios WHERE usuario = s.usuario) as horario_id,
                            (SELECT rol_id FROM usuarios WHERE usuario = s.usuario) as rol,
                            (   SELECT COUNT(*) 
                                FROM    horarios_feriados 
                                WHERE   (dia = CURDATE()) AND 
                                        (   (hora_inicio IS NULL) 
                                            OR (hora_fin IS NULL) 
                                            OR (    (NOW() >= hora_inicio) 
                                                    AND (NOW() <= hora_fin)))) as es_feriado
                FROM        sessions s
                WHERE       (latencia >= NOW() - INTERVAL 1 MINUTE) 
                            AND (usuario IS NOT NULL) ".$rol_where."
                ORDER BY    usuario ASC";
        
        $stmt = $GLOBALS['conf']['pdo']->query($sql);
        $usuarios = $stmt->fetchAll(PDO::FETCH_ASSOC);

        //Quitamos usuarios de vacaciones
        foreach($usuarios as $k=>$u)
            if( ((int)$u['rol'] <> 2) && (((int)$u['vacaciones']) || (!is_null($u['horario_id']) && !(int)$u['horario']) || (int)$u['es_feriado']) )
                unset($usuarios[$k]);

        //Obtenemos usuarios en linea
        $usuarios_online = array_column($usuarios, 'usuario');
        
        //Conversacion activa
        $interlocutor = $GLOBALS['parametros']['usuario'];
        
        //Si el usuario esta en linea evitamos todo esto
        if(!in_array($interlocutor, $usuarios_online) && ($interlocutor != '')){
            $estado = null;
            $nombre = '';
            $stmt = $GLOBALS['conf']['pdo']->query("SELECT usuario, rol_id, nombre FROM usuarios WHERE usuario='".$interlocutor."'");
            $usuario = $stmt->fetchAll(PDO::FETCH_ASSOC);
            if(count($usuario) && ((int)$usuario[0]['rol_id'] != 2)){
                $GLOBALS['resultado']->_result['proxima_conexion'] = $GLOBALS['toolbox']->calculaSiguienteHorario($interlocutor);
                $GLOBALS['resultado']->_result['nombre'] = $usuario[0]['nombre'];
            }
        }

        $GLOBALS['resultado']->_result['online'] = array_column($usuarios, 'usuario');
    }
    
    /**
     * @nombre: Obtiene mensajes actualizados de una charla
     * @descripcion: Obtiene mensajes actualizados de una charla
     */
    public function conversacion_update(){

        //Tomaos usuario actual
        $usuario = $GLOBALS['session']->getData('usuario');

        //Usuario con el que tenemos la charla
        $interlocutor = $GLOBALS['parametros']['usuario'];

        //Version de la conversacion
        $version = (int)$GLOBALS['parametros']['version'];



        //A partir del este ultimo mensaje
        $ultima_id = "";
        $mensaje = 0;
        if($GLOBALS['parametros']['mensaje'] != NULL){
            $version = 0;
            $mensaje = (int)$GLOBALS['parametros']['mensaje'];
            $ultima_id = " AND (id < ".$GLOBALS['parametros']['mensaje'].") ";
        }

        $sql = "SELECT  *   FROM (
                                SELECT      id,
                                            emisor,
                                            UNIX_TIMESTAMP(enviado) as enviado,
                                            UNIX_TIMESTAMP(recibido) as recibido,
                                            UNIX_TIMESTAMP(leido) as leido,
                                            UNIX_TIMESTAMP(actualizado) as actualizado,
                                            UNIX_TIMESTAMP(actualizado) as version,
                                            DATE_FORMAT(enviado,'%H:%i') as hora,
                                            DATE_FORMAT(enviado,'%e/%m/%Y') as fecha,
                                            mensaje,
                                            tipo
                                            
                                FROM        mensajes
                                WHERE       ((emisor = '".$interlocutor."' AND receptor = '".$usuario."') OR 
                                            (emisor = '".$usuario."' AND receptor='".$interlocutor."')) AND
                                            (actualizado > FROM_UNIXTIME(".$version."))". $ultima_id ."
                                ORDER BY    enviado DESC
                                LIMIT       11
                            ) t
                ORDER BY enviado ASC;";

        //Obtenemos datos del Interlocutor
        $sql .= "SELECT     IF( rol_id = 2, 
                                (CONCAT(usuario,' - ',(SELECT razon_social FROM clientes WHERE usuario = t.usuario))), 
                                nombre
                            ) as nombre,
                            rol_id
                 FROM       usuarios t
                 WHERE      usuario='".$interlocutor."';";

        //Actualizamos timestamp recibido de mensajes emitidos por interlocutor
        $sql .= "UPDATE mensajes SET recibido = NOW() WHERE (emisor = '".$interlocutor."' AND receptor = '".$usuario."') AND (recibido = 0);";
        //Actualizamos timestamp leido de mensajes emitidos por interlocutor
        $sql .= "UPDATE mensajes SET leido = NOW() WHERE (emisor = '".$interlocutor."' AND receptor = '".$usuario."') AND (leido = 0);";

        //Obtenemos los mensajes
        $stmt = $GLOBALS['conf']['pdo']->query($sql);
        $mensajes = $stmt->fetchAll(PDO::FETCH_ASSOC);

        //Obtenemos datos de Interlocutor
        $stmt->nextRowset();
        $nombre = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $stmt->nextRowset();
        $stmt->nextRowset();


        //Existen mas mensajes?
        $existen_mas_msgs = false;
        if(count($mensajes) == 11){
            $existen_mas_msgs = true;
            unset($mensajes[0]);
            $mensajes = array_values($mensajes);
        }

        //Avatar
        $avatar = '';
        $avatars = array_values(preg_grep('~^'.$interlocutor.'-.*\.(jpg)$~', scandir("avatar/")));
        if(count($avatars)) $avatar = './avatar/'.$avatars[0];



        $ultima_version = 0;
        foreach ($mensajes as $k=>$a){

            //Actualizamos la version
            if((int)$a['actualizado'] > $ultima_version) 
                $ultima_version = (int)$a['actualizado'];

            //Volvemos a setear los tipos
            $mensajes[$k]['id'] = (int)$a['id'];
            $mensajes[$k]['enviado'] = (int)$a['enviado'];
            $mensajes[$k]['recibido'] = (int)$a['recibido'];
            $mensajes[$k]['leido'] = (int)$a['leido'];
            $mensajes[$k]['actualizado'] = (int)$a['actualizado'];
            $mensajes[$k]['propio'] = (($a['emisor'] == $usuario) ? true : false);
            $mensajes[$k]['modo'] = (($mensajes[$k]['enviado'] > $version) ? 'agrega' : 'actualiza');

            //Agregamos Fecha y Label de Fecha
            $mensajes[$k]['fecha'] = date("j/n/y",(int)$a['enviado']);
            $label = date("j/n/y", (int)$a['enviado']);
            $numero_dia = date('N', (int)$a['enviado']);

            //Si paso menos de una semana del mensaje
            if((int)$a['enviado'] > strtotime("-1 week"))
                if($numero_dia == 1) $label = "Lunes";
                elseif($numero_dia == 2) $label = "Martes";
                elseif($numero_dia == 3) $label = "Miercoles";
                elseif($numero_dia == 4) $label = "Jueves";
                elseif($numero_dia == 5) $label = "Viernes";
                elseif($numero_dia == 6) $label = "Sabado";
                elseif($numero_dia == 7) $label = "Domingo";

            //Si la fecha es HOY o Ayer
            if(date("j/n/y") == $mensajes[$k]['fecha']) $label = "Hoy";
            elseif(date("j/n/y",time() - 60 * 60 * 24) == $mensajes[$k]['fecha']) $label = "Ayer";

            //Agregamos el Label
            $mensajes[$k]['fecha_label'] = $label;

            //Si es un adjunto, calculamos tamaño y extension
            if(($a['tipo'] == 'image') || ($a['tipo'] == 'document') || ($a['tipo'] == 'video') || ($a['tipo'] == 'ptt')){
                $adjunto_info = pathinfo($a['mensaje']);
                $mensajes[$k]['extension'] = $adjunto_info['extension'];
                $mensajes[$k]['peso'] = $GLOBALS['toolbox']->formatSizeUnits(filesize('./mensajes/'.$a['id'].'.'.$adjunto_info['extension']));
            }

            //Quitamos campos inservibles
            unset($mensajes[$k]['emisor']);
        }

        //Si no es un cliente Chequeamos si esta offline
        if($nombre[0]['rol_id'] != 2){
           $GLOBALS['resultado']->_result['proxima_conexion'] = $GLOBALS['toolbox']->calculaSiguienteHorario($interlocutor);
        }

        //Devolvemos resultados
        $GLOBALS['resultado']->_result['usuario'] = $interlocutor;
        $GLOBALS['resultado']->_result['nombre'] = $nombre[0]['nombre'];
        $GLOBALS['resultado']->_result['avatar'] = $avatar;
        $GLOBALS['resultado']->_result['mensajes'] = $mensajes;
        $GLOBALS['resultado']->_result['mensaje'] = $mensaje;
        $GLOBALS['resultado']->_result['version'] = $ultima_version;
        $GLOBALS['resultado']->_result['existen_mas'] = $existen_mas_msgs;
    }


    /**
     * @nombre: Cierra una conversacion abierta
     * @descripcion: Cierra una conversacion abierta
     */
    public function conversacion_cerrar(){
        //Usuario con el que tenemos la charla
        $interlocutor = $GLOBALS['parametros']['usuario'];
        
        //Usuario Actual
        $usuario = $GLOBALS['session']->getData('usuario');
        
        $sql = "DELETE FROM     conversaciones 
                WHERE           (usuario_a = '".$usuario."' AND usuario_b = '".$interlocutor."');";

        $sql .= "UPDATE mensajes SET actualizado=NOW() WHERE id IN (SELECT id FROM (
                                                    SELECT  max(id) as id, IF( emisor = 'fede', receptor, emisor) as interlocutor                                            
                                                    
                                                    FROM    mensajes 
                                                    WHERE   (receptor = '".$usuario."' AND emisor = '".$interlocutor."') OR
                                                            (receptor = '".$interlocutor."' AND emisor = '".$usuario."')
                                                    GROUP BY interlocutor
                                                ) t);";

        //Query DB
        $stmt = $GLOBALS['conf']['pdo']->query($sql);
    }

    /**
     * @nombre: Mostramos adjunto
     * @descripcion: Mostramos adjunto
     */
    public function adjunto(){
        $thumb = (int)$GLOBALS['parametros']['thumb'];
        $id = (int)$GLOBALS['parametros']['id'];

        //Busca el adjunto en el mensaje que nos pasaron por parametro
        $stmt = $GLOBALS['conf']['pdo']->query('SELECT * FROM mensajes WHERE id = '.$id);
        $mensajes =  $stmt->fetchAll(PDO::FETCH_ASSOC);
        $usuario = $GLOBALS['session']->getData('usuario');

        //Si no encontro mensajes salimos
        if(!count($mensajes)) return;
        $mensajes = $mensajes[0]; 

        //Obtenemos la extension
        $ext = pathinfo($mensajes['mensaje'], PATHINFO_EXTENSION);
        $name = './mensajes/'.$mensajes['id'].'.'.$ext;

        //Si no encontro el adjunto salimos
        if(!file_exists('./mensajes/'.$mensajes['id'].'.'.$ext)) return;

        //Si ni el receptor ni el emisor es este usuario salimos
        if(!(($mensajes['emisor'] == $usuario) || ($mensajes['receptor'] == $usuario))) exit;

        //Si el adjunto es una imange
        if(($ext == 'jpg') || ($ext == 'gif') || ($ext == 'png')){
            //Definimos rutas
            $ruta_thumb = './mensajes/'.$mensajes['id'].'_thumb'.'.'.$ext;
            $ruta_original = './mensajes/'.$mensajes['id'].'.'.$ext;
            //seteamos el nombre segun lo que pidan
            if($thumb) $name = $ruta_thumb;
            else $name = $ruta_original;
        }

        //Abrimos el adjunto
        $fp = fopen($name, 'rb');
        $finfo = finfo_open(FILEINFO_MIME_TYPE);
        $mime_adjunto = finfo_file($finfo, $name);
        finfo_close($finfo);

        $GLOBALS['toolbox']->smartReadFile($name, $mensajes['mensaje'], $mime_adjunto);
    }

    /**
     * @nombre: Limpia mensajes anteriores a 3 meses
     * @descripcion: Limpia mensajes anteriores a 3 meses
     */
    public function limpiar(){

        //Hay mensajes viejos?
        $stmt = $GLOBALS['conf']['pdo']->query('SELECT * FROM mensajes WHERE enviado <= DATE_SUB(CURDATE(), INTERVAL 3 MONTH)');
        $mensajes =  $stmt->fetchAll(PDO::FETCH_ASSOC);

        //Borramos todos los adjuntos
        foreach($mensajes as $k=>$m){
            if(($m['tipo'] == 'image') || ($m['tipo'] == 'document') || ($m['tipo'] == 'video') || ($m['tipo'] == 'ptt')){
                $ext = pathinfo($mensajes['mensaje'], PATHINFO_EXTENSION);
                $archivo = './mensajes/'.$m['id'].'.'.$ext;
                //Borramos archivo
                unlink($archivo);
                //Borramos thumb
                if($m['tipo'] != 'image') 
                    unlink('./mensajes/'.$m['id'].'_thumb.'.$ext);
            }
        }

        //Borramos los mensajes de la base de datos
        $GLOBALS['conf']['pdo']->query('DELETE FROM mensajes WHERE enviado <= DATE_SUB(CURDATE(), INTERVAL 3 MONTH)');
    }
}
?>