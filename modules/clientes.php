<?php
/**
 * @nombre: Clientes
 * @descripcion: Lista los Clientes.
 */
class clientes extends module{


    /*
	 * Constructor
	 */
    public function __construct(){
    }

    /**
     * Validaciones previas
     */
    public function __pre($accion){

        //Si existe el parametro usuario...
        if(isset($GLOBALS['parametros']['usuario']) && ($GLOBALS['parametros']['usuario'] != '')){
            $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT * FROM usuarios WHERE usuario = '".$GLOBALS['parametros']['usuario']."'");
            $stmt->execute();
            $usuario = $stmt->rowCount();
            $usr = $stmt->fetch(PDO::FETCH_ASSOC);
        }

        //Si existe el parametro email...
        if(isset($GLOBALS['parametros']['email']) && ($GLOBALS['parametros']['email'] != '')){
            $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT * FROM usuarios WHERE email = '".$GLOBALS['parametros']['email']."'");
            $stmt->execute();
            $email = $stmt->rowCount();
            $usr_email = $stmt->fetch(PDO::FETCH_ASSOC);
        }

        //Si existe el parametro localidad...
        if(isset($GLOBALS['parametros']['localidad']) && ($GLOBALS['parametros']['localidad'] != '')){
            $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT id FROM sys_localidades WHERE id = ".$GLOBALS['parametros']['localidad']);
            $stmt->execute();
            $localidad = $stmt->rowCount();
        }

        //Si existe el parametro cuit...
        if(isset($GLOBALS['parametros']['cuit']) && ($GLOBALS['parametros']['cuit'] != '')){
            $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT * FROM clientes WHERE cuit = '".$GLOBALS['parametros']['cuit']."'");
            $stmt->execute();
            $cuit = $stmt->rowCount();
            $usr_cuit = $stmt->fetch(PDO::FETCH_ASSOC);
        }

        //Si no existe el usuario
        if( isset($usuario) && !$usuario && 
            in_array($accion, array('eliminar','editar','info','permisos_listar','permisos_editar'))){
            $GLOBALS['resultado']->setError("El Cliente no existe.");
            return;
        }

        //Si existe el usuario
        if( isset($usuario) && $usuario && 
            in_array($accion, array('agregar','crear_cuenta'))){
            $GLOBALS['resultado']->setError("El Usuario que intenta agregar ya existe.");
            return;
        }

        //Si existe el email
        if( isset($email) && $email && 
            in_array($accion, array('agregar','crear_cuenta'))){
            $GLOBALS['resultado']->setError("El email que intenta agregar ya existe.");
            return;
        }

        //Si existe la Localidad
        if( isset($localidad) && !$localidad && 
            in_array($accion, array('editar_cuenta','crear_cuenta','agregar','editar'))){
            $GLOBALS['resultado']->setError("La Localidad que selecciono no existe.");
            return;
        }

        //No repetir el CUIT
        if( in_array($accion, array('editar_cuenta')) && isset($cuit) && $cuit &&
            ($usr_cuit['usuario'] != $GLOBALS['session']->getData('usuario'))){
            //Error
            $GLOBALS['resultado']->setError("El CUIT que esta intentando agregar esta siendo usado por otro cliente.");
            return;
        }

        //No repetir el Email
        if( in_array($accion, array('editar_cuenta')) && isset($email) && $email &&
            ($usr_email['usuario'] != $GLOBALS['session']->getData('usuario')) ){
            //Error
            $GLOBALS['resultado']->setError("El Email que esta intentando agregar esta siendo usado por otro cliente.");
            return;
        }

        //llamamos a la accion
        return call_user_func_array(array($this, $accion), array());
    }

    /**
     * @nombre: Información del Cliente
     * @descripcion: Pasamos login del Cliente y nos devuelve su Info.
     */
    public function info(){

        //Obtenemos los usuarios
        $stmt = $GLOBALS['conf']['pdo']->prepare("  SELECT  *,
                                                            (CONCAT(
                                                                (SELECT nombre FROM sys_localidades WHERE id = clientes.localidad),
                                                                ', ',
                                                                (SELECT nombre FROM sys_provincias WHERE id = (SELECT provincia_id FROM sys_localidades WHERE id = clientes.localidad))
                                                                    )
                                                            ) as localidad_label,
                                                            (SELECT nombre FROM usuarios WHERE usuario = clientes.vendedor) as vendedor_label,
                                                            (SELECT email from usuarios WHERE usuario = clientes.usuario) as email,
                                                            (SELECT activo from usuarios WHERE usuario = clientes.usuario) as activo

                                                    FROM clientes
                                                    WHERE usuario = '".$GLOBALS['parametros']['cliente']."'");
        $stmt->execute();
        $datos = $stmt->fetchAll(PDO::FETCH_ASSOC);

        //Si no tiene permisos
        if(!count($datos)){
            //Agregamos error 
            $GLOBALS['resultado']->setError("El Cliente no existe.");
            return;
        }

        //Guardamos los datos
        $GLOBALS['resultado']->_result = $datos[0];
    }

    /**
     * @nombre: Lista Clientes
     * @descripcion: Listado de Clientes completo.
     */
    public function listar(){

        //EStamos buscando o solo listando?
        if($GLOBALS['parametros']['q'] == '') $busqueda = false;
        else $busqueda = true;

        //SQL
        $stmt = $GLOBALS['conf']['pdo']->prepare("  SELECT  *,
                                                            (CONCAT(
                                                                (SELECT nombre FROM sys_localidades WHERE id = clientes.localidad),
                                                                ', ',
                                                                (SELECT nombre FROM sys_provincias WHERE id = (SELECT provincia_id FROM sys_localidades WHERE id = clientes.localidad))
                                                                    )
                                                            ) as localidad_label,
                                                            (SELECT nombre FROM usuarios WHERE usuario = clientes.vendedor) as vendedor_label,
                                                            (SELECT email from usuarios WHERE usuario = clientes.usuario) as email,
                                                            (SELECT activo from usuarios WHERE usuario = clientes.usuario) as activo,
                                                            (SELECT COUNT(*) FROM sessions WHERE (latencia >= NOW() - INTERVAL 1 MINUTE) AND (usuario = clientes.usuario)) as estado

                                                    FROM clientes
                                                    ORDER BY activo ASC, usuario ASC");
        $stmt->execute();
        $clientes =  $stmt->fetchAll(PDO::FETCH_ASSOC);
       
        //Si estamos buscando...
        if($busqueda){
            //Score
            foreach($clientes as $k=>$v)
                $clientes[$k]['score'] = $GLOBALS['toolbox']->compare_strings($GLOBALS['parametros']['q'], $v['usuario'].' '.$v['cuit'].' '.$v['razon_social'].' '.$v['nombre_del_comercio'].' '.$v['email']);

            //Eliminamos 0's
            foreach($clientes as $k=>$v)
                if($v['score'] == 0)
                    unset($clientes[$k]);

            //Ordenamos
            usort($clientes, function($a, $b) {
                return $b['score'] - $a['score'];
            });
        }

        //Asignamos resultados
        $GLOBALS['resultado']->_result = $clientes;
    }

    /**
     * @nombre: Agregar un Cliente
     * @descripcion: Agrega un Cliente, pasándole los datos y el usuario.
     */
    public function agregar(){

        //SQL
        $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT COUNT(*) as cant FROM usuarios WHERE email='".$GLOBALS['parametros']['email']."'");
        $stmt->execute();
        $count_user = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $count_user = (int)$count_user[0]['cant'];

        //Si no tiene permisos
        if( (int)$count_user ){
            //Agregamos error 
            $GLOBALS['resultado']->setError("El email ya existe en otro usuario.");
            return;
        }



        // Begin Transaction
        $GLOBALS['conf']['pdo']->beginTransaction();

        try {

            //Agregamos el usuario
            $stmt = $GLOBALS['conf']['pdo']->prepare("  INSERT INTO usuarios (usuario, password, email, activo, rol_id, nombre) 
                                                        VALUES (    '".$GLOBALS['parametros']['cliente']."',
                                                                    '".$GLOBALS['parametros']['password']."',
                                                                    '".$GLOBALS['parametros']['email']."',
                                                                    ".$GLOBALS['parametros']['activo'].",
                                                                    2, '')");
            $stmt->execute();

            //Agregamos el perfil
            $stmt = $GLOBALS['conf']['pdo']->prepare("  INSERT INTO clientes (usuario, cuit, razon_social, nombre_del_comercio, persona_de_contacto, localidad, domicilio, telefono) 
                                                        VALUES ('".$GLOBALS['parametros']['cliente']."',
                                                                '".$GLOBALS['parametros']['cuit']."',
                                                                '".$GLOBALS['parametros']['razon_social']."',
                                                                '".$GLOBALS['parametros']['nombre_del_comercio']."',
                                                                '".$GLOBALS['parametros']['persona_de_contacto']."',
                                                                ".$GLOBALS['parametros']['localidad'].",
                                                                '".$GLOBALS['parametros']['domicilio']."',
                                                                '".$GLOBALS['parametros']['telefono']."')");
            $stmt->execute();


            // Commit Transaction
            $GLOBALS['conf']['pdo']->commit();

        } // Si existieron errores generando las Querys de arriba...
        catch (PDOException $e) {
            // hacemos un Rollback
            $GLOBALS['conf']['pdo']->rollback();

            if( ((int)$e->errorInfo[0] == 23000) && ((int)$e->errorInfo[1] == 1062) ){
                $GLOBALS['resultado']->setError('El cliente que intenta agregar ya existe');
                echo $GLOBALS['resultado']->getResult();
                exit;
            }

            //Agregamos error 
            $GLOBALS['resultado']->setError($e);
            echo $GLOBALS['resultado']->getResult();
            exit;
        }
    }

    /**
     * @nombre: Crear Cuenta de Cliente (login)
     * @descripcion: Crea una cuenta y la deja pendiente de aprobacion.
     */
    public function crear_cuenta(){

        // Begin Transaction
        $GLOBALS['conf']['pdo']->beginTransaction();

        try {

            //Agregamos el usuario
            $stmt = $GLOBALS['conf']['pdo']->prepare("  INSERT INTO usuarios (usuario, password, email, activo, rol_id, permisos_manuales, nombre) 
                                                        VALUES (    '".$GLOBALS['parametros']['cliente']."',
                                                                    '".$GLOBALS['parametros']['password']."',
                                                                    '".$GLOBALS['parametros']['email']."',
                                                                    -1, 2, 0, '')");
            $stmt->execute();

            //Agregamos el perfil
            $stmt = $GLOBALS['conf']['pdo']->prepare("  INSERT INTO clientes (usuario, cuit, razon_social, nombre_del_comercio, persona_de_contacto, localidad, domicilio, telefono) 
                                                        VALUES ('".$GLOBALS['parametros']['cliente']."',
                                                                '".$GLOBALS['parametros']['cuit']."',
                                                                '".$GLOBALS['parametros']['razon_social']."',
                                                                '".$GLOBALS['parametros']['nombre_del_comercio']."',
                                                                '".$GLOBALS['parametros']['persona_de_contacto']."',
                                                                ".$GLOBALS['parametros']['localidad'].",
                                                                '".$GLOBALS['parametros']['domicilio']."',
                                                                '".$GLOBALS['parametros']['telefono']."')");
            $stmt->execute();


            // Commit Transaction
            $GLOBALS['conf']['pdo']->commit();

        } // Si existieron errores generando las Querys de arriba...
        catch (PDOException $e) {
            // hacemos un Rollback
            $GLOBALS['conf']['pdo']->rollback();

            //Agregamos error 
            $GLOBALS['resultado']->setError($e);
            return;
        }

        //Enviamos correo a los administradores

        $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT email FROM usuarios WHERE rol_id IN (-1)");
        $stmt->execute();
        $emails = $stmt->fetchAll(PDO::FETCH_COLUMN, 0);

        $mail = new send_mail(true);

        try {

            $mail->Subject = 'Datos de acceso';
             
            $mail->isHTML(true);

            //$mail->SMTPDebug  = 2; // Debugmode

            $mail->Body    = '<html><head></head><body text="#000000" bgcolor="#FFFFFF"><table width="100%"><tbody><tr><td align="right"><img alt="logo" style="float: right;" src="http://www.distribuidoralibertad.com/logo_distribuidora.jpg" height="54" width="200" align="right"></td></tr><tr><td>';

            $mail->Body   .= "Se ha registrado el Cliente número <b>".$GLOBALS['parametros']['cliente']."</b> ".$GLOBALS['parametros']['nombre_del_comercio']." y esta esperando su aprobación para poder comenzar a utilizar la cuenta. <br/>Por favor, ingrese al sistema para revisar los datos y dar de alta la Cuenta.";

            $mail->Body   .= "</td></tr><tr><td><hr><p style=\"font-size: 10px;\"><b style=\"font-size: 12px;\">Federico Iglesias Colombo<br>Distribuidora Libertad</b></p><p style=\"font-size: 10px;\">Mar del Plata. Buenos Aires. Argentina. <br>Chile 2019 (B7604-FHI)<br>TEL: (0223) 474-1222 / 474-1223 / 475-8352<br>FAX: (0223) 475- 7170 / 0800-333-6590<br></p></td></tr></tbody></table></body></html>";

            $mail->AltBody = "Se ha registrado el Cliente número ".$GLOBALS['parametros']['cliente']." ".$GLOBALS['parametros']['nombre_del_comercio']." y esta esperando su aprobación para poder comenzar a utilizar la cuenta. \nPor favor, ingrese al sistema para revisar los datos y dar de alta la Cuenta.";
            
            //Agregamos los correos
            foreach($emails as $m) $mail->AddAddress($m);

            $mail->Send();

        } catch (phpmailerException $e) {
            //echo $e->errorMessage(); //Pretty error messages from PHPMailer
            $GLOBALS['resultado']->setError("Ocurrió un error al comunicarle su alta al Administrador por favor comuniquese telefónicamente al 0223-4741222.");
            return;
        }


        $mail->ClearAddresses();
        $mail->ClearAttachments();
    }

    /**
     * @nombre: Editar un Cliente
     * @descripcion: Edita un Cliente, pasándole los datos y el usuario.
     */
    public function editar(){

        //Password
        if( ($GLOBALS['parametros']['password'] != '') && (strlen($GLOBALS['parametros']['password']) > 3) ){
            $GLOBALS['parametros']['password'] = "password='".$GLOBALS['parametros']['password']."',";
        }else{
            $GLOBALS['parametros']['password'] = '';
        }

        // Begin Transaction
        $GLOBALS['conf']['pdo']->beginTransaction();

        try {

            //Agregamos el usuario
            $stmt = $GLOBALS['conf']['pdo']->prepare("  UPDATE clientes 
                                                        SET cuit='".$GLOBALS['parametros']['cuit']."',
                                                            razon_social='".$GLOBALS['parametros']['razon_social']."',
                                                            nombre_del_comercio='".$GLOBALS['parametros']['nombre_del_comercio']."',
                                                            persona_de_contacto='".$GLOBALS['parametros']['persona_de_contacto']."',
                                                            vendedor='".$GLOBALS['parametros']['vendedor']."',
                                                            localidad=".$GLOBALS['parametros']['localidad'].",
                                                            domicilio='".$GLOBALS['parametros']['domicilio']."',
                                                            telefono='".$GLOBALS['parametros']['telefono']."'
                                                        WHERE usuario='".$GLOBALS['parametros']['cliente']."'");  
            $stmt->execute();


            //Agregamos el perfil
            $stmt = $GLOBALS['conf']['pdo']->prepare("  UPDATE usuarios 
                                                        SET ".$GLOBALS['parametros']['password']."
                                                            email='".$GLOBALS['parametros']['email']."',
                                                            activo=".$GLOBALS['parametros']['activo']."
                                                        WHERE usuario='".$GLOBALS['parametros']['cliente']."'");
            $stmt->execute();


            // Commit Transaction
            $GLOBALS['conf']['pdo']->commit();

        } // Si existieron errores generando las Querys de arriba...
        catch (PDOException $e) {
            // hacemos un Rollback
            $GLOBALS['conf']['pdo']->rollback();

            //Agregamos error 
            $GLOBALS['resultado']->setError($e);
            echo $GLOBALS['resultado']->getResult();
            exit;
        }
    }

    /**
     * @nombre: Editar Cuenta de Cliente
     * @descripcion: Edita un la cuenta de un cliente
     */
    public function editar_cuenta(){

        // Begin Transaction
        $GLOBALS['conf']['pdo']->beginTransaction();

        try {

            //Agregamos el usuario
            $stmt = $GLOBALS['conf']['pdo']->prepare("  UPDATE  clientes 
                                                        SET     cuit='".$GLOBALS['parametros']['cuit']."',
                                                                razon_social='".$GLOBALS['parametros']['razon_social']."',
                                                                nombre_del_comercio='".$GLOBALS['parametros']['nombre_del_comercio']."',
                                                                persona_de_contacto='".$GLOBALS['parametros']['persona_de_contacto']."',
                                                                localidad=".$GLOBALS['parametros']['localidad'].",
                                                                domicilio='".$GLOBALS['parametros']['domicilio']."',
                                                                telefono='".$GLOBALS['parametros']['telefono']."'
                                                        WHERE   usuario='".$GLOBALS['session']->getData('usuario')."'");  
            $stmt->execute();


            //Agregamos el perfil
            $stmt = $GLOBALS['conf']['pdo']->prepare("  UPDATE usuarios 
                                                        SET email='".$GLOBALS['parametros']['email']."'
                                                        WHERE usuario='".$GLOBALS['session']->getData('usuario')."'");
            $stmt->execute();


            // Commit Transaction
            $GLOBALS['conf']['pdo']->commit();

        }catch(PDOException $e){
            // hacemos un Rollback
            $GLOBALS['conf']['pdo']->rollback();

            //Agregamos error 
            $GLOBALS['resultado']->setError($e);
            return;
        }
    }

    /**
     * @nombre: Eliminar un Cliente
     * @descripcion: Elimina un Cliente, pasándole el usuario.
     */
    public function eliminar(){

        // Begin Transaction
        $GLOBALS['conf']['pdo']->beginTransaction();

        try {

            //Agregamos el usuario
            $stmt = $GLOBALS['conf']['pdo']->prepare("DELETE FROM usuarios WHERE usuario = '".$GLOBALS['parametros']['cliente']."'");
            $stmt->execute();

            // Commit Transaction
            $GLOBALS['conf']['pdo']->commit();

        } // Si existieron errores generando las Querys de arriba...
        catch (PDOException $e) {
            // hacemos un Rollback
            $GLOBALS['conf']['pdo']->rollback();

            //Agregamos error 
            $GLOBALS['resultado']->setError('Error al eliminar Cliente');
            return;
        }
    }

    /**
     * @nombre: Obtener Razon Social
     * @descripcion: Pasamos CUIT del Cliente y nos devuelve su Razon social.
     */
    public function info_razon_social(){

        //Guardamos los datos
        $razon_social = $GLOBALS['toolbox']->getRZ( $GLOBALS['parametros']['cuit'] );

        //Printeamos razon social
        $GLOBALS['resultado']->_result = $razon_social;
    }

    /**
     * @nombre: Obtener Vendedores
     * @descripcion: Obtener Vendedores
     */
    public function listar_vendedores(){

        //Calculate paging
        $page =  (intval($GLOBALS['parametros']['p'])-1) * 40;

        //EStamos buscando o solo listando?
        $busqueda = true;
        if($GLOBALS['parametros']['q'] == '') 
            $busqueda = false;

        //SQL
        $stmt = $GLOBALS['conf']['pdo']->prepare("  SELECT usuario as id, nombre            
                                                    FROM usuarios
                                                    WHERE (rol_id = 5) AND (usuario <> 'diego') AND (usuario <> 'melina')
                                                    ORDER BY nombre");
        $stmt->execute();
        $vendedores =  $stmt->fetchAll(PDO::FETCH_ASSOC);


        //Si estamos buscando...
        if($busqueda){
            //Score
            foreach($vendedores as $k=>$v)
                $vendedores[$k]['score'] = $GLOBALS['toolbox']->compare_strings($GLOBALS['parametros']['q'], $v['nombre']);

            //Eliminamos 0's
            foreach($vendedores as $k=>$v)
                if($v['score'] == 0)
                    unset($vendedores[$k]);

            //Ordenamos
            usort($vendedores, function($a, $b) {
                return $b['score'] - $a['score'];
            });
        }

        //Calculamos totales
        $GLOBALS['resultado']->_result['total'] = count($vendedores);

        //Devolvemos items y total
        $GLOBALS['resultado']->_result['items'] = array_slice($vendedores, $page, 40);
    }


}
?>