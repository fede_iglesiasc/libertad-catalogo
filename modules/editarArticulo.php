<?php
/**
 * @nombre: Edición general del Articulo
 * @descripcion: Edicion General, Aplicaciones, Equivalencias y Dimensiones.
 */
class editarArticulo extends module{


    /*
	 * Constructor
	 */
    public function __construct(){

    }

    /**
     * @nombre: Informacion general del Artículo
     * @descripcion: Info del ARtículo
     */
    public function info(){

        // Traemos los datos del articulo primero
        $stmt = $GLOBALS['conf']['pdo']->prepare("  SELECT  *, 
                                                            (SELECT nombre FROM catalogo_marcas WHERE id = id_marca) as marca_label

                                                    FROM    catalogo_articulos 
                                                    WHERE   id = ". $GLOBALS['parametros']['articulo']);
        $stmt->execute();
        $art = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $art = $art[0];

        //Conseguimos el listado completo de Rubros
        $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT * FROM catalogo_rubros");
        $stmt->execute();
        $rubros =  $stmt->fetchAll(PDO::FETCH_ASSOC);

        $GLOBALS['resultado']->_result['dimensiones'] = array();

        // Las dimensiones las pedimos a la base de datos solo si el rubro
        // es distinto de null
        if(!is_null($art['id_rubro'])){
            //Obtenemos mediante esta funcion todos los Rubros Progenitores
            $rubroFamilia = $GLOBALS['toolbox']->traeRubrosPadres($rubros, $art['id_rubro']);

            //Obtenemos datos del Rubro (propios y heredados)
            $stmt = $GLOBALS['conf']['pdo']->prepare("  SELECT id, nombre 
                                                        FROM catalogo_rubros_datos 
                                                        WHERE 
                                                            ( (rubro_id IN (".implode(',',$rubroFamilia).")) AND (cede_decendencia = 'Y') )
                                                            OR
                                                            (rubro_id = ".$art['id_rubro'].")");
            $stmt->execute();
            $datosRubros =  $stmt->fetchAll(PDO::FETCH_ASSOC);

            
            foreach ($datosRubros as $k=>$d)
                if( (trim($art[ 'extra_'.$d['id'] ]) != '') && !is_null($art[ 'extra_'.$d['id'] ]) )
                    $GLOBALS['resultado']->_result['dimensiones'][] = array('nombre'=> $d['nombre'], 'valor'=>$art[ 'extra_'.$d['id'] ]);
        }

        //Conseguimos el listado completo de Rubros
        $stmt = $GLOBALS['conf']['pdo']->prepare("  SELECT
                                                            articulo_id as id,
                                                            (SELECT CONCAT(prefijo,'-',codigo,'-',sufijo) FROM catalogo_articulos WHERE id = articulo_id) as codigo,
                                                            (SELECT nombre FROM catalogo_marcas WHERE id = marca_id) as marca,
                                                            (SELECT id_rubro FROM catalogo_articulos WHERE id = articulo_id) as rubro_id 
                                                    
                                                    FROM    catalogo_articulos_equiv 

                                                    WHERE   (id = (SELECT id FROM catalogo_articulos_equiv WHERE articulo_id = ".$GLOBALS['parametros']['articulo'].")) 
                                                            AND 
                                                            (articulo_id <> ".$GLOBALS['parametros']['articulo']." )");
        $stmt->execute();
        $equivalencias =  $stmt->fetchAll(PDO::FETCH_ASSOC);

        //Generamos bread
        foreach ($equivalencias as $x=>$e){
            $equivalencias[$x]['rubro'] = $GLOBALS['toolbox']->getBreadcrumbRubros($rubros,(int)$e['rubro_id']);
            unset($equivalencias[$x]['rubro_id']);
        }


        //Conseguimos el listado completo de Rubros
        $stmt = $GLOBALS['conf']['pdo']->prepare("  SELECT 
                                                        (SELECT CONCAT(prefijo,'-',codigo,'-',sufijo) FROM catalogo_articulos WHERE id = articulo_id) as codigo,
                                                        (SELECT nombre FROM catalogo_marcas WHERE id = (SELECT id_marca FROM catalogo_articulos WHERE id = articulo_id)) as marca,
                                                        (SELECT id_rubro FROM catalogo_articulos WHERE id = articulo_id) as rubro_id

                                                    FROM catalogo_articulos_equiv_dim 

                                                    WHERE 
                                                        (id = (SELECT id FROM catalogo_articulos_equiv_dim WHERE articulo_id = ".$GLOBALS['parametros']['articulo'].")) 
                                                        AND 
                                                        (articulo_id <> ".$GLOBALS['parametros']['articulo'].")");
        $stmt->execute();
        $equivDim =  $stmt->fetchAll(PDO::FETCH_ASSOC);

        //Generamos bread
        foreach ($equivDim as $x=>$e){
            $equivDim[$x]['rubro'] = $GLOBALS['toolbox']->getBreadcrumbRubros($rubros,(int)$e['rubro_id']);
            unset($equivDim[$x]['rubro_id']);
        }

        // Si la marca es automatica seteamos el id en -1
        // y le pasamos el nombre de la marca
        if(!$art['marca_manual']) $GLOBALS['resultado']->_result['marca_id'] = -1;
        
        // Si la marca es manual le pasamos el id que tiene asignado
        else $GLOBALS['resultado']->_result['marca_id'] = $art['id_marca'];

        // Sea automatica o manual siempre le pasamos al Nombre
        $GLOBALS['resultado']->_result['marca_label'] = $art['marca_label'];


        // Generamos el array de resultados
        $GLOBALS['resultado']->_result['rubro_id'] = $art['id_rubro'];
        $GLOBALS['resultado']->_result['rubro_bread'] = $GLOBALS['toolbox']->getBreadcrumbRubros($rubros,$art['id_rubro']);
        $GLOBALS['resultado']->_result['descripcion'] = $art['descripcion'];
        $GLOBALS['resultado']->_result['precio'] = $art['precio'];
        $GLOBALS['resultado']->_result['habilitado'] = (int)$art['habilitado'];
        $GLOBALS['resultado']->_result['equivalencias'] = $equivalencias;
        $GLOBALS['resultado']->_result['equivalencias_dim'] = $equivDim;
    }

    /**
     * @nombre: Listar los rubros
     * @descripcion: Listar los rubros
     */
    public function listar_rubros(){

        //Separamos las palabras clave en un arreglo
        $keyword_tokens = explode(' ', $GLOBALS['parametros']['q']);

        //Make query 
        $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT * FROM catalogo_rubros WHERE id <> 1 ORDER BY nombre");
        $stmt->execute();
        $rubros =  $stmt->fetchAll(PDO::FETCH_ASSOC);

        //Generamos el nivel de cada Rubro
        $rubros = $rubros_tmp = $GLOBALS['toolbox']->parentChildSort_r('id', 'padre', $rubros, $parentID = 1);

        //Paginado
        $page =  (intval($GLOBALS['parametros']['p'])-1) * 40;

        //Estamos buscando o listando
        if($GLOBALS['parametros']['q'] == '') $busqueda = false;
        else $busqueda = true;


        //creamos el breadcrumb
        foreach($rubros as $k=>$r) 
            $rubros[$k]['bread'] = $GLOBALS['toolbox']->getBreadcrumbRubros($rubros,$r['id']);


        //Si estamos buscando...
        if($busqueda){
            //Score
            foreach($rubros as $k=>$v)
                $rubros[$k]['score'] = $GLOBALS['toolbox']->compare_strings($GLOBALS['parametros']['q'], $v['bread']);

            // revisamos que existan todos los 
            // padres de cada uno de los rubros
            foreach ($rubros as $k=>$v) {
                // Si sumo algo de score
                // Y tiene algun padre q
                // no sea el 1
                if($v['score'] > 0){
                    //ids de los progenitores
                    $padres =  $GLOBALS['toolbox']->traeRubrosPadres($rubros,$v['id']);
                    //print_r($padres);

                    foreach ($rubros as $x=>$r1)
                        if( in_array($r1['id'],$padres) ){
                            //echo "padre: ".$r1['id'];
                            if($r1['score'] < $v['score'])
                                $rubros[$x]['score'] = $v['score'];
                        }
                    }
            }

            //Eliminamos 0's
            foreach($rubros as $k=>$v)
                if($v['score'] == 0)
                    unset($rubros[$k]);

            //Ordenamos
            usort($rubros, function($a, $b) {
                //Si el score es el mismo
                //Desempatan con natural order
                if ($a['score'] == $b['score'])
                    return strnatcmp ( $a['bread'], $b['bread'] );

                //Sino por score
                return ($a['score'] < $b['score']) ? 1 : -1;
            });
        }

        //Borramos variables no usadas
        foreach($rubros as $k=>$v)
            unset(  $rubros[$k]['padre'],
                    $rubros[$k]['nombre_singular'],
                    $rubros[$k]['orden'],
                    $rubros[$k]['nombre']);

        //Calculamos totales
        $GLOBALS['resultado']->_result['total'] = count($rubros);

        //Devolvemos items y total
        $GLOBALS['resultado']->_result['items'] = array_slice($rubros, $page, 40);

    }

    /**
     * @nombre: Listar los fabricantes
     * @descripcion: Listar los fabricantes
     */
    public function listar_fabricantes(){


        //Calculate paging
        $page =  ((int)$GLOBALS['parametros']['p'] - 1) * 40;

        //Separamos las palabras clave en un arreglo
        $keyword_tokens = explode(' ', trim( strtolower( $GLOBALS['toolbox']->stripAccents($GLOBALS['parametros']['q']) )));

        //Make query
        $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT * FROM catalogo_marcas WHERE id <> 1");
        $stmt->execute();
        $marcasDB =  $stmt->fetchAll(PDO::FETCH_ASSOC);

        // Quitamos las marcas que estan solamente para equivalencias
        // estas marcas, no usan codigos y no tienen hijos
        foreach ($marcasDB as $k=>$m){
            $tiene_hijos = false;
            if($m['usa_codigos'] == 'N'){

                //Chequeamos si tiene hijos 
                foreach ($marcasDB as $k1=>$m1)
                    if( ($m1['id'] != $m['id']) && ($m1['padre'] == $m['id']) ){
                        $tiene_hijos = true;
                        break;
                    }

                //Eliminamos la marca del listado
                if(!$tiene_hijos) unset($marcasDB[$k]);
            }
        }

        // Filtramos las marcas para el campo
        foreach ($marcasDB as $k=>$m)
            if($m['nivel'] == 2)
                $marcas[] = array( "id" => $m['id'], "nombre" => $m['nombre']);

        //Si estamos haciendo una busqueda..
        if($keyword_tokens[0] != ''){

            // recorremos todos los elementos y le damos score
            // inicial de 0, luego mostraremos solo los > 0
            foreach($marcas as $k=>$m) $marcas[$k]['score'] = 0;

            // Sumamos uno al score si 
            // hay coincidencia
            foreach($marcas as $k=>$v)
                foreach($keyword_tokens as $w)
                    //Le damos un punto si la palabra esta entre otras
                    if( stripos( strtolower($GLOBALS['toolbox']->stripAccents($v['nombre'])),$w) !== false)
                        $marcas[$k]['score']++;


            // Sumamos uno al score si hay
            // coincidencia justa
            foreach($marcas as $k=>$v){
                $nombre = stripos(strtolower($GLOBALS['toolbox']->stripAccents($v['nombre'])),$w);
                $nombre = explode(' ', $nombre);
                //Por cada palabra clave
                foreach($keyword_tokens as $w)
                    //Y por cada palabra del nombre...
                    foreach($nombre as $x)
                        // Si la palabra buscada es exacta sumamos puntos
                        if( $w == $x )
                            // Score ++
                            $marcas[$k]['score']++;
            }

            // Borramos scores 0
            foreach($marcas as $k=>$m)
                if($m['score'] == 0)
                    unset($marcas[$k]);

            // reindexamos
            $marcas = $equivAppSumadas = array_values($marcas);
            
            // Borramos variables no usadas
            foreach($marcas as $k=>$m)
                if(isset($m['score']))
                    unset($m['score']);

            // Ordenamos segun score
            // desempatamos por nombre
            usort($marcas, function($a, $b) {
                //Si el score es el mismo
                //Desempatan con natural order
                if ($a['score'] == $b['score'])
                    return strnatcmp ( $a['nombre'], $b['nombre'] );

                //Sino por score
                return ($a['score'] < $b['score']) ? 1 : -1;
            });

        }else{

            //Ordenamos alfabeticamente
            usort($marcas, function($a, $b) {
                return strnatcmp ( $a['nombre'], $b['nombre'] );
            });

        }

        //Cortamos el pedazo de array que nos interesa
        $marcas = array_slice($marcas, $page, 40);


        // Set resultados
        $GLOBALS['resultado']->_result['items'] = $marcas;
        $GLOBALS['resultado']->_result['total'] = count($marcas);
    }

    /**
     * @nombre: Editar el Artículo
     * @descripcion: Editar el Artículo
     */
    public function editar(){

        //Parseamos
        $equivalencias = $GLOBALS['parametros']['equivalencias'];
        //Validamos que todos los elementos sean enteros
        foreach ($equivalencias as $k=>$v) $equivalencias[$k] = (int)$equivalencias[$k];

        //Validamos
        $id = (int)$GLOBALS['parametros']['articulo'];
        $rubro = (int)$GLOBALS['parametros']['rubro'];
        $marca = (int)$GLOBALS['parametros']['fabricante'];
        $descripcion = $GLOBALS['parametros']['descripcion'];
        $habilitado = ($GLOBALS['parametros']['habilitado'] == '1')? 1 : 0;
        $precio = $GLOBALS['parametros']['precio'];
        

        //Conseguimos el listado completo de Rubros
        $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT * FROM catalogo_rubros");
        $stmt->execute();
        $rubros =  $stmt->fetchAll(PDO::FETCH_ASSOC);

        // Verificamos que el rubro por el cual queremos cambiar el articulo
        // exista, siempre y cuando no sea 0.

        if($rubro){
            $existe = false;
            foreach ($rubros as $k=>$r)
                if((int)$r['id'] == $rubro){
                    $existe = true;
                    break;
                }

            if(!$existe){
                //Agregamos error 
                $GLOBALS['resultado']->setError("El rubro no existe.");
                return;
            }
        }


        //Obtenemos datos del Rubro (propios y heredados)
        $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT * FROM catalogo_marcas");
        $stmt->execute();
        $marcasDB =  $stmt->fetchAll(PDO::FETCH_ASSOC);
        
        // Verificamos que la marca exista siempre y cuando no sea 0.
        if($marca && ($marca != -1)){
            $existe = false;
            foreach ($marcasDB as $k=>$m)
                if((int)$m['id'] == $marca){
                    $existe = true;
                    break;
                }

            if(!$existe){
                //Agregamos error 
                $GLOBALS['resultado']->setError("El rubro no existe.");
                return;
            }
        }


        // Traemos los datos del articulo primero
        $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT * FROM catalogo_articulos WHERE id = ". $id);
        $stmt->execute();
        $art = $stmt->fetchAll(PDO::FETCH_ASSOC);
        
        //Verificamos si existe el art
        if(!count($art)){
            //Agregamos error 
            $GLOBALS['resultado']->setError("El artículo no existe.");
            return;
        }

        //asignamos el articulo
        $art = $art[0];


        // Begin Transaction
        $GLOBALS['conf']['pdo']->beginTransaction();

        try {


            //Conseguimos todas las equivalencias que tiene el articulo
            //Si no tiene equivalencias igual agregamos el propio id
            $stmt = $GLOBALS['conf']['pdo']->query("SELECT articulo_id FROM catalogo_articulos_equiv WHERE id = (SELECT id FROM catalogo_articulos_equiv WHERE articulo_id = ". $id . ") AND articulo_id IS NOT NULL");
            $equivs = $stmt->fetchAll(PDO::FETCH_COLUMN, 0);
            if(!count($equivs)) $equivs[] = $id;


            // Revisamos primero si vamos a tener que hacer algun
            // cambio en el rubro, solo si cambiamos el rubro vamos
            // a realizar las consultas por sus equivalencias y las
            // validaciones que se requieran
            if( (int)$art['id_rubro'] != $rubro ){



                /*
                    Si funciona bien elimiar


                //Conseguimos todas las equivalencias que tiene el articulo
                $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT * FROM catalogo_articulos_equiv WHERE id = (SELECT id FROM catalogo_articulos_equiv WHERE articulo_id = ". $id . ")");
                $stmt->execute();
                $equivs = $stmt->fetchAll(PDO::FETCH_ASSOC);

                $equivsOK = array();
                foreach ($equivs as $x=>$eDB)
                    array_push($equivsOK, (int)$eDB['articulo_id']);

                array_push($equivsOK, $id);
                

                // Recorremos el arreglo de equivalencias que nos pasaron
                // por parametros filtrando los ids y dejando solo los que
                // existen en el grupo de equivalencias
                $equivsOK = array();
                foreach($equivalencias as $k=>$e)
                    foreach ($equivs as $x=>$eDB)
                        if((int)$eDB['articulo_id'] == $e)
                            array_push($equivsOK, (int)$e);
                    
                */
                //Agregamos el articulo actual al grupo de equivalencias
                

                // Ahora que tenemos las equivalencias validadas,
                // Si existe algún grupo donde se sincronicen las
                // dimensiones que incluya a los artículos a los
                // que queremos cambiarles el rubro, lo eliminamos.
                $stmt = $GLOBALS['conf']['pdo']->prepare("DELETE FROM catalogo_articulos_equiv_dim WHERE articulo_id IN (".implode(',',$equivs).")");
                $stmt->execute();

                // Quitamos tambien los articulos que quedaron sueltos en la tabla
                // de sincroniza dimensiones....
                $stmt = $GLOBALS['conf']['pdo']->prepare("DELETE FROM catalogo_articulos_equiv_dim WHERE id IN ( SELECT id FROM ( SELECT id FROM catalogo_articulos_equiv_dim GROUP BY id HAVING COUNT(*) = 1 ) AS ONLY_ONCE)");
                $stmt->execute();

                // Ahora que eliminamos los grupos innecesarios debemos
                // Actualizar los rubros de todos los artículos involucrados
                // y ademas eliminar los datos inecesarios del rubro anterior.
                // Tener en cuenta que si, el articulo por el cual etamos cambiando
                // comparte dimensiones con el nuevo rubro mantenemos estas dimensiones.

                //Obtenemos mediante esta funcion todos los Rubros Progenitores
                $rubroFamilia = $GLOBALS['toolbox']->traeRubrosPadres($rubros, $rubro);

                $sql = "SELECT id FROM catalogo_rubros_datos WHERE (rubro_id = ".$rubro.")";
                if(count($rubroFamilia)) $sql .= " OR ( (rubro_id IN (".implode(',',$rubroFamilia).")) AND (cede_decendencia = 'Y') )";

                //Obtenemos datos del Rubro (propios y heredados)
                $stmt = $GLOBALS['conf']['pdo']->prepare($sql);
                $stmt->execute();
                $datosRubro =  $stmt->fetchAll(PDO::FETCH_ASSOC);

                //Obtenemos datos del Rubro (propios y heredados)
                $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT id FROM catalogo_rubros_datos ORDER BY id");
                $stmt->execute();
                $datosRubros =  $stmt->fetchAll(PDO::FETCH_ASSOC);

                // Creamos un arreglo con todas las dimensiones que debemos NULLEAR.
                // Todas las dimensiones existentes menos las propias o heredadas del
                // nuevo rubro que vamos a asignar al artículo.
                foreach($datosRubros as $k=>$d){
                    $existe = false;
                    foreach($datosRubro as $x=>$d1)
                        if( $d['id'] == $d1['id'] ){
                            $existe = true;
                            break;
                        }
                    //Borramos de la lista la dimension
                    if($existe) unset($datosRubros[$k]);
                }
                $datosRubros = array_values($datosRubros);


                // Cometemos los cambios a los articulos, creamos una consulta SQL donde
                // actualizamos el rubro y nulleamos todas las dimensiones que no pertenecen
                // al nuevo rubro.

                //Insertamos el rubro
                if($rubro) $updateList[] = "id_rubro=".$rubro;
                else $updateList[] = "id_rubro= NULL";

                //Nulleamos las dimensiones
                foreach($datosRubros as $k=>$d) $updateList[] = "extra_".$d['id']."= NULL";

                //Ejecutamos la query
                $stmt = $GLOBALS['conf']['pdo']->prepare("UPDATE catalogo_articulos SET ".implode(', ',$updateList)." WHERE id IN (".implode(',',$equivs).")");
                $stmt->execute();
            }



            // Generamos el query para actualizar el articulo
            $updateList = array();

            //Agregamos precio
            $updateList[] = "precio=".$precio;


            //Cambiamos la descripcion de este y otros articulos
            if( $art['descripcion'] != $descripcion ){
                $stmt = $GLOBALS['conf']['pdo']->query("UPDATE catalogo_articulos SET descripcion='".$descripcion."' WHERE id IN (".implode(',', $equivs).")");
            }



            if( (int)$art['habilitado'] != $habilitado ) $updateList[] = "habilitado=".$habilitado;

            //Si la seleccion de la marca es automatica...
            if( $marca == -1 ){
                //Seteamos la deteccion de la marca a automatica
                $updateList[] = "marca_manual=0";
                //Obtenemos el iu de la marca segun el codigo
                $id_marca = $GLOBALS['toolbox']->getMarcaFromCodigo($art['prefijo'],$art['codigo'],$art['sufijo'],$marcasDB);
                
                //Si encontro la marca
                if($id_marca) $updateList[] = "id_marca=".$id_marca['id'];
                else $updateList[] = "id_marca= NULL";

            }else{
                //seteamos la marca manual
                $updateList[] = "marca_manual=1";
                //Seteamos la marca
                if($marca) $updateList[] = "id_marca=".$marca;
                else $updateList[] = "id_marca= NULL";
            }

            //Cometemos los cambios a la base de datos
            $stmt = $GLOBALS['conf']['pdo']->prepare("UPDATE catalogo_articulos SET ".implode(', ',$updateList)." WHERE id=".$id);
            $stmt->execute();


            // Commit Transaction
            $GLOBALS['conf']['pdo']->commit();



        } // Si existieron errores generando las Querys de arriba...
        catch (PDOException $e) {
            // hacemos un Rollback
            $GLOBALS['conf']['pdo']->rollback();

            //Agregamos error 
            $GLOBALS['resultado']->setError($e);
            return;
        }
    }
}
?>