<?php
/**
 * @nombre: Horarios
 * @descripcion: Administra panel de Horarios
 */
class horarios extends module{

    /* 
	 * Constructor
	 */
    public function __construct(){
    }



    //////////////////////////////////////////////
    //              HORARIOS                    //
    //////////////////////////////////////////////

    /**
     * @nombre: Información del Horario
     * @descripcion: Pasamos id del Horario y nos devuelve su Info.
     */
    public function info(){

        //Obtenemos los usuarios
        $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT * FROM horarios WHERE id = ".$GLOBALS['parametros']['horario']);
        $stmt->execute();
        $datos = $stmt->fetchAll(PDO::FETCH_ASSOC);

        //Si no Existe
        if(!count($datos)){
            //Agregamos error 
            $GLOBALS['resultado']->setError("El Horario no existe.");
            return;
        }

        //Guardamos los datos
        $GLOBALS['resultado']->_result = $datos[0];
    }

	/**
	* @nombre: Lista los horarios
	* @descripcion: Lista los horarios
	*/
	public function listar(){
		//Lista de Usuarios
		$stmt = $GLOBALS['conf']['pdo']->query("SELECT * FROM horarios");
		$horarios =  $stmt->fetchAll(PDO::FETCH_ASSOC);
		$GLOBALS['resultado']->_result = $horarios;
	}

    /**
     * @nombre: Agregar un Horario
     * @descripcion: Agrega un Horario, pasándole el nombre de Horario.
     */
    public function agregar(){
        $stmt = $GLOBALS['conf']['pdo']->prepare("INSERT INTO horarios (nombre) VALUES ('".$GLOBALS['parametros']['nombre']."')");
        $items = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $stmt->execute();
    }

    /**
     * @nombre: Edita un Horario
     * @descripcion: Edita un Horario, pasándole el nombre de Horario.
     */
    public function editar(){
        //Obtenemos los usuarios
        $stmt = $GLOBALS['conf']['pdo']->prepare(" 	UPDATE 	horarios 
        											SET 	nombre='".$GLOBALS['parametros']['nombre']."' 
        											WHERE 	id = ".$GLOBALS['parametros']['horario']);
        $stmt->execute();
    }

    /**
     * @nombre: Eliminar un Horario
     * @descripcion: Elimina un Horario
     */
    public function eliminar(){
        //Obtenemos los usuarios
        $stmt = $GLOBALS['conf']['pdo']->prepare("DELETE FROM horarios WHERE id = ".$GLOBALS['parametros']['horario']);
        $stmt->execute();
    }



    //////////////////////////////////////////////
    //              SEMANARIO                   //
    //////////////////////////////////////////////


    /**
     * @nombre: Información del Semanario de un Horario
     * @descripcion: Información del Semanario de un Horario
     */
    public function semanario_info(){

        //Obtenemos los usuarios
		$stmt = $GLOBALS['conf']['pdo']->query("	SELECT 	*,
															TIME_FORMAT(hora_inicio, '%H:%i') as hora_inicio_label,
															TIME_FORMAT(hora_fin, '%H:%i') as hora_fin_label 
													FROM 	horarios_semanario 
													WHERE 	id = ".$GLOBALS['parametros']['semanario']);
		$semanario =  $stmt->fetchAll(PDO::FETCH_ASSOC);
        
        //Si no Existe
        if(!count($semanario)){
            //Agregamos error 
            $GLOBALS['resultado']->setError("El Semanario no existe.");
            return;
        }

        $semanario = $semanario[0];
		if($semanario['dia'] == 1) $semanario['dia_label'] = 'Lunes';
		if($semanario['dia'] == 2) $semanario['dia_label'] = 'Martes';
		if($semanario['dia'] == 3) $semanario['dia_label'] = 'Miercoles';
		if($semanario['dia'] == 4) $semanario['dia_label'] = 'Jueves';
		if($semanario['dia'] == 5) $semanario['dia_label'] = 'Viernes';
		if($semanario['dia'] == 6) $semanario['dia_label'] = 'Sabado';
		if($semanario['dia'] == 7) $semanario['dia_label'] = 'Domingo';

        //Guardamos los datos
        $GLOBALS['resultado']->_result = $semanario;
    }

	/**
	* @nombre: Lista los Semanarios de un Horario
	* @descripcion: Lista los Semanarios de un Horario
	*/
	public function semanario_listar(){
		//Lista de Usuarios
		$stmt = $GLOBALS['conf']['pdo']->query("	SELECT 	*,
															TIME_FORMAT(hora_inicio, '%h:%i %p') as hora_inicio_label,
															TIME_FORMAT(hora_fin, '%h:%i %p') as hora_fin_label 
													FROM 	horarios_semanario 
													WHERE 	horario=".$GLOBALS['parametros']['horario']);
		$semanario =  $stmt->fetchAll(PDO::FETCH_ASSOC);

		foreach($semanario as $k=>$v){
			if($v['dia'] == 1) $semanario[$k]['dia_label'] = 'Lunes';
			if($v['dia'] == 2) $semanario[$k]['dia_label'] = 'Martes';
			if($v['dia'] == 3) $semanario[$k]['dia_label'] = 'Miercoles';
			if($v['dia'] == 4) $semanario[$k]['dia_label'] = 'Jueves';
			if($v['dia'] == 5) $semanario[$k]['dia_label'] = 'Viernes';
			if($v['dia'] == 6) $semanario[$k]['dia_label'] = 'Sabado';
			if($v['dia'] == 7) $semanario[$k]['dia_label'] = 'Domingo';
		}

		$GLOBALS['resultado']->_result = $semanario;
	}

    /**
     * @nombre: Agregar un Semanarios a un Horario
     * @descripcion: Agregar un Semanarios a un Horario
     */
    public function semanario_agregar(){
        $stmt = $GLOBALS['conf']['pdo']->prepare("INSERT INTO horarios_semanario (horario,dia,hora_inicio,hora_fin) VALUES (".$GLOBALS['parametros']['horario'].",".$GLOBALS['parametros']['dia'].",'".$GLOBALS['parametros']['hora_inicio']."','".$GLOBALS['parametros']['hora_fin']."')");
        $items = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $stmt->execute();
    }

    /**
     * @nombre: Edita un Semanario de un Horario
     * @descripcion: Edita un Semanario de un Horario
     */
    public function semanario_editar(){
        //Obtenemos los usuarios
        $stmt = $GLOBALS['conf']['pdo']->prepare(" 	UPDATE 	horarios_semanario 
        											SET 	dia=".$GLOBALS['parametros']['dia'].", 
        													hora_inicio='".$GLOBALS['parametros']['hora_inicio']."', 
        													hora_fin='".$GLOBALS['parametros']['hora_fin']."'
        											WHERE 	id=".$GLOBALS['parametros']['semanario']);
        $stmt->execute();
    }

    /**
     * @nombre: Eliminar un Semanario de un Horario
     * @descripcion: Elimina un Semanario de un Horario
     */
    public function semanario_eliminar(){
        //Obtenemos los usuarios
        $stmt = $GLOBALS['conf']['pdo']->prepare("DELETE FROM horarios_semanario WHERE id = ".$GLOBALS['parametros']['semanario']);
        $stmt->execute();
    }




    //////////////////////////////////////////////
    //              VACACIONES                  //
    //////////////////////////////////////////////
    
    /**
    * @nombre: Lista las Vacaciones de un Usuario
    * @descripcion: Lista las Vacaciones de un Usuario
    */
    public function vacaciones_listar(){
        //SQL
        $stmt = $GLOBALS['conf']['pdo']->query("    SELECT  id,
                                                            DATE_FORMAT(inicio,'%d-%m-%Y') as inicio,
                                                            DATE_FORMAT(fin,'%d-%m-%Y') as fin 
                                                    FROM    horarios_vacaciones 
                                                    WHERE   usuario='".$GLOBALS['parametros']['usuario']."'");
        $datos =  $stmt->fetchAll(PDO::FETCH_ASSOC);

        $stmt = $GLOBALS['conf']['pdo']->query("SELECT * FROM usuarios WHERE usuario='".$GLOBALS['parametros']['usuario']."'");
        $usuario =  $stmt->fetchAll(PDO::FETCH_ASSOC);

        $GLOBALS['resultado']->_result['nombre'] = $usuario[0]['nombre'];
        $GLOBALS['resultado']->_result['usuario'] = $usuario[0]['usuario'];
        $GLOBALS['resultado']->_result['vacaciones'] = $datos;
    }

    /**
     * @nombre: Información de las Vacaciones de un Usuario
     * @descripcion: Información de las Vacaciones de un Usuario
     */
    public function vacaciones_info(){

        //Obtenemos los usuarios
        $stmt = $GLOBALS['conf']['pdo']->query("    SELECT  id,
                                                            DATE_FORMAT(inicio,'%d-%m-%Y') as inicio,
                                                            DATE_FORMAT(fin,'%d-%m-%Y') as fin 
                                                    FROM    horarios_vacaciones 
                                                    WHERE   id = ".$GLOBALS['parametros']['vacaciones']);
        $datos =  $stmt->fetch(PDO::FETCH_ASSOC);

        //Guardamos los datos
        $GLOBALS['resultado']->_result = $datos;
    }

    /**
     * @nombre: Agregar Vacaciones a un Usuario
     * @descripcion: Agregar Vacaciones a un Usuario
     */
    public function vacaciones_agregar(){

        //Revisamos formatos de las fechas...
        //Validamos las fechas
        $inicio = array_map('intval', explode('/',$GLOBALS['parametros']['inicio']));
        $fin = array_map('intval', explode('/',$GLOBALS['parametros']['fin']));

        //Error de formato
        if(!checkdate($inicio[1],$inicio[0],$inicio[2])){
            //Agregamos error 
            $GLOBALS['resultado']->setError("El rango de Fechas tiene un formato incorrecto.");
            return;
        }

        //Error de formato
        if(!checkdate($fin[1],$fin[0],$fin[2])){
            //Agregamos error 
            $GLOBALS['resultado']->setError("El rango de Fechas tiene un formato incorrecto.");
            return;
        }

        $fecha_inicio = strtotime($inicio[1].'/'.$inicio[0].'/'.$inicio[2]);
        $fecha_fin = strtotime($fin[1].'/'.$fin[0].'/'.$fin[2]);

        //Error de rango
        if($fecha_inicio > $fecha_fin){
            //Agregamos error 
            $GLOBALS['resultado']->setError("El rango de fechas es incorrecto.");
            return;
        }



        //SQL
        $stmt = $GLOBALS['conf']['pdo']->prepare("  INSERT INTO     horarios_vacaciones (usuario, inicio, fin) 
                                                    VALUES          ('".$GLOBALS['parametros']['usuario']."','".date("Y-m-d",$fecha_inicio)."','".date("Y-m-d",$fecha_fin)."')");
        
        $items = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $stmt->execute();
    }

    /**
     * @nombre: Edita las Vacaciones de un Usuario
     * @descripcion: Edita las Vacaciones de un Usuario
     */
    public function vacaciones_editar(){

        //Revisamos formatos de las fechas...
        //Validamos las fechas
        $inicio = array_map('intval', explode('/',$GLOBALS['parametros']['inicio']));
        $fin = array_map('intval', explode('/',$GLOBALS['parametros']['fin']));

        //Error de formato
        if(!checkdate($inicio[1],$inicio[0],$inicio[2])){
            //Agregamos error 
            $GLOBALS['resultado']->setError("El rango de Fechas tiene un formato incorrecto.");
            return;
        }

        //Error de formato
        if(!checkdate($fin[1],$fin[0],$fin[2])){
            //Agregamos error 
            $GLOBALS['resultado']->setError("El rango de Fechas tiene un formato incorrecto.");
            return;
        }

        $fecha_inicio = strtotime($inicio[1].'/'.$inicio[0].'/'.$inicio[2]);
        $fecha_fin = strtotime($fin[1].'/'.$fin[0].'/'.$fin[2]);

        //Error de rango
        if($fecha_inicio > $fecha_fin){
            //Agregamos error 
            $GLOBALS['resultado']->setError("El rango de fechas es incorrecto.");
            return;
        }

        //Obtenemos los usuarios
        $stmt = $GLOBALS['conf']['pdo']->prepare("  UPDATE  horarios_vacaciones 
                                                    SET     inicio='".date("Y-m-d",$fecha_inicio)."', 
                                                            fin='".date("Y-m-d",$fecha_fin)."' 
                                                    WHERE   id=".$GLOBALS['parametros']['vacaciones']);
        $stmt->execute();
    }

    /**
     * @nombre: Eliminar las Vacaciones de un Usuario
     * @descripcion: Elimina las Vacaciones de un Usuario
     */
    public function vacaciones_eliminar(){
        //Obtenemos los usuarios
        $stmt = $GLOBALS['conf']['pdo']->prepare("DELETE FROM horarios_vacaciones WHERE id = ".$GLOBALS['parametros']['vacaciones']);
        $stmt->execute();
    }



    //////////////////////////////////////////////
    //               FERIADOS                   //
    //////////////////////////////////////////////

    /**
    * @nombre: Lista los Feriados
    * @descripcion: Lista los Feriados
    */
    public function feriados_listar(){
        //SQL
        $stmt = $GLOBALS['conf']['pdo']->query("    SELECT  nombre,
                                                            dia,
                                                            DATE_FORMAT(dia,'%d-%m-%Y') as dia_label,
                                                            (TIME_FORMAT(hora_inicio, '%h:%i %p')) as hora_inicio,
                                                            (TIME_FORMAT(hora_fin, '%h:%i %p')) as hora_fin
                                                    FROM    horarios_feriados
                                                    ORDER BY dia ASC");
        $datos =  $stmt->fetchAll(PDO::FETCH_ASSOC);
        $GLOBALS['resultado']->_result = $datos;
    }

    /**
     * @nombre: Información de un Feriado
     * @descripcion: Información de un Feriado
     */
    public function feriados_info(){

        //Obtenemos los usuarios
        $stmt = $GLOBALS['conf']['pdo']->query("    SELECT  id,
                                                            DATE_FORMAT(inicio,'%d-%m-%Y') as inicio,
                                                            DATE_FORMAT(fin,'%d-%m-%Y') as fin 
                                                    FROM    horarios_feriados 
                                                    WHERE   id = ".$GLOBALS['parametros']['feriado']);
        $datos =  $stmt->fetch(PDO::FETCH_ASSOC);

        //Guardamos los datos
        $GLOBALS['resultado']->_result = $datos;
    }

    /**
     * @nombre: Agregar Feriado
     * @descripcion: Agregar Feriado
     */
    public function feriados_agregar(){

        //Revisamos formatos de las fechas...
        //Validamos las fechas
        $dia = array_map('intval', explode('/',$GLOBALS['parametros']['dia']));

        //Error de formato
        if(!checkdate($dia[1],$dia[0],$dia[2])){
            //Agregamos error 
            $GLOBALS['resultado']->setError("El dia tiene un formato incorrecto.");
            return;
        }

        $dia = strtotime($dia[1].'/'.$dia[0].'/'.$dia[2]);

        $hora_inicio = $GLOBALS['parametros']['hora_inicio'];
        $hora_fin = $GLOBALS['parametros']['hora_fin'];

        if($hora_inicio == '0') $inicio = "NULL";
        if(preg_match("/(2[0-4]|[01][1-9]|10):([0-5][0-9])/", $hora_inicio)) $inicio = "'".$hora_inicio.":00'";
        if(!isset($inicio)){
            $GLOBALS['resultado']->setError("La hora de inicio de actividades es incorrecta");
            return;
        }

        if($hora_fin == '0') $fin = "NULL";
        if(preg_match("/(2[0-4]|[01][1-9]|10):([0-5][0-9])/", $hora_fin)) $fin = "'".$hora_fin.":00'";
        if(!isset($fin)){
            //Agregamos error 
            $GLOBALS['resultado']->setError("La hora de fin de actividades es incorrecta");
            return;
        }

        //SQL
        $stmt = $GLOBALS['conf']['pdo']->prepare("  INSERT INTO     horarios_feriados (nombre, dia, hora_inicio, hora_fin) 
                                                    VALUES          ('".$GLOBALS['parametros']['nombre']."','".date("Y-m-d",$dia)."',".$inicio.",".$fin.")");
        
        $items = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $stmt->execute();
    }

    /**
     * @nombre: Edita un Feriado
     * @descripcion: Edita un Feriado
     */
    public function feriados_editar(){

        //Revisamos formatos de las fechas...
        //Validamos las fechas
        $inicio = array_map('intval', explode('/',$GLOBALS['parametros']['inicio']));
        $fin = array_map('intval', explode('/',$GLOBALS['parametros']['fin']));

        //Error de formato
        if(!checkdate($inicio[1],$inicio[0],$inicio[2])){
            //Agregamos error 
            $GLOBALS['resultado']->setError("El rango de Fechas tiene un formato incorrecto.");
            return;
        }

        //Error de formato
        if(!checkdate($fin[1],$fin[0],$fin[2])){
            //Agregamos error 
            $GLOBALS['resultado']->setError("El rango de Fechas tiene un formato incorrecto.");
            return;
        }

        $fecha_inicio = strtotime($inicio[1].'/'.$inicio[0].'/'.$inicio[2]);
        $fecha_fin = strtotime($fin[1].'/'.$fin[0].'/'.$fin[2]);

        //Error de rango
        if($fecha_inicio > $fecha_fin){
            //Agregamos error 
            $GLOBALS['resultado']->setError("El rango de fechas es incorrecto.");
            return;
        }

        //Obtenemos los usuarios
        $stmt = $GLOBALS['conf']['pdo']->prepare("  UPDATE  horarios_vacaciones 
                                                    SET     inicio='".date("Y-m-d",$fecha_inicio)."', 
                                                            fin='".date("Y-m-d",$fecha_fin)."' 
                                                    WHERE   id=".$GLOBALS['parametros']['vacaciones']);
        $stmt->execute();
    }

    /**
     * @nombre: Eliminar un Feriado
     * @descripcion: Elimina un Feriado
     */
    public function feriados_eliminar(){

        //Revisamos formatos de las fechas...
        //Validamos las fechas
        $dia = array_map('intval', explode('/',$GLOBALS['parametros']['dia']));

        //Error de formato
        if(!checkdate($dia[1],$dia[0],$dia[2])){
            //Agregamos error 
            $GLOBALS['resultado']->setError("El dia tiene un formato incorrecto.");
            return;
        }

        $dia = strtotime($dia[1].'/'.$dia[0].'/'.$dia[2]);

        //Obtenemos los usuarios
        $stmt = $GLOBALS['conf']['pdo']->prepare("DELETE FROM horarios_feriados WHERE dia = '".date("Y-m-d",$dia)."'");
        $stmt->execute();
    }


    //////////////////////////////////////////////
    //    FUNCIONES COMPLEMENTARIAS             //
    //////////////////////////////////////////////



    /**
     * @nombre: Lista Horarios (select)
     * @descripcion: Listado de Horarios para popular el Select2.
     */
    public function horarios_lista_select(){

        //Separamos las palabras clave en un arreglo
        $keyword_tokens = explode(' ', $GLOBALS['parametros']['q']);

        //Calculate paging
        $page =  (intval($GLOBALS['parametros']['p'])-1) * 40;

        //SQL
        $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT id, nombre FROM horarios");
        $stmt->execute();
        $items = $stmt->fetchAll(PDO::FETCH_ASSOC);

        //Agregamos 'Sin Horario'
		array_unshift($items, array("id"=>0,"nombre"=>"Sin Horario"));

        //Si estamos haciendo una busqueda..
        if($keyword_tokens[0] != '') 
        	$items = $GLOBALS['toolbox']->matchingArray($items,'nombre',$keyword_tokens);

        //Calculamos totales
        $GLOBALS['resultado']->_result['total'] = count($items);

        //Devolvemos items y total
        $GLOBALS['resultado']->_result['items'] = array_slice($items, $page, 40);
    }


}
?>