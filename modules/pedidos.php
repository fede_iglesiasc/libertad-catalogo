<?php
/**
 * @nombre: Pedidos
 * @descripcion: Administrador de Pedidos.
 */
class pedidos extends module{

    /*
	 * Constructor
	 */
    public function __construct(){
    }

    /**
     * Validaciones previas
     */
    public function __pre($accion){

        //llamamos a la accion
        return call_user_func_array(array($this, $accion), array());
    }


    /**
     * @nombre: Listar Pedido Pendiente
     * @descripcion: Listar Pedido Pendiente
     */
    public function listar(){

        //Usuario
        $usuario = $GLOBALS['session']->getData('usuario');
        $articulos = array();


        //Obtenemos el pedido pendiente
        $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT * FROM pedidos WHERE (cliente = '".$usuario."') AND (fecha_creacion IS NULL)");
        $stmt->execute();
        $pedido = $stmt->fetchAll(PDO::FETCH_ASSOC);
        

        //Si tenemos el pedido Pendiente traemos los Articulos
        if($stmt->rowCount()){
            $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT  articulo,
            (SELECT CONCAT(prefijo,'-',codigo,'-',sufijo) FROM catalogo_articulos WHERE catalogo_articulos.id = articulo) as codigo,
            (SELECT nombre_singular FROM catalogo_rubros WHERE id = (SELECT id_rubro FROM catalogo_articulos WHERE catalogo_articulos.id = articulo)) as rubro,
            (SELECT descripcion FROM catalogo_articulos WHERE catalogo_articulos.id = articulo) as descripcion,
            (SELECT nombre FROM catalogo_marcas WHERE id = (SELECT id_marca FROM catalogo_articulos WHERE catalogo_articulos.id = articulo)) as fabricante,
            (SELECT GROUP_CONCAT(
                CONCAT  ((IF(marca_id IS NULL,'Universal (Cualquier vehículo)',(SELECT nombre FROM catalogo_autos_marca WHERE id = marca_id))),' ',
                        (IF(modelo_id IS NULL,'(Todos los modelos)',(SELECT nombre FROM catalogo_autos_modelo WHERE id = modelo_id))), ' ',
                        (IF(version_id IS NULL,'',(SELECT descripcion FROM catalogo_autos_versiones WHERE id = version_id))), ' ',
                        (CONCAT ( IF( anio_inicio = -1 AND anio_final = -1, '', CONCAT( IF( anio_inicio = -1, '[--', CONCAT('[',SUBSTRING(anio_inicio,3,2))), ' / ', IF( anio_final = -1, '--]', CONCAT(SUBSTRING(anio_final,3,2),']')))))) ) SEPARATOR '{{sep}}')
            FROM    catalogo_articulos_aplicaciones as g
            WHERE   g.articulo_id = p.articulo) as aplicaciones,
            cantidad
            FROM    pedidos_articulos p
            WHERE   pedido = (SELECT id FROM pedidos t WHERE (t.cliente = '".$usuario."') AND (t.fecha_creacion IS NULL))");
            $stmt->execute();
            $articulos = $stmt->fetchAll(PDO::FETCH_ASSOC);

        //Sino creamos un nuevo pedido
        }else{
            $stmt = $GLOBALS['conf']['pdo']->prepare("INSERT INTO pedidos(cliente) VALUES ('".$usuario."')");
            $stmt->execute();
        }

        //Asignamos resultados
        $GLOBALS['resultado']->_result['articulos'] = $articulos;
        $GLOBALS['resultado']->_result['notas'] = $pedido[0]['notas'];
    }

    /**
     * @nombre: Eliminar Articulo del Pedido
     * @descripcion: Eliminar Articulo del Pedido
     */
    public function eliminar(){
        
        //Usuario
        $usuario = $GLOBALS['session']->getData('usuario');

        //ID del Articulo
        $articulo = $GLOBALS['parametros']['articulo'];

        //Obtenemos el pedido pendiente
        $stmt = $GLOBALS['conf']['pdo']->prepare("  DELETE 
                                                    FROM    pedidos_articulos 
                                                    WHERE   (articulo = ".$articulo.") 
                                                            AND 
                                                            (pedido = ( SELECT  id 
                                                                        FROM    pedidos 
                                                                        WHERE   (pedidos.cliente = '".$usuario."') 
                                                                                AND (pedidos.fecha_creacion IS NULL)))");
        $stmt->execute();
    }

    /**
     * @nombre: Guarda el Pedido
     * @descripcion: Guarda el Pedido
     */
    public function guardar(){
        
        //Usuario
        $usuario = $GLOBALS['session']->getData('usuario');

        //Obtenemos el pedido pendiente
        $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT * FROM pedidos WHERE (cliente = '".$usuario."') AND (fecha_creacion IS NULL)");
        $stmt->execute();
        $pedido = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $pedido = $pedido[0];

        //SQL query
        $sql = "UPDATE pedidos SET notas='".$GLOBALS['parametros']['notas']."' WHERE id=".$pedido['id'].";";

        //Validamos los articulos
        if( is_array($GLOBALS['parametros']['articulos']) ){
            
            //Recorremos el arreglo..
            foreach ($GLOBALS['parametros']['articulos'] as $k=>$v)
                $sql .= "UPDATE pedidos_articulos SET cantidad=".$v['cantidad']." WHERE (pedido=".$pedido['id'].") AND (articulo=".$v['id'].");";
            
        //No es un array    
        }else{
            $GLOBALS['resultado']->setError("Error de parametros. no es objeto");
            return;
        }            

        //Cometemos los cambios en la DB
        $stmt = $GLOBALS['conf']['pdo']->query($sql);
    }

    /**
     * @nombre: Agregar Articulo al Pedido
     * @descripcion: Agregar Articulo al Pedido
     */
    public function agregar(){
        //Usuario
        $usuario = $GLOBALS['session']->getData('usuario');

        //ID del Articulo
        $articulo = $GLOBALS['parametros']['articulo'];

        //ID del Articulo
        $cantidad = $GLOBALS['parametros']['cantidad'];

        //Confirmar
        //$confirmacion = $GLOBALS['parametros']['confirmacion'];

        //Obtenemos el pedido pendiente
        $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT * FROM pedidos WHERE (cliente = '".$usuario."') AND (fecha_creacion IS NULL)");
        $stmt->execute();
        $pedido = $stmt->fetchAll(PDO::FETCH_ASSOC);

        //Si no existe un pedido
        //pendiente lo creamos
        if(!$stmt->rowCount()){
            $stmt = $GLOBALS['conf']['pdo']->prepare("INSERT INTO pedidos(cliente) VALUES ('".$usuario."')");
            $stmt->execute();

            //Obtenemos el nuevo pedido pendiente
            $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT * FROM pedidos WHERE (cliente = '".$usuario."') AND (fecha_creacion IS NULL)");
            $stmt->execute();
            $pedido = $stmt->fetchAll(PDO::FETCH_ASSOC);
        }

        //Obtenemos la ID
        $pedido_id = $pedido[0]['id'];

        //Existe el articulo ya agregado al pedido?
        $existe_cantidad = 0;
        $stmt = $GLOBALS['conf']['pdo']->query("SELECT * FROM pedidos_articulos WHERE pedido = " . $pedido_id . " AND articulo = " . $articulo);
        $existe = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if(count($existe)) $existe_cantidad = (int)$existe[0]['cantidad'];
        $GLOBALS['resultado']->_result['existe'] = $existe_cantidad;
        if(count($existe)) return;

        //Obtenemos el pedido pendiente
        $stmt = $GLOBALS['conf']['pdo']->prepare("  INSERT INTO     pedidos_articulos (pedido, articulo, cantidad) 
                                                    VALUES          (" . $pedido_id . "," . $articulo . "," . $cantidad . ") 
                                                    ON DUPLICATE KEY UPDATE cantidad = ". $cantidad );
        $stmt->execute();
    }

    /**
     * @nombre: Descarta el Pedido
     * @descripcion: Descarta el Pedido
     */
    public function descartar(){
        
        //Usuario
        $usuario = $GLOBALS['session']->getData('usuario');

        //Boramos los articulos pendientes
        $stmt = $GLOBALS['conf']['pdo']->prepare("  DELETE 
                                                    FROM    pedidos_articulos 
                                                    WHERE   pedido = ( SELECT  id 
                                                                        FROM    pedidos 
                                                                        WHERE   (pedidos.cliente = '".$usuario."') 
                                                                                AND (pedidos.fecha_creacion IS NULL))");
        $stmt->execute();

        //Borramos las notas
        $stmt = $GLOBALS['conf']['pdo']->prepare("  UPDATE  pedidos 
                                                    SET     notas='' 
                                                    WHERE   (cliente = '".$usuario."') 
                                                            AND (fecha_creacion IS NULL)");
        $stmt->execute();
    }



    /**
     * @nombre: Confirma el Pedido
     * @descripcion: Confirmo el Pedido
     */
    public function confirmar(){

        //Incluimos el generador del PDF
        include("./classes/mpdf57/mpdf.php");

        //Usuario
        $usuario = $GLOBALS['session']->getData('usuario');

        //Obtenemos el pedido pendiente
        $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT * FROM pedidos WHERE (cliente = '".$usuario."') AND (fecha_creacion IS NULL)");
        $stmt->execute();
        $pedido = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $pedido = $pedido[0];
        $pedido['notas'] = $GLOBALS['parametros']['notas'];

        //Ultimo comprobante generado...
        $stmt = $GLOBALS['conf']['pdo']->query("SELECT comprobante FROM pedidos ORDER BY comprobante DESC LIMIT 1");
        $comprobante = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $comprobante = $comprobante[0]['comprobante'] + 1;


        //SQL query
        $sql = "UPDATE pedidos SET notas='".$GLOBALS['parametros']['notas']."', comprobante=".$comprobante." WHERE id=".$pedido['id'].";";

        //Validamos los articulos
        if( is_array($GLOBALS['parametros']['articulos']) ){
            
            //Recorremos el arreglo..
            foreach ($GLOBALS['parametros']['articulos'] as $k=>$v)
                $sql .= "UPDATE pedidos_articulos SET cantidad=".$v['cantidad']." WHERE (pedido=".$pedido['id'].") AND (articulo=".$v['id'].");";
            
        //No es un array    
        }else{
            $GLOBALS['resultado']->setError("Error de parametros. no es objeto");
            return;
        }            

        //Cometemos los cambios en la DB
        $stmt = $GLOBALS['conf']['pdo']->query($sql);


        ////////////////////////////////////////////////////////////////
        //  NOTIFICACION PARA LUCHO Y GABRIEL                        ///
        ////////////////////////////////////////////////////////////////
        //Creamos la notificacion
        $stmt = $GLOBALS['conf']['pdo']->query("    INSERT INTO     notificaciones (fecha, titulo, mensaje) 
                                                    VALUES          (NOW(), 'Nuevo Pedido', 'El cliente ".$usuario." ha enviado un nuevo pedido.')");
 
        $notificacion_id = $GLOBALS['conf']['pdo']->lastInsertId();

        //Notificaciones pendientes
        $stmt = $GLOBALS['conf']['pdo']->query("    INSERT INTO     notificaciones_pendientes (notificacion_id, usuario_id) 
                                                    VALUES          (".$notificacion_id.",'luciano'), 
                                                                    (".$notificacion_id.",'gabriel'), 
                                                                    (".$notificacion_id.",'eduardo')");
        

        //OBtenemos todos los datos del Cliente
        $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT  
                        (SELECT razon_social FROM clientes WHERE usuario = '".$usuario."') as razon_social,
                        (SELECT persona_de_contacto FROM clientes WHERE usuario = '".$usuario."') as persona_de_contacto,
                        CONCAT( (SELECT domicilio FROM clientes WHERE usuario = '".$usuario."'),
                                ', ',
                                (SELECT nombre FROM sys_localidades WHERE id = (    SELECT  localidad 
                                                                                    FROM    clientes 
                                                                                    WHERE   usuario = '".$usuario."')),
                                ', ',
                                (SELECT nombre FROM sys_provincias WHERE id = (     SELECT  provincia_id 
                                                                                    FROM    sys_localidades 
                                                                                    WHERE   id = (  SELECT  localidad 
                                                                                                    FROM    clientes 
                                                                                                    WHERE   usuario = '".$usuario."')))
                        )as domicilio,
                        (SELECT telefono FROM clientes WHERE usuario = '".$usuario."') as telefono,
                        (SELECT nombre_del_comercio FROM clientes WHERE usuario = '".$usuario."') as nombre_del_comercio,
                        email
                FROM usuarios
                WHERE usuario = '".$usuario."'");

        //Cliente
        $stmt->execute();
        $cliente = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $cliente = $cliente[0];



        $sql = "SELECT  articulo,
        (SELECT CONCAT(prefijo,'-',codigo,'-',sufijo) FROM catalogo_articulos WHERE catalogo_articulos.id = articulo) as codigo,
        (SELECT nombre_singular FROM catalogo_rubros WHERE id = (SELECT id_rubro FROM catalogo_articulos WHERE catalogo_articulos.id = articulo)) as rubro,
        (SELECT descripcion FROM catalogo_articulos WHERE catalogo_articulos.id = articulo) as descripcion,
        (SELECT nombre FROM catalogo_marcas WHERE id = (SELECT id_marca FROM catalogo_articulos WHERE catalogo_articulos.id = articulo)) as fabricante,
        (SELECT GROUP_CONCAT(
            CONCAT  ((IF(marca_id IS NULL,'Universal (Cualquier vehículo)',(SELECT nombre FROM catalogo_autos_marca WHERE id = marca_id))),' ',
                    (IF(modelo_id IS NULL,'(Todos los modelos)',(SELECT nombre FROM catalogo_autos_modelo WHERE id = modelo_id))), ' ',
                    (IF(version_id IS NULL,'',(SELECT descripcion FROM catalogo_autos_versiones WHERE id = version_id))), ' ',
                    (CONCAT ( IF( anio_inicio = -1 AND anio_final = -1, '', CONCAT( IF( anio_inicio = -1, '[--', CONCAT('[',SUBSTRING(anio_inicio,3,2))), ' / ', IF( anio_final = -1, '--]', CONCAT(SUBSTRING(anio_final,3,2),']')))))) ) SEPARATOR '######SEPARATOR######')
        FROM    catalogo_articulos_aplicaciones as g
        WHERE   g.articulo_id = p.articulo) as aplicaciones,
        cantidad
        FROM    pedidos_articulos p
        WHERE   pedido = (SELECT id FROM pedidos t WHERE (t.cliente = '".$usuario."') AND (t.fecha_creacion IS NULL));";


        $GLOBALS['conf']['pdo']->query("SET group_concat_max_len = 15000");
        $stmt = $GLOBALS['conf']['pdo']->query($sql);
        $articulos = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $id = str_pad($pedido['id'], 6, '0', STR_PAD_LEFT);
        $fecha = date("d").'/'.date("m").'/'.date("Y");




        //Encabezado
        $html = <<<EOD
        <!doctype html>
        <html>
            <head>
                <meta charset="utf-8">
            </head>
            <body style="font-family: 'opensans', sans-serif;">
            
            <!-- Cabecera -->
            <table width="100%" cellspacing="0" cellpadding="0">
                <tr>
                    <td>
                        <img src="./img/logo_1.png" width="38%"/>
                        <p style="font-size: 9px; white-space: nowrap; margin: 0px 0px 0px 10px;">
                            Chile 2019 (B7604-FHI) - Mar del Plata. Buenos Aires. Argentina.<br>
                            TEL: (0223) 474-1222 | FAX: (0223) 475-7170 / 0800-333-6590<br>
                            PEDIDOS: pedidos@distribuidoralibertad.com
                        </p>
                    </td>
                    <td width="220"></td>
                    <td align="right" style="white-space: nowrap; vertical-align: top; padding: 10px 10px 0px 0px; font-size: 12px; font-weight: bold;">
                        <span style="text-align: center; font-size: 18px; font-family: arial black; text-transform: uppercase;">NOTA DE PEDIDO</span><br><br>
                        REF. NRO.:  [[id]]<br>FECHA: [[fecha]]
                    </td>
                </tr>

            </table>

            <!-- Datos del Cliente -->
            <table width="100%" cellspacing="0" cellpadding="0" style="margin-top: 0px;">
                <tr>
                    <td style="height: 20px;"></td>
                </tr>
                <tr>
                    <td style="background: #f4f4f4; color: #656565; padding: 5px 10px 0px 10px; font-size: 12px;"><b>[[cliente]]  -  [[razon_social]]</b></td>
                </tr>
                <tr>
                    <td style="background: #f4f4f4; color: #656565; padding: 1px 10px 5px 10px; font-size: 10px; text-transform: uppercase;">
                        [[domicilio]]   -   TEL: [[telefono]]
                    </td>
                </tr>
            </table>

            <!-- Articulos -->
            <table width="100%" cellspacing="0" cellpadding="0" class="lista_precios">
                <tr> 
                    <td class="lista_precios_encabezado" width="50">Cant.</td>
                    <td class="lista_precios_encabezado" width="150">Código</td>
                    <td class="lista_precios_encabezado" style="text-align: left;">Descripción</td>
                </tr>
                [[articulos]]
            </table>


            <!-- Notas -->
            <table class="notas">
                <tr>
                    <td style="font-size: 1em; padding: 10px; border: #edecec 1px solid; color: #3d4040; vertical-align: top;" width="50%">
                        <i>Notas / Transporte</i><br>
                        <i style="font-size: 0.75em; height: 134px; padding: 10px; vertical-align: top;">[[notas]]</i>
                    </td>

                    <td style="font-size: 12px; color: #69a3c2; padding: 0px 10px 10px 10px;" width="50%"><i>Estimado cliente, si desea cancelar este pedido o hacer modificaciones al mismo podrá hacerlo comunicandose telefonicamente al <b>(0223) 4741222</b>, o bien enviando un email a <b>pedidos@distribuidoralibertad.com</b>. Tenga en cuenta que de no informar modificaciones / cancelaciones el pedido sera despachado directamente a su domicilio.</i></td>
                </tr>
            </table>
EOD;



        $css = <<<EOD

                     .lista_precios{
                        margin-top: 10px;
                        margin-bottom: 40px;
                        margin-left: 0px;
                        border: #edecec 0px solid; 
                        -moz-border-radius: 10px; 
                        -webkit-border-radius: 10px; 
                        border-radius: 10px; 
                        text-align: center;
                    }

                    .lista_precios_encabezado{
                        height: 25px; 
                        text-align: 
                        center; color: #ffffff;
                        padding: 0px 10px 0px 10px;
                        background: -webkit-linear-gradient(top, #444748 0%, #37393a 100%); 
                        background-attachment:fixed; 
                        color: #ffffff;
                        text-transform: uppercase;
                        font-size: 12px;
                        font-weight: bold;
                    }

                    .lista_precios_celda{
                        color: #656565;
                        font-size: 12px;
                        height: 30px;
                        background: #f6f6f6;
                        padding: 4px 10px 4px 10px;
                    }

                    .lista_precios_espaciador{
                        height: 2px;
                        background: #ffffff;
                    }

                    .a_la_izq{
                        text-align: left;
                    }

                    .a_la_der{
                        text-align: right;
                    }

                    .notas{
                        width: 100%;
                        text-align: left;
                    }

                    .detalle{
                        display: block;
                        width: 100%;
                        float: left;
                    }

                    .detalle_encabezado{
                        height: 48px; 
                        text-align: center !important;
                        padding: 0px 10px 0px 10px  !important;
                        background: -webkit-linear-gradient(top, #6da7c6 0%, #3d9acb 100%)  !important; 
                        background-attachment:fixed  !important; 
                        color: #ffffff  !important;
                    }

                    .detalle_espaciador td{
                        height: 3px;
                        background: #ffffff;
                    }

                    .detalle_fila td{
                        height: 40px;
                        background: #f6f6f6;
                        padding: 0px 10px 0px 10px;
                        text-align: right;
                    }

                    .aplicaciones{
                        padding-top: 10px;
                        font-weight: bold;
                        font-style: italic;
                        font-size: 10px;
                    }

                    .publicidades tr td{
                        text-align: center;
                    }
EOD;


        //Numero de Cliente
        $html = str_replace("[[cliente]]", $usuario, $html);

        //Razon Social
        $html = str_replace("[[razon_social]]", $cliente['razon_social'], $html);

        //Domicilio
        $html = str_replace("[[domicilio]]", $cliente['domicilio'], $html);

        //Telefono
        $html = str_replace("[[telefono]]", $cliente['telefono'], $html);

        //id
        $html = str_replace("[[id]]", $comprobante, $html );

        //fecha
        $html = str_replace("[[fecha]]", $fecha, $html);

        //Notas
        $html = str_replace("[[notas]]", $pedido['notas'], $html);


        //Articulos
        $html_tmp = '';
        foreach ($articulos as $k=>$a){
            
            $a['aplicaciones'] = str_replace("######SEPARATOR######", ' <img src="./img/bullet.png" style="margin: 0px 7px 2px 7px;"> ', $a['aplicaciones']);

            $html_tmp .= '<tr><td class="lista_precios_espaciador" colspan="3"></td></tr><tr>';
            $html_tmp .= '<td class="lista_precios_celda" width="50"><b>'.$a['cantidad'].'</b></td>';
            $html_tmp .= '<td class="lista_precios_celda" width="150"><b>'.$a['codigo'].'</b></td>';
            $html_tmp .= '<td class="lista_precios_celda a_la_izq">' .$a['rubro'] .' '. $a['descripcion'] .' '. $a['fabricante'];
            $html_tmp .= '<img src="./img/separador.png" style="width: 300px; height: 10px;"><p class="aplicaciones"><img src="./img/bullet.png" style="margin: 0px 7px 2px 0px;">'.$a['aplicaciones'].'</p></td></tr>';
        }

        //Articulos
        $html = str_replace("[[articulos]]", $html_tmp, $html);

        //Notas
        $html = str_replace("[[notas]]", $pedido['notas'], $html);



        //////////////////////////////////////////////////
        // AGREGAMOS PUBLICIDAD INTELIGENTE
        /////////////////////////////////////////////////

        $sql = "SELECT  id, 
                        promocion_prioridad, 
                        nombre

              FROM      catalogo_marcas as mar 
              WHERE     id NOT IN ( SELECT DISTINCT IF((SELECT id_marca FROM catalogo_articulos WHERE id = articulo) IS NULL,0,(SELECT id_marca FROM catalogo_articulos WHERE id = articulo)) as fabricante 
                                    FROM    pedidos_articulos 
                                    WHERE   pedido IN ( SELECT  id 
                                                        FROM    pedidos 
                                                        WHERE   cliente = '".$usuario."' 
                                                                AND (fecha_creacion > date_sub(curdate(), interval 2 month)))) 

                        AND (padre = 1) 
                        AND promocion_prioridad <> 0
              ORDER BY  promocion_prioridad
              LIMIT     15;";

        $stmt = $GLOBALS['conf']['pdo']->query($sql);
        $promocion_fabricantes = $stmt->fetchAll(PDO::FETCH_ASSOC);

        //Si los fabricantes que trajimos desde la 
        //base de datos no alcanzan a completar 15
        if(count($promocion_fabricantes) < 15){

            //Recorremos en busqueda de los ids que ya se recolectaron
            foreach ($promocion_fabricantes as $k=>$f)
                $fabricantes_existentes[] = $f['id'];
            
            $sql = "SELECT  id, 
                            promocion_prioridad, 
                            nombre, 
                            (   SELECT  COUNT(*) as cnt 
                                FROM    catalogo_articulos as s 
                                WHERE   s.id_marca = mar1.id) as count 

                    FROM    catalogo_marcas as mar1
                    WHERE   (promocion_prioridad <> 0)
                            AND (padre = 1)
                            AND (id NOT IN (".implode(',', $fabricantes_existentes)."))
                    ORDER BY RAND()";

            $stmt = $GLOBALS['conf']['pdo']->query($sql);
            $promocion_fabricantes_aleatorios = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $promocion_fabricantes = $promocion_fabricantes + $promocion_fabricantes_aleatorios;
        }


        //Generamos lista de marcas
        $fila1_publicidad = '<table width="840" class="publicidades"><tr><td width="20%" valign="middle" height="70"><img src="./fabricantes/'.$promocion_fabricantes[0]['id'].'.svg" style="width: 100px; max-height: 55px;"/></td><td width="20%" valign="middle" height="70"><img src="./fabricantes/'.$promocion_fabricantes[1]['id'].'.svg" style="width: 100px; max-height: 55px;"/></td><td width="20%" valign="middle" height="70"><img src="./fabricantes/'.$promocion_fabricantes[2]['id'].'.svg" style="width: 100px; max-height: 55px;"/></td><td width="20%" valign="middle" height="70"><img src="./fabricantes/'.$promocion_fabricantes[3]['id'].'.svg" style="width: 100px; max-height: 55px;"/></td><td width="20%" valign="middle" height="70"><img src="./fabricantes/'.$promocion_fabricantes[4]['id'].'.svg" style="width: 100px; max-height: 55px;"/></td></tr></table>';
        
        $fila2_publicidad = '<table width="840" class="publicidades"><tr><td width="20%" valign="middle" height="70"><img src="./fabricantes/'.$promocion_fabricantes[5]['id'].'.svg" style="width: 100px; max-height: 55px;"/></td><td width="20%" valign="middle" height="70"><img src="./fabricantes/'.$promocion_fabricantes[6]['id'].'.svg" style="width: 100px; max-height: 55px;"/></td><td width="20%" valign="middle" height="70"><img src="./fabricantes/'.$promocion_fabricantes[7]['id'].'.svg" style="width: 100px; max-height: 55px;"/></td><td width="20%" valign="middle" height="70"><img src="./fabricantes/'.$promocion_fabricantes[8]['id'].'.svg" style="width: 100px; max-height: 55px;"/></td><td width="20%" valign="middle" height="70"><img src="./fabricantes/'.$promocion_fabricantes[9]['id'].'.svg" style="width: 100px; max-height: 55px;"/></td></tr></table>';
        
        $fila3_publicidad = '<table width="840" class="publicidades"><tr><td width="20%" valign="middle" height="70"><img src="./fabricantes/'.$promocion_fabricantes[10]['id'].'.svg" style="width: 100px; max-height: 55px;"/></td><td width="20%" valign="middle" height="70"><img src="./fabricantes/'.$promocion_fabricantes[11]['id'].'.svg" style="width: 100px; max-height: 55px;"/></td><td width="20%" valign="middle" height="70"><img src="./fabricantes/'.$promocion_fabricantes[12]['id'].'.svg" style="width: 100px; max-height: 55px;"/></td><td width="20%" valign="middle" height="70"><img src="./fabricantes/'.$promocion_fabricantes[13]['id'].'.svg" style="width: 100px; max-height: 55px;"/></td><td width="20%" valign="middle" height="70"><img src="./fabricantes/'.$promocion_fabricantes[14]['id'].'.svg" style="width: 100px; max-height: 55px;"/></td></tr></table>';
 
        //Creamos el PDF Vacio
        $mpdf_tmp1 =new mPDF('','', 11, '', 11, 10, 10, 10, 0, 0, '');
        $mpdf_tmp1->WriteHTML($css,1);
        $mpdf_tmp1->WriteHTML( $html . $fila1_publicidad . '</body></html>' );
        $contpag1 = $mpdf_tmp1->getPageCount();
        unset($mpdf_tmp1);

        //Creamos el PDF Vacio
        $mpdf_tmp2 =new mPDF('','', 11, '', 11, 10, 10, 10, 0, 0, '');
        $mpdf_tmp2->WriteHTML($css,1);
        $mpdf_tmp2->WriteHTML( $html . $fila1_publicidad . $fila2_publicidad . '</body></html>' );
        $contpag2 = $mpdf_tmp2->getPageCount();
        unset($mpdf_tmp2);

        //Creamos el PDF Vacio
        $mpdf_tmp3 =new mPDF('','', 11, '', 11, 10, 10, 10, 0, 0, '');
        $mpdf_tmp3->WriteHTML($css,1);
        $mpdf_tmp3->WriteHTML( $html . $fila1_publicidad . $fila2_publicidad . $fila3_publicidad . '</body></html>' );
        $contpag3 = $mpdf_tmp3->getPageCount();
        unset($mpdf_tmp3);

        //Creamos el PDF Vacio
        $mpdf_tmp4 =new mPDF('','', 11, '', 11, 10, 10, 10, 0, 0, '');
        $mpdf_tmp4->WriteHTML($css,1);
        $mpdf_tmp4->WriteHTML( $html . '</body></html>' );
        $contpag_inicial = $mpdf_tmp4->getPageCount();
        unset($mpdf_tmp4);

        //Agregamos las publicidades segun espacio disponible
        if($contpag_inicial == $contpag1) $html .= $fila1_publicidad;
        if($contpag_inicial == $contpag2) $html .= $fila2_publicidad;
        if($contpag_inicial == $contpag3) $html .= $fila3_publicidad;


        //////////////////////////////////////////////////
        // FIN DE PUBLICIDAD INTELIGENTE
        /////////////////////////////////////////////////

        //Creamos el PDF Vacio
        $mpdf =new mPDF('','', 11, '', 11, 10, 10, 10, 0, 0, '');
        $mpdf->WriteHTML($css,1);
        $mpdf->WriteHTML($html);
        $contpag = $mpdf->getPageCount();

        //Guardamos el pedido en el directorio de pedidos
        $mpdf->Output('./pedidos/pedido_'.$comprobante.'.pdf','F');
        
        //Actualizamos la fecha del pedido
        $stmt = $GLOBALS['conf']['pdo']->query("UPDATE pedidos SET fecha_creacion=NOW() WHERE id=".$pedido['id']);

        //Actualizamos la fecha del pedido
        $stmt = $GLOBALS['conf']['pdo']->query("INSERT INTO pedidos (cliente) VALUES ('".$usuario."')");
    }

    /**
     * @nombre: Reporte de Pedidos
     * @descripcion: Reporte de Pedidos
     */
    function reporte_listar(){

        //Si no nos pasan el cliente listamos los ultimos 150 pedidos
        $sql = "SELECT      id,
                            cliente,
                            DATE_FORMAT(fecha_creacion, '%d-%m-%Y') as fecha,
                            DATE_FORMAT(fecha_creacion, '%H:%i') as hora,
                            (SELECT razon_social FROM clientes WHERE usuario = cliente) as razon_social,
                            comprobante, 
                            impreso
                FROM        pedidos 
                WHERE       (fecha_creacion IS NOT NULL) AND (DATEDIFF(CURDATE(), fecha_creacion) < 14)  
                ORDER BY    comprobante DESC";

        $stmt = $GLOBALS['conf']['pdo']->prepare($sql);
        $stmt->execute();
        $pedidos = $stmt->fetchAll(PDO::FETCH_ASSOC);

        //Asignamos resultados
        $GLOBALS['resultado']->_result['pedidos'] = $pedidos;
    }

    /**
     * @nombre: Reimprimir pedido
     * @descripcion: Reimprimir pedido
     */
    function reporte_reimprimir(){
        //Create Printnode Client
        $GLOBALS['toolbox']->createPrintNodeClient();

        $GLOBALS['toolbox']->imprimePedido($GLOBALS['parametros']['pedido']);
    }

}
?>