<?php
/**
 * @nombre: Aplicaciones
 * @descripcion: Aplicaciones de Vehículos a los Artículos
 */
class aplicaciones  extends module{


    /*
	 * Constructor
	 */
    public function __construct(){
    }

    /**
     * Validaciones previas
     */
    public function __pre($accion){

        //Si existe el parametro articulo...
        if(isset($GLOBALS['parametros']['articulo'])){
            $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT id FROM catalogo_articulos WHERE id = ".$GLOBALS['parametros']['articulo']);
            $stmt->execute();
            $articulo = $stmt->rowCount();
        }

        //Si existe el articulo
        if( isset($articulo) && !$articulo && 
            in_array($accion, array('editar'))){
            $GLOBALS['resultado']->setError("El artículo no existe.");
            return;
        }

        //llamamos a la accion
        return call_user_func_array(array($this, $accion), array());
    }

    /**
     * @nombre: Lista las Aplicaciones
     * @descripcion: Asigna Vehículos a los Artículos
     */
    public function listar(){


        //Get Vehiculos
        $sql = "SELECT  articulo_id as id,
                        marca_id as vehiculo_marca_id,
                        (IF(marca_id IS NULL,'[TODOS]',(SELECT nombre FROM catalogo_autos_marca WHERE id = marca_id))) as vehiculo_marca_label,
                        modelo_id as vehiculo_modelo_id,
                        (IF(modelo_id IS NULL,'[TODOS]',(SELECT nombre FROM catalogo_autos_modelo WHERE id = modelo_id))) as vehiculo_modelo_label,
                        version_id as vehiculo_version_id,
                        (IF(version_id IS NULL,'[TODOS]',(SELECT descripcion FROM catalogo_autos_versiones WHERE id = version_id))) as vehiculo_version_label,
                        anio_inicio,
                        anio_final,
                        (CONCAT ( IF( anio_inicio = -1 AND anio_final = -1, 'TODOS', CONCAT( IF( anio_inicio = -1, '--', SUBSTRING(anio_inicio,3,2)), ' / ', IF( anio_final = -1, '--', SUBSTRING(anio_final,3,2)))))) as vehiculo_anio_label 
                FROM catalogo_articulos_aplicaciones  
                WHERE articulo_id = " . $GLOBALS['parametros']['articulo'];

        //Make query
        $stmt = $GLOBALS['conf']['pdo']->prepare($sql);
        $stmt->execute();
        $GLOBALS['resultado']->_result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * @nombre: Lista los Vehículos
     * @descripcion: Lista los vehículos para agregar Aplicación
     */
    public function listar_vehiculos(){

        //Separamos las palabras clave en un arreglo
        $keyword_tokens = explode(' ', $GLOBALS['parametros']['q']);

        //Calculate paging
        $page =  (intval($GLOBALS['parametros']['p'])-1) * 40;


        //Base SQL
        $sql = "(SELECT 
                         'Universal [Cualquier vehículo]' as nombre,
                         'null' as marca_id,
                         '' as marca_label,
                         'null' as modelo_id,
                         '' as modelo_label,
                         'null' as version_id,
                         '' as version_label
                )

                UNION

                (SELECT 
                         CONCAT(nombre,' [Todos los modelos]') as nombre,
                         id as marca_id,
                         nombre as marca_label,
                         'null' as modelo_id,
                         '' as modelo_label,
                         'null' as version_id,
                         '' as version_label

                FROM catalogo_autos_marca
                 
                ORDER BY nombre )

                UNION


                (SELECT
                         CONCAT( (SELECT nombre FROM catalogo_autos_marca WHERE id = id_marca), ' ', nombre, ' [Todas las versiones]') as nombre,
                         (SELECT id FROM catalogo_autos_marca WHERE id = id_marca) as marca_id,
                         (SELECT nombre FROM catalogo_autos_marca WHERE id = id_marca) as marca_label,
                         id as modelo_id,
                         nombre as modelo_nombre,
                         'null' as version_id,
                         '' as version_label

                FROM catalogo_autos_modelo 

                ORDER BY nombre)


                UNION

                ( SELECT (
                        CONCAT(
                                (SELECT nombre FROM catalogo_autos_marca WHERE id = (SELECT id_marca FROM catalogo_autos_modelo WHERE id = modelo_id)), ' ',
                                (SELECT nombre FROM catalogo_autos_modelo WHERE id = modelo_id), ' ', descripcion)) as nombre,
                                (SELECT id_marca FROM catalogo_autos_modelo WHERE id = modelo_id) as marca_id,
                                (SELECT nombre FROM catalogo_autos_marca WHERE id = (SELECT id_marca FROM catalogo_autos_modelo WHERE id = modelo_id)) as marca_label,
                                modelo_id,
                                (SELECT nombre FROM catalogo_autos_modelo WHERE id = modelo_id) as modelo_label,
                                id as version_id,
                                descripcion as version_label

                        FROM catalogo_autos_versiones
                )";


        //Make SQL query
        $stmt = $GLOBALS['conf']['pdo']->prepare($sql);
        $stmt->execute();

        //Guardamos los resultados
        $vehiculos = $stmt->fetchAll(PDO::FETCH_ASSOC);

        //Calculamos score
        foreach($vehiculos as $k=>$v){
            //Inicializamos el score en 0
            $vehiculos[$k]['score'] = 0;

            //Desglosamos el nombre
            $nombre = str_replace(' [Todos los modelos]', '', $v['nombre']);
            $nombre = str_replace(' [Todas las versiones]', '', $nombre);
            $nombre = str_replace(' [Cualquier vehículo]', '', $nombre);
            $nombre = explode(' ', $nombre);

            foreach($keyword_tokens as $w)
                //Le damos un punto si la palabra esta entre otras
                if(stripos($v['nombre'],$w) !== false){
                    $vehiculos[$k]['score']++;
                }

            //Por cada palabra clave
            foreach($keyword_tokens as $w)
                //Y por cada palabra del nombre...
                foreach($nombre as $x)
                    // Si la palabra buscada es exacta sumamos puntos
                    if( strtolower($w) == strtolower($x) )
                        // Score ++
                        $vehiculos[$k]['score']++;
                
        }
        


        function compareScore($a, $b) {
            if ($a['score'] == $b['score']) return 0;
            return ($a['score'] < $b['score']) ? 1 : -1;
        }



        //Sumamos puntos por Modelo o Marca
        if($keyword_tokens[0] != ''){
            
            foreach($vehiculos as $k=>$v)
                if($vehiculos[$k]['score'] == 0)
                    unset($vehiculos[$k]);

            usort($vehiculos, 'compareScore');

            //Calculamos score
            $scorelevelModelo = 0;
            foreach($vehiculos as $k=>$v)
                if(stripos($v['nombre'],'[Todas las versiones]') !== false){
                    $scorelevelModelo = $vehiculos[$k]['score'];
                    break;
                }


            //Todos los mayores scores al mismo nivel..
            foreach($vehiculos as $k=>$v){
                if( (stripos($v['nombre'],'[Todas las versiones]') !== false) && ($scorelevelModelo == $vehiculos[$k]['score'])){
                    $vehiculos[$k]['score']++;

                    //Por cada vehiculo buscamos su marca y la subimos
                    foreach($vehiculos as $k1=>$v1){
                        if( ($vehiculos[$k]['marca_id'] == $vehiculos[$k1]['marca_id']) && ($vehiculos[$k1]['modelo_id'] == 'null'))
                            $vehiculos[$k1]['score'] = $vehiculos[$k]['marca_id']+1;
                    }
                }
            }
                



            usort($vehiculos, 'compareScore');
        }



        //Calculamos totales
        $tot = count($vehiculos);
        $GLOBALS['resultado']->_result['total'] = $tot;


        //Cortamos el pedazo de array que nos interesa
        $res = array_slice($vehiculos, $page, 40);


        //Save items to result Object
        $GLOBALS['resultado']->_result['items'] = $res;
    }

    /**
     * @nombre: Guarda los cambios
     * @descripcion: Guarda los cambios en la DB
     */
    public function editar(){


        //Validamos las aplicaciones
        if( is_array($GLOBALS['parametros']['aplicaciones']) ){

            //Recorremos el arreglo..
            foreach ($GLOBALS['parametros']['aplicaciones'] as $k=>$v) {
                if( !(is_int($v['marca_id']) || is_null($v['marca_id'])) ||
                    !(is_int($v['modelo_id']) || is_null($v['modelo_id'])) ||
                    !(is_int($v['version_id']) || is_null($v['version_id'])) ||
                    !is_int($v['desde']) || !is_int($v['hasta']) ||
                    !( (((int)$v['desde'] > 1920) && ((int)$v['desde'] <= (int)date("Y"))) || ((int)$v['desde'] == -1)) ||
                    !( (((int)$v['hasta'] > 1920) && ((int)$v['hasta'] <= (int)date("Y"))) || ((int)$v['hasta'] == -1))
                    ){
                    //Error
                    $GLOBALS['resultado']->setError("Error de parametros. Tipos inesperado");
                    return;
                }

                //Convertimos los NULL valor en string
                if(is_null($v['marca_id'])) $GLOBALS['parametros']['aplicaciones'][$k]['marca_id'] = 'NULL';
                if(is_null($v['modelo_id'])) $GLOBALS['parametros']['aplicaciones'][$k]['modelo_id'] = 'NULL';
                if(is_null($v['version_id'])) $GLOBALS['parametros']['aplicaciones'][$k]['version_id'] = 'NULL';
            }

        //No es un array    
        }else{
            $GLOBALS['resultado']->setError("Error de parametros. no es objeto");
            return;
        }

        

        // Para validar las Aplicaciones antes de insertarlas en la base de datos
        // generamos un nuevo arreglo de aplicaciones y los filtramos con la funcion
        // del toolbox
        $apps = array();
        foreach ($GLOBALS['parametros']['aplicaciones'] as $k=>$app)
            $apps[] =   array(
                            'marca_id' => $app['marca_id'],
                            'modelo_id' => $app['modelo_id'], 
                            'version_id' => $app['version_id'], 
                            'anio_inicio' => (int)$app['desde'],
                            'anio_final' => (int)$app['hasta']
                        );


        // Combinamos y filtramos las aplicaciones
        $apps = $GLOBALS['toolbox']->combina_apps($apps);
        // La funcion anterior de la caja de herramientas devuelve un array
        // y en el elemento 0 define el valor 0 o 1 dependiendo si- se detecto
        // algun conflicto por solucionar con las versiones, a esta altura ya
        // no deberia haber conflictos asi que no lo dejamos continuar si no
        // los soluciona.. 30545824511
        if($apps[0]){
            $GLOBALS['resultado']->setError("Los cambios no se grabaron debido a que existen conflictos entre algunas Versiones de las Aplicaciones.");
            return;
        }

        // Quitamos el elemento 0
        unset($apps[0]);
        // Reindexamos el array
        $apps = array_values($apps);



        // Begin Transaction
        $GLOBALS['conf']['pdo']->beginTransaction();

        try {

            // Antes de hacer modificaciones al artículo actual, debemos saber si pertenece
            // algun grupo de equivalencias. Consultamos a la DB sus artículos compañeros de grupo
            $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT articulo_id FROM catalogo_articulos_equiv_app WHERE id = (SELECT id FROM catalogo_articulos_equiv_app WHERE articulo_id = ".$GLOBALS['parametros']['articulo'].") AND (articulo_id <> ".$GLOBALS['parametros']['articulo'].")");
            $stmt->execute(); 
            $artIds =  $stmt->fetchAll(PDO::FETCH_COLUMN);

            //Agregamos el articulo actual al arreglo.
            $artIds[] = $GLOBALS['parametros']['articulo'];

            //Borramos aplicaciones anteriores...
            $stmt = $GLOBALS['conf']['pdo']->prepare(" DELETE FROM catalogo_articulos_aplicaciones WHERE articulo_id IN (".implode(',', $artIds).")");
            $stmt->execute();

            //Si nos pasaron aplicaciones...
            if(count($apps)){
                
                // Generamos un array para agregar aplicaciones a la DB
                // Para el articulo actual y todos los compañeros de grupo
                // agregamos las todas las aplicaciones.
                foreach ($artIds as $x=>$artId)
                    foreach ($apps as $k=>$app)
                        $sqlElements[] = "(".$artId.",".$app['marca_id'].",".$app['modelo_id'].",".$app['version_id'].",".$app['anio_inicio'].",".$app['anio_final'].")";


                //Comentemos los cambios al a DB
                $stmt = $GLOBALS['conf']['pdo']->prepare("INSERT INTO catalogo_articulos_aplicaciones (articulo_id, marca_id, modelo_id, version_id, anio_inicio, anio_final) VALUES ".implode(',',$sqlElements));
                $stmt->execute();
            }

            // Commit Transaction
            $GLOBALS['conf']['pdo']->commit();

        } // Si existieron errores generando las Querys de arriba...
        catch (PDOException $e) {
            // hacemos un Rollback
            $GLOBALS['conf']['pdo']->rollback();

            //Agregamos error 
            $GLOBALS['resultado']->setError($e);
            echo $GLOBALS['resultado']->getResult();
            exit;
        }
    }
}
?>