<?php
/**
 * @nombre: Dimensiones
 * @descripcion: Dimensiones del Artículo
 */
class dimensiones  extends module{


    /*
	 * Constructor
	 */
    public function __construct(){
    }

    /**
     * Validaciones previas
     */
    public function __pre($accion){

        //Si existe el parametro articulo...
        if(isset($GLOBALS['parametros']['articulo']) && ($GLOBALS['parametros']['articulo'] != '')){
            $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT id FROM catalogo_articulos WHERE id = ".$GLOBALS['parametros']['articulo']);
            $stmt->execute();
            $articulo = $stmt->rowCount();
        }

        //Si existe el parametro rubro...
        if(isset($GLOBALS['parametros']['rubro']) && ($GLOBALS['parametros']['rubro'] != '')){
            $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT id FROM catalogo_rubros WHERE id = ".$GLOBALS['parametros']['rubro']);
            $stmt->execute();
            $rubro = $stmt->rowCount();
        }

        //Si existe el articulo
        if( isset($articulo) && $articulo && 
            in_array($accion, array('editar'))){
            $GLOBALS['resultado']->setError("El artículo no existe");
            return;
        }

        //Si existe el rubro
        if( isset($rubro) && !$rubro && 
            in_array($accion, array('listar_x_rubro','importar'))){
            $GLOBALS['resultado']->setError("El Rubro no existe");
            return;
        }


        //llamamos a la accion
        return call_user_func_array(array($this, $accion), array());
    }


    //--------------------------------------------------
    //------------ DIMENSIONES DEL ARTICULO ------------


    /**
     * @nombre: Información de Dimension
     * @descripcion: Pasamos 'id' la Dimensión y nos devuelve sus Datos.
     */
    public function info(){

        //Obtenemos los usuarios
        $stmt = $GLOBALS['conf']['pdo']->prepare("  SELECT  *, (SELECT nombre FROM catalogo_rubros WHERE id = rubro_id) as rubro_nombre
                                                    FROM    catalogo_rubros_datos 
                                                    WHERE   id= ".$GLOBALS['parametros']['dimension']);
        $stmt->execute();
        $datos = $stmt->fetch(PDO::FETCH_ASSOC);

        //Guardamos los datos
        $GLOBALS['resultado']->_result = $datos;
    }

    /**
     * @nombre: Lista las Dimensiones de un Artículo
     * @descripcion: Lista las Dimensiones de un Artículo
     */
    public function listar_x_articulo(){


        // Traemos los datos del articulo primero
        $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT * FROM catalogo_articulos WHERE id = ". $GLOBALS['parametros']['articulo']);
        $stmt->execute();
        $art = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $art = $art[0];

        //Conseguimos el listado completo de Rubros
        $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT * FROM catalogo_rubros");
        $stmt->execute();
        $rubros =  $stmt->fetchAll(PDO::FETCH_ASSOC);

        // Las dimensiones las pedimos a la base de datos solo si el rubro
        // es distinto de null
        if(!is_null($art['id_rubro'])){
            //Obtenemos mediante esta funcion todos los Rubros Progenitores
            $rubroFamilia = $GLOBALS['toolbox']->traeRubrosPadres($rubros, $art['id_rubro']);

            //Obtenemos datos del Rubro (propios y heredados)
            $stmt = $GLOBALS['conf']['pdo']->prepare("  (SELECT id, nombre
                                                        FROM catalogo_rubros_datos 
                                                        WHERE ( (rubro_id IN (".implode(',',$rubroFamilia).")) AND (cede_decendencia = 'Y') )
                                                        ORDER BY orden)

                                                        UNION

                                                        (SELECT id, nombre 
                                                        FROM catalogo_rubros_datos 
                                                        WHERE (rubro_id = ".$art['id_rubro'].")
                                                        ORDER BY orden)");
            $stmt->execute();
            $datosRubros =  $stmt->fetchAll(PDO::FETCH_ASSOC);

            //Obtenemos las opciones de los datos
            $idsDatos = array();
            foreach ($datosRubros as $k=>$v) $idsDatos[] = $v['id'];

            //Obtenemos datos del Rubro (propios y heredados)
            $datosRubrosOpciones = array();
            if(count($idsDatos)){
                $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT id, id_datos, nombre FROM catalogo_rubros_datos_opciones WHERE id_datos IN (".implode(',', $idsDatos).")");
                $stmt->execute();
                $datosRubrosOpciones = $stmt->fetchAll(PDO::FETCH_ASSOC);
            }


            //Generamos el arreglo de datos
            foreach($datosRubros as $k=>$d){
                //Obtenemos valor
                $valor = (is_null($art['extra_'.$d['id']]) ? '' : $art['extra_'.$d['id']]);

                $dimension = array();
                //Seteamos id y nombre
                $dimension['id'] = $d['id'];
                $dimension['nombre'] = $d['nombre'];
                $dimension['val'] = $valor;

                //Chequeamos si tiene opciones
                foreach ($datosRubrosOpciones as $x=>$o)
                    if($o['id_datos'] == $d['id']){
                        $opc = array();
                        $opc['id'] = $o['id'];
                        $opc['nombre'] = $o['nombre'];

                        //Si esta seleccionado este val..
                        if((int)$valor == $o['id']) 
                            $dimension['val'] = array('id'=> $o['id'], 'nombre'=>$o['nombre']);

                        //Agregamos la opcion al arreglo
                        $dimension['opciones'][] = $opc;
                    }

                //Agergamos la dimension al listado de resultados
                $GLOBALS['resultado']->_result[] = $dimension;
            }
        }
    }

    /**
     * @nombre: Edita las Dimensiones de un Artículo
     * @descripcion: Edita las Dimensiones de un Artículo
     */
    public function editar_x_articulo(){

        //De json a Array...
        $dimensiones_nuevas = $GLOBALS['parametros']['dimensiones'];

        //Convertimos las dimensiones a array
        //si o si...
        if(!is_array($dimensiones_nuevas))
            $dimensiones_nuevas = array();
   
        //dimensiones anteriores
        $dimensiones = array();

        // Traemos los datos del articulo primero
        $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT * FROM catalogo_articulos WHERE id = ". $GLOBALS['parametros']['articulo']);
        $stmt->execute();
        $art = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $art = $art[0];

        //Conseguimos el listado completo de Rubros
        $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT * FROM catalogo_rubros");
        $stmt->execute();
        $rubros =  $stmt->fetchAll(PDO::FETCH_ASSOC);


        // Las dimensiones las pedimos a la base de datos solo si el rubro
        // es distinto de null
        if(!is_null($art['id_rubro'])){
            //Obtenemos mediante esta funcion todos los Rubros Progenitores
            $rubroFamilia = $GLOBALS['toolbox']->traeRubrosPadres($rubros, $art['id_rubro']);

            //Obtenemos datos del Rubro (propios y heredados)
            $stmt = $GLOBALS['conf']['pdo']->prepare("  (SELECT id, nombre 
                                                        FROM catalogo_rubros_datos 
                                                        WHERE ( (rubro_id IN (".implode(',',$rubroFamilia).")) AND (cede_decendencia = 'Y') )
                                                        ORDER BY orden)

                                                        UNION

                                                        (SELECT id, nombre 
                                                        FROM catalogo_rubros_datos 
                                                        WHERE (rubro_id = ".$art['id_rubro'].")
                                                        ORDER BY orden)");
            $stmt->execute();
            $datosRubros =  $stmt->fetchAll(PDO::FETCH_ASSOC);

            //Obtenemos las opciones de los datos
            $idsDatos = array();
            foreach ($datosRubros as $k=>$v) $idsDatos[] = $v['id'];

            //Obtenemos datos del Rubro (propios y heredados)
            $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT id, id_datos, nombre FROM catalogo_rubros_datos_opciones WHERE id_datos IN (".implode(',', $idsDatos).")");
            $stmt->execute();
            $datosRubrosOpciones = $stmt->fetchAll(PDO::FETCH_ASSOC);

            //dimensiones
            $dimensiones = array();

            //Generamos el arreglo de datos
            foreach($datosRubros as $k=>$d){
                //Obtenemos valor
                $valor = (is_null($art['extra_'.$d['id']]) ? '' : $art['extra_'.$d['id']]);

                $dimension = array();
                //Seteamos id y nombre
                $dimension['id'] = $d['id'];
                $dimension['nombre'] = $d['nombre'];
                $dimension['val'] = $valor;

                //Chequeamos si tiene opciones
                foreach ($datosRubrosOpciones as $x=>$o)
                    if($o['id_datos'] == $d['id']){
                        $opc = array();
                        $opc['id'] = $o['id'];
                        $opc['nombre'] = $o['nombre'];

                        //Si esta seleccionado este val..
                        if((int)$valor == $o['id']) 
                            $dimension['val'] = array('id'=> $o['id'], 'nombre'=>$o['nombre']);

                        //Agregamos la opcion al arreglo
                        $dimension['opciones'][] = $opc;
                    }

                //Agergamos la dimension al listado de resultados
                $dimensiones[] = $dimension;
            }
        }


        //Una vez que obtenemos la dimensiones
        //Con sus opciones...
        foreach ($dimensiones as $k=>$v){
            //Chequeamos si nos pasaron esta dimension
            foreach ($dimensiones_nuevas as $x=>$v1){
                //Nos pasaron la dim!
                if($x == $v['id']){
                    //Si tiene opciones
                    if(isset($v['opciones'])){
                        $nuevo_val = false;
                        //Si tiene opciones nos fijamos cual coincide
                        foreach ($v['opciones'] as $y=>$o)
                            if($o['id'] == $v1){
                                $nuevo_val = $v1;
                                break;
                            }

                        //Si terminamos de recorrer las opciones
                        //y ninguna coincide, mantenemos el valor
                        //original o anterior de la dimension.
                        if(!$nuevo_val) {
                            if(isset($v['val']['id'])) $nuevo_val = $v['val']['id'];
                            else $nuevo_val = '';
                        }

                        //Nuevo valor
                        $dimensiones[$k]['nuevo_val'] = $nuevo_val;
                    
                    //Si no tiene opciones
                    }else{
                        $dimensiones[$k]['nuevo_val'] = $v1;
                    }
                }
            }
        }

        //Quitamos las dimensiones que no nos pasaron
        //por parametro, las matenemos sin cambios.
        foreach($dimensiones as $k=>$v)
            if(!isset($v['nuevo_val']))
                unset($dimensiones[$k]);

        //Reindexamos los valores...
        $dimensiones = array_values($dimensiones);


        //Buscamos los articulos que son equivalentes y sincronizan
        //dimensiones con otros articulos
        $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT id, articulo_id FROM catalogo_articulos_equiv_dim WHERE id = (SELECT id FROM catalogo_articulos_equiv_dim WHERE articulo_id = ".$art['id'].")");
        $stmt->execute();
        $equivs =  $stmt->fetchAll(PDO::FETCH_ASSOC);

        //Transformamos en array
        //unidimensional
        $equivalencias = array();
        foreach($equivs as $k=>$v)
            $equivalencias[] = $v['articulo_id'];



        //Generamos el sql para insertar todas las dimensiones
        //nuevas en los o el articulo..
        if(count($dimensiones)){
            $sql = "UPDATE catalogo_articulos SET ";
            foreach($dimensiones as $k=>$v){
                if($k) $sql .= ", ";
                $sql .= "extra_".$v['id']." = '".$v['nuevo_val']."'";
            }

            $sql .= " WHERE ";

            //Si posee equivalencias modificamos
            //todos los articulos...
            if(!count($equivalencias)){
                $sql .= "id = ".$art['id'];
            }else{
                $sql .= "id IN(".implode(',', $equivalencias).")";
            }

            //Cometemos los cambios en la DB
            $stmt = $GLOBALS['conf']['pdo']->prepare($sql);
            $stmt->execute();
        }


        $GLOBALS['resultado']->_result = $dimensiones;
    }




    //--------------------------------------------------
    //------------ DIMENSIONES DEL RUBRO ---------------

    /**
     * @nombre: Lista las Dimensiones de un Rubro
     * @descripcion: Lista las Dimensiones de un Rubro
     */
    public function listar_x_rubro(){

        //Conseguimos el listado completo de Rubros
        $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT * FROM catalogo_rubros");
        $stmt->execute();
        $rubros =  $stmt->fetchAll(PDO::FETCH_ASSOC);

        //Obtenemos mediante esta funcion todos los Rubros Progenitores
        $rubroFamilia = $GLOBALS['toolbox']->traeRubrosPadres($rubros, $GLOBALS['parametros']['rubro']);

        //Obtenemos datos del Rubro (propios y heredados)
        $stmt = $GLOBALS['conf']['pdo']->prepare("  (SELECT id, nombre as label, 1 as heredado, orden
                                                    FROM catalogo_rubros_datos 
                                                    WHERE ( (rubro_id IN (".implode(',',$rubroFamilia).")) AND (cede_decendencia = 'Y') )
                                                    ORDER BY orden)

                                                UNION

                                                    (SELECT id, nombre as label, 0 as heredado, orden
                                                    FROM catalogo_rubros_datos 
                                                    WHERE (rubro_id = ".$GLOBALS['parametros']['rubro']."))

                                                ORDER BY heredado DESC, orden ASC");
        $stmt->execute();
        $datosRubros =  $stmt->fetchAll(PDO::FETCH_ASSOC);


        //Guardamos los resultados
        $GLOBALS['resultado']->_result = $datosRubros;
    }


    /**
     * @nombre: Sube una posicion la dimension del Rubro
     * @descripcion: Sube una posicion la dimension del Rubro
     */
    public function subir_posicion(){

        //Conseguimos el listado completo de Rubros
        $stmt = $GLOBALS['conf']['pdo']->prepare("  SELECT  * 
                                                    FROM    catalogo_rubros_datos 
                                                    WHERE   rubro_id = (    SELECT  rubro_id 
                                                                            FROM    catalogo_rubros_datos 
                                                                            WHERE   id = ".$GLOBALS['parametros']['dimension'].")
                                                    ORDER BY orden");
        $stmt->execute();
        $datos_rubros =  $stmt->fetchAll(PDO::FETCH_ASSOC);


        //Intercambiamos posiciones con la 
        //dimension de nivel superior
        foreach($datos_rubros as $k=>$d)
            //Si encontramos la dimension...
            if($d['id'] == $GLOBALS['parametros']['dimension'])
                //Si la posicion actual es diferente a 0
                if($d['orden'] > 0){
                    $datos_rubros[$k] = $datos_rubros[$k-1];
                    $datos_rubros[$k-1] = $d;
                    break;
                }


        // Begin Transaction
        $GLOBALS['conf']['pdo']->beginTransaction();

        try {

            //Actualizamos el orden de todo
            foreach($datos_rubros as $k=>$d){
                $stmt = $GLOBALS['conf']['pdo']->prepare("UPDATE catalogo_rubros_datos SET orden=".$k." WHERE id=".$d['id']);
                $stmt->execute();
            }

            // Commit Transaction
            $GLOBALS['conf']['pdo']->commit();

        } // Si existieron errores generando las Querys de arriba...
        catch (PDOException $e) {
            // hacemos un Rollback
            $GLOBALS['conf']['pdo']->rollback();

            //Agregamos error 
            $GLOBALS['resultado']->setError("Error al tratar de actualizar");
            return;
        }
    }

    /**
     * @nombre: Baja una posicion la dimension del Rubro
     * @descripcion: Baja una posicion la dimension del Rubro
     */
    public function bajar_posicion(){

        //Conseguimos el listado completo de Rubros
        $stmt = $GLOBALS['conf']['pdo']->prepare("  SELECT  * 
                                                    FROM    catalogo_rubros_datos 
                                                    WHERE   rubro_id = (    SELECT  rubro_id 
                                                                            FROM    catalogo_rubros_datos 
                                                                            WHERE   id = ".$GLOBALS['parametros']['dimension'].")
                                                    ORDER BY orden");
        $stmt->execute();
        $datos_rubros =  $stmt->fetchAll(PDO::FETCH_ASSOC);


        $tope = count($datos_rubros)-1;

        //Intercambiamos posiciones con la 
        //dimension de nivel superior
        foreach($datos_rubros as $k=>$d)
            //Si encontramos la dimension...
            if($d['id'] == $GLOBALS['parametros']['dimension'])
                //Si la posicion actual es diferente a 0
                if($d['orden'] < $tope){
                    $datos_rubros[$k] = $datos_rubros[$k+1];
                    $datos_rubros[$k+1] = $d;
                    break;
                }


        // Begin Transaction
        $GLOBALS['conf']['pdo']->beginTransaction();

        try {

            //Actualizamos el orden de todo
            foreach($datos_rubros as $k=>$d){
                $stmt = $GLOBALS['conf']['pdo']->prepare("UPDATE catalogo_rubros_datos SET orden=".$k." WHERE id=".$d['id']);
                $stmt->execute();
            }

            // Commit Transaction
            $GLOBALS['conf']['pdo']->commit();

        } // Si existieron errores generando las Querys de arriba...
        catch (PDOException $e) {
            // hacemos un Rollback
            $GLOBALS['conf']['pdo']->rollback();

            //Agregamos error 
            $GLOBALS['resultado']->setError("Error al tratar de actualizar");
            return;
        }
    }


    /**
     * @nombre: Baja una posicion la dimension del Rubro
     * @descripcion: Baja una posicion la dimension del Rubro
     */
    public function eliminar_dimension(){

        // Begin Transaction
        $GLOBALS['conf']['pdo']->beginTransaction();

        try {

            //Conseguimos el listado completo de Rubros
            $stmt = $GLOBALS['conf']['pdo']->prepare("SHOW COLUMNS FROM catalogo_articulos LIKE 'extra_".$GLOBALS['parametros']['dimension']."'");
            $stmt->execute();
            $columna =  $stmt->fetchAll(PDO::FETCH_ASSOC);

            //Borramos la columna art.
            if(count($columna)){
                $stmt = $GLOBALS['conf']['pdo']->prepare("ALTER TABLE catalogo_articulos DROP COLUMN extra_".$GLOBALS['parametros']['dimension']);
                $stmt->execute();
            }

            $stmt = $GLOBALS['conf']['pdo']->prepare("DELETE FROM catalogo_rubros_datos WHERE id = ".$GLOBALS['parametros']['dimension']);
            $stmt->execute();

            // Commit Transaction
            $GLOBALS['conf']['pdo']->commit();

        } // Si existieron errores generando las Querys de arriba...
        catch (PDOException $e) {
            // hacemos un Rollback
            $GLOBALS['conf']['pdo']->rollback();

            //Agregamos error 
            $GLOBALS['resultado']->setError("Error al tratar de actualizar".$e);
            return;
        }
    }

    /**
     * @nombre: Edita dimension del Rubro
     * @descripcion: Edita dimension del Rubro
     */
    public function editar_dimension(){

        //hereda
        $hereda = 'N';
        if((int)$GLOBALS['parametros']['hereda'])
            $hereda = 'Y';

        $stmt = $GLOBALS['conf']['pdo']->prepare("  UPDATE  catalogo_rubros_datos 
                                                    SET     nombre='".$GLOBALS['parametros']['nombre']."', 
                                                            cede_decendencia='".$hereda."' 

                                                    WHERE   id=".$GLOBALS['parametros']['dimension']);
        $stmt->execute();
    }


    /**
     * @nombre: Crea una Dimension del Rubro
     * @descripcion: Crea una Dimension del Rubro
     */
    public function crear_dimension(){

        //hereda
        $hereda = 'N';
        if((int)$GLOBALS['parametros']['hereda'])
            $hereda = 'Y';

        // Begin Transaction
        $GLOBALS['conf']['pdo']->beginTransaction();

        try {

            //Seleccionamos todos las propiedades del mismo rubro
            $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT * FROM catalogo_rubros_datos WHERE rubro_id = ".$GLOBALS['parametros']['rubro']);
            $stmt->execute();
            $otrosDatos =  $stmt->fetchAll(PDO::FETCH_ASSOC);

            // Detectamos donde va a parar 
            // en orden la nueva variable
            $orden = count($otrosDatos);

            //Insertamos nueva Propiedad para el Rubro
            $stmt = $GLOBALS['conf']['pdo']->prepare("  INSERT INTO catalogo_rubros_datos 
                                                            (orden, rubro_id, nombre, cede_decendencia) 
                                                        VALUES (".$orden.",".$GLOBALS['parametros']['rubro'].",'".$GLOBALS['parametros']['nombre']."','Y')");
            $stmt->execute();

            //Obtenemos la id con la que se inserto la Propiedad
            $id = $GLOBALS['conf']['pdo']->lastInsertId();

            //Ahora que tenemos la id creamos una nueva columna en la tabla Articulos
            $stmt = $GLOBALS['conf']['pdo']->prepare("ALTER TABLE catalogo_articulos ADD extra_".$id." VARCHAR(150) NULL");
            $stmt->execute();

            // Commit Transaction
            $GLOBALS['conf']['pdo']->commit();

        } // Si existieron errores generando las Querys de arriba...
        catch (PDOException $e) {
            // hacemos un Rollback
            $GLOBALS['conf']['pdo']->rollback();

            //Agregamos error 
            $GLOBALS['resultado']->setError("Error al tratar de actualizar");
            return;
        }
    }


    //--------------------------------------------------
    //---------- IMPORTADOR DE DIMENSIONES -------------


    /**
     * @nombre: Importa dimensiones desde un arhivo XLS
     * @descripcion: Importa dimensiones desde un arhivo XLS
     */
    public function importar(){
        set_time_limit(0);

        

        //Guardamos los parametros en variables
        $rubro =  $GLOBALS['parametros']['rubro'];
        $modificar_rubro =  $GLOBALS['parametros']['modificar_rubro'];
        $actualizar_o_sobreescribir =  $GLOBALS['parametros']['actualizar_o_sobreescribir'];
        $columna_descripcion = $GLOBALS['parametros']['columna_descripcion'];
        $actualizar_o_sobreescribir_descripcion =  $GLOBALS['parametros']['actualizar_o_sobreescribir_descripcion'];
        $columna_codigo = $GLOBALS['parametros']['columna_codigo'];

        //Arreglo de Articulos - Dimensiones
        $art_arch = array();

        //Arreglo de Art. con Códigos Inválidos
        $invalidos = array();

        //Arreglo de Art. con Códigos Inválidos
        $inexistentes = array();

        //Arreglo de Art. con:
        //Con rubro asignado y diferente al seleccionado por el usuario
        //Perteneciente a un grupo de equivalencias con las que sincroniza dimensiones
        $revision_manual = array();

        //Arreglo de Art. con Códigos Inválidos
        $rubro_diferente = array();

        //Conseguimos el listado completo de Rubros
        $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT * FROM catalogo_rubros");
        $stmt->execute();
        $rubros =  $stmt->fetchAll(PDO::FETCH_ASSOC);

        //Obtenemos mediante esta funcion todos los Rubros Progenitores
        $rubroFamilia = $GLOBALS['toolbox']->traeRubrosPadres($rubros, $rubro);

        //Obtenemos datos del Rubro (propios y heredados)
        $stmt = $GLOBALS['conf']['pdo']->prepare("  (SELECT id, nombre as label 
                                                    FROM catalogo_rubros_datos 
                                                    WHERE ( (rubro_id IN (".implode(',',$rubroFamilia).")) AND (cede_decendencia = 'Y') )
                                                    ORDER BY orden)

                                                    UNION

                                                    (SELECT id, nombre as label 
                                                    FROM catalogo_rubros_datos 
                                                    WHERE (rubro_id = ".$rubro.")
                                                    ORDER BY orden)");
        $stmt->execute();
        $dimensiones = $dimensiones_originales =  $stmt->fetchAll(PDO::FETCH_ASSOC);

        //Si el Rubro no tiene Dimensiones
        //if(!count($dimensiones)){
        //    $GLOBALS['resultado']->setError("El Rubro tiene tiene dimensiones");
        //    return;
        //}


        //Verificamos validez del nombre de las columnas
        foreach($GLOBALS['parametros']['dimensiones'] as $k=>$d){
            $GLOBALS['parametros']['dimensiones'][$k] = strtoupper(trim($d));
            if($d == '') unset($GLOBALS['parametros']['dimensiones'][$k]);
            if(isset($GLOBALS['parametros']['dimensiones'][$k]) && 
                (!ctype_alpha($d) || (strlen($d) > 3))){
                $GLOBALS['resultado']->setError("Las columnas deben estar formadas por letras y hasta 3 caracteres");
                return;
            }
        }


        //Solo dimensiones que el rubro
        //posea realmente en la DB
        foreach($dimensiones as $k=>$d)
            foreach($GLOBALS['parametros']['dimensiones'] as $y=>$dp){

                if($d['id'] == $y){
                    
                    //Antes de agregar la columna verificamos
                    //si esta ya esta asignada a alguna otra dimension
                    foreach ($dimensiones as $x=>$d1){
                        if( isset($dimensiones[$x]['columna']) && ($x != $k) && 
                            ( ($dimensiones[$x]['columna'] == $dp) || ($columna_codigo == $dp))){
                            $GLOBALS['resultado']->setError("Existe una columna asignada a dos dimensiones");
                            return;
                        }
                    }

                    //Paso la prueba asique agregamos la columna
                    $dimensiones[$k]['columna'] = $dp;
                }
            }

        //Quitamos dimensiones que
        //no vayamos a actualizar.
        foreach($dimensiones as $k=>$d) 
            if(!isset($dimensiones[$k]['columna'])) 
                unset($dimensiones[$k]);


        //Otenemos informacion del archivo Excel
        require_once './classes/PHPExcel.php';

        //Ruta del archivo de dimensiones
        $archivo = $GLOBALS['parametros']['archivo']['nuevo_directorio'].$GLOBALS['parametros']['archivo']['name'];

        // array contains letters of column names
        $arrColumns = array(0=>'A',1=>'B',2=>'C',3=>'D',4=>'E',5=>'F',6=>'G',7=>'H',8=>'I',9=>'J',10=>'K',11=>'L',12=>'M',13=>'N',14=>'O',15=>'P',16=>'Q',17=>'R',18=>'S',19=>'T',20=>'U',21=>'V',22=>'W',23=>'X',24=>'Y',25=>'Z',26=>'AA',27=>'AB',28=>'AC',29=>'AD',30=>'AE',31=>'AF',32=>'AG',33=>'AH',34=>'AI',35=>'AJ',36=>'AK',37=>'AL',38=>'AM',39=>'AN',40=>'AO',41=>'AP',42=>'AQ',43=>'AR',44=>'AS',45=>'AT',46=>'AU',47=>'AV',48=>'AW',49=>'AX',50=>'AY',51=>'AZ',52=>'BA',53=>'BB',54=>'BC',55=>'BD',56=>'BE',57=>'BF',58=>'BG',59=>'BH',60=>'BI',61=>'BJ',62=>'BK',63=>'BL',64=>'BM',65=>'BN',66=>'BO',67=>'BP',68=>'BQ',69=>'BR',70=>'BS',71=>'BT',72=>'BU',73=>'BV',74=>'BW',75=>'BX',76=>'BY',77=>'BZ');

        $objPHPExcel = PHPExcel_IOFactory::load($archivo);
        $objPHPExcel->setActiveSheetIndex(0);
        $aSheet = $objPHPExcel->getActiveSheet();

        //Que cantidad de Filas
        $countRows = $aSheet->getHighestRow();

        //Que cantidad de columnas tiene el Arhivo
        $highestColumn = $aSheet->getHighestColumn();
        $countCols = PHPExcel_Cell::columnIndexFromString($highestColumn);
        $excel = array();

        //Generamos el Arreglo de Dimensiones
        for($i = 1; $i <= $countRows; $i++)
            for($y=0; $y<$countCols; $y++)
                $excel[$i][$arrColumns[$y]] = $aSheet->getCell($arrColumns[$y].$i)->getValue();

        //Borramos el archivo
        unlink($archivo);



        //Generamos arreglo de datos

        foreach($excel as $r_id=>$r){
            //Desglosamos
            $cod = $GLOBALS['toolbox']->desglosa_codigo($r[$columna_codigo]);

            //El Articulo es invalido
            if(!$cod){ 
                $invalidos[$r[$columna_codigo]] = $r;
                continue;
            }

            //Art. temporal
            $art_tmp = array();

            //Agregamos el codigo
            $art_tmp['prefijo'] = $cod['prefijo'];
            $art_tmp['codigo'] = $cod['codigo'];
            $art_tmp['sufijo'] = $cod['sufijo'];
            
            //Si el usuario indico que una de las
            //columnas del xls contiene la
            //descripcion del articulo
            if($columna_descripcion != '')

                //Si la celda de la descripcion
                //tiene algun dato igresado
                if(isset($r[$columna_descripcion])){
                    //echo "Actualiza solo las descripciones con texto: " . $actualizar_o_sobreescribir_descripcion . "<br>";
                    echo "descripcion: " . $r[$columna_descripcion] . "<br>";
                    //Asignamos la descripcion

                    $art_tmp['descripcion'] = $r[$columna_descripcion];

                    /*//Si nos estan pidiendo que actualicemos
                    //Solo las descripciones que tienen datos
                    if($actualizar_o_sobreescribir_descripcion == '0'){
                        $art_tmp['descripcion'] = trim($r[$columna_descripcion]);
                    }

                    if(is_null($r[$columna_descripcion]) || (trim($r[$columna_descripcion]) == '')){
                        if($actualizar_o_sobreescribir_descripcion == '0')
                            $art_tmp['descripcion'] = '';
                    }else{
                        $art_tmp['descripcion'] = trim($r[$columna_descripcion]);
                    }*/
                //Si la celda de la descripcion
                //se encuentra vacia
                }else{
                    //Y si ademas nos estan pidiendo que actualicemos
                    //la descripcion aunque esta este vacia...
                    if($actualizar_o_sobreescribir_descripcion == '0'){
                        $art_tmp['descripcion'] = '';
                    }
                }
            
            $art_tmp['dimensiones'] = array();

            //Agregamos las dimensiones 
            //que vamos a actualizar
            foreach($dimensiones as $k=>$d){
                //Si existe algun dato en la columna
                if(isset($r[$d['columna']])){
                    //Si la dimension esta vacia
                    if(is_null($r[$d['columna']]) || (trim($r[$d['columna']]) == '')){

                        //Si debemos sobreescribir todo
                        if($actualizar_o_sobreescribir == '0')
                            //Pisamos los datos existentes en la DB
                            $art_tmp['dimensiones'][$d['id']] = '';

                    }else{
                        $art_tmp['dimensiones'][$d['id']] = $r[$d['columna']];
                    }
                }
            }

            //Agregamos el articulo a la lista de articulos importados del archivo
            $art_arch[ $art_tmp['prefijo'].'-'.$art_tmp['codigo'].'-'.$art_tmp['sufijo'] ] = $art_tmp;
        }
        
        //echo "<pre>";
        //print_r($art_arch);
        //echo "</pre>";

        
        //Traemos Articulos de la DB
        $condicionales = array();

        //Obtenemos todas los codigos
        foreach($art_arch as $k=>$art)
            $condicionales[] = "((prefijo='".$art['prefijo']."') && (codigo='".$art['codigo']."') && (sufijo='".$art['sufijo']."'))";

        //Seleccionamos todos los articulos de la DB
        $sql = "    SELECT  id,
                            prefijo,
                            codigo,
                            sufijo,
                            id_rubro,
                            CONCAT(prefijo,'-',codigo,'-',sufijo) as cod,
                            (   SELECT  GROUP_CONCAT(CONCAT(prefijo,'-',codigo,'-',sufijo) SEPARATOR '#') 
                                FROM    catalogo_articulos 
                                WHERE   id IN ( SELECT DISTINCT articulo_id 
                                                FROM            catalogo_articulos_equiv_dim 
                                                WHERE           id = (  SELECT  id 
                                                                        FROM    catalogo_articulos_equiv_dim 
                                                                        WHERE   articulo_id = t.id)
                                                                ) AND (id <> t.id)
                            ) as equivalencias

                    FROM    catalogo_articulos as t";

        // Si tenemos los condicionales los agregamos
        if(count($condicionales)) $sql .= " WHERE " . implode(' OR ',$condicionales);

        //Seleccionamos todas las equivalencias
        $stmt = $GLOBALS['conf']['pdo']->prepare($sql);
        $stmt->execute();
        $art_db =  $stmt->fetchAll(PDO::FETCH_ASSOC);

        //Marcamos los existentes
        foreach($art_db as $k=>$v)
            if( isset($art_arch[$v['cod']]) ){
                //Marcamos si existe en el array de art
                $art_arch[$v['cod']]['existe'] = 1;
                //Guardamos el id del rubro para validar luego
                $art_arch[$v['cod']]['rubro'] = $v['id_rubro'];
            }

        //Agregamos inexistentes
        //a un array separado y lo
        //quitamos del arreglo principal
        foreach($art_arch as $k=>$v)
            if(!isset($v['existe'])){
               $inexistentes[$k] = $v;
               unset($art_arch[$k]); 
            }


        //recorremos el arreglo de los articulos
        //obtenidos de la base de datos y si las
        //equivalencias no son null las agregamos
        //con las dimensiones del articulo que nos
        //pasan en el archivo de importacion
        foreach($art_db as $k=>$v){

            //Si tenemos equivalencias disponibles
            //pero el articulo no pertenece al mismo
            //rubro que selecciono el usuario para
            //importar todos los articulos, no agregamos
            //sus equivalencias ni hacemos cambios a este
            //Lo separamos y le avisamos al usuario para
            //realice los cambios de forma manual ya que
            //puede tratarse de un error del usuario.
            if( !is_null($v['equivalencias']) && 
                !is_null($v['id_rubro']) && 
                ($v['id_rubro'] != $GLOBALS['parametros']['rubro'])){

                //Agregamos el articulo para revision manual
                $revision_manual[$v['cod']] = $art_arch[ $v['cod'] ];
                $revision_manual[$v['cod']]['equivalencias'] = explode('#', $v['equivalencias']);

                //Quitamos el articulo del arreglo principal
                unset($art_arch[$v['cod']]);

                continue;
            }


            $art_arch[$v['cod']]['equivalencias'] = null;

            //Si tenemos equivalencias disponibles
            if( !is_null($v['equivalencias'])){

                //Parseamos las equivalencias obtenidas de la DB
                $art_equivalencias = explode('#', $v['equivalencias']);

                //Guardamos las equivalencias
                $art_arch[$v['cod']]['equivalencias'] = $art_equivalencias;

                //Por cada una de las equivalencias
                //que le encontramos a este art.
                foreach($art_equivalencias as $x=>$e){

                    //Si existe en el arreglo de dimensiones
                    if(isset($art_arch[ $v['cod'] ])){

                        //Desglosamos el codigo de la equivalencia
                        $equiv_codigo = $GLOBALS['toolbox']->desglosa_codigo($e);
                        
                        //Generamos un nuevo item en la
                        //lista de art. a actualizar
                        if(is_array($equiv_codigo))
                            $art_arch[$e] = array(
                                "prefijo"   =>  $equiv_codigo['prefijo'],
                                "codigo"    =>  $equiv_codigo['codigo'],
                                "sufijo"    =>  $equiv_codigo['sufijo'],
                                "rubro"     =>  $art_arch[ $v['cod'] ]['rubro'],
                                "dimensiones"=> $art_arch[ $v['cod'] ]['dimensiones']
                            );
                    }
                }
            }
        }



        //Cambiamos los rubros si es que
        //Tenemos marcada la casilla sino
        //Separamos aquellos rubros diferentes
        foreach($art_arch as $k=>$art){
            
            //Si debemos de modificar el rubro
            if($modificar_rubro == '1'){
                //Si el Rubro es NULL lo cambiamos
                if(is_null($art['rubro']))
                    $art_arch[$k]['rubro'] = $GLOBALS['parametros']['rubro'];

                //Si el rubro no es null
                //es distinto al rubro que seleccionamos
                //y ademas no tiene equivalencias
                //cambiamos el rubro
                if( !is_null($art['rubro']) && 
                    is_null($art['equivalencias']) &&
                    ($GLOBALS['parametros']['rubro'] != $art['rubro']) ){
                    $art_arch[$k]['cambio_rubro'] = true;
                    $art_arch[$k]['rubro'] = $GLOBALS['parametros']['rubro'];
                }
            }

            //Si no debemos de modificar el Rubro
            if($modificar_rubro == '0'){
                //Si el rubro es diferente lo apilamos en otro arreglo
                if($art['rubro'] != $GLOBALS['parametros']['rubro']) 
                    $rubro_diferente[$k] = $art;
                    unset($art_arch[$k]);
            }
        }

        $sql = "SELECT id FROM catalogo_rubros_datos";

        if(count($dimensiones_originales)){

            //Obtenemos el resto de las dimensiones que
            //que no son las que pertenecen a este Rubro
            $sql .= " WHERE id NOT IN (";
            foreach($dimensiones_originales as $k=>$d)
                if($k) $sql .= ",".$d['id'];
                else $sql .= $d['id'];
            $sql .= ")";
        }


        //Traemos el resto de las dimensiones
        $stmt = $GLOBALS['conf']['pdo']->prepare($sql);
        $stmt->execute();
        $otras_dimensiones =  $stmt->fetchAll(PDO::FETCH_COLUMN, 0);



        $query = "";

        //Recorremos el arreglo de arts.
        foreach($art_arch as $cod=>$art){

            //Seteamos el rubro que es Obligatorio para actualizar las dimensiones
            $query .= "UPDATE catalogo_articulos SET id_rubro=".$art['rubro'];

            //Si esta seteada la descripcion
            if(isset($art['descripcion'])){
                echo "descripcion del insert: " . $art['descripcion'] . "<br>";
                $query .=", descripcion='". trim(str_replace('\'', '\\\'', $art['descripcion']))."'";
            }

            //Seteamos las dimensiones que existan
            foreach($art['dimensiones'] as $k=>$d)
                $query .=", extra_".$k."='".trim(str_replace('\'', '\\\'', $d))."'";

            //Si el rubro cambio seteamos 
            //el resto de las dimensiones a NULL
            if(isset($art['cambio_rubro']))
                if(isset($otras_dimensiones))
                    foreach ($otras_dimensiones as $k=>$d)
                        $query .=", extra_".$d."= NULL";

            //Agregamos el Where con el prefijo, codigo y sufijo
            $query .= " WHERE prefijo='".$art['prefijo']."' AND codigo='".$art['codigo']."' AND sufijo='".$art['sufijo']."';";
        }

        
        $GLOBALS['conf']['pdo']->beginTransaction();
        try {

            //Tratamos cada uno de los articulos
            //con una query individual.
            //Usamos una consulta atomica.
            $stmt = $GLOBALS['conf']['pdo']->query($query);

            // Commit Transaction
            $GLOBALS['conf']['pdo']->commit();

        }catch (PDOException $e) {
            // hacemos un Rollback
            $GLOBALS['conf']['pdo']->rollback();

            //Agregamos error 
            $GLOBALS['resultado']->setError($e);
            return;
        }


        $GLOBALS['resultado']->_result['articulos'] = $art_arch;
        $GLOBALS['resultado']->_result['inexistente'] = $inexistentes;
        $GLOBALS['resultado']->_result['invalidos'] = $invalidos;
        $GLOBALS['resultado']->_result['revision'] = $revision_manual;
        $GLOBALS['resultado']->_result['distinto_rubro'] = $rubro_diferente;
/*
        // Begin Transaction
        $GLOBALS['conf']['pdo']->beginTransaction();

        try {

            //Tratamos cada uno de los articulos
            //con una query individual.
            //Usamos una consulta atomica.
            foreach($SQL_updates as $k=>$q){
                $stmt = $GLOBALS['conf']['pdo']->prepare($q);
                $stmt->execute();
            }

            // Commit Transaction
            $GLOBALS['conf']['pdo']->commit();

        }catch (PDOException $e) {
            // hacemos un Rollback
            $GLOBALS['conf']['pdo']->rollback();

            //Agregamos error 
            $GLOBALS['resultado']->setError($e);
            return;
        }


        $GLOBALS['resultado']->_result['articulos'] = $art_arch;
        $GLOBALS['resultado']->_result['inexistente'] = $inexistentes;
        $GLOBALS['resultado']->_result['invalidos'] = $invalidos;
        $GLOBALS['resultado']->_result['revision'] = $revision_manual;
        $GLOBALS['resultado']->_result['distinto_rubro'] = $rubro_diferente;
        $GLOBALS['resultado']->_result['sql'] = $SQL_updates;


        

        //Guardamos en una variable mas facil
        //$columnas_dimensiones = $GLOBALS['parametros']['dimensiones'];

        */
    }
}
?>