<?php
/**
 * @nombre: Vehículos
 * @descripcion: Metodos para Administrar Vehículos
 */
class vehiculos extends module{


    /*
	 * Constructor
	 */
    public function __construct(){
    }

    /**
     * Validaciones previas
     */
    public function __pre($accion){

        //Si existe el parametro marca...
        if(isset($GLOBALS['parametros']['marca']) && ($GLOBALS['parametros']['marca'] != '')){
            $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT * FROM catalogo_autos_marca WHERE id = ".$GLOBALS['parametros']['marca']);
            $stmt->execute();
            $marca = $stmt->rowCount();
        }

        //Si existe el parametro modelo...
        if(isset($GLOBALS['parametros']['modelo']) && ($GLOBALS['parametros']['modelo'] != '')){
            $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT * FROM catalogo_autos_modelo WHERE id = ".$GLOBALS['parametros']['modelo']);
            $stmt->execute();
            $modelo = $stmt->rowCount();
        }

        //Si existe el parametro version...
        if(isset($GLOBALS['parametros']['version']) && ($GLOBALS['parametros']['version'] != '')){
            $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT * FROM catalogo_autos_versiones WHERE id = ".$GLOBALS['parametros']['version']);
            $stmt->execute();
            $version = $stmt->rowCount();
        }

        //No existe la marca
        if( in_array($accion, array('info_marca','editar_marca','eliminar_marca','agregar_modelo')) && isset($marca) && !$marca ){
            //Error
            $GLOBALS['resultado']->setError("La Marca no existe.");
            return;
        }

        //No existe el modelo
        if( in_array($accion, array('info_modelo','editar_modelo','eliminar_modelo','agregar_version')) && isset($modelo) && !$modelo ){
            //Error
            $GLOBALS['resultado']->setError("El Modelo no existe.");
            return;
        }

        //No existe la version
        if( in_array($accion, array('info_version','editar_version','eliminar_version')) && isset($version) && !$version ){
            //Error
            $GLOBALS['resultado']->setError("La Versión no existe.");
            return;
        }

        //llamamos a la accion
        return call_user_func_array(array($this, $accion), array());
    }

    /**
     * @nombre: Información de la Marca
     * @descripcion: Información de la Marca
     */
    public function info_marca(){
        //Obtenemos los usuarios
        $stmt = $GLOBALS['conf']['pdo']->prepare("  SELECT  nombre
                                                    FROM    catalogo_autos_marca
                                                    WHERE   id = ".$GLOBALS['parametros']['marca']);
        $stmt->execute();
        $datos = $stmt->fetch(PDO::FETCH_ASSOC);

        //Resultados
        $GLOBALS['resultado']->_result = $datos;
    }

    /**
     * @nombre: Lista las Marcas
     * @descripcion: Lista las Marcas
     */
    public function listar_marcas(){
        //SQL
        $stmt = $GLOBALS['conf']['pdo']->prepare("  SELECT      id, nombre,
                                                                (SELECT COUNT(*) FROM catalogo_autos_modelo WHERE id_marca = catalogo_autos_marca.id) as modelos
                                                    FROM        catalogo_autos_marca
                                                    ORDER BY    nombre");
        $stmt->execute();

        //Asignamos resultados
        $GLOBALS['resultado']->_result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * @nombre: Agregar una Marca
     * @descripcion: Agregar una Marca
     */
    public function agregar_marca(){
        $stmt = $GLOBALS['conf']['pdo']->prepare("  INSERT INTO catalogo_autos_marca 
                                                                (nombre) 
                                                    VALUES      ('".$GLOBALS['parametros']['nombre']."')");
        $stmt->execute();
    }

    /**
     * @nombre: Eliminar una Marca
     * @descripcion: Eliminar una Marca
     */
    public function eliminar_marca(){
        $stmt = $GLOBALS['conf']['pdo']->prepare("  DELETE FROM catalogo_autos_marca 
                                                    WHERE   id= ".$GLOBALS['parametros']['marca']);
        $stmt->execute();
    }

    /**
     * @nombre: Editar una Marca
     * @descripcion: Editar una Marca
     */
    public function editar_marca(){
        $stmt = $GLOBALS['conf']['pdo']->prepare("  UPDATE  catalogo_autos_marca 
                                                    SET     nombre='".$GLOBALS['parametros']['nombre']."' 
                                                    WHERE   id= ".$GLOBALS['parametros']['marca']);
        $stmt->execute();
    }

    /**
     * @nombre: Información del Modelo
     * @descripcion: Información del Modelo
     */
    public function info_modelo(){
        //Obtenemos los usuarios
        $stmt = $GLOBALS['conf']['pdo']->prepare("  SELECT  id, nombre, 
                                                            id_marca as marca_id,
                                                            (SELECT nombre FROM catalogo_autos_marca WHERE id = catalogo_autos_modelo.id_marca) as marca_nombre
                                                    FROM    catalogo_autos_modelo
                                                    WHERE   id = ".$GLOBALS['parametros']['modelo']);
        $stmt->execute();
        $datos = $stmt->fetch(PDO::FETCH_ASSOC);

        //Resultados
        $GLOBALS['resultado']->_result = $datos;
    }

    /**
     * @nombre: Lista los Modelos
     * @descripcion: Lista los Modelos
     */
    public function listar_modelos(){
        //SQL
        $stmt = $GLOBALS['conf']['pdo']->prepare("  SELECT      id, nombre,
                                                                (SELECT COUNT(*) FROM catalogo_autos_versiones WHERE modelo_id = catalogo_autos_modelo.id) as versiones
                                                    FROM        catalogo_autos_modelo
                                                    WHERE       id_marca = ".$GLOBALS['parametros']['marca']."
                                                    ORDER BY    nombre");
        $stmt->execute();

        //Asignamos resultados
        $GLOBALS['resultado']->_result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * @nombre: Agregar un Modelo
     * @descripcion: Agregar un Modelo
     */
    public function agregar_modelo(){
        $stmt = $GLOBALS['conf']['pdo']->prepare("  INSERT INTO catalogo_autos_modelo 
                                                                (id_marca, nombre) 
                                                    VALUES      (".$GLOBALS['parametros']['marca'].",'".$GLOBALS['parametros']['nombre']."')");
        $stmt->execute();
    }

    /**
     * @nombre: Eliminar un Modelo
     * @descripcion: Eliminar un Modelo
     */
    public function eliminar_modelo(){
        $stmt = $GLOBALS['conf']['pdo']->prepare("  DELETE FROM catalogo_autos_modelo
                                                    WHERE   id= ".$GLOBALS['parametros']['modelo']);
        $stmt->execute();
    }

    /**
     * @nombre: Editar un Modelo
     * @descripcion: Editar un Modelo
     */
    public function editar_modelo(){
        $stmt = $GLOBALS['conf']['pdo']->prepare("  UPDATE  catalogo_autos_modelo 
                                                    SET     nombre='".$GLOBALS['parametros']['nombre']."' 
                                                    WHERE   id= ".$GLOBALS['parametros']['modelo']);
        $stmt->execute();
    }

    /**
     * @nombre: Información de la Versión
     * @descripcion: Información de la Versión
     */
    public function info_version(){
        //Obtenemos los usuarios
        $stmt = $GLOBALS['conf']['pdo']->prepare("  SELECT
                                                            descripcion as nombre
                                                    FROM    catalogo_autos_versiones
                                                    WHERE   id = ".$GLOBALS['parametros']['version']);
        $stmt->execute();
        $datos = $stmt->fetch(PDO::FETCH_ASSOC);

        //Resultados
        $GLOBALS['resultado']->_result = $datos;
    }

    /**
     * @nombre: Lista las Versiones
     * @descripcion: Lista las Versiones
     */
    public function listar_versiones(){
        //SQL
        $stmt = $GLOBALS['conf']['pdo']->prepare("  SELECT      id, descripcion
                                                    FROM        catalogo_autos_versiones
                                                    WHERE       modelo_id = ".$GLOBALS['parametros']['modelo']."
                                                    ORDER BY    descripcion");
        $stmt->execute();

        //Asignamos resultados
        $GLOBALS['resultado']->_result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * @nombre: Agregar una Versión
     * @descripcion: Agregar una Versión
     */
    public function agregar_version(){
        $stmt = $GLOBALS['conf']['pdo']->prepare("  INSERT INTO catalogo_autos_versiones 
                                                                (modelo_id, descripcion) 
                                                    VALUES      (".$GLOBALS['parametros']['modelo'].",'".$GLOBALS['parametros']['nombre']."')");
        $stmt->execute();
    }

    /**
     * @nombre: Eliminar una Versión
     * @descripcion: Eliminar una Versión
     */
    public function eliminar_version(){
        $stmt = $GLOBALS['conf']['pdo']->prepare("  DELETE FROM catalogo_autos_versiones 
                                                    WHERE   id= ".$GLOBALS['parametros']['version']);
        $stmt->execute();
    }

    /**
     * @nombre: Editar una Versión
     * @descripcion: Editar una Versión
     */
    public function editar_version(){
        $stmt = $GLOBALS['conf']['pdo']->prepare("  UPDATE  catalogo_autos_versiones 
                                                    SET     descripcion='".$GLOBALS['parametros']['nombre']."' 
                                                    WHERE   id= ".$GLOBALS['parametros']['version']);
        $stmt->execute();
    }





    //SELECT 2



    /**
     * @nombre: Lista las Marcas (Select2)
     * @descripcion: Lista las Marcas (Select2)
     */
    public function listar_marcas_s2(){

        //Calculate paging
        $page =  (intval($GLOBALS['parametros']['p'])-1) * 40;

        //EStamos buscando o solo listando?
        if($GLOBALS['parametros']['q'] == '') $busqueda = false;
        else $busqueda = true;

        //Obtenemos los usuarios
        $stmt = $GLOBALS['conf']['pdo']->prepare("  SELECT  id, nombre 
                                                    FROM     catalogo_autos_marca
                                                    ORDER BY nombre");
        $stmt->execute();
        $api = $stmt->fetchAll(PDO::FETCH_ASSOC);


        //Si estamos buscando...
        if($busqueda){
            //Score
            foreach($api as $k=>$v)
                $api[$k]['score'] = $GLOBALS['toolbox']->compare_strings($GLOBALS['parametros']['q'], $v['nombre']);

            //Eliminamos 0's
            foreach($api as $k=>$v)
                if($v['score'] == 0)
                    unset($api[$k]);

            //Ordenamos
            usort($api, function($a, $b) {
                return $b['score'] - $a['score'];
            });
        }

        //Calculamos totales
        $GLOBALS['resultado']->_result['total'] = count($api);

        //Devolvemos items y total
        $GLOBALS['resultado']->_result['items'] = array_slice($api, $page, 40);
    }


    /**
     * @nombre: Lista las Marcas y Modelos (Select2)
     * @descripcion: Lista las Marcas y Modelos (Select2)
     */
    public function listar_marcas_modelos_s2(){

        //Calculate paging
        $page =  (intval($GLOBALS['parametros']['p'])-1) * 40;

        //EStamos buscando o solo listando?
        if($GLOBALS['parametros']['q'] == '') $busqueda = false;
        else $busqueda = true;

        //Obtenemos los usuarios
        $stmt = $GLOBALS['conf']['pdo']->prepare("  SELECT  id          as modelo_id,
                                                            nombre      as modelo_nombre,
                                                            id_marca    as marca_id,
                                                            (SELECT nombre FROM catalogo_autos_marca WHERE id = catalogo_autos_modelo.id_marca) as marca_nombre 
                                                    
                                                    FROM     catalogo_autos_modelo
                                                    ORDER BY marca_nombre, modelo_nombre");
        $stmt->execute();
        $api = $stmt->fetchAll(PDO::FETCH_ASSOC);


        //Si estamos buscando...
        if($busqueda){
            //Score
            foreach($api as $k=>$v)
                $api[$k]['score'] = $GLOBALS['toolbox']->compare_strings($GLOBALS['parametros']['q'], $v['marca_nombre'].' '.$v['modelo_nombre']);

            //Eliminamos 0's
            foreach($api as $k=>$v)
                if($v['score'] == 0)
                    unset($api[$k]);

            //Ordenamos
            usort($api, function($a, $b) {
                return $b['score'] - $a['score'];
            });
        }

        //Calculamos totales
        $GLOBALS['resultado']->_result['total'] = count($api);

        //Devolvemos items y total
        $GLOBALS['resultado']->_result['items'] = array_slice($api, $page, 40);
    }
}
?>