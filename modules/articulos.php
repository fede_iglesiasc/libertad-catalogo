<?php
/** 
* @nombre: Articulos
* @descripcion: Lista Artículos
*/
class articulos extends module{

 	public $session_vars = array(	'advSearch_prefijo',
									'advSearch_codigo',
									'advSearch_sufijo',
									'advSearch_fabricante',
									'advSearch_vehiculo',
									'advSearch_vehiculo_filas_individuales',
									'advSearch_rubro',
									'advSearch_dimensiones',		//Arreglo de dimensiones
									'advSearch_busqueda_dim',		//Busqueda dimensional
									'advSearch_dim_en_columnas',
									'quicksearch_term',
									'quicksearch_ids',
									'quicksearch_ultimos',
									'modo');	//Dimensiones en columnas

    /*
	 * Constructor
	 */
    public function __construct(){
    	
    	//Seteamos las variables si no exsiten en -1
    	foreach($this->session_vars as $k=>$v){
    		$valor = $GLOBALS['session']->getData($v);
    		//if('advSearch_sufijo' == $v) var_dump($valor);
    		if(is_bool($valor) && !$valor) 
    			$GLOBALS['session']->setData($v,'-1');
    	}
    }

    /**
     * Validaciones previas
     */
    public function __pre($accion){

        //Si existe el parametro articulo...
        if(isset($GLOBALS['parametros']['articulo'])){
            $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT id FROM catalogo_articulos WHERE id = '".$GLOBALS['parametros']['articulo']."'");
            $stmt->execute();
            $articulo = $stmt->rowCount();
        }



        //Si no existe el Articulo
        if( isset($articulo) && !$articulo && 
            in_array($accion, array('get_dimensiones','get_equivalencias'))){
            $GLOBALS['resultado']->setError("La Función no existe.");
            return;
        }

        //llamamos a la accion
        return call_user_func_array(array($this, $accion), array());
    }


	/** 
	* @nombre: Listar Artículos
	* @descripcion: Usado cada vez que se listan resultados de artículos.
	*/
    public function busqueda(){

    	//Rol del usuario
    	$rol = $GLOBALS['session']->getData('rol');

    	/* GENERATE COLS */
		function generaColumnas($sv){

			//Si estamos en modo carro
			if($sv['modo'] == 'carro'){
			    
				//Codigo
				$col['codigo']['index'] = 1;
				$col['codigo']['type'] = "codigo";
				$col['codigo']['friendly'] = "Código";
				$col['codigo']['format'] = "<b>{0}</b>";

				//Descripcion
				$col['descripcion']['index'] = 2;
				$col['descripcion']['type'] = "justificado_izq";
				$col['descripcion']['friendly'] = "Descripción";
				$col['descripcion']['format'] = "{0}";

				//Precio
				$col['precio_lista']['index'] = 3;
				$col['precio_lista']['type'] = "number";
				$col['precio_lista']['decimals'] = 2;
				$col['precio_lista']['friendly'] = "Precio U. (Lista)";
				$col['precio_lista']['format'] = "<div style=\"width: 64px;position: relative;left: 50%;margin-left: -32px;text-align: right;\"><span style=\"display: inline-block;line-height: 20px;\">$</span><span style=\"width: 57px;display: inline-block;line-height: 20px;\">{0}</span><div></div></div>";

				//Precio
				$col['precio']['index'] = 4;
				$col['precio']['type'] = "number";
				$col['precio']['decimals'] = 2;
				$col['precio']['friendly'] = "Precio U. (Costo)";
				$col['precio']['format'] = "<div style=\"width: 64px;position: relative;left: 50%;margin-left: -32px;text-align: right;\"><span style=\"display: inline-block;line-height: 20px;\">$</span><span style=\"width: 57px;display: inline-block;line-height: 20px;\">{0}</span><div></div></div>";

				//Cantidad
				$col['cantidad']['index'] = 5;
				$col['cantidad']['type'] = "number";
				$col['cantidad']['friendly'] = "Cantidad";
				$col['cantidad']['format'] = '<input type="text" data-anterior="{0}" value="{0}" class="form-control carro-cantidad"><div class="carro-cantidad-botones unselectable"><i class="fa fa-sort-asc carro-cantidad-up" style="font-size: 19px  !important;"></i><i class="fa fa-sort-desc carro-cantidad-down" style="font-size: 19px !important;"></i></div>';

				//Quitar
				$col['quitar']['index'] = 6;
				$col['quitar']['type'] = "string";
				$col['quitar']['friendly'] = " ";
				//$col['quitar']['format'] = '<button type="button" onClick="confirma(\'Eliminar Articulo del Pedido\',\'El Articulo se eliminará de forma permanente.\',\'exclamacion\',\'danger\',\'pedidos_articulo_eliminar(\\\'{0}\\\')\');" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>Quitar';
				$col['quitar']['format'] = '<span class="glyphicon glyphicon-remove" style="color: #D41515; font-size: 11px !important;" aria-hidden="true" onClick="confirma(\'Eliminar Articulo del Pedido\',\'El Articulo se eliminará de forma permanente.\',\'exclamacion\',\'danger\',\'pedidos_articulo_eliminar(\\\'{0}\\\')\');"></span> <b style="text-transform: uppercase; font-size: 9px !important; color: rgb(70, 70, 70);" onClick="confirma(\'Eliminar Articulo del Pedido\',\'El Articulo se eliminará de forma permanente.\',\'exclamacion\',\'danger\',\'pedidos_articulo_eliminar(\\\'{0}\\\')\');">Quitar</b>';

				//Devolvemos
				return $col;
			}

			//Si estamos en modo carro
			if($sv['modo'] == 'listas'){

				//Descripcion
				$col['fabricante']['index'] = 1;
				$col['fabricante']['type'] = "string";
				$col['fabricante']['friendly'] = "Fabricante";
				$col['fabricante']['format'] = '<img src="./fabricantes/{0}.svg" style="width: 100%; height: 100%; max-height: 20px; max-width: 50px; margin: 0px 10px 0px 10px;"/>';

				//Descripcion
				$col['fabricante_nombre']['index'] = 2;
				$col['fabricante_nombre']['type'] = "string";
				$col['fabricante_nombre']['friendly'] = " ";
				$col['fabricante_nombre']['format'] = '<b><p style="text-align: left; padding-left: 20px;">{0}</p></b>';

				//Precio
				$col['descripcion']['index'] = 3;
				$col['descripcion']['type'] = "string";
				$col['descripcion']['friendly'] = "Descripcion";
				$col['descripcion']['format'] = "{0}";

				//Fecha
				$col['fecha']['index'] = 4;
				$col['fecha']['type'] = "string";
				$col['fecha']['friendly'] = "Fecha";
				$col['fecha']['format'] = "{0}";

				//Porcentaje
				$col['cantidad']['index'] = 5;
				$col['cantidad']['type'] = "number";
				$col['cantidad']['friendly'] = "Cant. Art.";
				$col['cantidad']['format'] = '{0}';

				//Porcentaje
				$col['porcentaje']['index'] = 6;
				$col['porcentaje']['type'] = "string";
				$col['porcentaje']['friendly'] = "Porcentaje";
				$col['porcentaje']['format'] = '{0}';

				//Acciones
				$col['acciones']['index'] = 7;
				$col['acciones']['type'] = "string";
				$col['acciones']['friendly'] = " ";
				$col['acciones']['format'] = '{0}';

				//Devolvemos
				return $col;
			}

			/*CÓDIGO: Siempre va a estar*/
			$index = 1;

		    $col['codigo']['index'] = $index;
		    $col['codigo']['type'] = "codigo";
		    $col['codigo']['friendly'] = "Código";
		    $col['codigo']['format'] = "<b>{0}</b>";
		    $index++;

		    // RUBRO
		    $col['rubro']['index'] = $index;
		    $col['rubro']['type'] = "justificado_izq";
		    $col['rubro']['friendly'] = "Rubro";
		    $col['rubro']['format'] = "{0}";
		    $index++;

		    // DESCRIPCION 
		    $col['descripcion']['index'] = $index;
		    $col['descripcion']['type'] = "justificado_izq";
		    $col['descripcion']['friendly'] = "Descripción";
		    $col['descripcion']['format'] = "{0}";
		    $col['descripcion']['hidden'] = true;
		    $index++;

		    /* MARCA: Solo visible si no lo seleccionaron */
		    $col['fabricante']['index'] = $index;
		    $col['fabricante']['type'] = "string";
		    $col['fabricante']['friendly'] = "Fabricante";
		    $col['fabricante']['format'] = "{0}";
		    $index++;

		    //Si especificaron marca la ocultamos del listado
		    if($sv['advSearch_fabricante'] != '-1') $col['fabricante']['hidden'] = true;


		    if($sv['advSearch_vehiculo_filas_individuales'] == '1'){
			    /* APP - MARCA */
			    $col['vehiculo_marca']['index'] = $index;
			    $col['vehiculo_marca']['type'] = "string";
			    $col['vehiculo_marca']['friendly'] = "Marca";
			    $col['vehiculo_marca']['format'] = "{0}";
			    $index++;

			    $vehiculo = explode('#', $sv['advSearch_vehiculo']);

			    //Si especificaron App-marca la ocultamos
			    if(($sv['advSearch_vehiculo'] != '-1') && ($vehiculo[1] != '-1')) $col['vehiculo_marca']['hidden'] = true;

			    /* APP - MODELO */
			    $col['vehiculo_modelo']['index'] = $index;
			    $col['vehiculo_modelo']['type'] = "string";
			    $col['vehiculo_modelo']['friendly'] = "Modelo";
			    $col['vehiculo_modelo']['format'] = "{0}";
			    $index++;

			    //Si especificaron  App-modelo lo ocultamos
			    if(($sv['advSearch_vehiculo'] != '-1') && ($vehiculo[1] != '-1')) $col['vehiculo_modelo']['hidden'] = true;


			    /* APP - VERSIONES : mostramos siempre que haya aplicaciones para el producto*/
			    $col['vehiculo_version']['index'] = $index;
			    $col['vehiculo_version']['type'] = "string";
			    $col['vehiculo_version']['friendly'] = "Version";
			    $col['vehiculo_version']['format'] = "{0}";
				$index++;

			    $col['vehiculo_anio']['index'] = $index;
			    $col['vehiculo_anio']['type'] = "string";
			    $col['vehiculo_anio']['friendly'] = "Año";
			    $col['vehiculo_anio']['format'] = "{0}";
			    $index++;
			}


		    if(isset($sv['advSearch_dimensiones']) && is_array($sv['advSearch_dimensiones']) && ($sv['advSearch_dim_en_columnas'] == '1')){
			    
			    //Function that order the array
			    function compareOrden($a, $b){
				    if ($a['orden'] == $b['orden']) return 0;
				    return ($a['orden'] > $b['orden']) ? 1 : -1;
				}

				//Order the Array
				usort($sv['advSearch_dimensiones'], 'compareOrden');
		        
		        foreach($sv['advSearch_dimensiones'] as $key=>$variable){
		            $col['extra_'.$variable['id']]['index'] = $index;
		            $col['extra_'.$variable['id']]['type'] = "string";
		            $col['extra_'.$variable['id']]['friendly'] = $variable['label'];
		            $col['extra_'.$variable['id']]['format'] = "{0}";

		            if($variable['value'] != '-1')  $col['extra_'+$variable['id']]['hidden'] = true;

		            $index++;
		        }
		    }

		    /* PRECIO: se muestra por defecto*/
		    $col['precio']['index'] = $index;
		    $col['precio']['type'] = "number";
		    $col['precio']['friendly'] = "Precio";
		    $col['precio']['format'] = "<div style=\"width: 64px;position: relative;left: 50%;margin-left: -32px;text-align: right;\"><span style=\"display: inline-block;line-height: 20px;\">$</span><span style=\"width: 57px;display: inline-block;line-height: 20px;\">{0}</span><div></div></div>";

		    return $col;
		}

    	//Si pedimos un cambio de modo lo cambiamos
    	if(isset($GLOBALS['parametros']['modo'])){
    		if( $GLOBALS['parametros']['modo'] == 'carro')
    			$GLOBALS['session']->setData('modo','carro');

     		if( $GLOBALS['parametros']['modo'] == 'busqueda')
    			$GLOBALS['session']->setData('modo','-1');

     		if( $GLOBALS['parametros']['modo'] == 'listas')
    			$GLOBALS['session']->setData('modo','listas');

      		if( $GLOBALS['parametros']['modo'] == 'configuracion_precios')
    			$GLOBALS['session']->setData('modo','configuracion_precios');

        	if( $GLOBALS['parametros']['modo'] == 'mensajes')
    			$GLOBALS['session']->setData('modo','mensajes');
    	}

    	//Get variables
    	foreach($this->session_vars as $k=>$v) $sv[$v] = $GLOBALS['session']->getData($v);


		////////////////////////////////////////////////////////////////////////////////////
    	// CONFIGURACION
    	////////////////////////////////////////////////////////////////////////////////////
    	if($sv['modo'] == 'configuracion_precios'){

	        //Usuario
	        $usuario = $GLOBALS['session']->getData('usuario');

	        //Obtenemos el pedido pendiente
	        $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT * FROM pedidos WHERE (cliente = '".$usuario."') AND (fecha_creacion IS NULL)");
	        $stmt->execute();
	        $pedido = $stmt->fetchAll(PDO::FETCH_ASSOC);
	        

	        //Asignamos resultados
			$GLOBALS['resultado']->_result['modo'] = 'configuracion_precios';

    		//Salimos de la ejecucion
    		return;
    	}
 

		////////////////////////////////////////////////////////////////////////////////////
    	// MENSAJES
    	////////////////////////////////////////////////////////////////////////////////////
    	if($sv['modo'] == 'mensajes'){


	        //Asignamos resultados
			$GLOBALS['resultado']->_result['modo'] = 'mensajes';

    		//Salimos de la ejecucion
    		return;
    	}

		////////////////////////////////////////////////////////////////////////////////////
    	//CARRO DE COMPRAS
    	////////////////////////////////////////////////////////////////////////////////////
    	if($sv['modo'] == 'carro'){

	        //Usuario
	        $usuario = $GLOBALS['session']->getData('usuario');
	        $rol = $GLOBALS['session']->getData('rol');
	        $articulos = array();

	        //Obtenemos el pedido pendiente
	        $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT * FROM pedidos WHERE (cliente = '".$usuario."') AND (fecha_creacion IS NULL)");
	        $stmt->execute();
	        $pedido = $stmt->fetchAll(PDO::FETCH_ASSOC);

	        //Obtenemos descuento y condicion de venta
	        $descuento_general = 0;
		    $descuento_letra = NULL; 
	        
	        if($rol == 2){
		        $stmt = $GLOBALS['conf']['pdo']->prepare("	SELECT (
		        														SELECT 	descuento 
		        														FROM 	descuentos_cond_vta 
		        														WHERE 	id = (	SELECT 	condicion_venta 
		        																		FROM 	clientes 
		        																		WHERE 	usuario = '".$usuario."')
		        													) as descuento_general,
		        													descuento_letra

		        											FROM 	clientes 
		        											WHERE 	usuario = '".$usuario."'");
		        $stmt->execute();
		        $descuentos = $stmt->fetchAll(PDO::FETCH_ASSOC);

		        if(count($descuentos)){
		        	$descuento_general = $descuentos[0]['descuento_general'];
		        	$descuento_letra = $descuentos[0]['descuento_letra'];
		        }
	    	}

	        //Si tenemos el pedido Pendiente traemos los Articulos
	        if(count($pedido)){

	            $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT  articulo as id, articulo as quitar,
	            (SELECT CONCAT(prefijo,'-',codigo,'-',sufijo) FROM catalogo_articulos WHERE catalogo_articulos.id = articulo) as codigo,
	            CONCAT(
	            	IF(	(	SELECT nombre_singular 
	            			FROM 	catalogo_rubros 
	            			WHERE 	id = (	SELECT 	id_rubro 
	            							FROM 	catalogo_articulos 
	            							WHERE 	id = p.articulo)
	            		) IS NULL,'',
	            		
	            		(	SELECT 	nombre_singular 
	            			FROM 	catalogo_rubros 
	            			WHERE 	id = (SELECT id_rubro FROM catalogo_articulos WHERE id = p.articulo)
	            		)
	            	), ' ',

	            	(SELECT descripcion FROM catalogo_articulos WHERE catalogo_articulos.id = articulo), ' ',
	            	
	            	IF(	(	SELECT 	nombre 
	            			FROM 	catalogo_marcas 
	            			WHERE 	id = (	SELECT 	id_marca 
	            							FROM 	catalogo_articulos 
	            							WHERE 	id = p.articulo)
	            		) IS NULL,'',
	            		
	            		(	SELECT 	nombre 
	            			FROM 	catalogo_marcas 
	            			WHERE 	id = (SELECT id_marca FROM catalogo_articulos WHERE id = p.articulo)
	            		)
	            	)
	            	
	            ) as descripcion,
	            (SELECT precio FROM catalogo_articulos WHERE catalogo_articulos.id = articulo) as precio_lista,
	            (SELECT descuento FROM descuentos WHERE prefijo = (SELECT prefijo FROM catalogo_articulos WHERE id = p.articulo) AND cliente = '".$usuario."') as descuento_linea,
	            (SELECT descuento FROM catalogo_articulos_descuentos WHERE prefijo = (SELECT prefijo FROM catalogo_articulos WHERE id = p.articulo) AND codigo = (SELECT codigo FROM catalogo_articulos WHERE id = p.articulo) AND sufijo = (SELECT sufijo FROM catalogo_articulos WHERE id = p.articulo) AND letra = '".$descuento_letra."') as descuento_letra,
	            cantidad,
	            ( (SELECT precio FROM catalogo_articulos WHERE catalogo_articulos.id = articulo) * p.cantidad ) as total_lista
	            FROM    pedidos_articulos p
	            WHERE   pedido = (SELECT id FROM pedidos t WHERE (t.cliente = '".$usuario."') AND (t.fecha_creacion IS NULL))");

	            $stmt->execute();
	            $articulos = $stmt->fetchAll(PDO::FETCH_ASSOC);


	            //Generamos la columna de descuento del articulo
				foreach ($articulos as $k=>$a){

					//Inicializamos el costo
					$articulos[$k]['precio'] = $a['precio_lista'];

					//Si el descuento de la condicion de venta es mayor a 0
					if((double)$descuento_general > 0)
						$articulos[$k]['precio'] = $a['precio_lista'] - ($a['precio_lista'] * ((double)$descuento_general / 100));


					//Calculamos el precio de costo
					if(!is_null($a['descuento_linea'])){
						$articulos[$k]['precio'] = $articulos[$k]['precio'] - ($articulos[$k]['precio'] * ((double)$a['descuento_linea'] / 100));
					
					//Si tenemos alguna letra asignada
					}elseif($a['descuento_letra'] != NULL){
						$articulos[$k]['precio'] = $articulos[$k]['precio'] - ($articulos[$k]['precio'] * ((double)$a['descuento_letra'] / 100));
					}

					//Calculamos el precio total con descuentos
					$articulos[$k]['total'] = $articulos[$k]['precio'] * $articulos[$k]['cantidad'];
					$articulos[$k]['total'] = number_format($articulos[$k]['total'], 3, '.', '');
					$articulos[$k]['precio'] = number_format($articulos[$k]['precio'], 3, '.', '');

					//Borramos datos inecesarios
					unset($articulos[$k]['descuento_letra'], $articulos[$k]['descuento_linea']);
				}


	        //Sino creamos un nuevo pedido
	        }else{
	            $stmt = $GLOBALS['conf']['pdo']->query("INSERT INTO pedidos(cliente) VALUES ('".$usuario."')");

		        //Obtenemos el pedido pendiente
		        $stmt = $GLOBALS['conf']['pdo']->query("SELECT * FROM pedidos WHERE (cliente = '".$usuario."') AND (fecha_creacion IS NULL)");
		        $pedido = $stmt->fetchAll(PDO::FETCH_ASSOC);
	        }


	        //Asignamos resultados
	        $GLOBALS['resultado']->_result['cols'] = generaColumnas($sv);
	        $GLOBALS['resultado']->_result['rows'] = $articulos;
	        $GLOBALS['resultado']->_result['notas'] = $pedido[0]['notas'];
			$GLOBALS['resultado']->_result['modo'] = 'carro';

    		//Salimos de la ejecucion
    		return;
    	}



		////////////////////////////////////////////////////////////////////////////////////
    	//LISTAS DE PRECIO - AUMENTOS
    	////////////////////////////////////////////////////////////////////////////////////
    	if($sv['modo'] == 'listas'){

	        $aumentos = array();

	        //Obtenemos el usuario
	        $usuario = $GLOBALS['session']->getData('usuario');

	        //Consultamos Aumentos

	        $sql = "	SELECT 	id_marca,
	        					id_marca as fabricante,
	        					precio_update,
	        					(DATE_FORMAT(precio_update,'%d-%m-%Y')) as fecha,
								(SELECT nombre FROM catalogo_marcas WHERE id= a.id_marca) as fabricante_nombre,
								(SELECT COUNT(*) FROM catalogo_articulos WHERE (id_marca = a.id_marca) AND (precio_update = a.precio_update) AND (habilitado = 1)) as cantidad,
								( IF((SELECT COUNT(DISTINCT precio_porcentaje) FROM catalogo_articulos WHERE (id_marca = a.id_marca) AND (precio_update = a.precio_update)) > 1,'Promedio','Lineal') ) as tipo,
								(SELECT avg(precio_porcentaje) FROM catalogo_articulos WHERE (id_marca = a.id_marca) AND precio_update = a.precio_update) as porc,
								( SELECT CONCAT( CAST( ROUND(porc,1) AS CHAR) ,' (',tipo,')')) as porcentaje,
								( IF(
										(SELECT COUNT(*) FROM aumentos WHERE (a.precio_update = aumentos.fecha) AND (a.id_marca = aumentos.fabricante)) > 0,
										(SELECT descripcion FROM aumentos WHERE (a.precio_update = aumentos.fecha) AND (a.id_marca = aumentos.fabricante)),
										''
									) ) as descripcion,
								
								'' as acciones,
								0 as descarga,
								(SELECT descarga_por_rubro FROM aumentos WHERE (fabricante = a.id_marca) AND (fecha = a.precio_update)) as descarga_por_rubro,
								(IF(
									(SELECT descarga_por_rubro FROM aumentos WHERE (fabricante = a.id_marca) AND (fecha = a.precio_update)) = 1,
									0,
									(SELECT COUNT(*) FROM catalogo_articulos WHERE (id_marca = a.id_marca))
									)) as piezas_descargas
	        			FROM (

				        			SELECT DISTINCT 	id_marca, 
				        								precio_update
				        			FROM 				catalogo_articulos 
				        			WHERE 				(precio_update > '2015-12-04') AND 
														(id_marca IS NOT NULL) AND 
														(precio_anterior > 0.000) AND 
														habilitado = 1
									ORDER BY 			precio_update DESC
						) as a;";

			//Actualizamos Notificaciones
			$sql .= "UPDATE usuarios SET notificaciones_aumentos = NOW() WHERE usuario = '".$usuario."'";

	        $stmt = $GLOBALS['conf']['pdo']->prepare($sql);
	        $stmt->execute();
	        $aumentos = $stmt->fetchAll(PDO::FETCH_ASSOC);

	        //Quitamos si no hay modificacion
	        foreach ($aumentos as $i => $a){
	        	if(is_null($a['descarga_por_rubro']))
	        		$aumentos[$i]['descarga_por_rubro'] = 0;
	        	
	        	if($a['porc'] == '0.00000')
	        		unset($aumentos[$i]);
	        }

	        $aumentos = array_values($aumentos);

	        //Quitamos los aumentos que figuran con
	        //un 0.0 %
	        foreach($aumentos as $k=>$v)
	        	unset(	$aumentos[$k]['id_marca'], 
	        			$aumentos[$k]['tipo'], 
	        			$aumentos[$k]['porc']);
			
			//Arreglar cagada de Gabriel
			foreach($aumentos as $k=>$v)
				if(($aumentos[$k]['fabricante'] == "38") && ($aumentos[$k]['fecha'] == '11-05-2018'))
					$aumentos[$k]['porcentaje'] = "6.0 (Lineal)";
						
	        //Asignamos resultados
	        $GLOBALS['resultado']->_result['cols'] = generaColumnas($sv);
	        $GLOBALS['resultado']->_result['rows'] = $aumentos;
			$GLOBALS['resultado']->_result['modo'] = 'listas';

    		//Salimos de la ejecucion
    		return;
    	}



		////////////////////////////////////////////////////////////////////////////////////
    	//BUSQUEDA DE ARTICULOS
    	////////////////////////////////////////////////////////////////////////////////////
 

 
    	$GLOBALS['resultado']->_result['modo'] = $sv['modo'];
    	
		//Search conditionals..
		$condicionales = array();

		//Filtro Prefijo
		if($sv['advSearch_prefijo'] != '-1')
			$condicionales[] = " (prefijo = '".$sv['advSearch_prefijo']."' ) ";

		//Filtro Codigo
		if($sv['advSearch_codigo'] != '-1')
			$condicionales[] = " (codigo = '".$sv['advSearch_codigo']."' ) ";

		//Filtro Sufijo
		if($sv['advSearch_sufijo'] != '-1')
			$condicionales[] = " (sufijo = '".$sv['advSearch_sufijo']."' ) ";

		//Filtro Vehiculos
		$vehiculo = $sv['advSearch_vehiculo'];
		if($sv['advSearch_vehiculo'] != '-1'){
			//Explode marca and modelo
			$vehiculo = explode('#', $sv['advSearch_vehiculo']);

			//Make conditional
			$condtmp = "(id IN (SELECT articulo_id FROM catalogo_articulos_aplicaciones WHERE (marca_id = ".$vehiculo[0].")";

			//Add conditional for model...
			if($vehiculo[1] != '-1') $condtmp .= " AND (modelo_id = ".$vehiculo[1].")";

			//Finish sql conditional
			$condtmp .="))";

			//Add conditional
			$condicionales[] = $condtmp; 
		}

		//Filtro Rubro
		if($sv['advSearch_rubro'] != '-1'){

    		//CARGAMOS RUBROS
			$stmt = $GLOBALS['conf']['pdo']->prepare("SELECT id, nivel, padre FROM catalogo_rubros WHERE (id <> 1) ORDER BY orden");
			$stmt->execute();
			$rubros = $stmt->fetchAll(PDO::FETCH_ASSOC);
			$hijos = array_merge( (array)$sv['advSearch_rubro'] , $GLOBALS['toolbox']->traeRubrosHijos($rubros, $sv['advSearch_rubro']) );

			$condicionales[] = " ( id_rubro IN (".implode(',', $hijos).") ) ";
		}

		//Filtro Fabricante
		if($sv['advSearch_fabricante'] != '-1')
			$condicionales[] = " ( id_marca = ".$sv['advSearch_fabricante'].") ";

		//Filtro Dimensiones
		if( ($sv['advSearch_dimensiones'] != '-1') && (is_array($sv['advSearch_dimensiones'])) ){
			foreach($sv['advSearch_dimensiones'] as $k=>$d){
				//Si tiene seteado algun valor...
				if($d['value'] != '-1'){
					$condicionales[] = "( extra_".$d['id']." = '".$d['value']."' )";
				}
			}
		}



		//quicksearch
		if( ($sv['quicksearch_ultimos'] == '1') && is_array($sv['quicksearch_ids']) && count($sv['quicksearch_ids'])){
			$condicionales[] = "(id IN(".implode(',', $sv['quicksearch_ids'])."))";
			//$GLOBALS['session']->setData('quicksearch_ultimos','-1');
		}


		//Condicional para listar incorporaciones
		//Mustra incorporaciones en los últimos 30 dias
		if(!count($condicionales)) $condicionales[] = "(t.fecha_incorporacion >= DATE_SUB(CURDATE(), INTERVAL 1 MONTH))";


    	//Para Administradores y desarrollador mostramos todo
		if(($rol != 1) && ($rol != -1)) $condicionales[] = "( t.habilitado = 1 )";


		//SQL
		$sql = "SELECT 	id,
						CONCAT(prefijo,' - ',codigo,' - ',sufijo) as codigo,
						descripcion,
						id_rubro,
						( IF(t.id_rubro IS NOT NULL,(SELECT padre FROM catalogo_rubros WHERE id = t.id_rubro), NULL) ) as rubro_padre,
						habilitado,
						(SELECT COUNT(*) FROM catalogo_articulos_equiv WHERE id = (SELECT id FROM catalogo_articulos_equiv WHERE articulo_id = t.id LIMIT 1)) as equivalencias,
						(SELECT nombre FROM catalogo_marcas WHERE id = t.id_marca LIMIT 1) as marca,
						( IF( (SELECT COUNT(*) FROM catalogo_articulos_equiv_img WHERE articulo_id = t.id) > 0,
							(SELECT group_concat(CONCAT(prefijo,'-',codigo,'-',sufijo)) FROM catalogo_articulos WHERE id IN 
									(
										SELECT articulo_id 
	                                    FROM catalogo_articulos_equiv_img as g 
	                                    WHERE g.id = (SELECT e.id FROM catalogo_articulos_equiv_img as e WHERE e.articulo_id = t.id)
								        )
						), NULL)) as imgEq,
						precio";
		

		//Seleccionamos las dimensiones en caso de ser necesario
		if( ($sv['advSearch_busqueda_dim'] == '1') && ($sv['advSearch_dim_en_columnas'] == '1') && ($sv['advSearch_rubro'] != '-1') && ($sv['advSearch_rubro'] != ''))
			foreach($sv['advSearch_dimensiones'] as $k=>$d)
				//$sql .= ",extra_".$d['id'];
				$sql .= ",(IF( (SELECT COUNT(*) FROM catalogo_rubros_datos_opciones WHERE id_datos = ".$d['id'].") > 1, (SELECT nombre FROM catalogo_rubros_datos_opciones WHERE id = t.extra_".$d['id']."), IF(extra_".$d['id']." IS NULL, '', extra_".$d['id'].") )) as extra_".$d['id'];
				

		
		//$GLOBALS['resultado']->_result['debug'] = $sv;

		//Concat final SQL
		$sql .= " FROM catalogo_articulos t";

		//Si existen condicionales los agregamos
		if(count($condicionales)) $sql .= " WHERE ".implode(' AND ', $condicionales);

		//Ordenamos por ultimos 20 resultados
		if($sv['quicksearch_ultimos'] == '1')
			if(count($sv['quicksearch_ids']))
				$sql .= " ORDER BY FIELD(t.id, ".implode(',', $sv['quicksearch_ids']).")";

		//echo $sql;

		//Get the products from DB
		$stmt = $GLOBALS['conf']['pdo']->prepare($sql);
		$stmt->execute();
		$articulos = $stmt->fetchAll(PDO::FETCH_ASSOC);
		


		//Get Rubros
		$stmt = $GLOBALS['conf']['pdo']->prepare("SELECT id, nivel, padre, nombre FROM catalogo_rubros ORDER BY orden");
		$stmt->execute();
		$rubros = $stmt->fetchAll(PDO::FETCH_ASSOC);
		$rubros = $GLOBALS['toolbox']->orderRubros($rubros);

		//Get Datos
		$stmt = $GLOBALS['conf']['pdo']->prepare("SELECT id, rubro_id, nombre, cede_decendencia FROM catalogo_rubros_datos");
		$stmt->execute();
		$datos = $stmt->fetchAll(PDO::FETCH_ASSOC);

		//Count Datos
		foreach($rubros as $k=>$r){
			//print_r($r);
			$rubros[$k]['count_datos'] = 0;
			foreach($datos as $k1=>$d)
				if( ($r['id'] == $d['rubro_id']) || ( in_array( $d['rubro_id'], $r['padres'] )) )
					if(isset($rubros[$k]['count_datos'])) $rubros[$k]['count_datos']++;
		}
		/*echo $sql;
		var_dump($articulos);
		return;*/

		$vehiculos = array();

		if(count($articulos)){ 
		//Get Vehiculos
			$sql = "SELECT 	articulo_id as id,
						CONCAT(marca_id,'#',modelo_id) as cod,
                    	(IF(marca_id IS NULL,'[TODAS]',(SELECT nombre FROM catalogo_autos_marca WHERE id = marca_id))) as vehiculo_marca,
                    	(IF(modelo_id IS NULL,'[TODOS]',(SELECT nombre FROM catalogo_autos_modelo WHERE id = modelo_id))) as vehiculo_modelo,
                    	(IF(version_id IS NULL,'[TODOS]',(SELECT descripcion FROM catalogo_autos_versiones WHERE id = version_id))) as vehiculo_version,
						(CONCAT ( '[', IF( anio_inicio = -1 AND anio_final = -1, 'TODOS', CONCAT( IF( anio_inicio = -1, '--', SUBSTRING(anio_inicio,3,2)), ' / ', IF( anio_final = -1, '--', SUBSTRING(anio_final,3,2)))),']')) as vehiculo_anio

				 FROM 	catalogo_articulos_aplicaciones";

		
			$sql .= " WHERE articulo_id IN (";
			foreach($articulos as $k=>$a) { if($k>0) $sql .= ', '; $sql .= $a['id']; }
			$sql .= ")";
		


			//Make query
			$stmt = $GLOBALS['conf']['pdo']->prepare($sql);
			$stmt->execute();
			$vehiculos = $stmt->fetchAll(PDO::FETCH_ASSOC);
		}

		//Order Vehicles by product id
		$vh = array();
		foreach($vehiculos as $k=>$v)  	$vh[ $v['id'] ][] = $v;
		//Free memory
		unset($vehiculos);


		//Fotos...
		$fotos = array();

		if ( $handle = opendir('./articulos/') )
	        while (false !== ($entry = readdir($handle))) 
	            if ( ($entry != ".") && ($entry != "..") && ($entry != "_thumbs") )
	            	$fotos[$entry] = true;
	    

	    //Cerrar archivo
	    closedir($handle);

	    

		//Generate array of Products
		$art = array();
		foreach($articulos as $k=>$a){
			$art[$a['id']]['id'] = $a['id'];
			$art[$a['id']]['codigo'] = $a['codigo'];
			$art[$a['id']]['descripcion'] = $a['descripcion'];
			$art[$a['id']]['habilitado'] = $a['habilitado'];
			$art[$a['id']]['fabricante'] = $a['marca'];
			$art[$a['id']]['rubro_id'] = $a['id_rubro'];
			$art[$a['id']]['rubro_padre'] = $a['rubro_padre'];
			$art[$a['id']]['precio'] = $a['precio'];
			$art[$a['id']]['equivalencias'] = (int)$a['equivalencias'];

			//Save Categories and propertyes...
			$art[$a['id']]['rubro'] = '';
			$art[$a['id']]['rubro_count_dim'] = 0 ;
			foreach($rubros as $k1=>$r) 
				if($r['id'] == $a['id_rubro']){
					$art[$a['id']]['rubro'] = $r['nombre'];
					$art[$a['id']]['rubro_count_dim'] = $r['count_datos'];
				}

			//Si mostramos las aplicaciones en un Tooltip...
			if($sv['advSearch_vehiculo_filas_individuales'] == '-1'){
				if( isset($vh[$a['id']]) )
					foreach( $vh[$a['id']] as $k1=>$app){

						//Aplica a cualquier vehiculo
						if( ($app['vehiculo_marca'] == '[TODAS]') && ($app['vehiculo_modelo'] == '[TODOS]') && ($app['vehiculo_version'] == '[TODOS]'))
							$ve = "Universal (Cualquier vehículo)";

						//Aplica a todos los modelos de una marca
						elseif( ($app['vehiculo_marca'] != '[TODAS]') && ($app['vehiculo_modelo'] == '[TODOS]'))
							$ve = $app['vehiculo_marca']." (Todos los modelos)";

						//Aplica a una version en particular
						elseif( ($app['vehiculo_marca'] != '[TODAS]') && ($app['vehiculo_modelo'] != '[TODOS]') && ($app['vehiculo_version'] != '[TODOS]'))
							$ve = $app['vehiculo_marca']." ".$app['vehiculo_modelo']." ".$app['vehiculo_version'];

						//Aplica a todas las versiones de un modelo
						elseif( ($app['vehiculo_marca'] != '[TODAS]') && ($app['vehiculo_modelo'] != '[TODOS]') && ($app['vehiculo_version'] == '[TODOS]'))
							$ve = $app['vehiculo_marca']." ".$app['vehiculo_modelo'];

						//Agregamos año de ser necesario
						$ve .= ($app['vehiculo_anio']=='[TODOS]')? '': ' '.$app['vehiculo_anio'];
						
						//Lo agregamos al arreglo de vehiculos
						$art[$a['id']]['vehiculos'][] = $ve;
					}
			}

			
			//If we are making a dimensional search and need to show dimensions in individual columns....
			if( ($sv['advSearch_busqueda_dim'] == '1') && ($sv['advSearch_dim_en_columnas'] == '1') && ($sv['advSearch_rubro'] != '-1') ){
				//Foreach dimension in list...
				foreach($sv['advSearch_dimensiones'] as $k=>$d)
					$art[$a['id']]['extra_'.$d['id']] = $a['extra_'.$d['id']];
			}

			//TODOOOOOO
			//Generamos un arreglo para el gurpo de equivalencias
			if(!is_null($a['imgEq']) && (trim($a['imgEq']) != '')){
				$fotosEquiv = explode(',', $a['imgEq']);
			}else{
				$fotosEquiv = array(str_replace(' ', '', $a['codigo']));
			}
			


		   	// Armamos un arreglo donde especificamos el codigo del art. del grupo
		   	// y la cantidad de imagnes que tiene asignadas ese codigo de art.
		   	// de esta forma:
		   	// Fotos:
		   	//      [0] => codigo : '1-999-0', cant: 1
		   	//      [1] => codigo : '20-999-0', cant: 2
			//
			// En caso de no tener ninguna imagen asignada el elemento con key 'fotos'
			// no sera definido directamente.
		   	foreach ($fotosEquiv as $j=>$idArtEquiv)
			   	//Inicializamos el contador de imagenes
			   	for ($i=0; $i < 100; $i++)
			   		//Si existe la foto para el codigo del articulo
			   		if(!isset($fotos[ $idArtEquiv.'-'.$i.'.jpg' ])) {

			   			// Da la casualidad que la $i contiene la cantidad de imagenes encontradas
			   			// Mientras se hayan encontrado imagenes para este articulo se define el sub
			   			// array con el codigo del articulo y la cantidad de imagenes encontradas.
			   			if($i) $art[$a['id']]['fotos'][] = array( "cod" => $idArtEquiv, "cant" => $i);
			   			break;
			   		}



		}

		//If need to show each Vehicle in new line...
		if($sv['advSearch_vehiculo_filas_individuales'] == '1'){
			
			//Foreach Art
			foreach($art as $k=>$a){
				if( isset($vh[$k]) ){
					foreach( $vh[$k] as $k1=>$app){

						$cod = explode('#', $app['cod']);

						//if isset marca vehicle...
						if( ((is_array($vehiculo)) && ( $cod[0] == $vehiculo[0] )) || ($vehiculo == '-1') ){
							//echo "dump bar";
							$GLOBALS['resultado']->_result['rows'][] = $a + $app;
						}
					}
				}
			}
		}else{
			$GLOBALS['resultado']->_result['rows'] = $art;
		}

		//Reset Keys...
		$GLOBALS['resultado']->_result['rows'] = array_values($GLOBALS['resultado']->_result['rows']);

		//Generate cols...
		$GLOBALS['resultado']->_result['cols'] = generaColumnas($sv);

		//Display individual vehicles
		$GLOBALS['resultado']->_result['individual_vehicle_rows'] = $sv['advSearch_vehiculo_filas_individuales'];

		//Display individual vehicles
		$GLOBALS['resultado']->_result['dim_en_columnas'] = $sv['advSearch_dim_en_columnas'];

		//If all descriptions all empty
		foreach($GLOBALS['resultado']->_result['rows'] as $k=>$r)
			if( $r['descripcion'] != '' ){
				$GLOBALS['resultado']->_result['cols']['descripcion']['hidden'] = false;
				break;
			}
    }

	/** 
	* @nombre: Obtiene Dimensiones
	* @descripcion: Obtiene lista de dimensiones de un articulo entregando su ID.
	*/
    public function get_dimensiones(){

		//DB Query
		$stmt = $GLOBALS['conf']['pdo']->prepare("SELECT * FROM catalogo_articulos WHERE id = ".$GLOBALS['parametros']['articulo']);
		$stmt->execute();
		$articulo = $stmt->fetch(PDO::FETCH_ASSOC);


		//If the product have category associed..
		if( ($articulo['id_rubro'] != NULL) AND ($articulo['id_rubro'] != '') ){

			//Get Rubros
			$stmt = $GLOBALS['conf']['pdo']->prepare("SELECT id, nivel, padre, nombre FROM catalogo_rubros ORDER BY orden");
			$stmt->execute();
			$rubros = $stmt->fetchAll(PDO::FETCH_ASSOC);
			$rubros = $GLOBALS['toolbox']->orderRubros($rubros);
		
			$padres = array();

			//Search 'Rubro Padres'
			foreach($rubros as $k=>$r)
				if($r['id'] == $articulo['id_rubro']){
					$padres = $r['padres'];
					break;
				}

			//Get Dimensions
			$stmt = $GLOBALS['conf']['pdo']->prepare("	SELECT 	id, 
																orden, 
																rubro_id, 
																nombre, 
																cede_decendencia,
																(SELECT COUNT(*) FROM catalogo_rubros_datos_opciones WHERE id_datos = catalogo_rubros_datos.id) as opc

														FROM 	catalogo_rubros_datos 
														WHERE 	(rubro_id IN (".implode(',', $padres).") AND cede_decendencia = 'Y') OR (rubro_id = ".$articulo['id_rubro'].") 
														ORDER BY orden");
			$stmt->execute();
			$dimensiones = $stmt->fetchAll(PDO::FETCH_ASSOC);

			//Get options
			foreach($dimensiones as $k=>$d){
				$dimensiones[$k]['opciones'] = array();
				if($d['opc']){
					$stmt = $GLOBALS['conf']['pdo']->prepare("SELECT id, nombre FROM catalogo_rubros_datos_opciones WHERE id_datos = :dato");
					$stmt->bindParam(':dato', $d['id'], PDO::PARAM_INT); 
					$stmt->execute();
					$dimensiones[$k]['opciones'] = $stmt->fetchAll(PDO::FETCH_ASSOC);
				}
			}

			//Generate Final label and value array
			$dim = array();
			foreach($dimensiones as $k=>$d)
				if( trim($articulo['extra_'.$d['id']]) != ''){
					
					//Define the label
					$tmp['nombre'] = $d['nombre'];

					//Define value
					if($d['opc']){ 
						foreach($d['opciones'] as $k1=>$o) 
							if( $articulo['extra_'.$d['id']] == $o['id'])
								$tmp['valor'] = $o['nombre'];
					}else{
						$tmp['valor'] = $articulo['extra_'.$d['id']];
					}

					//Add to dimension array..
					$dim[] = $tmp;
				}


			//Generate Result
			$GLOBALS['resultado']->_result = '<table style="margin: 10px;"><tbody>';
			
			//Run across Dimensions Array
			foreach($dim as $k=>$d){
				$GLOBALS['resultado']->_result .= '<tr><td style="text-align: right; "><b>'.$d['nombre'].':</b></td><td style="text-align: left;">&nbsp;'.$d['valor']."</td></tr>";
			}

			//If no dimensions in array
			if(!count($dim)) $GLOBALS['resultado']->_result .= '<tr><td style="text-align: center; "><b>Sin Dimensiones</b></td></tr>';

			//Close table
			$GLOBALS['resultado']->_result .= '</tbody></table>';


		//If id NO EXIST
		}else{
			$GLOBALS['resultado']->setError('EL id es inexistente');
		}

    }

	/** 
	* @nombre: Obtiene Equivalencias
	* @descripcion: Obtiene lista de equivalencias de un articulo entregando su ID.
	*/
    public function get_equivalencias(){

		//Get Equivalencias
		$stmt = $GLOBALS['conf']['pdo']->prepare("	SELECT articulo_id,
														(
															IF( (articulo_id IS NULL) AND (codigo_original IS NOT NULL), 
																codigo_original,
																(SELECT CONCAT(prefijo,'-',codigo,'-',sufijo) FROM catalogo_articulos WHERE id = articulo_id))) as codigo,
														(SELECT nombre FROM catalogo_marcas WHERE id = marca_id) as marca

													FROM 
														catalogo_articulos_equiv
													WHERE
														id = (SELECT id FROM catalogo_articulos_equiv WHERE articulo_id = ".$GLOBALS['parametros']['articulo'].") 

													ORDER BY marca");

		$stmt->execute();
		$equivalencias = $stmt->fetchAll(PDO::FETCH_ASSOC);


		foreach ($equivalencias as $k=>$e)
			if($e['articulo_id'] == $GLOBALS['parametros']['articulo'])
				unset($equivalencias[$k]);

		//Ordenamos de forma natural antes de mostrar
		uasort($equivalencias, function($a, $b) {
			$a1 = $a['marca'].' '.$a['codigo'];
			$b1 = $b['marca'].' '.$b['codigo'];
		    return strnatcmp($a1, $b1);
		});

		//Generate Result
		$GLOBALS['resultado']->_result = '<table style="margin: 10px;"><tbody>';
		
		//Run across Dimensions Array
		foreach($equivalencias as $k=>$e){
			$GLOBALS['resultado']->_result .= '<tr><td style="text-align: right;"><b>'.$e['marca'].'</b></td><td style="text-align: left; margin-left: 10px;">&nbsp;&nbsp;'.$e['codigo']."</td></tr>";
		}

		//If no dimensions in array
		if(!count($equivalencias)) $GLOBALS['resultado']->_result .= '<tr><td style="text-align: center;"><b>Sin Equivalencias</b></td></tr>';

		//Close table
		$GLOBALS['resultado']->_result .= '</tbody></table>';
    }

	/** 
	* @nombre: Obtiene Detalles del articulo
	* @descripcion: Obtiene todos los detalles de un articulo
	*/
    public function get_detalles(){

    	//Conseguimos todos los datos del articulo optimizamos todo en una sola consulta
		$id = $GLOBALS['parametros']['articulo'];
		$usuario = $GLOBALS['session']->getData('usuario');
		$rol = $GLOBALS['session']->getData('rol');

		//Pedimos los parametros para este Modulo / Accion
		$sql = "	SELECT 	*, 
							(SELECT nombre from catalogo_marcas WHERE id = catalogo_articulos.id_marca) as nombre_marca,
							(SELECT nombre_singular FROM catalogo_rubros WHERE id = catalogo_articulos.id_rubro) as nombre_rubro
					FROM 	catalogo_articulos 
					WHERE 	id = ".$id.";";

		//$sql .= "SET @rubro_id = (SELECT id_rubro FROM catalogo_articulos WHERE id=".$id.");";

		$sql .= " SELECT id, padre, nombre FROM catalogo_rubros;";


		$sql .= "	SELECT articulo_id,
						(
							IF( (articulo_id IS NULL) AND (codigo_original IS NOT NULL), 
								codigo_original,
								(SELECT CONCAT(prefijo,'-',codigo,'-',sufijo) FROM catalogo_articulos WHERE id = articulo_id))) as codigo,
						(SELECT nombre FROM catalogo_marcas WHERE id = marca_id) as marca,
						(SELECT habilitado FROM catalogo_articulos WHERE id = articulo_id) as habilitado,
						TRUNCATE((SELECT precio FROM catalogo_articulos WHERE id = articulo_id),2) as precio


					FROM 
						catalogo_articulos_equiv
					WHERE
						id = (SELECT id FROM catalogo_articulos_equiv WHERE articulo_id = ".$id." LIMIT 1) AND (articulo_id IS NOT NULL)

					ORDER BY precio ASC;";

		$sql .= "SELECT articulo_id as id,
						CONCAT(marca_id,'#',modelo_id) as cod,
                    	(IF(marca_id IS NULL,'',(SELECT nombre FROM catalogo_autos_marca WHERE id = marca_id))) as vehiculo_marca,
                    	(IF(modelo_id IS NULL,'',(SELECT nombre FROM catalogo_autos_modelo WHERE id = modelo_id))) as vehiculo_modelo,
                    	(IF(version_id IS NULL,'',(SELECT descripcion FROM catalogo_autos_versiones WHERE id = version_id))) as vehiculo_version,
						(CONCAT ( '[', IF( anio_inicio = -1 AND anio_final = -1, 'TODOS', CONCAT( IF( anio_inicio = -1, '--', SUBSTRING(anio_inicio,3,2)), ' / ', IF( anio_final = -1, '--', SUBSTRING(anio_final,3,2)))),']')) as vehiculo_anio

				 FROM 	catalogo_articulos_aplicaciones
				 WHERE  articulo_id = ".$id.";";
		

		//Compartimos las imagenes del articulo mismo
		//y de las equivalencias con las que comparte imagenes
		$sql .= "(SELECT CONCAT(prefijo,'-',codigo,'-',sufijo) as codigo FROM catalogo_articulos WHERE id IN (
					SELECT 	articulo_id 
					FROM 	catalogo_articulos_equiv_img 
					WHERE 	id = (SELECT id FROM catalogo_articulos_equiv_img WHERE articulo_id = ".$id.")) AND id <> ".$id.")
				UNION (SELECT CONCAT(prefijo,'-',codigo,'-',sufijo) as codigo FROM catalogo_articulos WHERE id = ".$id.");";

		//Mostrar precios
		$sql .= " SELECT mostrar_precio_lista, mostrar_precio_costo, mostrar_precio_venta, ajuste, precio_venta FROM usuarios WHERE usuario = '".$usuario."';";

		//Pedimos descuentos
		$sql .= " 	SELECT 		* 
					FROM 		catalogo_articulos_descuentos 
					WHERE 		(prefijo = (SELECT prefijo FROM catalogo_articulos WHERE id = ".$id.")) AND 
								(codigo = (SELECT codigo FROM catalogo_articulos WHERE id = ".$id.")) AND 
								(sufijo = (SELECT sufijo FROM catalogo_articulos WHERE id = ".$id."));";

		//Obtenemos los datos para
		//calcular el precio
		if($rol == 2){
			$sql .= " SELECT condicion_venta, descuento_letra, (SELECT descuento FROM descuentos_cond_vta WHERE id = condicion_venta) as descuento FROM clientes WHERE usuario = '".$usuario."';";
			$sql .= " SELECT descuento FROM descuentos WHERE cliente = '".$usuario."' AND prefijo = (SELECT prefijo FROM catalogo_articulos WHERE id = ".$id.");";
		}
		
		//Obtenemos los resultados de la consulta
		//Para los permisos a este Usuario y Rol
		$stmt = $GLOBALS['conf']['pdo']->query($sql);
		$articulo = $stmt->fetchAll(PDO::FETCH_ASSOC);
		$articulo = $articulo[0];
		$stmt->nextRowset();
		$rubros = $stmt->fetchAll(PDO::FETCH_ASSOC);
		$stmt->nextRowset();
		$equivalencias = $stmt->fetchAll(PDO::FETCH_ASSOC);
		$stmt->nextRowset();
		$aplicaciones = $stmt->fetchAll(PDO::FETCH_ASSOC);
		$stmt->nextRowset();
		$fotos_equiv = $stmt->fetchAll(PDO::FETCH_COLUMN);
		$stmt->nextRowset();
		$mostrar_precios = $stmt->fetchAll(PDO::FETCH_ASSOC);
		$mostrar_precios = $mostrar_precios[0];
		$stmt->nextRowset();
		$descuentos_art = $stmt->fetchAll(PDO::FETCH_ASSOC);

		if($rol == 2){
			$stmt->nextRowset();
			$venta = $stmt->fetchAll(PDO::FETCH_ASSOC);
			$venta = $venta[0];

			//$GLOBALS['resultado']->_result['debugg'] = $precio;

			$stmt->nextRowset();
			$descuentos = $stmt->fetchAll(PDO::FETCH_ASSOC);
			$GLOBALS['resultado']->_result['descuentos'] = $descuentos;
		}

		//El precio de lista lo conocemos
		$GLOBALS['resultado']->_result['precio'] = number_format($articulo['precio'], 2);
		$GLOBALS['resultado']->_result['precio_iva'] = number_format($articulo['precio']*1.21, 2);

		$precios = array();
		$precios['mostrar']['lista'] = (int)$mostrar_precios['mostrar_precio_lista'];
		$precios['mostrar']['costo'] = (int)$mostrar_precios['mostrar_precio_costo'];
		$precios['mostrar']['venta'] = (int)$mostrar_precios['mostrar_precio_venta'];
		$precios['lista'] = $articulo['precio'];
		$precios['lista_iva'] = $articulo['precio']*1.21;
		$precios['costo'] = $precios['lista'];
		$precios['costo_iva'] = $precios['costo']*1.21;



		if($rol == 2){
			
			//Si el descuento de la condicion de venta es mayor a 0
			if((double)$venta['descuento'] > 0){
				$precios['costo'] = $precios['costo'] - ($precios['costo'] * ((double)$venta['descuento'] / 100));
				$precios['costo_iva'] = $precios['costo'] * 1.21;
			}


			//Calculamos el precio de costo
			if(count($descuentos)){

				$precios['costo'] = $precios['costo'] - ($precios['costo'] * ((double)$descuentos[0]['descuento'] / 100));
				$GLOBALS['resultado']->_result['costo1'] =  $precios['costo'];
				$precios['costo_iva'] = $precios['costo'] * 1.21;
		

			}else{

				//Si el cliente tiene asignada una letra
				if($venta['descuento_letra'] != NULL){

					//Obtenemos el descuento a aplicar
					$descuento_articulo = 0;
					foreach ($descuentos_art as $z=>$p)
						if($p['letra'] == $venta['descuento_letra']){
							$descuento_articulo = $p['descuento'];
							break;
						}

					//Aplicamos el descuento
					if((double)$descuento_articulo > 0){
						$precios['costo'] = $precios['costo'] - ($precios['costo'] * ((double)$descuento_articulo / 100));
						$precios['costo_iva'] = $precios['costo'] * 1.21;
					}


				}

			}
		}


		//Ahoa calculamos el precio de Venta:
		//Calculamos sobre el precio de lista
		if($mostrar_precios['precio_venta'] == 1){
			
			if((double)$mostrar_precios['ajuste'] > 0)
				$precios['venta'] = $precios['lista'] + ($precios['lista'] * ( abs((double)$mostrar_precios['ajuste']) / 100));
			else
				$precios['venta'] = $precios['lista'] - ($precios['lista'] * ( abs((double)$mostrar_precios['ajuste']) / 100));
			
			//Calculamos el iva
			$precios['venta_iva'] = $precios['venta']*1.21;
			
		}else{

			if((double)$mostrar_precios['ajuste'] > 0)
				$precios['venta'] = $precios['costo'] + ($precios['costo'] * ((double)$mostrar_precios['ajuste'] / 100));
			else
				$precios['venta'] = $precios['costo'] - ($precios['costo'] * ((double)$mostrar_precios['ajuste'] / 100));
			
			//Calculamos el iva
			$precios['venta_iva'] = $precios['venta']*1.21;
		}

		$GLOBALS['resultado']->_result['costo1_iva'] =  $precios['costo_iva'];

		//Damos formato a dos decimales
		$precios['venta'] = number_format($precios['venta'], 2);
		$precios['venta_iva'] = number_format($precios['venta_iva'], 2);
		$precios['lista'] = number_format($precios['lista'], 2);
		$precios['lista_iva'] = number_format($precios['lista_iva'], 2);
		$precios['costo'] = number_format($precios['costo'], 2);
		$precios['costo_iva'] = number_format($precios['costo_iva'],2);



		$GLOBALS['resultado']->_result['precios'] = $precios;

		//Ordenamos los rubros
		$rubros = $GLOBALS['toolbox']->orderRubros($rubros);

		//Conseguimos los rubros padres
		$padres = array();
		$padres = $GLOBALS['toolbox']->traeRubrosPadres($rubros, $articulo['id_rubro']);

		$datos_heredados = "";
		if(count($padres)) $datos_heredados = "(rubro_id IN (".implode(',', $padres).") AND cede_decendencia = 'Y') OR ";

		//Si no tiene rubro es NULL
		if(is_null($articulo['id_rubro'])) $articulo['id_rubro']= "NULL";

		$sql = "
			SELECT 	id, 
					orden, 
					rubro_id, 
					nombre, 
					cede_decendencia,
					(SELECT COUNT(*) FROM catalogo_rubros_datos_opciones WHERE id_datos = catalogo_rubros_datos.id) as opc 

			FROM 	catalogo_rubros_datos 
			WHERE 	(rubro_id = ".$articulo['id_rubro'].")
			ORDER BY orden
		";

		if(count($padres))
			$sql = "(SELECT id, 
							orden, 
							rubro_id, 
							nombre, 
							cede_decendencia,
							(SELECT COUNT(*) FROM catalogo_rubros_datos_opciones WHERE id_datos = catalogo_rubros_datos.id) as opc 

					FROM 	catalogo_rubros_datos 
					WHERE 	(rubro_id IN (".implode(',', $padres).") AND cede_decendencia = 'Y')
					ORDER BY orden)

					UNION 
					
					(".$sql.")";

		//Get Dimensions
		$stmt = $GLOBALS['conf']['pdo']->prepare($sql);
		$stmt->execute();
		$dimensiones = $stmt->fetchAll(PDO::FETCH_ASSOC);



		//Get options
		foreach($dimensiones as $k=>$d){
			$dimensiones[$k]['opciones'] = array();
			if($d['opc']){
				$stmt = $GLOBALS['conf']['pdo']->prepare("SELECT id, nombre FROM catalogo_rubros_datos_opciones WHERE id_datos = :dato");
				$stmt->bindParam(':dato', $d['id'], PDO::PARAM_INT); 
				$stmt->execute();
				$dimensiones[$k]['opciones'] = $stmt->fetchAll(PDO::FETCH_ASSOC);
			}
		}

		//Generate Final label and value array
		$dim = array();
		foreach($dimensiones as $k=>$d)
			if( trim($articulo['extra_'.$d['id']]) != ''){
				
				//Define the label
				$tmp['nombre'] = $d['nombre'];

				//Define value
				if($d['opc']){ 
					foreach($d['opciones'] as $k1=>$o) 
						if( $articulo['extra_'.$d['id']] == $o['id'])
							$tmp['valor'] = $o['nombre'];
				}else{
					$tmp['valor'] = $articulo['extra_'.$d['id']];
				}

				//Add to dimension array..
				$dim[] = $tmp;
			}

		//Fotos...
		$fotos_archivo = array();
		if ( $handle = opendir('./articulos/') )
	        while (false !== ($entry = readdir($handle))) 
	            if ( ($entry != ".") && ($entry != "..") && ($entry != "_thumbs") )
	            	$fotos_archivo[$entry] = true;
	    //Cerrar archivo
	    closedir($handle);



	    $fotos = array();
	    //Por cada uno de los articulos que comparte imagenes
	    foreach ($fotos_equiv as $k=>$c){
	    	for ($i=0; $i < 100; $i++){
	    		//Si encuentra la imagen sigue
	    		$img = $c.'-'.$i.'.jpg';
	    		if(isset($fotos_archivo[$img])) $fotos[] = $img;
	    		else break;
	    	}
	    }

	    $GLOBALS['resultado']->_result['rubro_id'] = $articulo['id_rubro'];
		$GLOBALS['resultado']->_result['codigo'] = $articulo['prefijo'].'-'.$articulo['codigo'].'-'.$articulo['sufijo'];
		$GLOBALS['resultado']->_result['rubro'] = $articulo['nombre_rubro'];
		$GLOBALS['resultado']->_result['fabricante'] = $articulo['nombre_marca'];
		$GLOBALS['resultado']->_result['descripcion'] = $articulo['descripcion'];
		$GLOBALS['resultado']->_result['fotos'] = $fotos;

		//Corregimos las Aplicaciones
		foreach($aplicaciones as $k=>$v){
			if( ($v['vehiculo_marca'] == '') && ($v['vehiculo_modelo'] == '') && ($v['vehiculo_version'] == '') )
				$aplicaciones[$k]['label'] = "Universal [Cualquier vehículo]";
			else if( ($v['vehiculo_marca'] != '') && ($v['vehiculo_modelo'] == '') )
				$aplicaciones[$k]['label'] = $v['vehiculo_marca'] . " [Todos los modelos]";
			else{
				$aplicaciones[$k]['label'] = $v['vehiculo_marca'] . " " .$v['vehiculo_modelo'] . " " . $v['vehiculo_version'];

				if($v['vehiculo_anio'] != '[TODOS]') $aplicaciones[$k]['label'] .= " " . $v['vehiculo_anio'];
			}
		}

		//Guardamos las dimensiones
		$GLOBALS['resultado']->_result['aplicaciones'] = "";
		foreach($aplicaciones as $k=>$v)
			$GLOBALS['resultado']->_result['aplicaciones'] .= $v['label'] ."<br>";


		//Guardamos las dimensiones
		$GLOBALS['resultado']->_result['dimensiones'] = "";
		foreach($dim as $k=>$v)
			$GLOBALS['resultado']->_result['dimensiones'] .= "<p><b>" . $v['nombre'] . ":</b> ".$v['valor']."</p>";


		//Quitamos este mismo articuloo de la lista
		foreach ($equivalencias as $k=>$e)
			if($e['articulo_id'] == $id)
				unset($equivalencias[$k]);

		//Gardamos las equivalencias
		$GLOBALS['resultado']->_result['equivalencias'] = array();
		foreach($equivalencias as $k=>$v)
			if((int)$v['habilitado'])
				$GLOBALS['resultado']->_result['equivalencias'][] = array($v['articulo_id'], $v['marca'], $v['codigo'], $v['precio'], $v['habilitado']);

		$GLOBALS['resultado']->_result['modo'] = $GLOBALS['session']->getData('modo');

    }




    	/** 
	* @nombre: Obtiene Detalles del articulo
	* @descripcion: Obtiene todos los detalles de un articulo
	*/
    public function get_detalless(){

    	//Conseguimos todos los datos del articulo optimizamos todo en una sola consulta
		$articulos = ["178","1064","1440","1815","3300","3550","3968","3975","4211","4705","4750","4752","5297","5344","5602","6572","9000","9680","KIT07","KIT09","KIT12","KIT15","KIT23","KIT27"];

		$articulos2 = ["161","168","549","588","638","688","713","728","738","746","764","769","788","805","825","839","849","910","929","930","942","954","980","981","983","1033","1034","1037","1075","1087","1090"];



    	//Conseguimos todos los datos del articulo optimizamos todo en una sola consulta
		foreach ($articulos as $arti => $articulo) {

			$sql =	"SELECT articulo_id as id,
					CONCAT(marca_id,'#',modelo_id) as cod,
					(IF(marca_id IS NULL,'',(SELECT nombre FROM catalogo_autos_marca WHERE id = marca_id))) as vehiculo_marca,
					(IF(modelo_id IS NULL,'',(SELECT nombre FROM catalogo_autos_modelo WHERE id = modelo_id))) as vehiculo_modelo,
					(IF(version_id IS NULL,'',(SELECT descripcion FROM catalogo_autos_versiones WHERE id = version_id))) as vehiculo_version,
					(CONCAT ( '[', IF( anio_inicio = -1 AND anio_final = -1, 'TODOS', CONCAT( IF( anio_inicio = -1, '--', SUBSTRING(anio_inicio,3,2)), ' / ', IF( anio_final = -1, '--', SUBSTRING(anio_final,3,2)))),']')) as vehiculo_anio

					FROM 	catalogo_articulos_aplicaciones
					WHERE  articulo_id = (SELECT id FROM catalogo_articulos WHERE (prefijo = '83') AND (codigo='".$articulo."'));";

			$stmt = $GLOBALS['conf']['pdo']->query($sql);
			$aplicaciones = $stmt->fetchAll(PDO::FETCH_ASSOC);

			/*
			//Corregimos las Aplicaciones
			foreach($aplicaciones as $k=>$v){
				$aplicaciones[$k]['label'] = $v['vehiculo_marca'] . " " .$v['vehiculo_modelo'] . " " . $v['vehiculo_version'];
				$aplicaciones[$k]['label_modelo'] = $v['vehiculo_modelo'] . " " . $v['vehiculo_version'];

				if($v['vehiculo_anio'] != '[TODOS]') {
					$aplicaciones[$k]['label'] .= " " . $v['vehiculo_anio'];
					$aplicaciones[$k]['label_modelo'] .= " " . $v['vehiculo_anio'];
				}
			}*/
			/*

			$marcas = array();
			foreach ($aplicaciones as $aplicacion_)
				$marcas[$aplicacion_['vehiculo_marca']][] = $aplicacion_['label_modelo'];
			
			foreach ($marcas as $marca => $modelos)
				echo $marca .' ' . implode(', ', $modelos);
			

			echo "<br/>"; */


		}




    }



}
?>