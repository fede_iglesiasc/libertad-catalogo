<?php
/**
 * @nombre: Parametros de la API
 * @descripcion: Parametros a las funciones de la API.
 */
class parametrosAPI extends module{


    /*
	 * Constructor
	 */
    public function __construct(){
    }

    /**
     * Validaciones previas
     */
    public function __pre($accion){

        //Si existe el parametro parametro...
        if(isset($GLOBALS['parametros']['parametro'])){
            $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT id FROM parametros WHERE id = ".$GLOBALS['parametros']['parametro']);
            $stmt->execute();
            $parametro = $stmt->rowCount();
        }

        //Si existe el parametro accion...
        if(isset($GLOBALS['parametros']['accion'])){
            $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT id FROM api WHERE id = ".$GLOBALS['parametros']['accion']);
            $stmt->execute();
            $api = $stmt->rowCount();
        }

        //Si no existe el parametro
        if( isset($parametro) && !$parametro && 
            in_array($accion, array('info','eliminar','editar','editar_caracteres','vincular_parametro','desvincular_parametro','editar_procesos'))){
            $GLOBALS['resultado']->setError("El Parametro no existe.");
            return;
        }

        //Si no existe el parametro
        if( isset($api) && !$api 
            && in_array($accion, array('listar','agregar','vincular_parametro','desvincular_parametro'))){
            $GLOBALS['resultado']->setError("La Acción no existe.");
            return;
        }

        //llamamos a la accion
        return call_user_func_array(array($this, $accion), array());
    }


    /**
     * @nombre: Informacion del Parametro
     * @descripcion: Pasamos id del Parametro y nos devuelve su Configuración.
     */
    public function info(){

        //Obtenemos los usuarios
        $stmt = $GLOBALS['conf']['pdo']->prepare("  SELECT  *
                                                    FROM    parametros
                                                    WHERE   id = ".$GLOBALS['parametros']['parametro']);
        $stmt->execute();
        $datos = $stmt->fetch(PDO::FETCH_ASSOC);

        // 'NULL' => ''
        foreach($datos as $k=>$v)
            if(is_null($v)) $datos[$k] = '';
        
        //Guardamos los datos
        $GLOBALS['resultado']->_result = $datos;
    }

    /**
     * @nombre: Lista Parametros de la API (completo)
     * @descripcion: Listado de Parametros de la API completo.
     */
    public function listar(){

        //SQL
        $stmt = $GLOBALS['conf']['pdo']->prepare("  SELECT  *
                                                    FROM    parametros
                                                    WHERE   id IN ( SELECT  parametro_id 
                                                                    FROM    parametros_api 
                                                                    WHERE   api_id = ".$GLOBALS['parametros']['accion'].")");
        $stmt->execute();
        $parametrosAPI =  $stmt->fetchAll(PDO::FETCH_ASSOC);



        //Generamos los resultados
        $items = array();
        foreach($parametrosAPI as $k=>$p){
            //Item
            $item = array();
            $item['id'] = $p['id'];
            $item['label'] = $p['label'];
            $item['obligatorio'] = $p['obligatorio'];
            $item['identificador'] = $p['identificador'];
            $item['label_articulo'] = $p['label_articulo'];
            $item['descripcion'] = $p['descripcion'];
            $item['filtro'] = $GLOBALS['toolbox']->generaFiltroParametro($p);

            //Agregamos el item a los resultados
            $items[] = $item;
        }

        //Asignamos resultados
        $GLOBALS['resultado']->_result = $items;
    }

    /**
     * @nombre: Agregar un Parametro
     * @descripcion: Agrega un Parametro, pasándole el nombre de Rol.
     */
    public function agregar(){

        // Begin Transaction
        $GLOBALS['conf']['pdo']->beginTransaction();

        try {

            //Creamos el Parámetro
            $stmt = $GLOBALS['conf']['pdo']->prepare("  INSERT INTO parametros (identificador, label, label_articulo, descripcion, obligatorio)  
                                                        VALUES ('".$GLOBALS['parametros']['identificador']."',
                                                                '".$GLOBALS['parametros']['label']."',
                                                                '".$GLOBALS['parametros']['label_articulo']."',
                                                                '".$GLOBALS['parametros']['descripcion']."',
                                                                ".$GLOBALS['parametros']['obligatorio'].")");
            $stmt->execute();

            $parametro = $GLOBALS['conf']['pdo']->lastInsertId();


            //Lo vinculamos a la API
            $stmt = $GLOBALS['conf']['pdo']->prepare("  INSERT INTO parametros_api (api_id, parametro_id) VALUES 
                                                        (".$GLOBALS['parametros']['accion'].",".$parametro.")");
            $stmt->execute();

            // Commit Transaction
            $GLOBALS['conf']['pdo']->commit();

        } // Si existieron errores generando las Querys de arriba...
        catch (PDOException $e) {
            // hacemos un Rollback
            $GLOBALS['conf']['pdo']->rollback();

            //Agregamos error 
            $GLOBALS['resultado']->setError($e);
            
            exit;
        }
    }

    /**
     * @nombre: Editar un Parametro
     * @descripcion: Edita un Parametro, pasándole el nombre de Rol y su id.
     */
    public function editar(){

        //Query DB
        $stmt = $GLOBALS['conf']['pdo']->prepare("  UPDATE  parametros 
                                                    SET     identificador='".$GLOBALS['parametros']['identificador']."',
                                                            label='".$GLOBALS['parametros']['label']."',
                                                            label_articulo='".$GLOBALS['parametros']['label_articulo']."',
                                                            obligatorio=".$GLOBALS['parametros']['obligatorio'].",
                                                            descripcion='".$GLOBALS['parametros']['descripcion']."'

                                                    WHERE id=".$GLOBALS['parametros']['parametro']);
        $stmt->execute();
    }

    /**
     * @nombre: Editar los Caracteres del Parametro
     * @descripcion: Edita los caracteres habilitados para este parametro.
     */
    public function editar_caracteres(){


        $stmt = $GLOBALS['conf']['pdo']->prepare("  UPDATE  parametros 
                                                    SET     caracteres_minusculas=".$GLOBALS['parametros']['caracteres_minusculas'].",
                                                            caracteres_mayusculas=".$GLOBALS['parametros']['caracteres_mayusculas'].",
                                                            caracteres_acentos=".$GLOBALS['parametros']['caracteres_acentos'].",
                                                            caracteres_numeros=".$GLOBALS['parametros']['caracteres_numeros'].",
                                                            caracteres_corchetes=".$GLOBALS['parametros']['caracteres_corchetes'].",
                                                            caracteres_parentesis=".$GLOBALS['parametros']['caracteres_parentesis'].",
                                                            caracteres_llaves=".$GLOBALS['parametros']['caracteres_llaves'].",
                                                            caracteres_espacios=".$GLOBALS['parametros']['caracteres_espacios'].",
                                                            caracteres_puntos=".$GLOBALS['parametros']['caracteres_puntos'].",
                                                            caracteres_comas=".$GLOBALS['parametros']['caracteres_comas'].",
                                                            caracteres_puntosycomas=".$GLOBALS['parametros']['caracteres_puntosycomas'].",
                                                            caracteres_dospuntos=".$GLOBALS['parametros']['caracteres_dospuntos'].",
                                                            caracteres_guion=".$GLOBALS['parametros']['caracteres_guion'].",
                                                            caracteres_guionbajo=".$GLOBALS['parametros']['caracteres_guionbajo'].",
                                                            caracteres_mas=".$GLOBALS['parametros']['caracteres_mas'].",
                                                            caracteres_asterisco=".$GLOBALS['parametros']['caracteres_asterisco'].",
                                                            caracteres_igual=".$GLOBALS['parametros']['caracteres_igual'].",
                                                            caracteres_porcentual=".$GLOBALS['parametros']['caracteres_porcentual'].",
                                                            caracteres_barra=".$GLOBALS['parametros']['caracteres_barra'].",
                                                            caracteres_barrainvertida=".$GLOBALS['parametros']['caracteres_barrainvertida'].",
                                                            caracteres_moneda=".$GLOBALS['parametros']['caracteres_moneda'].",
                                                            caracteres_interrogacion=".$GLOBALS['parametros']['caracteres_interrogacion'].",
                                                            caracteres_admiracion=".$GLOBALS['parametros']['caracteres_admiracion'].",
                                                            caracteres_arroba=".$GLOBALS['parametros']['caracteres_arroba'].",
                                                            caracteres_mayorymenor=".$GLOBALS['parametros']['caracteres_mayorymenor'].",
                                                            caracteres_ampersand=".$GLOBALS['parametros']['caracteres_ampersand'].",
                                                            caracteres_numeral=".$GLOBALS['parametros']['caracteres_numeral'].",
                                                            caracteres_comillassimples=".$GLOBALS['parametros']['caracteres_comillassimples'].",
                                                            caracteres_comillasdobles=".$GLOBALS['parametros']['caracteres_comillasdobles'].",
                                                            caracteres_saltosdelinea=".$GLOBALS['parametros']['caracteres_saltosdelinea'].",
                                                            caracteres_personalizado='".$GLOBALS['parametros']['caracteres_personalizado']."',
                                                            caracteres_quitarcaracteres='".$GLOBALS['parametros']['caracteres_quitarcaracteres']."' 
                                                    
                                                    WHERE   id=".$GLOBALS['parametros']['parametro']);
        $stmt->execute();
    }

    /**
     * @nombre: Eliminar un Parametro
     * @descripcion: Elimina un Parametro, pasándole el id.
     */
    public function eliminar(){

        //Query DB
        $stmt = $GLOBALS['conf']['pdo']->prepare("DELETE FROM parametros WHERE id = ".$GLOBALS['parametros']['parametro']);
        $stmt->execute();
    }

    /**
     * @nombre: Listar Parámetros (Tabla JS)
     * @descripcion: Lista Parámetros, con vinculaciones, etc.
     */
    public function listar_parametros(){

        //EStamos buscando o solo listando?
        if($GLOBALS['parametros']['q'] == '') $busqueda = false;
        else $busqueda = true;

        //Obtenemos los Parametros
        $stmt = $GLOBALS['conf']['pdo']->prepare("  SELECT  *, 
                                                            (   SELECT  GROUP_CONCAT(api_id) 
                                                                FROM    parametros_api
                                                                WHERE   parametro_id=parametros.id) as vinculos 

                                                    FROM    parametros 
                                                    ORDER BY label");
        $stmt->execute();
        $parametros = $stmt->fetchAll(PDO::FETCH_ASSOC);

        //Obtenemos los usuarios
        $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT id, (CONCAT(modulo_nombre,' - ',accion_nombre)) as label FROM api");
        $stmt->execute();
        $api = $stmt->fetchAll(PDO::FETCH_ASSOC);

        //resultados
        $resultado = array();

        //Recorremos los parametros
        foreach($parametros as $k=>$p){
            //Tiene vinculos?
            $vinculos = array();
            if(!is_null($p['vinculos']))
                $vinculos = explode(',', $p['vinculos']);

            //Recorremos la api
            //en busqueda de
            //acciones vinculadas
            $parametros[$k]['api'] = array();
            if(count($vinculos))
                foreach($api as $x=>$v)
                    if(in_array((int)$v['id'], $vinculos))
                        $parametros[$k]['api'][] = $v['label'];

            $parametro = array();
            $parametro['id'] = $p['id'];
            $parametro['descripcion'] = $p['descripcion'];
            $parametro['identificador'] = $p['identificador'];
            $parametro['label'] = ucfirst($p['label_articulo']).' '.$p['label'];
            $parametro['filtro'] = $GLOBALS['toolbox']->generaFiltroParametro($p);
            $parametro['api'] = $parametros[$k]['api'];

            $resultado[] = $parametro;
        }

        //Si estamos buscando...
        if($busqueda){
            //Score
            foreach($parametros as $k=>$v)
                $parametros[$k]['score'] = $GLOBALS['toolbox']->compare_strings($campos['q']->_value, $v['usuario'].' '.$v['cuit'].' '.$v['razon_social'].' '.$v['nombre_del_comercio'].' '.$v['email']);

            //Eliminamos 0's
            foreach($parametros as $k=>$v)
                if($v['score'] == 0)
                    unset($parametros[$k]);

            //Ordenamos
            usort($parametros, function($a, $b) {
                return $b['score'] - $a['score'];
            });
        }

        //Asignamos resultados
        $GLOBALS['resultado']->_result = $resultado;
    }

    /**
     * @nombre: Vincular un Parametro a una Acción de la API
     * @descripcion: Agrega un Parámetro a una Acción de la API.
     */
    public function vincular_parametro(){

        //Query DB
        $stmt = $GLOBALS['conf']['pdo']->prepare("  SELECT  *
                                                    FROM    parametros_api
                                                    WHERE   (api_id = ".$GLOBALS['parametros']['accion'].") AND 
                                                            (parametro_id = ".$GLOBALS['parametros']['parametro'].")");
        $stmt->execute();
        $vinculo =  $stmt->fetchAll(PDO::FETCH_ASSOC);

        //Si el vinculo no
        //existe lo agregamos
        if(!count($vinculo)){
            $stmt = $GLOBALS['conf']['pdo']->prepare("  INSERT INTO parametros_api (api_id, parametro_id) VALUES 
                                                                    (".$GLOBALS['parametros']['accion'].",".$GLOBALS['parametros']['parametro'].")");
            $stmt->execute();
        }
    }

    /**
     * @nombre: Desvincular un Parametro de una Acción de la API
     * @descripcion: Agrega un Parámetro a una Acción de la API.
     */
    public function desvincular_parametro(){

        //Query DB
        $stmt = $GLOBALS['conf']['pdo']->prepare("  DELETE FROM parametros_api
                                                    WHERE   (api_id = ".$GLOBALS['parametros']['accion'].") AND 
                                                            (parametro_id = ".$GLOBALS['parametros']['parametro'].")");
        $stmt->execute();
    }

    /**
     * @nombre: Editar los Procesos del Parametro
     * @descripcion: Edita un los Procesos del Parametro.
     */
    public function editar_procesos(){

        $valores = array();


        //validar por caracteres
        $valores[] = "validador_errorenfiltro=".$GLOBALS['parametros']['validador_errorenfiltro'];

        //filtro convertir a mayusculas
        $valores[] = "filtro_convertiramayusculas=".$GLOBALS['parametros']['filtro_convertiramayusculas'];

        //filtro convertir a minusculas
        $valores[] = "filtro_convertiraminusculas=".$GLOBALS['parametros']['filtro_convertiraminusculas'];

        //filtro convertir a castearainteger
        $valores[] = "filtro_castearainteger=".$GLOBALS['parametros']['filtro_castearainteger'];

        //filtro convertir a castearadecimal
        $valores[] = "filtro_castearadecimal=".$GLOBALS['parametros']['filtro_castearadecimal'];

        //filtro convertir a castearajson
        $valores[] = "filtro_castearajson=".$GLOBALS['parametros']['filtro_castearajson'];

        //filtro convertir a castearastring
        $valores[] = "filtro_castearastring=".$GLOBALS['parametros']['filtro_castearastring'];

        //filtro convertir a castearaarchivo
        $valores[] = "filtro_castearaarchivo=".$GLOBALS['parametros']['filtro_castearaarchivo'];

        //filtro convertir a castearastring
        $valores[] = "filtro_convertirtildes=".$GLOBALS['parametros']['filtro_convertirtildes'];

        //filtro validar email
        $valores[] = "filtro_email=".$GLOBALS['parametros']['filtro_email'];

        //DECIMALES


        //Filtro y validador para decimales
        if($GLOBALS['parametros']['filtro_decimales'] != '') $valores[] = "filtro_decimales='".$GLOBALS['parametros']['filtro_decimales']."'";
        else  $valores[] = "filtro_decimales=NULL";

        //Validador maxima cantida de caracteres
        if(($GLOBALS['parametros']['filtro_decimales'] == '') && $GLOBALS['parametros']['validador_decimales']) 
            $GLOBALS['parametros']['validador_decimales'] = 0;

        //Validador maxima cantida de caracteres
        $valores[] = "validador_decimales=".$GLOBALS['parametros']['validador_decimales'];



        //FILTRO Y VALIDADOR PARA EL MAXIMO VALOR DEL PARAMETRO


        //Filtro maximo
        if($GLOBALS['parametros']['filtro_maximo'] != '') $valores[] = "filtro_maximo='".$GLOBALS['parametros']['filtro_maximo']."'";
        else $valores[] = "filtro_maximo=NULL";

        //Validador maximo (validamos si tenemos el maximo definido)
        if(($GLOBALS['parametros']['filtro_maximo'] == '') && $GLOBALS['parametros']['validador_maximo'])
            $GLOBALS['parametros']['validador_maximo'] = 0;

        //Debemos de validar el maximo?
        $valores[] = "validador_maximo=".$GLOBALS['parametros']['validador_maximo'];


        //FILTRO Y VALIDADOR PARA EL MINIMO VALOR DEL PARAMETRO


        //Filtro minimo
        if($GLOBALS['parametros']['filtro_minimo'] != '') $valores[] = "filtro_minimo='".$GLOBALS['parametros']['filtro_minimo']."'";
        else $valores[] = "filtro_minimo=NULL";

        //Validador minimo (validamos si tenemos el minimo definido)
        if(($GLOBALS['parametros']['filtro_minimo'] == '') && $GLOBALS['parametros']['validador_minimo']) 
            $GLOBALS['parametros']['validador_minimo'] = 0;

        //Debemos de validar el minimo?
        $valores[] = "validador_minimo=".$GLOBALS['parametros']['validador_minimo'];


        //VALIDADOR PARA LA MINIMA CANTIDAD DE CARACTERES


        //Validador minima cantida de caracteres
        if($GLOBALS['parametros']['validador_minimodecaracteres'] != '') $valores[] = "validador_minimodecaracteres=".$GLOBALS['parametros']['validador_minimodecaracteres'];
        else $valores[] = "validador_minimodecaracteres=NULL";



        //VALIDADOR PARA LA MAXIMA CANTIDAD DE CARACTERES



        //Filtro maxima cantida de caracteres
        if($GLOBALS['parametros']['filtro_maximodecaracteres'] != '') $valores[] = "filtro_maximodecaracteres=".$GLOBALS['parametros']['filtro_maximodecaracteres'];
        $valores[] = "filtro_maximodecaracteres=NULL";

        //Validador maxima cantida de caracteres
        if(($GLOBALS['parametros']['filtro_maximodecaracteres'] == '') && $GLOBALS['parametros']['validador_maximodecaracteres']) 
            $GLOBALS['parametros']['validador_maximodecaracteres'] = 0;

        //Validador maxima cantida de caracteres
        $valores[] = "validador_maximodecaracteres=".$GLOBALS['parametros']['validador_maximodecaracteres'];



        //FILTRO Y VALIDADOR PARA LOS VALORES PERMITIDOS


        //Filtro valorespermitidos
        if($GLOBALS['parametros']['filtro_valorespermitidos'] != '') $valores[] = "filtro_valorespermitidos='".$GLOBALS['parametros']['filtro_valorespermitidos']."'";
        else $valores[] = "filtro_valorespermitidos=NULL";

        //Validador valorespermitidos (validamos si tenemos el valorespermitidos definido)
        if(($GLOBALS['parametros']['filtro_valorespermitidos'] == '') && $GLOBALS['parametros']['validador_valorespermitidos'])
            $GLOBALS['parametros']['validador_valorespermitidos'] = 0;

        //Debemos de validar los valorespermitidos?
        $valores[] = "validador_valorespermitidos=".$GLOBALS['parametros']['validador_valorespermitidos'];


        //FILTRO Y VALIDADOR PARA LOS VALORES NO PERMITIDOS


        //Filtro valoresnopermitidos
        if($GLOBALS['parametros']['filtro_valoresnopermitidos'] != '') $valores[] = "filtro_valoresnopermitidos='".$GLOBALS['parametros']['filtro_valoresnopermitidos']."'";
        else $valores[] = "filtro_valoresnopermitidos=NULL";

        //Validador valoresnopermitidos (validamos si tenemos el valoresnopermitidos definido)
        if(($GLOBALS['parametros']['filtro_valoresnopermitidos'] == '') && $GLOBALS['parametros']['validador_valoresnopermitidos'])
            $GLOBALS['parametros']['validador_valoresnopermitidos'] = 0;

        //Debemos de validar los valoresnopermitidos?
        $valores[] = "validador_valoresnopermitidos=".$GLOBALS['parametros']['validador_valoresnopermitidos'];


        //FILTRO Y VALIDADOR PARA EXPRESIONES REGULARES


        //Filtro regexp
        if($GLOBALS['parametros']['filtro_regexp'] != '') $valores[] = "filtro_regexp='".$GLOBALS['parametros']['filtro_regexp']."'";
        else $valores[] = "filtro_regexp=NULL";

        //Validador regexp (validamos si tenemos el regexp definido)
        if(($GLOBALS['parametros']['filtro_regexp'] == '') && $GLOBALS['parametros']['validador_regexp'])
            $GLOBALS['parametros']['validador_regexp'] = 0;

        //Debemos de validar los regexp?
        $valores[] = "validador_regexp=".$GLOBALS['parametros']['validador_regexp'];


        //VALIDACIONES DE ARCHIVOS

        //Filtro extensiones
        if($GLOBALS['parametros']['filtro_archivo_extensiones'] != '') $valores[] = "filtro_archivo_extensiones='".$GLOBALS['parametros']['filtro_archivo_extensiones']."'";
        else $valores[] = "filtro_archivo_extensiones=NULL";

        //Filtro Peso
        if($GLOBALS['parametros']['filtro_archivo_peso'] != '') $valores[] = "filtro_archivo_peso='".$GLOBALS['parametros']['filtro_archivo_peso']."'";
        else $valores[] = "filtro_archivo_peso=NULL";



        //Mover a Directorio
        if($GLOBALS['parametros']['filtro_archivo_directorio'] != ''){
            //Si es un directorio
            if(is_dir($GLOBALS['parametros']['filtro_archivo_directorio'])){
                $valores[] = "filtro_archivo_directorio='".$GLOBALS['parametros']['filtro_archivo_directorio']."'";
            //Si ademas incluye el
            //nombre de archivo
            }else{
                $archivo = pathinfo($GLOBALS['parametros']['filtro_archivo_directorio']);
                //Si el directorio sin el nombre de archivo existe
                if(is_dir($archivo['dirname'])){
                    $valores[] = "filtro_archivo_directorio='".$GLOBALS['parametros']['filtro_archivo_directorio']."'";
                }
            }
        }else {
            $valores[] = "filtro_archivo_directorio=NULL";
        }

        $stmt = $GLOBALS['conf']['pdo']->prepare("UPDATE parametros SET ".implode(', ', $valores)." WHERE id=".$GLOBALS['parametros']['parametro'] );
        $stmt->execute();
    }

    /**
     * @nombre: Listar Acciones API (Select2)
     * @descripcion: Lista modulos y acciones para Select2.
     */
    public function listar_accionesAPI(){

        //Calculate paging
        $page =  (intval($GLOBALS['parametros']['p'])-1) * 40;

        //EStamos buscando o solo listando?
        if($GLOBALS['parametros']['q'] == '') $busqueda = false;
        else $busqueda = true;

        //Obtenemos los usuarios
        $stmt = $GLOBALS['conf']['pdo']->prepare("  SELECT  id,
                                                            modulo_nombre,
                                                            accion_nombre
                                                    FROM api
                                                    ORDER BY modulo ASC, accion ASC");
        $stmt->execute();
        $api = $stmt->fetchAll(PDO::FETCH_ASSOC);


        //Si estamos buscando...
        if($busqueda){
            //Score
            foreach($api as $k=>$v)
                $api[$k]['score'] = $GLOBALS['toolbox']->compare_strings($GLOBALS['parametros']['q'], $v['modulo_nombre'].' '.$v['accion_nombre']);

            //Eliminamos 0's
            foreach($api as $k=>$v)
                if($v['score'] == 0)
                    unset($api[$k]);

            //Ordenamos
            usort($api, function($a, $b) {
                return $b['score'] - $a['score'];
            });
        }

        //Calculamos totales
        $GLOBALS['resultado']->_result['total'] = count($api);

        //Devolvemos items y total
        $GLOBALS['resultado']->_result['items'] = array_slice($api, $page, 40);
    }
}
?>