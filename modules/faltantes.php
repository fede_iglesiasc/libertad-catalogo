<?php
/**
 * @nombre: Faltantes
 * @descripcion: Agrega, edita, lista y reporta Faltantes de Artículos.
 */
class faltantes extends module{

    /*
	 * Constructor
	 */
    public function __construct(){
    }

    /**
     * Validaciones previas
     */
    public function __pre($accion){

        //Si existe el parametro faltante...
        if(isset($GLOBALS['parametros']['faltante'])){
            $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT id FROM faltantes WHERE id = ".$GLOBALS['parametros']['faltante']);
            $stmt->execute();
            $faltante = $stmt->rowCount();
        }


        //Si existe el parametro articulo...
        if(isset($GLOBALS['parametros']['articulo']) && ($GLOBALS['parametros']['articulo'] != '')){
            $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT id FROM catalogo_articulos WHERE id = ".$GLOBALS['parametros']['articulo']);
            $stmt->execute();
            $articulo = $stmt->rowCount();
        }

        //Si existe el parametro fabricante...
        if(isset($GLOBALS['parametros']['fabricante']) && ($GLOBALS['parametros']['fabricante'] != '')){
            $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT id FROM catalogo_marcas WHERE id = ".$GLOBALS['parametros']['fabricante']);
            $stmt->execute();
            $fabricante = $stmt->rowCount();
        }

        //Si no existe el Fabricante
        if( isset($fabricante) && !$fabricante && 
            in_array($accion, array('agregar'))){
            $GLOBALS['resultado']->setError("El Fabricante no existe.");
            return;
        }

        //Si no existe el Artículo
        if( isset($articulo) && !$articulo && 
            in_array($accion, array('agregar'))){
            $GLOBALS['resultado']->setError("El Artículo no existe.");
            return;
        }

        //Si no existe el Faltante
        if( isset($faltante) && !$faltante && 
            in_array($accion, array('editar','info','eliminar'))){
            $GLOBALS['resultado']->setError("El Faltante no existe.");
            return;
        }

        //llamamos a la accion
        return call_user_func_array(array($this, $accion), array());
    }

    /**
     * @nombre: Información de Faltante
     * @descripcion: Obtiene la info del Faltante
     */
    public function info(){

        //Usuario
        $usuario = $GLOBALS['session']->getData('usuario');

        //Obtenemos los usuarios
        $stmt = $GLOBALS['conf']['pdo']->prepare(" SELECT  articulo_id,
                    (SELECT id_marca FROM catalogo_articulos WHERE id = articulo_id) as marca_id,
                    codigo,
                    comentario,
                    (SELECT nombre FROM catalogo_marcas WHERE catalogo_marcas.id= (SELECT id_marca FROM catalogo_articulos WHERE catalogo_articulos.id = articulo_id)) as marca_label
            FROM    faltantes
            WHERE   (id = '".$GLOBALS['parametros']['faltante']."') AND (usuario = '".$usuario."')");

        $stmt->execute();
        $datos = $stmt->fetch(PDO::FETCH_ASSOC);

        //Guardamos los datos
        $GLOBALS['resultado']->_result = $datos;
    }

    /**
     * @nombre: Listar Faltantes 
     * @descripcion: Lista los faltantes que el usuario Agregó.
     */
    public function listar(){

        //EStamos buscando o solo listando?
        $busqueda = false;
        if($GLOBALS['parametros']['q'] != '') $busqueda = true;

        //Usuario
        $usuario = $GLOBALS['session']->getData('usuario');

        //Obtenemos los usuarios
        $stmt = $GLOBALS['conf']['pdo']->prepare("  SELECT      id, 
                                                                usuario, 
                                                                (IF(    articulo_id IS NOT NULL,
                                                                        articulo_id,
                                                                        -1)) as articulo_id,

                                                                (IF(    articulo_id IS NOT NULL, 
                                                                        (SELECT CONCAT(prefijo,'-',codigo,'-',sufijo) FROM catalogo_articulos WHERE id = faltantes.articulo_id),
                                                                        codigo)) as codigo,

                                                                (IF(    marca_id IS NOT NULL,
                                                                        (SELECT nombre FROM catalogo_marcas WHERE id = marca_id),
                                                                        'Marca desconocida')) as marca_label,
                                                                comentario

                                                    FROM        faltantes 
                                                    WHERE       usuario = '".$usuario."'
                                                    ORDER BY    marca_label ASC");
        $stmt->execute();
        $faltantes = $stmt->fetchAll(PDO::FETCH_ASSOC);


        //Si estamos buscando...
        if($busqueda){
            //Score
            foreach($faltantes as $k=>$v)
                $faltantes[$k]['score'] = $GLOBALS['toolbox']->compare_strings($GLOBALS['parametros']['q'], $v['marca_label'].' '.$v['codigo']);

            //Eliminamos 0's
            foreach($faltantes as $k=>$v)
                if($v['score'] == 0)
                    unset($faltantes[$k]);

            //Ordenamos
            usort($faltantes, function($a, $b) {
                return $b['score'] - $a['score'];
            });
        }


        //Asignamos resultados
        $GLOBALS['resultado']->_result = $faltantes;
    }

    /**
     * @nombre: Eliminar Faltante
     * @descripcion: Elimina un faltante agregado por el Usuario.
     */
    public function eliminar(){
    
        //usuario
        $usuario = $GLOBALS['session']->getData('usuario');

        //Obtenemos los usuarios
        $stmt = $GLOBALS['conf']['pdo']->prepare("  DELETE 
                                                    FROM    faltantes
                                                    WHERE   (id = ".$GLOBALS['parametros']['faltante'].") AND (usuario = '".$usuario."')");
        $stmt->execute();
    }

    /**
     * @nombre: Agregar Faltante
     * @descripcion: Le pasamos un Fabricante, el código y una descripción y agrega un faltante.
     */
    public function agregar(){

        //articulo
        if($GLOBALS['parametros']['articulo'] != '')
            $articulo = $GLOBALS['parametros']['articulo'];

        //Fabricante
        if($GLOBALS['parametros']['fabricante'] != '')
            $fabricante = $GLOBALS['parametros']['fabricante'];

        //Codigo
        if($GLOBALS['parametros']['codigo'] != '')
            $codigo = $GLOBALS['parametros']['codigo'];

        //Si no esta seteado ni el codigo ni el art.
        if(!isset($articulo) && !isset($codigo)){
            $GLOBALS['resultado']->setError("El código no puede estar vacio");
            return;
        }

        //Usuario
        $usuario = $GLOBALS['session']->getData('usuario');

        //Comentario
        $comentario = $GLOBALS['parametros']['comentario'];


        if( !((!isset($articulo) && isset($fabricante) && isset($codigo)) ||
              (isset($articulo) && !isset($fabricante) && !isset($codigo)) )){
            
            //Agregamos error 
            $GLOBALS['resultado']->setError("Existe un error en los parametros.");
            return;
        }

        //Si estamos agregando un artículo que existe...
        if(isset($articulo)){
            $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT id_marca, CONCAT(prefijo,'-',codigo,'-',sufijo) as codigo FROM catalogo_articulos WHERE id = ".$articulo);
            $stmt->execute();
            $artDB = $stmt->fetch(PDO::FETCH_ASSOC);

            $fabricante = $artDB['id_marca'];
            if(is_null($fabricante)) $fabricante = "NULL";
            $codigo = $artDB['codigo'];
        }

        //Si el articulo no fue seteado es NULL
        if(!isset($articulo)) $articulo = 'NULL';

        //Agregamos el faltante
        $stmt = $GLOBALS['conf']['pdo']->prepare("  INSERT INTO faltantes (usuario, articulo_id, marca_id, codigo, comentario, fecha) 
                                                    VALUES (
                                                        '".$usuario."',
                                                        ".$articulo.",
                                                        ".$fabricante.",
                                                        '".$codigo."',
                                                        '".$comentario."',
                                                        NOW())");
        $stmt->execute();
    }

    /**
     * @nombre: Editar Faltante
     * @descripcion: Le pasamos un Fabricante, el código y una descripción y edita un faltante.
     */
    public function editar(){

    
        //articulo
        if($GLOBALS['parametros']['articulo'] != '')
            $articulo = $GLOBALS['parametros']['articulo'];

        //Fabricante
        if($GLOBALS['parametros']['fabricante'] != '')
            $fabricante = $GLOBALS['parametros']['fabricante'];

        //Codigo
        if($GLOBALS['parametros']['codigo'] != '')
            $codigo = $GLOBALS['parametros']['codigo'];

        //Si no esta seteado ni el codigo ni el art.
        if(!isset($articulo) && !isset($codigo)){
            $GLOBALS['resultado']->setError("El código no puede estar vacio");
            return;
        }

        //Usuario
        $usuario = $GLOBALS['session']->getData('usuario');

        //Comentario
        $comentario = $GLOBALS['parametros']['comentario'];


        if( !((!isset($articulo) && isset($fabricante) && isset($codigo)) ||
              (isset($articulo) && !isset($fabricante) && !isset($codigo)) )){
            
            //Agregamos error 
            $GLOBALS['resultado']->setError("Existe un error en los parametros.");
            return;
        }

        //Si estamos agregando un artículo que existe...
        if(isset($articulo)){
            $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT id_marca, CONCAT(prefijo,'-',codigo,'-',sufijo) as codigo FROM catalogo_articulos WHERE id = ".$articulo);
            $stmt->execute();
            $artDB = $stmt->fetch(PDO::FETCH_ASSOC);

            $fabricante = $artDB['id_marca'];
            $codigo = $artDB['codigo'];
        }


        //Si estamos agregando un artículo que existe...
        if(isset($articulo)){
            $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT id_marca, CONCAT(prefijo,'-',codigo,'-',sufijo) as codigo FROM catalogo_articulos WHERE id = ".$articulo);
            $stmt->execute();
            $artDB = $stmt->fetch(PDO::FETCH_ASSOC);

            $fabricante = $artDB['id_marca'];
            $codigo = $artDB['codigo'];
        }


        //Validamos que no exista antes el faltante
        $stmt = $GLOBALS['conf']['pdo']->prepare("  SELECT  usuario, 
                                                            (SELECT nombre FROM usuarios WHERE usuario = faltantes.usuario) as nombre 
                                                    FROM    faltantes 
                                                    WHERE   id=".$GLOBALS['parametros']['faltante']);
        $stmt->execute();
        $valida = $stmt->fetch(PDO::FETCH_ASSOC);

        //El faltante ya existe
        if($valida['usuario'] != $GLOBALS['session']->getData('usuario')){
            //Armamos el mensaje de error
            $GLOBALS['resultado']->setError("El Faltante sólo puede ser editado por ".$valida['nombre'].".");
            return;
        }


        $sqlUpdate[] = "comentario='".$comentario."'";
        $sqlUpdate[] = "marca_id= ".$fabricante;
        $sqlUpdate[] = "codigo= '".$codigo."'";
        if(!isset($articulo)) $sqlUpdate[] = "articulo_id= NULL"; 
        else $sqlUpdate[] = "articulo_id=".$articulo;

        //Query DB
        $stmt = $GLOBALS['conf']['pdo']->prepare("  UPDATE  faltantes 
                                                    SET     ".implode(', ', $sqlUpdate)." 
                                                    WHERE   id = ".$GLOBALS['parametros']['faltante']);
        $stmt->execute();
    }

    /**
     * @nombre: Listar Fabricantes y codigos
     * @descripcion: Lista fabricantes y codigos de Artículos
     */
    public function fabricantes_codigo_listar(){


        //EStamos buscando o solo listando?
        $busqueda = false;
        if($GLOBALS['parametros']['q'] != ''){
            $busqueda = true;
            $keyword_tokens = explode(' ', $GLOBALS['parametros']['q']);
        }

        //Calculate paging
        $page =  (intval($GLOBALS['parametros']['p'])-1) * 40;


        $sql1 = "(  SELECT * 
                    FROM ( 
                            SELECT 
                                        NULL as codigo, 
                                        NULL as articulo_id, 
                                        id as marca_id, 
                                        nombre as marca_label, 
                                        '' as busqueda, 
                                        1 as score 

                            FROM        catalogo_marcas 
                            WHERE       padre = 1 
                            ORDER BY    marca_label
                          ) as a 
                ) 

                UNION ( 
                    SELECT 
                        codigo, 
                        articulo_id, 
                        marca_id, 
                        marca_label, 
                        busqueda, 
                        (";

        //Si hacemos una 
        //busqueda...
        if(!$busqueda) $sql1 .= "1";
        else
            foreach($keyword_tokens as $key=>$palabra){
                if($key > 0) $sql1 .= "+";
                $sql1 .= "IF(LOCATE('".$palabra."',LOWER(busqueda))>0,1,0)";
            }

        $sql1 .= ") AS score  
                    
                    FROM    (
                                SELECT 
                                        (CONCAT(prefijo,'-',codigo,'-',sufijo)) as codigo, 
                                        id as articulo_id, 
                                        id_marca as marca_id, 
                                        (SELECT nombre FROM catalogo_marcas WHERE id = id_marca) as marca_label, 
                                        CONCAT((SELECT nombre FROM catalogo_marcas WHERE id = id_marca),' ', prefijo,'-',codigo,'-',sufijo) as busqueda
                                FROM    catalogo_articulos
                            ) as r 

                    HAVING      score>0 

                    ORDER BY    score DESC, 
                                marca_label ASC)";



        //Make query
        $stmt = $GLOBALS['conf']['pdo']->prepare($sql1);
        $stmt->execute();
        $fabricantes =  $stmt->fetchAll(PDO::FETCH_ASSOC);
        
        //Si estamos buscando...
        if($busqueda){
            //Score
            foreach($fabricantes as $k=>$v){
                $fabricantes[$k]['score'] = $GLOBALS['toolbox']->compare_strings($GLOBALS['parametros']['q'], $v['marca_label'].' '.$v['codigo']);
                if(is_null($v['codigo']) && is_null($v['articulo_id']) && ($fabricantes[$k]['score'] > 1))
                    $fabricantes[$k]['score'] = $fabricantes[$k]['score'] + 10;
            }

            //Eliminamos 0's
            foreach($fabricantes as $k=>$v)
                if($v['score'] == 0)
                    unset($fabricantes[$k]);

            //Ordenamos
            usort($fabricantes, function($a, $b) {
                return $b['score'] - $a['score'];
            });
        }


        //Calculamos totales
        $tot = count($fabricantes);
        $GLOBALS['resultado']->_result['total'] = $tot;

        //Cortamos el pedazo de array que nos interesa
        $res = array_slice($fabricantes, $page, 40);

        //Eliminamos datos que no utilizamos
        foreach($res as $k=>$e){
            unset($res[$k]['score'], $res[$k]['codbin']);
        }

        //Save items to result Object
        $GLOBALS['resultado']->_result['items'] = $res;
    }

    /**
     * @nombre: Listar Fabricantes (para los reportes)
     * @descripcion: Lista fabricantes y codigos de Artículos
     */
    public function fabricantes_listar(){

        //EStamos buscando o solo listando?
        $busqueda = true;
        if($GLOBALS['parametros']['q'] == '') 
            $busqueda = false;

        //Calculate paging
        $page =  (intval($GLOBALS['parametros']['p'])-1) * 40;

        //SQL para buscar todas las marcas que trabajamos nosotros
        $sql = "(SELECT 0 as id, 'Todos los Fabricantes' as nombre)
                UNION(
                    SELECT id, nombre FROM (
                        SELECT  id, 
                                nombre 
                        FROM    catalogo_marcas as p 
                        WHERE   (padre = 1) AND 
                                NOT (
                                        (0 = (SELECT COUNT(*) id FROM catalogo_marcas WHERE padre = p.id)) AND 
                                        (usa_codigos='N')
                                    ) ORDER BY nombre ASC
                        ) as t ORDER BY nombre)";



        //Make query
        $stmt = $GLOBALS['conf']['pdo']->prepare($sql);
        $stmt->execute();
        $items =  $stmt->fetchAll(PDO::FETCH_ASSOC);
        
        //Si estamos buscando...
        if($busqueda){
            //Score
            foreach($items as $k=>$v){
                $items[$k]['score'] = $GLOBALS['toolbox']->compare_strings($GLOBALS['parametros']['q'], $v['nombre']);
            }

            //Eliminamos 0's
            foreach($items as $k=>$v)
                if($v['score'] == 0)
                    unset($items[$k]);

            //Ordenamos
            usort($items, function($a, $b) {
                return $b['score'] - $a['score'];
            });
        }


        //Eliminamos datos que no utilizamos
        foreach($items as $k=>$e)
            unset(  $items[$k]['score'], 
                    $items[$k]['codbin']);

        //Calculamos totales
        $GLOBALS['resultado']->_result['total'] = count($items);

        //Cortamos el pedazo de array que nos interesa
        $GLOBALS['resultado']->_result['items'] = array_slice($items, $page, 40);
    }

    /**
     * @nombre: Eliminar Reportes
     * @descripcion: Elimina listado de Faltantes por Fabricante y Fecha
     */
    public function reporte_eliminar(){
    
        //Validamos las fechas
        if($GLOBALS['parametros']['fecha_desde'] != '')
            $fecha_desde = array_map('intval', explode('/',$GLOBALS['parametros']['fecha_desde']));
        if($GLOBALS['parametros']['fecha_hasta'] != '')
            $fecha_hasta = array_map('intval', explode('/',$GLOBALS['parametros']['fecha_hasta']));


        //Tiramos el error
        if(isset($fecha_desde))
            if( !is_array($fecha_desde) || (count($fecha_desde) != 3) || !checkdate($fecha_desde[1],$fecha_desde[0],$fecha_desde[2]) ){
                //Agregamos error 
                $GLOBALS['resultado']->setError("El rango de Fechas tiene un formato incorrecto.");
                echo $GLOBALS['resultado']->getResult();
                exit;
            }

        //Tiramos el error
        if(isset($fecha_hasta))
            if( !is_array($fecha_hasta) || (count($fecha_hasta) != 3) || !checkdate($fecha_hasta[1],$fecha_hasta[0],$fecha_hasta[2]) ){
                //Agregamos error 
                $GLOBALS['resultado']->setError("El rango de Fechas tiene un formato incorrecto.");
                echo $GLOBALS['resultado']->getResult();
                exit;
            }

        //Validamos el fabricante
        $fabricante = (int)$GLOBALS['parametros']['fabricante'];

        /*if( $fabricante == -1){
            //Agregamos error 
            $GLOBALS['resultado']->setError("No puede eliminar los Faltantes de todos los Fabricantes de una sola vez.");
            return;
        }*/

        $sql = " DELETE FROM faltantes ";

        $condicionales = array();

        if( $fabricante != -1)
            $condicionales[] = "(marca_id = ".$GLOBALS['parametros']['fabricante'].")";

        if(isset($fecha_desde)) $condicionales[] = " AND (fecha >= '".$fecha_desde[2]."-".$fecha_desde[1]."-".$fecha_desde[0]."')";
        if(isset($fecha_hasta)) $condicionales[] = " AND (fecha <= '".$fecha_hasta[2]."-".$fecha_hasta[1]."-".$fecha_hasta[0]."')";

        if(count($condicionales)) $sql .= " WHERE " . implode(" AND ", $condicionales);

        //Obtenemos los usuarios
        $stmt = $GLOBALS['conf']['pdo']->prepare($sql);
        $stmt->execute();
    }

    /**
     * @nombre: Imprimir Reportes
     * @descripcion: Imprime listado de Faltantes por Fabricante y Fecha
     */
    public function reporte_imprimir(){
    
        
        //Validamos las fechas
        if($GLOBALS['parametros']['fecha_desde'] != '')
            $fecha_desde = array_map('intval', explode('/',$GLOBALS['parametros']['fecha_desde']));
        if($GLOBALS['parametros']['fecha_hasta'] != '')
            $fecha_hasta = array_map('intval', explode('/',$GLOBALS['parametros']['fecha_hasta']));


        //Tiramos el error
        if(isset($fecha_desde))
            if( !is_array($fecha_desde) || (count($fecha_desde) != 3) || !checkdate($fecha_desde[1],$fecha_desde[0],$fecha_desde[2]) ){
                //Agregamos error 
                $GLOBALS['resultado']->setError("El rango de Fechas tiene un formato incorrecto.");
                echo $GLOBALS['resultado']->getResult();
                exit;
            }

        //Tiramos el error
        if(isset($fecha_hasta))
            if( !is_array($fecha_hasta) || (count($fecha_hasta) != 3) || !checkdate($fecha_hasta[1],$fecha_hasta[0],$fecha_hasta[2]) ){
                //Agregamos error 
                $GLOBALS['resultado']->setError("El rango de Fechas tiene un formato incorrecto.");
                echo $GLOBALS['resultado']->getResult();
                exit;
            }

        //Validamos el fabricante
        if($GLOBALS['parametros']['fabricante'] != '')
            $fabricante = (int)$GLOBALS['parametros']['fabricante'];

        //Orden
        $orden = $GLOBALS['parametros']['orden'];

        //Condicionales
        $sqlWhere = array();

        //Fabricante
        if($fabricante) $sqlWhere[] = "(marca_id = ".$fabricante.")";
        //Fecha Desde
        if(isset($fecha_desde)) $sqlWhere[] = "(fecha >= '".$fecha_desde[2]."-".$fecha_desde[1]."-".$fecha_desde[0]."')";
        //Fecha Hasta
        if(isset($fecha_hasta)) $sqlWhere[] = "(fecha <= '".$fecha_hasta[2]."-".$fecha_hasta[1]."-".$fecha_hasta[0]."')";


        $sql = "    SELECT  
                            (SELECT nombre FROM usuarios WHERE usuario = faltantes.usuario) as usuario,
                            (DATE_FORMAT(fecha,'%d/%m/%Y')) as fecha,
                            (SELECT nombre FROM catalogo_marcas WHERE id = marca_id) as marca, 
                            codigo, 
                            comentario,
                            id                            

                    FROM    faltantes";

        //Where
        if(count($sqlWhere)) $sql .=" WHERE ";

        $sql .= implode(' AND ', $sqlWhere);

        //Ordenamos por defeto por fecha
        $sql .= " ORDER BY marca ASC, codigo ASC";

        //Obtenemos los usuarios
        $stmt = $GLOBALS['conf']['pdo']->prepare($sql);
        $stmt->execute();
        $items =  $stmt->fetchAll(PDO::FETCH_ASSOC);

        //Agregamos checkbox
        foreach($items as $k=>$v)
            $items[$k]['id'] = '<input type="checkbox" value="'.$v['id'].'">';

        //ordenamos por código
        if($orden == 'por_codigo')
            usort($items, function ($a, $b) {
                //Si el fabricante es el mismo ordenamos por codigo
                if(strnatcmp($a['marca'], $b['marca']) == 0){
                    return strnatcmp($a['codigo'], $b['codigo']);
                }else{
                    return strnatcmp($a['marca'], $b['marca']); 
                }

            });

        //La cabecera
        $header[] = array('id'=>'usuario', 'label'=>'Usuario');
        $header[] = array('id'=>'fecha', 'label'=>'Fecha');
        $header[] = array('id'=>'marca', 'label'=>'Fabricante');
        $header[] = array('id'=>'codigo', 'label'=>'Código');
        $header[] = array('id'=>'comentario', 'label'=>'Comentario');
        $header[] = array('id'=>'accion', 'label'=>'');

        //Guardamos los resultados
        $GLOBALS['resultado']->_result['header'] = $header;
        $GLOBALS['resultado']->_result['items'] = $items;
    }

    /**
     * @nombre: Exportar Reporte a Excel
     * @descripcion: Exporta el listado de Faltantes a xls.
     */
    public function reporte_exportar_xls(){
        require_once './classes/PHPExcel.php';
        $objPHPExcel = new PHPExcel();

        //Agergamos los datos al objeto PHPExcel
        $objPHPExcel->getActiveSheet()->fromArray($GLOBALS['parametros']['datos'], NULL, 'A1');

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('./tmp/reporte.xls');
    }

    /**
     * @nombre: Eliminar Faltantes Seleccionados
     * @descripcion: Eliminar Faltantes Seleccionados
     */
    public function reporte_eliminar_seleccionados(){
        //Obtenemos los faltantes
        $faltantes = json_decode($GLOBALS['parametros']['datos']);

        if(!count($faltantes)) return;

        //Eliminamos los faltantes
        $stmt = $GLOBALS['conf']['pdo']->prepare("DELETE FROM faltantes WHERE id IN (" . implode(',', $faltantes) . ")");
        $stmt->execute();

        $GLOBALS['resultado']->_result['faltantes_eliminados'] = $faltantes;
    }


}
?>