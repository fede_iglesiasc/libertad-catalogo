<?php
/**
 * @nombre: Descuentos
 * @descripcion: Descuentos del Artículo
 */
class descuentos extends module{


    /*
	 * Constructor
	 */
    public function __construct(){
    }

    /**
     * Validaciones previas
     */
    public function __pre($accion){

        //Si existe el parametro articulo...
        if(isset($GLOBALS['parametros']['articulo']) && ($GLOBALS['parametros']['articulo'] != '')){
            $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT id FROM catalogo_articulos WHERE id = ".$GLOBALS['parametros']['articulo']);
            $stmt->execute();
            $articulo = $stmt->rowCount();
        }

        //Si existe el articulo
        if( isset($articulo) && $articulo && 
            in_array($accion, array('editar'))){
            $GLOBALS['resultado']->setError("El artículo no existe");
            return;
        }

        //llamamos a la accion
        return call_user_func_array(array($this, $accion), array());
    }


    //--------------------------------------------------
    //------------ DESCUENTOS DEL ARTICULO ------------


    /**
     * @nombre: Lista los Descuentos de un Artículo
     * @descripcion: Lista los Descuentos de un Artículo
     */
    public function listar_x_articulo(){
        // Traemos los datos del articulo primero
        $stmt = $GLOBALS['conf']['pdo']->query("    SELECT      descuento, letra,
                                                                CONCAT(prefijo,'-',codigo,'-',sufijo) as codigo,
                                                                (   SELECT  id 
                                                                    FROM    catalogo_articulos 
                                                                    WHERE   (prefijo = t.prefijo) AND 
                                                                            (codigo = t.codigo) AND 
                                                                            (sufijo = t.sufijo)
                                                                ) as id
                                                    FROM        catalogo_articulos_descuentos t
                                                    WHERE       (prefijo = (SELECT prefijo FROM catalogo_articulos WHERE id = ".$GLOBALS['parametros']['articulo'].")) AND 
                                                                (codigo = (SELECT codigo FROM catalogo_articulos WHERE id = ". $GLOBALS['parametros']['articulo'].")) AND
                                                                (sufijo = (SELECT sufijo FROM catalogo_articulos WHERE id = ".$GLOBALS['parametros']['articulo']."))");
        $descuentos = $stmt->fetchAll(PDO::FETCH_ASSOC);


        //Devolvemos los descuentos
        $GLOBALS['resultado']->_result = $descuentos;
    }

    /**
     * @nombre: Edita un Descuento de un Articulo
     * @descripcion: Edita un Descuento de un Articulo
     */
    public function editar_x_articulo(){
        $articulo = $GLOBALS['parametros']['articulo'];
        $descuento = $GLOBALS['parametros']['descuento'];
        $letra = $GLOBALS['parametros']['letra'];

        $condicional[] = "((SELECT prefijo FROM catalogo_articulos WHERE id = ".$articulo.") = t.prefijo)";
        $condicional[] = "((SELECT codigo FROM catalogo_articulos WHERE id = ".$articulo.") = t.codigo)";
        $condicional[] = "((SELECT sufijo FROM catalogo_articulos WHERE id = ".$articulo.") = t.sufijo)";
        $condicional[] = "(t.letra = '".$letra."')";

        $sql = "UPDATE catalogo_articulos_descuentos t SET descuento = '".$descuento."' WHERE ".implode(' AND ', $condicional);
        $stmt = $GLOBALS['conf']['pdo']->query($sql);
    }

    /**
     * @nombre: Agrega un Descuento de un Articulo
     * @descripcion: Agrega un Descuento de un Articulo
     */
    public function agregar_x_articulo(){
        $articulo = $GLOBALS['parametros']['articulo'];
        $descuento = $GLOBALS['parametros']['descuento'];
        $letra = $GLOBALS['parametros']['letra'];

        $condicional[] = "((SELECT prefijo FROM catalogo_articulos WHERE id = ".$articulo.") = t.prefijo)";
        $condicional[] = "((SELECT codigo FROM catalogo_articulos WHERE id = ".$articulo.") = t.codigo)";
        $condicional[] = "((SELECT sufijo FROM catalogo_articulos WHERE id = ".$articulo.") = t.sufijo)";
        $condicional[] = "(t.letra = '".$letra."')";

        //Averiguamos primero si el descuento ya existe
        $stmt = $GLOBALS['conf']['pdo']->query("SELECT * FROM catalogo_articulos_descuentos t WHERE ".implode(' AND ', $condicional));
        if(count($stmt->fetchAll(PDO::FETCH_ASSOC))){
            $GLOBALS['resultado']->setError("La Letra de Descuento ya esta definida para este Artículo.");
            return;
        }

        //Insertamos
        $sql = "    INSERT INTO     catalogo_articulos_descuentos (prefijo,codigo,sufijo,letra,descuento) 
                    VALUES          ((SELECT prefijo FROM catalogo_articulos WHERE id = ".$articulo."),
                                     (SELECT codigo FROM catalogo_articulos WHERE id = ".$articulo."),
                                     (SELECT sufijo FROM catalogo_articulos WHERE id = ".$articulo."),
                                     '".$letra."','".$descuento."')";
        $stmt = $GLOBALS['conf']['pdo']->query($sql);
    }

    /**
     * @nombre: Elimina un Descuento de un Articulo
     * @descripcion: Elimina un Descuento de un Articulo
     */
    public function elimina_x_articulo(){
        $articulo = $GLOBALS['parametros']['articulo'];
        $letra = $GLOBALS['parametros']['letra'];

        $condicional[] = "((SELECT prefijo FROM catalogo_articulos WHERE id = ".$articulo.") = prefijo)";
        $condicional[] = "((SELECT codigo FROM catalogo_articulos WHERE id = ".$articulo.") = codigo)";
        $condicional[] = "((SELECT sufijo FROM catalogo_articulos WHERE id = ".$articulo.") = sufijo)";
        $condicional[] = "(letra = '".$letra."')";

        $sql = "DELETE FROM catalogo_articulos_descuentos WHERE ".implode(' AND ', $condicional);
        $stmt = $GLOBALS['conf']['pdo']->query($sql);
    }

    //--------------------------------------------------
    //------------ DESCUENTOS DEL CLIENTE ------------

    /**
     * @nombre: Lista los Descuentos de un Cliente
     * @descripcion: Lista los Descuentos de un Cliente
     */
    public function listar_x_cliente(){

        //Descuentos
        $stmt = $GLOBALS['conf']['pdo']->query("    SELECT  condicion_venta,
                                                            (SELECT nombre FROM descuentos_cond_vta WHERE id = c.condicion_venta) as condicion_venta_label,
                                                            descuento_letra
                                                    FROM    clientes as c
                                                    WHERE   usuario = '".$GLOBALS['parametros']['cliente']."'");
        $cliente = $stmt->fetchAll(PDO::FETCH_ASSOC);

        //Devolvemos los descuentos
        $GLOBALS['resultado']->_result['condicion_venta']['id'] = $cliente[0]['condicion_venta'];
        $GLOBALS['resultado']->_result['condicion_venta']['nombre'] = $cliente[0]['condicion_venta_label'];
        $GLOBALS['resultado']->_result['descuento_letra'] = $cliente[0]['descuento_letra'];

        // Traemos los datos del articulo primero
        $stmt = $GLOBALS['conf']['pdo']->query("    SELECT  *
                                                    FROM    descuentos 
                                                    WHERE   cliente = '".$GLOBALS['parametros']['cliente']."'");
        $GLOBALS['resultado']->_result['descuentos'] = $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * @nombre: Elimina un Descuento de un Cliente
     * @descripcion: Elimina un Descuento de un Cliente
     */
    public function eliminar_x_cliente(){

        //Descuentos
        $stmt = $GLOBALS['conf']['pdo']->query("    DELETE FROM     descuentos
                                                    WHERE           (cliente = '".$GLOBALS['parametros']['cliente']."') AND (prefijo = '".$GLOBALS['parametros']['prefijo']."')");
    }

    /**
     * @nombre: Agregar un Descuento de un Cliente
     * @descripcion: Agregar un Descuento de un Cliente
     */
    public function agregar_x_cliente(){

        // Traemos los datos del articulo primero
        $stmt = $GLOBALS['conf']['pdo']->query("SELECT  * FROM descuentos 
                                                WHERE   (cliente = '".$GLOBALS['parametros']['cliente']."') 
                                                        AND (prefijo = '".$GLOBALS['parametros']['prefijo']."')");

        $descuento = $stmt->fetchAll(PDO::FETCH_ASSOC);

        //Si existe el articulo
        if( count($descuento) ){
            $GLOBALS['resultado']->setError("Ya existe un descuento para ese prefijo");
            return;
        }

        //Descuentos
        $stmt = $GLOBALS['conf']['pdo']->query("INSERT INTO descuentos (prefijo, cliente, descuento) VALUES ('".$GLOBALS['parametros']['prefijo']."', '".$GLOBALS['parametros']['cliente']."', ".$GLOBALS['parametros']['descuento'].")");
    }

    /**
     * @nombre: Editar un Descuento de un Cliente
     * @descripcion: Editar un Descuento de un Cliente
     */
    public function editar_x_cliente(){

        // Traemos los datos del articulo primero
        $stmt = $GLOBALS['conf']['pdo']->query("SELECT  * FROM descuentos 
                                                WHERE   (cliente = '".$GLOBALS['parametros']['cliente']."') 
                                                        AND (prefijo = '".$GLOBALS['parametros']['prefijo']."')");

        $descuento = $stmt->fetchAll(PDO::FETCH_ASSOC);

        //Si existe el articulo
        if( !count($descuento) ){
            $GLOBALS['resultado']->setError("No existe un descuento con el prefijo '".$GLOBALS['parametros']['prefijo']."'para el cliente '".$GLOBALS['parametros']['cliente']."'");
            return;
        }

        //Descuentos
        $stmt = $GLOBALS['conf']['pdo']->query("UPDATE descuentos SET descuento = ".$GLOBALS['parametros']['descuento']." WHERE prefijo = '".$GLOBALS['parametros']['prefijo']."' AND cliente='".$GLOBALS['parametros']['cliente']."'");
    }

    /**
     * @nombre: Lista todas las Condiciones de Venta
     * @descripcion: Lista todas las Condiciones de Venta
     */
    public function listar_condiciones_venta(){
        //Separamos las palabras clave en un arreglo
        $keyword_tokens = explode(' ', $GLOBALS['parametros']['q']);

        //Calculate paging
        $page =  (intval($GLOBALS['parametros']['p'])-1) * 40;

        //SQL
        $stmt = $GLOBALS['conf']['pdo']->query("SELECT * FROM descuentos_cond_vta");
        $items = $stmt->fetchAll(PDO::FETCH_ASSOC);


        //Si estamos haciendo una busqueda..
        if($keyword_tokens[0] != '') 
            $items = $GLOBALS['toolbox']->matchingArray($items,'nombre',$keyword_tokens);

        //Calculamos totales
        $GLOBALS['resultado']->_result['total'] = count($items);

        //Devolvemos items y total
        $GLOBALS['resultado']->_result['items'] = array_slice($items, $page, 40);
    }


    //--------------------------------------------------
    //------------ IMPORTADORES ------------------------

    /**
     * @nombre: Importar descuentos del Articulo
     * @descripcion: Importa todos los descuentos según Articulo
     */
    public function importar_x_articulo(){

        //Ampliamos memoria
        ini_set('memory_limit', '2048M');

        //Ruta del archivo subido
        $archivo = $GLOBALS['parametros']['archivo']['nuevo_directorio'].$GLOBALS['parametros']['archivo']['name'];

        $fn = fopen($archivo,"r") or die("fail to open file");

        $excelLimpio = array();

        while($row = fgets($fn)) {

            $linea = explode(";", $row);
            $descuento = array();
            $descuento['prefijo'] = preg_replace("/[^A-Za-z0-9]/", '', $linea[0]);
            $descuento['codigo'] = preg_replace("/[^A-Za-z0-9]/", '', $linea[1]);
            $descuento['sufijo'] = preg_replace("/[^A-Za-z0-9]/", '', $linea[2]);
            $descuento['letra'] = preg_replace("/[^A-Za-z]/", '', $linea[4]);
            $descuento['descuento'] = number_format((float)preg_replace("/[^0-9.,]/", '', $linea[5]), 2, '.', '');

            if( ($descuento['prefijo'] != '') && ($descuento['codigo'] != '') && ($descuento['sufijo'] != '') && ($descuento['letra'] != '') && ($descuento['descuento'] != ''))
            $excelLimpio[] = $descuento;
        }

        //Borramos el archivo
        unlink($archivo);
        
        //Vaciamos los datos anteriores de la tabla
        $sql = "TRUNCATE catalogo_articulos_descuentos;";
        $sql .= "INSERT INTO catalogo_articulos_descuentos (prefijo, codigo, sufijo, letra, descuento) VALUES ";

        //Agregamos todos los datos nuevos
        $cont = 0;
        foreach ($excelLimpio as $k=>$v){
            if( ($v['prefijo'] != '') && ($v['codigo'] != '') && ($v['sufijo'] != '') && ($v['letra'] != '') && is_numeric($v['descuento']) ){
                if($cont > 0) $sql .= ", ";
                $sql .= "('".$v['prefijo']."','".$v['codigo']."','".$v['sufijo']."','".$v['letra']."','".$v['descuento']."')";
            }

            $cont++;
        }
        
        //Ejecutamos la query
        $GLOBALS['resultado']->_result['resultado'] = $GLOBALS['conf']['pdo']->query($sql);
    }


    /**
     * @nombre: Importar descuentos del Cliente
     * @descripcion: Importa todos los descuentos según Cliente
     */
    public function importar_x_cliente(){

        //Ampliamos memoria
        ini_set('memory_limit', '2048M');

        //Otenemos informacion del archivo Excel
        require_once './classes/PHPExcel.php';

        //Ruta del archivo subido
        $archivo = $GLOBALS['parametros']['archivo']['nuevo_directorio'].$GLOBALS['parametros']['archivo']['name'];

        //El arreglo tiene los nombres de las columnas
        $arrColumns = array(0=>'A',1=>'B',2=>'C',3=>'D',4=>'E',5=>'F',6=>'G',7=>'H',8=>'I',9=>'J',10=>'K',11=>'L',12=>'M',13=>'N',14=>'O',15=>'P',16=>'Q',17=>'R',18=>'S',19=>'T',20=>'U',21=>'V',22=>'W',23=>'X',24=>'Y',25=>'Z',26=>'AA',27=>'AB',28=>'AC',29=>'AD',30=>'AE',31=>'AF',32=>'AG',33=>'AH',34=>'AI',35=>'AJ',36=>'AK',37=>'AL',38=>'AM',39=>'AN',40=>'AO',41=>'AP',42=>'AQ',43=>'AR',44=>'AS',45=>'AT',46=>'AU',47=>'AV',48=>'AW',49=>'AX',50=>'AY',51=>'AZ',52=>'BA',53=>'BB',54=>'BC',55=>'BD',56=>'BE',57=>'BF',58=>'BG',59=>'BH',60=>'BI',61=>'BJ',62=>'BK',63=>'BL',64=>'BM',65=>'BN',66=>'BO',67=>'BP',68=>'BQ',69=>'BR',70=>'BS',71=>'BT',72=>'BU',73=>'BV',74=>'BW',75=>'BX',76=>'BY',77=>'BZ');

        //Creamos el objeto
        $objPHPExcel = PHPExcel_IOFactory::load($archivo);
        $objPHPExcel->setActiveSheetIndex(0);
        $aSheet = $objPHPExcel->getActiveSheet();

        //Que cantidad de Filas
        $countRows = $aSheet->getHighestRow();

        //Que cantidad de columnas tiene el Arhivo
        $highestColumn = $aSheet->getHighestColumn();
        $countCols = PHPExcel_Cell::columnIndexFromString($highestColumn);
        $excel = array();

        //Generamos el Arreglo
        for($i = 1; $i <= $countRows; $i++)
            for($y=0; $y<$countCols; $y++)
                $excel[$i][$arrColumns[$y]] = $aSheet->getCell($arrColumns[$y].$i)->getValue();



        //Borramos el archivo
        unlink($archivo);


        //Traemos todos los clientes para quitar los que no necesitamos
        $stmt = $GLOBALS['conf']['pdo']->query("SELECT usuario FROM clientes");
        $clientes =  $stmt->fetchAll(PDO::FETCH_COLUMN, 0);

        $GLOBALS['resultado']->_result['art'] = $excel;

        $c = array();
        foreach ($clientes as $k=>$cli){
            $c_tmp = array();
            $c_tmp['numero'] = $cli;
            $c_tmp['descuentos'] = array();
            $c_tmp['existe'] = false;

            //Recolectamos los datos del cliente
            foreach ($excel as $x=>$r){
                 if($cli == trim($r['A'])){
                    $c_tmp['cond_vta'] = trim($r['C']);
                    $c_tmp['letra_precios'] = trim($r['E']);
                    $c_tmp['existe'] = true;
                    break;
                }
            }

            //Recolectamos todos los descuentos
            foreach ($excel as $x=>$row)
                if($cli == trim($row['A']) && (trim($row['F']) != ''))
                    $c_tmp['descuentos'][] = array(trim($row['F']),number_format((float)$row['G'], 2, '.', ''));

            $c[] = $c_tmp;
        }

        $GLOBALS['resultado']->_result['c'] = $c;


        //Limpiamos descuentos
        $inicio = true;
        $sql = "TRUNCATE descuentos; ";
        $sql .= "INSERT INTO descuentos (cliente,prefijo,descuento) VALUES ";
        foreach ($c as $k=>$v)
            foreach ($v['descuentos'] as $x=>$v1){
                if(!$inicio) $sql .= ', ';
                $sql .= "('".$v['numero']."','".$v1[0]."','".$v1[1]."')";
                if($inicio) $inicio = false;
            }


        //Ejecutamos el SQL
        $GLOBALS['conf']['pdo']->query($sql);

        //Actualizamos todos los clientes
        $sql = '';
        foreach ($c as $k=>$v)
            if(isset($v['cond_vta']) && isset($v['letra_precios']))
            $sql .= "UPDATE clientes SET condicion_venta=".$v['cond_vta'].", descuento_letra='".$v['letra_precios']."' WHERE usuario='".$v['numero']."'; ";
        
        //Ejecutamos el SQL
        $GLOBALS['conf']['pdo']->query($sql);

        //Deshabilitamos los inexistentes
        $sql = "";
        foreach ($c as $k=>$c)
            if(!$c['existe']){
                $GLOBALS['resultado']->_result['deshabilitados'][] = $c['numero'];
                $sql .= "UPDATE usuarios SET activo=0 WHERE usuario = '".$c['numero']."'; ";
                $sql .= "DELETE FROM sessions WHERE usuario = '".$c['numero']."'; ";
            }

        if($sql != '') $GLOBALS['conf']['pdo']->query($sql);
    }
}
?>