<?php
/**
 * @nombre: Busqueda de Artículos
 * @descripcion: Busqueda Rápida y Avanzada de Artículos
 */
class busqueda extends module{

 	public $session_vars = array(	'advSearch_prefijo',
									'advSearch_codigo',
									'advSearch_sufijo',
									'advSearch_fabricante',
									'advSearch_vehiculo',
									'advSearch_vehiculo_filas_individuales',
									'advSearch_rubro',
									'advSearch_dimensiones',		//Arreglo de dimensiones
									'advSearch_busqueda_dim',		//Busqueda dimensional
									'advSearch_dim_en_columnas',
									'quicksearch_term',
									'quicksearch_ids',
									'quicksearch_ultimos');	//Dimensiones en columnas

    /*
	 * Constructor
	 */
    public function __construct(){

    	//Seteamos las variables si no exsiten en -1
    	foreach($this->session_vars as $k=>$v){
    		$valor = $GLOBALS['session']->getData($v);
    		//if('advSearch_sufijo' == $v) var_dump($valor);
    		if(is_bool($valor) && !$valor) 
    			$GLOBALS['session']->setData($v,'-1');
    	}
    }

	/** 
	* @nombre: Define el Prefijo (búsqueda avanzada)
	* @descripcion: Busqueda avanzada por Prefijo
	*/
    public function advSearch_prefijo(){

    	//Rol del usuario
    	$rol = $GLOBALS['session']->getData('rol');

        //Calculate paging
        $page =  (intval($GLOBALS['parametros']['p'])-1) * 40;

        //EStamos buscando o solo listando?
        if($GLOBALS['parametros']['q'] == '') $busqueda = false;
        else $busqueda = true;

    	//Get variables
    	foreach($this->session_vars as $k=>$v) $$v = $GLOBALS['session']->getData($v);

		//Search conditionals..
		$conditional = array();

		//Para Administradores y desarrollador mostramos todo
		if(($rol != 1) && ($rol != -1)) $conditional[] = " (habilitado = 1) ";

		//Filtro Prefijo
		if($advSearch_codigo != '-1')
			$conditional[] = " (codigo = ".$advSearch_codigo." ) ";

		//Filtro Sufijo
		if($advSearch_sufijo != '-1')
			$conditional[] = " (sufijo = ".$advSearch_sufijo." ) ";

		//Filtro Vehiculos
		if($advSearch_vehiculo != '-1'){
			//Explode marca and modelo
			$params = explode('#', $advSearch_vehiculo);

			//Make conditional

			$condtmp = "(id IN (SELECT articulo_id FROM catalogo_articulos_aplicaciones WHERE (marca_id = ".$params[0].")";

			//Add conditional for model...
			if($params[1] != '-1') $condtmp .= " AND (modelo_id = ".$params[1].")";

			//Finish sql conditional
			$condtmp .="))";

			//Add conditional
			$conditional[] = $condtmp; 
		}

		//Filtro Rubro
		if($advSearch_rubro != '-1')
			$conditional[] = " ( id_rubro IN (".$advSearch_rubro.") ) ";


		//Filtro Fabricante
		if($advSearch_fabricante != '-1')
			$conditional[] = " (id_marca = ".$advSearch_fabricante.") ";

		//Filtro Dimensiones
		if( ($advSearch_dimensiones != '-1') && (is_array($advSearch_dimensiones)) ){
			foreach($advSearch_dimensiones as $k=>$d){
				//Si tiene seteado algun valor...
				if($d['value'] != '-1'){
					$conditional[] = "( extra_".$d['id']." = '".$d['value']."' )";
				}
			}
		}

		//Sql
		$sql = "SELECT DISTINCT prefijo as id, prefijo as nombre FROM catalogo_articulos";
		

		//Embed conditionals
		if(count($conditional)) $sql .= " WHERE " . implode(' AND ', $conditional);


		//Traemos Prefijos
		$stmt = $GLOBALS['conf']['pdo']->prepare($sql);
		$stmt->execute();
		$codigos = $stmt->fetchAll(PDO::FETCH_ASSOC);

        //Si estamos buscando...
        if($busqueda){
            //Score
            foreach($codigos as $k=>$v)
                $codigos[$k]['score'] = $GLOBALS['toolbox']->compare_strings($GLOBALS['parametros']['q'], $v['nombre']);

            //Eliminamos 0's
            foreach($codigos as $k=>$v)
                if($v['score'] == 0)
                    unset($codigos[$k]);

            //Ordenamos
            usort($codigos, function($a, $b) {
                return $b['score'] - $a['score'];
            });
        }else{
            //Ordenamos
            usort($codigos, function($a, $b) {
            	return strnatcasecmp($a['nombre'], $b['nombre']);
            });
        }

        //Calculamos totales
        $GLOBALS['resultado']->_result['total'] = count($codigos);

        //Devolvemos items y total
        $GLOBALS['resultado']->_result['items'] = array_slice($codigos, $page, 40);
    }

	/** 
	* @nombre: Define el Código (búsqueda avanzada)
	* @descripcion: Busqueda avanzada por Código
	*/
    public function advSearch_codigo(){

    	//Rol del usuario
    	$rol = $GLOBALS['session']->getData('rol');

        //Calculate paging
        $page =  (intval($GLOBALS['parametros']['p'])-1) * 40;

        //EStamos buscando o solo listando?
        if($GLOBALS['parametros']['q'] == '') $busqueda = false;
        else $busqueda = true;

    	//Get variables
    	foreach($this->session_vars as $k=>$v) $$v = $GLOBALS['session']->getData($v);

		//Search conditionals..
		$conditional = array();

    	//Para Administradores y desarrollador mostramos todo
		if(($rol != 1) && ($rol != -1)) $conditional[] = " (habilitado = 1) ";

		//Filtro Prefijo
		if($advSearch_prefijo != '-1')
			$conditional[] = " (prefijo = ".$advSearch_prefijo." ) ";

		//Filtro Sufijo
		if($advSearch_sufijo != '-1')
			$conditional[] = " (sufijo = ".$advSearch_sufijo." ) ";

		//Filtro Vehiculos
		if($advSearch_vehiculo != '-1'){
			//Explode marca and modelo
			$params = explode('#', $advSearch_vehiculo);

			//Make conditional
			
			$condtmp = "(id IN (SELECT articulo_id FROM catalogo_articulos_aplicaciones WHERE (marca_id = ".$params[0].")";

			//Add conditional for model...
			if($params[1] != '-1') $condtmp .= " AND (modelo_id = ".$params[1].")";

			//Finish sql conditional
			$condtmp .="))";

			//Add conditional
			$conditional[] = $condtmp; 
		}

		//Filtro Rubro
		if($advSearch_rubro != '-1')
			$conditional[] = " ( id_rubro IN (".$advSearch_rubro.") ) ";

		//Filtro Fabricante
		if($advSearch_fabricante != '-1')
			$conditional[] = " (id_marca = ".$advSearch_fabricante.") ";

		//Filtro Dimensiones
		if( ($advSearch_dimensiones != '-1') && (is_array($advSearch_dimensiones)) ){
			foreach($advSearch_dimensiones as $k=>$d){
				//Si tiene seteado algun valor...
				if($d['value'] != '-1'){
					$conditional[] = "( extra_".$d['id']." = '".$d['value']."' )";
				}
			}
		}

		//Sql
		$sql = "SELECT DISTINCT codigo as nombre, codigo as id FROM catalogo_articulos";
		

		//Embed conditionals
		if(count($conditional)) $sql .= " WHERE " . implode(' AND ', $conditional);


		//DB Query
		$stmt = $GLOBALS['conf']['pdo']->prepare($sql);
		$stmt->execute();
		
		//Traemos Prefijos
		$stmt = $GLOBALS['conf']['pdo']->prepare($sql);
		$stmt->execute();
		$codigos = $stmt->fetchAll(PDO::FETCH_ASSOC);

        //Si estamos buscando...
        if($busqueda){
            //Score
            foreach($codigos as $k=>$v)
                $codigos[$k]['score'] = $GLOBALS['toolbox']->compare_strings($GLOBALS['parametros']['q'], $v['nombre']);

            //Eliminamos 0's
            foreach($codigos as $k=>$v)
                if($v['score'] == 0)
                    unset($codigos[$k]);

            //Ordenamos
            usort($codigos, function($a, $b) {
                return $b['score'] - $a['score'];
            });
        }else{
            //Ordenamos
            usort($codigos, function($a, $b) {
            	return strnatcasecmp($a['nombre'], $b['nombre']);
            });
        }

        //Calculamos totales
        $GLOBALS['resultado']->_result['total'] = count($codigos);

        //Devolvemos items y total
        $GLOBALS['resultado']->_result['items'] = array_slice($codigos, $page, 40);
    }

	/** 
	* @nombre: Define el Sufijo (búsqueda avanzada)
	* @descripcion: Busqueda avanzada por Sufijo
	*/
    public function advSearch_sufijo(){

    	//Rol del usuario
    	$rol = $GLOBALS['session']->getData('rol');

        //Calculate paging
        $page =  (intval($GLOBALS['parametros']['p'])-1) * 40;

        //EStamos buscando o solo listando?
        if($GLOBALS['parametros']['q'] == '') $busqueda = false;
        else $busqueda = true;

    	//Get variables
    	foreach($this->session_vars as $k=>$v) $$v = $GLOBALS['session']->getData($v);

		//Search conditionals..
		$conditional = array();

    	//Para Administradores y desarrollador mostramos todo
		if(($rol != 1) && ($rol != -1)) $conditional[] = " (habilitado = 1) ";

		//Filtro Prefijo
		if($advSearch_prefijo != '-1')
			$conditional[] = " (prefijo = ".$advSearch_prefijo." ) ";

		//Filtro Codigo
		if($advSearch_codigo != '-1')
			$conditional[] = " (codigo = ".$advSearch_codigo." ) ";

		//Filtro Vehiculos
		if($advSearch_vehiculo != '-1'){
			//Explode marca and modelo
			$params = explode('#', $advSearch_vehiculo);

			//Make conditional
			$condtmp = "(id IN (SELECT articulo_id FROM catalogo_articulos_aplicaciones WHERE (marca_id = ".$params[0].")";

			//Add conditional for model...
			if($params[1] != '-1') $condtmp .= " AND (modelo_id = ".$params[1].")";

			//Finish sql conditional
			$condtmp .="))";

			//Add conditional
			$conditional[] = $condtmp; 
		}

		//Filtro Rubro
		if($advSearch_rubro != '-1')
			$conditional[] = " ( id_rubro IN (".$advSearch_rubro.") ) ";

		//Filtro Fabricante
		if($advSearch_fabricante != '-1')
			$conditional[] = " (id_marca = ".$advSearch_fabricante.") ";

		//Filtro Dimensiones
		if( ($advSearch_dimensiones != '-1') && (is_array($advSearch_dimensiones)) ){
			foreach($advSearch_dimensiones as $k=>$d){
				//Si tiene seteado algun valor...
				if($d['value'] != '-1'){
					$conditional[] = "( extra_".$d['id']." = '".$d['value']."' )";
				}
			}
		}


		//Sql
		$sql = "SELECT DISTINCT sufijo as nombre, sufijo as id FROM catalogo_articulos";
		

		//Embed conditionals
		if(count($conditional)) $sql .= " WHERE " . implode(' AND ', $conditional);


		//Buscamos el usuario en la base de datos por EMAIL o #CLIENTE
		$stmt = $GLOBALS['conf']['pdo']->prepare($sql);
		$stmt->execute();
		$codigos = $stmt->fetchAll(PDO::FETCH_ASSOC);

		//Traemos Prefijos
		$stmt = $GLOBALS['conf']['pdo']->prepare($sql);
		$stmt->execute();
		$codigos = $stmt->fetchAll(PDO::FETCH_ASSOC);

        //Si estamos buscando...
        if($busqueda){
            //Score
            foreach($codigos as $k=>$v)
                $codigos[$k]['score'] = $GLOBALS['toolbox']->compare_strings($GLOBALS['parametros']['q'], $v['nombre']);

            //Eliminamos 0's
            foreach($codigos as $k=>$v)
                if($v['score'] == 0)
                    unset($codigos[$k]);

            //Ordenamos
            usort($codigos, function($a, $b) {
                return $b['score'] - $a['score'];
            });
        }else{
            //Ordenamos
            usort($codigos, function($a, $b) {
            	return strnatcasecmp($a['nombre'], $b['nombre']);
            });
        }

        //Calculamos totales
        $GLOBALS['resultado']->_result['total'] = count($codigos);

        //Devolvemos items y total
        $GLOBALS['resultado']->_result['items'] = array_slice($codigos, $page, 40);
    }

	/** 
	* @nombre: Define el Fabricante (búsqueda avanzada)
	* @descripcion: Busqueda avanzada por Fabricante
	*/
    public function advSearch_fabricante(){

    	//Rol del usuario
    	$rol = $GLOBALS['session']->getData('rol');

        //Calculate paging
        $page =  (intval($GLOBALS['parametros']['p'])-1) * 40;

        //EStamos buscando o solo listando?
        if($GLOBALS['parametros']['q'] == '') $busqueda = false;
        else $busqueda = true;

    	//Get variables
    	foreach($this->session_vars as $k=>$v) $$v = $GLOBALS['session']->getData($v);

		//Search conditionals..
		$conditional = array();

    	//Para Administradores y desarrollador mostramos todo
		if(($rol != 1) && ($rol != -1)) $conditional[] =  "(id in (SELECT DISTINCT id_marca FROM catalogo_articulos WHERE habilitado = 1))";

		$conditional[] = "(nivel = 2)";

		//Obtenemos solo mas marcas existen asignadas a articulos
		$conditional[] = "(id in (SELECT DISTINCT id_marca FROM catalogo_articulos))";

		//Filtro Prefijo
		if($advSearch_prefijo != '-1')
			$conditional[] = "(id in (SELECT DISTINCT id_marca FROM catalogo_articulos WHERE prefijo = ".$advSearch_prefijo."))";

		//Filtro Codigo
		if($advSearch_codigo != '-1')
			$conditional[] = "(id in (SELECT DISTINCT id_marca FROM catalogo_articulos WHERE codigo = ".$advSearch_codigo."))";

		//Filtro Sufijo
		if($advSearch_sufijo != '-1')
			$conditional[] = "(id in (SELECT DISTINCT id_marca FROM catalogo_articulos WHERE sufijo = ".$advSearch_sufijo."))";

		//Filtro Vehiculos
		if($advSearch_vehiculo != '-1'){
			//Explode marca and modelo
			$params = explode('#', $advSearch_vehiculo);

			//Make conditional
			$condtmp = "(id IN (SELECT DISTINCT id_marca FROM catalogo_articulos WHERE id IN (SELECT articulo_id FROM catalogo_articulos_aplicaciones WHERE (marca_id = ".$params[0].")";

			//Add conditional for model...
			if($params[1] != '-1') $condtmp .= " AND (modelo_id = ".$params[1].")";

			//Finish sql conditional
			$condtmp .=")))";

			//Add conditional
			$conditional[] = $condtmp; 
		}

		//Filtro Rubro
		if($advSearch_rubro != '-1')
			$conditional[] = "(id in (SELECT DISTINCT id_marca FROM catalogo_articulos WHERE id_rubro IN (".$advSearch_rubro.")))";

		//Filtro Dimensiones
		if( ($advSearch_dimensiones != '-1') && (is_array($advSearch_dimensiones)) )
			foreach($advSearch_dimensiones as $k=>$d)
				if($d['value'] != '-1')
					$conditional[] = "(id in (SELECT DISTINCT id_marca FROM catalogo_articulos WHERE extra_".$d['id']." = '".$d['value']."'))";

		//Sql
		$sql = "SELECT id, nombre FROM catalogo_marcas";

		//Condicionales
		if(count($conditional)) $sql .= " WHERE " . implode(' AND ', $conditional);

		$sql .= " ORDER BY nombre ASC";

		//DB Query
		$stmt = $GLOBALS['conf']['pdo']->prepare($sql);
		$stmt->execute();
		$fabricantes = $stmt->fetchAll(PDO::FETCH_ASSOC);

        //Si estamos buscando...
        if($busqueda){
            //Score
            foreach($fabricantes as $k=>$v)
                $fabricantes[$k]['score'] = $GLOBALS['toolbox']->compare_strings($GLOBALS['parametros']['q'], $v['nombre']);

            //Eliminamos 0's
            foreach($fabricantes as $k=>$v)
                if($v['score'] == 0)
                    unset($fabricantes[$k]);

            //Ordenamos
            usort($fabricantes, function($a, $b) {
                return $b['score'] - $a['score'];
            });
        }

        //Existen logos?
        foreach($fabricantes as $k=>$f)
        	if( file_exists('./fabricantes/'.$f['id'].'.svg') )
        		$fabricantes[$k]['logo'] = $f['id'].'.svg';

        //Calculamos totales
        $GLOBALS['resultado']->_result['total'] = count($fabricantes);

        //Devolvemos items y total
        $GLOBALS['resultado']->_result['items'] = array_slice($fabricantes, $page, 40);
    }

	/** 
	* @nombre: Define el Vehículo (búsqueda avanzada)
	* @descripcion: Busqueda avanzada por Vehículo
	*/
    public function advSearch_vehiculo(){

    	//Rol del usuario
    	$rol = $GLOBALS['session']->getData('rol');

    	//Separamos las palabras clave en un arreglo
    	$keyword_tokens = explode(' ', $GLOBALS['parametros']['q']);

    	//Calculate paging
    	$page =  (intval($GLOBALS['parametros']['p'])-1) * 40;

        //Buscando o listando
        if($GLOBALS['parametros']['q'] == '') $busqueda = false;
        else $busqueda = true;

    	//Get variables
    	foreach($this->session_vars as $k=>$v) $$v = $GLOBALS['session']->getData($v);

		//Search conditionals..
		$conditional = array();

    	//Para Administradores y desarrollador mostramos todo
		if(($rol != 1) && ($rol != -1)) 
			$conditional[] = "(id in ( SELECT DISTINCT modelo_id AS id FROM catalogo_articulos_aplicaciones WHERE articulo_id IN (SELECT id FROM catalogo_articulos WHERE habilitado = 1)))";

		if($advSearch_prefijo != '-1')
			$conditional[] = "(id in ( SELECT DISTINCT modelo_id AS id FROM catalogo_articulos_aplicaciones WHERE articulo_id IN (SELECT id FROM catalogo_articulos WHERE prefijo = ".$advSearch_prefijo.")))";

		if($advSearch_codigo != '-1')
			$conditional[] = "(id in ( SELECT DISTINCT modelo_id AS id FROM catalogo_articulos_aplicaciones WHERE articulo_id IN (SELECT id FROM catalogo_articulos WHERE codigo = ".$advSearch_codigo.")))";

		if($advSearch_sufijo != '-1')
			$conditional[] = "(id in ( SELECT DISTINCT modelo_id AS id FROM catalogo_articulos_aplicaciones WHERE articulo_id IN (SELECT id FROM catalogo_articulos WHERE sufijo = ".$advSearch_sufijo.")))";

		if($advSearch_fabricante != '-1')
			$conditional[] = "(id in ( SELECT DISTINCT modelo_id FROM catalogo_articulos_aplicaciones WHERE articulo_id IN (SELECT id FROM catalogo_articulos WHERE id_marca = ".$advSearch_fabricante.")))";

		if($advSearch_rubro != '-1')
			$conditional[] = "( id IN (SELECT DISTINCT modelo_id FROM catalogo_articulos_aplicaciones WHERE articulo_id IN (SELECT id FROM catalogo_articulos WHERE id_rubro IN (".$advSearch_rubro."))))";


		//Filtro Dimensiones
		if( ($advSearch_dimensiones != '-1') && (is_array($advSearch_dimensiones)) ){
			foreach($advSearch_dimensiones as $k=>$d){
				//Si tiene seteado algun valor...
				if($d['value'] != '-1'){
					$conditional[] = "(id in ( SELECT DISTINCT modelo_id AS id FROM catalogo_articulos_aplicaciones WHERE articulo_id IN (SELECT id FROM catalogo_articulos WHERE extra_".$d['id']." = '".$d['value']."')))";
				}
			}
		}

		        //Calculamos totales
        //$GLOBALS['resultado']->_result['debug'] = $advSearch_dimensiones;

		//Base SQL
		$sql = "SELECT  id, 
						id_marca,
						CONCAT(id_marca,'#',id) as ide,
						CONCAT((SELECT nombre FROM catalogo_autos_marca WHERE id = id_marca),' ',nombre) as nom,
						nombre,
						(SELECT nombre FROM catalogo_autos_marca WHERE id = id_marca) as marca 
				FROM 	catalogo_autos_modelo 
				WHERE 	(id IN (SELECT modelo_id FROM catalogo_articulos_aplicaciones))";


		//Add conditionals
		if(count($conditional)) $sql .= " AND ".implode(' AND ', $conditional);


		//Agregamos orden
		$sql .= "ORDER BY marca ASC, nombre ASC";

		$GLOBALS['resultado']->_result['debug'] = $sql;

		//Make SQL query
		$stmt = $GLOBALS['conf']['pdo']->prepare($sql);
		$stmt->execute();

		//Guardamos los resultados
		$vehiculos = $stmt->fetchAll(PDO::FETCH_ASSOC);

		//Agregamos [TODOS] para todas las marcas existentes
		$marcasSolas = array();
		foreach($vehiculos as $k=>$v){
			$cont = ( isset($marcasSolas[$v['id_marca']]) ) ? $marcasSolas[$v['id_marca']]["c"]+1 : 0;
			$marcasSolas[$v['id_marca']] = array( "nom" => $v['marca'], "c" => $cont);
		}

		//Agregamos al arreglo de vehiculos
		foreach ($marcasSolas as $k=>$v) 
			if($v['c'] > 1) $vehiculos[] = array( "ide" => $k.'#-1', "nom" => $v['nom'] );


        //Si estamos buscando...
        if($busqueda){
            //Score
            foreach($vehiculos as $k=>$v)
                $vehiculos[$k]['score'] = $GLOBALS['toolbox']->compare_strings($GLOBALS['parametros']['q'], $v['nom']);

            //Eliminamos 0's
            foreach($vehiculos as $k=>$v)
                if($v['score'] == 0)
                    unset($vehiculos[$k]);

            //Ordenamos
            usort($vehiculos, function($a, $b) {
            	//Si el score es el mismo
            	//Ordenamos por natcmp
            	if($b['score'] == $a['score'])
            		return strnatcasecmp($a['nom'], $b['nom']);

            	//Sino por score
                return $b['score'] - $a['score'];
            });
        }else{
            //Ordenamos
            usort($vehiculos, function($a, $b) {
            	return strnatcasecmp($a['nom'], $b['nom']);
            });
        }

        //Calculamos totales
        $GLOBALS['resultado']->_result['total'] = count($vehiculos);

        //Devolvemos items y total
        $GLOBALS['resultado']->_result['items'] = array_slice($vehiculos, $page, 40);

        //Limpiamos
        foreach ($GLOBALS['resultado']->_result['items'] as $k=>$v){
			$GLOBALS['resultado']->_result['items'][$k]['id'] = $v['ide'];
			unset(	$GLOBALS['resultado']->_result['items'][$k]['ide'],
					$GLOBALS['resultado']->_result['items'][$k]['id_marca'],
					$GLOBALS['resultado']->_result['items'][$k]['nombre'],
					$GLOBALS['resultado']->_result['items'][$k]['marca']);
        }
    }

	/** 
	* @nombre: Define el Rubro (búsqueda avanzada)
	* @descripcion: Busqueda avanzada por Rubro
	*/
    public function advSearch_rubro(){

    	//Rol del usuario
    	$rol = $GLOBALS['session']->getData('rol');

    	//Get variables
    	foreach($this->session_vars as $k=>$v) $$v = $GLOBALS['session']->getData($v);

		//Search conditionals..
		$conditional = array();

    	//Para Administradores y desarrollador mostramos todo
		if(($rol != 1) && ($rol != -1)) $conditional[] = "(id in (SELECT DISTINCT id_rubro FROM catalogo_articulos WHERE habilitado = 1))";

		//Todos los articulos que tienen rubros
		$conditional[] = "(id in (SELECT DISTINCT id_rubro FROM catalogo_articulos))";

		//Filtro Prefijo
		if($advSearch_prefijo != '-1')
			$conditional[] = "(id in (SELECT DISTINCT id_rubro FROM catalogo_articulos WHERE prefijo = ".$advSearch_prefijo."))";

		//Filtro Codigo
		if($advSearch_codigo != '-1')
			$conditional[] = "(id in (SELECT DISTINCT id_rubro FROM catalogo_articulos WHERE codigo = ".$advSearch_codigo."))";

		//Filtro Sufijo
		if($advSearch_sufijo != '-1')
			$conditional[] = "(id in (SELECT DISTINCT id_rubro FROM catalogo_articulos WHERE sufijo = ".$advSearch_sufijo."))";

		//Filtro Fabricante
		if($advSearch_fabricante != '-1')
			$conditional[] = "(id in (SELECT DISTINCT id_rubro FROM catalogo_articulos WHERE id_marca = ".$advSearch_fabricante."))";

		//Filtro Vehiculos
		if($advSearch_vehiculo != '-1'){
			//Explode marca and modelo
			$params = explode('#', $advSearch_vehiculo);

			//Make conditional
			$condtmp = "(id IN (SELECT DISTINCT id_rubro FROM catalogo_articulos WHERE id IN (SELECT articulo_id FROM catalogo_articulos_aplicaciones WHERE (marca_id = ".$params[0].")";

			//Add conditional for model...
			if($params[1] != '-1') $condtmp .= " AND (modelo_id = ".$params[1].")";

			//Finish sql conditional
			$condtmp .=")))";

			//Add conditional
			$conditional[] = $condtmp; 
		}

		//Filtro Dimensiones
		if( ($advSearch_dimensiones != '-1') && (is_array($advSearch_dimensiones)) ){
			foreach($advSearch_dimensiones as $k=>$d){
				//Si tiene seteado algun valor...
				if($d['value'] != '-1'){
					$conditional[] = "(id in (SELECT DISTINCT id_rubro FROM catalogo_articulos WHERE extra_".$d['id']." = '".$d['value']."'))";
				}
			}
		}

		//Obtenemos id filtrados
		$sql = "SELECT id FROM catalogo_rubros WHERE ".implode(' AND ', $conditional).";";
        
        //Todos los rubros
        $sql .= "SELECT * FROM catalogo_rubros WHERE id <> 1 ORDER BY nombre;";

        //Hacemos la consulta
        $stmt = $GLOBALS['conf']['pdo']->query($sql);

        //Obtenemos Rubros Filtrados
        $filtrados = $stmt->fetchAll(PDO::FETCH_COLUMN, 0);
        $stmt->nextRowset();

        //Obtenemos todos los Rubros
        $rubros = $GLOBALS['toolbox']->rubros_nivel($stmt->fetchAll(PDO::FETCH_ASSOC));

        //Generamos el nivel de cada Rubro
        $rubros = $rubros_tmp = $GLOBALS['toolbox']->parentChildSort_r('id', 'padre', $rubros, $parentID = 1);

        //Paginado
        $page =  (intval($GLOBALS['parametros']['p'])-1) * 40;

        //Estamos buscando o listando
        if($GLOBALS['parametros']['q'] == '') $busqueda = false;
        else $busqueda = true;


        //creamos el breadcrumb
        foreach($rubros as $k=>$r) 
            $rubros[$k]['bread'] = $GLOBALS['toolbox']->getBreadcrumbRubros($rubros,$r['id']);


        //Agregamos los padres de los filtrados
        foreach($filtrados as $k=>$f)
        	$filtrados = array_merge($filtrados, $GLOBALS['toolbox']->traeRubrosPadres($rubros_tmp,$f) );


        // revisamos que existan todos los 
        // padres de cada uno de los rubros
        foreach ($rubros as $k=>$v)
        	if( !in_array((int)$v['id'], $filtrados))
        		unset($rubros[$k]);


        //Si estamos buscando...
        if($busqueda){
            //Score
            foreach($rubros as $k=>$v)
                $rubros[$k]['score'] = $GLOBALS['toolbox']->compare_strings($GLOBALS['parametros']['q'], $v['bread']);

            // revisamos que existan todos los 
            // padres de cada uno de los rubros
            foreach ($rubros as $k=>$v) {
                // Si sumo algo de score
                // Y tiene algun padre q
                // no sea el 1
                if($v['score'] > 0){
                    //ids de los progenitores
                    $padres =  $GLOBALS['toolbox']->traeRubrosPadres($rubros,$v['id']);
                    //print_r($padres);

                    foreach ($rubros as $x=>$r1)
                        if( in_array($r1['id'],$padres) ){
                            //echo "padre: ".$r1['id'];
                            if($r1['score'] < $v['score'])
                                $rubros[$x]['score'] = $v['score'];
                        }
                    }
            }

            //Eliminamos 0's
            foreach($rubros as $k=>$v)
                if($v['score'] == 0)
                    unset($rubros[$k]);

            //Ordenamos
            usort($rubros, function($a, $b) {
                //Si el score es el mismo
                //Desempatan con natural order
                if ($a['score'] == $b['score'])
                    return strnatcmp ( $a['bread'], $b['bread'] );

                //Sino por score
                return ($a['score'] < $b['score']) ? 1 : -1;
            });
        }

        //Borramos variables no usadas
        foreach($rubros as $k=>$v)
            unset(  $rubros[$k]['padre'],
                    $rubros[$k]['nombre_singular'],
                    $rubros[$k]['orden'],
                    $rubros[$k]['nombre']);

        //Calculamos totales
        $GLOBALS['resultado']->_result['total'] = count($rubros);

        //Devolvemos items y total
        $GLOBALS['resultado']->_result['items'] = array_slice($rubros, $page, 40);
    }


	/** 
	* @nombre: Define la Dimensión (búsqueda avanzada)
	* @descripcion: Busqueda avanzada por Dimensiones
	*/
    public function advSearch_dimension(){

    	//Rol del usuario
    	$rol = $GLOBALS['session']->getData('rol');

    	//Get variables
    	foreach($this->session_vars as $k=>$v) $$v = $GLOBALS['session']->getData($v);

    	//El ID de la dimension pertenece al RUBRO?
    	$dimPerteneceRubro = false;
    	foreach($advSearch_dimensiones as $k=>$d)
    		if($d['id'] == $GLOBALS['parametros']['dimension'])
    			$dimPerteneceRubro = true;

        //Si el ID de la variable no pertence al RUBRO
        if(!$dimPerteneceRubro){
            //Agregamos error 
            $GLOBALS['resultado']->setError("La dimension no pertence al Rubro");
            return;
        }

		//Search conditionals..
		$conditional = array();

    	//Para Administradores y desarrollador mostramos todo
		if(($rol != 1) && ($rol != -1)) $conditional[] = " (habilitado = 1) ";

		$conditional[] = "( extra_".$GLOBALS['parametros']['dimension']." IS NOT NULL )";
		$conditional[] = "( extra_".$GLOBALS['parametros']['dimension']." <> '' )";

		//Filtro Rubro
		if($advSearch_rubro != '-1'){
			$stmt = $GLOBALS['conf']['pdo']->prepare("SELECT id, nivel, padre FROM catalogo_rubros WHERE (id <> 1) ORDER BY orden");
			$stmt->execute();
			$rubros = $stmt->fetchAll(PDO::FETCH_ASSOC);
			$hijos = array_merge( (array)$advSearch_rubro , $GLOBALS['toolbox']->traeRubrosHijos($rubros, $advSearch_rubro) );

			$conditional[] = "( id_rubro IN (".implode(',', $hijos).") )";
		}

		//Filtro Prefijo
		if($advSearch_prefijo != '-1')
			$conditional[] = "( prefijo = ".$advSearch_prefijo." )";

		//Filtro Codigo
		if($advSearch_codigo != '-1')
			$conditional[] = "(  codigo = ".$advSearch_codigo." )";

		//Filtro Sufijo
		if($advSearch_sufijo != '-1')
			$conditional[] = "( sufijo = ".$advSearch_sufijo." )";

		//Filtro Fabricante
		if($advSearch_fabricante != '-1')
			$conditional[] = "( id_marca = ".$advSearch_fabricante." )";

		//Filtro Vehiculos
		if($advSearch_vehiculo != '-1'){
			//Explode marca and modelo
			$params = explode('#', $advSearch_vehiculo);

			//Make conditional
			$condtmp = "(id IN (SELECT articulo_id FROM catalogo_articulos_aplicaciones WHERE (marca_id = ".$params[0].")";

			//Add conditional for model...
			if($params[1] != '-1') $condtmp .= " AND (modelo_id = ".$params[1].")";

			//Finish sql conditional
			$condtmp .="))";

			//Add conditional
			$conditional[] = $condtmp; 
		}

		//Filtro Dimensiones
		if( ($advSearch_dimensiones != '-1') && (is_array($advSearch_dimensiones)) ){
			foreach($advSearch_dimensiones as $k=>$d){
				//Si tiene seteado algun valor...
				if($d['value'] != '-1'){
					$conditional[] = "(extra_".$d['id']." = '".$d['value']."')";
				}
			}
		}

		//Obtenemos id filtrados
		$sql = "SELECT DISTINCT extra_".$GLOBALS['parametros']['dimension']." FROM catalogo_articulos WHERE ".implode(' AND ', $conditional);
		$stmt = $GLOBALS['conf']['pdo']->prepare($sql);
		$stmt->execute();
		$dim = $stmt->fetchAll(PDO::FETCH_ASSOC);

		$items = array();

    	//El ID de la dimension pertenece al RUBRO?
    	$opcRub = array();
    	foreach($advSearch_dimensiones as $k=>$d)
    		if( ($d['id'] == $GLOBALS['parametros']['dimension']) && is_array($d['opciones']) && count($d['opciones']))
    			$opcRub = $d['opciones'];


		//Recorremos los resultados
		foreach($dim as $k=>$d){
			$dTmp = array();
			$dTmp['id'] = $d['extra_'.$GLOBALS['parametros']['dimension']];

			//Si tenemos opciones...
			if(count($opcRub)){
    			//Por cada una de las opciones
    			foreach($opcRub as $k1=>$d1)
    				//Cuando encontramos la opcion
    				if($d1['id'] == $dTmp['id'])
    					$dTmp['nombre'] = $d1['nombre'];
    		
    		//Si no hay opciones
    		}else{
				$dTmp['nombre'] = $dTmp['id'];
    		}

    		//Agregamos a la lista de resultados
			$items[] = $dTmp;
		}



        //Calculate paging
        $page =  (intval($GLOBALS['parametros']['p'])-1) * 40;

        //EStamos buscando o solo listando?
        if($GLOBALS['parametros']['q'] == '') $busqueda = false;
        else $busqueda = true;

        //Si estamos buscando...
        if($busqueda){
            //Score
            foreach($items as $k=>$v)
                $items[$k]['score'] = $GLOBALS['toolbox']->compare_strings($GLOBALS['parametros']['q'], $v['nombre']);

            //Eliminamos 0's
            foreach($items as $k=>$v)
                if($v['score'] == 0)
                    unset($items[$k]);

            //Ordenamos por Score desempatamos por nombre
            usort($items, function($a, $b) {
                //Si el score es el mismo
                //Desempatan con natural order
                if ($a['score'] == $b['score'])
                    return strnatcmp ( $a['nombre'], $b['nombre'] );

                //Sino por score
                return ($a['score'] < $b['score']) ? 1 : -1;
            });
        }else{

			//Ordenamos por nombre
			usort($items, function($a, $b) {
				return strnatcmp ( $a['nombre'], $b['nombre'] );
			});
        }

        //Calculamos totales
        $GLOBALS['resultado']->_result['total'] = count($items);

        //Devolvemos items y total
        $GLOBALS['resultado']->_result['items'] = array_slice($items, $page, 40);
    }

	/** 
	* @nombre: Setea sessionvars (búsqueda avanzada)
	* @descripcion: Setea Sessionvars
	*/
	public function advSearch_setSessionVar(){

    	//Rol del usuario
    	$rol = $GLOBALS['session']->getData('rol');

		//El rubro sigue siendo el mismo que antes?
		$rubros_anteriores = $GLOBALS['session']->getData('advSearch_rubro');

    	//Seteamos el valor de la variable que nos estan pasando antes que nada
    	if( in_array( $GLOBALS['parametros']['variable'], $this->session_vars) ){
    		$GLOBALS['session']->setData($GLOBALS['parametros']['variable'], $GLOBALS['parametros']['value']);
    	}


    	//QUICKSEARCH: si quitamos los resultados de la busqueda rapida tambien quitamos variables asosciadas
    	if(($GLOBALS['parametros']['variable'] == 'quicksearch_term') && ($GLOBALS['parametros']['value'] == '-1')){
    		$GLOBALS['session']->setData('advSearch_rubro', '-1');
			$GLOBALS['session']->setData('advSearch_busqueda_dim','-1');
    		$GLOBALS['session']->setData('quicksearch_term', '-1');
    		$GLOBALS['session']->setData('quicksearch_ids', '-1');
    		$GLOBALS['session']->setData('quicksearch_ultimos', '-1');
    		$GLOBALS['session']->setData('advSearch_dim_en_columnas', '-1');
    	}

    	//QUICKSEARCH: Si seleccionamos otra busqueda que no sea rapida, ni modos deshacemos la busqueda rapida
    	if(	($GLOBALS['parametros']['variable'] != 'quicksearch_term') AND 
    		($GLOBALS['parametros']['variable'] != 'advSearch_dim_en_columnas') AND
    		($GLOBALS['parametros']['variable'] != 'advSearch_vehiculo_filas_individuales')){
    		$GLOBALS['session']->setData('quicksearch_term', '-1');
    		$GLOBALS['session']->setData('quicksearch_ids', '-1');
    		$GLOBALS['session']->setData('quicksearch_ultimos', '-1');
    	}


    	
    	//RUBRO: Si quitamos el rubro tambien quitamos tambien toda la parte de las dimensiones
		if( ($GLOBALS['parametros']['variable'] == 'advSearch_rubro') && ($GLOBALS['parametros']['value'] == '-1')){
			$GLOBALS['session']->setData('advSearch_dimensiones','-1');
			$GLOBALS['session']->setData('advSearch_dim_en_columnas','-1');
			$GLOBALS['session']->setData('advSearch_busqueda_dim','-1');
		}

		//DIMENSIONES EN CULUMNAS
		if( $GLOBALS['parametros']['variable'] == 'advSearch_dim_en_columnas'){

			//Si estamos activando las dimensiones en columnas
			if($GLOBALS['parametros']['value'] == '1'){

				foreach($this->session_vars as $k=>$v){ 
					$sv[$v] = $GLOBALS['session']->getData($v);
				}

				//Search conditionals..
				$condicionales = array();

				//Filtro Prefijo
				if($sv['advSearch_prefijo'] != '-1')
					$condicionales[] = " (prefijo = '".$sv['advSearch_prefijo']."' ) ";

				//Filtro Codigo
				if($sv['advSearch_codigo'] != '-1')
					$condicionales[] = " (codigo = '".$sv['advSearch_codigo']."' ) ";

				//Filtro Sufijo
				if($sv['advSearch_sufijo'] != '-1')
					$condicionales[] = " (sufijo = '".$sv['advSearch_sufijo']."' ) ";

				//Filtro Vehiculos
				$vehiculo = $sv['advSearch_vehiculo'];
				if($sv['advSearch_vehiculo'] != '-1'){
					//Explode marca and modelo
					$vehiculo = explode('#', $sv['advSearch_vehiculo']);

					//Make conditional
					$condtmp = "(id IN (SELECT articulo_id FROM catalogo_articulos_aplicaciones WHERE (marca_id = ".$vehiculo[0].")";

					//Add conditional for model...
					if($vehiculo[1] != '-1') $condtmp .= " AND (modelo_id = ".$vehiculo[1].")";

					//Finish sql conditional
					$condtmp .="))";

					//Add conditional
					$condicionales[] = $condtmp; 
				}

				//Filtro Rubro
				if($sv['advSearch_rubro'] != '-1'){

		    		//CARGAMOS RUBROS
					$stmt = $GLOBALS['conf']['pdo']->prepare("SELECT id, nivel, padre FROM catalogo_rubros WHERE (id <> 1) ORDER BY orden");
					$stmt->execute();
					$rubros = $stmt->fetchAll(PDO::FETCH_ASSOC);
					$hijos = array_merge( (array)$sv['advSearch_rubro'] , $GLOBALS['toolbox']->traeRubrosHijos($rubros, $sv['advSearch_rubro']) );

					$condicionales[] = " ( id_rubro IN (".implode(',', $hijos).") ) ";
				}

				//Filtro Fabricante
				if($sv['advSearch_fabricante'] != '-1')
					$condicionales[] = " ( id_marca = ".$sv['advSearch_fabricante'].") ";

				//Filtro Dimensiones
				$GLOBALS['resultado']->_result['varsss1'] = $sv['advSearch_dimensiones'];
				if( ($sv['advSearch_dimensiones'] != '-1') && (is_array($sv['advSearch_dimensiones'])) ){
					$GLOBALS['resultado']->_result['varsss'] = $sv['advSearch_dimensiones'];
					foreach($sv['advSearch_dimensiones'] as $k=>$d){
						//Si tiene seteado algun valor...
						if($d['value'] != '-1'){
							$condicionales[] = "( extra_".$d['id']." = '".$d['value']."' )";
						}
					}
				}

				//Filtro del quicksearch
				if( ($sv['quicksearch_ultimos'] == '1') && is_array($sv['quicksearch_ids']) && count($sv['quicksearch_ids'])){
					$condicionales[] = "(id IN(".implode(',', $sv['quicksearch_ids'])."))";
				}


				//Condicional para listar incorporaciones
				//Mustra incorporaciones en los últimos 30 dias
				if(!count($condicionales)) 
					$condicionales[] = "(fecha_incorporacion >= DATE_SUB(CURDATE(), INTERVAL 1 MONTH))";



				//Para Administradores y desarrollador mostramos todo
				if(($rol != 1) && ($rol != -1)) $condicionales[] = "( t.habilitado = 1 )";


				//SQL
				$sql = "SELECT DISTINCT 	id_rubro,
											(SELECT padre FROM catalogo_rubros WHERE id = t.id_rubro) as padre 
						FROM 				catalogo_articulos t WHERE ".implode(' AND ', $condicionales);
			

				//Get the products from DB
				$stmt = $GLOBALS['conf']['pdo']->prepare($sql);
				$stmt->execute();
				$articulos = $stmt->fetchAll(PDO::FETCH_ASSOC);

				$mismo_rubro_padre = array();
				$mismo_rubro_general = array();
				foreach ($articulos as $k=>$v){
					$mismo_rubro_padre[$v['padre']] = $v['padre'];
					$mismo_rubro_general[$v['id_rubro']] = $v['id_rubro'];
				}


				$GLOBALS['parametros']['variable'] = 'advSearch_rubro';

				if(count($mismo_rubro_padre) == 1){
					$GLOBALS['parametros']['variable'] = 'advSearch_rubro';
					$GLOBALS['parametros']['value'] = array_keys($mismo_rubro_padre)[0];
				}

				if(count($mismo_rubro_general) == 1){
					$GLOBALS['parametros']['variable'] = 'advSearch_rubro';
					$GLOBALS['parametros']['value'] = array_keys($mismo_rubro_general)[0];
				}


				//Si solo tenemos un rubro...
				if( (count($mismo_rubro_padre) == 1) || count($mismo_rubro_general)){
					//$GLOBALS['session']->setData('advSearch_dimensiones','1');
					$GLOBALS['session']->setData('advSearch_dim_en_columnas','1');
					$GLOBALS['session']->setData('advSearch_busqueda_dim','1');
				}
			}
		}

    	//Si quitamos el filtro del RUBRO
    	if( ($GLOBALS['parametros']['variable'] == 'advSearch_rubro') && ($GLOBALS['parametros']['value'] != '-1')){
    		
    		//CARGAMOS RUBROS
			$stmt = $GLOBALS['conf']['pdo']->prepare("SELECT id, nivel, padre FROM catalogo_rubros WHERE (id <> 1) ORDER BY orden");
			$stmt->execute();
			$rubros = $stmt->fetchAll(PDO::FETCH_ASSOC);


			$hijos = array_merge( (array)$GLOBALS['parametros']['value'] , $GLOBALS['toolbox']->traeRubrosHijos($rubros, $GLOBALS['parametros']['value']) );

			//El rubro sigue siendo el mismo que antes?
			$mismo_rubro = false;
			if($rubros_anteriores == implode(',', $hijos)) 
				$mismo_rubro = true;


			$GLOBALS['session']->setData('advSearch_rubro', implode(',', $hijos));

			//Search for Progenitores
			$progenitores = $GLOBALS['toolbox']->traeRubrosPadres($rubros, $GLOBALS['parametros']['value']);
			$contProgenitores = count($progenitores);

			//Get Inherited vars...
			$sql = "SELECT id, orden, rubro_id, nombre, cede_decendencia FROM catalogo_rubros_datos WHERE (rubro_id = ". $GLOBALS['parametros']['value'].")";
			
			//Have parents ??
			if($contProgenitores)
				$sql .= " OR ( (rubro_id IN (".implode(',', $progenitores).")) && (cede_decendencia = 'Y'))"; 
			
			//Order...
			$sql .= " ORDER BY orden";

			//Make the query
			$stmt = $GLOBALS['conf']['pdo']->prepare($sql);
			$stmt->execute();
			$vars = $stmt->fetchAll(PDO::FETCH_ASSOC);

			//Add selected rubro to progenitors array
			array_push($progenitores, $GLOBALS['parametros']['value']);

			//Generate session Array Vars
			$sessVars = array();

			//Create vars array
			foreach($progenitores as $k=>$p)
				foreach($vars as $k1=>$v)
					if($v['rubro_id'] == $p){
						//Return results
						$temp = array();
						$temp['id'] = $v['id'];
						$temp['label'] = $v['nombre'];
						$temp['nombre'] = 'extra_'.$v['id'];
						$GLOBALS['resultado']->_result['items'][] = $temp;

						//Session var array
						$temp1 = array();
						$temp1['id'] = $v['id'];
						$temp1['label'] = $v['nombre'];
						$temp1['orden'] = $v['orden'];
						$temp1['value'] = '-1';
						$temp1['opciones'] = array();
						$sessVars[] = $temp1;
					}


			//Buscamos las opciones
			$opc = array();
			if(count($sessVars)){
				$sql = "SELECT id, nombre, id_datos FROM catalogo_rubros_datos_opciones WHERE id_datos IN (";
				foreach($sessVars as $k=>$v){ if($k>0) $sql .= ","; $sql .= $v['id']; }
				$sql .= ")";
				//Make the query
				$stmt = $GLOBALS['conf']['pdo']->prepare($sql);
				$stmt->execute();
				$opc = $stmt->fetchAll(PDO::FETCH_ASSOC);
			}

			//Agregamos opciones
			foreach($sessVars as $k=>$v)
				foreach($opc as $k1=>$o)
					if($v['id'] == $o['id_datos']){
						$sessVars[$k]['opciones'][] = $o;
					}

			//Si el rubro es distinto al anterior
			//cargamos las nuevas dimensiones
			$GLOBALS['resultado']->_result['mismo_rubro'] = $mismo_rubro;
			if(!$mismo_rubro) $GLOBALS['session']->setData('advSearch_dimensiones',$sessVars);

			//Total vars..
			$GLOBALS['resultado']->_result['vars'] = count($vars);
    	}


    	//Partimos el nombre de la variable
    	$variable = explode('_', $GLOBALS['parametros']['variable']);

    	//Si es una dimension...
    	if( (isset($variable[1]) && ($variable[1] == 'dimension')) &&  (isset($variable[2]) && ctype_digit($variable[2])) ){
    		
    		$dimensions = $GLOBALS['session']->getData('advSearch_dimensiones');

    		//Agregamos el valor al arreglo de dimensiones
    		if(is_array($dimensions))
    			foreach($dimensions as $k=>$d)
    				if($d['id'] == $variable[2])
    					$dimensions[$k]['value'] = $GLOBALS['parametros']['value'];

    		//Save data to session
    		$GLOBALS['session']->setData('advSearch_dimensiones', $dimensions);
    	}

    	//Mostramos vehiculos en filas individuales
    	if( ($GLOBALS['parametros']['variable'] == 'advSearch_vehiculo_filas_individuales')){
    		$val = '-1';
    		if($GLOBALS['parametros']['value'] == '1') $val = '1';
    		$GLOBALS['session']->setData('advSearch_vehiculo_filas_individuales', $val);
    	}

    	//Si buscamos por dimensiones
    	if( ($GLOBALS['parametros']['variable'] == 'advSearch_busqueda_dim')){
    		$val = '-1';
    		if($GLOBALS['parametros']['value'] == '1') $val = '1';
    		$GLOBALS['session']->setData('advSearch_busqueda_dim', $val);
    	}

    	// Mostramos las dimensiones en columnas
    	if( ($GLOBALS['parametros']['variable'] == 'advSearch_dim_en_columnas')){
    		$val = '-1';
    		if($GLOBALS['parametros']['value'] == '1') $val = '1';
    		$GLOBALS['session']->setData('advSearch_dim_en_columnas', $val);
    	}
	}

	/** 
	* @nombre: Limpiar búsqueda (búsqueda avanzada)
	* @descripcion: Limpiar búsqueda
	*/
	public function advSearch_reset(){

        //Reset all session variables
        foreach($this->session_vars as $k=>$v)
        	$GLOBALS['session']->setData($v,'-1');
	}

	/** 
	* @nombre: Obtiene las variables de la session (búsqueda avanzada)
	* @descripcion: Obtiene las variables de la session
	*/
	public function advSearch_getSessionVars(){ 
		//return;

		//MODO
		$modo = $GLOBALS['session']->getData('modo');



		////////////////////////////////////////////////////////////////////////
		// MODO CONFIGURACION
		////////////////////////////////////////////////////////////////////////
		if($modo == 'configuracion_precios'){
			$rol = $GLOBALS['session']->getData('rol');
			$nombre = $GLOBALS['session']->getData('nombre');
			$usuario = $GLOBALS['session']->getData('usuario');

			//Nombre para mostrar
			$nombre_mostrar = $nombre;

			if($rol == 2){
				$stmt = $GLOBALS['conf']['pdo']->prepare("SELECT razon_social FROM clientes WHERE usuario='".$usuario."'");
				$stmt->execute();
				$cliente = $stmt->fetchAll(PDO::FETCH_ASSOC);
				$nombre_mostrar = $usuario .' - '.$cliente[0]['razon_social'];
			}
			
			$GLOBALS['resultado']->_result['modo'] = $modo;
			$GLOBALS['resultado']->_result['nombre'] = $nombre_mostrar;
			//return;
		}


		////////////////////////////////////////////////////////////////////////
		// MODO CARRO
		////////////////////////////////////////////////////////////////////////
		if($modo == 'carro'){
			$rol = $GLOBALS['session']->getData('rol');
			$nombre = $GLOBALS['session']->getData('nombre');
			$usuario = $GLOBALS['session']->getData('usuario');

			//Nombre para mostrar
			$nombre_mostrar = $nombre;

			if($rol == 2){
				$stmt = $GLOBALS['conf']['pdo']->prepare("SELECT razon_social FROM clientes WHERE usuario='".$usuario."'");
				$stmt->execute();
				$cliente = $stmt->fetchAll(PDO::FETCH_ASSOC);
				$nombre_mostrar = $usuario .' - '.$cliente[0]['razon_social'];
			}
			
			$GLOBALS['resultado']->_result['modo'] = $modo;
			$GLOBALS['resultado']->_result['nombre'] = $nombre_mostrar;
		}

        //Init
        $GLOBALS['resultado']->_result['vars']['fabricante']['id'] = '-1';
        $GLOBALS['resultado']->_result['vars']['vehiculo']['id'] = '-1';
        $GLOBALS['resultado']->_result['vars']['rubro']['id'] = '-1';
        $GLOBALS['resultado']->_result['sess'] = $GLOBALS['session']->getData('advSearch_sufijo');
		
		//modo
		$GLOBALS['resultado']->_result['modo'] = $GLOBALS['session']->getData('modo');

		//Get variables
    	foreach($this->session_vars as $k=>$v) $$v = $GLOBALS['session']->getData($v);


    	//Save SQL to get VARS LABELS
    	$fields = array();

    	//Fabricante
    	if($advSearch_fabricante != '-1')
    		$fields[] = "(SELECT nombre FROM catalogo_marcas WHERE id = ".$advSearch_fabricante.") as fabricante";

    	//Vehiculo
    	if($advSearch_vehiculo != '-1'){
    		$idVehiculo = explode('#',$advSearch_vehiculo);
    		if( ($idVehiculo[0] != '-1') && ($idVehiculo[1] != '-1') )
    			$fields[] = "(CONCAT((SELECT nombre FROM catalogo_autos_marca WHERE id = ".$idVehiculo[0]."), ' ', (SELECT nombre FROM catalogo_autos_modelo WHERE id = ".$idVehiculo[1]."))) as vehiculo";
    		else 
    			$fields[] = "(SELECT nombre FROM catalogo_autos_marca WHERE id = ".$idVehiculo[0].") as vehiculo";

    		//$fields[] = "(CONCAT( (SELECT nombre FROM catalogo_autos_marca WHERE id = ".$idVehiculo[0]."), ' ', (IF(".$idVehiculo[1]."<>-1, (SELECT nombre FROM catalogo_autos_modelo WHERE id = ".$idVehiculo[1]."), '')))) as vehiculo";
    	}

    	//Rubro
    	if($advSearch_rubro != '-1'){

    		$rubros = explode(',', $advSearch_rubro);

    		if(is_array($rubros)) $rubro_id = $rubros[0];
    		else $rubro_id = $advSearch_rubro;

	    	//CARGAMOS RUBROS
			$stmt = $GLOBALS['conf']['pdo']->prepare("SELECT id, nivel, padre, nombre FROM catalogo_rubros ORDER BY orden");
			$stmt->execute();
			$rubros = $stmt->fetchAll(PDO::FETCH_ASSOC);
			$rubros = $GLOBALS['toolbox']->orderRubros($rubros);

			//Obtnemos el nombre Completo
			foreach($rubros as $k=>$r)
				if($rubro_id == $r['id'])
					$rubro = $r['nombre'];
		
		}

		//Get Labels
		if(count($fields)){
			$sql = "SELECT ".implode(', ', $fields);
			$stmt = $GLOBALS['conf']['pdo']->prepare($sql);
			$stmt->execute();
			$labels = $stmt->fetch(PDO::FETCH_ASSOC);
		}

        //Sufijo
        $GLOBALS['resultado']->_result['vars']['quicksearch_term']['id'] 	= $quicksearch_term;
        $GLOBALS['resultado']->_result['vars']['quicksearch_term']['nom'] 	= $quicksearch_term;

        //Lo mostramos solo una vez
        //if($quicksearch_term != -1) $GLOBALS['session']->setData('quicksearch_term',-1);

        //Prefijo
        $GLOBALS['resultado']->_result['vars']['prefijo']['id'] 	= $advSearch_prefijo;
        $GLOBALS['resultado']->_result['vars']['prefijo']['nom'] 	= $advSearch_prefijo;

        //Codigo
        $GLOBALS['resultado']->_result['vars']['codigo']['id'] 	= $advSearch_codigo;
        $GLOBALS['resultado']->_result['vars']['codigo']['nom'] 	= $advSearch_codigo;

        //Sufijo
        $GLOBALS['resultado']->_result['vars']['sufijo']['id'] 	= $advSearch_sufijo;
        $GLOBALS['resultado']->_result['vars']['sufijo']['nom'] 	= $advSearch_sufijo;



        //Fabricante
        if(isset($labels['fabricante'])){
       		$GLOBALS['resultado']->_result['vars']['fabricante']['id'] 	= $advSearch_fabricante;
        	$GLOBALS['resultado']->_result['vars']['fabricante']['nom'] 	= $labels['fabricante'];
        }

        //Vehiculo
        if(isset($labels['vehiculo'])){
       		$GLOBALS['resultado']->_result['vars']['vehiculo']['id'] 	= $advSearch_vehiculo;
        	$GLOBALS['resultado']->_result['vars']['vehiculo']['nom'] 	= $labels['vehiculo'];
        }

        //Rubro
        if(isset($rubro)){
       		$GLOBALS['resultado']->_result['vars']['rubro']['id'] 		= $rubro_id;
        	$GLOBALS['resultado']->_result['vars']['rubro']['nom'] 	= $rubro;
        }

        //Dimensiones
        if(is_array($advSearch_dimensiones) && count($advSearch_dimensiones)){
        	//Foreach dimensiones
        	foreach($advSearch_dimensiones as $k=>$d){
        		$dTmp['id'] = $d['id'];
        		$dTmp['label'] = $d['label'];
        		$dTmp['value']['id'] = $d['value'];
        		$dTmp['value']['nom'] = $d['value'];
        		//Si la dimension tiene opciones...
        		if(count($d['opciones']))
        			//Buscamos entre todas las opciones...
        			foreach($d['opciones'] as $k1=>$o)
        				//La que tenga el mismo id que el valor...
        				if($d['value'] == $o['id'])
        					//Y guardamos en su label el nombre.
        					$dTmp['value']['nom'] = $o['nombre'];

        		//Guardamos en el arreglo de dimensiones la dimension en cuestion
        		$GLOBALS['resultado']->_result['vars']['dimensiones'][] = $dTmp;
        	}
        }

        //Busqueda Dimensional
        $GLOBALS['resultado']->_result['vars']['busqueda_dim'] = $advSearch_busqueda_dim;

        //Dimensiones en columnas
        $GLOBALS['resultado']->_result['vars']['dim_en_columnas'] = $advSearch_dim_en_columnas;

        //Dimensiones en columnas
        $GLOBALS['resultado']->_result['vars']['vehiculo_filas_individuales'] = $advSearch_vehiculo_filas_individuales;
	}

	/** 
	* @nombre: Busqueda rápida (Lista resultados)
	* @descripcion: Búsqueda rápida
	*/
	public function quick_search(){

        //Separamos las palabras clave en un arreglo
        $palabras = trim($GLOBALS['parametros']['q']);

        //Quitamos las letras duplicadas de las palabras
        $palabras = preg_replace('/([^\d])\1+/', '$1', $palabras);

        //Quitamos todo lo que no sea letra o numero
        $palabras = preg_replace("/[^ \w-_]+/", "", $palabras);

        //Solo dejamos un espacio entre las palabras
        $palabras = preg_replace( "/\s+/", " ", $palabras );

    	//Aca guardamos los condicionales
    	//para el query de busqueda
    	$condicionales = array();

        //Detectamos un codigo con guiones completo
        preg_match("/(\d+)-([0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz]+)-(\d+)/", trim($palabras), $es_codigo);
    	if( count($es_codigo) ){
    		$condicionales[] = "(id IN (SELECT id FROM catalogo_articulos WHERE (prefijo='".$es_codigo[1]."') AND (codigo='".$es_codigo[2]."') AND (sufijo='".$es_codigo[3]."')))";
    		$palabras= '';
    	}

        //Detectamos un codigo solo con codigo de fabricante
        preg_match("/(\d+)-([0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz]+)/", trim($palabras), $es_codigo);
    	if(count($es_codigo)){
    		$condicionales[] = "(id IN (SELECT id FROM catalogo_articulos WHERE (prefijo='".$es_codigo[1]."') AND (codigo='".$es_codigo[2]."') ))";
    		$palabras= '';
    	}


        //Quitamos los 'de' 'para' 'con'
        $palabras = str_replace(' de ', ' ', $palabras);
        $palabras = str_replace(' a ', ' ', $palabras);
        $palabras = str_replace(' para ', ' ', $palabras);
        $palabras = str_replace(' con ', ' ', $palabras);
        $palabras = str_replace('-', '', $palabras);
        $palabras = str_replace('_', '', $palabras);
        $palabras = str_replace(' r ', ' r', $palabras);
        $palabras = str_replace(' s ', ' s', $palabras);
        $palabras = str_replace(' c ', ' c', $palabras);
        $palabras = str_replace(' d ', ' d', $palabras);
        $palabras = str_replace(' f ', ' f', $palabras);

        $usuario = $GLOBALS['session']->getData('usuario');

        //Quitamos los espacios traceros y delanteros
        $palabras = $GLOBALS['toolbox']->corrector($palabras);

        //LOG de busqueda
        $GLOBALS['conf']['pdo']->query("INSERT INTO log_busquedas (query, usuario) VALUES ('".$palabras."', '".$usuario."')");

        //Adivinamos los fabricantes
        $detectar_fabricantes = $GLOBALS['toolbox']->adivina_fabricante($palabras);

        //Guardamos los fabricantes detectados
        $fabricantes = $detectar_fabricantes['fabricantes'];

        //Guardamos la nueva Query sin fabricantes
        $palabras = $detectar_fabricantes['query'];

        //Adivinamos los rubros
        $detectar_rubros = $GLOBALS['toolbox']->adivina_rubro($palabras);

        //Guardamos los fabricantes detectados
        $rubros = $detectar_rubros['rubros'];

        //Guardamos la nueva Query sin fabricantes
        $palabras = $detectar_rubros['query'];

        //Mostramos las palabras en debug
        //$GLOBALS['resultado']->_debug['palabras'] = $palabras;
        //$GLOBALS['resultado']->_debug['rubros'] = $rubros;
        //$GLOBALS['resultado']->_debug['fabricantes'] = $fabricantes;

        //Desglosamos las palabras que quedan
        //para hacer la busqueda por descripcion y vehiculo
        $keyword_tokens = explode(' ', trim($palabras));
        foreach ($keyword_tokens as $k=>$v)
        	if( strlen(trim($v)) == 0)
        		unset($keyword_tokens[$k]);

    	//Condicional de busqueda cuando ya sabemos los fabricantes
    	if(count($fabricantes))
    		$condicionales[] = "(id IN (SELECT id FROM catalogo_articulos WHERE (id_marca=".implode(') OR (id_marca=', $fabricantes). ")))";
  
  		//Agrupamos todos los rubros por score
    	$rubs = array();
    	$score_temp = -1;
    	$indice = 0;
  		foreach ($rubros as $k=>$r){

  			//Si este rubro tiene un score
  			//mas bajo que el anterior lo
  			//guardamos en el siguiente indice
  			if($r[1] < $score_temp)
  				$indice++;

  			//Guardamos el score anterior
  			$score_temp = $r[1];

  			//Agregamos al indice de rubros
  			$rubs[$indice][] = $r[0];
  		}


    	//Guardamos los IDs de los fabricantes
    	$rubros_ids = array();
    	foreach ($rubros as $x => $rubro){
    		$rubros_ids[] = $rubro[0];
    	}
    	
    	//print_r($rubros_ids);

    	//Si tenemos silo una palabra
    	if( (count($keyword_tokens) == 1) && !(preg_match('/[^0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz]/', $keyword_tokens[0])) && ($keyword_tokens[0] != '') && (count($rubros_ids) == 0) ){
    	//if( (count($keyword_tokens) == 1) && !(preg_match('/[^0123456789KITLkitlHh]/', $keyword_tokens[0])) && ($keyword_tokens[0] != '') ){
    		$condicionales[] = "(id IN (SELECT id FROM catalogo_articulos WHERE (codigo='".$keyword_tokens[0]."')))";
    		$keyword_tokens= array();
    	}

    	//Condicional de busqueda cuando ya sabemos los rubros
    	//if(count($rubros_ids))
    	//	$condicionales[] = "(id IN (SELECT id FROM catalogo_articulos WHERE (id_rubro=".implode(') OR (id_rubro=', $rubros_ids). ")))";

        //Calculate paging
        $page =  (intval($GLOBALS['parametros']['p'])-1) * 40;

		//Recorremos el arreglo de rubros
		$i=0;
		$sql = "SELECT * FROM (";
		do {

			//Sql inicial
			$sql .= "(SELECT ".$i." as grupo, id, descripcion, TRUNCATE((SELECT precio FROM catalogo_articulos WHERE id = b.id),2) as precio, (CONCAT((SELECT prefijo FROM catalogo_articulos WHERE id = b.id),'-',(SELECT codigo FROM catalogo_articulos WHERE id = b.id),'-',(SELECT sufijo FROM catalogo_articulos WHERE id = b.id))) as codigo_completo, ";

			//Si ya detectamos el rubro y ademas
			//nos queda alguna palabra mas donde
			//por la cualqueramos filtrar
			if(count($keyword_tokens)){
				$sql .= "(";

				//Agregamos el contador de score de palabras
				foreach($keyword_tokens as $key=>$palabra){
				    if($key > 0) $sql .= "+";
				    //$sql .= "IF(LOCATE('".$palabra."',LOWER(busqueda))>0,1,0)";
				    $sql .= "IF(LOCATE(' ".$palabra." ',LOWER(busqueda))>0,1,0)";
				    //$sql .= "+IF(LOCATE(' ".$palabra."',LOWER(busqueda))>0,1,0)";
				    //$sql .= "+IF(LOCATE('".$palabra." ',LOWER(busqueda))>0,1,0)";
				}
			
				//Cerramos...
				$sql .= ")";
			}else{
				$sql .= "1";
			}

			$sql .= " AS score FROM catalogo_articulos_busqueda b";

			$condicionales_tmp = $condicionales;

			//Si tenemos algun rubro
			//detectado lo ponemos
			if(isset($rubs[$i]))
				$condicionales_tmp[] = "(id IN (SELECT id FROM catalogo_articulos WHERE (id_rubro=".implode(') OR (id_rubro=', $rubs[$i]). ")))";
			

			//Si tenemos condicionales, los agregamos aca
			if (count($condicionales_tmp))
				$sql .= " WHERE " . implode(' AND ', $condicionales_tmp);

			$sql .= " HAVING score>0 ORDER BY score DESC, descripcion ASC, precio ASC)";

			//Sumamos uno el indice
			$i++;

			//Unimos
			if(isset($rubs[$i])) $sql.= " UNION ";

		} while ( $i < count($rubs) );

		$sql .= ") a ORDER BY grupo ASC, score DESC";

		//echo $sql;

		//Guardamos el sql
		$GLOBALS['resultado']->_debug['sql'] = $sql;

		//Buscamos el usuario en la base de datos por EMAIL o #CLIENTE
		$stmt = $GLOBALS['conf']['pdo']->prepare($sql);
		$stmt->execute();

		//$stmt = $GLOBALS['conf']['pdo']->prepare("SELECT * FROM catalogo_articulos_busqueda WHERE id = 1233");
		//$stmt->execute();


		//Guardamos los resultados
		$GLOBALS['resultado']->_result['items'] = $stmt->fetchAll(PDO::FETCH_ASSOC);
		

		//Primeros 20 ids
		$primeros_20 = array();
		foreach ($GLOBALS['resultado']->_result['items'] as $k=>$v){
			$primeros_20[ $v['id'] ] = true;
			if(count($primeros_20) == 20) break;
		}

		//print_r(array_keys($primeros_20));

		$GLOBALS['session']->setData('quicksearch_ids', array_keys($primeros_20));

		//Agregamos ultimos 20 resultados
		if(count($GLOBALS['resultado']->_result['items']))
			array_unshift($GLOBALS['resultado']->_result['items'], array("id"=>rand()*-1, "term"=> strtolower($GLOBALS['parametros']['q'])));

		//Cortamos el pedazo de array que nos interesa
    	$res = array_slice($GLOBALS['resultado']->_result['items'], $page, 40);

		//resultados
        $GLOBALS['resultado']->_result['items'] = $res;
        $GLOBALS['resultado']->_result['total'] = count($GLOBALS['resultado']->_result['items']);

		//imprimimos los resultados...
	}

	/** 
	* @nombre: Búsqueda rápida (Muestra en tabla los resultados)
	* @descripcion: Muestra los resultados en la tabla
	*/
	public function quick_search_commit(){


    	//Cambiamos el modo
    	$GLOBALS['session']->setData('modo', '-1');

    	//Palabras clave
    	$palabra = $GLOBALS['parametros']['q'];

    	//ID Del articulo
    	//Si es negativo: Piden listar los últimos 20 resultados
    	//si es positivo: Estan pidiendo un articulo en especial.
    	$busquedaXarticulo = true;
    	if( (int)$GLOBALS['parametros']['articulo'] < 1 ) $busquedaXarticulo = false;

    	//Buscamos el codigo
    	if($busquedaXarticulo){
	        $stmt = $GLOBALS['conf']['pdo']->prepare("  SELECT  prefijo,codigo,sufijo
	                                                    FROM    catalogo_articulos
	                                                    WHERE   id = ".$GLOBALS['parametros']['articulo']);
	        $stmt->execute();
	        $datos = $stmt->fetch(PDO::FETCH_ASSOC);

	        //Borramos las variables
	        foreach($this->session_vars as $k=>$v) $GLOBALS['session']->setData($v,'-1');

	        //Seteamosel codigo de larticulo especifico
	        $GLOBALS['session']->setData('advSearch_prefijo', $datos['prefijo']);
	        $GLOBALS['session']->setData('advSearch_codigo', $datos['codigo']);
	        $GLOBALS['session']->setData('advSearch_sufijo', $datos['sufijo']);
	    }


	    //Si lo que estamos haciendo es una busqued
	    //listando los ultimos resultados...
	    if(!$busquedaXarticulo){

	        //Borramos las variables
	        $ids =  $GLOBALS['session']->getData('quicksearch_ids');
	        foreach($this->session_vars as $k=>$v) $GLOBALS['session']->setData($v,'-1');
	        //Seteamos el termino
	        $GLOBALS['session']->setData('quicksearch_term', $GLOBALS['parametros']['q']);
	        $GLOBALS['session']->setData('quicksearch_ids', $ids);
	        $GLOBALS['session']->setData('quicksearch_ultimos', '1');
	    }
	}


	/** 
	* @nombre: Reindexa la Busqueda Rapida
	* @descripcion: Reindexa la Busqueda Rapida
	*/
	public function quick_search_reindex(){

		//Quitamos el limite de tiempo
		set_time_limit(0);
		ini_set('memory_limit', '512M');

		//Rubro - descripcion - marca - aplicacion
		$sql = "SELECT * FROM (SELECT articulo_id as id,
					   CONCAT( 
							IF( (SELECT nombre_singular FROM catalogo_rubros WHERE id = (SELECT id_rubro FROM catalogo_articulos WHERE id = articulo_id)) <> '',
								CONCAT(
									(SELECT nombre_singular FROM catalogo_rubros WHERE id = (SELECT id_rubro FROM catalogo_articulos WHERE id = articulo_id)), 
									' '
								),
								''
							),

							IF( (SELECT descripcion FROM catalogo_articulos WHERE id = articulo_id) <> '', 
								(SELECT descripcion FROM catalogo_articulos WHERE id = articulo_id),
								''
							)



							

		 ) as descripcion,

		(IF( 	(SELECT id_marca FROM catalogo_articulos WHERE id = articulo_id) IS NOT NULL, 
				(SELECT nombre FROM catalogo_marcas WHERE id = (SELECT id_marca FROM catalogo_articulos WHERE id = articulo_id)),
				'')) as fabricante,

		(IF( marca_id IS NOT NULL, 
								CONCAT(
									(SELECT nombre FROM catalogo_autos_marca WHERE id = marca_id),
									IF(
										modelo_id IS NOT NULL,
										CONCAT(
											' ',
											(SELECT nombre FROM catalogo_autos_modelo WHERE id = modelo_id)
										),
										''
									) 
								),
							'')) as vehiculo,

		(IF(version_id IS NOT NULL,(SELECT descripcion FROM catalogo_autos_versiones WHERE id = version_id),'')) as version,

		(CONCAT ( '[', IF( anio_inicio = -1 AND anio_final = -1, 'TODOS', CONCAT( IF( anio_inicio = -1, '--', SUBSTRING(anio_inicio,3,2)), ' / ', IF( anio_final = -1, '--', SUBSTRING(anio_final,3,2)))),']')) as anio,

		(SELECT habilitado FROM catalogo_articulos WHERE id = articulo_id) as habilitado

		  FROM catalogo_articulos_aplicaciones) t WHERE t.habilitado = 1

		UNION(

		SELECT id,
			   CONCAT( 
					IF( ((SELECT nombre_singular FROM catalogo_rubros WHERE id = id_rubro) <> '') AND (id_rubro IS NOT NULL),
						CONCAT(
							(SELECT nombre_singular FROM catalogo_rubros WHERE id = id_rubro), 
							' '
						),
						''
					),

					IF( descripcion <> '', 
						CONCAT( descripcion,' '),
						''
					),

					IF( id_marca IS NOT NULL, 
						CONCAT( (SELECT nombre FROM catalogo_marcas WHERE id = id_marca),' '),
						''
					)


		 ) as descripcion,
		'' as fabricante,
		'' as vehiculo,
		'' as version,
		'' as anio,

		habilitado

		FROM catalogo_articulos WHERE id NOT IN (SELECT DISTINCT articulo_id FROM catalogo_articulos_aplicaciones) AND (habilitado = 1)
		)";

		$stmt = $GLOBALS['conf']['pdo']->query("TRUNCATE catalogo_articulos_busqueda");
		$stmt = $GLOBALS['conf']['pdo']->query($sql);
		$resultados = $stmt->fetchAll(PDO::FETCH_ASSOC);

		$search[] = " L/ caja junta homocinética - c/ triceta (c/ chapa) ";
		$replace[] = " L/ caja (c/ chapa)";

		$search[] = " L/ caja junta homocinética - c/ triceta ";
		$replace[] = " L/ caja ";

		$search[] = " L/ caja - l/ rueda junta homocinética - c/ triceta ";
		$replace[] = " ";

		$search[] = " L/ caja junta homocinética - Bendixweis ";
		$replace[] = " L/ caja ";

		$search[] = " L/ rueda junta homocinética - c/ triceta ";
		$replace[] = " L/ rueda ";

		$search[] = " L/ caja - rueda ";
		$replace[] = " ";

		$search[] = " L/ ";
		$replace[] = " l/ ";

		//Recorremos todos los resultados y correjimos abreviaturas de busqueda
		foreach($resultados as $key=>$resultado){
			if((int)$resultado['id'] == 61133) $GLOBALS['resultado']->_result['asds'] = $resultado;
			$resultados[$key]['busqueda'] =  str_replace($search, $replace, $resultado['descripcion'] . ' '. $resultado['vehiculo']);
			$resultados[$key]['descripcion'] = trim($resultados[$key]['descripcion'] . ' ' . $resultados[$key]['fabricante'] . ' ' . $resultados[$key]['vehiculo'] . ' ' . $resultados[$key]['version']);

			if((int)$resultado['id'] == 61133) $GLOBALS['resultado']->_result['asdsaaaaaaaaa'] = $resultados[$key]['busqueda'];
 
			//Acortamos la busqueda a la minima expresion
			$query = $resultados[$key]['busqueda'];

			//Pasamos la cadena entera a minusculas
			$query = strtolower($query);

			//Removemos todas las aclaraciones entre parentesis
			$query = preg_replace("/\([^)]+\)/","",$query);

			//Quitamos abreviatura de Con
	        $query = str_replace('c/', '', $query);
	        $query = str_replace('c /', '', $query);

	        //Quitamos abreviatura de Sin
	        $query = str_replace('s/', '', $query);
	        $query = str_replace('s /', '', $query);

	        //Quitamos abreviatura de para
	        $query = str_replace('p/', '', $query);
	        $query = str_replace('p /', '', $query);


	        //Quitamos los 'de' 'para' 'con'
	        $query = str_replace(' de ', ' ', $query);
	        $query = str_replace(' a ', ' ', $query);
	        $query = str_replace(' para ', ' ', $query);
	        $query = str_replace(' con ', ' ', $query);
	        $query = str_replace('-', '', $query);
	        $query = str_replace('_', '', $query);
	        $query = str_replace('delantera', 'del', $query);
	        $query = str_replace('delantero', 'del', $query);
	        $query = str_replace('trasera', 'tras', $query);
	        $query = str_replace('trasero', 'tras', $query);
	        $query = str_replace('inferior', 'inf', $query);
	        $query = str_replace('superior', 'sup', $query);
	        $query = str_replace('derecha', 'der', $query);
	        $query = str_replace('derecho', 'der', $query);
	        $query = str_replace('izquierda', 'izq', $query);
	        $query = str_replace('izquierdo', 'izq', $query);
	        $query = str_replace('interna', 'int', $query);
	        $query = str_replace('interno', 'int', $query);
	        $query = str_replace('externa', 'ext', $query);
	        $query = str_replace('externo', 'ext', $query);


	        //Reemplazamos letras con acento  a sin acento
	        $query = $GLOBALS['toolbox']->stripAccents($query);

	        //Quitamos todo lo que no sea letra o numero
	        $query = preg_replace("/[^ \w]+/", "", $query);

	        //Solo dejamos un espacio entre las palabras
	        $query = preg_replace( "/\s+/", " ", $query );

	        //Quitamos letras duplicadas
			$query = preg_replace('/([^\d])\1+/', '$1', $query);

	        //Final
	        $resultados[$key]['busqueda'] = ' '.$query.' ';

	        if($resultados[$key]['anio'] != '') $resultados[$key]['descripcion'] .= ' ' . $resultados[$key]['anio'];

			//Quitamos
			unset($resultados[$key]['fabricante'], $resultados[$key]['vehiculo'], $resultados[$key]['version'], $resultados[$key]['anio']);
		}

		//sql inicial
		$sql = "INSERT INTO catalogo_articulos_busqueda (id, descripcion, busqueda) VALUES ";
		foreach($resultados as $key=>$resultado){
			if($resultado['habilitado'] == 1){
				if($key > 0) $sql .= ", ";
				$sql .= "(".$resultado['id'].",'".$resultado['descripcion']."','".$resultado['busqueda']."')";
			}
		}


		$stmt = $GLOBALS['conf']['pdo']->query($sql);

	}


	/** 
	* @nombre: Cambiar modo
	* @descripcion: Cmabia el modo del sistema
	*/
	public function cambiar_modo(){


    	//Palabras clave
    	$modo = $GLOBALS['parametros']['modo'];

     	if($modo == 'mensajes')
    		$GLOBALS['session']->setData('modo', 'mensajes');

    	if($modo == 'carro')
    		$GLOBALS['session']->setData('modo', 'carro');

     	if($modo == 'busqueda')
    		$GLOBALS['session']->setData('modo', '-1');

    	if($modo == 'listas')
    		$GLOBALS['session']->setData('modo', 'listas');

     	if($modo == 'configuracion_precios')
    		$GLOBALS['session']->setData('modo', 'configuracion_precios');

     	if($modo == 'configuracion_cuenta')
    		$GLOBALS['session']->setData('modo', 'configuracion_cuenta');

    	$GLOBALS['resultado']->_result['total'] = $GLOBALS['session']->getData('modo');
    	$GLOBALS['resultado']->_result['modo'] = $modo;

	}







	//////////////////////////////////////////////////////////////////////////
	//// FUNCIONES EXCLUSIVAS PARA LA APP MOBILE 
	//////////////////////////////////////////////////////////////////////////






	/** 
	* @nombre: Busqueda APP
	* @descripcion: Busqueda APP
	*/
	public function app_search(){

		//Usuario
		$usuario = $GLOBALS['session']->getData('usuario');

        //Obtenemos el query string y lo limpiamos para poder usarlo
        $query = $GLOBALS['toolbox']->corrector_busqueda($GLOBALS['parametros']['q']);
        $palabras = $query['query'];

        //Codidigo Detectado!
        if($query['es_codigo']){
        	$condicional = "(id IN (SELECT id FROM catalogo_articulos WHERE (prefijo='".$query['codigo'][0]."') AND (codigo='".$query['codigo'][1]."')";
        	if(isset($query['codigo'][2])) $condicional .= " AND (sufijo='".$query['codigo'][2]."')";
        	$condicional .= "))";

        	$condicionales[] = $condicional;
        	$palabras = '';
        }

        //Adivinamos los fabricantes
        $detectar_fabricantes = $GLOBALS['toolbox']->adivina_fabricante($palabras);

        //Guardamos los fabricantes detectados
        $fabricantes = $detectar_fabricantes['fabricantes'];

        //Guardamos la nueva Query sin fabricantes
        $palabras = $detectar_fabricantes['query'];

        //Adivinamos los rubros
        $detectar_rubros = $GLOBALS['toolbox']->adivina_rubro($palabras);

        //Guardamos los rubros detectados
        $rubros = $detectar_rubros['rubros'];

        //Guardamos la nueva Query sin fabricantes
        $palabras = $detectar_rubros['query'];

        //Condicionales
        $condicionales = Array();

        //Desglosamos las palabras que quedan
        //para hacer la busqueda por descripcion y vehiculo
        $keyword_tokens = explode(' ', trim($palabras));
        foreach ($keyword_tokens as $k=>$v)
        	if( strlen(trim($v)) == 0)
        		unset($keyword_tokens[$k]);

    	//Condicional de busqueda cuando ya sabemos los fabricantes
    	if(count($fabricantes))
    		$condicionales[] = "(id IN (SELECT id FROM catalogo_articulos WHERE (id_marca=".implode(') OR (id_marca=', $fabricantes). ")))";
  
  		//Agrupamos todos los rubros por score
    	$rubs = array();
    	$score_temp = -1;
    	$indice = 0;
  		foreach ($rubros as $k=>$r){

  			//Si este rubro tiene un score
  			//mas bajo que el anterior lo
  			//guardamos en el siguiente indice
  			if($r[1] < $score_temp)
  				$indice++;

  			//Guardamos el score anterior
  			$score_temp = $r[1];

  			//Agregamos al indice de rubros
  			$rubs[$indice][] = $r[0];
  		}


    	//Guardamos los IDs de los fabricantes
    	$rubros_ids = array();
    	foreach ($rubros as $x => $rubro) $rubros_ids[] = $rubro[0];
    	

    	//Si tenemos solo una palabra
    	if( (count($keyword_tokens) == 1) && !(preg_match('/[^0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz]/', $keyword_tokens[0])) && ($keyword_tokens[0] != '') && (count($rubros_ids) == 0) ){

    		$condicionales[] = "(id IN (SELECT id FROM catalogo_articulos WHERE (codigo='".$keyword_tokens[0]."')))";
    		$keyword_tokens= array();
    	}



		$sql = "SELECT 	*,
						CONCAT(prefijo,'-',codigo,'-',sufijo) as codigo,
						IF(id_marca IS NULL,'(Sin Fabricante)',(SELECT nombre FROM catalogo_marcas WHERE id = id_marca)) as marca,
						(SELECT GET_RUBROS_PADRES(id_rubro)) as rubros, 
						(SELECT GET_APLIC_BY_ART(id)) as aplicaciones,
						(SELECT GET_DIMENSIONS(id, id_rubro)) as dimensiones,
						(SELECT GET_EQUIV_BY_ART(id)) as equivalencias,
						(SELECT GET_IMGS_BY_ART(id)) as imgs
						
				FROM 	catalogo_articulos 
				
				WHERE 	id IN (SELECT id FROM (";

		$i=0;
		do {

			//Sql inicial
			$sql .= "(SELECT ".$i." as grupo, id, ";

			if(count($keyword_tokens)){
				$sql .= "(";

				//Agregamos el contador de score de palabras
				foreach($keyword_tokens as $key=>$palabra){
				    if($key > 0) $sql .= "+";
				    $sql .= "IF(LOCATE(' ".$palabra." ',LOWER(busqueda))>0,1,0)";
				}
			
				//Cerramos...
				$sql .= ")";
			}else{
				$sql .= "1";
			}

			$sql .= " AS score FROM catalogo_articulos_busqueda b";

			$condicionales_tmp = $condicionales;

			//Si tenemos algun rubro
			//detectado lo ponemos
			if(isset($rubs[$i]))
				$condicionales_tmp[] = "(id IN (SELECT id FROM catalogo_articulos WHERE (id_rubro=".implode(') OR (id_rubro=', $rubs[$i]). ")))";
			

			//Si tenemos condicionales, los agregamos aca
			if (count($condicionales_tmp))
				$sql .= " WHERE " . implode(' AND ', $condicionales_tmp);

			$sql .= " HAVING score>0 ORDER BY score DESC)";

			//Sumamos uno el indice
			$i++;

			//Unimos
			if(isset($rubs[$i])) $sql.= " UNION ";

		} while ( $i < count($rubs) );

		$sql .= ") a ORDER BY grupo ASC, score DESC) ";
		$sql .= " AND habilitado = 1";

		//SQL Query
		$stmt = $GLOBALS['conf']['pdo']->prepare($sql);
		$stmt->execute();

		//Items
		$items = $stmt->fetchAll(PDO::FETCH_ASSOC);

		$columnas = [	"codigo", "id", "descripcion", "precio", "marca", "rubros", 
						"aplicaciones", "dimensiones", "equivalencias", "imgs"];

		//Reestructurar Applicaciones
		foreach ($items as $k => $v){
			$items[$k] = $GLOBALS['toolbox']->reestructurar_dimensiones($items[$k]);
			$items[$k]['aplicaciones'] = $GLOBALS['toolbox']->reestructurar_aplicaciones($v['aplicaciones']);
			$items[$k]['imgs'] = $GLOBALS['toolbox']->reestructurar_imgs($v['imgs']);
			$items[$k]['rubros'] = json_decode($v['rubros']);
			$items[$k]['equivalencias'] = json_decode($v['equivalencias']);

			//Remove unnesesary columns
			foreach($items[$k] as $x=>$f) if(!in_array($x, $columnas)) unset($items[$k][$x]);
		}

		//resultados
		$GLOBALS['resultado']->_result['items'] = $items;
	}
}
?>