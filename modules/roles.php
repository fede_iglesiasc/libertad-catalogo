<?php
/**
 * @nombre: Roles
 * @descripcion: Lista los Roles.
 */
class roles extends module{


    /*
	 * Constructor
	 */
    public function __construct(){
    }

    /**
     * Validaciones previas
     */
    public function __pre($accion){

        //Si existe el parametro rol...
        if(isset($GLOBALS['parametros']['rol'])){
            $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT COUNT(*) FROM roles WHERE id = ".$GLOBALS['parametros']['rol']);
            $stmt->execute();
            $datos = $stmt->fetch(PDO::FETCH_ASSOC);

            //Si no existe el rol
            if(!$stmt->rowCount() && in_array($accion, array('eliminar','editar','info','permisos_listar','permisos_editar'))){
                $GLOBALS['resultado']->setError("El Rol no existe.");
                return;
            }

            //Si existe el rol y estamos agregando
            if(in_array($accion, array('agregar')) && $stmt->rowCount()){
                $GLOBALS['resultado']->setError("El Rol que esta intentando agregar ya existe.");
                return;
            }
        }

        //llamamos a la accion
        return call_user_func_array(array($this, $accion), array());
    }

    /**
     * @nombre: Información de Rol
     * @descripcion: Pasamos 'id' del Rol y nos devuelve su Nombre.
     */
    public function info(){

        //Obtenemos los usuarios
        $stmt = $GLOBALS['conf']['pdo']->prepare("  SELECT *
                                                    FROM roles
                                                    WHERE id = ".$GLOBALS['parametros']['rol']);
        $stmt->execute();
        $datos = $stmt->fetch(PDO::FETCH_ASSOC);

        //Guardamos los datos
        $GLOBALS['resultado']->_result = $datos;
    }

    /**
     * @nombre: Lista Roles (completa)
     * @descripcion: Listado de Roles completo.
     */
    public function listar(){

        //SQL
        $stmt = $GLOBALS['conf']['pdo']->prepare("  SELECT  *
                                                    FROM    roles
                                                    WHERE   id >= 0 
                                                    ORDER BY nombre ASC");
        $stmt->execute();
        $roles =  $stmt->fetchAll(PDO::FETCH_ASSOC);


        // 'Cliente' en 3º lugar
        foreach($GLOBALS['resultado']->_result as $k=>$v)
            if($v['nombre'] == 'Cliente'){
                unset($GLOBALS['resultado']->_result[$k]);
                array_unshift($GLOBALS['resultado']->_result, $v);
                break;
            }

        // 'Administrador' en 2º lugar
        foreach($GLOBALS['resultado']->_result as $k=>$v)
            if($v['nombre'] == 'Administrador'){
                unset($GLOBALS['resultado']->_result[$k]);
                array_unshift($GLOBALS['resultado']->_result, $v);
                break;
            }

        // 'Anónimo' en 1º lugar
        foreach($GLOBALS['resultado']->_result as $k=>$v)
            if($v['nombre'] == 'Anónimo'){
                unset($GLOBALS['resultado']->_result[$k]);
                array_unshift($GLOBALS['resultado']->_result, $v);
                break;
            }


        //Asignamos resultados
        $GLOBALS['resultado']->_result = $roles;
    }

    /**
     * @nombre: Agregar un Rol
     * @descripcion: Agrega un Rol, pasándole el nombre de Rol.
     */
    public function agregar(){

        //DB query
        $stmt = $GLOBALS['conf']['pdo']->prepare("INSERT INTO roles(nombre) VALUES ('".$GLOBALS['parametros']['nombre']."')");
        $items = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $stmt->execute();
    }

    /**
     * @nombre: Editar un Rol
     * @descripcion: Edita un Rol, pasándole el nombre de Rol y su id.
     */
    public function editar(){

        //DB query
        $stmt = $GLOBALS['conf']['pdo']->prepare("UPDATE roles SET nombre='".$GLOBALS['parametros']['nombre']."' WHERE id = ".$GLOBALS['parametros']['rol']);
        $items = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $stmt->execute();
    }

    /**
     * @nombre: Eliminar un Rol
     * @descripcion: Elimina un Rol, pasándole el id.
     */
    public function eliminar(){

        //DB query
        $stmt = $GLOBALS['conf']['pdo']->prepare("DELETE FROM roles WHERE id = ".$GLOBALS['parametros']['rol']);
        $stmt->execute();
    }

    /**
     * @nombre: Lista Roles (select)
     * @descripcion: Listado de Roles para popular el Select2.<br/>No muestran los Roles 'Anonimo' ni 'Cliente'.
     */
    public function roles_lista_select(){

        //Separamos las palabras clave en un arreglo
        $keyword_tokens = explode(' ', $GLOBALS['parametros']['q']);

        //Calculate paging
        $page =  (intval($GLOBALS['parametros']['p'])-1) * 40;

        //SQL
        $stmt = $GLOBALS['conf']['pdo']->prepare("  SELECT
                                                        id,
                                                        nombre
                                                    FROM roles
                                                    WHERE (id <> 2) AND (id <> 0) AND (id <> -1)
                                                    ORDER BY nombre ASC");
        $stmt->execute();
        $items = $stmt->fetchAll(PDO::FETCH_ASSOC);


        //Si estamos haciendo una busqueda..
        if($keyword_tokens[0] != '') 
        	$items = $GLOBALS['toolbox']->matchingArray($items,'nombre',$keyword_tokens);

        //Calculamos totales
        $GLOBALS['resultado']->_result['total'] = count($items);

        //Devolvemos items y total
        $GLOBALS['resultado']->_result['items'] = array_slice($items, $page, 40);
    }

    /**
     * @nombre: Listar Permisos
     * @descripcion: Lista los permisos según Rol
     */
    public function permisos_listar(){

        //Obtenemos permisos
        $stmt = $GLOBALS['conf']['pdo']->prepare("  SELECT  id,
                                                            grupo,
                                                            accion,
                                                            (SELECT COUNT(*) FROM permisos_roles WHERE (funcion_id = id) AND (rol_id = ".$GLOBALS['parametros']['rol'].")) as acceso
                                                    FROM funciones
                                                    ORDER BY acceso DESC, grupo ASC, accion ASC");
        $stmt->execute();
        $permisos = $stmt->fetchAll(PDO::FETCH_ASSOC);


        //Asignamos resultados
        $GLOBALS['resultado']->_result = $permisos;
    }

    /**
     * @nombre: Editar Permisos
     * @descripcion: Guarda los cambios de los Permisos a la API de los Roles
     */
    public function permisos_editar(){

        // Begin Transaction
        $GLOBALS['conf']['pdo']->beginTransaction();

        try {

            //Borramos todos los permisos anteriores
            $stmt = $GLOBALS['conf']['pdo']->prepare("DELETE FROM permisos_roles WHERE rol_id =" . $GLOBALS['parametros']['rol']);
            $stmt->execute();

            //Si existen permisos que setear...
            if(count($GLOBALS['parametros']['permisos'])){
                //Generamos la Query
                $sql = "INSERT INTO permisos_roles (funcion_id, rol_id) VALUES ";
                foreach($GLOBALS['parametros']['permisos'] as $k=>$v){
                    if($k) $sql .= ', ';
                    $sql .= "(".$v.",".$GLOBALS['parametros']['rol'].")";
                }

                //Comentemos los cambios en la DB
                $stmt = $GLOBALS['conf']['pdo']->prepare($sql);
                $stmt->execute();
            }

            // Commit Transaction
            $GLOBALS['conf']['pdo']->commit();

        } // Si existieron errores
        catch (PDOException $e) {
            // hacemos un Rollback
            $GLOBALS['conf']['pdo']->rollback();

            //Agregamos error 
            $GLOBALS['resultado']->setError($e);
            return;
        }
    }
}
?>