<?php
/**
 * @nombre: Printnode
 * @descripcion: Configurar Printnode
 */
class printnode extends module{

    /*
	 * Constructor
	 */
    public function __construct(){
    }

    /**
     * Validaciones previas
     */
    public function __pre($accion){

        //llamamos a la accion
        return call_user_func_array(array($this, $accion), array());
    }


    /**
     * @nombre: Api Key Info
     * @descripcion: Api Key Info
     */
    public function apikey_info(){
        $stmt = $GLOBALS['conf']['pdo']->query("SELECT api_key FROM impresoras LIMIT 1");
        $apikey =  $stmt->fetchColumn();

        //Asignamos resultados
        $GLOBALS['resultado']->_result['apikey'] = $apikey;
    }

    /**
     * @nombre: Api Key guardar
     * @descripcion: Api Key guardar
     */
    public function apikey_editar(){
        $apikey = $GLOBALS['parametros']['apikey'];
        $stmt = $GLOBALS['conf']['pdo']->query("UPDATE impresoras SET api_key = '".$apikey."'");
    }

    /**
     * @nombre: Api Key Listar Impresoras
     * @descripcion: Api Key Listar Impresoras
     */
    public function impresoras_dropdown(){


        //Creamos el cliente si no existe..
        $GLOBALS['toolbox']->createPrintNodeClient();

        //Pedimos toda la lista de impresoras
        $computers = $GLOBALS['printnode']->viewComputers();
        $printers = $GLOBALS['printnode']->viewPrinters();

        $stmt = $GLOBALS['conf']['pdo']->query("SELECT impresora_1, impresora_2, impresora_3, impresora_4, impresora_5 FROM impresoras LIMIT 1");
        $s =  $stmt->fetch(PDO::FETCH_NUM);

        //Parseamos a int
        foreach($s as $k => $v) $s[$k] = (int)$v;


        $result = [];
        foreach ($computers as $k=>$c){
            $pc_tmp = new StdClass();
            $pc_tmp->name = $c->name;
            $pc_tmp->state = $c->state;
            $pc_tmp->acceptOfflinePrintJobs = $c->acceptOfflinePrintJobs;
            $pc_tmp->children = [];

            //Add printers
            foreach($printers as $x=>$p)
                if($p->computer->id == $c->id){
                    $printer_tmp = new StdClass();
                    $printer_tmp->name = $p->name;
                    $printer_tmp->id = $p->id;
                    $printer_tmp->state = $p->state;
                    $printer_tmp->pc_name = $c->name;
                    $printer_tmp->pc_state = $c->state;
                    $pc_tmp->children[] = $printer_tmp;

                    if (false !== $y = array_search($printer_tmp->id, $s))
                        $s[$y] = $printer_tmp;
                }


            $result[] = $pc_tmp;
        }

        //Asignamos resultados
        $GLOBALS['resultado']->_result['impresoras'] = $result;
        $GLOBALS['resultado']->_result['seleccionadas'] = $s;
    }


    /**
     * @nombre: Edita las impresoras activas
     * @descripcion: Edita las impresoras activas
     */
    public function impresoras_editar(){

        $sql = "UPDATE impresoras SET impresora_1 = " . $GLOBALS['parametros']['impresora_1'];
        if($GLOBALS['parametros']['impresora_2'] != '') $sql .= ", impresora_2 = " . $GLOBALS['parametros']['impresora_2'];
        else $sql .= ", impresora_2 = NULL";
        if($GLOBALS['parametros']['impresora_3'] != '') $sql .= ", impresora_3 = " . $GLOBALS['parametros']['impresora_3'];
        else $sql .= ", impresora_3 = NULL";
        if($GLOBALS['parametros']['impresora_4'] != '') $sql .= ", impresora_4 = " . $GLOBALS['parametros']['impresora_4'];
        else $sql .= ", impresora_4 = NULL";
        if($GLOBALS['parametros']['impresora_5'] != '') $sql .= ", impresora_5 = " . $GLOBALS['parametros']['impresora_5'];
        else $sql .= ", impresora_5 = NULL";

        $stmt = $GLOBALS['conf']['pdo']->query($sql);
    }

}
?>