<?php
/**
 * @nombre: Funciones
 * @descripcion: Lista los Funciones.
 */
class funciones extends module{

    /*
	 * Constructor
	 */
    public function __construct(){
    }

    /**
     * Validaciones previas
     */
    public function __pre($accion){

        //Si existe el parametro funcion...
        if(isset($GLOBALS['parametros']['funcion'])){
            $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT id FROM funciones WHERE id = '".$GLOBALS['parametros']['funcion']."'");
            $stmt->execute();
            $funcion = $stmt->rowCount();
        }

        //Si existe el parametro identificador...
        if(isset($GLOBALS['parametros']['identificador'])){
            $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT id FROM funciones WHERE identificador = '".$GLOBALS['parametros']['identificador']."'");
            $stmt->execute();
            $identificador = $stmt->rowCount();
        }

        //Si existe el parametro identificador...
        if(isset($GLOBALS['parametros']['grupo']) && isset($GLOBALS['parametros']['accion'])){
            $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT id FROM funciones WHERE (grupo = '".$GLOBALS['parametros']['grupo']."') AND (accion = '".$GLOBALS['parametros']['accion']."')");
            $stmt->execute();
            $data = $stmt->fetch(PDO::FETCH_ASSOC);
            $grupo_accion = $stmt->rowCount();

            //Si estamos editando una accion y le ponemos el mismo nombre de Grupo
            //y accion que otra funcion ya existente tiramos error.
            if(in_array($accion, array('editar')) && $grupo_accion && ($data['id'] != $GLOBALS['parametros']['funcion'])){
                $GLOBALS['resultado']->setError("Ya existe una Función ".$GLOBALS['parametros']['grupo']." ".$GLOBALS['parametros']['accion']);
                return;
            }
        }

        //Si ya existe una Función con el mismo identificador
        if( isset($identificador) && $identificador && 
            in_array($accion, array('agregar'))){
            $GLOBALS['resultado']->setError("Ya existe una Función con el identificador ".$GLOBALS['parametros']['identificador']);
            return;
        }

        //Si ya existe el mismo grupo y accion
        if( isset($grupo_accion) && $grupo_accion && 
            in_array($accion, array('agregar'))){
            $GLOBALS['resultado']->setError("Ya existe una Función ".$GLOBALS['parametros']['grupo']." ".$GLOBALS['parametros']['accion']);
            return;
        }

        //Si no existe la Función
        if( isset($funcion) && !$funcion && 
            in_array($accion, array('editar','info','eliminar','permisos_listar','permisos_editar','relaciones_listar','relaciones_editar'))){
            $GLOBALS['resultado']->setError("La Función no existe.");
            return;
        }

        //llamamos a la accion
        return call_user_func_array(array($this, $accion), array());
    }

    /**
     * @nombre: Información de Función
     * @descripcion: Información de Función
     */
    public function info(){

        //Query DB
        $stmt = $GLOBALS['conf']['pdo']->prepare("  SELECT *
                                                    FROM funciones
                                                    WHERE id = ".$GLOBALS['parametros']['funcion']);
        $stmt->execute();
        $datos = $stmt->fetch(PDO::FETCH_ASSOC);

        //Guardamos los datos
        $GLOBALS['resultado']->_result = $datos;
    }

    /**
     * @nombre: Lista Funciones (completa)
     * @descripcion: Listado de Funciones completo.
     */
    public function listar(){

        //EStamos buscando o solo listando?
        if($GLOBALS['parametros']['q'] == '') $busqueda = false;
        else $busqueda = true;

        //FUNCIONES
        $stmt = $GLOBALS['conf']['pdo']->prepare("  SELECT      *
                                                    FROM        funciones
                                                    ORDER BY    grupo ASC, 
                                                                accion ASC");
        $stmt->execute();
        $funciones =  $stmt->fetchAll(PDO::FETCH_ASSOC);



        //PERMISOS API
        $stmt = $GLOBALS['conf']['pdo']->prepare("  SELECT 
                                                            id_funcion,
                                                            id_api,
                                                            (SELECT CONCAT(modulo_nombre, ' - ', accion_nombre) as label FROM api WHERE id = id_api) as label_api
                                                    
                                                    FROM funciones_api");
        $stmt->execute();
        $permisos_api =  $stmt->fetchAll(PDO::FETCH_ASSOC);


        //RELACIONES
        $stmt = $GLOBALS['conf']['pdo']->prepare("  SELECT
                                                        id,
                                                        funcion_id,
                                                        CONCAT( (SELECT grupo FROM funciones WHERE id = funcion_id), 
                                                                ' - ', 
                                                                (SELECT accion FROM funciones WHERE id = funcion_id)) as label
                                                    FROM funciones_funciones");
        $stmt->execute();
        $relaciones =  $stmt->fetchAll(PDO::FETCH_ASSOC);


        
        //Agregamos los permisos
        foreach($funciones as $k=>$f){
            $funciones[$k]['api_permisos'] = $GLOBALS['toolbox']->get_permisos_funcion($f['id'], $permisos_api, $relaciones, $funciones);
        }



        foreach($funciones as $k=>$f){
            $funciones[$k]['relaciones'] = array();
            foreach ($relaciones as $k1=>$r)
                if($r['id'] == $f['id']){
                    $relacion = array();
                    $relacion['id'] = $r['funcion_id'];
                    $relacion['label'] = $r['label'];
                    $funciones[$k]['relaciones'][] = $relacion; 
                }
        }


        //Si estamos buscando...
        if($busqueda){

            //Score
            foreach($funciones as $k=>$v)
                $funciones[$k]['score'] = $GLOBALS['toolbox']->compare_strings($GLOBALS['parametros']['q'], $v['grupo'].' '.$v['accion']);

            //Eliminamos 0's
            foreach($funciones as $k=>$v)
                if($v['score'] == 0)
                    unset($funciones[$k]);

            //Ordenamos
            usort($funciones, function($a, $b) {
                return $b['score'] - $a['score'];
            });
        }

        //Guardamos los datos
        $GLOBALS['resultado']->_result = $funciones;
    }

    /**
     * @nombre: Agregar una Función
     * @descripcion: Agrega funciones al sistema
     */
    public function agregar(){

        $stmt = $GLOBALS['conf']['pdo']->prepare("  INSERT INTO funciones (identificador,grupo,accion) 
                                                    VALUES ('".$GLOBALS['parametros']['identificador']."','".$GLOBALS['parametros']['grupo']."','".$GLOBALS['parametros']['accion']."')");
        $items = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $stmt->execute();
    }

    /**
     * @nombre: Editar una Función
     * @descripcion: Edita datos basicos de la Función
     */
    public function editar(){

        //DB query
        $stmt = $GLOBALS['conf']['pdo']->prepare("  UPDATE funciones 
                                                    SET 
                                                        identificador='".$GLOBALS['parametros']['identificador']."', 
                                                        grupo='".$GLOBALS['parametros']['grupo']."', 
                                                        accion='".$GLOBALS['parametros']['accion']."' 
                                                    WHERE id = ".$GLOBALS['parametros']['funcion']);
        $items = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $stmt->execute();
    }

    /**
     * @nombre: Eliminar una Función
     * @descripcion: Eliminar una Función
     */
    public function eliminar(){

        //DB Query
        $stmt = $GLOBALS['conf']['pdo']->prepare("DELETE FROM funciones WHERE id = '".$GLOBALS['parametros']['funcion']."'");
        $stmt->execute();
    }

    /**
     * @nombre: Lista los Permisos a la API
     * @descripcion: Lista los Permisos a la API
     */
    public function permisos_listar(){

        //Query DB
        $stmt = $GLOBALS['conf']['pdo']->prepare("  SELECT  id,
                                                            modulo_nombre,
                                                            accion_nombre,
                                                            (SELECT COUNT(*) FROM funciones_api WHERE (id_funcion = ".$GLOBALS['parametros']['funcion'].") AND (id_api = id)) as acceso
                                                    FROM api
                                                    ORDER BY acceso DESC, modulo ASC, accion ASC");
        $stmt->execute();
        $api = $stmt->fetchAll(PDO::FETCH_ASSOC);

        //Asignamos resultados
        $GLOBALS['resultado']->_result = $api;
    }

    /**
     * @nombre: Edita los Permisos a la API
     * @descripcion: Edita los Permisos a la API
     */
    public function permisos_editar(){

        //Acciones API
        $parametros= array();
        if(count($GLOBALS['parametros']['permisos'])){
            $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT id FROM api WHERE id IN (".implode(',',$GLOBALS['parametros']['permisos']).")");
            $stmt->execute();
            $parametros = $stmt->fetchAll(PDO::FETCH_COLUMN, 0);
        }


        // Begin Transaction
        $GLOBALS['conf']['pdo']->beginTransaction();

        try {

            //Borramos todos los permisos anteriores
            $stmt = $GLOBALS['conf']['pdo']->prepare("DELETE FROM funciones_api WHERE id_funcion =" . $GLOBALS['parametros']['funcion']);
            $stmt->execute();

            //Si existen permisos que setear...
            if(count($parametros)){
                //Generamos la Query
                $sql = "INSERT INTO funciones_api (id_funcion, id_api) VALUES ";
                foreach($parametros as $k=>$v){
                    if($k) $sql .= ', ';
                    $sql .= "(".$GLOBALS['parametros']['funcion'].",".$v.")";
                }

                //Comentemos los cambios en la DB
                $stmt = $GLOBALS['conf']['pdo']->prepare($sql);
                $stmt->execute();
            }

            // Commit Transaction
            $GLOBALS['conf']['pdo']->commit();

        } // Si existieron errores
        catch (PDOException $e) {
            // hacemos un Rollback
            $GLOBALS['conf']['pdo']->rollback();

            //Agregamos error 
            $GLOBALS['resultado']->setError('Ocurrio un error al tratar de Guardar los Cambios.');
            return;
        }
    }

    /**
     * @nombre: Lista las Dependencias con otras Funciones
     * @descripcion: Lista las Dependencias con otras Funciones
     */
    public function relaciones_listar(){

        //Obtenemos los usuarios
        $stmt = $GLOBALS['conf']['pdo']->prepare("  SELECT  id,
                                                            identificador,
                                                            grupo,
                                                            accion,
                                                            (SELECT COUNT(*) FROM funciones_funciones WHERE id = ".$GLOBALS['parametros']['funcion']." AND funcion_id = funciones.id ) as activo
                                                    FROM    funciones
                                                    ORDER BY grupo DESC, accion DESC");
        $stmt->execute();
        $funciones = $stmt->fetchAll(PDO::FETCH_ASSOC);

        //Guardamos los datos
        $GLOBALS['resultado']->_result = $funciones;
    }

    /**
     * @nombre: Edita las Dependencias con otras Funciones
     * @descripcion: Parametros:<br/>id
     */
    public function relaciones_editar(){
 
        //Filtramos las relaciones
        $relaciones = array();
        if(count($GLOBALS['parametros']['relaciones'])){
            $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT id FROM funciones WHERE id IN (".implode(',',$GLOBALS['parametros']['relaciones']).")");
            $stmt->execute();
            $relaciones = $stmt->fetchAll(PDO::FETCH_COLUMN, 0);
        }

        // Begin Transaction
        $GLOBALS['conf']['pdo']->beginTransaction();

        try {

            //Borramos las relaciones anteriores
            $stmt = $GLOBALS['conf']['pdo']->prepare("DELETE FROM funciones_funciones WHERE id = ".$GLOBALS['parametros']['funcion']);
            $stmt->execute();

            //Si existen permisos que setear...
            if(count($relaciones)){
                //Generamos la Query
                $sql = "INSERT INTO funciones_funciones (id, funcion_id) VALUES ";
                foreach($relaciones as $k=>$v){
                    if($k) $sql .= ', ';
                    $sql .= "(".$GLOBALS['parametros']['funcion'].",".$v.")";
                }

                //Comentemos los cambios en la DB
                $stmt = $GLOBALS['conf']['pdo']->prepare($sql);
                $stmt->execute();
            }

            // Commit Transaction
            $GLOBALS['conf']['pdo']->commit();

        } // Si existieron errores
        catch(PDOException $e){
            // hacemos un Rollback
            $GLOBALS['conf']['pdo']->rollback();

            //Agregamos error 
            $GLOBALS['resultado']->setError($e);
            return;
        }
    }
}
?>