<?php




// Mostramos todos los errores
// Ojo porque aparecen warnings
// que hacen que no se devuelva el json


ini_set ('display_errors', 'on');
ini_set ('log_errors', 'on');
ini_set ('display_startup_errors', 'on');
ini_set ('error_reporting', E_ALL);


header("Access-Control-Allow-Origin: http://localhost:3000");
header("Access-Control-Allow-Credentials: true");
header("Access-Control-Max-Age: 1000");
header("Access-Control-Allow-Headers: X-Requested-With, Content-Type, Origin, Cache-Control, Pragma, Authorization, Accept, Accept-Encoding");
header("Access-Control-Allow-Methods: PUT, POST, GET, OPTIONS, DELETE");


//Captamos salida para GZIP
ob_start("ob_gzhandler");

// Archivo de configuracion
include_once('config.php');

//Incluimos Printnode
include './classes/PrintNode/Bootstrap.php';

//Sanatizamos MODULE
$modulo = '';
if(isset($_REQUEST["module"])) $modulo = trim( preg_replace('/[^-a-zA-Z0-9_]/', '', $_REQUEST["module"]) );

//Sanatizamos RUN
$accion = '';
if(isset($_REQUEST["run"])) $accion = trim( preg_replace('/[^-a-zA-Z0-9_]/', '', $_REQUEST["run"]) );


//Module y Run deben tener algun dato
if( ($modulo == '') || ($accion == '') ){
	$GLOBALS['resultado']->setError("Error de parametros");
	return;
}

//Chequeamos que exista el modulo
if (!file_exists("./modules/".$modulo.".php")){
	$GLOBALS['resultado']->setError("El modulo no existe");

	//Si no existe el modulo, antes de salir verificamos que no exista en la DB tampoco
	$stmt = $GLOBALS['conf']['pdo']->query("DELETE FROM api WHERE modulo = '".$modulo."'");

	//Salir
	return;
}

//Incluimos el modulo que vamos a utilizar
include_once("./modules/".$modulo.".php");


//Instanciamos la Clase
if( !($modulo_instance = new $modulo()) ){
	$GLOBALS['resultado']->setError("No se pudo instanciar el modulo");

	//Salir
	return;
}

//Validamos existencia del metodo
if (!method_exists($modulo_instance, $accion)) {
	$GLOBALS['resultado']->setError("La accion no existe");

	//Si no existe el modulo, antes de salir verificamos que no exista en la DB tampoco
	$stmt = $GLOBALS['conf']['pdo']->query("DELETE FROM api WHERE modulo = '".$modulo."' AND accion='".$accion."'");

	//Salir
	return;
}

//Obtenemos Nombre y descripcion de la Clase y el metodo
$modulo_datos = $GLOBALS['toolbox']->getClaseComent($modulo);
$accion_datos = $GLOBALS['toolbox']->getMetodoComent($modulo, $accion);


//Si llegamos hasta aqui, estamos seguros que la clase y el metodo existen en los archivos
//Estas tareas las agruparemos en una multiple query para optimizar tiempo de respuesta:
// - Crear una nueva ocurrencia si no existe el metodo.
// - Actualizar los datos del mapeado API si ya existe el Metodo o la Clase.
// - Consultar que permisos tiene este usuario en particular.

//Por defecto el usuario es 'anonimo'
//si existe un usuario en las session
//lo recuperamos y lo guardamos.
$usuario = "anonimo";
if($GLOBALS['session']->getData('usuario'))
	$usuario = $GLOBALS['session']->getData('usuario');

//Agregamos el modulo si no existe
$sql = "INSERT INTO api (modulo, modulo_nombre, modulo_descripcion, accion, accion_nombre, accion_descripcion)
    	SELECT 		t.modulo, t.modulo_nombre, t.modulo_descripcion, t.accion, t.accion_nombre, t.accion_descripcion 
		FROM 		(	SELECT 	'".$modulo."' as modulo, 
	             				'".$modulo_datos['nombre']."' as modulo_nombre, 
	             				'".$modulo_datos['descripcion']."' as modulo_descripcion, 
	             				'".$accion."' as accion, 
	             				'".$accion_datos['nombre']."' as accion_nombre, 
	             				'".$accion_datos['descripcion']."' as accion_descripcion) as t
	
    	WHERE NOT EXISTS ( SELECT * FROM api WHERE modulo='".$modulo."' AND accion='".$accion."' );";

//Obtenemos el Rol del usuario actual
if($usuario != 'anonimo') $sql .= "SET @rol = (SELECT rol_id FROM usuarios WHERE usuario = '".$GLOBALS['session']->getData('usuario')."');";
else $sql .= "SET @rol = 0;";

//Chequeamos que el usuario este activo
if($usuario != 'anonimo') $sql .= "SET @activo = (SELECT COUNT(usuario) FROM usuarios WHERE usuario = '".$usuario."' AND activo = 1);";
else $sql .= "SET @activo = 1;";

//Guardamos el id de el Modulo / Accion
$sql .= "SET @accion_id = (SELECT id FROM api WHERE modulo='".$modulo."' AND accion='".$accion."');";

//Devolvemos el id del rol actual
$sql .= "SELECT @rol LIMIT 1;";

//Consultamos el permiso para este Modulo / Accion
$sql .= "SELECT DISTINCT COUNT(id_api) as permiso FROM funciones_api 
		WHERE id_funcion IN ( SELECT funcion_id 
		FROM (	(SELECT funcion_id FROM permisos_roles WHERE rol_id = @rol) 
				UNION 
				(SELECT funcion_id FROM permisos_usuarios WHERE usuario_id = '".$usuario."' AND acceso = 1)
			) as t 
		WHERE funcion_id NOT IN ( SELECT funcion_id FROM permisos_usuarios WHERE usuario_id = '".$usuario."' AND acceso = 0)) 
		AND (id_api = @accion_id) AND ( @activo = 1 );";

//Pedimos los parametros para este Modulo / Accion
$sql .= "SELECT * FROM parametros WHERE id IN ( SELECT  parametro_id FROM parametros_api WHERE api_id = @accion_id );";

//Obtenemos los resultados de la consulta
//Para los permisos a este Usuario y Rol
$stmt = $GLOBALS['conf']['pdo']->query($sql);
$stmt->nextRowset();
$stmt->nextRowset();
$stmt->nextRowset();
$stmt->nextRowset();
$rol = (int)$stmt->fetchColumn();
$stmt->nextRowset();
$permiso = (bool)(int)$stmt->fetchColumn();
$stmt->nextRowset();


//Si el rol es dearrollador 
//le damos permiso siempre
if($rol == -1) $permiso = true; 


//Si no tenemos permisos 
//terminamos la ejecucion
if(!$permiso){ 
	$GLOBALS['resultado']->setError("No tiene permisos para acceder");
	return;
}


//Recolectamos los Parametros
$parametros = $stmt->fetchAll(PDO::FETCH_ASSOC);
$GLOBALS['toolbox']->generaParametrosGUMP($parametros);



//Llamamos al metodo si es que
//no tenemos algun error de parametros
if($GLOBALS['resultado']->_status)
	$modulo_instance->__caller($accion);

ob_flush();
?>