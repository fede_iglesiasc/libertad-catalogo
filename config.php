<?php
$GLOBALS['cronometrar'] = false;


//Iniciamos cronometro
if($GLOBALS['cronometrar']) 
	$GLOBALS['cronometro'] = round(microtime(true) * 1000);


//Incluimos las librerias
include_once('mysql.php');
include_once('classes/class.module.php');
include_once('classes/class.result.php');
include_once('classes/class.gump.php');

//Clase resultado
$GLOBALS['resultado'] = new Result();

//Creamos la clase para validar params
$GLOBALS['GUMP'] = new GUMP();


//Obtenemos la session
try{ 
  
  //Creamos o recuperamos la session
  $GLOBALS['session'] = new Session();

	//Si no tiene definido un usuario entonces es anonimo
	if( !$GLOBALS['session']->getData('usuario') )
		$GLOBALS['session']->setData('rol',0);

}catch (Exception $e) {
  $resultado = 'No se pudo acceder a la session';
  echo json_encode($resultado);
  exit;
}


//Leemos los permisos desde la session
//no tenemosque recrearlos cada vez que
//accedemos a una funcion de la API para
//optimizar el uso del script.
$GLOBALS['permisos_api'] = $GLOBALS['session']->getData('permisos_api');

//Generamos los permisos para toda la APP
$GLOBALS['toolbox'] ->getPermisosFunciones();
?>