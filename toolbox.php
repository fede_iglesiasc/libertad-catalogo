<?php
class Toolbox
{
    //Diccionario
    public $dicc = array();

    public function __construct(){
        //Diccionario de palabras de los Rubros
        $this->dicc['abrazadera'] = array('abrazadera','abrasadera','avrazadera','avrasadera','abrazaderas','abrasaderas','avrazaderas','avrasaderas'); 
        $this->dicc['acople'] = array('akople','acoples','akoples'); 
        $this->dicc['aditivo'] = array('aditibo','aditibos','aditivos'); 
        $this->dicc['aire'] = array('aires'); 
        $this->dicc['altura'] = array('alturas'); 
        $this->dicc['areglo'] = array('areglos'); 
        $this->dicc['amortiguador'] = array('amortiguadores','amortiwador','amortiwadores'); 
        $this->dicc['anticorosivo'] = array('anticorocivo','anticorosibo','anticorocibo','anticorosivos','anticorocivos','anticorosibos','anticorocibos'); 
        $this->dicc['auxiliar'] = array('aucsiliar','ausiliar','auciliar','auxiliares','aucsiliares','ausiliares','auciliares'); 
        $this->dicc['axial'] = array('acsial','asial','acial','azial','axiales','acsiales','asiales','aciales','aziales'); 
        $this->dicc['balanceo'] = array('valanceo','balanseo','valanseo','balanzeo','valanzeo','balancear','valancear','balansear','valansear','balanzear','valanzear'); 
        $this->dicc['bara'] = array('vara','barras','varas'); 
        $this->dicc['bieleta'] = array('bieletas','vieleta','vieletas'); 
        $this->dicc['bolilero'] = array('volilero','boliyero','voliyero','bolileros','volileros','boliyeros','voliyeros'); 
        $this->dicc['bomba'] = array('bomva','vomba','vomva','bonba','bonva','vonba','vonva','bombas','bomvas','vombas','vomvas','bonbas','bonvas','vonbas','vonvas'); 
        $this->dicc['bombin'] = array('bombin','bomvin','vombin','vomvin','bonbin','bonvin','vonbin','vonvin','bombim','bomvim','vombim','vomvim','bonbim','bonvim','vonbim','vonvim','bombines','bomvines','vombines','vomvines','bonbines','bonvines','vonbines','vonvines','bombimes','bomvimes','vombimes','vomvimes','bonbimes','bonvimes','vonbimes','vonvimes');
        $this->dicc['brazo'] = array('braso','vrazo','vraso','brazos','brasos','vrazos','vrasos'); 
        $this->dicc['buje'] = array('buge','buhe','vuje','vuge','vuhe','bujes','buges','buhes','vujes','vuges','vuhes');
        $this->dicc['bulon'] = array('bulones','vulon','vulones'); 
        $this->dicc['cabina'] = array('cavina','kabina','kavina','cabinas','cavinas','kabinas','kavinas'); 
        $this->dicc['caja'] = array('caha','kaja','kaha','cajas','cahas','kajas','kahas'); 
        $this->dicc['caliper'] = array('kaliper','calipers','kalipers'); 
        $this->dicc['cambio'] = array('cambio','camvio','kambio','kamvio','cambios','camvios','kambios','kamvios'); 
        $this->dicc['campana'] = array('campana','canpana','kampana','kanpana','campanas','canpanas','kampanas','kanpanas'); 
        $this->dicc['cano'] = array('cano','kano','canos','kanos'); 
        $this->dicc['capot'] = array('capot','kapot','capo','kapo'); 
        $this->dicc['cardan'] = array('cardan','kardan','cardanes','kardanes'); 
        $this->dicc['caroceria'] = array('caroceria','caroseria','carozeria','karoceria','karoseria','karozeria','carocerias','caroserias','carozerias','karocerias','karoserias','karozerias'); 
        $this->dicc['carter'] = array('carter','karter','carteres','karteres'); 
        $this->dicc['cazoleta'] = array('cazoleta','casoleta','kazoleta','kasoleta','cazoletas','casoletas','kazoletas','kasoletas'); 
        $this->dicc['chasis'] = array('chasis','chacis','chazis','chasiz','chaciz','chaziz'); 
        $this->dicc['cilindro'] = array('cilindro','silindro','zilindro','cilindros','silindros','zilindros'); 
        $this->dicc['columna'] = array('columna','kolumna','coluna','koluna','columnas','kolumnas','colunas','kolunas'); 
        $this->dicc['comba'] = array('comba','comva','conba','conva','komba','komva','konba','konva','combas','comvas','conbas','convas','kombas','komvas','konbas','konvas'); 
        $this->dicc['compensadora'] = array('compensadora','conpensadora','compenzadora','conpenzadora','kompensadora','konpensadora','kompenzadora','konpenzadora','compensadoras','conpensadoras','compenzadoras','conpenzadoras','kompensadoras','konpensadoras','kompenzadoras','konpenzadoras'); 
        $this->dicc['contrapeso'] = array('contrapeso','contrapezo','kontrapeso','kontrapezo','contrapesos','contrapezos','kontrapesos','kontrapezos'); 
        $this->dicc['corector'] = array('corector','korector','corektor','korektor','corectores','korectores','corektores','korektores'); 
        $this->dicc['crapodina'] = array('crapodina','krapodina','crapodima','krapodima','crapodinas','krapodinas','crapodimas','krapodimas'); 
        $this->dicc['cremalera'] = array('cremalera','cremayera','kremalera','kremayera','cremaleras','cremayeras','kremaleras','kremayeras'); 
        $this->dicc['cruceta'] = array('cruceta','kruceta','cruseta','kruseta','cruzeta','kruzeta','crucetas','krucetas','crusetas','krusetas','cruzetas','kruzetas'); 
        $this->dicc['cubeta'] = array('cubeta','cuveta','kubeta','kuveta','cubetas','cuvetas','kubetas','kuvetas');
        $this->dicc['cubre'] = array('cubre','cuvre','kubre','kuvre'); 
        $this->dicc['cubrecarter'] = array('cubrecarter','cuvrecarter','kubrecarter','kuvrecarter','cubrekarter','cuvrekarter','kubrekarter','kuvrekarter','cubrecarters','cuvrecarters','kubrecarters','kuvrecarters','cubrekarters','cuvrekarters','kubrekarters','kuvrekarters'); 
        $this->dicc['deposito'] = array('depocito','depozito','depositos','depocitos','depozitos');
        $this->dicc['desengrasante'] = array('desengrasante','decengrasante','dezengrasante','desengrazante','decengrazante','dezengrazante','desengrasantes','decengrasantes','dezengrasantes','desengrazantes','decengrazantes','dezengrazantes'); 
        $this->dicc['despiece'] = array('despiese','despieze','dezpiese','dezpieze'); 
        $this->dicc['diferencial'] = array('diferenciales','diferensial','diferensiales','diferenzial','diferenziales'); 
        $this->dicc['direcion'] = array('direcion','direcsion','direxion','diresion','direciones','direcsiones','direxiones','diresiones'); 
        $this->dicc['disco'] = array('discos','disko','diskos');
        $this->dicc['duty'] = array('duti');
        $this->dicc['distribucion'] = array('distribucion','distrivucion','distribusion','distrivusion','distribuzion','distrivuzion','distribuciones','distrivuciones','distribusiones','distrivusiones','distribuziones','distrivuziones'); 
        $this->dicc['eje'] = array('ejes','ehe','ehes'); 
        $this->dicc['elastico'] = array('elasticos','elastiko','elastikos'); 
        $this->dicc['embrague'] = array('embrague','embrage','embraye','embraje','emvrague','emvrage','emvraye','emvraje','enbrague','enbrage','enbraye','enbraje','envrague','envrage','envraye','envraje','embragues','embrages','embrayes','embrajes','emvragues','emvrages','emvrayes','emvrajes','enbragues','enbrages','enbrayes','enbrajes','envragues','envrages','envrayes','envrajes'); 
        $this->dicc['escape'] = array('escapes','eskape','eskapes'); 
        $this->dicc['espiral'] = array('espirales','ezpiral','ezpirales'); 
        $this->dicc['estabilizadora'] = array('estabilizadora','eztabilizadora','estavilizadora','eztavilizadora','estabilisadora','eztabilisadora','estavilisadora','eztavilisadora','estabilizadoras','eztabilizadoras','estavilizadoras','eztavilizadoras','estabilisadoras','eztabilisadoras','estavilisadoras','eztavilisadoras');
        $this->dicc['extremo'] = array('extremo','estremo','ecstremo','extremos','estremos','ecstremos'); 
        $this->dicc['filtro'] = array('filtros'); 
        $this->dicc['fin'] = array('fines'); 
        $this->dicc['flexible'] = array('flexible','flesible','flecsible','flecible','flexibles','flesibles','flecsibles','flecibles'); 
        $this->dicc['fluido'] = array('fluidos'); 
        $this->dicc['freno'] = array('frenos'); 
        $this->dicc['fuele'] = array('fueye','fueles','fueyes');
        $this->dicc['grasa'] = array('grasas','graza','grazas'); 
        $this->dicc['guardapolvo'] = array('guardapolvo','guardapolbo','wardapolvo','wardapolbo','guardapolvos','guardapolbos','wardapolvos','wardapolbos'); 
        $this->dicc['heavy'] = array('heavy','eavy','geavy','javy','heavi','eavi','geavi','jeavi','heaby','eaby','geaby','jaby','heabi','eabi','geabi','jeabi'); 
        $this->dicc['homocinetica'] = array('homocinetica','omocinetica','homosinetica','omosinetica','homozinetica','omozinetica','homocineticas','omocineticas','homosineticas','omosineticas','homozineticas','omozineticas','homocinetika','omocinetika','homosinetika','omosinetika','homozinetika','omozinetika','homocinetikas','omocinetikas','homosinetikas','omosinetikas','homozinetikas','omozinetikas'); 
        $this->dicc['horquila'] = array('horquila','orquila','horqila','orqila','horkila','orkila','horquiya','orquiya','horqiya','orqiya','horkiya','orkiya','horquilas','orquilas','horqilas','orqilas','horkilas','orkilas','horquiyas','orquiyas','horqiyas','orqiyas','horkiyas','orkiyas'); 
        $this->dicc['juego'] = array('juegos'); 
        $this->dicc['kit'] = array('kits'); 
        $this->dicc['lubricante'] = array('lubricante','luvricante','lubrikante','luvrikante','lubricantes','luvricantes','lubrikantes','luvrikantes'); 
        $this->dicc['maestro'] = array('maestros','maeztro','maeztros'); 
        $this->dicc['manchon'] = array('manchones'); 
        $this->dicc['maza'] = array('mazas','masa','masas'); 
        $this->dicc['motor'] = array('motores'); 
        $this->dicc['movimiento'] = array('movimientos','mobimiento','mobimientos'); 
        $this->dicc['multigrado'] = array('multigrados');
        $this->dicc['neumatico'] = array('neumaticos'); 
        $this->dicc['palanca'] = array('palancas','palanka','palankas'); 
        $this->dicc['panhard'] = array('panhard','panjard','panhar','panjar','pamhard','pamjard','pamhar','pamjar');
        $this->dicc['parila'] = array('parila','pariya','parilas','pariyas'); 
        $this->dicc['parte'] = array('partes'); 
        $this->dicc['pastila'] = array('pastila','pastiya','paztila','paztiya','pastilas','pastiyas','paztilas','paztiyas'); 
        $this->dicc['perno'] = array('pernos'); 
        $this->dicc['piston'] = array('pistones','pizton','piztones'); 
        $this->dicc['pitman'] = array(); 
        $this->dicc['pivot'] = array('pivotes','pibot','pibotes', 'pivote', 'pivotes', 'pibote','pibotes');
        $this->dicc['precap'] = array('precaps','prekap','prekaps'); 
        $this->dicc['liquido'] = array('liquido','likido','liquidos','likidos'); 
        $this->dicc['axial'] = array('axiales','acsial','acsiales','asial','asiales','acial','aciales'); 
        $this->dicc['progresivo'] = array('progresivo','progresibo','progrecivo','progrecibo','progrezivo','progrezibo','progresivos','progresibos','progrecivos','progrecibos','progrezivos','progrezibos'); 
        $this->dicc['puente'] = array('puentes'); 
        $this->dicc['punta'] = array('puntas'); 
        $this->dicc['radiador'] = array('radiadores'); 
        $this->dicc['refrigerante'] = array('refrigerante','refrijerante','refrigerantes','refrijerantes'); 
        $this->dicc['registro'] = array('registro','rejistro','registros','rejistros'); 
        $this->dicc['regulador'] = array('reguladores'); 
        $this->dicc['reparacion'] = array('reparacion','reparasion','reparazion','reparaxion','reparaciones','reparasiones','reparaziones','reparaxiones'); 
        $this->dicc['repuesto'] = array('repuestos','repuezto','repueztos');
        $this->dicc['resorte'] = array('resorte','rezorte','resortes','rezortes'); 
        $this->dicc['reten'] = array('retenes'); 
        $this->dicc['rodamiento'] = array('rodamiento','rodamientos','ruleman','rulemanes'); 
        $this->dicc['rodilo'] = array('rodilo','rodiyo','rodilos','rodiyos'); 
        $this->dicc['rotula'] = array('rotulas'); 
        $this->dicc['rueda'] = array('ruedas'); 
        $this->dicc['salida'] = array('salidas','zalida','zalidas'); 
        $this->dicc['sector'] = array('sectores','cector','cectores','zector','zectores'); 
        $this->dicc['semieje'] = array('semieje','semiege','semiehe','senieje','seniege','seniehe','cemieje','cemiege','cemiehe','cenieje','ceniege','ceniehe','zemieje','zemiege','zemiehe','zenieje','zeniege','zeniehe','semiejes','semieges','semiehes','seniejes','senieges','seniehes','cemiejes','cemieges','cemiehes','ceniejes','cenieges','ceniehes','zemiejes','zemieges','zemiehes','zeniejes','zenieges','zeniehes'); 
        $this->dicc['servo'] = array('servo','cervo','zervo','serbo','cerbo','zerbo','servos','cervos','zervos','serbos','cerbos','zerbos');
        $this->dicc['servofreno'] = array('servofreno','cervofreno','zervofreno','serbofreno','cerbofreno','zerbofreno','servofrenos','cervofrenos','zervofrenos','serbofrenos','cerbofrenos','zerbofrenos'); 
        $this->dicc['soporte'] = array('soporte','zoporte','soportes','zoportes'); 
        $this->dicc['suplemento'] = array('suplemento','zuplemento','suplementos','zuplementos'); 
        $this->dicc['suspension'] = array('suspension','zuspension','suspencion','zuspencion','suspenzion','zuspenzion'); 
        $this->dicc['tapa'] = array('tapas'); 
        $this->dicc['tensor'] = array('tensores','tenzor','tenzores'); 
        $this->dicc['tireta'] = array('tiretas'); 
        $this->dicc['tope'] = array('topes'); 
        $this->dicc['torsion'] = array('torcion','torzion'); 
        $this->dicc['transmision'] = array('transmision','trasmision','tranzmision','trazmision','transmicion','trasmicion','tranzmicion','trazmicion','transmizion','trasmizion','tranzmizion','trazmizion','transmisiones','trasmisiones','tranzmisiones','trazmisiones','transmiciones','trasmiciones','tranzmiciones','trazmiciones','transmiziones','trasmiziones','tranzmiziones','trazmiziones'); 
        $this->dicc['trasero'] = array('trasero','tracero','trazero','traseros','traceros','trazeros'); 
        $this->dicc['travesano'] = array('travesano','trabesano','travezano','trabezano','travesanos','trabesanos','travezanos','trabezanos'); 
        $this->dicc['triceta'] = array('triceta','triseta','trizeta','tricetas','trisetas','trizetas'); 
        $this->dicc['universal'] = array('universal','unibersal','univerzal','uniberzal','universales','unibersales','univerzales','uniberzales'); 
        $this->dicc['valvula'] = array('valvula','balvula','valbula','balbula','valvulas','balvulas','valbulas','balbulas'); 




        //Correccion para las marcas
        $this->dicc['snr'] = array('srn');
        $this->dicc['solmi'] = array('zolmi','solni','zolni');
        $this->dicc['sadar'] = array('zadar');
        $this->dicc['teca'] = array('teka');
        $this->dicc['fabila'] = array('favila');
        $this->dicc['fremax'] = array('fremaz','fremas');
        $this->dicc['avan'] = array('aban');
        $this->dicc['gacri'] = array('gakri');
        $this->dicc['wen'] = array('guen');
        $this->dicc['kroy'] = array('croi','croy','kroi');
        $this->dicc['aston'] = array('azton');
        $this->dicc['bator'] = array('vator');
        $this->dicc['tribuno'] = array('trivuno');
        $this->dicc['vth'] = array('vht','bth','bht');
        $this->dicc['mazfren'] = array('masfren','maxfren','masfrem','maxfrem','mazfrem','masafren');
        $this->dicc['rsf'] = array('rfs');
        $this->dicc['drl'] = array('dlr');
        $this->dicc['koyo'] = array('coyo','colo','kolo');
        $this->dicc['timken'] = array('timken','tinken','timqen','tinqen','timquen','tinquen','timkem','tinkem','timqem','tinqem','timquem','tinquem');
        $this->dicc['corven'] = array('corven','corben','korven','korben','corvem','corbem','korvem','korbem');
        $this->dicc['bendix'] = array('bendix','vendix','bendis','vendis');
        $this->dicc['ctr'] = array('crt');
        $this->dicc['rey'] = array('rei');
        $this->dicc['viemar'] = array('biemar');
        $this->dicc['mbs'] = array('msb','msv','mvs');
        $this->dicc['500 milas'] = array('500 miyas');
        $this->dicc['indaltmec'] = array('indalmec','indalmek','indaltmek');
        $this->dicc['fv'] = array('fb');
        $this->dicc['grazimetal'] = array('grasimetal','gracimetal');
        $this->dicc['generica'] = array('generico');
        $this->dicc['nakata'] = array('nacata');
        $this->dicc['capemi'] = array('kapemi');
        $this->dicc['raybestos'] = array('raybestos','raibestos','rayvestos','raivestos','raybesto','raibesto','rayvesto','raivesto');
        $this->dicc['trw'] = array('twr');
        $this->dicc['apex'] = array('apec','apes');
        $this->dicc['tensa'] = array('tenza');
        $this->dicc['joint'] = array('yoin','join','yoint');
        


        //Marcas de vehiculos
        $this->dicc['renault'] = array('renau','reno','renaul','renau');
        $this->dicc['peugeot'] = array('peuyo','peujo','peugot','peujeot','peuyot');
        $this->dicc['volkswagen'] = array('volwagen','volkwagen','volwagen','vw','voulwagen');
        $this->dicc['skoda'] = array('escoda', 'scoda');
        $this->dicc['rover'] = array('rober');
        $this->dicc['ssangyong'] = array('sangyong','yansong','sangyoung','ssangyoung');
        $this->dicc['subaru'] = array('subaru','zubaru','suvaru','zuvaru');
        $this->dicc['suzuki'] = array('suzuki','zuzuki','susuki','zusuki','suzuky','zuzuky','susuky','zusuky');
        $this->dicc['volvo'] = array('volvo','volbo','bolvo','bolbo');

        //Dimensiones ubicaciones etc
        $this->dicc['der'] = array('derecha','derecho');
        $this->dicc['izq'] = array('izquierda','izquierdo');
        $this->dicc['inf'] = array('inferior','infer');
        $this->dicc['sup'] = array('superior','superi');
        $this->dicc['tras'] = array('trasera','trasero','trazero','tracero','trazera','tracera');
        $this->dicc['del'] = array('delantera','delantero');
    }

    //Marca un nuevo punto en el cronometrado
    //Le pasamos un string con la descripcion
    //Que debe devolver
    function cronometra($descripcion){
        
        //Ahora mismo
        $now = round(microtime(true) * 1000);

        //Si no seteamos nunca el incial lo hacemos
        if(!isset($GLOBALS['cronometro']))
            $GLOBALS['cronometro'] = round(microtime(true) * 1000);

        $GLOBALS['resultado']->_cronometro[] = $descripcion . ': ' . (round(microtime(true) * 1000) - $GLOBALS['cronometro']);
        $GLOBALS['cronometro'] = $now;
    }

    //Le pasamos un string con un comentario de una funcion
    //y un nombre del tag (por ej @descripcion) y nos devuelve
    //el valor de dicho tag
    function getDocComment($str, $tag = ''){ 
        // Si no declaramo un tag devolvemos
        // la descripcion general
        if(empty($tag)) return $str;

        $matches = array(); 
        preg_match("/".$tag.":(.*)(\\r\\n|\\r|\\n)/U", $str, $matches); 

        //Si encontramos el tag que etan pidiendo devolvemos
        //su valor.
        if (isset($matches[1])) return trim($matches[1]);

        // Si no pasa nada de lo anterior
        // devolvemos un string vacio.
        return ''; 
    }

    //Le pasamos un string con el codigo completo con
    //prefijo, codigo y sufijo. Si en codigo tiene un
    //formato correcto nos devuelve un arreglo assoc
    //con los keys prefijo, codigo y sufijo sino FALSE
    function desglosa_codigo($codigo){
        //Antes que nada quitamos todos los espacios
        $codigo = str_replace(' ','',$codigo);

        //La cantidad minima de guiones es 2
        if(substr_count($codigo,'-') < 2) 
            return false;

        //Buscamos la posicion del primer guion
        $primer_guion = strpos($codigo, '-');

        //El prefijo debe tener como minimo un caracter y el sufijo tmb
        if( ($primer_guion == 0) || ($primer_guion >= (strlen($codigo)-2)) ) 
            return false;

        //Buscamos la posicion del ultimo guion
        $segundo_guion = strrpos($codigo, '-');

        //El sufijo debe tener como minimo un caracter y el sufijo tmb
        if( ($segundo_guion >= (strlen($codigo)-1) ) || ($segundo_guion <= 2 )) 
            return false;

        $resultado['prefijo'] = substr($codigo, 0, $primer_guion);
        $resultado['sufijo'] = strtoupper(substr($codigo, $segundo_guion+1, strlen($codigo)));
        $resultado['codigo'] = substr($codigo, $primer_guion+1, ((strlen($resultado['sufijo'])+1)*-1) );

        //Chequeamos que todos sean o numeros o letras
        if(!ctype_alnum($resultado['prefijo']) || !ctype_alnum($resultado['codigo']) || !ctype_alnum($resultado['sufijo']))
            return false;

        return $resultado;
    }

    //Obtenemos los comentarios de una clase
    function getClaseComent($clase){
        $rfc = new ReflectionClass($clase);
        $comentario = $rfc->getDocComment();
        $resultado['nombre'] = str_replace("'","\\'",$GLOBALS['toolbox']->getDocComment($comentario, "@nombre"));
        $resultado['descripcion'] = str_replace("'","\\'",$GLOBALS['toolbox']->getDocComment($comentario, "@descripcion"));
        return $resultado;
    }

    //Obtenemos los comentarios de una clase
    function getMetodoComent($clase, $metodo){
        $rfm = new ReflectionMethod($clase, $metodo);
        $comentario = $rfm->getDocComment();
        $resultado['nombre'] = str_replace("'","\\'",$GLOBALS['toolbox']->getDocComment($comentario, "@nombre"));
        $resultado['descripcion'] = str_replace("'","\\'",$GLOBALS['toolbox']->getDocComment($comentario, "@descripcion"));
        return $resultado;
    }

    //Genera un arreglo (mapa) con todos los modulos y acciones
    //de la api con sus nombres, descripciones y nombres de metodos
    function mapModules(){
        $modulos_archivo = array();
        foreach (glob("modules/*.php") as $file){
            $clase = array();
            $clase['modulo'] = str_replace('.php','', str_replace('modules/','',$file));
            

            //Extraemos el comentario
            $rc = new ReflectionClass($clase['modulo']);
            $comentario = $rc->getDocComment();

            $clase['modulo_nombre'] =  $GLOBALS['toolbox']->getDocComment($comentario, "@nombre");
            $clase['modulo_descripcion'] =  $GLOBALS['toolbox']->getDocComment($comentario, "@descripcion");


            // Por cada metodo obtenemos la descripcion
            foreach(get_class_methods($clase['modulo']) as $a){
                if(($a != '__construct') && ($a != '__pre') && ($a != '__caller') && ($a != '__destruct')){
                    //Inicializamos
                    $accion = array();
                    //Limpiamos el nombre de la accion
                    $clase['accion'] = str_replace('()', '', $a);

                    // Obtenemos el comentario
                    $rm = new ReflectionMethod($clase['modulo'], $clase['accion']);
                    $comentario = $rm->getDocComment();

                    $clase['accion_nombre'] =  $GLOBALS['toolbox']->getDocComment($comentario, "@nombre");
                    $clase['accion_descripcion'] =  $GLOBALS['toolbox']->getDocComment($comentario, "@descripcion");

                    //Inicializamos la data
                    $data = array();

                    // Si la accion tiene algun
                    // comentario...
                    if($comentario){

                        // Cargamos todas las variables al arreglo
                        $vars = array();
                        preg_match_all("/@(.*):/", $comentario, $vars);

                        //Insertamos la variable
                        foreach($vars[1] as $v)
                            if(($v != 'nombre') && ($v != 'descripcion')) 
                                $data[$v] = $GLOBALS['toolbox']->getDocComment($comentario, "@".$v);
                    }

                    //Insertamos los datos serializados
                    $clase['data'] = serialize($data);

                    //modificado
                    $clase['modificado'] = filemtime($file);

                    //Agergamos la clase a la lista
                    $modulos_archivo[] = $clase;
                }
            }


        }

        //Devolvemos el listado
        return $modulos_archivo;
    }

    //Actualiza los modulos de la api automaticamente
    //a medida que vamos programando y modificando archivos
    //y los guarda en la DB para no tener que volver a
    //generarlos de forma manual.
    function updateModules(){

        //Traemos todos los modulos existentes en la DB
        $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT * FROM api");
        $stmt->execute();
        $modulos_db =  $stmt->fetchAll(PDO::FETCH_ASSOC);

        //Obtenemos todas las fechas de modificacion de los modulos
        $modulos_archivo = $GLOBALS['toolbox']->mapModules();

        //Inicializamos array
        $acciones_insert = array();
        $acciones_update = array();

        //Recorremos todos los modulos que deberían estar..
        foreach($modulos_archivo as $k=>$ma){
            //Inicializamos
            $existe = false;

            //Recorremos todos los modulos de la DB
            foreach($modulos_db as $x=>$mdb)
                //Si el módulo y la accion existe...
                if( ($ma['modulo'] == $mdb['modulo']) && ($ma['accion'] == $mdb['accion']) ){
                    $existe = true;

                    //Ahora que sabemos que este es la acción
                    //revisamos que este actualizado a la fecha
                    if($mdb['modificado'] < $ma['modificado']) $acciones_update[] = $ma;

                    // Si ya no lo necesitamos
                    // quitamos el mod. de la DB
                    unset($modulos_db[$x]);

                    //Salimos del loop
                    break;
                }

            // Si el mod. no existe en la DB lo insertamos
            if(!$existe) $acciones_insert[] = $ma;
        }



        if( count($acciones_insert) || count($acciones_update) || count($modulos_db)){

            // Begin Transaction
            $GLOBALS['conf']['pdo']->beginTransaction();

            try {

                //Insertamos los modulos nuevos
                if(count($acciones_insert)){

                    //Generamos array de insert
                    $sql_values = array();
                    foreach ($acciones_insert as $k=>$v) 
                        $sql_values[] = "('".$v['modulo']."','".str_replace("'","\'",$v['modulo_nombre'])."','".str_replace("'","\'",$v['modulo_descripcion'])."','".$v['accion']."','".str_replace("'","\'",$v['accion_nombre'])."','".str_replace("'","\'",$v['accion_descripcion'])."','".$v['data']."',".$v['modificado'].")";
                    
                    //Cometemos los cambios en la DB
                    $stmt = $GLOBALS['conf']['pdo']->prepare("INSERT INTO api (modulo, modulo_nombre, modulo_descripcion, accion, accion_nombre, accion_descripcion, data, modificado) VALUES ".implode(",", $sql_values));
                    $stmt->execute();
                }


                //Actualizamos modulos desactualizados
                foreach ($acciones_update as $k=>$v){
                    //Cometemos los cambios en la DB
                    $stmt = $GLOBALS['conf']['pdo']->prepare("  UPDATE api 
                                                                SET 
                                                                    modulo='".$v['modulo']."', 
                                                                    modulo_nombre='".str_replace("'","\'",$v['modulo_nombre'])."', 
                                                                    modulo_descripcion='".str_replace("'","\'",$v['modulo_descripcion'])."', 
                                                                    accion='".$v['accion']."', 
                                                                    accion_nombre='".str_replace("'","\'",$v['accion_nombre'])."', 
                                                                    accion_descripcion='".str_replace("'","\'",$v['accion_descripcion'])."', 
                                                                    data='".$v['data']."', 
                                                                    modificado=".$v['modificado']."  
                                                                WHERE 
                                                                    (modulo='".$v['modulo']."') AND 
                                                                    (accion='".$v['accion']."')");
                    $stmt->execute();
                }


                //Eliminamos los sobrantes
                foreach($modulos_db as $k=>$v){
                    //Cometemos los cambios en la DB
                    $stmt = $GLOBALS['conf']['pdo']->prepare("  DELETE 
                                                                FROM api  
                                                                WHERE 
                                                                    (modulo='".$v['modulo']."') AND 
                                                                    (accion='".$v['accion']."')");
                    $stmt->execute();
                }



                // Commit Transaction
                $GLOBALS['conf']['pdo']->commit();

            } // Si existieron errores generando las Querys de arriba...
            catch (PDOException $e) {
                // hacemos un Rollback
                $GLOBALS['conf']['pdo']->rollback();


                //Agregamos error 
                $resultado = new Result();
                $resultado->setError($e);
                echo $resultado->getResult();
                exit;
            }
        }
    }

    //Le pasamos un valor en estilo humano
    //de peso de datos y nos devuelve el valor
    //en bytes.
    function return_bytes($val) {
        $val = trim($val);
        $last = strtolower($val[strlen($val)-1]);
        switch($last) {
            // The 'G' modifier is available since PHP 5.1.0
            case 'g':
                $val *= 1024;
            case 'm':
                $val *= 1024;
            case 'k':
                $val *= 1024;
        }

        return $val;
    }

    //Convierte entero de bytes en MB GB etc
    function formatBytes($bytes, $precision = 2) { 
        $units = array('B', 'KB', 'MB', 'GB', 'TB'); 

        $bytes = max($bytes, 0); 
        $pow = floor(($bytes ? log($bytes) : 0) / log(1024)); 
        $pow = min($pow, count($units) - 1); 

        // Uncomment one of the following alternatives
        $bytes /= pow(1024, $pow);
        // $bytes /= (1 << (10 * $pow)); 

        return round($bytes, $precision) . ' ' . $units[$pow]; 
    } 

    //Hay internet??
    function es_local(){
        return !checkdnsrr($_SERVER['SERVER_NAME'], 'NS');
    }

    //Esta funcion es un handler para los errores de PHP
    //(REVISAR si se necesita realmente)
    function errorHandler($errno, $errstr, $errfile, $errline){
        
        //Instanciamos la clase resultado
        $resultado = new Result();

        if (!(error_reporting() & $errno)) {
            // This error code is not included in error_reporting
            $resultado->setError("This error code is not included in error_reporting.");
            echo $resultado->getResult();
            return;
        }

        switch ($errno) {
            case E_USER_ERROR:

                $resultado->setError("<b>ERROR</b> [$errno] $errstr <br /> Error fatal en linea $errline en archivo $errfile");
                echo $resultado->getResult();
                exit(1);
                break;

            case E_USER_WARNING:
                $GLOBALS['warnings'][] = "<b>ALERTA</b> [$errno] $errstr<br />";
                break;

            case E_USER_NOTICE:
                $GLOBALS['notices'][] = "<b>AVISO</b> [$errno] $errstr<br />";
                break;

            default:
                $GLOBALS['errores_desconocidos'][] = "<b>ERROR DESCONOCIDO</b> [$errno] $errstr<br />";
                break;
        }

        /* Don't execute PHP internal error handler */
        return true;
    }

    //Recolecta los errores de los paramentros
    //para luego mostrarlos (es posible que esta funcion
    //se modifique o desaparezca cuando se cree el modulo
    //de parametros para cada una de las funciones)
    public function imprime_errores_campos($campos){
        
        //Instanciamos el resultado..
        $resultado = new Result();

        //Buscamos los errores
        foreach($campos as $key=>$campo)
            if(isset($campo->_errors))
                foreach($campo->_errors as $key1=>$error)
                    $resultado->setError($error);



        //Imprimimos los errores y salimos...
        if( !$resultado->getStatus() ){
            echo $resultado->getResult();
            exit;
        }
    }


    //Le pasamos un numero de cuit y lo valida
    public function validaCUIT($cuit){
        $cadena = str_split($cuit);

        if(count($cadena) == 11){

            $result = $cadena[0]*5;
            $result += $cadena[1]*4;
            $result += $cadena[2]*3;
            $result += $cadena[3]*2;
            $result += $cadena[4]*7;
            $result += $cadena[5]*6;
            $result += $cadena[6]*5;
            $result += $cadena[7]*4;
            $result += $cadena[8]*3;
            $result += $cadena[9]*2;

            $div = intval($result/11);
            $resto = $result - ($div*11);

            if($resto==0){
                if($resto==$cadena[10]){
                    return true;
                }else{
                    return false;
                }
            }elseif($resto==1){
                if($cadena[10]==9 AND $cadena[0]==2 AND $cadena[1]==3){
                    return true;
                }elseif($cadena[10]==4 AND $cadena[0]==2 AND $cadena[1]==3){
                    return true;
                }
            }elseif($cadena[10]==(11-$resto)){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }


    //Le pasamos un numero de cuit y busca
    //en la pagina cuitonline la razon social.
    public function getRZ($cuit){
        //Consultamos la razon social a cuitonline.com
        $url = "http://www.cuitonline.com/search.php?q=".$cuit;
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
        $html = curl_exec($curl);
        curl_close($curl);


        //Creamos un buffer para evitar WARNINGS y NOTICES de 'loadHTML'
        $previous_value = libxml_use_internal_errors(TRUE);

        //Obtenemos el contenido de el div 'denominacion'
        $classname = 'denominacion';
        $dom = new DOMDocument;
        
        if(isset($html) && $html != ''){
            $dom->loadHTML($html);
            $xpath = new DOMXPath($dom);
            $results = $xpath->query("//*[@class='" . $classname . "']");

            //Limpiamos errores
            libxml_clear_errors();
            libxml_use_internal_errors($previous_value);

            //Si hubo resultados...
            if ( (int)$results->length == 0) {
                return 'Razon Social no encontrada';
            }else{
                return trim($results->item(0)->nodeValue); 
            }

        //No hay codigo HTML
        }else{
            return 0;
        }
    }


    //Le pasamos una tabla con todos los rubros y un id
    //y nos devuelve un arreglo con el id de todos los
    //rubros ancestros
    public function traeRubrosPadres($rubros, $id_rub){
        
        //arreglo que devolveremos
        $padres = array();
        
        //Recorremos el arreglo en busca del padre
        foreach($rubros as $key=>$value){
            if($value['id'] == $id_rub){
                if($value['padre'] != '0')
                    $padres = array_merge($this->traeRubrosPadres($rubros,$value['padre']),(array)$value['padre']);
            }
        }

        //Devolvemos los padres
        return $padres;
    }


    //Le pasamos la tabla completa de los rubros y un
    //id de un rubro y nos devuelve un arreglo con todos
    //los hijos del rubro que le pasamos. Si no tiene hijos
    //nos devuelve el arreglo vacio
    public function traeRubrosHijos($rubros, $id_rub){
        
        //arreglo que devolveremos
        $hijos = array();
        
        //Recorremos el arreglo en busca del padre
        foreach($rubros as $key=>$value){
            if($value['padre'] == $id_rub){
                    //Add actual rubro
                    $hijos = array_merge( $hijos, (array)$value['id']);
                    //Add children rubros
                    $hijos = array_merge( $this->traeRubrosHijos($rubros,$value['id']), $hijos );
            }
        }

        //Devolvemos los padres
        return $hijos;
    }


    //Ordena el arreglo completo de los rubros 
    //en forma de arbol
    public function orderRubros($rubros){

        //Set padres
        foreach($rubros as $k=>$r){
            $rubros[$k]['padres'] = $this->traeRubrosPadres($rubros, $r['id']);

            //Modify name
            for($k1 = count($rubros[$k]['padres'])-1 ; $k1 >= 0 ; $k1--) {
                if($rubros[$k]['padres'][$k1] != 1)
                    foreach($rubros as $k2=>$r1)
                        if($r1['id'] == $rubros[$k]['padres'][$k1])
                            $rubros[$k]['nombre'] =  $rubros[$k2]['nombre'] . ' ' . $rubros[$k]['nombre'];
                
            }

            //Insertamos Hijos
            $rubros[$k]['hijos'] = $this->traeRubrosHijos($rubros, $rubros[$k]['id']);
        }

        return $rubros;
    }


    //A esta funcion le pasamos el arreglo completo de rubros y un id
    //y nos devuelve el Breadcrumb o la ruta del rubro.
    public function getBreadcrumbRubros($rubros, $id=0){

        // Si el valor que nos pasan del id
        // es null, seteamos el bread en ''
        // y salteamos todo el proceso
        if(is_null($id)) $bread = '';
        else{

            //Obtenemos el id del rubro
            //que necesitamos devolver 
            //el breadcrumb
            foreach ($rubros as $k=>$r)
                if( $r['id'] == $id )
                    break;

            //incializamos las variables
            $bandera = true;
            $bread = $r['nombre'];
            if($r['padre'] == 1) $bandera = false;

            //recorremos armando el bread
            while($bandera){

                //Agregamos el nombre del padre al string
                foreach ($rubros as $k=>$r1)
                    if( $r['padre'] == $r1['id'] ){
                        $bread = $r1['nombre'] . ' ' . $bread;
                        $r['padre'] = $r1['padre'];
                        break;
                    }

                //Chequeamos si llegamos al limite
                if($r['padre'] == 1) $bandera = false;
            }
        }
        
        return $bread;
    }

    // Filtramos uns string quitandole acentos
    public function stripAccents($cadena){
        $originales = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿŔŕ';
        $modificadas = 'AAAAAAACEEEEIIIIDNOOOOOOUUUUYbsaaaaaaaceeeeiiiidnoooooouuuyybyRr';
        $cadena = utf8_decode($cadena);
        $cadena = strtr($cadena, utf8_decode($originales), $modificadas);
        //$cadena = strtolower($cadena);
        return utf8_encode($cadena);
    }

    //Igual a la funcion nativa de PHP in_array
    //pero pasandole un arreglo asociativo y su key
    function is_in_array($array, $key, $key_value){
          $within_array = 'no';
          foreach( $array as $k=>$v ){
            if( is_array($v) ){
                $within_array = is_in_array($v, $key, $key_value);
                if( $within_array == 'yes' ){
                    break;
                }
            } else {
                    if( $v == $key_value && $k == $key ){
                            $within_array = 'yes';
                            break;
                    }
            }
          }
          return $within_array;
    }

    //Le pasamos un string y un string con caracteres
    //valodos y devuelve el string original filtrado
    public function filtrar($string,$pattern){
        $pattern = "/[^".preg_quote($pattern, "/")."]/u";
        return preg_replace($pattern, "", $string); 
    }

    // Le pasamos prefijo, codigo, sufijo y Tabla de marcas
    // Nos devuelve un arreglo con id y nombre de la marca
    // al que pertenece el articulo.
    // Si no encontro nada devuelve falso
    public function getMarcaFromCodigo($prefijo, $codigo, $sufijo, $marcas){
        
        //El codigo es numerico?
        $cod_num = ctype_digit($codigo);

        //recorremos las marcas
        foreach($marcas as $k=>$m){
            //si usa codigos y a demas coinciden los prefijos y sufijos
            if( ($m['usa_codigos'] == 'Y') && ( ($prefijo == $m['prefijo']) || is_null($m['prefijo']) ) && ( ($sufijo == $m['sufijo']) || is_null($m['sufijo']) ) ){
                
                //Si tenemos que chequear ademas por maximo y minimo y el codigo lo permite
                if( (!is_null($m['codigo_min']) || !is_null($m['codigo_max'])) && $cod_num ){

                    //si debemos chequear minimo o maximo
                    if(     ( (!is_null($m['codigo_min']) && ((int)$codigo >= $m['codigo_min'])) || is_null($m['codigo_min']) ) &&
                            ( (!is_null($m['codigo_max']) && ((int)$codigo <= $m['codigo_max'])) || is_null($m['codigo_max']) )     ){

                        //Si no tiene padre, lo encontramos!
                        if($m['padre'] == 1){
                            return array( "id"=>$m['id'], "nombre"=>$m['nombre'] );
                        
                        //Si tiene padre, lo buscamos y lo devolvemos
                        }else{
                            foreach ($marcas as $k1=>$m1)
                                if($m1['id'] == $m['padre'])
                                    return array( "id"=>$m1['id'], "nombre"=>$m1['nombre'] );
                        }
                    }

                //O si no hace falta chequear minimos y maximos
                }elseif( is_null($m['codigo_min']) && is_null($m['codigo_max'])){

                    //Si no tiene padre, lo encontramos!
                    if($m['padre'] == 1){
                        return array( "id"=>$m['id'], "nombre"=>$m['nombre'] );
                    
                    //Si tiene padre, lo buscamos y lo devolvemos
                    }else{
                        foreach ($marcas as $k1=>$m1)
                            if($m1['id'] == $m['padre'])
                                return array( "id"=>$m1['id'], "nombre"=>$m1['nombre'] );
                    }
                }
            }
        }

        //Si llegamos a este punto es porque no encontramos marca coincidente
        return false;
    }

    //marca de agua
    function watermark($img, $dest){
        //Watermark image
        $watermark = $GLOBALS['conf']['img_upload_watermark_path'];
        
        // Creamosel watermark desde archivo
        $wm = imagecreatefrompng($watermark);
        
        // Obtenemos tamaño del watermark
        $wm_size = getimagesize($watermark);
        
        // Creamos una imagen desde la original
        $image = imagecreatefromjpeg($img);

        // Obtenemos tamaño de imagen original
        $size = getimagesize($img);
        // placing the watermark 10px from bottom and right
        //$dest_x = $size[0] - $wm_size[0] - 10;  
        //$dest_y = $size[1] - $wm_size[1] - 10;
        $dest_x = 0;  
        $dest_y = 0;
        
        // Ensimamos ambas capas
        imagealphablending($image, true);
        imagealphablending($wm, true); 
        
        // Creamos la nueva imagen
        if(!imagecopy($image, $wm, $dest_x, $dest_y, 0, 0, $wm_size[0] , $wm_size[1]))
            return false;

        // Guardamos el JPG en el nuevo destino
        if(!imagejpeg($image, $dest, $GLOBALS['conf']['img_upload_output_quality']))
            return false;

        //Liberamos la memoria
        imagedestroy($image);
        imagedestroy($wm);

        //Todo ok!
        return true;
    }

    //Escalar uma imagen
    function resizeImage($url, $width, $height, $url_out, $keep_ratio = true){

        if($height <= 0 && $width <= 0)
            return false;
        else{

            copy($url, $url_out);
            $info = getimagesize($url);
            $image = '';
            $final_width = 0;
            $final_height = 0;
            list($width_old, $height_old) = $info;
            if($keep_ratio){
              if($width == 0)
                $factor = $height/$height_old;
              elseif($height == 0)
                $factor = $width/$width_old;
              else
                $factor = min($width / $width_old, $height / $height_old);
              $final_width = round( $width_old * $factor );
              $final_height = round( $height_old * $factor );
            }else{
              $final_width = ($width <= 0) ? $width_old : $width;
              $final_height = ($height <= 0) ? $height_old : $height;
            }

            switch($info[2]){
              case IMAGETYPE_GIF:
                $image = imagecreatefromgif($url_out);
                break;
              case IMAGETYPE_JPEG:
                $image = imagecreatefromjpeg($url_out);
                break;
              case IMAGETYPE_PNG:
                $image = imagecreatefrompng($url_out);
                break;
              default:
                return false;
            }

            $image_resized = imagecreatetruecolor($final_width, $final_height);
            if($info[2] == IMAGETYPE_GIF || $info[2] == IMAGETYPE_PNG){
              $transparency = imagecolortransparent($image);
              if($transparency >= 0){
                $transparent_color = imagecolorsforindex($image, $trnprt_indx);
                $transparency = imagecolorallocate($image_resized, $trnprt_color['red'], $trnprt_color['green'], $trnprt_color['blue']);
                imagefill($image_resized, 0, 0, $transparency);
                imagecolortransparent($image_resized, $transparency);
              }
              elseif($info[2] == IMAGETYPE_PNG){
                imagealphablending($image_resized, false);
                $color = imagecolorallocatealpha($image_resized, 0, 0, 0, 127);
                imagefill($image_resized, 0, 0, $color);
                imagesavealpha($image_resized, true);
              }
            }

            $resultado = imagecopyresampled($image_resized, $image, 0, 0, 0, 0, $final_width, $final_height, $width_old, $height_old);
            
            //Si no se pudo copiar volvemos
            if($resultado == false) return false;
            
            switch($info[2]){
              case IMAGETYPE_GIF:
                $resultado = imagegif($image_resized, $url_out, 100);
                break;
              case IMAGETYPE_JPEG:
                $resultado = imagejpeg($image_resized, $url_out, 100);
                break;
              case IMAGETYPE_PNG:
                $resultado = imagepng($image_resized, $url_out);
                break;
              default:
                return false;
            }

            return $resultado;

            imagedestroy($image_resized);
            return true;
          }
    }

    //Convertir imagen PNG a JPG
    function png2jpg($originalFile, $outputFile, $quality) {
        $source = imagecreatefrompng($originalFile);
        $image = imagecreatetruecolor(imagesx($source), imagesy($source));

        $white = imagecolorallocate($image, 255, 255, 255);
        imagefill($image, 0, 0, $white);

        imagecopy($image, $source, 0, 0, 0, 0, imagesx($image), imagesy($image));

        imagejpeg($image, $outputFile, $quality);
        imagedestroy($image);
        imagedestroy($source);
    }

    //Convertir imagen GIF a JPG
    function gif2jpg($originalFile, $outputFile, $quality) {
        $source = imagecreatefromgif($originalFile);
        $image = imagecreatetruecolor(imagesx($source), imagesy($source));

        $white = imagecolorallocate($image, 255, 255, 255);
        imagefill($image, 0, 0, $white);

        imagecopy($image, $source, 0, 0, 0, 0, imagesx($image), imagesy($image));

        imagejpeg($image, $outputFile, $quality);
        imagedestroy($image);
        imagedestroy($source);
    }

    //Pasamos por paraemtros lista de marcas, 
    //prefijo sufijo y codigo y nos devuelve 
    //id_marca y marca_label
    function idMarca($marcas,$prefijo,$sufijo,$codigo){
        
        $resultado['padre'] = 'NULL';
        $resultado['nombre'] = 'No se encontraron Marcas coincidentes.';
        
        //var_dump($marcas);

        //Recorremos el arreglo de marcas y nos fijamos en cual cabe
        foreach($marcas as $key=>$marca){

            //Vamos haciendo todas las pruebas y si las pasa todas walla...

            //Si existe un prefijo determinado y el articulo no coincide cortamos...
            if( ($marca['prefijo'] != 'NULL') && ($prefijo != $marca['prefijo']))
                continue;

            //Si el prefijo esta definido y no coincide con el prefijo de larticulo cortamos...
            if( ($marca['sufijo'] != 'NULL') && ($sufijo != $marca['sufijo']))
                continue;

            //Si existe un maximo, el codigo del articulo no es numerico cortamos
            if( ($marca['codigo_max'] != 'NULL') && !ctype_digit($codigo))
                continue;

            //Si existe un minimo, el codigo del articulo no es numerico cortamos
            if( ($marca['codigo_min'] != 'NULL') && ctype_digit($codigo))
                continue;
     
            //Si existe un maximo, el codigo del articulo es numerico y se pasa de ese maximo cortamos
            if( ($marca['codigo_max'] != 'NULL') && ctype_digit($codigo) && ( (int)$codigo > (int)$marca['codigo_max'] ))
                continue;

            //Si existe un minimo, el codigo del articulo es numerico y es mas pequeño que el minimo cortamos
            if( ($marca['codigo_min'] != 'NULL') && ctype_digit($codigo) && ( (int)$codigo < (int)$marca['codigo_min'] ))
                continue;

            $resultado = $marca;
            break;
        }
        
        //devolvemos el resultado
        return $resultado;
    }

    //Zipea directorio
    function Zip($source, $destination){
        set_time_limit(0);

        if (!extension_loaded('zip') || !file_exists($source)) {
            return false;
        }

        $zip = new ZipArchive();
        if (!$zip->open($destination, ZIPARCHIVE::CREATE)) {
            return false;
        }

        $source = str_replace('\\', '/', realpath($source));

        if (is_dir($source) === true){

            $files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($source), RecursiveIteratorIterator::SELF_FIRST);

            foreach ($files as $file)
            {
                $file = str_replace('\\', '/', $file);

                // Ignore "." and ".." folders
                if( in_array(substr($file, strrpos($file, '/')+1), array('.', '..')) )
                    continue;

                $file = realpath($file);

                if (is_dir($file) === true)
                {
                    $zip->addEmptyDir(str_replace($source . '/', '', $file . '/'));
                }
                else if (is_file($file) === true)
                {
                    $zip->addFromString(str_replace($source . '/', '', $file), file_get_contents($file));
                }
            }
        }
        else if (is_file($source) === true)
        {
            $zip->addFromString(basename($source), file_get_contents($source));
        }

        return $zip->close();
    }

    //Backup DB
    function backup(){

        //Apliamos el limite de memoria para generar backup
        ini_set('memory_limit','2048M');
        set_time_limit(0);

        //Calcula nombre
        $now = new DateTime();
        $nombreBackup = $now->format('d-m-Y_H-i');
        $absoluteBackupPathTmp = str_replace('\\', '/', dirname(__FILE__)) ."/tmp/";

        //GENERAMOS BACKUP SQL
        system("C:/xampp/mysql/bin/mysqldump -u root libertad > ".$absoluteBackupPathTmp.$nombreBackup.".sql");

        //Creamos zip
        $zip = new ZipArchive();
        if ($zip->open( './tmp/'.$nombreBackup.'.zip', ZIPARCHIVE::CREATE) != TRUE)
            die ("Could not open archive");

        //Agregamos el archivo sql
        $zip->addFile( './tmp/'.$nombreBackup.'.sql', $nombreBackup.'.sql');

        //Cerramos el zip
        $zip->close();

        //Creamos zip final
        $zip = new ZipArchive();
        if ($zip->open( './backups/'.$nombreBackup.'.zip', ZIPARCHIVE::CREATE) != TRUE)
            die ("Could not open archive");

        //Agregamos el SQL comprimido
        $zip->addFile( './tmp/'.$nombreBackup.'.zip', 'sql.zip');

        //Cerramos el zip
        $zip->close();

        $GLOBALS['toolbox']->Zip('./articulos/', './backups/'.$nombreBackup.'.zip');

        //Borramos sql
        if(file_exists('./tmp/'.$nombreBackup.'.sql')) @unlink('./tmp/'.$nombreBackup.'.sql');

        //Borramos zip
        if(file_exists('./tmp/'.$nombreBackup.'.zip')) @unlink('./tmp/'.$nombreBackup.'.zip');
    }

    //Borrar carpeta entera
    function delete_files($target) {
        if(is_dir($target)){
            $files = glob( $target . '*', GLOB_MARK ); //GLOB_MARK adds a slash to directories returned
            
            foreach( $files as $file )
            {
                $GLOBALS['toolbox']->delete_files($file);     
            }
            //borrar
            if(file_exists($target))
                rmdir( $target );
        } elseif(is_file($target)) {
            unlink( $target );  
        }
    }

    //mime type
    function mime_content_type($filename) {

        $mime_types = array(

            'txt' => 'text/plain',
            'htm' => 'text/html',
            'html' => 'text/html',
            'php' => 'text/html',
            'css' => 'text/css',
            'js' => 'application/javascript',
            'json' => 'application/json',
            'xml' => 'application/xml',
            'swf' => 'application/x-shockwave-flash',
            'flv' => 'video/x-flv',

            // images
            'png' => 'image/png',
            'jpe' => 'image/jpeg',
            'jpeg' => 'image/jpeg',
            'jpg' => 'image/jpeg',
            'gif' => 'image/gif',
            'bmp' => 'image/bmp',
            'ico' => 'image/vnd.microsoft.icon',
            'tiff' => 'image/tiff',
            'tif' => 'image/tiff',
            'svg' => 'image/svg+xml',
            'svgz' => 'image/svg+xml',

            // archives
            'zip' => 'application/zip',
            'rar' => 'application/x-rar-compressed',
            'exe' => 'application/x-msdownload',
            'msi' => 'application/x-msdownload',
            'cab' => 'application/vnd.ms-cab-compressed',

            // audio/video
            'mp3' => 'audio/mpeg',
            'qt' => 'video/quicktime',
            'mov' => 'video/quicktime',

            // adobe
            'pdf' => 'application/pdf',
            'psd' => 'image/vnd.adobe.photoshop',
            'ai' => 'application/postscript',
            'eps' => 'application/postscript',
            'ps' => 'application/postscript',

            // ms office
            'doc' => 'application/msword',
            'rtf' => 'application/rtf',
            'xls' => 'application/vnd.ms-excel',
            'ppt' => 'application/vnd.ms-powerpoint',

            // open office
            'odt' => 'application/vnd.oasis.opendocument.text',
            'ods' => 'application/vnd.oasis.opendocument.spreadsheet',
        );

        $ext = strtolower(pathinfo($filename, PATHINFO_EXTENSION));
        if (array_key_exists($ext, $mime_types)) {
            return $mime_types[$ext];
        }
        elseif (function_exists('finfo_open')) {
            $finfo = finfo_open(FILEINFO_MIME);
            $mimetype = finfo_file($finfo, $filename);
            finfo_close($finfo);
            return $mimetype;
        }
        else {
            return 'application/octet-stream';
        }
    }

    // COMBINA APLIACIONES
    // Esta funcion recibe un areglo con aplicaciones (tal cual aparecen en la DB)
    // Revisa todas las aplicaciones quita las duplicadas, combina años, si existen
    // versiones especificas de un modelo y ademas existe tambien el mismo modelo 
    // incluyendo 'todas las versiones' se especifica en el primer elemento del arreglo
    // devuelto. Si existe dicha situacion se devuelve 1, sino 0
    function combina_apps($apps){


        //Si existe un articulo 'Universal' quitamos todo el resto
        foreach ($apps as $k=>$app)
            if(isset($apps[$k]))
                if( is_null($apps[$k]['marca_id']) && is_null($apps[$k]['modelo_id']))
                    foreach ($apps as $x=>$app_n)
                        if(isset($apps[$x]))
                            if(($k != $x))
                                unset($apps[$x]);
        //recalculamos los indices
        $apps = array_values($apps);

        //Quitamos los articulos que son exactamente iguales
        foreach ($apps as $k=>$app)
            if(isset($apps[$k]))
                foreach ($apps as $x=>$app_n)
                    if(isset($apps[$x]))
                        if(($k != $x)   && ($apps[$x]['marca_id'] == $apps[$k]['marca_id'])
                                        && ($apps[$x]['modelo_id'] == $apps[$k]['modelo_id'])
                                        && ($apps[$x]['version_id'] == $apps[$k]['version_id'])
                                        && ($apps[$x]['anio_inicio'] == $apps[$k]['anio_inicio'])
                                        && ($apps[$x]['anio_final'] == $apps[$k]['anio_final']))
                                        unset($apps[$x]);
        //recalculamos los indices
        $apps = array_values($apps);


        // Combinamos los años de las aplicaciones que aplican a una marca
        // y todos sus modelos...
        foreach ($apps as $k=>$app)
            if(isset($apps[$k]))
                foreach ($apps as $x=>$app_n)
                    if(isset($apps[$x]))
                        if(($k != $x)   && ($app['marca_id'] == $app_n['marca_id'])
                                        && is_null($app['modelo_id'])
                                        && is_null($app_n['modelo_id'])
                                        && is_null($app['version_id']) 
                                        && is_null($app_n['version_id']) ){

                            //Si la app nueva tiene abierto el año de inicio
                            if((int)$app_n['anio_inicio'] == -1) $apps[$k]['anio_inicio'] = -1;

                            //Si la app nueva tiene abierto el año de final
                            if((int)$app_n['anio_final'] == -1) $apps[$k]['anio_final'] = -1;

                            //Detectamos el aniom as chico para anio_inicio
                            if( ((int)$app_n['anio_inicio'] != -1) && ((int)$app['anio_inicio'] != -1) && is_int($app['anio_inicio']) && ((int)$app_n['anio_inicio'] < (int)$app['anio_inicio'])) $apps[$k]['anio_inicio'] = $app_n['anio_inicio'];

                            //Detectamos el año mas grande para anio_final
                            if( ((int)$app_n['anio_final'] != -1) && ((int)$app['anio_final'] != -1) && is_int($app['anio_final']) && ((int)$app_n['anio_final'] < (int)$app['anio_final'])) $apps[$k]['anio_final'] = $app_n['anio_final'];

                            unset($apps[$x]);
                        }
        //recalculamos los indices
        $apps = array_values($apps);

        // Combinamos los años de las aplicaciones que aplican a una marca, modelo
        // y a una version especifica.
        foreach ($apps as $k=>$app)
            if(isset($apps[$k]))
                foreach ($apps as $x=>$app_n)
                    if(isset($apps[$x]))
                        if(($k != $x)   && ($app['marca_id'] == $app_n['marca_id'])
                                        && !is_null($app['marca_id']) && !is_null($app_n['marca_id']) 
                                        && ($app['modelo_id'] == $app_n['modelo_id'])
                                        && !is_null($app['modelo_id']) && !is_null($app_n['modelo_id'])
                                        && ($app['version_id'] == $app_n['version_id'])
                                        && !is_null($app['version_id']) && !is_null($app_n['version_id'])){


                            //Si la app nueva tiene abierto el año de inicio
                            if((int)$app_n['anio_inicio'] == -1) $apps[$k]['anio_inicio'] = -1;

                            //Si la app nueva tiene abierto el año de final
                            if((int)$app_n['anio_final'] == -1) $apps[$k]['anio_final'] = -1;

                            //Detectamos el aniom as chico para anio_inicio
                            if( ((int)$app_n['anio_inicio'] != -1) && ((int)$app['anio_inicio'] != -1) && is_int($app['anio_inicio']) && ((int)$app_n['anio_inicio'] < (int)$app['anio_inicio'])) $apps[$k]['anio_inicio'] = $app_n['anio_inicio'];

                            //Detectamos el año mas grande para anio_final
                            if( ((int)$app_n['anio_final'] != -1) && ((int)$app['anio_final'] != -1) && is_int($app['anio_final']) && ((int)$app_n['anio_final'] < (int)$app['anio_final'])) $apps[$k]['anio_final'] = $app_n['anio_final'];

                            unset($apps[$x]);
                        }
        //recalculamos los indices
        $apps = array_values($apps);

        // Combinamos los años de las aplicaciones que aplican a un modelo
        // y todas sus versiones.
        foreach ($apps as $k=>$app)
            if(isset($apps[$k]))
                foreach ($apps as $x=>$app_n)
                    if(isset($apps[$x]))
                        if(($k != $x)   && ($app['marca_id'] == $app_n['marca_id'])
                                        && ($app['modelo_id'] == $app_n['modelo_id'])
                                        && is_null($app['version_id']) 
                                        && is_null($app_n['version_id'])){

                            //Si la app nueva tiene abierto el año de inicio
                            if((int)$app_n['anio_inicio'] == -1) $apps[$k]['anio_inicio'] = -1;

                            //Si la app nueva tiene abierto el año de final
                            if((int)$app_n['anio_final'] == -1) $apps[$k]['anio_final'] = -1;

                            //Detectamos el aniom as chico para anio_inicio
                            if( ((int)$app_n['anio_inicio'] != -1) && ((int)$app['anio_inicio'] != -1) && is_int($app['anio_inicio']) && ((int)$app_n['anio_inicio'] < (int)$app['anio_inicio'])) $apps[$k]['anio_inicio'] = $app_n['anio_inicio'];

                            //Detectamos el año mas grande para anio_final
                            if( ((int)$app_n['anio_final'] != -1) && ((int)$app['anio_final'] != -1) && is_int($app['anio_final']) && ((int)$app_n['anio_final'] < (int)$app['anio_final'])) $apps[$k]['anio_final'] = $app_n['anio_final'];

                            unset($apps[$x]);
                        }
        //recalculamos los indices
        $apps = array_values($apps);

        //Si existe un 'Todos los modelos' quitamos
        //los modelos que sobran
        foreach ($apps as $k=>$app)
            if(isset($apps[$k]))
                if( !is_null($apps[$k]['marca_id']) && is_null($apps[$k]['modelo_id']))
                    foreach ($apps as $x=>$app_n)
                        if(isset($apps[$x]))
                            if(($k != $x) && ($apps[$k]['marca_id'] == $apps[$x]['marca_id']))
                                unset($apps[$x]);
        //recalculamos los indices
        $apps = array_values($apps);

        // Buscamos un modelo que incluya 'todas las versiones'.
        // Si lo encontramos, buscamos entre el resto de las apps
        // y si encontramos alguna que pertenezca a dicho modelo
        // seteamos en 1 la bandera en el elemento 0 del arreglo.
        $bandera = 0;
        foreach ($apps as $k=>$app)
            if( !is_null($apps[$k]['marca_id']) && !is_null($apps[$k]['modelo_id']) && is_null($apps[$k]['version_id']) )
                foreach ($apps as $x=>$app_n)
                    if( ($k != $x) &&   ($apps[$k]['marca_id'] == $apps[$x]['marca_id']) && 
                                        ($apps[$k]['modelo_id'] == $apps[$x]['modelo_id']) && 
                                        (!is_null($apps[$x]['modelo_id']) ) )
                                        $bandera = 1;
        array_unshift($apps, $bandera);
        $apps = array_values($apps);


        //Devolvemos el arreglo de aplicaciones
        //combinado y validado
        return $apps;
    }

    // Genera un indexado de los articulos para la
    // busqueda rapida
    function crawler(){
        //Corre por tiempo ilimitado
        set_time_limit(0);
        ini_set('mysql.connect_timeout', 300);
        ini_set('default_socket_timeout', 300);

        //Rubro - descripcion - marca - aplicacion
        $sql = "SELECT articulo_id as id, 
                       CONCAT( 
                            IF( (SELECT nombre_singular FROM catalogo_rubros WHERE id = (SELECT id_rubro FROM catalogo_articulos WHERE id = articulo_id)) <> '',
                                CONCAT(
                                    (SELECT nombre_singular FROM catalogo_rubros WHERE id = (SELECT id_rubro FROM catalogo_articulos WHERE id = articulo_id)), 
                                    ' '
                                ),
                                ''
                            ),
                            IF( (SELECT descripcion FROM catalogo_articulos WHERE id = articulo_id) <> '', 
                                CONCAT( (SELECT descripcion FROM catalogo_articulos WHERE id = articulo_id),' '),
                                ''
                            ),
                            (SELECT nombre FROM catalogo_marcas WHERE id = (SELECT id_marca FROM catalogo_articulos WHERE id = articulo_id)),
                            ' ',
                            IF( marca_id IS NOT NULL, 
                                CONCAT(
                                    (SELECT nombre FROM catalogo_autos_marca WHERE id = marca_id),
                                    IF(
                                        modelo_id IS NOT NULL,
                                        CONCAT(
                                            ' ',
                                            (SELECT nombre FROM catalogo_autos_modelo WHERE id = modelo_id),
                                            IF(
                                                version_id IS NOT NULL,
                                                CONCAT(
                                                    ' ',
                                                    (SELECT descripcion FROM catalogo_autos_versiones WHERE id = version_id)
                                                ),
                                                ''
                                            )
                                        ),
                                        ''
                                    ) 
                                ),
                            '')

         ) as descripcion,

        CONCAT(
                (SELECT prefijo FROM catalogo_articulos WHERE id = articulo_id),
                '-',
                (SELECT codigo FROM catalogo_articulos WHERE id = articulo_id),
                '-',
                (SELECT sufijo FROM catalogo_articulos WHERE id = articulo_id)
        ) as codigo_completo,

        (SELECT precio FROM catalogo_articulos WHERE id = articulo_id) as precio

          FROM catalogo_articulos_aplicaciones

        UNION(

        SELECT id,
               CONCAT( 
                    IF( ((SELECT nombre_singular FROM catalogo_rubros WHERE id = id_rubro) <> '') AND (id_rubro IS NOT NULL),
                        CONCAT(
                            (SELECT nombre_singular FROM catalogo_rubros WHERE id = id_rubro), 
                            ' '
                        ),
                        ''
                    ),

                    IF( descripcion <> '', 
                        CONCAT( descripcion,' '),
                        ''
                    ),

                    IF( id_marca IS NOT NULL, 
                        CONCAT( (SELECT nombre FROM catalogo_marcas WHERE id = id_marca),' '),
                        ''
                    )


         ) as descripcion,

        CONCAT(prefijo,'-',codigo,'-',sufijo) as codigo_completo,
        precio

        FROM catalogo_articulos WHERE id NOT IN (SELECT DISTINCT articulo_id FROM catalogo_articulos_aplicaciones)
        )";

    
        //Vaciamos resultados anteriores
        $stmt = $GLOBALS['conf']['pdo']->prepare("truncate catalogo_articulos_busqueda");
        $stmt->execute();


        //Obtenemos tabla de Marcas
        $stmt = $GLOBALS['conf']['pdo']->query($sql);
        $resultados = $stmt->fetchAll(PDO::FETCH_ASSOC);



        $search[] = " L/ caja junta homocinética - c/ triceta (c/ chapa) ";
        $replace[] = " L/ caja (c/ chapa)";

        $search[] = " L/ caja junta homocinética - c/ triceta ";
        $replace[] = " L/ caja ";

        $search[] = " L/ caja - l/ rueda junta homocinética - c/ triceta ";
        $replace[] = " ";

        $search[] = " L/ caja junta homocinética - Bendixweis ";
        $replace[] = " L/ caja ";

        $search[] = " L/ rueda junta homocinética - c/ triceta ";
        $replace[] = " L/ rueda ";

        $search[] = " L/ caja - rueda ";
        $replace[] = " ";

        $search[] = " L/ ";
        $replace[] = " l/ ";

        //Recorremos todos los resultados y correjimos abreviaturas
        foreach($resultados as $key=>$resultado){
            $resultados[$key]['descripcion'] =  str_replace("'", "''",  str_replace($search, $replace, $resultado['descripcion']) );
        }




        //ABREVIATURAS
        $search[] = "der.";
        $replace[] = "derecha derecho";

        $search[] = "izq.";
        $replace[] = "izquierda izquierdo";

        $search[] = "tras.";
        $replace[] = "trasera trasero";

        $search[] = "del.";
        $replace[] = "delantera delantero";

        $search[] = "sup.";
        $replace[] = "superior";

        $search[] = "inf.";
        $replace[] = "inferior";



        //Recorremos todos los resultados y correjimos abreviaturas de busqueda
        foreach($resultados as $key=>$resultado){
            $resultados[$key]['busqueda'] =  str_replace("'", "''", $GLOBALS['toolbox']->stripAccents(str_replace($search, $replace, $resultado['descripcion'])));
        }


        /* INICIALIZADORES */

        //sql inicial
        $sql = "INSERT INTO catalogo_articulos_busqueda (id, descripcion, busqueda, codigo_completo, precio) VALUES ";

        //Contador en 0
        $cont = 0;

        //Tope
        $cant_result = max(array_keys($resultados));

        //Recorremos los articulos
        foreach($resultados as $key=>$resultado){

            //Agregamos articulo...
            if($cont > 0) $sql .= ", "; 
            $sql .= "(".$resultado['id'].",'".$resultado['descripcion']."','".$resultado['busqueda']."','".$resultado['codigo_completo']."',".$resultado['precio'].")";

            //Sumamos uno..
            $cont++;

            //Si llegamos a 5000 rows o al final del arreglo
            if( ($cont == 1000) || ( $key == $cant_result ) ){

                //Insertamos los valores
                $stmt = $GLOBALS['conf']['pdo']->prepare($sql);
                $stmt->execute();

                //Reiniciamos sql
                $sql = "INSERT INTO catalogo_articulos_busqueda (id, descripcion, busqueda, codigo_completo, precio) VALUES ";
                //Ponemos el contador en 0
                $cont = 0;
            }
        }
    }

    //Le pasamos el codigo de error y nos devuelve el texto
    function getFileUploaderror($code){
        switch ($code) { 
            case UPLOAD_ERR_INI_SIZE: 
                $message = "El archivo subido exede la directiva 'upload_max_filesize' en php.ini."; 
                break; 
            case UPLOAD_ERR_FORM_SIZE: 
                $message = "El archivo subido exede la constante MAX_FILE_SIZE que fue definida en el formulario."; 
                break; 
            case UPLOAD_ERR_PARTIAL: 
                $message = "El archivo subido esta corrupto o subio a medias."; 
                break; 
            case UPLOAD_ERR_NO_FILE: 
                $message = "El archivo no fue subido."; 
                break; 
            case UPLOAD_ERR_NO_TMP_DIR: 
                $message = "No se encontró el directorio temporal."; 
                break; 
            case UPLOAD_ERR_CANT_WRITE: 
                $message = "Error al escribir el archivo al disco."; 
                break; 
            case UPLOAD_ERR_EXTENSION: 
                $message = "La subida fue cancelada por el servidor."; 
                break; 

            default: 
                $message = "Error desconocido"; 
                break; 
        } 

        return $message;
    }


    // PUNTUA UN ARREGLO SEGUN EL MATCH
    // Le pasamos un arreglo, el key contra el quedebemos hacer
    // las comparaciones y un arreglo de palabras clave.
    function matchingArray($items, $key, $keywords){

    	//Nos aseguramos que los keywords sean sin acentos y en minusculas
    	foreach ($keywords as $k=>$v) 
    		$keywords[$k] = strtolower($GLOBALS['toolbox']->stripAccents($v));

        // recorremos todos los elementos y le damos score
        // inicial de 0, luego mostraremos solo los > 0
        foreach($items as $k=>$v) $items[$k]['score'] = 0;

        // Sumamos uno al score si 
        // hay coincidencia
        foreach($items as $k=>$v)
            foreach($keywords as $w)
                //Le damos un punto si la palabra esta entre otras
                if( stripos( strtolower($GLOBALS['toolbox']->stripAccents($v[$key])),$w) !== false)
                    $items[$k]['score']++;


        // Sumamos uno al score si hay
        // coincidencia justa
        foreach($items as $k=>$v){
            $nombre = stripos(strtolower($GLOBALS['toolbox']->stripAccents($v[$key])),$w);
            $nombre = explode(' ', $nombre);
            //Por cada palabra clave
            foreach($keywords as $w)
                //Y por cada palabra del nombre...
                foreach($nombre as $x)
                    // Si la palabra buscada es exacta sumamos puntos
                    if( $w == $x )
                        // Score ++
                        $items[$k]['score']++;
    	}

    	//Ordenamos el arreglo por score
        usort($items, function($a, $b) {
            return ($a['score'] < $b['score']) ? 1 : -1;
        });

        //Borramos scores 0
        foreach($items as $k=>$v)
            if($v['score'] == 0)
                unset($items[$k]);

        //reindexamos
        array_values($items);

        return $items;
	}

    /**
    * Nos devuelve el error del parametro json
    */
    function json_error_msg($code){
        $_messages = array(
            JSON_ERROR_NONE => 'No error has occurred',
            JSON_ERROR_DEPTH => 'The maximum stack depth has been exceeded',
            JSON_ERROR_STATE_MISMATCH => 'Invalid or malformed JSON',
            JSON_ERROR_CTRL_CHAR => 'Control character error, possibly incorrectly encoded',
            JSON_ERROR_SYNTAX => 'Syntax error',
            JSON_ERROR_UTF8 => 'Malformed UTF-8 characters, possibly incorrectly encoded'
        );

        return $_messages[$code];
    }

    /**
    * Nos devuelve un score comparando dos
    * Strings
    */
    function compare_strings($busqueda, $str){

        //tanto a la busqueda como al string
        //Le quitamos todos los acentos ñ u 
        //otros caracteres raros
        $busqueda = $GLOBALS['toolbox']->stripAccents($busqueda);
        $str = $GLOBALS['toolbox']->stripAccents($str);
        //$busqueda = preg_replace("/[^A-Za-z0-9-]/", ' ', $busqueda);
        $str = preg_replace("/[^A-Za-z0-9-]/", ' ', $str);


        //Convertimos ambas a minusculas
        $busqueda = strtolower($busqueda);
        $str = strtolower($str);

        //Quitamos espacios dobles a ambos
        //strings para parsearlos correctamente
        while (strpos($busqueda, "  ")!==false)
            $busqueda = str_replace("  ", " ", $busqueda);

        while (strpos($str, "  ")!==false)
            $str = str_replace("  ", " ", $str);

        $str = str_replace(",", "", $str);

        //Tokens
        $busqueda_tokens = explode(' ', $busqueda);
        $str_tokens = explode(' ', $str);


        //Total de palabras de busqueda
        $total_palabras = count($busqueda_tokens);

        //Score
        $score = 0;

        //Le damos un punto mas por cada match
        //exacto entre cada palabra de la busqueda
        //y cada palabra del string.
        foreach($busqueda_tokens as $w)
            //Y por cada palabra del nombre...
            foreach($str_tokens as $x)
                // Si la palabra buscada es exacta sumamos puntos
                if($w == $x)
                    // Score ++
                    $score++;

        //A esta altura ya sabemos cuantas de todas
        //de cada una de las palabras de la busqueda
        //pegamos, entonces podemos calcular el porcentaje
        // de matching sobre el total de las palabras de la
        //busqueda...
        //$score += ($score * 100) / $total_palabras;

        //Si el match es exacto le damos un punto
        if($busqueda == $str) 
            $score++;

        //Le damos un punto si la palabra de
        //busqueda aparece entre alguna de las
        //del string.
        foreach($busqueda_tokens as $w)
            if(strstr($str,$w))
                $score++;

        return $score;
    }

    //Le pasamos un arreglo con los Rubros
    //devuelve el arrglo de rubros, agregando
    //un item mas que indica el nivel.
    function rubros_nivel($rubros){ 
        //Recorremos los Rubros
        foreach($rubros as $k=>$v){
            //Contador
            $nivel = 2;

            //Padre
            $padre = $v['padre'];

            //Mientras que el padre
            //no sea 1 aumentamos...
            while($padre != 1){
                //Level Up!
                $nivel++;
                //Buscamos siguiente padre
                foreach($rubros as $k1=>$v1)
                    if($v1['id'] == $padre){
                        $padre = $v1['padre'];
                        break;
                    }
            }

            //Guardamos el nivel
            $rubros[$k]['nivel'] = $nivel;
        }

        //Devolvemos los rubros
        return $rubros;
    }

    //Le pasamos un id de una funcion y nos devuelve
    //un array con todas las funciones dependientes de
    //esta que deberiamos de activar tambien.
    function getIdFuncionesDependientes($ids,$relaciones = NULL){

        //Si no tenemos el arreglo 
        //de relaciones lo consultamos 
        //a la DB
        if(is_null($relaciones)){
            $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT * FROM funciones_funciones");
            $stmt->execute();
            $relaciones = $stmt->fetchAll(PDO::FETCH_ASSOC);
        }

        $return = array();

        //Buscamos funciones que dependan...
        foreach($relaciones as $k=>$v)
            //Encontramos dependencia
            if($v['id'] == $ids)
                //Agregamos la dependencia
                $return[] = $v['funcion_id'];


        //Por cada una de las funciones
        //dependientes que encontramos
        //hacemos lo mismo (recursiva)
        $dependencias = array();
        foreach($return as $k=>$v){
            //Agergamos al arreglo de return
            //Todos los ids de las dependientes
            //de forma recursiva
            $dependencias = array_merge($GLOBALS['toolbox']->getIdFuncionesDependientes($v,$relaciones), $dependencias);
        }

        //Unimos las dependientes directas
        //con las colectadas por la recursividad
        $return = array_merge($return,$dependencias);

        //Quitamos items duplicados
        $return = array_unique($return);

        //Revolvemos ids
        return $return;

    }

    //Le pasamos:
    // -El id de la funcion de la cual queremos saber los permisos
    // -La lista de permisos propios y 
    public function get_permisos_funcion($id, $permisos, $relaciones, $funciones){
        //Array donde iremos
        //guardando los resultados
        $return = array();


        //Recorremos los permisos y agregamos
        //los propios de la funcion actual
        foreach($permisos as $k=>$v)
            if($v['id_funcion'] == $id){
                $permiso = array();
                $permiso['id'] = $v['id_api'];
                if(isset($v['label_api']))
                    $permiso['label'] = $v['label_api'];
                $permiso['heredado'] = 0;
                $return[] = $permiso;
            }
        

        //Funciones de las que depende la actual
        $relaciones_actuales = $GLOBALS['toolbox']->getIdFuncionesDependientes($id,$relaciones);


        //Buscmos entre los permisos aquellos que pertenecen
        //a alguna de las funciones relacionadas.
        foreach($permisos as $k=>$v) {
            //Si el id esta entre alguno
            //de los del arreglo de relaciones
            if(in_array((int)$v['id_funcion'], $relaciones_actuales)){
                $permiso = array();
                $permiso['id'] = $v['id_api'];
                if(isset($v['label_api']))
                    $permiso['label'] = $v['label_api'];
                $permiso['heredado'] = 1;
                $return[] = $permiso;
            }
        }

        return $return;
    }

    //Le pasamos el modulo y la accion de la API
    //y nos devuelve si tiene permiso o no el 
    //ROL / USUARIO de la session actual. 
    function check_permiso($modulo, $accion){

        //Obtenemos el Rol
        $rol = $GLOBALS['session']->getData('rol');

        //Si es desarrollador accedemos
        if($rol == -1) return 1;

        //Obtenemos el Usuario de la Session
        $usuario = $GLOBALS['session']->getData('usuario');

        //API
        $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT id, modulo, accion FROM api");
        $stmt->execute();
        $api =  $stmt->fetchAll(PDO::FETCH_ASSOC);

        //FUNCIONES
        $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT * FROM funciones ORDER BY grupo ASC, accion ASC");
        $stmt->execute();
        $funciones =  $stmt->fetchAll(PDO::FETCH_ASSOC);

        //PERMISOS API
        $stmt = $GLOBALS['conf']['pdo']->prepare("  SELECT 
                                                            id_funcion,
                                                            id_api,
                                                            (SELECT CONCAT(modulo_nombre, ' - ', accion_nombre) as label FROM api WHERE id = id_api) as label_api
                                                    
                                                    FROM funciones_api");
        $stmt->execute();
        $permisos_api =  $stmt->fetchAll(PDO::FETCH_ASSOC);


        //Obtenemos id de la accion
        foreach($api as $k=>$v)
            if( ($v['modulo'] == $modulo) && ($v['accion'] == $accion) ){
                $accion_id = $v['id'];
                break;
            }


        //Obtenemos las funciones que incluyen a este metodo
        $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT DISTINCT id_funcion FROM funciones_api WHERE id_api = ".$accion_id);
        $stmt->execute();
        $funciones_implicadas =  $stmt->fetchAll(PDO::FETCH_ASSOC);


        //Guadamos las 
        //Funciones
        $f = array();
        foreach($funciones_implicadas as $k=>$v)
            $f[] = $v['id_funcion'];

        //Obtenemos todas las relaciones
        //de las funciones

        //Obtenemos las funciones que incluyen a este metodo
        if(count($f)){
            $stmt = $GLOBALS['conf']['pdo']->prepare("  SELECT * 
                                                        FROM permisos_roles 
                                                        WHERE 
                                                            funcion_id IN (".implode(',', $f).") AND 
                                                            rol_id=".$rol);
            $stmt->execute();
            $permisos_roles = $stmt->fetchAll(PDO::FETCH_ASSOC);
        }

        //Si no se encontraron permisos definidos
        //para esta funcion y este rol, entonces
        //no tiene permisos, si si existen si hay permiso
        if(isset($permisos_roles) && count($permisos_roles)) $accede = true;
        else $accede = false;

        //echo $accion_id;

        //Si el usuario esta definido
        //chequeamos si existen permisos
        //especiales para esta accion 
        //definidos especificamente para este user
        //Si es asi pisamos los permisos anteriores
        if($usuario){

        }

        //buscamos las funciones que dependen
        //de la actual.
        if(0){
            //Para todas las funciones relacionadas de la actual
            //Buscamos todas las ids que debemos de activar...
            $funciones_activar = $GLOBALS['toolbox']->getIdFuncionesDependientes((int)$funcion_actual['id']);

            //Buscamos todos los permisos de los usuarios o roles que tienen
            //acceso a la funcion que estamos editando para darle permiso a todas
            //las funciones de las que depende esta accion
            $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT * FROM permisos_roles");
            $stmt->execute();
            $permisos_roles = $stmt->fetchAll(PDO::FETCH_ASSOC);

            //Que roles estan haciendo uso de esta funcion?
            foreach($permisos_roles as $k=>$v){
                //lista de funciones a agregar
                $f_act_temp = $funciones_activar;
                if($funcion_actual['id'] == $v['funcion_id']){
                    
                    //Para el Rol que acabamos de encontrar
                    //cuales son las funciones dependientes
                    //que falta agregar el permiso???
                    foreach($permisos_roles as $x=>$v1){
                        if(($v['rol_id'] == $v1['rol_id']) && (in_array((int)$v1['funcion_id'], $f_act_temp)) ){
                            if (($key = array_search((int)$v1['funcion_id'], $f_act_temp)) !== false) {
                                unset($f_act_temp[$key]);
                            }
                        }
                    }

                    //en el arreglo $f_act_temp tenemos todos permisos
                    //que debemos agregar a la funcion y rol actuales
                    if(count($f_act_temp)){
                        //escribimos en la base de datos
                        //los permisos para el rol
                        
                    }
                }
            }
        }

        //$permisos_func = $GLOBALS['toolbox']->get_permisos_funcion($accion_id, $permisos_api, $relaciones, $funciones);



        //Obtengamos todos los permisos de este usuario/rol...
        //$permisos = $GLOBALS['toolbox']->getPermisos($rol,$usuario);

        //Obtenemos el id de la Accion
        return $accede;
    }

    //Tenemos un arreglo guardado en la session del usuario
    //que recuperamos en GLOBALS['permisos_api']['modulo@accion'].
    //Si en algun momento cambiamos los permisos de los Roles o
    //de un usuario regeneramos todos los arreglos de los permisos
    //guardados en las sessiones del usuario del rol implicado
    //Esta funcion genera el arreglo de un Rol determinado
    function generaPermisosAPIRol($rol,$pedirDependencias = true){
        
        //Obtenemos API
        $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT id, modulo, accion FROM api ORDER BY modulo ASC, accion ASC");
        $stmt->execute();
        $api = $stmt->fetchAll(PDO::FETCH_ASSOC);

        //Obtenemos Funciones
        $stmt = $GLOBALS['conf']['pdo']->prepare(" SELECT * FROM funciones");
        $stmt->execute();
        $funciones = $stmt->fetchAll(PDO::FETCH_ASSOC);

        //Obtenemos Dependencias entre Funciones
        $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT * FROM funciones_funciones");
        $stmt->execute();
        $funciones_funciones = $stmt->fetchAll(PDO::FETCH_ASSOC);

        //Obtenemos los permisos del Rol a las Funciones
        $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT * FROM permisos_roles WHERE rol_id = ".$rol);
        $stmt->execute();
        $permisos_roles = $stmt->fetchAll(PDO::FETCH_ASSOC);
        
        //Obtenemos los permisos de las Funciones a la API
        $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT * FROM funciones_api");
        $stmt->execute();
        $funciones_api = $stmt->fetchAll(PDO::FETCH_ASSOC);



        //176234/5

        //Recorremos todos los permisos
        //asignados por el administrador al Rol.
        $permisos_funciones_api = array();
        foreach($permisos_roles as $k=>$pr){
            foreach($funciones as $x=>$f){
                //Buscamos la funcion a la que tiene
                //acceso este Rol.
                if($pr['funcion_id'] == $f['id']){
                    //Obtenemos los permisos que tiene esta funcion
                    $permisos_funciones_api = array_merge($permisos_funciones_api, $GLOBALS['toolbox']->get_permisos_funcion($f['id'], $funciones_api, $funciones_funciones, $funciones));
                }
            }
        }


        //Generamos el arreglo de
        //Permisos a la API
        $return = array();
        foreach($api as $k=>$v){

            //Necesitamos obtener el modulo y la accion
            //de la api para generar el arreglo de permisos
            $existe = false;
            foreach($permisos_funciones_api as $x=>$p){

                //Cuando encontramos el 
                if($p['id'] == $v['id']){
                    
                    //Si es propio
                    if(!$p['heredado']){
                        $existe = true;
                        $permisos_api[ $v['modulo'].'___'.$v['accion'] ] = 1;
                    }

                    //si es heredado, y pedimos los heredados
                    if($pedirDependencias && $p['heredado']){
                        $existe = true;
                        $permisos_api[ $v['modulo'].'___'.$v['accion'] ] = 1;
                    }

                    //Si encontramos el permiso
                    //salimos del for de permisos
                    break;    
                }
            }

            //Si no tenia permiso le negamos el acceso
            if(!$existe) $permisos_api[ $v['modulo'].'___'.$v['accion'] ] = 0;

        }

        //Devolvemos todos los
        //Permisos a la API
        return $permisos_api;
    }


    //Le pasamos 
    function generaPermisosAPIUsuario($usuario,$permisosRol = NULL){
        
        //Obtenemos API
        $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT id, modulo, accion FROM api ORDER BY modulo ASC, accion ASC");
        $stmt->execute();
        $api = $stmt->fetchAll(PDO::FETCH_ASSOC);

        //Obtenemos Funciones
        $stmt = $GLOBALS['conf']['pdo']->prepare(" SELECT * FROM funciones");
        $stmt->execute();
        $funciones = $stmt->fetchAll(PDO::FETCH_ASSOC);

        //Obtenemos Dependencias entre Funciones
        $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT * FROM funciones_funciones");
        $stmt->execute();
        $funciones_funciones = $stmt->fetchAll(PDO::FETCH_ASSOC);

        //Obtenemos los permisos del Rol a las Funciones
        $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT * FROM permisos_usuarios WHERE usuario_id = '".$usuario."'");
        $stmt->execute();
        $permisos_usuarios = $stmt->fetchAll(PDO::FETCH_ASSOC);

        //Obtenemos los permisos de las Funciones a la API
        $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT * FROM funciones_api");
        $stmt->execute();
        $funciones_api = $stmt->fetchAll(PDO::FETCH_ASSOC);

        //Obtenemos el Rol
        $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT rol_id FROM usuarios WHERE usuario='".$usuario."'");
        $stmt->execute();
        $rol = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $rol = $rol[0]['rol_id'];

        //Obtenemos los permisos del Rol a las Funciones
        $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT * FROM permisos_roles WHERE rol_id = ".$rol);
        $stmt->execute();
        $permisos_roles = $stmt->fetchAll(PDO::FETCH_ASSOC);

        //Si somos desarrolladores 
        //damos permiso a todo...
        if($rol == -1){
            $permisos_finales = array();
            foreach($api as $x=>$v) $permisos_finales[ $v['modulo'].'___'.$v['accion'] ] = 1;
            return $permisos_finales;
        }

        //Hacemos una copia de las funciones
        $funciones_ids = $funciones;

        //Negamos todo por defecto
        foreach($funciones_ids as $x=>$f)
            $funciones_ids[$x]['acceso'] = 0;

        //Le damos acceso según Rol
        foreach($permisos_roles as $k=>$p)
            foreach($funciones_ids as $x=>$f)
                if($p['funcion_id'] == $f['id'])
                    $funciones_ids[$x]['acceso'] = 1;

        //Buscamos los permisos que tiene el usuario
        foreach($permisos_usuarios as $k=>$p)
            foreach($funciones_ids as $x=>$f)
                if($p['funcion_id'] == $f['id'])
                    if($p['acceso']) $funciones_ids[$x]['acceso'] = 1;
                    else $funciones_ids[$x]['acceso'] = 0;



        //Ahora si recorremos todos las funciones
        //y buscamos los permisos propios y los
        //heredados de las funciones dependientes
        $permisos_API = array();
        foreach($funciones_ids as $k=>$f)
            if($f['acceso'])
                $permisos_API =   array_merge(
                                                $permisos_API, 
                                                $GLOBALS['toolbox']->get_permisos_funcion(
                                                    $f['id'], 
                                                    $funciones_api, 
                                                    $funciones_funciones, 
                                                    $funciones)
                                            );

        //Hacemos una copia de la API
        $permisos_finales = array();
        foreach($api as $k=>$a){
            
            //Agregamos este permiso por defecto
            //negado luego nos fijamos si tiene
            //acceso recorriendo el array de Permisos
            $permisos_finales[ $a['modulo'].'___'.$a['accion'] ] = 0;

            //Esta accion, tiene permiso?
            foreach($permisos_API as $x=>$v)
                if($a['id'] == $v['id'])
                    $permisos_finales[ $a['modulo'].'___'.$a['accion'] ] = 1;

        }


        //Devolvemos todos los
        //Permisos a la API
        return $permisos_finales;

    }




    /*
     Devuelve un arreglo asociativo, donde cada una de las
     claves de dicho array tienen el identificador de la funcion
     y su valor es booleano indicando si tiene acceso el usuario
     actuao o no a dicha funcion.
     EJ:
        $permisos['articulos_listar'] = true;
        $permisos['clientes_eliminar'] = false;
    */
    function getPermisosFunciones(){
        
        //FUNCIONES
        $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT id, identificador, 0 as acceso FROM funciones");
        $stmt->execute();
        $funciones =  $stmt->fetchAll(PDO::FETCH_ASSOC);

        //PERMISOS DEL ROL
        $rol = $GLOBALS['session']->getData('rol');
        $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT * FROM permisos_roles WHERE rol_id = ".$rol);
        $stmt->execute();
        $permisos_rol =  $stmt->fetchAll(PDO::FETCH_ASSOC);

        //PERMISOS USUARIO
        //Solo si el usuario esta seteado...
        if($GLOBALS['session']->getData('usuario')){
            $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT * FROM permisos_usuarios WHERE usuario_id = '".$GLOBALS['session']->getData('usuario')."'");
            $stmt->execute();
            $permisos_usuario =  $stmt->fetchAll(PDO::FETCH_ASSOC);
        }


        //Inyectamos los permisos del ROL
        foreach($funciones as $k=>$f)
            foreach($permisos_rol as $y=>$p)
                if( $f['id'] == $p['funcion_id'])
                    $funciones[$k]['acceso'] = 1;

        //Inyectamos los permisos del usuario
        //Si es que esta definido
        if(isset($permisos_usuario))
            foreach($funciones as $k=>$f)
                foreach($permisos_usuario as $y=>$p)
                    if( $f['id'] == $p['funcion_id'])
                        $funciones[$k]['acceso'] = (int)$p['acceso'];

        //Si es desarrollador 
        //todo permitido
        if($rol == -1)
            foreach($funciones as $k=>$f)
                $funciones[$k]['acceso'] = 1;


        //Definimos como variable GLOBAL los permisos
        $GLOBALS['permisos'] = array();
        foreach($funciones as $k=>$f)
            $GLOBALS['permisos'][ $f['identificador'] ] = (int)$f['acceso']; 
    }

    function menuCheckPerm(& $menuBranch){
        $activo = 0;
        
        //Par acada uno de los items
        foreach($menuBranch as $k=>$v){

            //Si es un subitem corremos
            //la funcion recursiva esta
            if(count($v['children']))
                if($GLOBALS['toolbox']->menuCheckPerm($v['children']))
                    $activo = 1;
            
            //Si es un item
            if(!count($v['children']))
                //Y si esta visible
                if($menuBranch[$k]['show'] == 1)
                    $activo = 1;
        }

        //devolvemos activo o no?
        return $activo;
    }

    //Funcion recursiva que fixea el menu dependiendo de los
    //pèrmisos que tenga el usuario, muestra los items padres
    //o no.
    function createMenu(& $menuBranch = NULL){

        //Si no nos pasaron parametros
        //Traemos el menu completo de la DB
        if(is_null($menuBranch)){

            //Obtenemos el menu de la base de datos
            $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT * FROM menu ORDER BY posicion");
            $stmt->execute();
            $menu = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $GLOBALS['menu'] = $GLOBALS['toolbox']->buildTree($menu);
            $GLOBALS['menu'] = $GLOBALS['menu'][0]['children'];

            //Asignamos al branch
            $menuBranch = & $GLOBALS['menu'];
        }

        //Recorrremos los items
        foreach($menuBranch as $k=>$v){
            
            //Si es un item y ademas no tiene permisos
            if( (trim($menuBranch[$k]['permiso']) == '') && 
                (!count($menuBranch[$k]['children'])) )
                $menuBranch[$k]['show'] = 0;

            //Si es un item y ademas tiene permisos
            if( (trim($menuBranch[$k]['permiso']) != '') && 
                (!count($menuBranch[$k]['children'])) )
                $menuBranch[$k]['show'] = eval($menuBranch[$k]['permiso']);

            //si es un subitem primero generamos permisos
            //para los hijos
            if( count($menuBranch[$k]['children']) ){
                //Corremos la funcion recursiva para los hijos
                $GLOBALS['toolbox']->createMenu( $menuBranch[$k]['children'] );
                //Para este item actual chequeamos si debemos
                //mostrarlo o no dependiendo si algunos de los
                //sibitems que contiene se muestra o no.
                $menuBranch[$k]['show'] = $GLOBALS['toolbox']->menuCheckPerm($menuBranch[$k]['children']);
            }
        }
    }

     //Esta funcion transforma el array anidado en el menu
     //HTML listo para ser mostrado en la UI del sistema
     function menuToHTML($branchMenu = NULL){
        
        //Si estamos llamando la 
        //funcion por primera vez
        $html = "";
        $pVez = is_null($branchMenu);
        if($pVez){
            $html =  '<div class="btn-group pull-right"><a class="menu_button pull-right fa fa-bars fa-2x" data-toggle="dropdown"></a>';
            $html .= '<ul class="dropdown-menu dropdown-menu-arrow agrega_flechita pull-left" role="menu" style="margin-top: 14px;">';
            $html .= '<li><strong style="display: block; text-align: center;">'.$GLOBALS['session']->getData('nombre').'</strong></li>';
            $html .= '<li class="divider"></li>';
            $datos = $GLOBALS['menu'];
        }else{
            $datos = $branchMenu;
        }


        //recorremos el menu
        foreach($datos as $k=>$v){

            //Si es item lo agregamos
            if(!count($v['children'])){
                if($v['show']){
                    $html .= '<li><a tabindex="-1" href="#" onclick="';
                    if(trim($v['click']) != '') $html .= $v['click'];
                    else $html .= 'event.preventDefault();';
                    $html .= '">'.$v['text'].'</a>';
                }
            }

            //Pero si es un submenu agregamos el principio del html
            //luego llamamos a esta funcion recursiva, y al final
            //añadimos el ultimo pedacito de html </ul>
            elseif(count($v['children'])){
                if($v['show']){
                    $html .= '<li class="dropdown-submenu"><a tabindex="-1" href="#" onclick="';
                    if(trim($v['click']) != '') $html .= $v['click'];
                    else $html .= 'event.preventDefault();';
                    $html .= '">'.$v['text'].'</a><ul class="dropdown-menu">';

                    //Llamamos a esta funcion recursiva para que genere
                    //Los subitems de este item
                    $html .= $GLOBALS['toolbox']->menuToHTML($v['children']);

                    //Agregamos el final del subitem
                    $html .= "</ul></li>";
                }
            }
        }

        //Si estamos llamando la 
        //funcion por primera vez
        //agregamos el ultimo pedazo
        //de codigo...
        if($pVez) $html .= "</ul></div>"; 

        return $html;
     }




    //Inserta un elemento en un index especifico del array
    function insertArrayIndex($array, $new_element, $index) {
         /*** get the start of the array ***/
         $start = array_slice($array, 0, $index); 
         /*** get the end of the array ***/
         $end = array_slice($array, $index);
         /*** add the new element to the array ***/
         $start[] = $new_element;
         /*** glue them back together and return ***/
         return array_merge($start, $end);
    }

    //Ordena arreglos Multidimensionales
    //EXCELENTE FUNCION!!!
    //http://stackoverflow.com/questions/96759/how-do-i-sort-a-multidimensional-array-in-php
    function make_comparer() {
        // Normalize criteria up front so that the comparer finds everything tidy
        $criteria = func_get_args();
        foreach ($criteria as $index => $criterion) {
            $criteria[$index] = is_array($criterion)
                ? array_pad($criterion, 3, null)
                : array($criterion, SORT_ASC, null);
        }

        return function($first, $second) use (&$criteria) {
            foreach ($criteria as $criterion) {
                // How will we compare this round?
                list($column, $sortOrder, $projection) = $criterion;
                $sortOrder = $sortOrder === SORT_DESC ? -1 : 1;

                // If a projection was defined project the values now
                if ($projection) {
                    $lhs = call_user_func($projection, $first[$column]);
                    $rhs = call_user_func($projection, $second[$column]);
                }
                else {
                    $lhs = $first[$column];
                    $rhs = $second[$column];
                }

                // Do the actual comparison; do not return if equal
                if ($lhs < $rhs) {
                    return -1 * $sortOrder;
                }
                else if ($lhs > $rhs) {
                    return 1 * $sortOrder;
                }
            }

            return 0; // tiebreakers exhausted, so $first == $second
        };
    }

    //Construye un arbol a partir de un array con parent y children
    function buildTree(array &$elements, $parentId = NULL) {
        $branch = array();

        //generamos el branch
        foreach($elements as $k=>$element){

            //Si este hijo pertenece al padre que pedimos
            if($element['parent'] == $parentId){

                //Guardamos los hijos de este 
                $element['children'] = $GLOBALS['toolbox']->buildTree($elements, $element['id']);

                //Unimos el hijo a la rama
                $branch[] = $element;
            }
        }

        //retornamos la rama
        return $branch;
    }


    /*---------------------------------
    function parentChildSort_r
    $idField        = The item's ID identifier (required)
    $parentField    = The item's parent identifier (required)
    $els            = The array (required)
    $parentID       = The parent ID for which to sort (internal)
    $result         = The result set (internal)
    $depth          = The depth (internal)
    ----------------------------------*/
    function parentChildSort_r($idField, $parentField, $els, $parentID = 0, &$result = array(), &$depth = 0){
        foreach ($els as $key => $value):
            if ($value[$parentField] == $parentID){
                $value['depth'] = $depth;
                array_push($result, $value);
                unset($els[$key]);
                $oldParent = $parentID; 
                $parentID = $value[$idField];
                $depth++;
                $GLOBALS['toolbox']->parentChildSort_r($idField,$parentField, $els, $parentID, $result, $depth);
                $parentID = $oldParent;
                $depth--;
            }
        endforeach;
        return $result;
    }

    //Genera string a partir de un parametro de la DB
    function generaFiltroParametro($p){

            //Armamos el arreglo para filtrar el parametro
            $filtro = '';

            //Mayusculas
            if($p['caracteres_mayusculas']) $filtro .= 'ABCDEFGHIJKMNLOPQRSTUVWXYZÑ';
            
            //Minusculas
            if($p['caracteres_minusculas']) $filtro .= 'abcdefghijkmnlopqrstuvwxyzñ';

            //Acentos en Mayusculas
            if($p['caracteres_mayusculas'] && $p['caracteres_acentos']) $filtro .= 'ÁÉÍÓÚÜ';

            //Acentos en Minusculas
            if($p['caracteres_minusculas'] && $p['caracteres_acentos']) $filtro .= 'áéíóúü';

            //Números
            if($p['caracteres_numeros']) $filtro .= '0123456789';

            //Corchetes
            if($p['caracteres_corchetes']) $filtro .= '[]';

            //Parentesis
            if($p['caracteres_parentesis']) $filtro .= '()';

            //Llaves
            if($p['caracteres_llaves']) $filtro .= '{}';

            //Espacios
            if($p['caracteres_espacios']) $filtro .= ' ';

            //Puntos
            if($p['caracteres_puntos']) $filtro .= '.';

            //Comas
            if($p['caracteres_comas']) $filtro .= ',';

            //Puntos y comas
            if($p['caracteres_puntosycomas']) $filtro .= ';';

            //Dos puntos
            if($p['caracteres_dospuntos']) $filtro .= ':';

            //Guion
            if($p['caracteres_guion']) $filtro .= '-';

            //Guion bajo
            if($p['caracteres_guionbajo']) $filtro .= '_';

            //Mas
            if($p['caracteres_mas']) $filtro .= '+';

            //Asterisco
            if($p['caracteres_asterisco']) $filtro .= '*';

            //Igual
            if($p['caracteres_igual']) $filtro .= '=';

            //Porcentual
            if($p['caracteres_porcentual']) $filtro .= '%';

            //Barra
            if($p['caracteres_barra']) $filtro .= '/';

            //Barra invertida
            if($p['caracteres_barrainvertida']) $filtro .= '\\';

            //Moneda
            if($p['caracteres_moneda']) $filtro .= '$';

            //Interrogacion
            if($p['caracteres_interrogacion']) $filtro .= '¿?';

            //Admiracion
            if($p['caracteres_admiracion']) $filtro .= '¡!';

            //Arroba
            if($p['caracteres_arroba']) $filtro .= '@';

            //Mayor y Menor
            if($p['caracteres_mayorymenor']) $filtro .= '<>';

            //Ampersand
            if($p['caracteres_ampersand']) $filtro .= '&';

            //Numeral
            if($p['caracteres_numeral']) $filtro .= '#';

            //Comillas simples
            if($p['caracteres_comillassimples']) $filtro .= '\'';

            //Comillas dobles
            if($p['caracteres_comillasdobles']) $filtro .= '"';

            //Saltos de linea
            if($p['caracteres_saltosdelinea']) $filtro .= "\r\n";

            //Filtro personalizado
            if(trim($p['caracteres_personalizado']) != '')
                $filtro .= trim($p['caracteres_personalizado']);

            //Quitamos algunos caracteres
            if(trim($p['caracteres_quitarcaracteres']) != '')
                foreach (str_split($p['caracteres_quitarcaracteres']) as $c)
                    $filtro = str_replace($c, '', $filtro);

            //Quitamos los caracteres duplicados
            //$filtro = count_chars( $filtro, 3);

            return $filtro;
    }

    //Genera Arreglo de parametros
    function generaParametrosAPI($modulo,$accion){

        //Inicializamos los parametros vacios
        $GLOBALS['parametros'] = array();

        //Obtenemos todos los parametros de este modulo@accion
        $stmt = $GLOBALS['conf']['pdo']->prepare("  SELECT  * 
                                                    FROM    parametros 
                                                    WHERE   id IN (
                                                            SELECT  parametro_id
                                                            FROM    parametros_api
                                                            WHERE   api_id = (
                                                                            SELECT  id 
                                                                            FROM    api 
                                                                            WHERE   (modulo ='".$modulo."') AND
                                                                                    (accion ='".$accion."')
                                                                            )
                                                    )");

        $stmt->execute();
        $parametros = $stmt->fetchAll(PDO::FETCH_ASSOC);

        //colector
        $colector = array();

        //Recorremos los parametros y 
        //armamos el arreglo en GLOBALS
        foreach($parametros as $k=>$p){

            //Inicializamos
            $parametro = array();
            $parametro['identificador'] = $p['identificador'];
            

            //Guardamos el valor

            if(isset( $_REQUEST[ $p['identificador'] ] )){
                $parametro['request'] = true;
                $parametro['value_original'] = $_REQUEST[ $p['identificador'] ];
            }else{
                //$parametro['request'] = $_REQUEST[ $p['identificador'] ];
                $parametro['request'] = false;
            }

            $parametro['value'] = '';

            //Si    es Obligatorio, 
            //      no esta seteado 
            //      no tenemos un valor por defecto
            //      
            if( !$parametro['request'] && $p['obligatorio'] && is_null($p['valorxdefecto'])){
                //$GLOBALS['resultado']->_debug[] = var_dump($parametro['value_original']);
                $parametro['error'] = ucfirst($p['label_articulo']). ' ' . ucfirst($p['label']) . ' es obligatorioy.';
                $colector[] = $parametro;
                //Continuamos con
                //el sig campo
                continue;
            }

            //Si nos pasaron algo por parametros lo guardamos en el value
            if($parametro['request']) $parametro['value'] = $parametro['value_original'];


            //Alunas veces, por ejemplo cuando estamos agergando a la DB
            //un email, debemos usar solo ciertos caracteres como letras,
            //numeros puntos y guiones arroba, etc. ademas de filtrar el string
            //quidando caracteres indeseados deberiamos avisarle al usuario
            //si ha usado algun caracter no permitido para que este al tanto
            //de el error que ha cometido y frenar la ejecucion de la accion
            //Antes armamos un string con los caracteres permitidos
            $filtro = $GLOBALS['toolbox']->generaFiltroParametro($p);
            if($p['validadores_errorenfiltro']){
                //generamos una copia del string original
                $tmp_val = $parametro['value_original'];
                
                //Y le vamos quitando los caracteres permitidos
                //al final si el string queda vacio, significa
                //que no existen caracteres invalidos.
                foreach(str_split($filtro) as $x=>$fc)
                    $tmp_val = str_replace($fc, '', $tmp_val);

                //Si todavia quedan caracteres...
                if(strlen($tmp_val)){
                    $parametro['error'] = ucfirst($p['label_articulo']) . ' ' . $p['label'] . ' contiene caracteres inválidos.';
                    $colector[] = $parametro;
                    //Continuamos con
                    //el sig campo
                    continue;
                }
            }


            //Ahora procedemos a filtrar el valor que nos pasaron por parametro
            if($filtro != '') $parametro['value'] = $GLOBALS['toolbox']->filtrar($parametro['value'],$filtro);
            else $parametro['value'] = '';


            //Primero apliamos todos los FILTROS UNIVERSALES
            //es decir no importa el tipo de dato con el que estemos
            //trabajando van a funcionar de forma universal.


            //Si debemos convertir todo a mayusculas...
            if($p['validadores_convertiramayusculas'])
                $parametro['value'] = strtoupper($parametro['value']);

            //Si debemos convertir todo a minusculas...
            if($p['validadores_convertiraminusculas'])
                $parametro['value'] = strtolower($parametro['value']);

            //Reemplazamos las tildes por caracteres comunes
            if($p['validadores_convertirtildes']) 
                $parametro['value'] = $GLOBALS['toolbox']->stripAccents($parametro['value']);

            //Si debemos validar la cantidad minima de caracteres del string
            if(!is_null($p['validadores_minimodecaracteres'])){
                //Si todavia quedan caracteres...
                if(strlen($parametro['value']) < (int)$p['validadores_minimodecaracteres']){

                    //Si debemos mostrar error al exederse de los caracteres permitidos
                    if($p['validadores_maximodecaracteresmuestraerror']){
                        $parametro['error'] = ucfirst($p['label_articulo']) . ' ' . $p['label'] . ' debe tener como mínimo '.$p['validadores_minimodecaracteres'].' caracteres.';
                        $colector[] = $parametro;
                        //Continuamos con
                        //el sig campo
                        continue;
                    }
                }
            }

            //Si debemos validar la cantidad máxima de caracteres del string
            if(!is_null($p['validadores_maximodecaracteres'])){
                //Se exede del máximo permitido??
                if(strlen($parametro['value']) > (int)$p['validadores_maximodecaracteres']){

                    //Si debemos mostrar error al exederse de los caracteres permitidos
                    if($p['validadores_maximodecaracteresmuestraerror']){
                        //Agregamos el error al listado
                        $parametro['error'] = ucfirst($p['label_articulo']) . ' ' . $p['label'] . ' debe tener como máximo '.$p['validadores_maximodecaracteres'].' caracteres.';
                        $colector[] = $parametro;
                        //Continuamos con
                        //el sig campo
                        continue;
                    
                    }else{
                        //Si no debemos de mostrar el error 
                        //solo truncamos el string y continuamos
                        $parametro['value'] = substr($empty, 0, (int)$p['validadores_maximodecaracteres']);
                    }
                }
            }

            //Chequeamos el valor contra una Expresion Regular
            if($p['validadores_regexp'] != ''){
                $regex = $p['validadores_regexp'];
                $resultado = preg_match($regex, $parametro['value'], $matches);

                //Si no tuvimos matches
                //Tiramos error de vuelta
                if(!$resultado){
                    $parametro['error'] = ucfirst($p['label_articulo']). ' ' . ucfirst($p['label']) . ' tiene un formato invalido (error: REGEXP3215).';
                    $colector[] = $parametro;
                    //Continuamos con
                    //el sig campo
                    continue;
                }else{
                    //Si tuvimos matches le asignamos el
                    //primero acertado...
                    $parametro['value'] = $matches[0];
                }
            }



            //Convertimos a arreglos los valores permitidos
            if($p['validadores_valorespermitidos'] != ''){
                //Convertimos JSON a arreglo para trabajar en PHP
                $p['validadores_valorespermitidos'] = json_decode($p['validadores_valoresespermitidos'],true);
                //Si no logramos obtener un array del string
                if(!is_array($p['validadores_valorespermitidos']))
                    $p['validadores_valorespermitidos'] = false;
            }else $p['validadores_valorespermitidos'] = false;

            //Convertimos a arreglos los valores NO PERMITIDOS
            if($p['validadores_valoresnopermitidos'] != ''){
                //Convertimos JSON a arreglo para trabajar en PHP
                $p['validadores_valoresnopermitidos'] = json_decode($p['validadores_valoresesnopermitidos'],true);
                //Si no logramos obtener un array del string
                if(!is_array($p['validadores_valoresnopermitidos']))
                    $p['validadores_valoresnopermitidos'] = false;
            }else $p['validadores_valoresnopermitidos'] = false;

            





            //Ahora que validamos todos los FILTROS UNIVERSALES
            //casteamos el parametro al tipo de dato que corresponda
            //y continuamos haciendole las validaciones que correspondan
            //a cada tipo de dato.




            //NÚMERO DECIMAL
            if($p['validadores_castearadecimal']){

                //A esta altura estamos esperando un STRING con formato de decimal
                //puede tener un punto o no tenerlo, si no tiene punto se lo agregamos.
                //Si sucede que no estamos recibiendo un valor numerico pero tenemos uno
                //por defecto, entonces hacemos uso de ese valor.
                //Si aun asi seguimos sin tener un valor y el campo es obligatorio
                //Tiramos error.

                //El valor no es numerico pero tenemos un valor x defecto para usar
                if(!is_numeric($parametro['value']) && !is_null($p['valorxdefecto']))
                    $parametro['value'] = $p['valorxdefecto'];

                //El valor no es numerico y ademas es obligatorio, tiramos error.
                if(!is_numeric($parametro['value']) && $p['obligatorio']){
                    //Agregamos el error al listado
                    $parametro['error'] = ucfirst($p['label_articulo']) . ' ' . $p['label'] . ' es un campo obligatorio.';
                    $colector[] = $parametro;
                    //Continuamos con
                    //el sig campo
                    continue;
                }

                //Convertimos la coma en punto
                $parametro['value'] = str_replace(',', '.', $parametro['value']);

                //Quitamos los puntos duplicados
                $pos = strpos($parametro['value'],'.');
                if($pos !== false)
                    $parametro['value'] = substr($parametro['value'],0,$pos+1) . str_replace('.','',substr($parametro['value'],$pos+1));

                //Por defecto dos decimales
                if(!(int)$p['validadores_decimales']) $p['validadores_decimales'] = 2;
                else $p['validadores_decimales'] = (int)$p['validadores_decimales'];

                //Convertimos el número en Decimal con los números de decimales deseados
                $parametro['value'] = number_format((float)$parametro['value'], $p['validadores_decimales']);



            //NÚMERO ENTERO
            }elseif($p['validadores_castearainteger']){

                //El valor no es numerico pero tenemos un valor x defecto para usar
                if(!is_numeric($parametro['value']) && !is_null($p['valorxdefecto']))
                    $parametro['value'] = $p['valorxdefecto'];

                //El valor no es numerico y ademas es obligatorio, tiramos error.
                if(!is_numeric($parametro['value']) && $p['obligatorio']){
                    //Agregamos el error al listado
                    $parametro['error'] = ucfirst($p['label_articulo']) . ' ' . $p['label'] . ' es un campo obligatorio.';
                    $colector[] = $parametro;
                    //Continuamos con
                    //el sig campo
                    continue;
                }

                //Casteamos a entero
                $parametro['value'] = (int)$parametro['value'];

            //JSON
            }elseif($p['validadores_castearajson']){
                //Casteamos
                $parametro['value'] = json_decode($parametro['value'], true);

                //En este caso estamos esperando un string en forma de JSON
                //Si no nos esta llegando un string JSON valido y tenemos 
                //disponible un valor por defecto, usamos ese valor
                if(!$parametro['value'] && !is_null($p['valorxdefecto']))
                    $parametro['value'] = json_decode($p['valorxdefecto'], true);

                //Si seguimos teniendo un JSON invalido
                //y el campo es obligatorio entonces no
                //podemos continuar con la ejecucion.
                if(!$parametro['value'] && $p['obligatorio']){
                    //Agregamos el error al listado
                    $parametro['error'] = ucfirst($p['label_articulo']) . ' ' . $p['label'] . ' es un campo obligatorio.';
                    $colector[] = $parametro;
                    //Continuamos con
                    //el sig campo
                    continue;
                }

                //Si el campo no es obligatorio y no pudimos recuperar
                //un valor correcto el resultado sera falso.


            //STRING
            }else{
                //Si tenemos un valor lo casteamos
                //a decimal, dandole formato correcto
                if(($parametro['value'] == '') && !is_null($p['valorxdefecto']))
                    $parametro['value'] = $p['valorxdefecto'];

                //El valor no es numerico y ademas es obligatorio, tiramos error.
                if( ($parametro['value'] == '') && $p['obligatorio']){
                    //Agregamos el error al listado
                    $parametro['error'] = ucfirst($p['label_articulo']) . ' ' . $p['label'] . ' es un campo obligatorioxp.';
                    $colector[] = $parametro;
                    //Continuamos con
                    //el sig campo
                    continue;
                }
            }


            //Valores especificos comparamos el valor
            //pasado por parametro contra un arreglo
            //en notacion json ['valor1','valor2',10,5.20]
            if($p['validadores_valorespermitidos'] && !$p['validadores_castearajson']){
                if(!in_array($parametro['value'], $p['validadores_valorespermitidos'])){
                    //Vaciamos el valor
                    $parametro['value'] = '';
                    //muestra error si es necesario y corta la ejecucion
                    if($p['validadores_valorespermitidosmuestraerror']){
                        $parametro['error'] = 'Los valores permitidos para '.$p['label_articulo']. ' ' . ucfirst($p['label']) . ' son "' . implode('" , "', $p['validadores_valorespermitidos']).'".';
                        $colector[] = $parametro;
                        //Continuamos con
                        //el sig campo
                        continue;
                    }
                }
            }

            //Valores especificos NO PERMITIDOS comparamos el valor
            //pasado por parametro contra un arreglo
            //en notacion json ['valor1','valor2',10,5.20]
            if($p['validadores_valoresnopermitidos'] && !$p['validadores_castearajson']){
                if(!in_array($parametro['value'], $p['validadores_valoresnopermitidos'])){
                    //Vaciamos el valor
                    $parametro['value'] = '';
                    //muestra error si es necesario y corta la ejecucion
                    if($p['validadores_valoresnopermitidosmuestraerror']){
                        $parametro['error'] = 'Los valores permitidos para '.$p['label_articulo']. ' ' . ucfirst($p['label']) . ' son "' . implode('" , "', $p['validadores_valoresnopermitidos']).'".';
                        $colector[] = $parametro;
                        //Continuamos con
                        //el sig campo
                        continue;
                    }
                }
            }


            //Si existe un minimo y estamos trabajando con Números
            if( ($p['validadores_minimo'] != '') && ($p['validadores_castearadecimal'] || $p['validadores_castearainteger'])) {

                //Si tiene un punto, lo convertimos a decimal
                if(strpos($p['validadores_minimo'],'.') !== false)
                    $p['validadores_minimo'] = (float)$p['validadores_minimo'];
                else
                    $p['validadores_minimo'] = (int)$p['validadores_minimo'];

                //Si esta fuera de rango tiramos error
                if($parametro['value'] < $p['validadores_minimo']){
                    
                    //Si el valor es mas pequeño que el minimo
                    //Seteamos el valor del parametro al minimo
                    $parametro['value'] = $p['validadores_minimo'];

                    //muestra error si es necesario y corta la ejecucion
                    if($p['validadores_minimomuestraerror']){
                        $parametro['error'] = 'El mínimo para '.$p['label_articulo']. ' ' . ucfirst($p['label']) . ' es ' . $p['validadores_minimo'];
                        $colector[] = $parametro;
                        //Continuamos con
                        //el sig campo
                        continue;
                    }
                }
            }

            //Si existe un minimo y estamos trabajando con Números
            if( ($p['validadores_maximo'] != '') && ($p['validadores_castearadecimal'] || $p['validadores_castearainteger'])) {

                //Si tiene un punto, lo convertimos a decimal
                if(strpos($p['validadores_maximo'],'.') !== false)
                    $p['validadores_maximo'] = (float)$p['validadores_maximo'];
                else
                    $p['validadores_maximo'] = (int)$p['validadores_maximo'];

                //Si esta fuera de rango tiramos error
                if($parametro['value'] < $p['validadores_maximo']){
                    
                    //Si el valor es mas pequeño que el minimo
                    //Seteamos el valor del parametro al minimo
                    $parametro['value'] = $p['validadores_maximo'];

                    //muestra error si es necesario y corta la ejecucion
                    if($p['validadores_maximomuestraerror']){
                        $parametro['error'] = 'El máximo para '.$p['label_articulo']. ' ' . ucfirst($p['label']) . ' es ' . $p['validadores_maximo'];
                        $colector[] = $parametro;
                        //Continuamos con
                        //el sig campo
                        continue;
                    }
                }
            }


            //Colectamos el parcial de este parametro
            $colector[] = $parametro;
        }

        //recorremos los resultados 
        //en busca de algun error
        $error = false;
        $GLOBALS['resultado']->_debug = $colector;
        //$GLOBALS['resultado']->setError('lala');
        //echo $GLOBALS['resultado']->getResult();
        //exit;
        foreach($colector as $k=>$p){
            $GLOBALS['parametros'][$p['identificador']] = $p['value'];
            if( isset($p['error'])){
                $error = true;
                $GLOBALS['resultado']->setError($error);
            }
        }

        //Cuando terminamos de colectar
        //Los errores si es que existen
        if($error){
            echo $GLOBALS['resultado']->getResult();
            exit;
        }
    }

    //Genera arreglo de parametros
    function generaParametrosGUMP($parametros){

        

        //Juntamos ambos arrays de parametros
        $_REQUEST = array_merge($_REQUEST, $_FILES);


        //Inicializamos los parametros vacios
        $GLOBALS['parametros'] = array();


        //Recorremos los parametros que
        //supuestamente deberiamos de
        //recibir en esta accion API
        foreach($parametros as $k=>$p){

            //Si directamente no nos pasaron el parametro
            //Lo ponemos como vacio
            if(!isset($_REQUEST[ $p['identificador'] ]))
               $_REQUEST[ $p['identificador'] ] = '';

            //Array de parametros para GUMP
            $parametros[$k]['GUMP'] = array();
            $parametros[$k]['GUMP']['validacion'] = array();
            $parametros[$k]['GUMP']['filtros'] = array();


            //VALIDADORES

            $filtro = str_replace(',', '_##coma##_', $GLOBALS['toolbox']->generaFiltroParametro($p));

            //Filtro caracteres
            if($p['validador_errorenfiltro']) 
                $parametros[$k]['GUMP']['validacion'][] = 'caracteres_permitidos,'. $filtro;

            //Validador de campo obligatorio
            if($p['obligatorio']) $parametros[$k]['GUMP']['validacion'][] = 'required';

            //Entero
            if($p['filtro_castearainteger']) $parametros[$k]['GUMP']['validacion'][] = 'integer';

            //Decimal
            if($p['filtro_castearadecimal']) $parametros[$k]['GUMP']['validacion'][] = 'float';

            //Email
            if($p['filtro_email']){
                $parametros[$k]['GUMP']['validacion'][] = 'valid_email';
                $parametros[$k]['GUMP']['filtros'][] = 'sanitize_email';
            } 

            //Minimo de caracteres
            if($p['validador_minimodecaracteres']) 
                $parametros[$k]['GUMP']['validacion'][] = 'min_len,'.$p['validador_minimodecaracteres'];

            //Maximo de caracteres
            if(!is_null($p['filtro_maximodecaracteres']) && $p['validador_maximodecaracteres']) 
                $parametros[$k]['GUMP']['validacion'][] = 'max_len,'.$p['filtro_maximodecaracteres'];

            //Valor minimo
            if(!is_null($p['filtro_minimo']) && $p['validador_minimo']) 
                $parametros[$k]['GUMP']['validacion'][] = 'min_numeric,'.$p['filtro_minimo'];
            
            //Valor maximo
            if(!is_null($p['filtro_maximo']) && $p['validador_maximo']) 
                $parametros[$k]['GUMP']['validacion'][] = 'max_numeric,'.$p['filtro_maximo'];

            //Filtro RegExp
            if(!is_null($p['filtro_regexp']) && $p['validador_regexp'])
                $parametros[$k]['GUMP']['validacion'][] = 'regexp,' . str_replace(',', '_##coma##_', $p['filtro_regexp']);

            //Filtro valores permitidos
            if(!is_null($p['filtro_valorespermitidos']) && $p['validador_valorespermitidos'])
                $parametros[$k]['GUMP']['validacion'][] = 'valores_permitidos,' . str_replace(',', '_##coma##_', $p['filtro_valorespermitidos']);

            //Filtro valores permitidos
            if(!is_null($p['filtro_valoresnopermitidos']) && $p['validador_valoresnopermitidos'])
                $parametros[$k]['GUMP']['validacion'][] = 'valores_no_permitidos,' . str_replace(',', '_##coma##_', $p['filtro_valoresnopermitidos']);

            //Filtro cantidad de decimales
            if( !is_null($p['filtro_decimales']) && $p['validador_decimales'] && $p['filtro_castearadecimal'])
                $parametros[$k]['GUMP']['validacion'][] = 'cantidad_dec,' . $p['filtro_decimales'];

            //Validador de JSON
            if($p['filtro_castearajson'])
                $parametros[$k]['GUMP']['validacion'][] = 'json';


            //Validaciones para ARCHIVOS

            //Filtramos el nombre a caracteres validos unicamente
            if(isset($_REQUEST[$p['identificador']]['name'])){
                
                //Separamos todo
                $adjunto = pathinfo($_REQUEST[$p['identificador']]['name']); 

                //Del nombre original retiramos todos los caratcteres que no queremos
                $_REQUEST[$p['identificador']]['name'] = preg_replace("/[^a-zA-Z0-9_\-]+/", "", $GLOBALS['toolbox']->stripAccents($adjunto['filename'])) . '.' . $adjunto['extension'];
            }

            //Validamos archivo obligatorio
            if($p['obligatorio'] && $p['filtro_castearaarchivo']) 
                $parametros[$k]['GUMP']['validacion'][] = 'required_file';

            //Validamos extensiones permitidas
            if($p['filtro_castearaarchivo'] && !is_null($p['filtro_archivo_extensiones'])) 
                $parametros[$k]['GUMP']['validacion'][] = 'extension,'.$p['filtro_archivo_extensiones'];

            //Validamos tamaño maximo
            if($p['filtro_castearaarchivo']){
                $peso = $p['filtro_archivo_peso'].'M';
                if(is_null($p['filtro_archivo_peso']))
                    $peso = ini_get('post_max_size');

                //Coremos filtro
                $parametros[$k]['GUMP']['validacion'][] = 'archivo_maxsize,'.$GLOBALS['toolbox']->return_bytes($peso);
            } 

            //Movemos el archivo al nuevo directorio
            if($p['filtro_castearaarchivo'] && !is_null($p['filtro_archivo_directorio'])){
                $parametros[$k]['GUMP']['validacion'][] = 'archivo_mover,'.$p['filtro_archivo_directorio'];
                $parametros[$k]['GUMP']['filtros'][] = 'archivo_mover,'.$p['filtro_archivo_directorio'];
            }




            //Filtro caracteres
            if(!$p['filtro_castearaarchivo'])
                $parametros[$k]['GUMP']['filtros'][] = 'caracteres_permitidos,'. $filtro;

            //Filtro caracteres
            if($p['filtro_convertirtildes'])
                $parametros[$k]['GUMP']['filtros'][] = 'stripAccents';

            //Filtro mayusculas
            if($p['filtro_convertiramayusculas'])
                $parametros[$k]['GUMP']['filtros'][] = 'upper';

            //Filtro minusculas
            if($p['filtro_convertiraminusculas'])
                $parametros[$k]['GUMP']['filtros'][] = 'lower';

            //Filtro valores permitidos
            if(!is_null($p['filtro_valorespermitidos'])){
                $jsonPermitidos = str_replace(',', '_##coma##_', $p['filtro_valorespermitidos']);
                $parametros[$k]['GUMP']['filtros'][] = 'allowedValues,'.$jsonPermitidos;
            }

            //Filtro valores no permitidos
            if(!is_null($p['filtro_valoresnopermitidos'])){
                $jsonNopermitidos = str_replace(',', '_##coma##_',$p['filtro_valoresnopermitidos']);
                $parametros[$k]['GUMP']['filtros'][] = 'removeValues,'.$jsonNopermitidos;
            }

            //Filtro expresion regular
            if(!is_null($p['filtro_regexp'])){
                $rexexp = str_replace(',', '_##coma##_',$p['filtro_regexp']);
                $rexexp = str_replace('|', '_##palo##_',$p['filtro_regexp']);
                $parametros[$k]['GUMP']['filtros'][] = 'regexp,'.$rexexp;
            }

            //Filtro casteamos a decimal
            if($p['filtro_castearadecimal']){
                //Por defecto dos decimales
                if(!(int)$p['filtro_decimales']) $p['filtro_decimales'] = 2;
                else $p['filtro_decimales'] = (int)$p['filtro_decimales'];
                $parametros[$k]['GUMP']['filtros'][] = 'toFloat,'.$p['filtro_decimales'];
            }

            //Filtro casteamos a entero
            if($p['filtro_castearainteger'])
                $parametros[$k]['GUMP']['filtros'][] = 'toInteger';

            //Filtro casteamos a entero
            if($p['filtro_castearajson'])
                $parametros[$k]['GUMP']['filtros'][] = 'toArray';

            //Valor minimo
            if(!is_null($p['filtro_minimo'])) 
                $parametros[$k]['GUMP']['filtros'][] = 'minimo,'.$p['filtro_minimo'];
            
            //Valor maximo
            if(!is_null($p['filtro_maximo'])) 
                $parametros[$k]['GUMP']['filtros'][] = 'maximo,'.$p['filtro_maximo'];

            //Valor maximo
            if(!is_null($p['filtro_maximodecaracteres'])) 
                $parametros[$k]['GUMP']['filtros'][] = 'max_caracteres,'.$p['filtro_maximodecaracteres'];

            //Filtrar comillas simples si las permitimos
            if($p['caracteres_comillassimples'])
                $parametros[$k]['GUMP']['filtros'][] = 'escapar_comillas_simples';

            //Filtrar comillas simples si las permitimos
            if($p['caracteres_barrainvertida'])
                $parametros[$k]['GUMP']['filtros'][] = 'escapar_barras_invertidas';

        }



        //Una vez que terminamos de crear 
        //el arreglo de validacion y filtro
        $reglas = $filtros = array();
        foreach($parametros as $k=>$param){


            //Si existen reglas
            if(count($param['GUMP']['validacion']))
                $reglas[ $param['identificador'] ] = implode('|', $param['GUMP']['validacion']);

            //Si existen filtros
            if(count($param['GUMP']['filtros']))
                $filtros[ $param['identificador'] ] = implode('|', $param['GUMP']['filtros']);

        }

        

        $request = array();
        foreach ($parametros as $k=>$param){
            foreach ($_REQUEST as $k1=>$param1){
                if(isset($_REQUEST[ $param['identificador'] ]))
                    $request[$k1] = $param1;
            }
        }

        //print_r($request);

        //Ahora validamos y mostramos errores
        $validados = $GLOBALS['GUMP']->validate( $request, $reglas );

        // Check if validation was successful
        if($validados === TRUE){

            //Filtramos los parametros
            $paramRecibidos = $GLOBALS['GUMP']->filter($request, $filtros);

            //Asignamos los parametros al global
            $GLOBALS['parametros'] = $paramRecibidos;

        }else{
            $errores = $GLOBALS['GUMP']->get_readable_errors();
            $GLOBALS['resultado']->setError($errores);
        }
    }


    //Zipper
    function Zipper($source, $destination){

        if (!extension_loaded('zip') || !file_exists($source)) {
            return false;
        }

        $zip = new ZipArchive();
        if (!$zip->open($destination, ZIPARCHIVE::CREATE)) {
            return false;
        }

        $source = str_replace('\\', '/', realpath($source));

        //Si el source es un directorio
        if (is_dir($source) === true)
        {
            $files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($source), RecursiveIteratorIterator::SELF_FIRST);

            foreach ($files as $file)
            {
                $file = str_replace('\\', '/', $file);

                // Ignorar "." y ".." y backups
                if( in_array(substr($file, strrpos($file, '/')+1), array('.', '..', 'backups')) )
                    continue;

                $file = realpath($file);

                if (is_dir($file) === true)
                {
                    $zip->addEmptyDir(str_replace($source . '/', '', $file . '/'));
                }
                else if (is_file($file) === true)
                {
                    $zip->addFromString(str_replace($source . '/', '', $file), file_get_contents($file));
                }
            }
        }
        else if (is_file($source) === true)
        {
            $zip->addFromString(basename($source), file_get_contents($source));
        }

        return $zip->close();
    }


    /*
    ESTA FUNCION TE LEE LA MENTE INCLUSO ANTES DE QUE TIPEES EL TEXTO, A TRAVEZ DEL TAROT Y
    Y LA QUIROMANCIA ARCANICA DETECTA LOS DESEOS DEL CLIENTE INLCUSO SI ES MEDIO CATETO...

        Nos pasan un string, (que es el texto que el cliente ingreso) y
        un arreglo con:
            1- ID del Rubro
            2- Nombre del Rubro
            3- Palabras o frases que identifican al Rubro

        Devolvemos un arreglo con los rubros ordenados por prioridades (score)
        En primer lugar el que mas cerca estuvo de acertarle...

        Si no le pegamos a nada ... el array vuelve vacio e intentamos hacer la busqueda de siempre
        tratando de decifrar que nos quiso poner por otro metodo menos exacto


        $resultado['query'] = Query final luego de detectar fabricantes
        $resultado['rubros'] = array(id,score,palabra_matcheada);

    */
    function adivina_rubro($query){

        //Obtenemos primiero todos los rubros con sus palabras clave
        $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT id, nombre, nombres_alternativos FROM catalogo_rubros");
        $stmt->execute();
        $rubros = $stmt->fetchAll(PDO::FETCH_ASSOC);

        //Inizializamos
        $resultado['rubros'] = array();
        $resultado['query'] = $query;

        //Por cada uno de los rubros
        foreach ($rubros as $k=>$rubro){
            //Guardamos todoas las palabras clave
            //implotadas en un arreglo
            $rubro_nombres_alternativos = explode(',', $rubro['nombres_alternativos']);

            //Aca vamos a guardar el score que obtuvo este rubro
            $score = 0;

            //Ahora comparamos cada una de las palabras...
            foreach ($rubro_nombres_alternativos as $k1=>$palabra){
                $palabra = trim($palabra);
                if($palabra != '')
                    //Encontramos la palabra
                    if(strpos($query, $palabra) !== false){
                        //chequeamos si ya existia antes en el arreglo
                        $largo = strlen($palabra);
                        $agregar = true;
                        foreach ($resultado['rubros'] as $k2 => $r_tmp){
                            
                            if( ($r_tmp[0] == (int)$rubro['id']) ){
                                $agregar = false;
                                if($largo > $r_tmp[1]){
                                    $resultado['rubros'][$k2][1] = $largo;
                                    $resultado['rubros'][$k2][2] = $r_tmp[2];
                                }

                                break;
                            }
                        }
                        //Guardamos el nuevo rubro, con la palabra o frase matcheada
                        if($agregar) $resultado['rubros'][] = array((int)$rubro['id'],strlen($palabra), $palabra);
                    }
            }
        }

        //Ordenamos el arreglo de rubros por Score
        usort($resultado['rubros'], function($a, $b) {
            return $b[1] - $a[1];
        });


        //Por cada rubro matcheado le quitamos al query el rubro
        foreach($resultado['rubros'] as $x => $rub){
            $resultado['query'] = str_replace($rub[2], '', $resultado['query']);
        }   

        //Quitamos todos los espacios extra que hayan quedado
        $resultado['query'] = preg_replace('/([^\d])\1+/', '$1', trim($resultado['query']));

        //Devolvemos el resultado
        return $resultado;
    }




    /*
        Esta funcion recibe una la palabra e intenta corregir faltas de
        ortografia que puede producir el cliente al intentar buscar
    */
    function corrector($query){

        //Devolvemos resultado
        $resultado = [];

        //Por defecto no es un codigo
        $resultado['es_codigo'] = false;

        //Quitamos espacios de principio
        //y final de la frase
        $query = trim($query);

        //Quitamos las letras duplicadas de las palabras
        $query = preg_replace('/([^\d])\1+/', '$1', $query);

        //Reemplazamos acentos por letras comunes
        $query = $this->stripAccents($query);

        //Quitamos todo lo que no sea letra o numero
        $query = preg_replace("/[^ \w-_]+/", "", $query);

        //Solo dejamos un espacio entre las palabras
        $query = preg_replace( "/\s+/", " ", $query );

        //Pasamos todo a minuscula
        $query = strtolower($query);

        //Verificamos si es un codigo


        //Quitamos prefijos simbolos y extras inecesarios
        $query = str_replace('c/', '', $query);
        $query = str_replace('s/', '', $query);
        $query = str_replace('p/', '', $query);
        $query = str_replace(' r-', ' r', $query);
        $query = str_replace(' r ', ' r', $query);
        $query = str_replace(' f-', ' f', $query);
        $query = str_replace(' f ', ' f', $query);
        $query = str_replace(' k-', ' k', $query);
        $query = str_replace(' k ', ' k', $query);


        //Quitamos los 'de' 'para' 'con'
        $query = str_replace(' de ', ' ', $query);
        $query = str_replace(' a ', ' ', $query);
        $query = str_replace(' para ', ' ', $query);
        $query = str_replace(' con ', ' ', $query);
        //$query = str_replace('-', ' ', $query);
        $query = str_replace('_', ' ', $query);


        //Quitamos las letras duplicadas de las palabras
        $query = preg_replace('/([^\d])\1+/', '$1', $query);

        //Transformamos la query en un arreglo
        $query_arr = explode(' ', $query);

        //Por cada palabra de la query...
        foreach ($query_arr as $x => $palabra){

            //Buscamos matches en el diccionario
            foreach ($this->dicc as $k=>$ocurrencia){

                //ocurrencias de una misma palabra
                foreach ($ocurrencia as $k1=>$pal){
                    
                    //Si hay coindicencia
                    if($palabra == $pal){
                        $query_arr[$x] = $k;
                        break;
                    }
                }
            }
        }

        //Volvemos a armar el String
        $query = implode(' ', $query_arr);


        //Buscamos modelos de autos especiales
        $query = str_replace('renault 4', 'renault r4s', $query);
        $query = str_replace('renault 4s', 'renault r4s', $query);
        $query = str_replace('renault 5', 'renault r5', $query);
        $query = str_replace('renault 6', 'renault r6', $query);
        $query = str_replace('renault 9', 'renault r9', $query);
        $query = str_replace('renault 11', 'renault r11', $query);
        $query = str_replace('renault 12', 'renault r12', $query);
        $query = str_replace('renault 18', 'renault r18', $query);
        $query = str_replace('renault 19', 'renault r19', $query);
        $query = str_replace('renault 21', 'renault r21', $query);


        //Devolvemos el resultado
        return $query;
    }

    /*
        Esta funcion recibe una la palabra e intenta corregir faltas de
        ortografia que puede producir el cliente al intentar buscar y
        detecta si es un codigo o no, devuelve
        Array[
                query => query ya corregida
                es_codigo => boolean si detecta codigo o no
                codigo = Array [0,1,2] con prefijo, codigo y sufijo
        ]
    */
    function corrector_busqueda($query){

        //Devolvemos resultado
        $resultado = [];

        //Por defecto no es un codigo
        $resultado['es_codigo'] = false;

        //Quitamos espacios de principio
        //y final de la frase
        $query = trim($query);

        //Quitamos las letras duplicadas de las palabras
        $query = preg_replace('/([^\d])\1+/', '$1', $query);

        //Reemplazamos acentos por letras comunes
        $query = $this->stripAccents($query);

        //Quitamos todo lo que no sea letra o numero
        $query = preg_replace("/[^ \w-_]+/", "", $query);

        //Solo dejamos un espacio entre las palabras
        $query = preg_replace( "/\s+/", " ", $query );

        //Pasamos todo a minuscula
        $query = strtolower($query);

        //Verificamos si es un codigo
        $es_codigo = $this->detecta_codigo($query);

        //Guardamos resultado
        $resultado['es_codigo'] = is_array($es_codigo);

        //Si es un codigo RETORNAMOS
        if($resultado['es_codigo']){
            $resultado['query'] = $query;
            $resultado['codigo'] = $es_codigo;
            return $resultado;
        }


        //Si no es un codigo proseguimos

        //Quitamos prefijos simbolos y extras inecesarios
        $query = str_replace('c/', '', $query);
        $query = str_replace('s/', '', $query);
        $query = str_replace('p/', '', $query);
        $query = str_replace(' r-', ' r', $query);
        $query = str_replace(' r ', ' r', $query);
        $query = str_replace(' f-', ' f', $query);
        $query = str_replace(' f ', ' f', $query);
        $query = str_replace(' k-', ' k', $query);
        $query = str_replace(' k ', ' k', $query);


        //Quitamos los 'de' 'para' 'con'
        $query = str_replace(' de ', ' ', $query);
        $query = str_replace(' a ', ' ', $query);
        $query = str_replace(' para ', ' ', $query);
        $query = str_replace(' con ', ' ', $query);
        $query = str_replace('_', ' ', $query);
        $query = str_replace('-', ' ', $query);


        //Quitamos las letras duplicadas de las palabras
        $query = preg_replace('/([^\d])\1+/', '$1', $query);

        //Transformamos la query en un arreglo
        $query_arr = explode(' ', $query);

        //Por cada palabra de la query...
        foreach ($query_arr as $x => $palabra){

            //Buscamos matches en el diccionario
            foreach ($this->dicc as $k=>$ocurrencia){

                //ocurrencias de una misma palabra
                foreach ($ocurrencia as $k1=>$pal){
                    
                    //Si hay coindicencia
                    if($palabra == $pal){
                        $query_arr[$x] = $k;
                        break;
                    }
                }
            }
        }

        //Volvemos a armar el String
        $query = implode(' ', $query_arr);


        //Buscamos modelos de autos especiales
        $query = str_replace('renault 4', 'renault r4s', $query);
        $query = str_replace('renault 4s', 'renault r4s', $query);
        $query = str_replace('renault 5', 'renault r5', $query);
        $query = str_replace('renault 6', 'renault r6', $query);
        $query = str_replace('renault 9', 'renault r9', $query);
        $query = str_replace('renault 11', 'renault r11', $query);
        $query = str_replace('renault 12', 'renault r12', $query);
        $query = str_replace('renault 18', 'renault r18', $query);
        $query = str_replace('renault 19', 'renault r19', $query);
        $query = str_replace('renault 21', 'renault r21', $query);

        //Query
        $resultado['query'] = $query;

        //Devolvemos el resultado
        return $resultado;
    }

    function detecta_codigo($query){
        //prefijo, codigo y sufijo
        preg_match("/(\d+)-([0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz]+)-(\d+)/", $query, $es_codigo);
        if(count($es_codigo)) return Array($es_codigo[1], $es_codigo[2], $es_codigo[3]);

        //prefijo y codigo
        preg_match("/(\d+)-([0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz]+)/", $query, $es_codigo);
        if(count($es_codigo)) return Array($es_codigo[1], $es_codigo[2]);

        return false;
    }

    /*
        A esta funcion le pasamos el string  de busqueda e intenta
        adivinar los fabricantes que el cliente esta intentando solicitar
        devuelve un array con el siguiente formato:

        $resultado['query'] = Query final luego de detectar fabricantes
        $resultado['fabricantes'] = array(1,5,6);
    */
    function adivina_fabricante($query){

        //Obtenemos primiero todos los rubros con sus palabras clave
        $stmt = $GLOBALS['conf']['pdo']->prepare("SELECT id, nombre FROM catalogo_marcas WHERE padre = 1");
        $stmt->execute();
        $fab = $stmt->fetchAll(PDO::FETCH_ASSOC);

        //Array de fabricantes
        $fabricantes = array();

        //Corregimos
        foreach ($fab as $k => $f){
            
            //guardamos el nombre en minuscula
            $nombre = strtolower($f['nombre']);

            //No repetimos las letras
            $nombre = preg_replace('/([^\d])\1+/', '$1', $nombre);

            //Guardamos el id
            $fabricantes[$nombre] = $f['id'];
        }

        //Guardamos el query en un arreglo
        $palabras = explode(' ', $query);

        //Resultado
        $resultado['fabricantes'] = array();

        //Buscamos en cada una de las palabras
        foreach ($palabras as $k => $p){
            foreach ($fabricantes as $nombre => $id){
                //si tenemos match
                if($nombre == $p){
                    //Guardamos el Fabricante
                    $resultado['fabricantes'][] = $id;

                    //Quitamos la palabra
                    unset($palabras[$k]);
                }
            }
        }


        //rearmamos el query
        $resultado['query'] = implode(' ', $palabras);

        //Devolvemos todo
        return $resultado; 
    }

    /* A esta funcion le pasamos una frase de busqueda, la limpia y la prepara para ser usada */
    function limpia_query_busqueda($frase){
        //Quitamos espacios de principio
        //y final de la frase
        $palabras = trim($frase);

        //Quitamos las letras duplicadas de las palabras
        $palabras = preg_replace('/([^\d])\1+/', '$1', $palabras);

        //Reemplazamos acentos por letras comunes
        $palabras = $this->stripAccents($palabras);

        //Quitamos todo lo que no sea letra o numero
        $palabras = preg_replace("/[^ \w-_]+/", "", $palabras);

        //Solo dejamos un espacio entre las palabras
        $palabras = preg_replace( "/\s+/", " ", $palabras );

        return $palabras;
    }


    function formatSizeUnits($bytes){
        if ($bytes >= 1073741824)
        {
            $bytes = number_format($bytes / 1073741824, 2) . ' GB';
        }
        elseif ($bytes >= 1048576)
        {
            $bytes = number_format($bytes / 1048576, 2) . ' MB';
        }
        elseif ($bytes >= 1024)
        {
            $bytes = number_format($bytes / 1024, 2) . ' KB';
        }
        elseif ($bytes > 1)
        {
            $bytes = $bytes . ' bytes';
        }
        elseif ($bytes == 1)
        {
            $bytes = $bytes . ' byte';
        }
        else
        {
            $bytes = '0 bytes';
        }

        return $bytes;
    }

    function nombre_dia($timestamp){

        //Codigo de dia
        $dia = date('N', $timestamp);
        $label = "";

        //Si el timestamp tiene menos de una semana
        if($timestamp > strtotime("-1 week"))
            if($dia == 1) $label = "Lunes";
            elseif($dia == 2) $label = "Martes";
            elseif($dia == 3) $label = "Miercoles";
            elseif($dia == 4) $label = "Jueves";
            elseif($dia == 5) $label = "Viernes";
            elseif($dia == 6) $label = "Sabado";
            elseif($dia == 7) $label = "Domingo";

        return $label;
    }

    //Crea la coneccion a la base de datos
    function mysql_conn($host,$db_name,$user,$password){
        try{
            $pdo = new PDO('mysql:host='.$GLOBALS['conf']['mysql_host'].';dbname='.$GLOBALS['conf']['mysql_db'].';charset=utf8',$GLOBALS['conf']['mysql_user'],$GLOBALS['conf']['mysql_pass']);
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 
            $pdo->setAttribute(PDO::ATTR_TIMEOUT, 600); 
            $pdo->exec("SET NAMES utf8");
            $pdo->query("SET SESSION group_concat_max_len=3423543543;");
            return $pdo;
        }catch(PDOException $ex){
            echo 'No se pudo conectar con la base de datos.'; 
        }
    }

    function smartReadFile($location, $filename, $mimeType = 'application/octet-stream'){
        if (!file_exists($location))
        {
            header ("HTTP/1.1 404 Not Found");
            return;
        }
        
        $size   = filesize($location);
        $time   = date('r', filemtime($location));
        
        $fm     = @fopen($location, 'rb');
        if (!$fm)
        {
            header ("HTTP/1.1 505 Internal server error");
            return;
        }
        
        $begin  = 0;
        $end    = $size - 1;
        
        if (isset($_SERVER['HTTP_RANGE']))
        {
            if (preg_match('/bytes=\h*(\d+)-(\d*)[\D.*]?/i', $_SERVER['HTTP_RANGE'], $matches))
            {
                $begin  = intval($matches[1]);
                if (!empty($matches[2]))
                {
                    $end    = intval($matches[2]);
                }
            }
        }
        if (isset($_SERVER['HTTP_RANGE']))
        {
            header('HTTP/1.1 206 Partial Content');
        }
        else
        {
            header('HTTP/1.1 200 OK');
        }
        
        header("Content-Type: $mimeType"); 
        header('Cache-Control: public, must-revalidate, max-age=0');
        header('Pragma: no-cache');  
        header('Accept-Ranges: bytes');
        header('Content-Length:' . (($end - $begin) + 1));
        if (isset($_SERVER['HTTP_RANGE']))
        {
            header("Content-Range: bytes $begin-$end/$size");
        }
        header("Content-Disposition: inline; filename=$filename");
        header("Content-Transfer-Encoding: binary");
        header("Last-Modified: $time");
        
        $cur    = $begin;
        fseek($fm, $begin, 0);
        
        while(!feof($fm) && $cur <= $end && (connection_status() == 0))
        {
            print fread($fm, min(1024 * 16, ($end - $cur) + 1));
            $cur += 1024 * 16;
        }
    }

    function calculaSiguienteHorario($interlocutor){
            
            

            $sql = "SELECT      usuario,
                                (SELECT COUNT(*) FROM horarios_vacaciones WHERE (inicio <= CURDATE()) AND (fin >= CURDATE()) AND (usuario = s.usuario)) as vacaciones,
                                (SELECT COUNT(*) FROM horarios_semanario WHERE (horario = (SELECT horario FROM usuarios WHERE usuario = s.usuario)) AND (dia = (WEEKDAY(CURDATE())+1)) AND (NOW() >= hora_inicio) AND (NOW() <= hora_fin)) as horario,
                                (SELECT horario FROM usuarios WHERE usuario = s.usuario) as horario_id,
                                (SELECT COUNT(*) FROM horarios_feriados WHERE   (dia = CURDATE()) AND ((hora_inicio IS NULL) OR (hora_fin IS NULL) OR ((NOW() >= hora_inicio) AND (NOW() <= hora_fin)))) as es_feriado
                    FROM        sessions s
                    WHERE       (latencia >= NOW() - INTERVAL 1 MINUTE) AND (usuario = '".$interlocutor."')
                    ORDER BY    usuario ASC;";

            $sql .= "SELECT * FROM horarios_semanario WHERE horario = (SELECT horario FROM usuarios WHERE usuario = '".$interlocutor."');";
            $sql .= "SELECT * FROM horarios_vacaciones WHERE usuario = '".$interlocutor."' ORDER BY fin ASC;";
            $sql .= "SELECT * FROM horarios_feriados;";


            $stmt = $GLOBALS['conf']['pdo']->query($sql);
            $estado = $stmt->fetchAll(PDO::FETCH_ASSOC);
            if(count($estado)) $e = $estado[0];
        
            //Si no tiene ninguna Session Abierta
            //O Tiene una session abierta pero esta de vacaciones
            //O Tiene seteado algun horario y esta fuera de ese horario
            //O Es feriado y no se trabaja en niingun horario del dia...
            if( !count($estado) || (int)$e['vacaciones'] || (!is_null($e['horario_id']) && !(int)$e['horario']) || (int)$e['es_feriado']){

                //SQL QUERY                
                $stmt->nextRowset();
                $horario = $stmt->fetchAll(PDO::FETCH_ASSOC);
                $stmt->nextRowset();
                $vacaciones = $stmt->fetchAll(PDO::FETCH_ASSOC);
                $stmt->nextRowset();
                $feriados = $stmt->fetchAll(PDO::FETCH_ASSOC);
                $no_laborables = [];
                $especiales = [];



                //Vacaciones
                foreach($vacaciones as $k=>$v){
                    //Casteamos fechas
                    $comienzo = new DateTime($v['inicio']);
                    $fin = new DateTime($v['fin']);
                    //Calculamos el periodo
                    $intervalo = DateInterval::createFromDateString('1 day');
                    $periodo = new DatePeriod($comienzo, $intervalo, $fin);
                    //Agregamos las fechas individuales de las vacaciones
                    foreach ($periodo as $dt) $no_laborables[] = $dt->format("Y-m-d");
                    $no_laborables[] = $v['fin'];
                }

                //Feriados
                foreach($feriados as $k=>$v)
                    if(is_null($v['hora_inicio']) && is_null($v['hora_fin'])) $no_laborables[] = $v['dia'];
                    else {
                        $especiales[$v['dia']]['hora_inicio'] = strtotime($v['dia'] ." ". $v['hora_inicio']);
                        $especiales[$v['dia']]['hora_fin'] = strtotime($v['dia'] ." ". $v['hora_fin']);
                    }

                


                for ($c=0; $c < 30; $c++){
                    
                    if($c == 180) return "No es posible calcular Cuando estara onlinep";

                    //Hoy
                    $proximoDia = new DateTime();

                    if($c>0){
                        //Agregamos n dias
                        $proximoDia->modify('+'.$c.' day');
                        //Reseteamos Hora
                        $proximoDia->setTime(0, 2);
                    }

                    //Dia de la semana
                    $proximoDia_w = (int)$proximoDia->format('w');
                    //Domingo = 7
                    if(!$proximoDia_w) $proximoDia_w = 7;
                    //Timestamp
                    $proximoDia_ts = $proximoDia->getTimestamp();
                    //Fecha
                    $proximoDia_fecha = $proximoDia->format("Y-m-d");

                    $dias_label = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sábado");
                    $meses_label = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");

                    //Dia label
                    $proximoDia_label = "";

                    //Label
                    if($c==0) $proximoDia_label = "hoy";
                    elseif($c==1) $proximoDia_label = "mañana";
                    elseif($c<=7) $proximoDia_label = "el próximo " . $dias_label[date('w',$proximoDia_ts)];
                    elseif($c>7) $proximoDia_label = "el " . $dias_label[date('w',$proximoDia_ts)]." ".date('j',$proximoDia_ts) . " de " . $meses_label[date('n',$proximoDia_ts)-1];

                    //Si hoy es dia laborable
                    foreach($horario as $k=>$h){
                        //Si: 
                        // -Es dia laborable 
                        // -Todavia tenemos horario por cumplir
                        // -No son vacaciones 
                        // -No es un Feriado Tradicional ni Especial
                        if( ((int)$h['dia'] == $proximoDia_w) && (strtotime($proximoDia_fecha . ' ' . $h['hora_inicio']) > $proximoDia_ts) && !in_array($proximoDia_fecha, $no_laborables) && !isset($especiales[$proximoDia_fecha])){
                            
                            return $proximoDia_label . " a las " . date('G:i',strtotime($h['hora_inicio']))." Hs.";

                        //Si es un feriado especial y aun queda horario por cumplir
                        }else if(isset($especiales[$proximoDia_fecha]) && ($especiales[$proximoDia_fecha]['hora_inicio'] > $proximoDia_ts) && !in_array($proximoDia_fecha, $no_laborables)){
                            return $proximoDia_label . " a las " . date('G:i',$especiales[$proximoDia_fecha]['hora_inicio']) . " Hs.";
                        }
                    }
                }
            }
    }

    function imprimePedido($pedido){

        $computers = $GLOBALS['printnode']->viewComputers();
        $connectedPc = null;

        //PRINTSERVER 
        if(isset($computers[55393]) && ($computers[55393]->state == 'connected')){
            $connectedPc = $computers[55393];
        }

        //SSD
        else if(isset($computers[55137]) && ($computers[55137]->state == 'connected')){
            $connectedPc = $computers[55137];
        }

        //Mechanic
        elseif(isset($computers[55360]) && ($computers[55360]->state == 'connected')){
            $connectedPc = $computers[55360];
        }

        //Gabriel
        elseif(isset($computers[55294]) && ($computers[55294]->state == 'connected')){
            $connectedPc = $computers[55294];
        }

        //Si no hay ninguna impresora prendida
        //Por defecto es el printserver
        if(!$connectedPc) $connectedPc = $computers[55393];

        $printJob = new \PrintNode\Entity\PrintJob($client);
        $printJob->title = "Pedido ".$pedido;
        $printJob->source = 'Catalogo';
        $printJob->contentType = 'pdf_base64';
        $printJob->addPdfFile('./pedido_'.$pedido.'.pdf');

        //printserver 388963
        if($connectedPc->id == 55393) $printJob->printer = '388963';
        //ssd
        if($connectedPc->id == 55137) $printJob->printer = '378619';
        //mecanico
        if($connectedPc->id == 55360) $printJob->printer = '336289';
        //gabrielrt
        if($connectedPc->id == 55294) $printJob->printer = '391255';


        $printJobId = $client->createPrintJob($printJob);
    }

    function createPrintNodeClient(){
        
        //Si el Printnode client ya existe salimos
        if(isset($GLOBALS['printnode'])) return;

        //Api Key
        $stmt = $GLOBALS['conf']['pdo']->query("SELECT api_key FROM impresoras LIMIT 1");
        $apikey =  $stmt->fetchColumn();

        //Creamos cliente
        $credentials = new \PrintNode\Credentials\ApiKey($apikey);
        $GLOBALS['printnode'] = new \PrintNode\Client($credentials);
    }

    /*
    *   SQL que trae todos las aplicaciones de un articulo en particular
    */
    function SQL_get_applications_by_articulo_id($articulo){

        $sql = "    SELECT CONCAT('[', 
                            GROUP_CONCAT(CONCAT(
                                '{',
                                    '\"marca\":\"', (SELECT nombre FROM catalogo_autos_marca WHERE id = marca_id), '\", ',
                                    '\"marca_id\": ', marca_id, ', ',
                                    '\"modelo\": \"', (SELECT nombre FROM catalogo_autos_modelo WHERE id = modelo_id), '\", ',
                                    '\"modelo_id\": ', modelo_id, ', ',
                                    '\"version\": \"', (SELECT descripcion FROM catalogo_autos_versiones WHERE id = version_id) ,'\", ',
                                    '\"desde\": ', anio_inicio, ', ',
                                    '\"hasta\": ', anio_final,
                                '}'
                                ) separator ','),
                        ']') as modelos

                    FROM catalogo_articulos_aplicaciones a 
                    WHERE articulo_id = ".$articulo." 
                    GROUP BY articulo_id";
        return $sql;
    }

    /*
    *   SQL que trae todos los rubros padres de un rubro n
    */
    function SQL_get_rubros_padres($rubro_id){
        $sql = "SELECT
                    @r AS _id,
                    (SELECT @r := padre FROM catalogo_rubros WHERE id = _id) AS padre,
                    nombre,
                    nombre_singular,
                    @l := @l + 1 AS lvl,
                    1 as grupo
                FROM
                    (SELECT @r := ".$rubro_id.", @l := 0) vars,
                    catalogo_rubros m
                WHERE @r <> 0 AND _id <> ".$rubro_id."
                GROUP BY grupo";
        
        return $sql;
    }

    /*
     * Funcion que reordena las aplicaciones que nos pasan en un string
     * en JSON, el resultado final es el siguiente

        [
            {
                marca: 'Peugeot',
                marca_id: 12,
                modelos: [
                    {modelo: '206', modelo_id: 52, version: '1.4', desde: '', hasta, ''},
                    {modelo: 'Partner', modelo_id: 53, version: 'hdi', desde: '95', hasta, '--'},
                ]
            }
        ]

    */
    function reestructurar_aplicaciones($items){
        if(is_null($items)) return Array();

        //Results
        $results = Array();

        //Data
        $items = json_decode($items);
       
        //Recorremos las aplicaciones
        foreach ($items as $k=>$i){

            //Creamos item nuevo
            $new_item = new stdClass();
            $new_item->modelo = $i->modelo;
            $new_item->modelo_id = $i->modelo_id;
            $new_item->version = $i->version;
            $new_item->desde = ($i->desde == -1 ? '--' : substr((string)$i->desde,2));
            $new_item->hasta = ($i->hasta == -1 ? '--' : substr((string)$i->hasta,2));

            //Creamos el fabricante
            if(!isset($results[$i->marca_id])){
                $results[$i->marca_id] = new stdClass();
                $results[$i->marca_id]->nombre = $i->marca;
                $results[$i->marca_id]->id = $i->marca_id;
                $results[$i->marca_id]->modelos = Array();
            }

            //Agregamos el modelo
            $results[$i->marca_id]->modelos[] = $new_item;
        }

        //Retornamos resultados
        return $results = array_values($results);
    }

    function reestructurar_dimensiones($item){

        //String a JSON
        $item['dimensiones'] = json_decode($item['dimensiones']);
        
        //volvemos
        if(!count($item['dimensiones'])) return;

        //Tomamos valores
        foreach($item['dimensiones'] as $k=>$d){
            $item['dimensiones'][$k]->valor = $item['extra_' . $d->id];
            foreach($d->opciones as $x=>$o)
                if($o->id == (int)$item['dimensiones'][$k]->valor)
                    $item['dimensiones'][$k]->valor = $o->nombre;

            //delete
            unset($item['dimensiones'][$k]->opciones);
        }

        //Delete extra
        foreach($item as $x=>$f) if(strpos($x, 'extra_') !== false) unset($item[$x]);

        return $item;
    }

    function reestructurar_imgs($imgs){
        $imgs = json_decode($imgs);

        $fotos = array();
        foreach($imgs as $k=>$c)
            for($i=0; $i < 100; $i++){
                $img = $c.'-'.$i.'.jpg';
                if(file_exists('./articulos/'.$img)) $fotos[] = $img;
                else break;
            }

        return $fotos;
    }
}
?>